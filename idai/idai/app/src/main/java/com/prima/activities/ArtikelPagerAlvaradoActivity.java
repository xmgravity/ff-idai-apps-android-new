package com.prima.activities;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.prima.MainActivity;
import com.prima.adapters.PagerAdapter;
import com.prima.controllers.Parser;
import com.prima.entities.Artikel;
import com.prima.entities.Question;
import com.prima.fragments.ArtikelOrange1Fragment;
import com.prima.helpers.DatabaseHandler;
import com.prima.helpers.DateHelper;

import java.util.ArrayList;
import java.util.List;

import prima.test.prima.R;

/**
 * Created by Codelabs on 06/10/2015.
 */
public class ArtikelPagerAlvaradoActivity extends BaseActivity implements View.OnClickListener {
    private Button ok;
    private Context mContext;
    private ViewPager mPager;
    private PagerAdapter mPagerAdapter;
    private static int currentPage;
    PageListener pageListener;
    static TextView title;
    List<Fragment> fragmentList = new ArrayList<Fragment>();
    static LinearLayout indikatorWrapper;
    private Artikel artikel;

    //
    private ImageView close;
    private RequestQueue mRequest;
    MaterialDialog dialog;
    Question question;
    DateHelper dateHelper;
    DatabaseHandler db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = getApplicationContext();
    }

    @Override
    public void initView() {
        indikatorWrapper = (LinearLayout) findViewById(R.id.indikator_pager);
        mPager = (ViewPager) findViewById(R.id.pager);
        title = (TextView) findViewById(R.id.title);
        pageListener = new PageListener();
        mPager.setOnPageChangeListener(pageListener);
        ok = (Button) findViewById(R.id.lanjut);
        Typeface tf = Typeface.createFromAsset(getApplicationContext().getAssets(), "BREVIA-BLACK.OTF");
        title.setTypeface(tf);

        Typeface tfAriali = Typeface.createFromAsset(getApplicationContext().getAssets(), "ariali.ttf");
        Typeface tfArialb = Typeface.createFromAsset(getApplicationContext().getAssets(), "arialbd.ttf");

        TextView trademark = (TextView) findViewById(R.id.trademark);
        TextView trademark2 = (TextView) findViewById(R.id.trademark2);
        TextView trademark3 = (TextView) findViewById(R.id.trademark3);
        trademark.setTypeface(tfAriali);
        trademark2.setTypeface(tfArialb);
        trademark3.setTypeface(tfAriali);
        db = new DatabaseHandler(context);
        dateHelper = new DateHelper();
        mRequest = Volley.newRequestQueue(getApplicationContext());
        close = (ImageView) findViewById(R.id.close);
        title = (TextView) findViewById(R.id.title);
        title.setText(getResources().getString(R.string.titleSkorAlvarado));


    }

    @Override
    public void setUICallbacks() {
        ok.setOnClickListener(this);
        close.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if (v == ok) {
            getQuestion();
        } else if (v == close) {
            Intent i = new Intent(getApplicationContext(), MainActivity.class);
            startActivity(i);
            finish();
        }
    }


    @Override
    public int getLayout() {
        return R.layout.activity_alvarado;
    }

    @Override
    public void updateUI() {
        indikatorWrapper.removeAllViewsInLayout();
        fragmentList.clear();
        Bundle bn = getIntent().getExtras();
        if (bn != null) {
            String respon = bn.getString("result");
            artikel = Parser.getArtikel(respon);
            //   title.setText(artikel.getObjects().get(0).getTitle());
            for (int i = 0; i < artikel.getObjects().size(); i++) {
                fragmentList.add(new ArtikelOrange1Fragment(i, artikel));
                View child = getLayoutInflater().inflate(R.layout.indikator_pager, null);
                View child2 = getLayoutInflater().inflate(R.layout.indikator_pager_blue, null);
                if (i == 0) {
                    indikatorWrapper.addView(child2);
                } else {
                    indikatorWrapper.addView(child);
                }
            }

            setupFrae();
            if (artikel.getObjects().size() > 1) {
                indikatorWrapper.setVisibility(View.VISIBLE);
            } else {
                indikatorWrapper.setVisibility(View.GONE);
            }
        }


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

    }


    @Override
    public void onBackPressed() {
        Intent i = new Intent(getApplicationContext(), MainActivity.class);
        startActivity(i);
        finish();
    }

    private class PageListener extends ViewPager.SimpleOnPageChangeListener {
        public void onPageSelected(int position) {
            currentPage = position;
            // title.setText(artikel.getObjects().get(position).getTitle());
            indikatorWrapper.removeAllViewsInLayout();
            for (int i = 0; i < artikel.getObjects().size(); i++) {
                View child = getLayoutInflater().inflate(R.layout.indikator_pager, null);
                View child2 = getLayoutInflater().inflate(R.layout.indikator_pager_blue, null);
                if (i == position) {
                    indikatorWrapper.addView(child2);
                } else {
                    indikatorWrapper.addView(child);
                }
            }

        }
    }



    private void getQuestion() {
        /*final String code = "ALVARADO";
        progresDialog(false, getResources().getString(R.string.MemuatData));

        String response = "{\"status\":1,\"messages\":\"Sukses\",\"data\":[{\"id\":971,\"questionnaire_age_id\":\"355\",\"title\":\"Nyeri berpindah\",\"sub_title\":\"\",\"version\":\"1\",\"age\":{\"id\":355,\"code\":null,\"name\":\"Nyeri berpindah\",\"range_age\":\"60-72 Bulan\",\"min_age\":\"60\",\"max_age\":\"72\",\"type\":\"Text\",\"filename\":null},\"options\":[{\"id\":939,\"question_id\":\"971\",\"type\":\"Text\",\"option\":\"Ya\",\"score\":\"1.00\",\"version\":\"1\"},{\"id\":940,\"question_id\":\"971\",\"type\":\"Text\",\"option\":\"Tidak\",\"score\":\"0.00\",\"version\":\"1\"}]},{\"id\":972,\"questionnaire_age_id\":\"356\",\"title\":\"Mual dan muntah\",\"sub_title\":\"\",\"version\":\"1\",\"age\":{\"id\":356,\"code\":null,\"name\":\"Mual dan muntah\",\"range_age\":\"60-72 Bulan\",\"min_age\":\"60\",\"max_age\":\"72\",\"type\":\"Text\",\"filename\":null},\"options\":[{\"id\":941,\"question_id\":\"972\",\"type\":\"Text\",\"option\":\"Ya\",\"score\":\"1.00\",\"version\":\"1\"},{\"id\":942,\"question_id\":\"972\",\"type\":\"Text\",\"option\":\"Tidak\",\"score\":\"0.00\",\"version\":\"1\"}]},{\"id\":973,\"questionnaire_age_id\":\"357\",\"title\":\"Anoreksia\",\"sub_title\":\"\",\"version\":\"1\",\"age\":{\"id\":357,\"code\":null,\"name\":\"Anoreksia\",\"range_age\":\"60-72 Bulan\",\"min_age\":\"60\",\"max_age\":\"72\",\"type\":\"Text\",\"filename\":null},\"options\":[{\"id\":943,\"question_id\":\"973\",\"type\":\"Text\",\"option\":\"Ya\",\"score\":\"1.00\",\"version\":\"1\"},{\"id\":944,\"question_id\":\"973\",\"type\":\"Text\",\"option\":\"Tidak\",\"score\":\"0.00\",\"version\":\"1\"}]},{\"id\":981,\"questionnaire_age_id\":\"358\",\"title\":\"Nyeri tekan pada kuadran kanan bawah\",\"sub_title\":\"\",\"version\":\"1\",\"age\":{\"id\":358,\"code\":null,\"name\":\"Nyeri tekan pada kuadran kanan bawah\",\"range_age\":\"60-72 Bulan\",\"min_age\":\"60\",\"max_age\":\"72\",\"type\":\"Text\",\"filename\":null},\"options\":[{\"id\":945,\"question_id\":\"981\",\"type\":\"Text\",\"option\":\"Ya\",\"score\":\"2.00\",\"version\":\"1\"},{\"id\":946,\"question_id\":\"981\",\"type\":\"Text\",\"option\":\"Tidak\",\"score\":\"0.00\",\"version\":\"1\"}]},{\"id\":982,\"questionnaire_age_id\":\"359\",\"title\":\"Nyeri lepas pada kuadran kanan bawah\",\"sub_title\":\"\",\"version\":\"1\",\"age\":{\"id\":359,\"code\":null,\"name\":\"Nyeri lepas pada kuadran kanan bawah\",\"range_age\":\"60-72 Bulan\",\"min_age\":\"60\",\"max_age\":\"72\",\"type\":\"Text\",\"filename\":null},\"options\":[{\"id\":947,\"question_id\":\"982\",\"type\":\"Text\",\"option\":\"Ya\",\"score\":\"1.00\",\"version\":\"1\"},{\"id\":948,\"question_id\":\"982\",\"type\":\"Text\",\"option\":\"Tidak\",\"score\":\"0.00\",\"version\":\"1\"}]},{\"id\":983,\"questionnaire_age_id\":\"360\",\"title\":\"Peningkatan suhu\",\"sub_title\":\"\",\"version\":\"1\",\"age\":{\"id\":360,\"code\":null,\"name\":\"Peningkatan suhu\",\"range_age\":\"60-72 Bulan\",\"min_age\":\"60\",\"max_age\":\"72\",\"type\":\"Text\",\"filename\":null},\"options\":[{\"id\":949,\"question_id\":\"983\",\"type\":\"Text\",\"option\":\"Ya\",\"score\":\"1.00\",\"version\":\"1\"},{\"id\":950,\"question_id\":\"983\",\"type\":\"Text\",\"option\":\"Tidak\",\"score\":\"0.00\",\"version\":\"1\"}]},{\"id\":991,\"questionnaire_age_id\":\"361\",\"title\":\"Leukositosis (>10.000/uL)\",\"sub_title\":\"\",\"version\":\"1\",\"age\":{\"id\":361,\"code\":null,\"name\":\"Leukositosis (\\u003e10.000\\u2044uL)\",\"range_age\":\"60-72 Bulan\",\"min_age\":\"60\",\"max_age\":\"72\",\"type\":\"Text\",\"filename\":null},\"options\":[{\"id\":951,\"question_id\":\"991\",\"type\":\"Text\",\"option\":\"Ya\",\"score\":\"2.00\",\"version\":\"1\"},{\"id\":952,\"question_id\":\"991\",\"type\":\"Text\",\"option\":\"Tidak\",\"score\":\"0.00\",\"version\":\"1\"}]},{\"id\":992,\"questionnaire_age_id\":\"362\",\"title\":\"Pergeseran ke kiri dari neurofil 75%\",\"sub_title\":\"\",\"version\":\"1\",\"age\":{\"id\":362,\"code\":null,\"name\":\"Pergeseran ke kiri dari neurofil 75%\",\"range_age\":\"60-72 Bulan\",\"min_age\":\"60\",\"max_age\":\"72\",\"type\":\"Text\",\"filename\":null},\"options\":[{\"id\":953,\"question_id\":\"992\",\"type\":\"Text\",\"option\":\"Ya\",\"score\":\"1.00\",\"version\":\"1\"},{\"id\":954,\"question_id\":\"992\",\"type\":\"Text\",\"option\":\"Tidak\",\"score\":\"0.00\",\"version\":\"1\"}]}]}";
        question = new Question();
        question = Parser.getQuestion(response);


        for (int j = 0; j < question.getObjects().size(); j++) {
            db.deleteQuestion(question.getObjects().get(j).getId(), question.getObjects().get(j).getMin_age(), question.getObjects().get(j).getMax_age(), "");
            db.deleteAnswer(question.getObjects().get(j).getId());
            db.insertQuestion(question.getObjects().get(j), code);
            for (int k = 0; k < question.getObjects().get(j).getAnswer_option().size(); k++) {
                db.insertAnswer(question.getObjects().get(j).getAnswer_option().get(k));
            }
        }*/

        Intent i = new Intent(getApplicationContext(), AlvaradoQuestionActivity.class);
        //i.putExtra("respon", response.toString());
        startActivity(i);
    }


    private void setupFrae() {
        mPagerAdapter = new PagerAdapter(getSupportFragmentManager(),
                ArtikelPagerAlvaradoActivity.this, fragmentList);
        mPager.setAdapter(mPagerAdapter);

    }

}
