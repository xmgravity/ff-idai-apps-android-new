package com.prima.helpers;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Codelabs on 3/26/2015.
 */
public class TimeConvert {

    public static String relativeTime(String date) throws ParseException {
        final int SECOND = 1;
        final int MINUTE = 60*SECOND;
        final int HOUR = 60*MINUTE;
        final int DAY = 24*HOUR;
        final int MONTH = 30*DAY;
        final int YEAR = 12*MONTH;

        String interval;

        SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date d = f.parse(date);
        long delta = System.currentTimeMillis() - d.getTime();

        long intervalTime = (delta / 1000);

        if (intervalTime < 0) {
            interval = "In the future!";

        } else if (intervalTime < MINUTE) {
            interval = (intervalTime==1) ? "one second ago" : (intervalTime)+" seconds ago";

        } else if (intervalTime < 2 * MINUTE) {
            interval = "a minute ago";

        } else if (intervalTime < 45 * MINUTE) {
            interval = (intervalTime/MINUTE)%60+" minutes ago";

        } else if (intervalTime < 90 * MINUTE) {
            interval = "an hour ago";

        } else if (intervalTime < 24 * HOUR) {
            interval = (intervalTime/HOUR)%60+" hours ago";

        } else if (intervalTime < 48 * HOUR) {
            interval = "yesterday";

        } else if (intervalTime < 30 * DAY) {
            interval = (intervalTime/DAY)+" days ago";

        } else if (intervalTime < 12 * MONTH) {
            interval = (intervalTime/MONTH==1) ? "one month ago" : (intervalTime/MONTH)+" months ago";

        } else {
            interval = (intervalTime/YEAR==1) ? "one year ago" : (intervalTime/YEAR)+" years ago";

        }

        return interval;
    }

}
