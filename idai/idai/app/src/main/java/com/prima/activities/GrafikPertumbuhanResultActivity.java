package com.prima.activities;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.prima.Preferences;
import com.prima.adapters.PagerAdapter;
import com.prima.fragments.GrafikPertumbuhanFragment;

import java.util.ArrayList;
import java.util.List;

import prima.test.prima.R;

/**
 * Created by Codelabs on 09/08/2015.
 */
public class GrafikPertumbuhanResultActivity extends BaseActivity implements View.OnClickListener {
    private Context mContext;
    private ImageView close;
    private ImageView back;
    private ViewPager mPager;
    private Button buttonKesimpulan, buttonContohKasus;
    private PagerAdapter mPagerAdapter;
    PageListener pageListener;
    List<Fragment> fragmentList;
    private TextView title;
    ArrayList<String> data;
    ArrayList<String> titlegrafik;
    ArrayList<String> listInterpretasi;
    ArrayList<String> listLabel;
    static TextView jenis;
    static LinearLayout indikatorWrapper;
    String result;
    private TextView interpretasi, keterangan, interpretasiText;
    private int currentPage;
    private String hasilKesimpulan;

    public ArrayList<String> getData() {
        return data;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = getApplicationContext();
    }

    @Override
    public void initView() {
        currentPage = 0;
        title = (TextView) findViewById(R.id.title);
        indikatorWrapper = (LinearLayout) findViewById(R.id.indikator_pager);
        interpretasi = (TextView) findViewById(R.id.interpretasi);
        keterangan = (TextView) findViewById(R.id.keterangan);
        interpretasiText = (TextView) findViewById(R.id.interpretasiText);
        back = (ImageView) findViewById(R.id.back);
        close = (ImageView) findViewById(R.id.close);
        buttonKesimpulan = (Button) findViewById(R.id.buttonKesimpulan);
        buttonContohKasus = (Button) findViewById(R.id.buttonContohKasus);
        //set font
        Typeface tf0 = Typeface.createFromAsset(this.getAssets(), "BREVIA-BLACK.OTF");
        Typeface tf = Typeface.createFromAsset(this.getAssets(), "BREVIA-MEDIUM.OTF");
        title.setTypeface(tf0);
        buttonKesimpulan.setTypeface(tf);
        buttonContohKasus.setTypeface(tf);
        mPager = (ViewPager) findViewById(R.id.pager);
        pageListener = new PageListener();
        mPager.setOnPageChangeListener(pageListener);
        jenis = (TextView) findViewById(R.id.jenis);


        Typeface tfAriali = Typeface.createFromAsset(getApplicationContext().getAssets(), "ariali.ttf");
        Typeface tfArialb = Typeface.createFromAsset(getApplicationContext().getAssets(), "arialbd.ttf");

        TextView trademark = (TextView) findViewById(R.id.trademark);
        TextView trademark2 = (TextView) findViewById(R.id.trademark2);
        TextView trademark3 = (TextView) findViewById(R.id.trademark3);
        trademark.setTypeface(tfAriali);
        trademark2.setTypeface(tfArialb);
        trademark3.setTypeface(tfAriali);

        if(preferences.getString(Preferences.TIPE_USER, "").equalsIgnoreCase("awam")){
            trademark.setVisibility(View.GONE);
            trademark2.setVisibility(View.GONE);
            trademark3.setVisibility(View.GONE);
        }
    }

    @Override
    public void setUICallbacks() {
        back.setOnClickListener(this);
        close.setOnClickListener(this);
        keterangan.setOnClickListener(this);
        buttonKesimpulan.setOnClickListener(this);
        buttonContohKasus.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        if (v == back) {
            if (currentPage > 0) {
                currentPage--;
                mPager.setCurrentItem(currentPage, true);
            } else {
                fragmentList.clear();
                mPagerAdapter.notifyDataSetChanged();
                finish();
            }
        } else if (v == close) {
            fragmentList.clear();
            mPagerAdapter.notifyDataSetChanged();
            finish();
        } else if (v == keterangan) {
            Intent i = new Intent(context, ImagePreviewActivity.class);
            i.putExtra("image", listLabel.get(0));
            startActivity(i);

        } else if (v == buttonKesimpulan) {
            Intent i = new Intent(context, GrafikPertumbuhanKesimpulanActivity.class);
            i.putExtra("hasilKesimpulan", hasilKesimpulan);
            i.putExtra("listInterpretasi", listInterpretasi);
            i.putExtra("titleGrafik", titlegrafik);
            startActivity(i);

        } else if (v == buttonContohKasus) {
            Intent i = new Intent(context, GrafikPertumbuhanContohKasusActivity.class);
            i.putExtra("titleKasus", titlegrafik.get(currentPage));
            startActivity(i);

        }
    }


    @Override
    public int getLayout() {
        return R.layout.activity_grafik_pertumbuhan_result;
    }

    @Override
    public void updateUI() {
        currentPage = 0;
        indikatorWrapper.removeAllViewsInLayout();
        fragmentList = new ArrayList<Fragment>();
        data = new ArrayList<String>();
        titlegrafik = new ArrayList<String>();
        listInterpretasi = new ArrayList<String>();
        listLabel = new ArrayList<String>();
        Bundle bn = getIntent().getExtras();
        if (bn != null) {
            data = bn.getStringArrayList("page");
            titlegrafik = bn.getStringArrayList("title");
            listInterpretasi = bn.getStringArrayList("interpretasi");
            listLabel = bn.getStringArrayList("label");
            result = bn.getString("result");
            //  Toast.makeText(getApplicationContext(), String.valueOf(data.size()), Toast.LENGTH_LONG).show();
            for (int i = 0; i < data.size(); i++) {
                fragmentList.add(new GrafikPertumbuhanFragment(i));
                View child = getLayoutInflater().inflate(R.layout.indikator_pager, null);
                View child2 = getLayoutInflater().inflate(R.layout.indikator_pager_blue, null);
                if (i == 0) {
                    indikatorWrapper.addView(child2);
                } else {
                    indikatorWrapper.addView(child);
                }
            }
            jenis.setText(titlegrafik.get(0));


            if(listLabel.get(0).equalsIgnoreCase("NELLHAUS")){
                title.setText(getResources().getString(R.string.GrafikNellhausResult));
            }else{
                if(titlegrafik.get(0).equalsIgnoreCase("Head Circumference for Age") ||  titlegrafik.get(0).equalsIgnoreCase("Lingkar Kepala Terhadap Umur")){
                    if (preferences.getString(Preferences.LANGUAGE, "").equalsIgnoreCase("id")){
                        title.setText("GRAFIK LINGKAR KEPALA " + listLabel.get(0));
                    } else{
                        title.setText(listLabel.get(0) + " " + getResources().getString(R.string.GrafikLingkarKepala));
                    }
                }else{
                    if (preferences.getString(Preferences.LANGUAGE, "").equalsIgnoreCase("id")){
                        title.setText("GRAFIK PERTUMBUHAN " + listLabel.get(0));
                    } else{
                        title.setText(listLabel.get(0) + " " + getResources().getString(R.string.GrafikPertumbuhan));
                    }
                }

            }

            if (listInterpretasi.get(0).equals("")) {
                interpretasi.setText("-");
            } else {
                interpretasi.setText(listInterpretasi.get(0));
            }

            setupFrae();



            if (listLabel.get(0).equalsIgnoreCase("WHO")) {
                if(preferences.getString(Preferences.TIPE_USER, "").equalsIgnoreCase("awam")){
                    interpretasiText.setVisibility(View.GONE);
                    interpretasi.setVisibility(View.GONE);
                    buttonKesimpulan.setVisibility(View.VISIBLE);
                    keterangan.setVisibility(View.GONE);
                    buttonContohKasus.setVisibility(View.GONE);

                } else {
                    interpretasiText.setVisibility(View.GONE);
                    interpretasi.setVisibility(View.GONE);
                    buttonKesimpulan.setVisibility(View.GONE);
                    keterangan.setVisibility(View.VISIBLE);
                    buttonContohKasus.setVisibility(View.GONE);
                }

            } else if (listLabel.get(0).equalsIgnoreCase("CDC")) {
                if (titlegrafik.get(0).equalsIgnoreCase(getResources().getString(R.string.heightWeight))) {
                    if(preferences.getString(Preferences.TIPE_USER, "").equalsIgnoreCase("awam")){
                        interpretasiText.setVisibility(View.GONE);
                        interpretasi.setVisibility(View.GONE);
                        buttonKesimpulan.setVisibility(View.GONE);
                        keterangan.setVisibility(View.GONE);
                        buttonContohKasus.setVisibility(View.VISIBLE);

                        if(Double.parseDouble(preferences.getString(Preferences.MOM_HEIGHT, ""))>0){
                            interpretasiText.setVisibility(View.VISIBLE);
                            interpretasi.setVisibility(View.VISIBLE);
                            buttonKesimpulan.setVisibility(View.GONE);
                            keterangan.setVisibility(View.GONE);
                            buttonContohKasus.setVisibility(View.GONE);

                            interpretasi.setText(getResources().getString(R.string.InterpretasiTPG)+" "+preferences.getString(Preferences.MOM_HEIGHT, "")+getResources().getString(R.string.InterpretasiTPG2)+" "+preferences.getString(Preferences.DAD_HEIGHT, "")+"cm");

                        }
                    } else {
                        interpretasiText.setVisibility(View.GONE);
                        interpretasi.setVisibility(View.GONE);
                        buttonKesimpulan.setVisibility(View.GONE);
                        keterangan.setVisibility(View.VISIBLE);
                        buttonContohKasus.setVisibility(View.GONE);

                        if(Double.parseDouble(preferences.getString(Preferences.MOM_HEIGHT, ""))>0){
                            interpretasiText.setVisibility(View.VISIBLE);
                            interpretasi.setVisibility(View.VISIBLE);
                            buttonKesimpulan.setVisibility(View.GONE);
                            keterangan.setVisibility(View.GONE);
                            buttonContohKasus.setVisibility(View.GONE);
                            interpretasi.setText(getResources().getString(R.string.InterpretasiTPG)+" "+preferences.getString(Preferences.MOM_HEIGHT, "")+getResources().getString(R.string.InterpretasiTPG2)+" "+preferences.getString(Preferences.DAD_HEIGHT, "")+"cm");

                        }
                    }
                } else {
                    if(preferences.getString(Preferences.TIPE_USER, "").equalsIgnoreCase("awam")){
                        interpretasiText.setVisibility(View.GONE);
                        interpretasi.setVisibility(View.GONE);
                        buttonKesimpulan.setVisibility(View.GONE);
                        keterangan.setVisibility(View.GONE);
                        buttonContohKasus.setVisibility(View.VISIBLE);
                    } else {
                        interpretasiText.setVisibility(View.GONE);
                        interpretasi.setVisibility(View.GONE);
                        buttonKesimpulan.setVisibility(View.GONE);
                        keterangan.setVisibility(View.VISIBLE);
                        buttonContohKasus.setVisibility(View.GONE);
                    }
                }
            }

            if(titlegrafik.get(0).equalsIgnoreCase(getResources().getString(R.string.grafikLingkarKepalaUmur))) {
                interpretasiText.setVisibility(View.GONE);
                interpretasi.setVisibility(View.GONE);
                buttonKesimpulan.setVisibility(View.GONE);
                keterangan.setVisibility(View.GONE);
                buttonContohKasus.setVisibility(View.GONE);
            }

            //System.out.println(listInterpretasi.size());
            hasilKesimpulan = "0";
            for (int i = 0; i < listInterpretasi.size(); i++) {
                System.out.println("interpretasi " + i + " :" +titlegrafik.get(i) +" " +listInterpretasi.get(i)+"");
                if (listInterpretasi.get(i).equalsIgnoreCase("Gizi Cukup") || listInterpretasi.get(i).equalsIgnoreCase("Normal")){
                    System.out.println(listInterpretasi.get(i) +" : "+hasilKesimpulan);
                }else{
                    hasilKesimpulan = "1";
                    System.out.println(listInterpretasi.get(i) +" : "+hasilKesimpulan);
                }
                System.out.println("Total Kesimpulan Interpretasi: "+hasilKesimpulan);
            }


        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

    }

    private class PageListener extends ViewPager.SimpleOnPageChangeListener {
        public void onPageSelected(int position) {
            currentPage = position;
            jenis.setText(titlegrafik.get(position));
            interpretasi.setText(listInterpretasi.get(position));
            if (preferences.getString(Preferences.LANGUAGE, "").equalsIgnoreCase("id")){
                title.setText("GRAFIK PERTUMBUHAN " + listLabel.get(0));
            } else{
                title.setText(listLabel.get(0) + " " + getResources().getString(R.string.GrafikPertumbuhan));
            }            indikatorWrapper.removeAllViewsInLayout();
            for (int i = 0; i < data.size(); i++) {
                View child = getLayoutInflater().inflate(R.layout.indikator_pager, null);
                View child2 = getLayoutInflater().inflate(R.layout.indikator_pager_blue, null);
                if (i == position) {
                    indikatorWrapper.addView(child2);
                } else {
                    indikatorWrapper.addView(child);
                }
            }


            if (listLabel.get(position).equalsIgnoreCase("WHO")) {
                if(preferences.getString(Preferences.TIPE_USER, "").equalsIgnoreCase("awam")){
                    interpretasiText.setVisibility(View.GONE);
                    interpretasi.setVisibility(View.GONE);
                    buttonKesimpulan.setVisibility(View.VISIBLE);
                    keterangan.setVisibility(View.GONE);
                    buttonContohKasus.setVisibility(View.GONE);
                } else {
                    interpretasiText.setVisibility(View.GONE);
                    interpretasi.setVisibility(View.GONE);
                    buttonKesimpulan.setVisibility(View.GONE);
                    keterangan.setVisibility(View.VISIBLE);
                    buttonContohKasus.setVisibility(View.GONE);
                }
            } else if (listLabel.get(position).equalsIgnoreCase("CDC")) {
                if (titlegrafik.get(position).equalsIgnoreCase(getResources().getString(R.string.heightWeight))) {
                    if(preferences.getString(Preferences.TIPE_USER, "").equalsIgnoreCase("awam")){

                        interpretasiText.setVisibility(View.GONE);
                        interpretasi.setVisibility(View.GONE);
                        buttonKesimpulan.setVisibility(View.GONE);
                        keterangan.setVisibility(View.GONE);
                        buttonContohKasus.setVisibility(View.VISIBLE);

                        if(Double.parseDouble(preferences.getString(Preferences.MOM_HEIGHT, ""))>0){
                            interpretasiText.setVisibility(View.VISIBLE);
                            interpretasi.setVisibility(View.VISIBLE);
                            buttonKesimpulan.setVisibility(View.GONE);
                            keterangan.setVisibility(View.GONE);
                            buttonContohKasus.setVisibility(View.GONE);
                            interpretasi.setText(getResources().getString(R.string.InterpretasiTPG)+" "+preferences.getString(Preferences.MOM_HEIGHT, "")+getResources().getString(R.string.InterpretasiTPG2)+" "+preferences.getString(Preferences.DAD_HEIGHT, "")+"cm");

                        }
                    } else {
                        interpretasiText.setVisibility(View.GONE);
                        interpretasi.setVisibility(View.GONE);
                        buttonKesimpulan.setVisibility(View.GONE);
                        keterangan.setVisibility(View.VISIBLE);
                        buttonContohKasus.setVisibility(View.GONE);

                        if(Double.parseDouble(preferences.getString(Preferences.MOM_HEIGHT, ""))>0){
                            interpretasiText.setVisibility(View.VISIBLE);
                            interpretasi.setVisibility(View.VISIBLE);
                            buttonKesimpulan.setVisibility(View.GONE);
                            keterangan.setVisibility(View.GONE);
                            buttonContohKasus.setVisibility(View.GONE);
                            interpretasi.setText(getResources().getString(R.string.InterpretasiTPG)+" "+preferences.getString(Preferences.MOM_HEIGHT, "")+getResources().getString(R.string.InterpretasiTPG2)+" "+preferences.getString(Preferences.DAD_HEIGHT, "")+"cm");

                        }
                    }
                } else {
                    if(preferences.getString(Preferences.TIPE_USER, "").equalsIgnoreCase("awam")){
                        interpretasiText.setVisibility(View.GONE);
                        interpretasi.setVisibility(View.GONE);
                        buttonKesimpulan.setVisibility(View.GONE);
                        keterangan.setVisibility(View.GONE);
                        buttonContohKasus.setVisibility(View.VISIBLE);
                    } else {
                        interpretasiText.setVisibility(View.GONE);
                        interpretasi.setVisibility(View.GONE);
                        buttonKesimpulan.setVisibility(View.GONE);
                        keterangan.setVisibility(View.VISIBLE);
                        buttonContohKasus.setVisibility(View.GONE);
                    }
                }
            }

            if(titlegrafik.get(position).equalsIgnoreCase(getResources().getString(R.string.grafikLingkarKepalaUmur))) {
                interpretasiText.setVisibility(View.GONE);
                interpretasi.setVisibility(View.GONE);
                buttonKesimpulan.setVisibility(View.GONE);
                keterangan.setVisibility(View.GONE);
                buttonContohKasus.setVisibility(View.GONE);
            }

        }
    }


    @Override
    public void onBackPressed() {
        //    Intent i = new Intent(getApplicationContext(), ArtikelPagerGrafikActivity.class);
        //    i.putExtra("result", result);
        //    startActivity(i);
        fragmentList.clear();
        mPagerAdapter.notifyDataSetChanged();
        finish();
    }

    private void setupFrae() {
        mPagerAdapter = new PagerAdapter(getSupportFragmentManager(),
                GrafikPertumbuhanResultActivity.this, fragmentList);
        mPager.setAdapter(mPagerAdapter);

    }


}
