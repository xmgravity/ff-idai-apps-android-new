package com.prima.activities;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.prima.Preferences;
import com.prima.entities.Question;
import com.prima.helpers.DatabaseHandler;

import java.util.ArrayList;

import prima.test.prima.R;

/**
 * Created by Codelabs on 12/08/2015.
 */
public class AlvaradoQuestionActivity extends BaseActivity implements View.OnClickListener {
    private ImageView close;
    private ImageView back;
    private Button lanjut;
    TextView title;
    private ListView list;
    private ArrayList<String> listItem = new ArrayList<String>();
    private ArrayList<Integer> answered = new ArrayList<Integer>();
    MaterialDialog dialog;
    private RequestQueue mRequest;
    Question question;
    DatabaseHandler db;
    ImageView   rad_a1y, rad_a2y, rad_a3y, rad_b1y, rad_b2y, rad_b3y, rad_c1y, rad_c2y,
                rad_a1n, rad_a2n, rad_a3n, rad_b1n, rad_b2n, rad_b3n, rad_c1n, rad_c2n;
    int scores[] = {1,1,1,2,1,1,2,1};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }

    @Override
    public void initView() {
        db = new DatabaseHandler(context);
        mRequest = Volley.newRequestQueue(getApplicationContext());
        back = (ImageView) findViewById(R.id.back);
        close = (ImageView) findViewById(R.id.close);
        lanjut = (Button) findViewById(R.id.lanjut);
        title = (TextView) findViewById(R.id.tv_actionbar_title);
        Typeface tf = Typeface.createFromAsset(getApplicationContext().getAssets(), "BREVIA-BLACK.OTF");
        title.setTypeface(tf);
        title.setText(getResources().getString(R.string.SkorAlvarado));

        rad_a1n = (ImageView) findViewById(R.id.rad_a1n);
        rad_a2n = (ImageView) findViewById(R.id.rad_a2n);
        rad_a3n = (ImageView) findViewById(R.id.rad_a3n);
        rad_b1n = (ImageView) findViewById(R.id.rad_b1n);
        rad_b2n = (ImageView) findViewById(R.id.rad_b2n);
        rad_b3n = (ImageView) findViewById(R.id.rad_b3n);
        rad_c1n = (ImageView) findViewById(R.id.rad_c1n);
        rad_c2n = (ImageView) findViewById(R.id.rad_c2n);

        rad_a1y = (ImageView) findViewById(R.id.rad_a1y);
        rad_a2y = (ImageView) findViewById(R.id.rad_a2y);
        rad_a3y = (ImageView) findViewById(R.id.rad_a3y);
        rad_b1y = (ImageView) findViewById(R.id.rad_b1y);
        rad_b2y = (ImageView) findViewById(R.id.rad_b2y);
        rad_b3y = (ImageView) findViewById(R.id.rad_b3y);
        rad_c1y = (ImageView) findViewById(R.id.rad_c1y);
        rad_c2y = (ImageView) findViewById(R.id.rad_c2y);

        for (int i = 0; i < 8; i++) {
            answered.add(-1);
        }



    }

    @Override
    public void setUICallbacks() {
        back.setOnClickListener(this);
        close.setOnClickListener(this);
        lanjut.setOnClickListener(this);
        rad_a1n.setOnClickListener(this);
        rad_a2n.setOnClickListener(this);
        rad_a3n.setOnClickListener(this);
        rad_b1n.setOnClickListener(this);
        rad_b2n.setOnClickListener(this);
        rad_b3n.setOnClickListener(this);
        rad_c1n.setOnClickListener(this);
        rad_c2n.setOnClickListener(this);
        rad_a1y.setOnClickListener(this);
        rad_a2y.setOnClickListener(this);
        rad_a3y.setOnClickListener(this);
        rad_b1y.setOnClickListener(this);
        rad_b2y.setOnClickListener(this);
        rad_b3y.setOnClickListener(this);
        rad_c1y.setOnClickListener(this);
        rad_c2y.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        if (v == back) {
            //   Intent i = new Intent(getApplicationContext(), MilestoneRedflagActivity.class);
            //    startActivity(i);
            if (preferences.getString(Preferences.ID, "").equals("1")) {
                editor.putString(Preferences.BIRTH_DAY, "");
                editor.putString(Preferences.JENIS_KELAMIN, "");
                editor.putString(Preferences.BERAT, "");
                editor.putString(Preferences.TINGGI, "");
                editor.putString(Preferences.ID, "");
                editor.commit();
            }
            finish();
        } else if (v == close) {
            //    Intent i = new Intent(getApplicationContext(), MilestoneRedflagActivity.class);
            //    startActivity(i);
            if (preferences.getString(Preferences.ID, "").equals("1")) {
                editor.putString(Preferences.BIRTH_DAY, "");
                editor.putString(Preferences.JENIS_KELAMIN, "");
                editor.putString(Preferences.BERAT, "");
                editor.putString(Preferences.TINGGI, "");
                editor.putString(Preferences.ID, "");
                editor.commit();
            }
            finish();
        } else if (v == lanjut) {
            boolean answeredAll = true;
            for (int i = 0; i < answered.size(); i++) {
                if (answered.get(i) < 0) {
                    answeredAll = false;
                }
            }

            if (answeredAll) {
                int totalScore = 0, score = 0;
                for (int i = 0; i < answered.size(); i++) {
                    score += answered.get(i) * scores[i];
                    totalScore += scores[i];
                }
                String interpretasi;
                if (score < 6) {
                    interpretasi = getResources().getString(R.string.InterpretasiAlvarado);
                } else {
                    interpretasi = getResources().getString(R.string.InterpretasiAlvarado2);
                }
                Intent i = new Intent(getApplicationContext(), AlvaradoResultActivity.class);
                i.putExtra("score", score);
                i.putExtra("interpretasi", interpretasi);
                i.putExtra("max_score",totalScore);
                startActivity(i);
                finish();
            } else {
                dialog2(getResources().getString(R.string.JawabSeluruhPertanyaan), getResources().getString(R.string.Panduan));
            }
        } else if (v == rad_a1n) {
            setAnswered(0, false);
            rad_a1n.setBackgroundResource(R.mipmap.icn_bocheckedblue);
            rad_a1y.setBackgroundResource(R.mipmap.icn_boxuncheck);
        } else if (v == rad_a2n) {
            setAnswered(1, false);
            rad_a2n.setBackgroundResource(R.mipmap.icn_bocheckedblue);
            rad_a2y.setBackgroundResource(R.mipmap.icn_boxuncheck);
        } else if (v == rad_a3n) {
            setAnswered(2, false);
            rad_a3n.setBackgroundResource(R.mipmap.icn_bocheckedblue);
            rad_a3y.setBackgroundResource(R.mipmap.icn_boxuncheck);
        } else if (v == rad_b1n) {
            setAnswered(3, false);
            rad_b1n.setBackgroundResource(R.mipmap.icn_bocheckedblue);
            rad_b1y.setBackgroundResource(R.mipmap.icn_boxuncheck);
        } else if (v == rad_b2n) {
            setAnswered(4, false);
            rad_b2n.setBackgroundResource(R.mipmap.icn_bocheckedblue);
            rad_b2y.setBackgroundResource(R.mipmap.icn_boxuncheck);
        } else if (v == rad_b3n) {
            setAnswered(5, false);
            rad_b3n.setBackgroundResource(R.mipmap.icn_bocheckedblue);
            rad_b3y.setBackgroundResource(R.mipmap.icn_boxuncheck);
        } else if (v == rad_c1n) {
            setAnswered(6, false);
            rad_c1n.setBackgroundResource(R.mipmap.icn_bocheckedblue);
            rad_c1y.setBackgroundResource(R.mipmap.icn_boxuncheck);
        } else if (v == rad_c2n) {
            setAnswered(7, false);
            rad_c2n.setBackgroundResource(R.mipmap.icn_bocheckedblue);
            rad_c2y.setBackgroundResource(R.mipmap.icn_boxuncheck);
        } else if (v == rad_a1y) {
            setAnswered(0, true);
            rad_a1y.setBackgroundResource(R.mipmap.icn_bocheckedblue);
            rad_a1n.setBackgroundResource(R.mipmap.icn_boxuncheck);
        } else if (v == rad_a2y) {
            setAnswered(1, true);
            rad_a2y.setBackgroundResource(R.mipmap.icn_bocheckedblue);
            rad_a2n.setBackgroundResource(R.mipmap.icn_boxuncheck);
        } else if (v == rad_a3y) {
            setAnswered(2, true);
            rad_a3y.setBackgroundResource(R.mipmap.icn_bocheckedblue);
            rad_a3n.setBackgroundResource(R.mipmap.icn_boxuncheck);
        } else if (v == rad_b1y) {
            setAnswered(3, true);
            rad_b1y.setBackgroundResource(R.mipmap.icn_bocheckedblue);
            rad_b1n.setBackgroundResource(R.mipmap.icn_boxuncheck);
        } else if (v == rad_b2y) {
            setAnswered(4, true);
            rad_b2y.setBackgroundResource(R.mipmap.icn_bocheckedblue);
            rad_b2n.setBackgroundResource(R.mipmap.icn_boxuncheck);
        } else if (v == rad_b3y) {
            setAnswered(5, true);
            rad_b3y.setBackgroundResource(R.mipmap.icn_bocheckedblue);
            rad_b3n.setBackgroundResource(R.mipmap.icn_boxuncheck);
        } else if (v == rad_c1y) {
            setAnswered(6, true);
            rad_c1y.setBackgroundResource(R.mipmap.icn_bocheckedblue);
            rad_c1n.setBackgroundResource(R.mipmap.icn_boxuncheck);
        } else if (v == rad_c2y) {
            setAnswered(7, true);
            rad_c2y.setBackgroundResource(R.mipmap.icn_bocheckedblue);
            rad_c2n.setBackgroundResource(R.mipmap.icn_boxuncheck);
        }
    }


    @Override
    public int getLayout() {
        return R.layout.activity_alvarado_question;
    }

    @Override
    public void updateUI() {

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

    }


    @Override
    public void onBackPressed() {
        //   Intent i = new Intent(getApplicationContext(), MilestoneRedflagActivity.class);
        // startActivity(i);
        if (preferences.getString(Preferences.ID, "").equals("1")) {
            editor.putString(Preferences.BIRTH_DAY, "");
            editor.putString(Preferences.JENIS_KELAMIN, "");
            editor.putString(Preferences.BERAT, "");
            editor.putString(Preferences.TINGGI, "");
            editor.putString(Preferences.ID, "");
            editor.commit();
        }
        finish();
    }


    public void dialog2(String content, String msg) {
        new MaterialDialog.Builder(this)
                .title(msg)
                .content(content)
                .negativeText("OK")
                .callback(new MaterialDialog.ButtonCallback() {
                    @Override
                    public void onPositive(MaterialDialog dialog) {

                    }

                    @Override
                    public void onNegative(MaterialDialog dialog) {
                        super.onNegative(dialog);
                    }

                })
                .show();
    }

    private void setAnswered(int question, boolean yes) {
        if (yes)
            answered.set(question, 1);
        else
            answered.set(question, 0);
    }

}
