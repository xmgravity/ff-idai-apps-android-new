package com.prima.helpers;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

import com.prima.fragments.BaseFragment;

public class FragmentHelper {
	private FragmentManager fragmentManager;
	private static FragmentHelper fragmentHelper;
	
	public static FragmentHelper getInstance(FragmentManager fragmentManager){
		if (fragmentHelper == null) {
			fragmentHelper = new FragmentHelper();
		}
		fragmentHelper.setFragmentManager(fragmentManager);
		return fragmentHelper;
	}
	
	public void replaceFragment(int container,BaseFragment fragment,boolean addBackToStack){
		FragmentTransaction ft = getFragmentManager().beginTransaction();
		if (addBackToStack) {
			ft.addToBackStack(fragment.getPageTitle());
		}
		ft.replace(container, fragment);
		ft.commit();
	}

    public void replaceFragmentWithoutAnim(int container,BaseFragment fragment,boolean addBackToStack){
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        if (addBackToStack) {
            ft.addToBackStack(fragment.getPageTitle());
        }
        ft.replace(container, fragment);
        ft.commit();
    }

    public void replaceFragment(int container,BaseFragment fragment,boolean addBackToStack, String fragmentTag){
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        if (addBackToStack) {
            ft.addToBackStack(fragmentTag);
        }
        ft.replace(container, fragment,fragmentTag+"tag");
        ft.commit();
    }
	
	public void addFragment(int container,BaseFragment fragment,boolean addBackToStack){
		FragmentTransaction ft = getFragmentManager().beginTransaction();
		if (addBackToStack) {
			ft.addToBackStack(fragment.getPageTitle());
		}
		ft.add(container, fragment);
		ft.commit();
	}
	
	public void replaceFragment(int container,Fragment fragment,boolean addBackToStack){
		FragmentTransaction ft = getFragmentManager().beginTransaction();
		if (addBackToStack) {
			ft.addToBackStack(fragment.getTag());
		}
		ft.replace(container, fragment);
		ft.commit();
	}
	
	public void addFragment(int container,Fragment fragment,boolean addBackToStack){
		FragmentTransaction ft = getFragmentManager().beginTransaction();
		if (addBackToStack) {
			ft.addToBackStack(fragment.getTag());
		}
		ft.add(container, fragment);
		ft.commit();
	}
	
	public FragmentManager getFragmentManager() {
		return fragmentManager;
	}
	
	public void setFragmentManager(FragmentManager fragmentManager) {
		this.fragmentManager = fragmentManager;
	}
}
