package com.prima.entities;

import java.util.ArrayList;

/**
 * Created by Codelabs on 31/08/2015.
 */
public class Question extends Message {
    private ArrayList<Kuesioner> data;

    public ArrayList<Kuesioner> getObjects() {
        return data;
    }

    public void setObjects(ArrayList<Kuesioner> objects) {
        this.data = objects;
    }

    public class Kuesioner {
        private String id;
        private String questionnaire_age_id;
        private String title;
        private String sub_title;
        private String version;

        private Age age;

        public Age getAge(){return age;}
        public void setAge(Age age){this.age = age;}


        private ArrayList<Answer> options;

        public String getKode() {
            return age.getCode();
        }

        public void setKode(String kode) {
            this.age.setCode(kode);
        }

        public int getMin_age() {
            return age.getMinAge();
        }

        public void setMin_age(int min_age) {
            this.age.setMinAge(min_age);
        }

        public int getMax_age() {
            return age.getMaxAge();
        }

        public void setMax_age(int max_age) {
            this.age.setMaxAge(max_age);
        }

        public String getPath() {
            return age.getFilename();
        }

        public void setPath(String path) {
            this.age.setFilename(path);
        }

        public String getType() {
            return age.getType();
        }

        public void setType(String type) {
            this.age.setType(type);
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getQuestionnaire_age_id() {
            return questionnaire_age_id;
        }

        public void setQuestionnaire_age_id(String questionnaire_age_id) {
            this.questionnaire_age_id = questionnaire_age_id;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getSub_title() {
            return sub_title;
        }

        public void setSub_title(String sub_title) {
            this.sub_title = sub_title;
        }

        public String getQuestion_version() {
            return version;
        }

        public void setQuestion_version(String question_version) {
            this.version = question_version;
        }

        public ArrayList<Answer> getAnswer_option() {
            return options;
        }

        public void setAnswer_option(ArrayList<Answer> answer_option) {
            this.options = answer_option;
        }
    }

    public class Age{
        private String type;
        private int min_age;
        private int max_age;
        private String code;
        private String filename;

        public String getType() {
            return type;
        }
        public void setType(String type) {
            this.type = type;
        }

        public int getMinAge() {
            return min_age;
        }
        public void setMinAge(int min_age) {
            this.min_age = min_age;
        }

        public int getMaxAge() {
            return max_age;
        }
        public void setMaxAge(int max_age) {
            this.max_age = max_age;
        }

        public String getCode() {return code;}
        public void setCode(String code) {
            this.code = code;
        }

        public String getFilename() {return filename;}
        public void setFilename(String filename) {
            this.filename = filename;
        }

    }

    public class Answer {
        private String id;
        private String question_id;
        private String type;
        private String option;
        private String score;
        private String version;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getQuestionnaire_id() {
            return this.question_id;
        }

        public void setQuestionnaire_id(String questionnaire_id) {
            this.question_id = questionnaire_id;
        }

        public String getOption_type() {
            return type;
        }

        public void setOption_type(String option_type) {
            this.type = option_type;
        }

        public String getOption() {
            return option;
        }

        public void setOption(String option) {
            this.option = option;
        }

        public String getScore() {
            return score;
        }

        public void setScore(String score) {
            this.score = score;
        }

        public String getAnswer_version() {
            return version;
        }

        public void setAnswer_version(String answer_version) {
            this.version = answer_version;
        }
    }
}
