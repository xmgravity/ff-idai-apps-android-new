package com.prima.activities;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.prima.Preferences;
import com.prima.adapters.PagerAdapter;
import com.prima.controllers.Parser;
import com.prima.entities.Question;
import com.prima.fragments.BallardScoreFragment;
import com.prima.helpers.NonSwipeAbleViewPager;

import java.util.ArrayList;
import java.util.List;

import prima.test.prima.R;

/**
 * Created by Codelabs on 07/08/2015.
 */
public class BallardScoreQuestionActivity extends BaseActivity implements View.OnClickListener {
    private ImageView close;
    private ImageView back;
    private Button ok;
    private static Context mContext;
    private NonSwipeAbleViewPager mPager;
    private PagerAdapter mPagerAdapter;
    private static int currentPage;
    PageListener pageListener;
    static TextView title, page, title_question, sub_title_question;
    LinearLayout toolbar;
    private static ProgressBar progres;
    static int progresValue;
    static Question question;
    static List<Fragment> fragmentList = new ArrayList<Fragment>();
    private ArrayList<String> ansewer = new ArrayList<String>();

    public int getMax_score() {
        return max_score;
    }

    public void setMax_score(int max_score) {
        this.max_score = max_score;
    }

    private int max_score = 0;


    public List<Fragment> getFragmentList() {
        return fragmentList;
    }

    public ArrayList<String> getAnsewer() {
        return ansewer;
    }

    public void setAnsewer(ArrayList<String> ansewer) {
        this.ansewer = ansewer;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = getApplicationContext();
        currentPage = 0;


    }

    @Override
    public void initView() {
        page = (TextView) findViewById(R.id.page);
        title_question = (TextView) findViewById(R.id.title_question);
        sub_title_question = (TextView) findViewById(R.id.sub_title_question);
        progres = (ProgressBar) findViewById(R.id.progressBar);
        progres.setProgress(15);
        back = (ImageView) findViewById(R.id.back);
        mPager = (NonSwipeAbleViewPager) findViewById(R.id.pager);
        close = (ImageView) findViewById(R.id.close);
        pageListener = new PageListener();
        mPager.setOnPageChangeListener(pageListener);
        toolbar = (LinearLayout) findViewById(R.id.toolbar);

        title = (TextView) findViewById(R.id.tv_actionbar_title);
        Typeface tf = Typeface.createFromAsset(getApplicationContext().getAssets(), "BREVIA-BLACK.OTF");
        title.setTypeface(tf);
        title.setText("New Ballard Score");


        getData();


    }

    @Override
    public void setUICallbacks() {
        back.setOnClickListener(this);
        close.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        if (v == back) {
            if (currentPage > 0) {
                currentPage--;
                mPager.setCurrentItem(currentPage, true);
            } else {
                //Intent i = new Intent(getApplicationContext(), BallardScoreActivity.class);
                //  startActivity(i);
                if (preferences.getString(Preferences.ID,"").equals("1")) {
                    editor.putString(Preferences.BIRTH_DAY, "");
                    editor.putString(Preferences.JENIS_KELAMIN, "");
                    editor.putString(Preferences.BERAT, "");
                    editor.putString(Preferences.TINGGI, "");
                    editor.putString(Preferences.ID, "");
                    editor.commit();
                }
                finish();
            }
        } else if (v == close) {
            //  Intent i = new Intent(getApplicationContext(), BallardScoreActivity.class);
            //  startActivity(i);
            if (preferences.getString(Preferences.ID,"").equals("1")) {
                editor.putString(Preferences.BIRTH_DAY, "");
                editor.putString(Preferences.JENIS_KELAMIN, "");
                editor.putString(Preferences.BERAT, "");
                editor.putString(Preferences.TINGGI, "");
                editor.putString(Preferences.ID, "");
                editor.commit();
            }
            finish();
        }
    }


    @Override
    public int getLayout() {
        return R.layout.activity_ballard_question;
    }

    @Override
    public void updateUI() {

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

    }


    void getData() {
        Bundle bn = getIntent().getExtras();
        if (bn != null) {
            String response = bn.getString("respon");
            setupFrae();
            Log.i("RESULT JSON", response);
            question = new Question();
            question = Parser.getQuestion(response);
            sub_title_question.setText(question.getObjects().get(0).getTitle());
            title_question.setText(question.getObjects().get(0).getSub_title());
            fragmentList.clear();
            for (int i = 0; i < question.getObjects().size(); i++) {
                for (int j = 0; j < question.getObjects().get(i).getAnswer_option().size(); j++) {
                    if (j == question.getObjects().get(i).getAnswer_option().size() - 1) {
                        max_score = max_score + Integer.valueOf(question.getObjects().get(i).getAnswer_option().get(j).getScore().replace(".00",""));
                    }
                }
                fragmentList.add(new BallardScoreFragment(question.getObjects().get(i), i));
                ansewer.add("");
            }
            progresValue = 100 / fragmentList.size();
            progres.setProgress(progresValue);
            setupFrae();

            page.setText(page.getResources().getString(R.string.dari2) + " " + fragmentList.size() + " " + page.getResources().getString(R.string.penilaian));
        }
    }


    @Override
    public void onBackPressed() {
        if (currentPage > 0) {
            currentPage--;
            mPager.setCurrentItem(currentPage, true);
        } else {
            if (preferences.getString(Preferences.ID,"").equals("1")) {
                editor.putString(Preferences.BIRTH_DAY, "");
                editor.putString(Preferences.JENIS_KELAMIN, "");
                editor.putString(Preferences.BERAT, "");
                editor.putString(Preferences.TINGGI, "");
                editor.putString(Preferences.ID, "");
                editor.commit();
            }
            //  Intent i = new Intent(getApplicationContext(), BallardScoreActivity.class);
            //   startActivity(i);
            finish();
        }
    }

    private static class PageListener extends ViewPager.SimpleOnPageChangeListener {


        public void onPageSelected(int position) {
            currentPage = position;
            page.setText(currentPage + 1 + " " + page.getResources().getString(R.string.dari) + " "+ fragmentList.size() + " " + page.getResources().getString(R.string.penilaian));
            progres.setProgress(progresValue * (position + 1));
            sub_title_question.setText(question.getObjects().get(position).getTitle());
            title_question.setText(question.getObjects().get(position).getSub_title());
            if ((position + 1) == question.getObjects().size()) {
                progres.setProgress(100);
            }

        }
    }


    private void setupFrae() {
        mPagerAdapter = new PagerAdapter(getSupportFragmentManager(),
                BallardScoreQuestionActivity.this, fragmentList);
        mPager.setAdapter(mPagerAdapter);

    }

}
