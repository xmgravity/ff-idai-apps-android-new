package com.prima.activities;

import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.net.Uri;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.google.gson.JsonObject;
import com.google.gson.JsonSyntaxException;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.prima.Preferences;
import com.prima.controllers.ApiReferences;
import com.prima.controllers.Parser;
import com.prima.entities.Message;
import com.prima.helpers.DateHelper;
import com.prima.helpers.HexagonMaskView;
import com.soundcloud.android.crop.Crop;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import prima.test.prima.R;

/**
 * Created by Codelabs on 19/08/2015.
 */
public class BuatCatatanActivity extends BaseActivity implements View.OnClickListener {

    TextView title, datetime_value;
    ImageView close, back, profile_image, reminder;
    LinearLayout judul_layout, catatan_layout;
    MaterialDialog dialog;
    EditText title_value, description_value;
    DateHelper dateHelper;
    HexagonMaskView hexa;
    Button Simpan;
    private RequestQueue mRequest;
    private Uri selectedImage;
    private String imagePath;
    private Uri resultImage;


    @Override
    public void initView() {
        dateHelper = new DateHelper();
        mRequest = Volley.newRequestQueue(getApplicationContext());

        Simpan = (Button) findViewById(R.id.Simpan);
        datetime_value = (TextView) findViewById(R.id.note_date_time);
        title_value = (EditText) findViewById(R.id.judul_catatan);
        description_value = (EditText) findViewById(R.id.buat_catatan);
        title = (TextView) findViewById(R.id.tv_actionbar_title);
        catatan_layout = (LinearLayout) findViewById(R.id.catatan_layout);
        judul_layout = (LinearLayout) findViewById(R.id.judul_layout);
        reminder = (ImageView) findViewById(R.id.reminder);
        close = (ImageView) findViewById(R.id.close);
        back = (ImageView) findViewById(R.id.back);
        //set font
        Typeface tf = Typeface.createFromAsset(getApplicationContext().getAssets(), "BREVIA-BLACK.OTF");
        title.setTypeface(tf);
        title.setText(getResources().getString(R.string.Catatan2));

        hexa = new HexagonMaskView();
        Glide.with(BuatCatatanActivity.this)
                .load(preferences.getString(Preferences.FOTO, ""))
                .asBitmap()
                .override(200, 200)
                .into(new SimpleTarget<Bitmap>() {
                    @Override
                    public void onResourceReady(Bitmap bitmap, GlideAnimation anim) {
                        Bitmap b = hexa.getHexagonShape(hexa.scaleCenterCrop(bitmap, 200, 200));
                        close.setImageBitmap(b);
                    }
                });


    }

    @Override
    public void setUICallbacks() {
        reminder.setOnClickListener(this);
        back.setOnClickListener(this);
        Simpan.setOnClickListener(this);
        close.setOnClickListener(this);

    }


    @Override
    public void onClick(View v) {
        if (v == back) {
            Intent i = new Intent(getApplicationContext(), CatatanActivity.class);
            startActivity(i);
            finish();
        } else if (v == close) {
            showPickPhotoDialog();
        } else if (v == Simpan) {
            if (title_value.getText().toString().trim().equals("")
                    || description_value.getText().toString().trim().equals("")) {
                dialog(getResources().getString(R.string.HarapLengkapiData), getResources().getString(R.string.Perhatian));
            } else {
                save();
                Intent i = new Intent(getApplicationContext(), CatatanActivity.class);
                startActivity(i);
                finish();

            }

        }
    }


    private void save() {


        System.out.println("title :"+title_value.getText().toString()+", description :"+description_value.getText().toString());


        StringRequest listdata = new StringRequest(Request.Method.POST,
                ApiReferences.insertCatatan(), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    Log.i("RESULT JSON", response.toString());
                    Message message = Parser.getMessage(response
                            .toString());
                    if (message.getStatus().equalsIgnoreCase("1")) {
                        dialog(getResources().getString(R.string.CatatanBerhasilDibuat), getResources().getString(R.string.Perhatian));


                        Intent i = new Intent(getApplicationContext(), CatatanActivity.class);
                        startActivity(i);
                        finish();
                    }
                } catch (JsonSyntaxException e) {
                    dialog(getResources().getString(R.string.KesalahanServer), getResources().getString(R.string.Perhatian));
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                NetworkResponse response = error.networkResponse;
                if (error instanceof ServerError && response != null) {
                    try {
                        String res = new String(response.data,
                                HttpHeaderParser.parseCharset(response.headers, "utf-8"));
                        // Now you can use any deserializer to make sense of data
                        JSONObject obj = new JSONObject(res);
                        // dialog(res.toString(), getResources().getString(R.string.Perhatian));
                    } catch (UnsupportedEncodingException e1) {
                        // Couldn't properly decode data to string
                        e1.printStackTrace();
                    } catch (JSONException e2) {
                        // returned data is not JSONObject?
                        e2.printStackTrace();
                    }
                }
            }
        }) {

            public Map<String, String> getHeaders() {
                Map<String, String>  params = new HashMap<String, String>();
                params.put("token", preferences.getString(Preferences.ACCESS_TOKEN, ""));

                return params;
            }

            protected Map<String, String> getParams() {
                HashMap<String, String> params = new HashMap<String, String>();
                params.put("title", title_value.getText().toString());
                params.put("description", description_value.getText().toString());




                return params;

                /*
                    params.put("title", "");
                    params.put("description", "");
                    params.put(date_time", "");

                    return params;
                    */
            }
        };
        listdata.setRetryPolicy(new DefaultRetryPolicy(
                20000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        mRequest.add(listdata);
    }



    @Override
    public int getLayout() {
        return R.layout.activity_buat_catatan;
    }

    @Override
    public void updateUI() {

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1) {
            if (resultCode == RESULT_OK && data != null) {

                selectedImage = data.getData();
                String[] filePathColumn = {MediaStore.Images.Media.DATA};

                Cursor cursor = getContentResolver().query(selectedImage,
                        filePathColumn, null, null, null);
                cursor.moveToFirst();

                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                String picturePath = cursor.getString(columnIndex);
                imagePath = picturePath;
                cursor.close();

                if (imagePath != null) {
                    cropPicture();
                }

            }
        } else if (requestCode == 0) {
            if (resultCode == RESULT_OK) {

                final String[] imageColumns = {MediaStore.Images.Media._ID,
                        MediaStore.Images.Media.DATA};
                final String imageOrderBy = MediaStore.Images.Media._ID
                        + " DESC";
                Cursor cursor = getContentResolver().query(
                        MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                        imageColumns, null, null, imageOrderBy);
                int column_index_data = cursor
                        .getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
                cursor.moveToFirst();

                imagePath = cursor.getString(column_index_data);
                cursor.close();

                if (imagePath != null) {
                    cropPicture();
                } else {
                    String text = "Terjadi kesalahan pada aplikasi kamera. Silakan ulang foto kembali";
                    Toast.makeText(context, text, Toast.LENGTH_LONG).show();
                }
            }
        } else if (requestCode == Crop.REQUEST_CROP && resultCode == RESULT_OK) {
            imagePath = this.getExternalFilesDir(null)+fileDir+filename;
            Ion.with(context)
                    .load("http://54.169.50.255/revamp/api/v1/user/updateProfilePicture")
                    .setHeader("token", preferences.getString(Preferences.ACCESS_TOKEN, ""))
                    .setTimeout(60 * 60 * 1000)
                    .setMultipartFile("profile_picture", new File(imagePath))
                    .asJsonObject()
                    .setCallback(new FutureCallback<JsonObject>() {
                        @Override
                        public void onCompleted(Exception e, JsonObject result) {
                            if (e != null) {
                                Toast.makeText(context, "Error uploading file", Toast.LENGTH_LONG).show();
                                return;
                            }
                            Toast.makeText(context, "File upload complete", Toast.LENGTH_LONG).show();
                            imagePath = "http://54.169.50.255/revamp/" + result.getAsJsonObject("data").get("profile_picture").getAsString();
                            editor.putString(Preferences.FOTO, imagePath);
                            editor.commit();
                            Glide.with(BuatCatatanActivity.this)
                                    .load(preferences.getString(Preferences.FOTO, ""))
                                    .asBitmap()
                                    .override(200, 200)
                                    .into(new SimpleTarget<Bitmap>() {
                                        @Override
                                        public void onResourceReady(Bitmap bitmap, GlideAnimation anim) {
                                            Bitmap b = hexa.getHexagonShape(hexa.scaleCenterCrop(bitmap, 200, 200));
                                            close.setImageBitmap(b);
                                        }
                                    });
                        }
                    });
        }
    }


    @Override
    public void onBackPressed() {
        Intent i = new Intent(getApplicationContext(), CatatanActivity.class);
        startActivity(i);
        finish();
    }

    public void showPickPhotoDialog() {

        dialog = new MaterialDialog.Builder(this)
                .title("Pick Photo")
                .negativeText("Cancel")
                .items(R.array.photo)
                .itemsCallback(new MaterialDialog.ListCallback() {
                    @Override
                    public void onSelection(MaterialDialog dialog, View view, int which, CharSequence text) {
                        if (which == 0) {
                            pickFromCamera();
                        } else {
                            pickFromGalery();
                        }
                    }
                })
                .show();
    }


    private void pickFromCamera() {
        int requestCode = 0;
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss")
                .format(new Date());
        String imageFileName = "PHOTO" + timeStamp + "_";

        ContentValues values = new ContentValues();
        values.put(MediaStore.Images.Media.TITLE, imageFileName);
        selectedImage = getContentResolver().insert(
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);

        // create Intent to take a picture and return control to the calling
        // application

        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        intent.putExtra(MediaStore.EXTRA_OUTPUT, selectedImage);

        // start the image capture Intent
        startActivityForResult(intent, requestCode);
        dialog.dismiss();
    }

    private void pickFromGalery() {
        int requestCode = 1;
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss")
                .format(new Date());
        String imageFileName = "PHOTO" + timeStamp + "_";

        ContentValues values = new ContentValues();
        values.put(MediaStore.Images.Media.TITLE, imageFileName);
        selectedImage = getContentResolver().insert(
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);

        // create Intent to take a picture and return control to the calling
        // application

        Intent intent = new Intent(Intent.ACTION_PICK);
        intent.setType("image/*");
        intent.putExtra(MediaStore.EXTRA_OUTPUT, selectedImage);

        startActivityForResult(intent, requestCode);
        dialog.dismiss();
    }

    private void cropPicture() {
        if (!fileExists(fileDir)) {
            File newDir = new File(this.getExternalFilesDir(null)+fileDir);
            newDir.mkdirs();
        }
        if (fileExists(fileDir+filename)) {
            deleteFile(fileDir+filename);
        }
        File imgFile = new File(this.getExternalFilesDir(null)+fileDir, filename);
        resultImage = Uri.fromFile(imgFile);
        Crop.of(selectedImage, resultImage).asSquare().start(this);
    }

    public void dialog(String content, String msg) {
        new MaterialDialog.Builder(this)
                .title(msg)
                .content(content)
                .positiveText(getResources().getString(R.string.MuatUlang))
                .negativeText(getResources().getString(R.string.Kembali))
                .callback(new MaterialDialog.ButtonCallback() {
                    @Override
                    public void onPositive(MaterialDialog dialog) {
                        // getKalender();
                    }

                    @Override
                    public void onNegative(MaterialDialog dialog) {
                        // super.onNegative(dialog);
                        // finish();
                    }

                })
                .show();
    }


}