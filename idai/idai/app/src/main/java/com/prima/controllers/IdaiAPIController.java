package com.prima.controllers;

import android.content.Context;

import com.prima.Preferences;

public abstract class IdaiAPIController extends BaseAPIController {

	public IdaiAPIController(Context context) {
		super(context);
		String accessToken = preferences.getString(Preferences.ACCESS_TOKEN,"");
		client.addHeader("Authorization","Bearer "+accessToken);
	}

}
