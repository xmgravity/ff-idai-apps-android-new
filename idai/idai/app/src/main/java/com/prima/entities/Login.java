package com.prima.entities;

/**
 * Created by Codelabs on 13/10/2015.
 */
public class Login extends Message {
    public ObjectLogin getObject() {
        return object;
    }

    public void setObject(ObjectLogin object) {
        this.object = object;
    }

    private ObjectLogin object;

    public class ObjectLogin {
        private String token;
        private UserLogin user;
        private String url;


        public String getToken() {
            return token;
        }

        public void setToken(String token) {
            this.token = token;
        }

        public UserLogin getUser() {
            return user;
        }

        public void setUser(UserLogin user) {
            this.user = user;
        }

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }
    }

    public class UserLogin {
        private String fullname;
        private String email;
        private String photo;
        private String id;

        public String getPhoto() {
            return photo;
        }

        public void setPhoto(String photo) {
            this.photo = photo;
        }

        public String getFullname() {
            return fullname;
        }

        public void setFullname(String fullname) {
            this.fullname = fullname;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }
    }
}
