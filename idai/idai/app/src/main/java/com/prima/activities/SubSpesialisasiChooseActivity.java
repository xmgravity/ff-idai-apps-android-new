package com.prima.activities;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.JsonSyntaxException;
import com.prima.Preferences;
import com.prima.adapters.ListSubSpesialisasiAdapter;
import com.prima.controllers.ApiReferences;
import com.prima.controllers.Parser;
import com.prima.entities.Message;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import prima.test.prima.R;

/**
 * Created by Codelabs on 19/08/2015.
 */
public class SubSpesialisasiChooseActivity extends BaseActivity implements View.OnClickListener {

    TextView title;
    ImageView close, back;
    ArrayList<String> listItem = new ArrayList<String>();
    ListView list;
    Context mContext;
    Button ok;
    String selected = "";
    ListSubSpesialisasiAdapter adapterList;
    int pos;
    MaterialDialog dialog;
    private RequestQueue mRequest;

    @Override
    public void initView() {
        mRequest = Volley.newRequestQueue(context);
        title = (TextView) findViewById(R.id.tv_actionbar_title);
        close = (ImageView) findViewById(R.id.close);
        back = (ImageView) findViewById(R.id.back);
        list = (ListView) findViewById(R.id.listView);
        close.setVisibility(View.GONE);
        ok = (Button) findViewById(R.id.ok);
        mContext = getApplicationContext();
        //set font
        Typeface tf = Typeface.createFromAsset(getApplicationContext().getAssets(), "BREVIA-BLACK.OTF");
        title.setTypeface(tf);
        title.setText(getResources().getString(R.string.SubSpesialisasi3));

        listItem.add("Alergi Imunologi");
        listItem.add("Emergensi dan Rawat Intensif Anak (ERIA)");
        listItem.add("Endokrinologi");
        listItem.add("Gastrohepatologi");
        listItem.add("Hermatologi - Onkologi");
        listItem.add("Infeksi dan Penyakit Tropis");
        listItem.add("Kardiologi");
        listItem.add("Nefrologi");
        listItem.add("Neonatologi");
        listItem.add("Neurologi");
        listItem.add("Nutrisi dan Penyakit Metabolik");
        listItem.add("Pencitraan");
        listItem.add("Respirologi");
        listItem.add("Tumbuh Kembang Ped.Sos");
        listItem.add("Umum");

        selected = subSpesialisasi(preferences.getString(Preferences.SUBSPESIALISASI, ""));


        adapterList = new ListSubSpesialisasiAdapter(
                listItem, mContext, selected);
        list.setAdapter(adapterList);


        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                selected = listItem.get(position);
                adapterList.setSelected(selected);
                adapterList.notifyDataSetChanged();
                pos = position;
            }
        });


    }

    @Override
    public void setUICallbacks() {
        back.setOnClickListener(this);
        ok.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        if (v == back) {
            Intent i = new Intent(getApplicationContext(), ProfileActivity.class);
            startActivity(i);
            finish();
        } else if (v == ok) {
            if (selected.equals("")) {
                dialog(getResources().getString(R.string.PilihSebelumMenyimpan), getResources().getString(R.string.Perhatian));
            } else {
                save(preferences.getString(Preferences.ID_USER, ""), pos);
            }
        }
    }


    @Override
    public int getLayout() {
        return R.layout.activity_subspesialisasi;
    }

    @Override
    public void updateUI() {

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

    }


    @Override
    public void onBackPressed() {
        Intent i = new Intent(getApplicationContext(), ProfileActivity.class);
        startActivity(i);
        finish();
    }

    private void save(final String id, final int subspesialisasi) {
        progresDialog(false, getResources().getString(R.string.Menyimpan));
        StringRequest listdata = new StringRequest(Request.Method.POST,
                ApiReferences.updateProfile(id),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        dialog.dismiss();
                        try {
                            Log.i("RESULT JSON", response.toString());
                            Message login = Parser.getMessage(response.toString());
                            if (login.getMessages().equals("Success")) {
                                editor.putString(Preferences.SUBSPESIALISASI, String.valueOf(pos));
                                editor.commit();
                                finish();
                            }
                        } catch (JsonSyntaxException e) {
                            dialog(getResources().getString(R.string.KesalahanServer), getResources().getString(R.string.Perhatian));
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dialog.dismiss();
                dialog(error.toString(), getResources().getString(R.string.Perhatian));
            }
        }) {
            protected Map<String, String> getParams() {
                HashMap<String, String> params = new HashMap<String, String>();
                params.put("subspesialisasi", String.valueOf(subspesialisasi));
                return params;
            }
        };
        listdata.setRetryPolicy(new DefaultRetryPolicy(
                20000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        mRequest.add(listdata);
    }


    private void progresDialog(boolean horizontal, String msg) {
        dialog = new MaterialDialog.Builder(this)
                .title(msg)
                .content(getResources().getString(R.string.MohonTunggu))
                .progress(true, 0)
                .progressIndeterminateStyle(horizontal)
                .cancelable(false)
                .show();
    }


    public void dialog(String content, String msg) {
        new MaterialDialog.Builder(this)
                .title(msg)
                .content(content)
                .positiveText("Ok")
                .callback(new MaterialDialog.ButtonCallback() {
                    @Override
                    public void onPositive(MaterialDialog dialog) {
                    }

                })
                .show();
    }

    public String subSpesialisasi(String id) {
        if (id.equals("0")) {
            return "Alergi Imunologi";
        } else if (id.equals("1")) {
            return "Emergensi dan Rawat Intensif Anak (ERIA)";
        } else if (id.equals("2")) {
            return "Endokrinologi";
        } else if (id.equals("3")) {
            return "Gastrohepatologi";
        } else if (id.equals("4")) {
            return "Hermatologi - Onkologi";
        } else if (id.equals("5")) {
            return "Infeksi dan Penyakit Tropis";
        } else if (id.equals("6")) {
            return "Kardiologi";
        } else if (id.equals("7")) {
            return "Nefrologi";
        } else if (id.equals("8")) {
            return "Neonatologi";
        } else if (id.equals("9")) {
            return "Neurologi";
        } else if (id.equals("10")) {
            return "Nutrisi dan Penyakit Metabolik";
        } else if (id.equals("11")) {
            return "Pencitraan";
        } else if (id.equals("12")) {
            return "Respirologi";
        } else if (id.equals("13")) {
            return "Tumbuh Kembang Ped.Sos";
        } else if (id.equals("14")) {
            return "Umum";
        } else {
            return "";
        }
    }

}
