package com.prima.activities;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.prima.MainActivity;
import com.prima.Preferences;
import com.prima.controllers.ApiReferences;
import com.prima.controllers.Parser;
import com.prima.entities.MenuAge;
import com.prima.entities.Message;
import com.prima.entities.Pasien;
import com.prima.entities.Question;
import com.prima.helpers.DatabaseHandler;
import com.prima.helpers.DateHelper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import prima.test.prima.R;

/**
 * Created by Codelabs on 18/08/2015.
 */
public class KpspActivity extends BaseActivity implements View.OnClickListener {
    private Button source;
    private TextView txtSource, title;
    private LinearLayout box;
    private ImageView close;
    private ImageView flag_decor;
    private Button lanjut;
    private TextView content;
    RelativeLayout source_wraper;
    MaterialDialog dialog;
    Question question;
    private RequestQueue mRequest;
    DateHelper dateHelper;
    DatabaseHandler db;

    @Override
    public void initView() {
        db = new DatabaseHandler(context);
        dateHelper = new DateHelper();
        mRequest = Volley.newRequestQueue(getApplicationContext());
        content = (TextView) findViewById(R.id.content);
        source_wraper = (RelativeLayout) findViewById(R.id.source_wraper);
        source = (Button) findViewById(R.id.btn_source);
        txtSource = (TextView) findViewById(R.id.source);
        box = (LinearLayout) findViewById(R.id.box);
        close = (ImageView) findViewById(R.id.close);
        flag_decor = (ImageView) findViewById(R.id.flag_decor);
        lanjut = (Button) findViewById(R.id.lanjut);
        txtSource.setVisibility(View.GONE);

        title = (TextView) findViewById(R.id.title);
        //set font
        Typeface tf = Typeface.createFromAsset(getApplicationContext().getAssets(), "Brevia-ExtraBlackItalic.otf");
        title.setTypeface(tf);

    }

    @Override
    public void setUICallbacks() {
        source.setOnClickListener(this);
        lanjut.setOnClickListener(this);
        close.setOnClickListener(this);

    }


    @Override
    public void onClick(View v) {
        if (v == source) {
            if (txtSource.getVisibility() == View.GONE) {
                txtSource.setVisibility(View.VISIBLE);
            } else {
                txtSource.setVisibility(View.GONE);
            }
        } else if (v == lanjut) {
            if (preferences.getString(Preferences.ID, "").equalsIgnoreCase("") || preferences.getString(Preferences.JENIS_KELAMIN, "").equalsIgnoreCase("")) {
//                // get prompts.xml view
//                LayoutInflater li = LayoutInflater.from(getApplicationContext());
//                View dialogView = li.inflate(R.layout.dialog_sesi_pasien_baru, null);
//                final Dialog dialog3;
//                dialog3 = new Dialog(context);
//                dialog3.requestWindowFeature(Window.FEATURE_NO_TITLE);
//                dialog3.setContentView(dialogView);
//                //make transparent dialog_sesi_offline_pelod
//                dialog3.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
//                dialog3.show();
//
//
//                Button dialogLanjut = (Button) dialogView.findViewById(R.id.ok);
//                final RadioButton rad1 = (RadioButton) dialogView.findViewById(R.id.rad1);
//                final RadioButton rad2 = (RadioButton) dialogView.findViewById(R.id.rad2);
//                rad1.setChecked(true);
//
//                rad1.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        rad1.setChecked(true);
//                        rad2.setChecked(false);
//                    }
//                });
//
//                rad2.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        rad2.setChecked(true);
//                        rad1.setChecked(false);
//                    }
//                });
//
//                final EditText edt = (EditText) dialogView.findViewById(R.id.edt1);
//                final EditText edt2 = (EditText) dialogView.findViewById(R.id.edt2);
//                final EditText edt3 = (EditText) dialogView.findViewById(R.id.edt3);
//
//
//                edt.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        showDatepickerDialog(edt);
//                    }
//                });
//                dialogLanjut.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        if (edt.getText().toString().equals("") ||
//                                edt2.getText().toString().equals("") ||
//                                edt3.getText().toString().equals("")) {
//                            dialog2(getResources().getString(R.string.IsiSeluruhIsian), getResources().getString(R.string.Perhatian));
//                        } else {
//                            String jk;
//                            if (rad1.isChecked()) {
//                                jk = "1";
//                            } else if (rad2.isChecked()) {
//                                jk = "0";
//                            } else {
//                                jk = "";
//                            }
//                            saveSesiPasien(jk, edt.getText().toString(), edt3.getText().toString(), edt2.getText().toString());
//                            dialog3.dismiss();
//                        }
//                    }
//                });

                getMenuAge("KPSP");
            } else {
                getQuestion();
            }
        } else if (v == close) {
            Intent i = new Intent(getApplicationContext(), MainActivity.class);
            startActivity(i);
            finish();
        }
    }


    private void getMenuAge(final String code) {
        progresDialog(false, getResources().getString(R.string.MemuatData));
        StringRequest listdata = new StringRequest(Request.Method.GET,
                ApiReferences.getMenuAge(code,preferences.getString(Preferences.LANGUAGE, "")),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
            dialog.dismiss();
                        try {
                        if (response.equals("[]")) {
                            dialog(getResources().getString(R.string.BelumAdaData), getResources().getString(R.string.Pesan));
                        } else {
                            Message msg = Parser.getMessage(response);
                            if (msg.getStatus().equals("1")) {
                                Log.i("RESULT JSON", response.toString());

                                MenuAge menu = Parser.getMenuAge(response);

                                ArrayList<MenuAge.Menu> list = db.getMenuAge(code);
                                if (list.size() > 0) {
                                    db.deleteMenuAge(code);
                                    for (int i = 0; i < menu.getObjects().size(); i++) {
                                        db.insertMenuAge(menu.getObjects().get(i));
                                    }
                                } else {
                                    for (int i = 0; i < menu.getObjects().size(); i++) {
                                        db.insertMenuAge(menu.getObjects().get(i));
                                    }
                                }

                                Intent i = new Intent(getApplicationContext(), KpspUmurActivity.class);
                                i.putExtra("respon", response);
                                startActivity(i);

                            } else {
                                dialog(msg.getMessages(), getResources().getString(R.string.Pesan));
                            }
                        }
                        } catch (JsonSyntaxException e) {
                            dialog(getResources().getString(R.string.KesalahanServer), getResources().getString(R.string.Perhatian));
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dialog.dismiss();
                ArrayList<MenuAge.Menu> list = db.getMenuAge(code);
                if (list.size() > 0) {
                    MenuAge menu = new MenuAge();
                    menu.setObjects(list);
                   // db.deleteMenuAge(code);
                    Gson gs = new Gson();
                    Intent i = new Intent(getApplicationContext(), KpspUmurActivity.class);
                    i.putExtra("respon", gs.toJson(menu));
                    startActivity(i);
                } else {
                    dialog(getResources().getString(R.string.NoConnection), getResources().getString(R.string.Perhatian));
                }

            }
        });
        listdata.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        mRequest.add(listdata);
    }


    @Override
    public int getLayout() {
        return R.layout.kpsp_single_page;
    }


    private void getQuestion() {
        final String code = "KPSP";
        progresDialog(false, getResources().getString(R.string.MemuatData));
        StringRequest listdata = new StringRequest(Request.Method.GET,
                ApiReferences.getQuestion(preferences.getString(Preferences.ID, ""), code, preferences.getString(Preferences.LANGUAGE, "")),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        if (response.equals("[]")) {
                            dialog(getResources().getString(R.string.BelumAdaData), getResources().getString(R.string.Pesan));
                        } else {
                            Message msg = Parser.getMessage(response);
                            if (msg.getStatus().equals("1")) {
                                Log.i("RESULT JSON", response.toString());
                                question = new Question();
                                question = Parser.getQuestion(response);


                                for (int j = 0; j < question.getObjects().size(); j++) {
                                    db.deleteQuestion(question.getObjects().get(j).getId(), question.getObjects().get(j).getMin_age(), question.getObjects().get(j).getMax_age(), "");
                                    db.deleteAnswer(question.getObjects().get(j).getId());
                                    db.insertQuestion(question.getObjects().get(j), code);
                                    for (int k = 0; k < question.getObjects().get(j).getAnswer_option().size(); k++) {
                                        db.insertAnswer(question.getObjects().get(j).getAnswer_option().get(k));
                                    }
                                }

                                Intent i = new Intent(getApplicationContext(), KpspQuestionActivity.class);
                                i.putExtra("respon", response.toString());
                                startActivity(i);
                            } else {
                                dialog(msg.getMessages(), getResources().getString(R.string.Pesan));
                            }
                            //  finish();
                        }
                        dialog.dismiss();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dialog.dismiss();
                Question temp = db.getQuestion(code, getMonth());
                Gson gson = new Gson();
                Log.i("RESULT JSON2", gson.toJson(temp));
                if (temp.getObjects().size() > 0) {
                    Intent i = new Intent(getApplicationContext(), KpspQuestionActivity.class);
                    i.putExtra("respon", gson.toJson(temp));
                    startActivity(i);
                } else {
                    dialog(getResources().getString(R.string.NoConnection), getResources().getString(R.string.Perhatian));
                }
            }
        });
        listdata.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        mRequest.add(listdata);
    }


    @Override
    public void updateUI() {
//        Bundle bn = getIntent().getExtras();
//        if (bn != null) {
//            String result = bn.getString("result");
//            Artikel artikel = Parser.getArtikel(result);
//            content.setText(artikel.getObjects().getDeskripsi());
//            Spannable s = (Spannable) Html.fromHtml("<a href=\"" + artikel.getObjects().getSumber() + "\">" + artikel.getObjects().getSumber() + "</a>");
//            URLSpan[] spans = s.getSpans(0, s.length(), URLSpan.class);
//            for (URLSpan span : spans) {
//                int start = s.getSpanStart(span);
//                int end = s.getSpanEnd(span);
//                s.removeSpan(span);
//                span = new URLSpan(span.getURL());
//                s.setSpan(span, start, end, 0);
//            }
//            txtSource.setText(s);
//            txtSource.setMovementMethod(LinkMovementMethod.getInstance());
//
//            if (artikel.getObjects().getSumber().trim().equals("")) {
//                txtSource.setVisibility(View.GONE);
//                source_wraper.setVisibility(View.GONE);
//                source.setVisibility(View.GONE);
//
//            }
//
//        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

    }


    private void saveSesiPasien(final String jenis_kelamin, final String tanggal_lahir, final String berat, final String tinggi) {
        progresDialog(false, getResources().getString(R.string.MenyimpanDataPasien));
        StringRequest postlogin = new StringRequest(Request.Method.POST,
                ApiReferences.insertPasien(), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                Log.i("RESULT JSON", response.toString());
                Pasien pasien = Parser.getPasien(response
                        .toString());
                if (pasien.getStatus().equalsIgnoreCase("1")) {
                    editor.putString(Preferences.ID, pasien.getObjects().getId());
                    editor.putString(Preferences.BERAT, pasien.getObjects().getBerat());
                    editor.putString(Preferences.TINGGI, pasien.getObjects().getTinggi());
                    editor.putString(Preferences.JENIS_KELAMIN, pasien.getObjects().getJenis_kelamin());
                    editor.putString(Preferences.BIRTH_DAY, pasien.getObjects().getTanggal_lahir());
                    editor.commit();
                    dialog.dismiss();
                    dialogEmail("Kode : " + pasien.getObjects().getId() + "\nSimpan Code ini untuk pemeriksaan berikutnya.", "Pendaftaran Sesi Pasien Berhasil", pasien.getObjects().getId());
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                dialog.dismiss();
                dialog(getResources().getString(R.string.NoConnection), getResources().getString(R.string.Perhatian));
            }
        }) {

            protected Map<String, String> getParams() {
                HashMap<String, String> params = new HashMap<String, String>();
                params.put("jenis_kelamin", jenis_kelamin);
                params.put("tanggal_lahir", tanggal_lahir);
                params.put("berat", berat);
                params.put("tinggi", tinggi);
                return params;
            }
        };
        postlogin.setRetryPolicy(new DefaultRetryPolicy(
                20000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        mRequest.add(postlogin);
    }


    public void dialogEmail(String content, String msg, final String kode) {
        new MaterialDialog.Builder(this)
                .title(msg)
                .content(content)
                .autoDismiss(false)
                .cancelable(false)
                .positiveText(getResources().getString(R.string.KirimEmail))
                .negativeText(getResources().getString(R.string.tutup))
                .callback(new MaterialDialog.ButtonCallback() {
                    @Override
                    public void onPositive(MaterialDialog dialog) {
                        Intent sendEmail = new Intent(Intent.ACTION_SEND);
                        sendEmail.setType("text/plain");
                        sendEmail.putExtra(Intent.EXTRA_EMAIL, new String[]{""});
                        sendEmail.putExtra(Intent.EXTRA_SUBJECT, "Kode Pasien");
                        sendEmail.putExtra(Intent.EXTRA_TEXT, "Berikut ini adalah kode pasien anda.\nKode : " + kode);
                        startActivity(Intent.createChooser(sendEmail, "Email With"));
                    }

                    @Override
                    public void onNegative(MaterialDialog dialog) {
                        dialog.dismiss();
                        getQuestion();
                    }
                })
                .show();
    }


    private void showDatepickerDialog(final EditText txt) {
        dateHelper.showDatePicker(this, System.currentTimeMillis(), new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                int _year = year;
                int _month = monthOfYear + 1;
                int _day = dayOfMonth;
                String date = _year + "-" + _month + "-" + _day;
                String birthDate = dateHelper.formatDate(date);
                txt.setText(birthDate);
            }
        });
    }

    @Override
    public void onBackPressed() {
        Intent i = new Intent(getApplicationContext(), MainActivity.class);
        startActivity(i);
        finish();
    }

    private void progresDialog(boolean horizontal, String msg) {
        dialog = new MaterialDialog.Builder(this)
                .title(msg)
                .content(getResources().getString(R.string.MohonTunggu))
                .progress(true, 0)
                .progressIndeterminateStyle(horizontal)
                .cancelable(false)
                .show();
    }

    public void dialog(String content, String msg) {
        new MaterialDialog.Builder(this)
                .title(msg)
                .content(content)
                .positiveText(getResources().getString(R.string.MuatUlang))
                .negativeText(getResources().getString(R.string.Kembali))
                .callback(new MaterialDialog.ButtonCallback() {
                    @Override
                    public void onPositive(MaterialDialog dialog) {
                        getQuestion();
                    }

                    @Override
                    public void onNegative(MaterialDialog dialog) {
                        super.onNegative(dialog);
                    }

                })
                .show();
    }


    public void dialog2(String content, String msg) {
        new MaterialDialog.Builder(this)
                .title(msg)
                .content(content)
                .negativeText("OK")
                .callback(new MaterialDialog.ButtonCallback() {
                    @Override
                    public void onPositive(MaterialDialog dialog) {

                    }

                    @Override
                    public void onNegative(MaterialDialog dialog) {
                        super.onNegative(dialog);
                    }

                })
                .show();
    }
//
//    private static Button ok;
//    private Context mContext;
//    private static ViewPager mPager;
//    private PagerAdapter mPagerAdapter;
//    private static int currentPage;
//    PageListener pageListener;
//    static TextView title;
//    List<Fragment> fragmentList = new ArrayList<Fragment>();
//    static LinearLayout indikatorWrapper;
//    ImageView close;
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        mContext = getApplicationContext();
//        fragmentList.add(new KpspFragment());
//        fragmentList.add(new KpspFragment());
//        fragmentList.add(new KpspFragment());
//        fragmentList.add(new KpspFragment());
//        setupFrae();
//
//    }
//
//    @Override
//    public void initView() {
//        indikatorWrapper = (LinearLayout) findViewById(R.id.indikator_pager);
//        close = (ImageView) findViewById(R.id.close);
//        ok = (Button) findViewById(R.id.ok);
//        ok.setVisibility(View.GONE);
//        mPager = (ViewPager) findViewById(R.id.pager);
//        pageListener = new PageListener();
//        mPager.setOnPageChangeListener(pageListener);
//    }
//
//    @Override
//    public void setUICallbacks() {
//        ok.setOnClickListener(this);
//        close.setOnClickListener(this);
//    }
//
//
//    @Override
//    public void onClick(View v) {
//        if (v == ok) {
//            Intent i = new Intent(getApplicationContext(), KpspUmurActivity.class);
//            startActivity(i);
//            finish();
//        } else if (v==close){
//            Intent i = new Intent(getApplicationContext(), MainActivity.class);
//            startActivity(i);
//            finish();
//        }
//    }
//
//
//    @Override
//    public int getLayout() {
//        return R.layout.activity_kpsp;
//    }
//
//    @Override
//    public void updateUI() {
//        indikatorWrapper.removeAllViewsInLayout();
//        fragmentList.clear();
//        //  Toast.makeText(getApplicationContext(), String.valueOf(data.size()), Toast.LENGTH_LONG).show();
//        for (int i = 0; i < 4; i++) {
//            fragmentList.add(new KpspFragment());
//            View child = getLayoutInflater().inflate(R.layout.indikator_pager, null);
//            View child2 = getLayoutInflater().inflate(R.layout.indikator_pager_blue, null);
//            if (i == 0) {
//                indikatorWrapper.addView(child2);
//            } else {
//                indikatorWrapper.addView(child);
//            }
//        }
//        setupFrae();
//    }
//
//    @Override
//    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//
//    }
//
//
//    @Override
//    public void onBackPressed() {
//        if (currentPage > 0) {
//            currentPage--;
//            mPager.setCurrentItem(currentPage, true);
//        } else {
//            Intent i = new Intent(getApplicationContext(), MainActivity.class);
//            startActivity(i);
//            finish();
//        }
//    }
//
//    private class PageListener extends ViewPager.SimpleOnPageChangeListener {
//        public void onPageSelected(int position) {
//            currentPage = position;
//
//            currentPage = position;
//            indikatorWrapper.removeAllViewsInLayout();
//            for (int i = 0; i < 4; i++) {
//                View child = getLayoutInflater().inflate(R.layout.indikator_pager, null);
//                View child2 = getLayoutInflater().inflate(R.layout.indikator_pager_blue, null);
//                if (i == position) {
//                    indikatorWrapper.addView(child2);
//                } else {
//                    indikatorWrapper.addView(child);
//                }
//            }
//            if (currentPage == 3) {
//                ok.setVisibility(View.VISIBLE);
//            } else {
//                ok.setVisibility(View.GONE);
//            }
//        }
//    }
//
//
//    private void setupFrae() {
//        mPagerAdapter = new PagerAdapter(getSupportFragmentManager(),
//                KpspActivity.this, fragmentList);
//        mPager.setAdapter(mPagerAdapter);
//
//    }

}
