package com.prima.activities;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.davemorrissey.labs.subscaleview.ImageSource;
import com.davemorrissey.labs.subscaleview.SubsamplingScaleImageView;

import com.prima.Preferences;
import com.prima.controllers.ApiReferences;
import prima.test.prima.R;

/**
 * Created by Codelabs on 31/08/2015.
 */
public class ImagePreviewActivity extends BaseActivity implements View.OnClickListener {
    private SubsamplingScaleImageView img;
    private ImageView close;


    @Override
    public void initView() {
        img = (SubsamplingScaleImageView) findViewById(R.id.img);
        close = (ImageView) findViewById(R.id.close);
        // Asynchronously load the image and set the thumbnail and pager view
        Bundle bn = getIntent().getExtras();
        if (bn != null) {
            String url = bn.getString("url");
            String image = bn.getString("image");
            if (url == null) {
                System.out.println("gender:"+ preferences.getString(Preferences.JENIS_KELAMIN, ""));


                if (image.equalsIgnoreCase("WHO")) {
                    if (preferences.getString(Preferences.JENIS_KELAMIN, "").equals("m")) {
                        Glide.with(getApplicationContext())
                                .load(R.drawable.who_laki)
                                .asBitmap()
                                .override(1000,700)
                                .into(new SimpleTarget<Bitmap>() {
                                    @Override
                                    public void onResourceReady(Bitmap bitmap, GlideAnimation anim) {
                                        img.setImage(ImageSource.bitmap(bitmap));
                                    }
                                });
                    } else {
                        Glide.with(getApplicationContext())
                                .load(R.drawable.who_perempuan)
                                .asBitmap()
                                .override(1000, 700)
                                .into(new SimpleTarget<Bitmap>() {
                                    @Override
                                    public void onResourceReady(Bitmap bitmap, GlideAnimation anim) {
                                        img.setImage(ImageSource.bitmap(bitmap));
                                    }
                                });
                    }
                } else if (image.equals("CDC")) {
                    if (preferences.getString(Preferences.JENIS_KELAMIN, "").equals("m")) {
                        Glide.with(getApplicationContext())
                                .load(R.drawable.cdc_laki)
                                .asBitmap()
                                .override(1000, 700)
                                .into(new SimpleTarget<Bitmap>() {
                                    @Override
                                    public void onResourceReady(Bitmap bitmap, GlideAnimation anim) {
                                        img.setImage(ImageSource.bitmap(bitmap));
                                    }
                                });
                    } else {
                        Glide.with(getApplicationContext())
                                .load(R.drawable.cdc_perempuan)
                                .asBitmap()
                                .override(1000,700)
                                .into(new SimpleTarget<Bitmap>() {
                                    @Override
                                    public void onResourceReady(Bitmap bitmap, GlideAnimation anim) {
                                        img.setImage(ImageSource.bitmap(bitmap));
                                    }
                                });
                    }
                } else if (image.equals("Panjang/Tinggi Terhadap Umur")) {
                    if (preferences.getString(Preferences.JENIS_KELAMIN, "").equals("m")) {
                        Glide.with(getApplicationContext())
                                .load(R.drawable.cdc_tb_u_l)
                                .asBitmap()
                                .override(1000, 700)
                                .into(new SimpleTarget<Bitmap>() {
                                    @Override
                                    public void onResourceReady(Bitmap bitmap, GlideAnimation anim) {
                                        img.setImage(ImageSource.bitmap(bitmap));
                                    }
                                });
                    } else {
                        Glide.with(getApplicationContext())
                                .load(R.drawable.cdc_tb_u_p)
                                .asBitmap()
                                .override(1000,700)
                                .into(new SimpleTarget<Bitmap>() {
                                    @Override
                                    public void onResourceReady(Bitmap bitmap, GlideAnimation anim) {
                                        img.setImage(ImageSource.bitmap(bitmap));
                                    }
                                });
                    }
                } else if (image.equals("Berat Terhadap Umur")) {
                    if (preferences.getString(Preferences.JENIS_KELAMIN, "").equals("m")) {
                        Glide.with(getApplicationContext())
                                .load(R.drawable.cdc_bb_u_l)
                                .asBitmap()
                                .override(1000, 700)
                                .into(new SimpleTarget<Bitmap>() {
                                    @Override
                                    public void onResourceReady(Bitmap bitmap, GlideAnimation anim) {
                                        img.setImage(ImageSource.bitmap(bitmap));
                                    }
                                });
                    } else {
                        Glide.with(getApplicationContext())
                                .load(R.drawable.cdc_bb_u_p)
                                .asBitmap()
                                .override(1000,700)
                                .into(new SimpleTarget<Bitmap>() {
                                    @Override
                                    public void onResourceReady(Bitmap bitmap, GlideAnimation anim) {
                                        img.setImage(ImageSource.bitmap(bitmap));
                                    }
                                });
                    }
                } else if (image.equals("Berat Terhadap Panjang/Tinggi")) {
                    if (preferences.getString(Preferences.JENIS_KELAMIN, "").equals("m")) {
                        Glide.with(getApplicationContext())
                                .load(R.drawable.cdc_bb_tb_l)
                                .asBitmap()
                                .override(1000, 700)
                                .into(new SimpleTarget<Bitmap>() {
                                    @Override
                                    public void onResourceReady(Bitmap bitmap, GlideAnimation anim) {
                                        img.setImage(ImageSource.bitmap(bitmap));
                                    }
                                });
                    } else {
                        Glide.with(getApplicationContext())
                                .load(R.drawable.cdc_bb_tb_p)
                                .asBitmap()
                                .override(1000,700)
                                .into(new SimpleTarget<Bitmap>() {
                                    @Override
                                    public void onResourceReady(Bitmap bitmap, GlideAnimation anim) {
                                        img.setImage(ImageSource.bitmap(bitmap));
                                    }
                                });
                    }
                } else if (image.equals("Indeks Masa Tubuh Terhadap Umur")) {
                    if (preferences.getString(Preferences.JENIS_KELAMIN, "").equals("m")) {
                        Glide.with(getApplicationContext())
                                .load(R.drawable.cdc_imt_u_l)
                                .asBitmap()
                                .override(1000, 700)
                                .into(new SimpleTarget<Bitmap>() {
                                    @Override
                                    public void onResourceReady(Bitmap bitmap, GlideAnimation anim) {
                                        img.setImage(ImageSource.bitmap(bitmap));
                                    }
                                });
                    } else {
                        Glide.with(getApplicationContext())
                                .load(R.drawable.cdc_imt_u_p)
                                .asBitmap()
                                .override(1000,700)
                                .into(new SimpleTarget<Bitmap>() {
                                    @Override
                                    public void onResourceReady(Bitmap bitmap, GlideAnimation anim) {
                                        img.setImage(ImageSource.bitmap(bitmap));
                                    }
                                });
                    }
                }

            } else {

                Glide.with(getApplicationContext())
                        .load(ApiReferences.getImageUrl() + url)
                        .asBitmap()
                        .into(new SimpleTarget<Bitmap>() {
                            @Override
                            public void onResourceReady(Bitmap bitmap, GlideAnimation anim) {
                                img.setImage(ImageSource.bitmap(bitmap));
                            }
                        });
            }
        }
    }


    @Override
    public void setUICallbacks() {
        close.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if (v == close) {
            finish();
        }
    }


    @Override
    public int getLayout() {
        return R.layout.activity_image_preview;
    }

    @Override
    public void updateUI() {

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

    }


    @Override
    public void onBackPressed() {
        finish();
    }


}
