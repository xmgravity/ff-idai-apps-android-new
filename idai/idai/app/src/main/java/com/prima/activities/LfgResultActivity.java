package com.prima.activities;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.prima.Preferences;

import org.joda.time.Days;
import org.joda.time.LocalDate;
import org.joda.time.LocalDateTime;
import org.joda.time.Months;
import org.joda.time.Period;
import org.joda.time.PeriodType;
import org.joda.time.Weeks;
import org.joda.time.Years;

import java.util.Calendar;

import prima.test.prima.R;

/**
 * Created by Codelabs on 10/08/2015.
 */
public class LfgResultActivity extends BaseActivity implements View.OnClickListener {

    Button ok;
    TextView title, usia, hasil_hitung;
    LinearLayout result1,
            result2,
            result3,
            result4,
            result5,
            result6,
            result7,
            result8,
            result9;

    @Override
    public void initView() {
        result1 = (LinearLayout) findViewById(R.id.result1);
        result2 = (LinearLayout) findViewById(R.id.result2);
        result3 = (LinearLayout) findViewById(R.id.result3);
        result4 = (LinearLayout) findViewById(R.id.result4);
        result5 = (LinearLayout) findViewById(R.id.result5);
        result6 = (LinearLayout) findViewById(R.id.result6);
        result7 = (LinearLayout) findViewById(R.id.result7);
        result8 = (LinearLayout) findViewById(R.id.result8);
        result9 = (LinearLayout) findViewById(R.id.result9);
        ok = (Button) findViewById(R.id.ok);
        title = (TextView) findViewById(R.id.title);
        usia = (TextView) findViewById(R.id.usia);
        hasil_hitung = (TextView) findViewById(R.id.hasil_hitung);


        //set font
        Typeface tf = Typeface.createFromAsset(getApplicationContext().getAssets(), "BREVIA-BLACK.OTF");
        title.setTypeface(tf);

        Typeface tfAriali = Typeface.createFromAsset(getApplicationContext().getAssets(), "ariali.ttf");
        Typeface tfArialb = Typeface.createFromAsset(getApplicationContext().getAssets(), "arialbd.ttf");

        TextView trademark = (TextView) findViewById(R.id.trademark);
        TextView trademark2 = (TextView) findViewById(R.id.trademark2);
        TextView trademark3 = (TextView) findViewById(R.id.trademark3);
        trademark.setTypeface(tfAriali);
        trademark2.setTypeface(tfArialb);
        trademark3.setTypeface(tfAriali);

        if(preferences.getString(Preferences.TIPE_USER, "").equalsIgnoreCase("awam")){
            trademark.setVisibility(View.GONE);
            trademark2.setVisibility(View.GONE);
            trademark3.setVisibility(View.GONE);
        }
    }

    @Override
    public void setUICallbacks() {
        ok.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        if (v == ok) {
            //   Intent i = new Intent(getApplicationContext(), LfgActivity.class);
            //  startActivity(i);
            if (preferences.getString(Preferences.ID, "").equals("1")) {
                editor.putString(Preferences.BIRTH_DAY, "");
                editor.putString(Preferences.JENIS_KELAMIN, "");
                editor.putString(Preferences.BERAT, "");
                editor.putString(Preferences.TINGGI, "");
                editor.putString(Preferences.ID, "");
                editor.commit();
            }
            finish();
        }
    }


    @Override
    public int getLayout() {
        return R.layout.activity_result_lfg;
    }

    @Override
    public void updateUI() {
        Bundle bn = getIntent().getExtras();
        if (bn != null) {
            float hasil = bn.getFloat("hasil");
            Log.i("TESTING HASIL", String.valueOf(hasil));
            Log.i("MY BIRTHDAY (Year)", String.valueOf(getAge()));
            Log.i("MY BIRTHDAY (day)", String.valueOf(getDays()));
            Log.i("MY BIRTHDAY (Month)", String.valueOf(getMonth()));
            int month = getMonth() - (getAge() * 12);
            LocalDateTime now = LocalDateTime.now();
            int year = now.getYear();
            Calendar calendar = Calendar.getInstance();
            int totalDays = 0;
            for (int i = 1; i <= month; i++) {
                calendar.set(Calendar.YEAR, year);
                calendar.set(Calendar.MONTH, i);
                int numDays = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
                totalDays = totalDays + numDays;
            }


            LocalDate birthdate = new LocalDate(preferences.getString(Preferences.BIRTH_DAY, ""));          //Birth date
            LocalDate today = new LocalDate();                    //Today's date
            Period period = new Period(birthdate, today, PeriodType.yearMonthDay());


            String temp = "";
            if (period.getYears() > 0) {
                temp = temp + period.getYears() + " " + getResources().getString(R.string.tahun);
            }

            if (period.getWeeks() > 0) {
                temp = temp + period.getWeeks() + " " + getResources().getString(R.string.minggu);
            }

            if (period.getMonths() > 0) {
                temp = temp + period.getMonths() + " " + getResources().getString(R.string.bulan);
            }

            if (period.getDays() > 0) {
                temp = temp + period.getDays() + " " + getResources().getString(R.string.Hari);
            }

            if (temp.equals("")){
                temp = getResources().getString(R.string.Lahir);
            }


            usia.setText(getResources().getString(R.string.UsiaPasien) + temp);
            hasil_hitung.setText(getResources().getString(R.string.HasilLFG) + String.valueOf(String.format("%.02f", hasil)).replace(".",",")+" mg/dL");

            if (getAge() <= 15) {
                if ((getDays() >= 0 && getDays() <= 1) && (hasil >= 20.8 - 1.9 && hasil <= 20.8 + 1.9)) {
                    result1.setBackgroundResource(R.color.orange_selected);
                } else if ((getWeeks() == 1) && (hasil >= 46 - 5.2 && hasil <= 46 + 5.2)) {
                    result2.setBackgroundResource(R.color.orange_selected);
                } else if ((getWeeks() >= 3 && getWeeks() <= 5) && (hasil >= 60.1 - 4.6 && hasil <= 60.1 + 4.6)) {
                    result3.setBackgroundResource(R.color.orange_selected);
                } else if ((getWeeks() >= 6 && getWeeks() <= 9) && (hasil >= 67.5 - 6.5 && hasil <= 67.5 + 6.5)) {
                    result4.setBackgroundResource(R.color.orange_selected);
                } else if ((getMonth() >= 3 && getMonth() <= 6) && (hasil >= 73.8 - 7.2 && hasil <= 73.8 + 7.2)) {
                    result5.setBackgroundResource(R.color.orange_selected);
                } else if ((getMonth() >= 6 && getMonth() <= 12) && (hasil >= 93.7 - 14.0 && hasil <= 93.7 + 14.0)) {
                    result6.setBackgroundResource(R.color.orange_selected);
                } else if ((getAge() >= 1 && getAge() <= 2) && (hasil >= 99.1 - 18.7 && hasil <= 99.1 + 18.7)) {
                    result7.setBackgroundResource(R.color.orange_selected);
                } else if ((getAge() >= 2 && getAge() <= 5) && (hasil >= 126.5 - 24.0 && hasil <= 126.5 + 24.0)) {
                    result8.setBackgroundResource(R.color.orange_selected);
                } else if ((getAge() >= 5 && getAge() <= 15) && (hasil >= 116.7 - 20.2 && hasil <= 1167 + 20.2)) {
                    result9.setBackgroundResource(R.color.orange_selected);
                }
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

    }


    @Override
    public void onBackPressed() {
        //  Intent i = new Intent(getApplicationContext(), LfgActivity.class);
        //  startActivity(i);
        if (preferences.getString(Preferences.ID, "").equals("1")) {
            editor.putString(Preferences.BIRTH_DAY, "");
            editor.putString(Preferences.JENIS_KELAMIN, "");
            editor.putString(Preferences.BERAT, "");
            editor.putString(Preferences.TINGGI, "");
            editor.putString(Preferences.ID, "");
            editor.commit();
        }
        finish();
    }


    public int getAge() {
        Years years = Years.yearsBetween(new LocalDate(preferences.getString(Preferences.BIRTH_DAY, "")), new LocalDate());
        return years.getYears();
    }

    public int getDays() {
        Days days = Days.daysBetween(new LocalDate(preferences.getString(Preferences.BIRTH_DAY, "")), new LocalDate());
        return days.getDays();
    }

    public int getMonth() {
        Months month = Months.monthsBetween(new LocalDate(preferences.getString(Preferences.BIRTH_DAY, "")), new LocalDate());
        return month.getMonths();
    }


    public int getWeeks() {
        Weeks week = Weeks.weeksBetween(new LocalDate(preferences.getString(Preferences.BIRTH_DAY, "")), new LocalDate());
        return week.getWeeks();
    }

}
