package com.prima.activities;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.prima.Preferences;
import com.prima.entities.Question;
import com.prima.helpers.DatabaseHandler;

import org.joda.time.LocalDate;
import org.joda.time.Months;
import org.joda.time.Years;

import java.util.ArrayList;

import prima.test.prima.R;

/**
 * Created by Codelabs on 12/08/2015.
 */
public class TandaVitalResultActivity extends BaseActivity implements View.OnClickListener {
    private ImageView close;
    private ImageView back;
    private Button lanjut, table;
    TextView title;
    private ListView list;
    private ArrayList<String> listItem = new ArrayList<String>();
    private ArrayList<Integer> answered = new ArrayList<Integer>();
    MaterialDialog dialog;
    private RequestQueue mRequest;
    Question question;
    DatabaseHandler db;

    EditText lajuNadi, lajuNafas, suhu, tekananDarah;

    private Button startButton;
    private Button pauseButton;
    private Button resetButton;

    private TextView timerSecValueTxt, timerMinValueTxt;

    private long startTime = 0L;

    private Handler customHandler = new Handler();

    long timeInMilliseconds = 0L;
    long timeSwapBuff = 0L;
    long updatedTime = 0L;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }

    @Override
    public void initView() {
        db = new DatabaseHandler(context);
        mRequest = Volley.newRequestQueue(getApplicationContext());
        back = (ImageView) findViewById(R.id.back);
        close = (ImageView) findViewById(R.id.close);
        lanjut = (Button) findViewById(R.id.lanjut);
        table = (Button) findViewById(R.id.table);
        title = (TextView) findViewById(R.id.tv_actionbar_title);
        Typeface tf = Typeface.createFromAsset(getApplicationContext().getAssets(), "BREVIA-BLACK.OTF");
        title.setTypeface(tf);
        title.setText(getResources().getString(R.string.TandaVital));


        lajuNadi = (EditText) findViewById(R.id.lajuNadi);
        lajuNafas = (EditText) findViewById(R.id.lajuNafas);
        suhu = (EditText) findViewById(R.id.suhu);
        tekananDarah = (EditText) findViewById(R.id.tekananDarah);

        timerMinValueTxt = (TextView) findViewById(R.id.timerValueMin);
        timerSecValueTxt = (TextView) findViewById(R.id.timerValueSec);
        //timerMicroSecValueTxt = (TextView) findViewById(R.id.timerMicroSecValue);

        startButton = (Button) findViewById(R.id.startButton);

        startButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                resetButton.setEnabled(false);
                startTime = SystemClock.uptimeMillis();
                customHandler.postDelayed(updateTimerThread, 0);
            }
        });

        pauseButton = (Button) findViewById(R.id.pauseButton);
        resetButton = (Button) findViewById(R.id.resetButton);

        pauseButton.setOnClickListener(new View.OnClickListener() {

            public void onClick(View view) {

                resetButton.setEnabled(true);
                timeSwapBuff += timeInMilliseconds;
                customHandler.removeCallbacks(updateTimerThread);
            }
        });



        resetButton.setOnClickListener(new View.OnClickListener(){
            public void onClick(View view){

                int secs = 00;
                int mins = 00;
                secs = secs % 60;
                timerMinValueTxt.setText(String.format("%02d", mins));
                timerSecValueTxt.setText(String.format("%02d", secs));

                startTime = 0L;
                timeInMilliseconds = 0L;
                timeSwapBuff = 0L;
                updatedTime = 0L;




            }
        });


    }


    private Runnable updateTimerThread = new Runnable() {

        public void run() {

            timeInMilliseconds = SystemClock.uptimeMillis() - startTime;

            updatedTime = timeSwapBuff + timeInMilliseconds;

            int secs = (int) (updatedTime / 1000);
            int mins = secs / 60;
            secs = secs % 60;
            int milliseconds = (int) (updatedTime % 1000);
            timerMinValueTxt.setText(String.format("%02d", mins));
            timerSecValueTxt.setText(String.format("%02d", secs));
            //timerMicroSecValueTxt.setText(String.format("%02d", milliseconds));

            customHandler.postDelayed(this, 0);
        }
    };

    @Override
    public void setUICallbacks() {
        back.setOnClickListener(this);
        close.setOnClickListener(this);
        lanjut.setOnClickListener(this);
        table.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        if (v == back) {
            //   Intent i = new Intent(getApplicationContext(), MilestoneRedflagActivity.class);
            //    startActivity(i);
            if (preferences.getString(Preferences.ID, "").equals("1")) {
                editor.putString(Preferences.BIRTH_DAY, "");
                editor.putString(Preferences.JENIS_KELAMIN, "");
                editor.putString(Preferences.BERAT, "");
                editor.putString(Preferences.TINGGI, "");
                editor.putString(Preferences.ID, "");
                editor.commit();
            }
            finish();
        } else if (v == close) {
            //    Intent i = new Intent(getApplicationContext(), MilestoneRedflagActivity.class);
            //    startActivity(i);
            if (preferences.getString(Preferences.ID, "").equals("1")) {
                editor.putString(Preferences.BIRTH_DAY, "");
                editor.putString(Preferences.JENIS_KELAMIN, "");
                editor.putString(Preferences.BERAT, "");
                editor.putString(Preferences.TINGGI, "");
                editor.putString(Preferences.ID, "");
                editor.commit();
            }
            finish();
        } else if (v == lanjut) {

            LayoutInflater li = LayoutInflater.from(getApplicationContext());
            View dialogView = li.inflate(R.layout.dialog_interpretasi_tanda_vital, null);
            final Dialog dialog3;
            dialog3 = new Dialog(context);
            dialog3.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog3.setContentView(dialogView);
            //make transparent dialog_sesi_offline_pelod
            dialog3.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));


            TextView lajuNadiResult = (TextView) dialogView.findViewById(R.id.lajuNadi);
            TextView lajuNafasResult = (TextView) dialogView.findViewById(R.id.lajuNafas);
            TextView suhuResult = (TextView) dialogView.findViewById(R.id.suhu);
            TextView tekananDarahResult = (TextView) dialogView.findViewById(R.id.tekananDarah);

            Button dialogLanjut = (Button) dialogView.findViewById(R.id.ok);

            /*lajuNadi.getText().toString().equals("0");
            lajuNafas.getText().toString().equals("0");
            suhu.getText().toString().equals("0");*/

            if(!lajuNadi.getText().toString().equals("") ||
                    !lajuNafas.getText().toString().equals("") ||
                    !suhu.getText().toString().equals("")||
                    !tekananDarah.getText().toString().equals("")) {
                lajuNadiResult.setText(getInterpretasiNadi(lajuNadi.getText().toString()));
                lajuNafasResult.setText(getInterpretasiNafas(lajuNafas.getText().toString()));
                suhuResult.setText(getInterpretasiSuhu(suhu.getText().toString()));
                tekananDarahResult.setText(getInterpretasiKelamin(tekananDarah.getText().toString()));

                dialog3.show();
            }
            else{
                progresDialog(false, getResources().getString(R.string.MasukkanHasilPemeriksaan));
            }

            dialogLanjut.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog3.dismiss();
                }
            });
        }else if (v == table) {
            Intent i = new Intent(getApplicationContext(), TandaVitalTableActivity.class);
            startActivity(i);
            finish();
        }
    }


    public String getKelamin(){
        String kelamin = preferences.getString(Preferences.JENIS_KELAMIN,"");
        return kelamin;
    }


    public int getAgeYear() {
        Years years = Years.yearsBetween(new LocalDate(preferences.getString(Preferences.BIRTH_DAY, "")), new LocalDate());
        return years.getYears();
    }

    public int getAgeMonth(){
        Months months = Months.monthsBetween(new LocalDate(preferences.getString(Preferences.BIRTH_DAY, "")), new LocalDate());
        return months.getMonths();
    }

    @Override
    public int getLayout() {
        return R.layout.activity_tanda_vital_question;
    }

    @Override
    public void updateUI() {

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

    }


    @Override
    public void onBackPressed() {
        //   Intent i = new Intent(getApplicationContext(), MilestoneRedflagActivity.class);
        // startActivity(i);
        if (preferences.getString(Preferences.ID, "").equals("1")) {
            editor.putString(Preferences.BIRTH_DAY, "");
            editor.putString(Preferences.JENIS_KELAMIN, "");
            editor.putString(Preferences.BERAT, "");
            editor.putString(Preferences.TINGGI, "");
            editor.putString(Preferences.ID, "");
            editor.commit();
        }
        finish();
    }


    public void dialog2(String content, String msg) {
        new MaterialDialog.Builder(this)
                .title(msg)
                .content(content)
                .negativeText("OK")
                .callback(new MaterialDialog.ButtonCallback() {
                    @Override
                    public void onPositive(MaterialDialog dialog) {

                    }

                    @Override
                    public void onNegative(MaterialDialog dialog) {
                        super.onNegative(dialog);
                    }

                })
                .show();
    }

    private void setAnswered(int question, boolean yes) {
        if (yes)
            answered.set(question, 1);
        else
            answered.set(question, 0);
    }

    //gender
    public String getInterpretasiKelamin(String kelamin){
        //cowo
        if (getAgeYear()>=1 && getAgeYear()<4 && getKelamin().equals("m")){
                return kelamin+ "(120/75 - 80/34)";
        }
        if (getAgeYear()>=4 && getAgeYear()<7 && getKelamin().equals("m")){
                return kelamin+ "(128/84 - 88/47)";
        }
        if (getAgeYear()>=7 && getAgeYear()<10 && getKelamin().equals("m")){
                return kelamin+ "(130/90 - 92/53)";
        }

        //cewe
        if (getAgeYear()>=1 && getAgeYear()<4 && getKelamin().equals("f")){
                return kelamin+ "(117/76 - 83/38)";
        }
        if (getAgeYear()>=4 && getAgeYear()<7 && getKelamin().equals("f")){
                return kelamin+ "(122/83 - 88/50)";
        }
        if (getAgeYear()>=7 && getAgeYear()<10 && getKelamin().equals("f")){
                return kelamin+ "(129/88 - 93/55)";
        }
        return "unknown";
    }

    public String getInterpretasiNadi(String lajuNadi) {


        //lajuNadi = 0;
        if(getAgeMonth()<1 && getAgeYear()<1){
                return lajuNadi+" (120-170)";
        }else if(getAgeMonth()>=1 && getAgeMonth()<3 && getAgeYear()<1){
                return lajuNadi+" (100-150)";
        }else if(getAgeMonth()>=3 && getAgeMonth()<6 && getAgeYear()<1){
                return lajuNadi+" (90-120)";
        }else if(getAgeMonth()>=6 && getAgeMonth()<12 && getAgeYear()<1){
                return lajuNadi+" (80-120)";
        }else if(getAgeYear()>=1 && getAgeYear()<3 ){
                return lajuNadi+" (70-110)";
        }else if(getAgeYear()>=3 && getAgeYear()<6 ){
                return lajuNadi+" (65-110)";
        }else if(getAgeYear()>=6 && getAgeYear()<12 ){
                return lajuNadi+" (60-95)";
        }else if(getAgeYear()>=12 ){
                return lajuNadi+" (55-85)";
        }
        return "unknown";
    }

    public String getInterpretasiNafas(String lajuNafas) {

        //lajuNafas = 0;
        if(getAgeMonth()<1 && getAgeYear()<1){
                return lajuNafas +" (40-60)";

        }else if(getAgeMonth()>=1 && getAgeMonth()<3 && getAgeYear()<1){

                return lajuNafas +" (35-55)";

        }else if(getAgeMonth()>=3 && getAgeMonth()<6 && getAgeYear()<1){

                return lajuNafas +" (30-45)";

        }else if(getAgeMonth()>=6 && getAgeMonth()<12 && getAgeYear()<1){

                return lajuNafas +" (25-40)";

        }else if(getAgeYear()>=1 && getAgeYear()<3 ){

                return lajuNafas +" (20-30)";

        }else if(getAgeYear()>=3 && getAgeYear()<6 ){

                return lajuNafas +" (20-25)";

        }else if(getAgeYear()>=6 && getAgeYear()<12 ){

                return lajuNafas +" (14-22)";

        }else if(getAgeYear()>=12 ){

                return lajuNafas +" (12-18)";

        }
        return "unknown";
    }

    public String getInterpretasiSuhu(String suhu) {
        //suhu = 0;
            return suhu +" (36.5-37.5)";
    }

    private void progresDialog(boolean horizontal, String msg) {
        dialog = new MaterialDialog.Builder(this)
                .title(msg)
                .progressIndeterminateStyle(horizontal)
                .cancelable(true)
                .show();
    }
}
