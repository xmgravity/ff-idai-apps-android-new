/**
 * This class use for store sharedprefrence key
 *
 * @author Codelabs
 */
package com.prima;

public class Preferences {
    public static final String ACCESS_TOKEN = "token";
    public static final String ID = "id";
    public static final String TINGGI = "height";
    public static final String BERAT = "weight";
    public static final String LINGKAR_KEPALA = "lingkar_kepala";
    public static final String JENIS_KELAMIN = "jenis_kelamin";
    public static final String ISLOGOUT = "logout";
    public static final String BIRTH_DAY = "birth_day";
    public static final String ID_CALENDAR = "id_calendar";
    public static final String LOADED = "loaded";
    public static final String ISLOGIN = "islogin";
    public static final String TIPE_USER = "tipe_user";
    public static final String USER_ONLINE = "user_offline";
    public static final String SHOW_HINT = "show_hint";
    public static final String PHOTO = "photo";
    public static final String MOM_HEIGHT = "mom_height";
    public static final String DAD_HEIGHT = "dad_height";
    public static final String JUDUL_CATATAN = "title";
    public static final String DESKRIPSI_CATATAN = "description";
    public static final String DATE_TIME = "date_time";
    public static final String LANGUAGE = "language";


    //doctor
    public static final String NAME = "name";
    public static final String EMAIL = "email";
    public static final String SUBSPESIALISASI = "subspesialisasi";
    public static final String NPA = "npa";
    public static final String FOTO = "foto";
    public static final String EMAILOTOMATIS = "email_otomatis";
    public static final String ID_USER = "id_user";
    public static final String DOB = "dob";
    public static final String GENDER = "gender";

    //orang tua
    public static final String ORANGTUA_ID = "ortu_id"; // temp value untuk save orangtua by doctor
    public static final String ADDRESS = "ortu_address";
    public static final String FULLNAME = "fullname_ortu";
    public static final String HANDPHONE = "handphone_ortu";

    //AWAM

    //anak
    public static final String CHILD_NAME = "child_name";

}
