package com.prima.activities;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.RadioButton;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import prima.test.prima.R;

/**
 * Created by Codelabs on 14/09/2015.
 */
public class AgendaPengingatActivity extends BaseActivity implements View.OnClickListener {

    Button simpan;
    TextView title;
    List<RadioButton> radioButtons;
    List<RadioButton> rads1;
    List<RadioButton> rads2;

    @Override
    public void initView() {
        simpan = (Button) findViewById(R.id.simpan);
        title = (TextView) findViewById(R.id.tv_actionbar_title);
        //set font
        Typeface tf = Typeface.createFromAsset(getApplicationContext().getAssets(), "BREVIA-BLACK.OTF");
        title.setTypeface(tf);
        title.setText(getResources().getString(R.string.Pengingat));


        radioButtons = new ArrayList<RadioButton>();

        radioButtons.add((RadioButton) findViewById(R.id.rad1));
        radioButtons.add((RadioButton) findViewById(R.id.rad2));
        radioButtons.add((RadioButton) findViewById(R.id.rad3));
        radioButtons.add((RadioButton) findViewById(R.id.rad4));
        radioButtons.add((RadioButton) findViewById(R.id.rad5));
        radioButtons.add((RadioButton) findViewById(R.id.rad6));
        radioButtons.add((RadioButton) findViewById(R.id.rad7));

        rads1 = new ArrayList<RadioButton>();
        rads1.add((RadioButton) findViewById(R.id.rad8));
        rads1.add((RadioButton) findViewById(R.id.rad9));

        rads2 = new ArrayList<RadioButton>();
        rads2.add((RadioButton) findViewById(R.id.rad10));
        rads2.add((RadioButton) findViewById(R.id.rad11));

        //checked
        for (RadioButton button : rads1) {
            button.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if (isChecked) processRadioButtonClick2(buttonView);
                }
            });

        }

        //checked
        for (RadioButton button : rads2) {
            button.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if (isChecked) processRadioButtonClick3(buttonView);
                }
            });

        }

        //checked
        for (RadioButton button : radioButtons) {
            button.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if (isChecked) processRadioButtonClick(buttonView);
                }
            });

        }


        Bundle bn = getIntent().getExtras();
        if (bn != null) {
            String pengingat = bn.getString("pengingat");
            String getar = bn.getString("getar");
            String suara = bn.getString("suara");

            if (getar.equals("0")) {
                rads2.get(1).setChecked(true);
            } else if (getar.equals("1")) {
                rads2.get(0).setChecked(true);
            }

            if (suara.equals("0")) {
                rads1.get(1).setChecked(true);
            } else if (suara.equals("1")) {
                rads1.get(0).setChecked(true);
            }


            if (pengingat.equals("1440")) {
                radioButtons.get(0).setChecked(true);
            }
            if (pengingat.equals("180")) {
                radioButtons.get(1).setChecked(true);
            }
            if (pengingat.equals("120")) {
                radioButtons.get(2).setChecked(true);
            }
            if (pengingat.equals("60")) {
                radioButtons.get(3).setChecked(true);
            }
            if (pengingat.equals("30")) {
                radioButtons.get(4).setChecked(true);
            }
            if (pengingat.equals("15")) {
                radioButtons.get(5).setChecked(true);
            }
            if (pengingat.equals("5")) {
                radioButtons.get(6).setChecked(true);
            }
        }

    }

    @Override
    public void setUICallbacks() {
        simpan.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        if (v == simpan) {
            Intent i = new Intent(this, AgendaAddActivity.class);
            if (rads2.get(0).isChecked()) {
                i.putExtra("getar", "1");
            } else {
                i.putExtra("getar", "0");
            }
            if (rads1.get(0).isChecked()) {
                i.putExtra("suara", "1");
            } else {
                i.putExtra("suara", "0");
            }

            if (radioButtons.get(0).isChecked())
                i.putExtra("reminder", "1");
            if (radioButtons.get(1).isChecked())
                i.putExtra("reminder", "2");
            if (radioButtons.get(2).isChecked())
                i.putExtra("reminder", "3");
            if (radioButtons.get(3).isChecked())
                i.putExtra("reminder", "4");
            if (radioButtons.get(4).isChecked())
                i.putExtra("reminder", "5");
            if (radioButtons.get(5).isChecked())
                i.putExtra("reminder", "6");
            if (radioButtons.get(6).isChecked())
                i.putExtra("reminder", "7");

            setResult(1, i);
            finish();
        }
    }


    @Override
    public int getLayout() {
        return R.layout.activity_pengingat;
    }

    @Override
    public void updateUI() {
        Bundle bn = getIntent().getExtras();
        if (bn != null) {

        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

    }

    private void processRadioButtonClick(CompoundButton buttonView) {

        for (RadioButton button : radioButtons) {
            if (button != buttonView) button.setChecked(false);
        }

    }

    private void processRadioButtonClick2(CompoundButton buttonView) {

        for (RadioButton button : rads1) {
            if (button != buttonView) button.setChecked(false);
        }

    }

    private void processRadioButtonClick3(CompoundButton buttonView) {

        for (RadioButton button : rads2) {
            if (button != buttonView) button.setChecked(false);
        }

    }


    @Override
    public void onBackPressed() {
        //  Intent i = new Intent(getApplicationContext(), BallardScoreActivity.class);
        //  startActivity(i);
        finish();
    }
}
