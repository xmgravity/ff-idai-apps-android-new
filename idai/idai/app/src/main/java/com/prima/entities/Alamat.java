package com.prima.entities;

import java.util.ArrayList;

/**
 * Created by Codelabs on 20/08/2015.
 */
public class Alamat {
    private ArrayList<ListItemAlamat> results;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    private  String status;

    public void setResults(ArrayList<ListItemAlamat> results) {
        this.results = results;
    }

    public class ListItemAlamat {
        private String formatted_address;
        private  Geometry geometry;

        public String getFormatted_address() {
            return formatted_address;
        }

        public void setFormatted_address(String formatted_address) {
            this.formatted_address = formatted_address;
        }

        public Geometry getGeometry() {
            return geometry;
        }

        public void setGeometry(Geometry geometry) {
            this.geometry = geometry;
        }
    }

    public class Geometry{
        private Locations  location;

        public Locations getLocation() {
            return location;
        }

        public void setLocation(Locations location) {
            this.location = location;
        }
    }


    public class Locations {
        private double lat;
        private  double lng;

        public double getLat() {
            return lat;
        }

        public void setLat(double lat) {
            this.lat = lat;
        }

        public double getLng() {
            return lng;
        }

        public void setLng(double lng) {
            this.lng = lng;
        }
    }



    public ArrayList<ListItemAlamat> getResults() {
        return results;
    }

}
