package com.prima.entities;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Codelabs on 23/08/2015.
 */
public class VaksinPasien extends Message implements Serializable {
    private ArrayList<Child> data;
    public ArrayList<Child> getObjects() {
        return data;
    }
    public void setObjects(ArrayList<Child> objects) {
        this.data = objects;
    }


    public class GetChild extends Message {
        private Child objects;

        public Child getObjects() {
            return objects;
        }

        public void setObjects(Child objects) {
            this.objects = objects;
        }
    }

    public class Child {
        private String id;
        private String child_id;
        private String vaccine_step_id;
        private String vaccine_type;
        private String vaccine_brand;
        private String vaccine_date;

        public String getId() {
            return id;
        }
        public void setId(String id) {
            this.id = id;
        }

        public String getchild_id() {
            return child_id;
        }
        public void setchild_id(String child_id) {
            this.child_id = child_id;
        }

        public String getvaccine_step_id() {
            return vaccine_step_id;
        }
        public void setvaccine_step_id(String vaccine_step_id) {
            this.vaccine_step_id = vaccine_step_id;
        }

        public String getvaccine_type() {
            return vaccine_type;
        }
        public void setvaccine_type(String vaccine_type) {
            this.vaccine_type = vaccine_type;
        }

        public String getvaccine_brand() {
            return vaccine_brand;
        }
        public void setvaccine_brand(String vaccine_brand) {
            this.vaccine_brand = vaccine_brand;
        }

        public String getvaccine_date() {
            return vaccine_date;
        }
        public void setvaccine_date(String vaccine_date) {
            this.vaccine_date = vaccine_date;
        }

    }
}