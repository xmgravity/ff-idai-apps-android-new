/**
 * @author Codelabs
 */
package com.prima.callbacks;

public interface OnCallAPI {
	public void onAPIsuccess(String message);
	public void onAPIFailed(String errorMessage);
	public void executeAPI();
}
