package com.prima.activities;

import android.content.Intent;
import android.graphics.Typeface;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.prima.MainActivity;
import com.prima.adapters.ListNotifAdapter;
import com.prima.entities.Agenda;
import com.prima.entities.Notif;
import com.prima.helpers.DatabaseHandler;

import org.joda.time.DateTime;
import org.joda.time.Minutes;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import prima.test.prima.R;

/**
 * Created by Codelabs on 21/09/2015.
 */
public class NotifikasiActivity extends BaseActivity implements View.OnClickListener {
    private ImageView back;
    private ImageView close;
    private TextView title;
    ListView list;
    DatabaseHandler db;

    @Override
    public void initView() {
        db = new DatabaseHandler(context);
        list = (ListView) findViewById(R.id.listView);
        close = (ImageView) findViewById(R.id.close);
        close.setVisibility(View.GONE);
        back = (ImageView) findViewById(R.id.back);

        title = (TextView) findViewById(R.id.tv_actionbar_title);
        Typeface tf = Typeface.createFromAsset(getApplicationContext().getAssets(), "BREVIA-BLACK.OTF");
        title.setTypeface(tf);
        title.setText(getResources().getString(R.string.titleNotifikasi));

        ArrayList<Notif> notif = db.getNotif();
        ArrayList<String> listId = new ArrayList<String>();
        for (int i = 0; i < notif.size(); i++) {
            listId.add(notif.get(i).getId_agenda());
        }

        ArrayList<Agenda.Agendas> listAgenda = db.getAllAgenda().getObjects();
        ArrayList<Agenda.Agendas> agendas = new ArrayList<Agenda.Agendas>();


        Calendar c = Calendar.getInstance();
        System.out.println("Current time => " + c.getTime());

        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat df2 = new SimpleDateFormat("HH:mm");
        String formattedDate = df.format(c.getTime());
        String formattedDate2 = df2.format(c.getTime());



        Log.e("Log", "" + formattedDate + " " + formattedDate2);


        for (int j = 0; j < listAgenda.size(); j++) {
            String waktu1 = formattedDate + " " + formattedDate2;
            String waktu2 = listAgenda.get(j).getTanggal_kunjungan() + " " + listAgenda.get(j).getJam_mulai().substring(0, 5);


            String waktu3 = formattedDate + " 00:00";
            String waktu4 = listAgenda.get(j).getTanggal_kunjungan() + " 00:00";



            if ((getMinutes(waktu1, waktu2) <= Integer.valueOf(listAgenda.get(j).getPengingat()))
                    && getMinutes(waktu3, waktu4) >= 0) {
                agendas.add(listAgenda.get(j));
            }

        }


        ListNotifAdapter adapterList = new ListNotifAdapter(
                agendas, context);
        list.setAdapter(adapterList);

    }


    @Override
    public void setUICallbacks() {
        back.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if (v == back) {
            Intent i = new Intent(this, MainActivity.class);
            startActivity(i);
            finish();
        }
    }


    @Override
    public int getLayout() {
        return R.layout.activity_notifikasi;
    }

    @Override
    public void updateUI() {

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

    }


    @Override
    public void onBackPressed() {
        Intent i = new Intent(this, MainActivity.class);
        startActivity(i);
        finish();
    }


    public int getMinutes(String waktu, String waktu2) {
        final DateTimeFormatter formatter1 = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm");
        final DateTime dateTime1 = formatter1.parseDateTime(waktu);
        final DateTime dateTime2 = formatter1.parseDateTime(waktu2);
        Minutes minutes = Minutes.minutesBetween(dateTime1, dateTime2);
        return minutes.getMinutes();
    }


}
