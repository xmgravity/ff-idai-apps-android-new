/**
 * Base Model
 * @author Codelabs
 * 27 October 2014
 */
package com.prima.model;

import android.content.Context;

import com.j256.ormlite.dao.Dao;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.prima.entities.BaseEntity;
import com.prima.helpers.DbHelper;


public class BaseModel<T> {
	private static DbHelper dbHelper;
	private Dao<T, ?> dao;

	public BaseModel(Context context, Class<?> cls) {
		initDB(context);
		getDao(cls);
	}

	private void initDB(Context context) {
		if (dbHelper == null) {
			dbHelper = new DbHelper(context);
		}
	}

	@SuppressWarnings({ "unchecked", "hiding" })
	public <T extends BaseEntity> List<T> all() {
		try {
			return (List<T>) dao.queryForAll();
		} catch (SQLException e) {
			e.printStackTrace();
			return new ArrayList<T>();
		}
	}

	public T find(String ID) {
		try {
			return dao.queryBuilder().where()
					.eq("id", ID).queryForFirst();
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}

	public T find() {
		try {
			return dao.queryBuilder().queryForFirst();
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}

	@SuppressWarnings({ "unchecked", "hiding" })
	public <T extends BaseEntity> List<T> findMany(String ID) {
		try {
			return (List<T>) dao.queryBuilder().where().eq("id", ID)
					.query();
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}

	public BaseEntity findBy(String condition, Object value) {
		try {
			return (BaseEntity) dao.queryBuilder().where().eq(condition, value)
					.queryForFirst();
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}

	@SuppressWarnings({ "unchecked", "hiding" })
	public <T extends BaseEntity> List<T> findManyBy(String condition,
													 Object value) {
		try {
			return (List<T>) dao.queryBuilder().where().like(condition, value)
					.query();
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}

	@SuppressWarnings("unchecked")
	public void save(BaseEntity entity) {
		try {
			dao.createIfNotExists((T) entity);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	@SuppressWarnings("unchecked")
	public void update(BaseEntity entity) {
		try {
			dao.createOrUpdate((T) entity);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	@SuppressWarnings("unchecked")
	public void delete(BaseEntity entity) {
		try {
			dao.delete((T) entity);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	@SuppressWarnings("unchecked")
	public void softDelete(BaseEntity entity) {
		try {
			entity.setDeleted(true);
			dao.update((T) entity);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public void deleteAll(List<T> entities) {
		try {
			dao.delete(entities);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	@SuppressWarnings("unchecked")
	private void getDao(Class<?> cls) {
		try {
			if (dao == null) {
				dao = (Dao<T, ?>) dbHelper.getDao(cls);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

}
