package com.prima.activities;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.JsonSyntaxException;
import com.joanzapata.pdfview.PDFView;
import com.joanzapata.pdfview.listener.OnPageChangeListener;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.prima.Preferences;
import com.prima.controllers.ApiReferences;
import com.prima.controllers.Parser;
import com.prima.entities.Message;
import com.prima.entities.Pdf;
import com.prima.helpers.DatabaseHandler;

import org.joda.time.LocalDate;
import org.joda.time.Months;
import org.joda.time.Years;

import java.io.File;

import prima.test.prima.R;

/**
 * Created by Codelabs on 10/08/2015.
 */
public class BloodPressureResultActivity extends BaseActivity implements View.OnClickListener {

    LinearLayout linearPDF, linearInterpretation, linearPercentile, linearPercentileTop, linearTinggi;
    Button kembali;
    TextView title, jenis_kelamin, month, year, tgl_periksa, tinggi, berat, diastolik, sistolik, sistolikDiastolik, percentile, interpretasi, textTitlePercentile;
    ImageView back, close;
    private float diasto;
    private float sisto;
    MaterialDialog dialog;
    private RequestQueue mRequest;
    private PDFView pdfView;
    DatabaseHandler db;
    private Context mContext;
    String url, id, menu, category;

    @Override
    public void initView() {
        linearPDF = (LinearLayout) findViewById(R.id.linearPDF);
        linearPercentile = (LinearLayout) findViewById(R.id.linearPercentile);
        linearPercentileTop = (LinearLayout) findViewById(R.id.linearPercentileTop);
        linearInterpretation = (LinearLayout) findViewById(R.id.linearInterpretation);
        textTitlePercentile = (TextView) findViewById(R.id.textTitlePercentile);
        linearTinggi = (LinearLayout) findViewById(R.id.linearTinggi);

        mRequest = Volley.newRequestQueue(getApplicationContext());
        kembali = (Button) findViewById(R.id.kembali);
        title = (TextView) findViewById(R.id.title);
        jenis_kelamin = (TextView) findViewById(R.id.jenis_kelamin);
        month = (TextView) findViewById(R.id.month);
        year = (TextView) findViewById(R.id.year);
        tgl_periksa = (TextView) findViewById(R.id.tgl_periksa);
        tinggi = (TextView) findViewById(R.id.tinggi);
        berat = (TextView) findViewById(R.id.berat);
        diastolik = (TextView) findViewById(R.id.diastolik);
        sistolik = (TextView) findViewById(R.id.sistolik);
        sistolikDiastolik = (TextView) findViewById(R.id.sistolikDiastolik);
        percentile = (TextView) findViewById(R.id.percentile);
        interpretasi = (TextView) findViewById(R.id.interpretasi);
        pdfView = (PDFView) findViewById(R.id.pdfview);
        back = (ImageView) findViewById(R.id.back);
        close = (ImageView) findViewById(R.id.close);

        //set font
        Typeface tf = Typeface.createFromAsset(getApplicationContext().getAssets(), "Brevia-ExtraBlackItalic.otf");
        title.setTypeface(tf);


        if (preferences.getString(Preferences.JENIS_KELAMIN, "").equalsIgnoreCase("1")) {
            jenis_kelamin.setText("Laki-laki");
        } else {
            jenis_kelamin.setText("Perempuan");
        }


        berat.setVisibility(View.GONE);

        month.setText(String.valueOf(getAgeMonth() - (getAge() * 12)));
        year.setText(String.valueOf(getAge()));
        tgl_periksa.setText((new LocalDate()).toString());
        tinggi.setText(preferences.getString(Preferences.TINGGI, ""));
        berat.setText(preferences.getString(Preferences.BERAT, ""));


    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = getApplicationContext();
        db = new DatabaseHandler(context);
    }


    @Override
    public void setUICallbacks() {
        kembali.setOnClickListener(this);
        close.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        if (v == kembali) {
            //  Intent i = new Intent(getApplicationContext(), BloodPressureActivity.class);
            //    startActivity(i);
            if (preferences.getString(Preferences.ID, "").equals("1")) {
                editor.putString(Preferences.BIRTH_DAY, "");
                editor.putString(Preferences.JENIS_KELAMIN, "");
                editor.putString(Preferences.BERAT, "");
                editor.putString(Preferences.TINGGI, "");
                editor.putString(Preferences.ID, "");
                editor.commit();
            }
            finish();
        }else if (v == close) {
            Intent i = new Intent(getApplicationContext(), BloodPressureActivity.class);
            startActivity(i);
            finish();
        }
    }


    @Override
    public int getLayout() {
        return R.layout.activity_result_blood_pressure;
    }

    @Override
    public void updateUI() {
        Bundle bn = getIntent().getExtras();
        if (bn != null) {
            diastolik.setText(bn.getString("diastolik"));
            sistolik.setText(bn.getString("sistolik"));

            if (bn.getString("sistolik").equals("")){
                sisto = 0;
            }else{
                sisto = Integer.valueOf(bn.getString("sistolik"));
            }

            if (bn.getString("diastolik").equals("")){
                diasto = 0;
            }else{
                diasto = Integer.valueOf(bn.getString("diastolik"));
            }

            sistolikDiastolik.setText(sisto + "/" + diasto);

            float hasil = (int) ((sisto + (2 * diasto)) / 3);

            percentile.setText(String.valueOf(hasil));

//            if (sisto <= 120 && diasto <= 80) {
//                interpretasi.setText("Pasien berada dalam batas normal");
//            } else if ((sisto >= 121 && sisto <= 139) || (diasto >= 81 && diasto <= 89)) {
//                interpretasi.setText("Pasien diduga prehipertensi");
//            } else if ((sisto >= 140 && sisto <= 159) || (diasto >= 90 && diasto <= 99)) {
//                interpretasi.setText("Pasien diduga hipertensi derajat 1");
//            } else if (sisto >= 160 || diasto >= 100) {
//                interpretasi.setText("Pasien diduga hipertensi derajat 2");
//            }


            if (sisto < 120 && diasto < 78) {
                interpretasi.setText("Pasien berada dalam batas normal");
            } else if (((sisto >= 120 && diasto >= 78) && (sisto < 124 && diasto < 82)) || ((sisto / diasto > 120 / 80) && (sisto < 120 && diasto < 78) && (sisto < 124 && diasto < 82))) {
                interpretasi.setText("Pasien diduga prehipertensi");
            } else if (((sisto + 5) > 131 && (diasto + 5) > 90)) {
                interpretasi.setText("Pasien diduga prehipertensi");
            } else if ((sisto > 124 && diasto > 82)) {
                interpretasi.setText("Pasien diduga hipertensi derajat 2");
            } else if (((sisto + 5) > 124 && (diasto + 5) > 82) && ((sisto + 5) <= 131 && (diasto + 5) <= 90)) {
                interpretasi.setText("Pasien diduga hipertensi derajat 1");
            }


            if(getAge()<1) {
                linearPDF.setVisibility(View.VISIBLE);
                linearPercentile.setVisibility(View.GONE);
                linearPercentileTop.setVisibility(View.GONE);
                linearInterpretation.setVisibility(View.GONE);
                textTitlePercentile.setVisibility(View.GONE);
                linearTinggi.setVisibility(View.GONE);

                if (preferences.getString(Preferences.JENIS_KELAMIN, "").equalsIgnoreCase("1")) {
                    getPdf("BPC", "193");
                } else {
                    getPdf("BPC", "194");
                }

            }else{
                linearPDF.setVisibility(View.GONE);
                linearPercentile.setVisibility(View.VISIBLE);
                linearPercentileTop.setVisibility(View.VISIBLE);
                linearInterpretation.setVisibility(View.VISIBLE);
                textTitlePercentile.setVisibility(View.VISIBLE);
                linearTinggi.setVisibility(View.VISIBLE);
            }
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

    }

    public int getAge() {
        Years years = Years.yearsBetween(new LocalDate(preferences.getString(Preferences.BIRTH_DAY, "")), new LocalDate());
        return years.getYears();
    }

    public int getAgeMonth() {
        Months month = Months.monthsBetween(new LocalDate(preferences.getString(Preferences.BIRTH_DAY, "")), new LocalDate());
        return month.getMonths();
    }

    @Override
    public void onBackPressed() {
        if (preferences.getString(Preferences.ID,"").equals("1")) {
            editor.putString(Preferences.BIRTH_DAY, "");
            editor.putString(Preferences.JENIS_KELAMIN, "");
            editor.putString(Preferences.BERAT, "");
            editor.putString(Preferences.TINGGI, "");
            editor.putString(Preferences.ID, "");
            editor.commit();
        }
        //    Intent i = new Intent(getApplicationContext(), BloodPressureActivity.class);
        // startActivity(i);
        finish();
    }


    private void getPdf(final String ID, final String code) {
        progresDialog(false, getResources().getString(R.string.MemuatData));

        StringRequest listdata = new StringRequest(Request.Method.GET,
                ApiReferences.getPdf(ID, code,  preferences.getString(Preferences.LANGUAGE, "")),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        dialog.dismiss();
                        try {
                            if (response.equals("[]")) {
                                dialog(getResources().getString(R.string.BelumAdaData), getResources().getString(R.string.Pesan));
                            } else {
                                Message msg = Parser.getMessage(response);
                                if (msg.getStatus().equals("1")) {
                                    Log.i("RESULT JSON", response.toString());
                                    Pdf pdf = Parser.getPdf(response);


                                    url = pdf.getObjects().getFilename();
                                    id = pdf.getObjects().getId();
                                    menu = pdf.getObjects().getMenu();
                                    category = pdf.getObjects().getCategory();


                                    Ion.with(context)
                                            .load(ApiReferences.getImageUrl() + url)
                                            .write(new File("/sdcard/prima/pdf/" + "temp" + getFilename(ApiReferences.getImageUrl() + url)))
                                            .setCallback(new FutureCallback<File>() {
                                                @Override
                                                public void onCompleted(Exception e, File fsile) {
                                                    // download done...
                                                    // do stuff with the File or error
                                                    File file = fsile;

                                                    try {
                                                        if (e.toString().contains("ETIMEDOUT")) {
                                                            deletePdf("/sdcard/prima/pdf/" + "temp" + getFilename(ApiReferences.getImageUrl() + url));
                                                            file = null;
                                                        }
                                                    } catch (NullPointerException err) {
                                                        err.printStackTrace();
                                                    }

                                                    dialog.dismiss();
                                                    if (file == null) {
                                                        if (!db.getFile(Integer.valueOf(category)).equals("")) {
                                                            File f = new File("/sdcard/prima/pdf/" + db.getFile(Integer.valueOf(category)));
                                                            pdfView.fromFile(f)
                                                                    .defaultPage(1)
                                                                    .showMinimap(false)
                                                                    .enableSwipe(true)
                                                                    .onPageChange(new OnPageChangeListener() {
                                                                        @Override
                                                                        public void onPageChanged(int i, int i1) {

                                                                        }
                                                                    }).load();
                                                        } else {
                                                            dialog(getResources().getString(R.string.GagalMengunduhPdf), getResources().getString(R.string.Perhatian));
                                                        }
                                                    } else {
                                                        deletePdf("/sdcard/prima/pdf/" + db.getFile(Integer.valueOf(category)));
                                                        db.deleteFile(Integer.valueOf(id));

                                                        File from = new File("/sdcard/prima/pdf/", "temp" + getFilename(ApiReferences.getImageUrl() + url));
                                                        File to = new File("/sdcard/prima/pdf/", getFilename(getFilename(ApiReferences.getImageUrl() + url)));
                                                        from.renameTo(to);

                                                        db.insertFile(Integer.valueOf(id), menu, getFilename(ApiReferences.getImageUrl() + url), category);
                                                        File f = new File("/sdcard/prima/pdf/" + db.getFile(Integer.valueOf(category)));

                                                        pdfView.fromFile(f)
                                                                .defaultPage(1)
                                                                .showMinimap(false)
                                                                .enableSwipe(true)
                                                                .onPageChange(new OnPageChangeListener() {
                                                                    @Override
                                                                    public void onPageChanged(int i, int i1) {

                                                                    }
                                                                })
                                                                .load();
                                                    }
                                                }
                                            });
                                } else {
                                    dialog(msg.getMessages(), getResources().getString(R.string.Pesan));
                                }
                            }
                        } catch (JsonSyntaxException e) {
                            dialog(getResources().getString(R.string.KesalahanServer), getResources().getString(R.string.Perhatian));
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dialog.dismiss();
                Pdf pdf = db.getFilesObject(Integer.valueOf(ID));
                if (!pdf.getObjects().equals("") && pdf.getObjects() != null) {
                    /*Intent i = new Intent(getApplicationContext(), ArtikelPagerGrafikFentonResultActivity.class);
                    i.putStringArrayListExtra("title", grafikName);
                    i.putExtra("url", pdf.getObjects().getFilename());
                    i.putExtra("id", pdf.getObjects().getId());
                    i.putExtra("menu", pdf.getObjects().getMenu());
                    i.putExtra("category", pdf.getObjects().getCategory());
                    startActivity(i);*/
                } else {
                    dialog(getResources().getString(R.string.NoConnection), getResources().getString(R.string.Perhatian));
                }
            }
        });
        listdata.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        mRequest.add(listdata);
    }


    private void progresDialog(boolean horizontal, String msg) {
        dialog = new MaterialDialog.Builder(this)
                .title(msg)
                .content(getResources().getString(R.string.MohonTunggu))
                .progress(true, 0)
                .progressIndeterminateStyle(horizontal)
                .cancelable(false)
                .show();
    }

    public void dialog(String content, String msg) {
        new MaterialDialog.Builder(this)
                .title(msg)
                .content(content)
                .positiveText("Ok")
                .callback(new MaterialDialog.ButtonCallback() {
                    @Override
                    public void onPositive(MaterialDialog dialog) {
                    }

                })
                .show();
    }
}
