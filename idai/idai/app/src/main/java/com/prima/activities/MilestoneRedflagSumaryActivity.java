package com.prima.activities;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.prima.Preferences;
import com.prima.controllers.Parser;
import com.prima.entities.Question;

import java.util.ArrayList;
import java.util.List;

import prima.test.prima.R;

/**
 * Created by Codelabs on 21/08/2015.
 */
public class MilestoneRedflagSumaryActivity extends BaseActivity implements View.OnClickListener {
    private ImageView close;
    private ImageView back;
    private Button lanjut;
    TextView title;
    List<CheckBox> radioButtons;
    LinearLayout listMenu;
    Question question;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }

    @Override
    public void initView() {
        listMenu = (LinearLayout) findViewById(R.id.menu);
        back = (ImageView) findViewById(R.id.back);
        close = (ImageView) findViewById(R.id.close);
        lanjut = (Button) findViewById(R.id.lanjut);
        title = (TextView) findViewById(R.id.tv_actionbar_title);
        Typeface tf = Typeface.createFromAsset(getApplicationContext().getAssets(), "BREVIA-BLACK.OTF");
        title.setTypeface(tf);
        title.setText(getResources().getString(R.string.TahapandanTandaBahayaPerkembangan));

        radioButtons = new ArrayList<CheckBox>();
        Bundle bn = getIntent().getExtras();
        if (bn != null) {
            String respon = bn.getString("respon");
            question = Parser.getQuestion(respon);
            for (int i = 0; i < question.getObjects().size(); i++) {
                for (int j = 0; j < question.getObjects().get(i).getAnswer_option().size(); j++) {
                    View child = getLayoutInflater().inflate(R.layout.list_item_milestone_summary, null);
                    LinearLayout judul = (LinearLayout) child.findViewById(R.id.judul);
                    CheckBox ck1 = (CheckBox) child.findViewById(R.id.ck1);
                    TextView txt1 = (TextView) child.findViewById(R.id.txt1);
                    TextView txt2 = (TextView) child.findViewById(R.id.txt2);
                    ck1.setId(i + j);
                    txt1.setId(i + j);
                    judul.setId(i + j);
                    txt2.setId(i + j + 99);
                    if (j == 0) {
                        judul.setVisibility(View.VISIBLE);
                        txt2.setText(question.getObjects().get(i).getTitle());
                    } else {
                        judul.setVisibility(View.GONE);
                    }
                    txt1.setText(question.getObjects().get(i).getAnswer_option().get(j).getOption());
                    radioButtons.add(ck1);
                    listMenu.addView(child);
                }
            }
            //checked
            for (CheckBox button : radioButtons) {
                button.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                    }
                });

            }
        }

    }

    @Override
    public void setUICallbacks() {
        back.setOnClickListener(this);
        close.setOnClickListener(this);
        lanjut.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        if (v == back) {
         //   Intent i = new Intent(getApplicationContext(), MilestoneRedflagActivity.class);
         //   startActivity(i);
            finish();
        } else if (v == close) {
         //   Intent i = new Intent(getApplicationContext(), MilestoneRedflagActivity.class);
         //   startActivity(i);
            finish();
        } else if (v == lanjut) {
            boolean answeredAll = true;
            for (int i = 0; i < radioButtons.size(); i++) {
                if (radioButtons.get(i).isChecked()) {
                    answeredAll = false;
                }
            }
            if (answeredAll) {
                Intent i = new Intent(getApplicationContext(), MilestoneRedflagResult.class);
                if (preferences.getString(Preferences.ID,"").equals("1")) {
                    editor.putString(Preferences.BIRTH_DAY, "");
                    editor.putString(Preferences.JENIS_KELAMIN, "");
                    editor.putString(Preferences.BERAT, "");
                    editor.putString(Preferences.TINGGI, "");
                    editor.putString(Preferences.ID, "");
                    editor.commit();
                }
                i.putExtra("status", "OK2");
                startActivity(i);
                finish();
            } else {
                if (preferences.getString(Preferences.ID,"").equals("1")) {
                    editor.putString(Preferences.BIRTH_DAY, "");
                    editor.putString(Preferences.JENIS_KELAMIN, "");
                    editor.putString(Preferences.BERAT, "");
                    editor.putString(Preferences.TINGGI, "");
                    editor.putString(Preferences.ID, "");
                    editor.commit();
                }
                Intent i = new Intent(getApplicationContext(), MilestoneRedflagResult.class);
                i.putExtra("status", "NOTOK");
                startActivity(i);
                finish();
            }
        }
    }


    @Override
    public int getLayout() {
        return R.layout.activity_milestone_redflag_summary;
    }

    @Override
    public void updateUI() {

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

    }


    @Override
    public void onBackPressed() {
     //   Intent i = new Intent(getApplicationContext(), MilestoneRedflagActivity.class);
   //     startActivity(i);
        finish();
    }


}
