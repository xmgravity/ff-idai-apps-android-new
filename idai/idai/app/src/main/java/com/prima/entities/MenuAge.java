package com.prima.entities;

import java.util.ArrayList;

/**
 * Created by Codelabs on 07/09/2015.
 */
public class MenuAge extends Message {
    private ArrayList<Menu> data;

    public ArrayList<Menu> getObjects() {
        return data;
    }

    public void setObjects(ArrayList<Menu> objects) {
        this.data = objects;
    }

    public class Menu {
        private String id;
        private String range_age;
        private String code_menu;
        private String name_menu;
        private String min_age;
        private String max_age;

        public String getCode_menu() {
            return code_menu;
        }

        public void setCode_menu(String code_menu) {
            this.code_menu = code_menu;
        }

        public String getName_menu() {
            return name_menu;
        }

        public void setName_menu(String name_menu) {
            this.name_menu = name_menu;
        }

        public String getMin_age() {
            return min_age;
        }

        public void setMin_age(String min_age) {
            this.min_age = min_age;
        }

        public String getMax_age() {
            return max_age;
        }

        public void setMax_age(String max_age) {
            this.max_age = max_age;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getRange_age() {
            return range_age;
        }

        public void setRange_age(String range_age) {
            this.range_age = range_age;
        }
    }
}
