package com.prima;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.ContentUris;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.CalendarContract;
import android.support.v4.widget.DrawerLayout;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.crashlytics.android.Crashlytics;
import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.mixpanel.android.mpmetrics.MixpanelAPI;
import com.prima.activities.AgendaActivity;
import com.prima.activities.ArtikelPagerAlvaradoActivity;
import com.prima.activities.ArtikelPagerBalardActivity;
import com.prima.activities.ArtikelPagerBloodPressureActivity;
import com.prima.activities.ArtikelPagerGpphActivity;
import com.prima.activities.ArtikelPagerGrafikActivity;
import com.prima.activities.ArtikelPagerGrafikFentonActivity;
import com.prima.activities.ArtikelPagerKalkulatorActivity;
import com.prima.activities.ArtikelPagerKaloriSehariActivity;
import com.prima.activities.ArtikelPagerKmmeActivity;
import com.prima.activities.ArtikelPagerKpspActivity;
import com.prima.activities.ArtikelPagerLfgActivity;
import com.prima.activities.ArtikelPagerPelodsActivity;
import com.prima.activities.ArtikelPagerPgcsActivity;
import com.prima.activities.ArtikelPagerPsc17Activity;
import com.prima.activities.ArtikelPagerRedFlagActivity;
import com.prima.activities.ArtikelPagerTahapPubertasActivity;
import com.prima.activities.ArtikelPagerTandaVitalActivity;
import com.prima.activities.AwamChangePasswordActivity;
import com.prima.activities.BahasaActivity;
import com.prima.activities.BaseActivity;
import com.prima.activities.EdukasiOrangtuaActivity;
import com.prima.activities.GrafikPertumbuhanResultActivity;
import com.prima.activities.ImunisasiIdaiPdf;
import com.prima.activities.KuesionerKunjunganAnakSakitActivity;
import com.prima.activities.KuesionerKunjunganAnakSehatActivity;
import com.prima.activities.KuesionerSebelumKedokterActivity;
import com.prima.activities.MasalahUmumActivity;
import com.prima.activities.MchatActivity;
import com.prima.activities.MchatrfPdfActivity;
import com.prima.activities.NotifikasiActivity;
import com.prima.activities.ProfileActivity;
import com.prima.activities.QuestionAndAnswerActivity;
import com.prima.activities.SaranActivity;
import com.prima.activities.SettingActivity;
import com.prima.activities.SkriningMedisPenjelasanKondisiActivity;
import com.prima.activities.SkriningMedisUtamaActivity;
import com.prima.activities.SplashPagerActivity;
import com.prima.activities.StimulasiPerkembanganActivity;
import com.prima.activities.TabelImunisasiActivity;
import com.prima.activities.TentangApplikasiActivity;
import com.prima.controllers.ApiReferences;
import com.prima.controllers.Parser;
import com.prima.entities.Artikel;
import com.prima.entities.Grafik;
import com.prima.entities.MenuAge;
import com.prima.entities.Message;
import com.prima.entities.Notif;
import com.prima.entities.OrangTua;
import com.prima.entities.Pasien;
import com.prima.entities.Pdf;
import com.prima.helpers.DatabaseHandler;
import com.prima.helpers.DateHelper;
import com.prima.helpers.HexagonMaskView;
import com.prima.helpers.ServiceHelper;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import prima.test.prima.R;


public class MainActivity extends BaseActivity implements View.OnClickListener {

    private RelativeLayout
            generasi_prima,
            imunisasi,
            imunisasi_idai,
            grafik,
            glasgowComaScale,
            pediatric_logistic,
            new_ballard,
            blood_pressure,
            laju_filtrasi,
            skala,
            tahap_perkembangan,
            kuisioner,
            kuesioner_kunjungan,
            skrining,
            perhitungan_kalori,
            kebutuhan_kalori_sehari,
            tahap_puber,
            kalkulator_bmr,
            milestones,
            stimulasi_perkembangan,
            kuesioner_sebelum,
            kunjungan_anak_sehat,
            kunjungan_anak_sakit,
            skrining_utama,
            masalah,
            penjelasan_kondisi_medis,
            materi_edukasi,
            agenda,
            saran,
            bahasa,
            ttgApp,
            ubah_pass,
            skrining_lainnya,
            kpsp_awam,
            sesi_pasien,
            tanya_jawab,
            pengaturan,
            beranda,
            kode_pasien_side_menu,
            logout,
            grafik_pertumbuhan,
            grafik_nellhaus,
            grafik_fenton,
            skor_alvarado,
            tanda_vital;

    private DrawerLayout mDrawerLayout;
    private ImageView icDrawer;
    private LinearLayout group_generasi_prima,
            navdrawer,
            group_skala,
            group_perkembangan_anak,
            group_kuesioner,
            group_kuesioner_kunjungan,
            group_skrining_medis,
            group_grafik,
            group_bmr;
    private TextView title, hint_txt,
            btn_reset, kode_pasien, username;
    private ImageView indikator,
            indikator_skala,
            indikator2,
            indikator3,
            indikator_kunjungan,
            indikator4,
            indikator5,
            indikator6,
            iv_notif,
            icon,
            indikator_grafik;

    private Button profile;
    private boolean menu1 = false;
    private boolean menu2 = false;
    private boolean menu3 = false;
    private boolean menu4 = false;
    private boolean menu5 = false;
    private boolean menu6 = false;
    private boolean menu7 = false;
    private boolean menu_grafik = false;
    DateHelper dateHelper;
    Pasien pasien = new Pasien();
    private RequestQueue mRequest;
    MaterialDialog dialog;
    private DatabaseHandler db;
    String isalar = "";
    HexagonMaskView hexa;

    String result;
    ArrayList<String> selected;
    ArrayList<String> grafikName;
    ArrayList<String> interpretasi;
    ArrayList<String> label;


    @Override
    protected void onCreate(Bundle savedInstanceState) {


        super.onCreate(savedInstanceState);
        sendAnalytic("Dashboard");




        // TODO: Move this to where you establish a user session
        logUser();

        String projectToken = "cd40bb64af98d2d99b4b128f8954c724"; // e.g.: "1ef7e30d2a58d27f4b90c42e31d6d7ad"
        MixpanelAPI mixpanel = MixpanelAPI.getInstance(this, projectToken);

        mixpanel.identify(preferences.getString(Preferences.EMAIL, ""));



        try {
            JSONObject props = new JSONObject();


            //set page
            if(preferences.getString(Preferences.JENIS_KELAMIN, "").equalsIgnoreCase("m")){
                props.put("Gender", "Male");
            }else{
                props.put("Gender", "Female");
            }

            if(preferences.getString(Preferences.TIPE_USER, "").equalsIgnoreCase("awam")){
                props.put("User Type", "Patient");
            }else{
                props.put("User Type", "Doctor");
            }

            //set profile



            mixpanel.getPeople().identify(preferences.getString(Preferences.EMAIL, ""));

            mixpanel.getPeople().set("Email", preferences.getString(Preferences.EMAIL, ""));

            if(preferences.getString(Preferences.JENIS_KELAMIN, "").equalsIgnoreCase("m")){
                mixpanel.getPeople().set("Gender", "Male");
            }else{
                mixpanel.getPeople().set("Gender", "Female");
            }

            if(preferences.getString(Preferences.TIPE_USER, "").equalsIgnoreCase("awam")){
                mixpanel.getPeople().set("User Type", "Patient");
            }else{
                mixpanel.getPeople().set("User Type", "Doctor");
            }




            props.put("Logged in", true);
            mixpanel.track("MainActivity - onCreate called by PrimaApp", props);




        } catch (JSONException e) {
            Log.e("MYAPP", "Unable to add properties to JSONObject", e);
        }

    }

    public void forceCrash() {
        throw new RuntimeException("This is a crash");
    }

    private void logUser() {
        // TODO: Use the current user's information
        // You can call any combination of these three methods
        Crashlytics.setUserIdentifier(preferences.getString(Preferences.ID, ""));
        Crashlytics.setUserEmail(preferences.getString(Preferences.EMAIL, ""));
        Crashlytics.setUserName(preferences.getString(Preferences.NAME, ""));
    }

    @Override
    public void initView() {


        db = new DatabaseHandler(context);
        hexa = new HexagonMaskView();
        ArrayList<Notif> notif = db.getNotif();


        startService(new Intent(this, ServiceHelper.class));

        mRequest = Volley.newRequestQueue(getApplicationContext());
        //drawer layout
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        icDrawer = (ImageView) findViewById(R.id.drawer);
        iv_notif = (ImageView) findViewById(R.id.action);
        navdrawer = (LinearLayout) findViewById(R.id.navdrawer);
        username = (TextView) findViewById(R.id.username);
        if (preferences.getString(Preferences.TIPE_USER, "").equals("awam")) {
            username.setText(preferences.getString(Preferences.NAME, ""));
        }else{
            username.setText(getResources().getString(R.string.PendiriIdai));
        }
        skala = (RelativeLayout) findViewById(R.id.skala);

        tahap_perkembangan = (RelativeLayout) findViewById(R.id.tahap_perkembangan);
        kuisioner = (RelativeLayout) findViewById(R.id.kuisioner);
        kuesioner_kunjungan = (RelativeLayout) findViewById(R.id.kuesioner_kunjungan);
        skrining = (RelativeLayout) findViewById(R.id.skrining);

        perhitungan_kalori = (RelativeLayout) findViewById(R.id.perhitungan_kalori);
        kebutuhan_kalori_sehari = (RelativeLayout) findViewById(R.id.kebutuhan_kalori_sehari);

        group_skala = (LinearLayout) findViewById(R.id.group_skala);
        group_skala.setVisibility(View.GONE);

        group_perkembangan_anak = (LinearLayout) findViewById(R.id.group_perkembangan_anak);
        group_perkembangan_anak.setVisibility(View.GONE);

        group_kuesioner = (LinearLayout) findViewById(R.id.group_kuesioner);
        group_kuesioner.setVisibility(View.GONE);

        group_kuesioner_kunjungan = (LinearLayout) findViewById(R.id.group_kuesioner_kunjungan);
        group_kuesioner_kunjungan.setVisibility(View.GONE);

        group_skrining_medis = (LinearLayout) findViewById(R.id.group_skrining_medis);
        group_skrining_medis.setVisibility(View.GONE);


        //indicator group
        indikator = (ImageView) findViewById(R.id.indikator);
        indikator.setBackgroundResource(R.mipmap.ic_expand);
        indikator_skala = (ImageView) findViewById(R.id.indikator_skala);
        indikator_skala.setBackgroundResource(R.mipmap.ic_expand);

        indikator2 = (ImageView) findViewById(R.id.indikator2);
        indikator2.setBackgroundResource(R.mipmap.ic_expand);
        indikator3 = (ImageView) findViewById(R.id.indikator3);
        indikator3.setBackgroundResource(R.mipmap.ic_expand);
        indikator_kunjungan = (ImageView) findViewById(R.id.indikator_kunjungan);
        indikator_kunjungan.setBackgroundResource(R.mipmap.ic_expand);
        indikator4 = (ImageView) findViewById(R.id.indikator4);
        indikator4.setBackgroundResource(R.mipmap.ic_expand);
        indikator5 = (ImageView) findViewById(R.id.indikator5);
        indikator5.setBackgroundResource(R.mipmap.ic_expand);
        indikator6 = (ImageView) findViewById(R.id.indikator6);
        indikator6.setBackgroundResource(R.mipmap.ic_expand);

        indikator_grafik = (ImageView) findViewById(R.id.indikator_grafik);
        indikator_grafik.setBackgroundResource(R.mipmap.ic_expand);


        imunisasi = (RelativeLayout) findViewById(R.id.imunisasi);
        imunisasi_idai = (RelativeLayout) findViewById(R.id.imunisasi_idai);
        imunisasi_idai.setVisibility(View.GONE);
        glasgowComaScale = (RelativeLayout) findViewById(R.id.pediatric_glasgow);

        grafik = (RelativeLayout) findViewById(R.id.grafik);
        group_grafik = (LinearLayout) findViewById(R.id.group_grafik);
        group_grafik.setVisibility(View.GONE);
        grafik_pertumbuhan = (RelativeLayout) findViewById(R.id.grafik_pertumbuhan);
        grafik_nellhaus = (RelativeLayout) findViewById(R.id.grafik_nellhaus);
        grafik_fenton = (RelativeLayout) findViewById(R.id.grafik_fenton);

        group_bmr = (LinearLayout) findViewById(R.id.group_bmr);
        group_bmr.setVisibility(View.GONE);

        generasi_prima = (RelativeLayout) findViewById(R.id.generasi_prima);
        pediatric_logistic = (RelativeLayout) findViewById(R.id.pediatric_logistic);
        new_ballard = (RelativeLayout) findViewById(R.id.new_ballard);
        blood_pressure = (RelativeLayout) findViewById(R.id.blood_pressure);
        laju_filtrasi = (RelativeLayout) findViewById(R.id.laju_filtrasi);
        tahap_puber = (RelativeLayout) findViewById(R.id.tahap_puber);
        kalkulator_bmr = (RelativeLayout) findViewById(R.id.kalkulator_bmr);
        milestones = (RelativeLayout) findViewById(R.id.milestones);
        stimulasi_perkembangan = (RelativeLayout) findViewById(R.id.stimulasi_perkembangan);
        kuesioner_sebelum = (RelativeLayout) findViewById(R.id.kuesioner_sebelum);
        kunjungan_anak_sehat = (RelativeLayout) findViewById(R.id.kunjungan_anak_sehat);
        kunjungan_anak_sakit = (RelativeLayout) findViewById(R.id.kunjungan_anak_sakit);
        skrining_utama = (RelativeLayout) findViewById(R.id.skrining_utama);
        kpsp_awam = (RelativeLayout) findViewById(R.id.kpsp_awam);
        masalah = (RelativeLayout) findViewById(R.id.masalah);
        penjelasan_kondisi_medis = (RelativeLayout) findViewById(R.id.penjelasan_kondisi_medis);
        materi_edukasi = (RelativeLayout) findViewById(R.id.materi_edukasi);
        agenda = (RelativeLayout) findViewById(R.id.agenda);
        saran = (RelativeLayout) findViewById(R.id.saran);
        bahasa = (RelativeLayout) findViewById(R.id.bahasa);
        ttgApp = (RelativeLayout) findViewById(R.id.ttgApp);
        ubah_pass = (RelativeLayout) findViewById(R.id.ubah_pass);
        skrining_lainnya = (RelativeLayout) findViewById(R.id.skrining_lainnya);
        sesi_pasien = (RelativeLayout) findViewById(R.id.sesi_pasien);
        tanya_jawab = (RelativeLayout) findViewById(R.id.tanya_jawab);
        logout = (RelativeLayout) findViewById(R.id.logout);
        pengaturan = (RelativeLayout) findViewById(R.id.pengaturan);
        skor_alvarado = (RelativeLayout) findViewById(R.id.skor_alvarado);
        tanda_vital = (RelativeLayout) findViewById(R.id.tanda_vital);

        kode_pasien = (TextView) findViewById(R.id.kode_pasien);
        beranda = (RelativeLayout) findViewById(R.id.beranda);
        kode_pasien_side_menu = (RelativeLayout) findViewById(R.id.kode_pasien_side_menu);

        //group1
        group_generasi_prima = (LinearLayout) findViewById(R.id.group_generasi_prima);
        group_generasi_prima.setVisibility(View.GONE);
        masalah.setVisibility(View.GONE);

        icon = (ImageView) findViewById(R.id.icon);
        profile = (Button) findViewById(R.id.profile);

        btn_reset = (TextView) findViewById(R.id.btn_reset);
        title = (TextView) findViewById(R.id.tv_actionbar_title);


        Typeface tf = Typeface.createFromAsset(getApplicationContext().getAssets(), "BREVIA-BLACK.OTF");
        title.setTypeface(tf);
        title.setText("PRIMA");

        dateHelper = new DateHelper();

        Typeface tfAriali = Typeface.createFromAsset(getApplicationContext().getAssets(), "ariali.ttf");
        Typeface tfArialb = Typeface.createFromAsset(getApplicationContext().getAssets(), "arialbd.ttf");

        TextView trademark = (TextView) findViewById(R.id.trademark);
        TextView trademark2 = (TextView) findViewById(R.id.trademark2);
        TextView trademark3 = (TextView) findViewById(R.id.trademark3);
        trademark.setTypeface(tfAriali);
        trademark2.setTypeface(tfArialb);
        trademark3.setTypeface(tfAriali);

        if(preferences.getString(Preferences.TIPE_USER, "").equalsIgnoreCase("awam")){
            trademark.setVisibility(View.GONE);
            trademark2.setVisibility(View.GONE);
            trademark3.setVisibility(View.GONE);
        }





    }

    @Override
    public void setUICallbacks() {
        if(preferences.getString(Preferences.TIPE_USER, "").equalsIgnoreCase("awam")){
            imunisasi.setOnClickListener(this);
            imunisasi_idai.setOnClickListener(this);
            generasi_prima.setOnClickListener(this);
            grafik.setOnClickListener(this);
            grafik_pertumbuhan.setOnClickListener(this);
            grafik_nellhaus.setOnClickListener(this);
            grafik_fenton.setOnClickListener(this);
            glasgowComaScale.setOnClickListener(this);
            pediatric_logistic.setOnClickListener(this);
            new_ballard.setOnClickListener(this);
            blood_pressure.setOnClickListener(this);
            laju_filtrasi.setOnClickListener(this);
            icDrawer.setOnClickListener(this);
            skala.setOnClickListener(this);
            tahap_perkembangan.setOnClickListener(this);
            kuisioner.setOnClickListener(this);
            kuesioner_kunjungan.setOnClickListener(this);
            skrining.setOnClickListener(this);
            tahap_puber.setOnClickListener(this);
            kalkulator_bmr.setOnClickListener(this);
            perhitungan_kalori.setOnClickListener(this);
            kebutuhan_kalori_sehari.setOnClickListener(this);
            milestones.setOnClickListener(this);
            stimulasi_perkembangan.setOnClickListener(this);
            kuesioner_sebelum.setOnClickListener(this);
            kunjungan_anak_sehat.setOnClickListener(this);
            kunjungan_anak_sakit.setOnClickListener(this);
            skrining_utama.setOnClickListener(this);
            kpsp_awam.setOnClickListener(this);
            masalah.setOnClickListener(this);
            penjelasan_kondisi_medis.setOnClickListener(this);
            materi_edukasi.setOnClickListener(this);
            agenda.setOnClickListener(this);
            saran.setOnClickListener(this);
            bahasa.setOnClickListener(this);
            ttgApp.setOnClickListener(this);
            ubah_pass.setOnClickListener(this);
            skrining_lainnya.setOnClickListener(this);
            sesi_pasien.setOnClickListener(this);
            btn_reset.setOnClickListener(this);
            profile.setOnClickListener(this);
            tanya_jawab.setOnClickListener(this);
            logout.setOnClickListener(this);
            pengaturan.setOnClickListener(this);
            iv_notif.setOnClickListener(this);
            beranda.setOnClickListener(this);
            kode_pasien_side_menu.setOnClickListener(this);


            grafik_nellhaus.setVisibility(View.GONE);
            grafik_fenton.setVisibility(View.GONE);
            imunisasi_idai.setVisibility(View.GONE);
            skala.setVisibility(View.GONE);
            group_skala.setVisibility(View.GONE);
            sesi_pasien.setVisibility(View.GONE);
            blood_pressure.setVisibility(View.GONE);
            kalkulator_bmr.setVisibility(View.GONE);
            kode_pasien_side_menu.setVisibility(View.GONE);
            skrining_lainnya.setVisibility(View.GONE);
            penjelasan_kondisi_medis.setVisibility(View.GONE);
            blood_pressure.setVisibility(View.GONE);
        }else{
            imunisasi.setOnClickListener(this);
            imunisasi_idai.setOnClickListener(this);
            generasi_prima.setOnClickListener(this);
            grafik.setOnClickListener(this);
            grafik_pertumbuhan.setOnClickListener(this);
            grafik_nellhaus.setOnClickListener(this);
            grafik_fenton.setOnClickListener(this);
            glasgowComaScale.setOnClickListener(this);
            pediatric_logistic.setOnClickListener(this);
            new_ballard.setOnClickListener(this);
            blood_pressure.setOnClickListener(this);
            laju_filtrasi.setOnClickListener(this);
            icDrawer.setOnClickListener(this);
            skala.setOnClickListener(this);
            tahap_perkembangan.setOnClickListener(this);
            kuisioner.setOnClickListener(this);
            kuesioner_kunjungan.setOnClickListener(this);
            skrining.setOnClickListener(this);
            tahap_puber.setOnClickListener(this);
            kalkulator_bmr.setOnClickListener(this);
            perhitungan_kalori.setOnClickListener(this);
            kebutuhan_kalori_sehari.setOnClickListener(this);
            milestones.setOnClickListener(this);
            stimulasi_perkembangan.setOnClickListener(this);
            kuesioner_sebelum.setOnClickListener(this);
            kunjungan_anak_sehat.setOnClickListener(this);
            kunjungan_anak_sakit.setOnClickListener(this);
            skrining_utama.setOnClickListener(this);
            kpsp_awam.setOnClickListener(this);
            masalah.setOnClickListener(this);
            penjelasan_kondisi_medis.setOnClickListener(this);
            materi_edukasi.setOnClickListener(this);
            agenda.setOnClickListener(this);
            saran.setOnClickListener(this);
            bahasa.setOnClickListener(this);
            ttgApp.setOnClickListener(this);
            ubah_pass.setOnClickListener(this);
            skrining_lainnya.setOnClickListener(this);
            sesi_pasien.setOnClickListener(this);
            btn_reset.setOnClickListener(this);
            profile.setOnClickListener(this);
            tanya_jawab.setOnClickListener(this);
            logout.setOnClickListener(this);
            pengaturan.setOnClickListener(this);
            iv_notif.setOnClickListener(this);
            beranda.setOnClickListener(this);
            kode_pasien_side_menu.setOnClickListener(this);
            skor_alvarado.setOnClickListener(this);
            tanda_vital.setOnClickListener(this);

            blood_pressure.setVisibility(View.GONE);
            kalkulator_bmr.setVisibility(View.VISIBLE);

            imunisasi_idai.setVisibility(View.GONE);
            kpsp_awam.setVisibility(View.GONE);
            kode_pasien_side_menu.setVisibility(View.GONE);
            ubah_pass.setVisibility(View.GONE);
        }

        Log.i("dbgbg", preferences.getString(Preferences.FOTO, ""));

        Glide.with(MainActivity.this)
                .load(preferences.getString(Preferences.FOTO, ""))
                .asBitmap()
                .override(200, 200)
                .into(new SimpleTarget<Bitmap>() {
                    @Override
                    public void onResourceReady(Bitmap bitmap, GlideAnimation anim) {
                        Bitmap b = hexa.getHexagonShape(hexa.scaleCenterCrop(bitmap, 200, 200));
                        icon.setImageBitmap(b);
                    }
                });



        if(preferences.getString(Preferences.SHOW_HINT, "").equalsIgnoreCase("true")) {
            LayoutInflater li = LayoutInflater.from(getApplicationContext());
            View dialogView = li.inflate(R.layout.activity_tutorial_hint, null);

            final Dialog dialog;
            dialog = new Dialog(context);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(dialogView);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            dialog.show();

            RelativeLayout hintLayout = (RelativeLayout) dialogView.findViewById(R.id.hint_layout);

            hintLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    editor.putString(Preferences.SHOW_HINT, "false");
                    editor.commit();
                    dialog.dismiss();
                }
            });
        }

    }


    @Override
    public void onClick(View v) {
        /*if (v == imunisasi) {
            String isalar = "";
            if (preferences.getString(Preferences.ID, "").equals("")) {
                // get prompts.xml view
                LayoutInflater li = LayoutInflater.from(getApplicationContext());
                View dialogView = li.inflate(R.layout.dialog_sesi_pasien, null);

                final Dialog dialog;
                dialog = new Dialog(context);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(dialogView);
                //make transparent dialog_sesi_offline_pelod
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                dialog.show();

                Button dialogLanjut = (Button) dialogView.findViewById(R.id.ok);
                Button dialogKode = (Button) dialogView.findViewById(R.id.kode);


                dialogLanjut.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        // get prompts.xml view
                        LayoutInflater li = LayoutInflater.from(getApplicationContext());
                        View dialogView = li.inflate(R.layout.dialog_sesi_pasien_baru, null);
                        dialog.dismiss();

                        final Dialog dialog3;
                        dialog3 = new Dialog(context);
                        dialog3.requestWindowFeature(Window.FEATURE_NO_TITLE);
                        dialog3.setContentView(dialogView);
                        //make transparent dialog_sesi_offline_pelod
                        dialog3.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                        dialog3.show();

                        final EditText email = (EditText) dialogView.findViewById(R.id.email_sesi_baru_edt);
                        final TextView email_status = (TextView) dialogView.findViewById(R.id.email_status);


                        Button dialogLanjut = (Button) dialogView.findViewById(R.id.ok);
                        final RadioButton rad1 = (RadioButton) dialogView.findViewById(R.id.rad1);
                        final RadioButton rad2 = (RadioButton) dialogView.findViewById(R.id.rad2);
                        rad1.setChecked(true);

                        rad1.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                rad1.setChecked(true);
                                rad2.setChecked(false);
                            }
                        });

                        rad2.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                rad2.setChecked(true);
                                rad1.setChecked(false);
                            }
                        });

                        final EditText edt = (EditText) dialogView.findViewById(R.id.edt1);
                        final EditText edt2 = (EditText) dialogView.findViewById(R.id.edt2);
                        final EditText edt3 = (EditText) dialogView.findViewById(R.id.edt3);


                        edt.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                showDatePickerForBirthDay(edt);
                            }
                        });
                        dialogLanjut.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                if (edt.getText().toString().equals("") ||
                                        edt2.getText().toString().equals("") ||
                                        edt3.getText().toString().equals("") ||
                                        email.getText().toString().equals("")) {
                                    dialog(getResources().getString(R.string.IsiSeluruhIsian), getResources().getString(R.string.Perhatian));
                                } else {
                                    String jk;
                                    if (rad1.isChecked()) {
                                        jk = "m";
                                    } else if (rad2.isChecked()) {
                                        jk = "f";
                                    } else {
                                        jk = "";
                                    }
                                    saveSesiPasien(jk, edt.getText().toString(), edt3.getText().toString(), edt2.getText().toString(), "alarm");
                                    dialog3.dismiss();
                                }
                            }
                        });
                    }
                });

                dialogKode.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        // get prompts.xml view
                        LayoutInflater li = LayoutInflater.from(getApplicationContext());
                        View dialogView = li.inflate(R.layout.dialog_sesi_pasien_kunjungan_ulang, null);

                        if (dialogView instanceof TextView) {
                            setTypeFace((TextView) v);
                        }
                        dialog.dismiss();

                        final Dialog dialog2;
                        dialog2 = new Dialog(context);
                        dialog2.requestWindowFeature(Window.FEATURE_NO_TITLE);
                        dialog2.setContentView(dialogView);
                        //make transparent dialog_sesi_offline_pelod
                        dialog2.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                        dialog2.show();
                        Button dialogLanjut = (Button) dialogView.findViewById(R.id.ok);
                        final EditText edt = (EditText) dialogView.findViewById(R.id.edt1);
                        final EditText edt2 = (EditText) dialogView.findViewById(R.id.edt2);
                        final EditText edt3 = (EditText) dialogView.findViewById(R.id.edt3);
                        final EditText edt4 = (EditText) dialogView.findViewById(R.id.email_sesi_baru_edt);
                        dialogLanjut.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                if (edt.getText().toString().equals("") ||
                                        edt2.getText().toString().equals("") ||
                                        edt3.getText().toString().equals("") ||
                                        edt4.getText().toString().equals("")) {
                                    dialog(getResources().getString(R.string.IsiSeluruhIsian), getResources().getString(R.string.Perhatian));
                                } else {
                                    editSesi(edt.getText().toString(), edt2.getText().toString(), edt3.getText().toString());
                                    dialog2.dismiss();
                                }
                            }
                        });
                    }
                });
            } else {
                getTable();
            }*/

        if (v == imunisasi) {

            //forceCrash();
            getTable();
            //getPdfImunisasi("0", "IMUN-IDAI");
            /*
            if (menu7) {
                indikator5.setBackgroundResource(R.mipmap.ic_expand);
                imunisasi_idai.setVisibility(View.GONE);
                menu7 = false;
            } else {
                indikator5.setBackgroundResource(R.mipmap.icn_extend);
                imunisasi_idai.setVisibility(View.VISIBLE);
                menu7 = true;
            }*/
        } else if (v == imunisasi_idai) {
            getTable();
            //getPdfImunisasi("0", "IMUN-IDAI");
        } else if (v == grafik) {
            if (menu_grafik) {
                indikator_grafik.setBackgroundResource(R.mipmap.ic_expand);
                group_grafik.setVisibility(View.GONE);
            } else {
                group_grafik.setVisibility(View.VISIBLE);
                indikator_grafik.setBackgroundResource(R.mipmap.icn_extend);
            }
            menu_grafik = !menu_grafik;
        } else if(v == grafik_pertumbuhan) {
            if (preferences.getString(Preferences.TIPE_USER, "").equalsIgnoreCase("awam")) {
                getArtikel("GRAFIK_AWAM");
            }else{
                getArtikel("GRAFIK");
            }
        } else if (v == grafik_nellhaus) {
            showNellhausDialog();
        } else if (v == grafik_fenton) {
            getArtikel("FEN");
        } else if (v == glasgowComaScale) {
            getArtikel("PGCS");
        } else if (v == generasi_prima) {
            if (menu1) {
                indikator.setBackgroundResource(R.mipmap.ic_expand);
                group_generasi_prima.setVisibility(View.GONE);
                menu1 = false;
            } else {
                group_generasi_prima.setVisibility(View.VISIBLE);
                indikator.setBackgroundResource(R.mipmap.icn_extend);
                //     setAnim(group_generasi_prima);
                menu1 = true;
            }
        } else if (v == pediatric_logistic) {
            getArtikel("PELODS");
        } else if (v == new_ballard) {
            getArtikel("NBS");
        } else if (v == blood_pressure) {
            getArtikel("BPC");
        } else if (v == laju_filtrasi) {
            getArtikel("LFG");
        } else if (v == icDrawer) {
            mDrawerLayout.openDrawer(navdrawer);
        } else if (v == skala) {
            if (menu2) {
                indikator_skala.setBackgroundResource(R.mipmap.ic_expand);
                group_skala.setVisibility(View.GONE);
                menu2 = false;
            } else {
                group_skala.setVisibility(View.VISIBLE);
                indikator_skala.setBackgroundResource(R.mipmap.icn_extend);
                menu2 = true;
            }
        } else if (v == tahap_perkembangan) {
            if (menu3) {
                indikator2.setBackgroundResource(R.mipmap.ic_expand);
                group_perkembangan_anak.setVisibility(View.GONE);
                menu3 = false;
            } else {
                group_perkembangan_anak.setVisibility(View.VISIBLE);
                indikator2.setBackgroundResource(R.mipmap.icn_extend);
                menu3 = true;
            }
        } else if (v == kuisioner) {
            if (menu4) {

                indikator3.setBackgroundResource(R.mipmap.ic_expand);
                group_kuesioner.setVisibility(View.GONE);
                menu4 = false;
            } else {
                group_kuesioner.setVisibility(View.VISIBLE);
                indikator3.setBackgroundResource(R.mipmap.icn_extend);
                menu4 = true;
            }
        } else if (v == kuesioner_kunjungan) {
            if (menu5) {

                indikator_kunjungan.setBackgroundResource(R.mipmap.ic_expand);
                group_kuesioner_kunjungan.setVisibility(View.GONE);
                menu5 = false;
            } else {
                group_kuesioner_kunjungan.setVisibility(View.VISIBLE);
                indikator_kunjungan.setBackgroundResource(R.mipmap.icn_extend);
                menu5 = true;
            }
        } else if (v == skrining) {
            if (menu6) {
                indikator4.setBackgroundResource(R.mipmap.ic_expand);
                group_skrining_medis.setVisibility(View.GONE);
                menu6 = false;
            } else {
                group_skrining_medis.setVisibility(View.VISIBLE);
                indikator4.setBackgroundResource(R.mipmap.icn_extend);
                menu6 = true;
            }
        } else if (v == perhitungan_kalori) {
            if (menu7) {
                indikator6.setBackgroundResource(R.mipmap.ic_expand);
                group_bmr.setVisibility(View.GONE);
                menu7 = false;
            } else {
                group_bmr.setVisibility(View.VISIBLE);
                indikator6.setBackgroundResource(R.mipmap.icn_extend);
                menu7 = true;
            }

        }else if (v == kpsp_awam) {
            getArtikel("KPSP");
        }else if (v == tahap_puber) {
            getArtikel("PPA");
        } else if (v == kalkulator_bmr) {
            getArtikel("BMR");
        } else if (v == kebutuhan_kalori_sehari) {
            getArtikel("KALORISEHARI");
        }else if (v == milestones) {
            getArtikel("MAR");
        } else if (v == stimulasi_perkembangan) {
            getMenuAge("TPA-SP");
        } else if (v == kuesioner_sebelum) {
            getMenuAge("KSKD");
        } else if (v == kunjungan_anak_sehat) {
            getMenuAge("KASE");
        } else if (v == kunjungan_anak_sakit) {
            getMenuAge("KASA");
        } else if (v == skrining_utama) {
            getMenuAge("SU");
        } else if (v == masalah) {
            getMenuAge("MU");
        } else if (v == penjelasan_kondisi_medis) {
            getMenuAge("PKM");
        } else if (v == materi_edukasi) {
            getMenuAge("MEOT");
        } else if (v == agenda) {
            sendAnalytic("Agenda");
            Intent i = new Intent(getApplicationContext(), AgendaActivity.class);
            startActivity(i);
            finish();
        }else if (v == saran) {
            Intent i = new Intent(getApplicationContext(), SaranActivity.class);
            startActivity(i);
            finish();
        }else if (v == bahasa) {
            Intent i = new Intent(getApplicationContext(), BahasaActivity.class);
            startActivity(i);
            finish();
        }else if (v == ttgApp) {
            Intent i = new Intent(getApplicationContext(), TentangApplikasiActivity.class);
            startActivity(i);
            finish();
        }else if (v == ubah_pass) {
            Intent i = new Intent(getApplicationContext(), AwamChangePasswordActivity.class);
            startActivity(i);
            finish();
        } else if (v == skrining_lainnya) {
            // get prompts.xml view
            LayoutInflater li = LayoutInflater.from(getApplicationContext());
            View dialogView = li.inflate(R.layout.dialog_skrining_lainnya, null);

            final Dialog dialog;
            dialog = new Dialog(context);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(dialogView);
            //make transparent dialog_sesi_offline_pelod
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            dialog.show();

            TextView menu1 = (TextView) dialogView.findViewById(R.id.txt_kpsp);
            TextView menu2 = (TextView) dialogView.findViewById(R.id.txt_kmme);
            TextView menu3 = (TextView) dialogView.findViewById(R.id.txt_gpph);
            TextView menu4 = (TextView) dialogView.findViewById(R.id.txt_mchat);
            TextView menu5 = (TextView) dialogView.findViewById(R.id.txt_psc17);

            menu1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    getArtikel("KPSP");
                }
            });
            menu2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    getArtikel("KMME");

                }
            });
            menu3.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    getArtikel("GPPH");

                }
            });

            menu4.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(getApplicationContext(), MchatActivity.class);
                    startActivity(i);
                    finish();
                }
            });
            menu5.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    getArtikel("PSC17");
                }
            });


        } else if (v == sesi_pasien) {
            if (preferences.getString(Preferences.ID, "").equals("")) {
                // get prompts.xml view
                LayoutInflater li = LayoutInflater.from(getApplicationContext());
                View dialogView = li.inflate(R.layout.dialog_sesi_pasien, null);

                final Dialog dialog;
                dialog = new Dialog(context);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(dialogView);
                //make transparent dialog_sesi_offline_pelod
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                dialog.show();

                Button dialogLanjut = (Button) dialogView.findViewById(R.id.ok);
                Button dialogKode = (Button) dialogView.findViewById(R.id.kode);


                dialogLanjut.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        // get prompts.xml view
                        LayoutInflater li = LayoutInflater.from(getApplicationContext());
                        View dialogView = li.inflate(R.layout.dialog_sesi_pasien_baru, null);
                        dialog.dismiss();

                        final Dialog dialog3;
                        dialog3 = new Dialog(context);
                        dialog3.requestWindowFeature(Window.FEATURE_NO_TITLE);
                        dialog3.setContentView(dialogView);
                        //make transparent dialog_sesi_offline_pelod
                        dialog3.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                        dialog3.show();

                        final EditText email = (EditText) dialogView.findViewById(R.id.email_sesi_baru_edt);
                        final TextView email_status = (TextView) dialogView.findViewById(R.id.email_status);
                        final TextView email_status_front = (TextView) dialogView.findViewById(R.id.email_status_front);
                        email_status.setVisibility(View.GONE);
                        email_status_front.setVisibility(View.GONE);


                        email.addTextChangedListener(new TextWatcher() {
                            CharSequence seq;

                            @Override
                            public void onTextChanged(CharSequence s, int start, int count, int after) {seq = s;}

                            @Override
                            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

                            @Override
                            public void afterTextChanged(Editable arg0) {
                                email_status.setVisibility(View.VISIBLE);
                                findOrtu(seq.toString().trim(), email_status);

                            }
                        });


                        Button dialogLanjut = (Button) dialogView.findViewById(R.id.ok);
                        final RadioButton rad1 = (RadioButton) dialogView.findViewById(R.id.rad1);
                        final RadioButton rad2 = (RadioButton) dialogView.findViewById(R.id.rad2);
                        rad1.setChecked(true);

                        rad1.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                rad1.setChecked(true);
                                rad2.setChecked(false);
                            }
                        });

                        rad2.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                rad2.setChecked(true);
                                rad1.setChecked(false);
                            }
                        });

                        final TextView edt = (TextView) dialogView.findViewById(R.id.edt1);
                        final EditText edt2 = (EditText) dialogView.findViewById(R.id.edt2);
                        final EditText edt3 = (EditText) dialogView.findViewById(R.id.edt3);


                        edt.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                showDatePickerForBirthDay(edt);
                            }
                        });
                        dialogLanjut.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                if (edt.getText().toString().equals("") ||
                                        edt2.getText().toString().equals("") ||
                                        edt3.getText().toString().equals("") ||
                                        email.getText().toString().equals("")) {
                                    dialog(getResources().getString(R.string.IsiSeluruhIsian), getResources().getString(R.string.Perhatian));
                                } else {
                                    String jk;
                                    if (rad1.isChecked()) {
                                        jk = "m";
                                    } else if (rad2.isChecked()) {
                                        jk = "f";
                                    } else {
                                        jk = "";
                                    }
                                    saveSesiPasien(jk, edt.getText().toString(), edt3.getText().toString(), edt2.getText().toString(), "");
                                    dialog3.dismiss();
                                }
                            }
                        });
                    }
                });

                //TODO cek pasien kunjungan ulang
                dialogKode.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        // get prompts.xml view
                        LayoutInflater li = LayoutInflater.from(getApplicationContext());
                        View dialogView = li.inflate(R.layout.dialog_sesi_pasien_kunjungan_ulang, null);

                        if (dialogView instanceof TextView) {
                            setTypeFace((TextView) v);
                        }
                        dialog.dismiss();

                        final Dialog dialog2;
                        dialog2 = new Dialog(context);
                        dialog2.requestWindowFeature(Window.FEATURE_NO_TITLE);
                        dialog2.setContentView(dialogView);
                        //make transparent dialog_sesi_offline_pelod
                        dialog2.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                        dialog2.show();
                        Button dialogLanjut = (Button) dialogView.findViewById(R.id.ok);
                        final EditText edt = (EditText) dialogView.findViewById(R.id.edt1);
                        final EditText edt2 = (EditText) dialogView.findViewById(R.id.edt2);
                        final EditText edt3 = (EditText) dialogView.findViewById(R.id.edt3);
                        final TextView id_status = (TextView) dialogView.findViewById(R.id.id_status);

                        edt.addTextChangedListener(new TextWatcher() {
                            CharSequence seq;

                            @Override
                            public void onTextChanged(CharSequence s, int start, int count, int after) {seq = s;}

                            @Override
                            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

                            @Override
                            public void afterTextChanged(Editable arg0) {
                                id_status.setVisibility(View.VISIBLE);
                                findPasien(seq.toString().trim(), id_status);

                            }
                        });


                        dialogLanjut.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                if (edt.getText().toString().equals("") ||
                                        edt2.getText().toString().equals("") ||
                                        edt3.getText().toString().equals("")) {
                                    dialog(getResources().getString(R.string.IsiSeluruhIsian), getResources().getString(R.string.Perhatian));
                                } else {
                                    editSesi(edt.getText().toString(), edt2.getText().toString(), edt3.getText().toString());
                                    dialog2.dismiss();
                                }
                            }
                        });
                    }
                });
            } else {
                String msg = "Kode Pasien : " + preferences.getString(Preferences.ID, "") + "\n" +
                        "Tanggal Lahir : " + preferences.getString(Preferences.BIRTH_DAY, "") + "\n" +
                        "Tinggi Badan : " + preferences.getString(Preferences.TINGGI, "") + " cm\n" +
                        "Berat Badan : " + preferences.getString(Preferences.BERAT, "") + " Kg";
                dialogEmail(msg, "Sesi Pasien", preferences.getString(Preferences.ID, ""));
            }
        } else if (v == btn_reset) {
            // get prompts.xml view
            LayoutInflater li = LayoutInflater.from(getApplicationContext());
            View dialogView = li.inflate(R.layout.dialog_reset_sesi_pasien, null);
            final Dialog dialog2;
            dialog2 = new Dialog(context);
            dialog2.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog2.setContentView(dialogView);
            //make transparent dialog_sesi_offline_pelod
            dialog2.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            dialog2.show();

            Button ya = (Button) dialogView.findViewById(R.id.ya);
            Button tidak = (Button) dialogView.findViewById(R.id.tidak);

            ya.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog2.dismiss();
                    editor.putString(Preferences.ID, "");
                    editor.putString(Preferences.BERAT, "");
                    editor.putString(Preferences.TINGGI, "");
                    editor.putString(Preferences.JENIS_KELAMIN, "");
                    editor.putString(Preferences.BIRTH_DAY, "");
                    editor.commit();
                    dialog(getResources().getString(R.string.SesiPasienReset), getResources().getString(R.string.Pesan));
                    if (preferences.getString(Preferences.ID, "").equals("")) {
                        btn_reset.setVisibility(View.GONE);
                        kode_pasien.setVisibility(View.GONE);
                    } else {
                        btn_reset.setVisibility(View.VISIBLE);
                        kode_pasien.setVisibility(View.VISIBLE);
                        kode_pasien.setText(getResources().getString(R.string.Kodesemicolon) + preferences.getString(Preferences.ID, ""));
                    }
                }
            });

            tidak.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog2.dismiss();
                }
            });

        } else if (v == profile) {
            sendAnalytic("Profile");
            Intent i = new Intent(getApplicationContext(), ProfileActivity.class);
            startActivity(i);
            finish();
        } else if (v == tanya_jawab) {
            sendAnalytic("Tanya jawab");
            Intent i = new Intent(getApplicationContext(), QuestionAndAnswerActivity.class);
            startActivity(i);
            finish();
        } else if (v == logout) {
            // get prompts.xml view
            LayoutInflater li = LayoutInflater.from(getApplicationContext());
            View dialogView = li.inflate(R.layout.dialog_logout, null);
            final Dialog dialog;
            dialog = new Dialog(context);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(dialogView);
            //make transparent dialog_sesi_offline_pelod
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            dialog.show();

            Button ya = (Button) dialogView.findViewById(R.id.ya);
            Button tidak = (Button) dialogView.findViewById(R.id.tidak);

            ya.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                    db.truncateAgenda();
                    db.truncateJadwal();
                    deleteCalendar();
                    editor.clear();
                    editor.putString(Preferences.ISLOGOUT, "LOGOUT");
                    editor.commit();

                    //Intent i = new Intent(getApplicationContext(), BumperActivity.class);
                    Intent i = new Intent(getApplicationContext(), SplashPagerActivity.class);
                    startActivity(i);
                    finish();
                }
            });

            tidak.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });

        } else if (v == pengaturan) {
            Intent i = new Intent(getApplicationContext(), SettingActivity.class);
            startActivity(i);
            finish();
        } else if (v == iv_notif) {
            Intent i = new Intent(getApplicationContext(), NotifikasiActivity.class);
            startActivity(i);
            finish();
        } else if (v == beranda) {
            mDrawerLayout.closeDrawer(navdrawer);
        } else if (v == kode_pasien_side_menu) {
            mDrawerLayout.closeDrawer(navdrawer);
        } else if (v == skor_alvarado) {
            getArtikel("ALVARADO");
        } else if (v == tanda_vital) {
            getArtikel("VITAL");
        }

    }

    private void showNellhausDialog() {
        LayoutInflater li = LayoutInflater.from(getApplicationContext());
        View dialogView = li.inflate(R.layout.dialog_offline_nellhaus, null);
        final Dialog dialog3;
        dialog3 = new Dialog(context);
        dialog3.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog3.setContentView(dialogView);
        //make transparent dialog_sesi_offline_pelod
        dialog3.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog3.show();


        Button dialogLanjut = (Button) dialogView.findViewById(R.id.ok);
        final RadioButton rad1 = (RadioButton) dialogView.findViewById(R.id.rad1);
        final RadioButton rad2 = (RadioButton) dialogView.findViewById(R.id.rad2);
        rad1.setChecked(true);

        rad1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rad1.setChecked(true);
                rad2.setChecked(false);
            }
        });

        rad2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rad2.setChecked(true);
                rad1.setChecked(false);
            }
        });

        final TextView edt = (TextView) dialogView.findViewById(R.id.tglLahirTxt);
        final EditText edt2 = (EditText) dialogView.findViewById(R.id.lingkarKepalaTxt);


        edt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDatepickerDialog(edt);
            }
        });
        dialogLanjut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (edt.getText().toString().equals("") ||
                        edt2.getText().toString().equals("")) {
                    dialog(getResources().getString(R.string.IsiSeluruhIsian), getResources().getString(R.string.Perhatian));
                } else {
                    String jk;
                    if (rad1.isChecked()) {
                        jk = "m";
                    } else if (rad2.isChecked()) {
                        jk = "f";
                    } else {
                        jk = "";
                    }
                    editor.putString(Preferences.JENIS_KELAMIN, jk);
                    editor.putString(Preferences.BIRTH_DAY, edt.getText().toString());
                    editor.putString(Preferences.LINGKAR_KEPALA, edt2.getText().toString());
                    editor.commit();
                    //saveSesiPasien(jk, edt.getText().toString(), edt3.getText().toString(), edt2.getText().toString());
                    dialog3.dismiss();
                    showNellhausOptionDialog();
                }
            }
        });
    }

    private void showNellhausOptionDialog() {
        LayoutInflater li = LayoutInflater.from(getApplicationContext());
        View dialogView = li.inflate(R.layout.dialog_offline_nellhaus_option, null);
        final Dialog dialog3;
        dialog3 = new Dialog(context);
        dialog3.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog3.setContentView(dialogView);
        //make transparent dialog_sesi_offline_pelod
        dialog3.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog3.show();


        Button dialogLanjut = (Button) dialogView.findViewById(R.id.ok);
        final RadioButton rad1 = (RadioButton) dialogView.findViewById(R.id.rad1);
        final RadioButton rad2 = (RadioButton) dialogView.findViewById(R.id.rad2);
        rad1.setChecked(true);

        rad1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rad1.setChecked(true);
                rad2.setChecked(false);
            }
        });

        rad2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rad2.setChecked(true);
                rad1.setChecked(false);
            }
        });


        dialogLanjut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                    String jk;
                    if (rad1.isChecked()) {
                        jk = "1";
                        System.out.println("nandha WHO");
                        lingkarKepala(1);
                    } else if (rad2.isChecked()) {
                        jk = "0";
                        lingkarKepala(2);
                        System.out.println("nandha Nellhaus");
                    } else {
                        jk = "";
                    }
                    //saveSesiPasien(jk, edt.getText().toString(), edt3.getText().toString(), edt2.getText().toString());
                    dialog3.dismiss();
                    //showNellhausOptionDialog();
            }
        });
    }

    private void lingkarKepala(final int option) {
        //  progresDialog(false, "Memuat Grafik");


        selected = new ArrayList<String>();
        grafikName = new ArrayList<String>();
        interpretasi = new ArrayList<String>();
        label = new ArrayList<String>();


        final String lingkarKepala = preferences.getString(Preferences.LINGKAR_KEPALA, "");

        if(!preferences.getString(Preferences.USER_ONLINE, "").equalsIgnoreCase("false")) {

            String id = preferences.getString(Preferences.ID, "");
            if (option == 1) {
                StringRequest listdata = new StringRequest(Request.Method.GET,
                        ApiReferences.generateLingkarKepalaTUmur(id, lingkarKepala),
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                try {
                                    if (response.equals("[]")) {
                                        dialog(getResources().getString(R.string.GrafikBelumTersedia), getResources().getString(R.string.Perhatian));
                                    } else {
                                        Message msg = Parser.getMessage(response);
                                        if (msg.getStatus().equals("0")) {
                                            dialog(msg.getMessages(), getResources().getString(R.string.Perhatian));
                                        } else {
                                            Log.i("RESULT JSON", response.toString());
                                            Grafik graf = new Grafik();
                                            graf = Parser.getGrafik(response
                                                    .toString());
                                            if (graf.getStatus().equals("1")) {
                                                selected.add(graf.getObjects().getImage_url());
                                                interpretasi.add(graf.getObjects().getInterpretation());
                                                if (option == 1) {
                                                    label.add(graf.getObjects().getLabel());
                                                }else{
                                                    label.add("NELLHAUS");
                                                }
                                                grafikName.add(getResources().getString(R.string.grafikLingkarKepalaUmur));
                                            }

                                            Intent i = new Intent(MainActivity.this, GrafikPertumbuhanResultActivity.class);
                                            i.putStringArrayListExtra("page", selected);
                                            i.putStringArrayListExtra("title", grafikName);
                                            i.putStringArrayListExtra("interpretasi", interpretasi);
                                            i.putStringArrayListExtra("label", label);
                                            i.putExtra("result", result);
                                            startActivity(i);
                                            //    finish();
                                        }
                                    }
                                } catch (JsonSyntaxException e) {
                                    dialog.dismiss();
                                    dialog(getResources().getString(R.string.KesalahanServer), getResources().getString(R.string.Perhatian));
                                }
                            }
                        }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        dialog.dismiss();
                        dialog(getResources().getString(R.string.NoConnection), getResources().getString(R.string.Perhatian));
                    }
                });
                listdata.setRetryPolicy(new DefaultRetryPolicy(
                        20000,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                mRequest.add(listdata);
            } else if (option == 2) {
                String uri;
                uri = ApiReferences.generateLingkarKepalaTUmurNellhausOffline();
            } else {
                //TODO: error response
            }
        }else{
            String uri;
            if (option == 1) {
                Log.d("dbgbg", "offline who");
                uri = ApiReferences.generateLingkarKepalaTUmurOffline();
            } else if (option == 2) {
                uri = ApiReferences.generateLingkarKepalaTUmurNellhausOffline();
            } else {
                //TODO: error response
                uri = "";
            }
            StringRequest listdata = new StringRequest(Request.Method.POST,
                    uri,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try {
                                if (response.equals("[]")) {
                                    dialog(getResources().getString(R.string.GrafikBelumTersedia), getResources().getString(R.string.Perhatian));
                                } else {
                                    Message msg = Parser.getMessage(response);
                                    if (msg.getStatus().equals("0")) {
                                        dialog(msg.getMessages(), getResources().getString(R.string.Perhatian));
                                    } else {
                                        Log.i("RESULT JSON", response.toString());
                                        Grafik graf = new Grafik();
                                        graf = Parser.getGrafik(response
                                                .toString());
                                        if (graf.getStatus().equals("1")) {
                                            selected.add(graf.getObjects().getImage_url());
                                            interpretasi.add(graf.getObjects().getInterpretation());
                                            if (option == 1) {
                                                label.add(graf.getObjects().getLabel());
                                            }else{
                                                label.add("NELLHAUS");
                                            }
                                            grafikName.add(getResources().getString(R.string.grafikLingkarKepalaUmur));
                                        }
                                        Intent i = new Intent(MainActivity.this, GrafikPertumbuhanResultActivity.class);
                                        i.putStringArrayListExtra("page", selected);
                                        i.putStringArrayListExtra("title", grafikName);
                                        i.putStringArrayListExtra("interpretasi", interpretasi);
                                        i.putStringArrayListExtra("label", label);
                                        i.putExtra("result", result);
                                        startActivity(i);
                                        //    finish();
                                    }
                                }
                            } catch (JsonSyntaxException e) {
                                dialog.dismiss();
                                dialog(getResources().getString(R.string.KesalahanServer), getResources().getString(R.string.Perhatian));
                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                    NetworkResponse response = error.networkResponse;
                    if (error instanceof ServerError && response != null) {
                        try {
                            String res = new String(response.data,
                                    HttpHeaderParser.parseCharset(response.headers, "utf-8"));
                            // Now you can use any deserializer to make sense of data
                            JSONObject obj = new JSONObject(res);
                            dialog(res.toString(), getResources().getString(R.string.Perhatian));
                        } catch (UnsupportedEncodingException e1) {
                            // Couldn't properly decode data to string
                            e1.printStackTrace();
                        } catch (JSONException e2) {
                            // returned data is not JSONObject?
                            e2.printStackTrace();
                        }
                    }


                }
            })
            {

                protected Map<String, String> getParams() {
                    HashMap<String, String> params = new HashMap<String, String>();
                    params.put("dob", preferences.getString(Preferences.BIRTH_DAY, ""));
                    params.put("gender", preferences.getString(Preferences.JENIS_KELAMIN, ""));
                    params.put("head_circumference", lingkarKepala);
                    return params;

                }
            };
            listdata.setRetryPolicy(new DefaultRetryPolicy(
                    20000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            mRequest.add(listdata);

        }

    }

    private void showDatepickerDialog(final TextView txt) {
        dateHelper.showDatePicker(this, System.currentTimeMillis(), new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                int _year = year;
                int _month = monthOfYear + 1;
                int _day = dayOfMonth;
                String date = _year + "-" + _month + "-" + _day;
                String birthDate = dateHelper.formatDate(date);
                txt.setText(birthDate);
            }
        });
    }


    @Override
    public int getLayout() {
        return R.layout.activity_main;
    }

    @Override
    public void updateUI() {
        if (preferences.getString(Preferences.ID, "").equals("1")) {
            editor.putString(Preferences.BIRTH_DAY, "");
            editor.putString(Preferences.JENIS_KELAMIN, "");
            editor.putString(Preferences.BERAT, "");
            editor.putString(Preferences.TINGGI, "");
            editor.putString(Preferences.ID, "");
            editor.commit();
        }

        if (preferences.getString(Preferences.ID, "").equals("")) {
            btn_reset.setVisibility(View.GONE);
            kode_pasien.setVisibility(View.GONE);
        } else {
            btn_reset.setVisibility(View.VISIBLE);
            kode_pasien.setVisibility(View.VISIBLE);
            kode_pasien.setText(getResources().getString(R.string.Kodesemicolon) + preferences.getString(Preferences.ID, ""));
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    private void setAnim(View v) {
        YoYo.with(Techniques.SlideInDown)
                .duration(500)
                .playOn(v);
    }

    private void setTypeFace(TextView tv) {
        Typeface tf = Typeface.createFromAsset(getApplicationContext().getAssets(), "BREVIA-SEMIBOLD.OTF");
        tv.setTypeface(tf);
    }


    private void showDatePickerForBirthDay(final TextView txt) {
        dateHelper.showDatePicker(MainActivity.this, System.currentTimeMillis(), new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                int _year = year;
                int _month = monthOfYear + 1;
                int _day = dayOfMonth;
                String date = _year + "-" + _month + "-" + _day;
                String birthDate = dateHelper.formatDate(date);
                txt.setText(birthDate);
            }
        });
    }


    private void editSesi(final String id, final String tinggi, final String berat) {
        progresDialog(false, getResources().getString(R.string.MemuatDataPasien));
        StringRequest listdata = new StringRequest(Request.Method.POST,
                ApiReferences.editPasien(id),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        dialog.dismiss();
                        try {
                            Log.i("RESULT JSON", response.toString());
                            pasien = Parser.getPasien(response
                                    .toString());
                            if (pasien.getStatus().equalsIgnoreCase("1")) {
                                editor.putString(Preferences.ID, pasien.getObjects().getChild().getId());
                                editor.putString(Preferences.BERAT, pasien.getObjects().getBerat());
                                editor.putString(Preferences.TINGGI, pasien.getObjects().getTinggi());
                                editor.putString(Preferences.JENIS_KELAMIN, pasien.getObjects().getChild().getGender());
                                editor.putString(Preferences.BIRTH_DAY, pasien.getObjects().getChild().getDob());
                                editor.commit();
                                dialog(getResources().getString(R.string.DataPasienBerhasilDimuat), getResources().getString(R.string.Pesan));
                                if (preferences.getString(Preferences.ID, "").equals("")) {
                                    btn_reset.setVisibility(View.GONE);
                                    kode_pasien.setVisibility(View.GONE);
                                } else {
                                    btn_reset.setVisibility(View.VISIBLE);
                                    kode_pasien.setVisibility(View.VISIBLE);
                                    kode_pasien.setText(getResources().getString(R.string.Kodesemicolon) + preferences.getString(Preferences.ID, ""));
                                }
                            }
                        } catch (JsonSyntaxException e) {
                            dialog(getResources().getString(R.string.KesalahanServer), getResources().getString(R.string.Perhatian));
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                NetworkResponse response = error.networkResponse;
                if (error instanceof ServerError && response != null) {
                    try {
                        String res = new String(response.data,
                                HttpHeaderParser.parseCharset(response.headers, "utf-8"));
                        // Now you can use any deserializer to make sense of data
                        JSONObject obj = new JSONObject(res);
                        dialog(getResources().getString(R.string.NoConnection), getResources().getString(R.string.Perhatian));
                    } catch (UnsupportedEncodingException e1) {
                        // Couldn't properly decode data to string
                        e1.printStackTrace();
                    } catch (JSONException e2) {
                        // returned data is not JSONObject?
                        e2.printStackTrace();
                    }
                }

            }
        }) {

            protected Map<String, String> getParams() {
                HashMap<String, String> params = new HashMap<String, String>();
                params.put("weight", berat);
                params.put("height", tinggi);
                params.put("head_circumference", "");

                return params;
            }
        };
        listdata.setRetryPolicy(new DefaultRetryPolicy(
                20000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        mRequest.add(listdata);
    }


    private void saveSesiPasien(final String jenis_kelamin, final String tanggal_lahir, final String berat, final String tinggi, final String alarm) {
        progresDialog(false, getResources().getString(R.string.MenyimpanDataPasien));
        isalar = alarm;

        StringRequest postlogin = new StringRequest(Request.Method.POST,
                ApiReferences.insertPasien(), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    Log.i("RESULT JSON", response.toString());
                    pasien = Parser.getPasien(response
                            .toString());
                    //TODO put proper value for patient
                    if (pasien.getStatus().equalsIgnoreCase("1")) {
                        editor.putString(Preferences.ID, pasien.getObjects().getId());
                        //editor.putString(Preferences.BERAT, pasien.getObjects().getBerat());
                        //editor.putString(Preferences.TINGGI, pasien.getObjects().getTinggi());
                        editor.putString(Preferences.BERAT, berat);
                        editor.putString(Preferences.TINGGI, tinggi);
                        editor.putString(Preferences.JENIS_KELAMIN, pasien.getObjects().getJenis_kelamin());
                        editor.putString(Preferences.BIRTH_DAY, pasien.getObjects().getTanggal_lahir());
                        editor.commit();
                        dialog.dismiss();
                        if (preferences.getString(Preferences.ID, "").equals("")) {
                            btn_reset.setVisibility(View.GONE);
                            kode_pasien.setVisibility(View.GONE);
                        } else {
                            btn_reset.setVisibility(View.VISIBLE);
                            kode_pasien.setVisibility(View.VISIBLE);
                            kode_pasien.setText(getResources().getString(R.string.Kodesemicolon) + preferences.getString(Preferences.ID, ""));
                        }

                        sendAnalytic(getResources().getString(R.string.SesiPasien));

                        //save schedule
//                    pasienModel.save(pasien);
                        dialogEmail(getResources().getString(R.string.Kodesemicolon) + pasien.getObjects().getId() + getResources().getString(R.string.SimpanKodePemeriksaan), getResources().getString(R.string.PendaftaranSesiPasienBerhasil), pasien.getObjects().getId());
                    }
                } catch (JsonSyntaxException e) {
                    dialog(getResources().getString(R.string.KesalahanServer), getResources().getString(R.string.Perhatian));
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                NetworkResponse response = error.networkResponse;
                if (error instanceof ServerError && response != null) {
                    try {
                        String res = new String(response.data,
                                HttpHeaderParser.parseCharset(response.headers, "utf-8"));
                        // Now you can use any deserializer to make sense of data
                        JSONObject obj = new JSONObject(res);
                        dialog(res.toString(), getResources().getString(R.string.Perhatian));
                    } catch (UnsupportedEncodingException e1) {
                        // Couldn't properly decode data to string
                        e1.printStackTrace();
                    } catch (JSONException e2) {
                        // returned data is not JSONObject?
                        e2.printStackTrace();
                    }
                }
                dialog.dismiss();
            }
        }) {

            protected Map<String, String> getParams() {
                HashMap<String, String> params = new HashMap<String, String>();
                params.put("name", preferences.getString(Preferences.NAME, ""));
                params.put("gender", jenis_kelamin);
                params.put("dob", tanggal_lahir);
                params.put("weight", berat);
                params.put("height", tinggi);
                params.put("head_circumference", "");
                params.put("user_id", preferences.getString(Preferences.ORANGTUA_ID, ""));

                return params;
            }
        };
        postlogin.setRetryPolicy(new DefaultRetryPolicy(
                20000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        mRequest.add(postlogin);
    }


    private void getMenuAge(final String code) {
        progresDialog(false, getResources().getString(R.string.MemuatData));
        StringRequest listdata = new StringRequest(Request.Method.GET,
                ApiReferences.getMenuAge(code,preferences.getString(Preferences.LANGUAGE, "")),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        dialog.dismiss();
                        if (response.equals("[]")) {
                            dialog(getResources().getString(R.string.BelumAdaData), getResources().getString(R.string.Pesan));
                        } else {
                            try {
                                Message msg = Parser.getMessage(response);
                                if (msg.getStatus().equals("1")) {
                                    Log.i("RESULT JSON", response.toString());

                                    MenuAge menu = Parser.getMenuAge(response);

                                    ArrayList<MenuAge.Menu> list = db.getMenuAge(code);
                                    if (list.size() > 0) {
                                        db.deleteMenuAge(code);
                                        for (int i = 0; i < menu.getObjects().size(); i++) {
                                            db.insertMenuAge(menu.getObjects().get(i));
                                        }
                                    } else {
                                        for (int i = 0; i < menu.getObjects().size(); i++) {
                                            db.insertMenuAge(menu.getObjects().get(i));
                                        }
                                    }
                                    intentTo2(code, response);

                                } else {
                                    dialog(msg.getMessages(), getResources().getString(R.string.Pesan));
                                }
                            } catch (JsonSyntaxException e) {
                                dialog(getResources().getString(R.string.KesalahanServer), getResources().getString(R.string.Perhatian));
                            }
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dialog.dismiss();
                ArrayList<MenuAge.Menu> list = db.getMenuAge(code);
                if (list.size() > 0) {
                    MenuAge menu = new MenuAge();
                    menu.setObjects(list);
                    db.deleteMenuAge(code);
                    Gson gs = new Gson();
                    intentTo2(code, gs.toJson(menu));
                } else {
                    dialog(getResources().getString(R.string.NoConnection), getResources().getString(R.string.Perhatian));
                }

            }
        });
        listdata.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        mRequest.add(listdata);
    }


    private void getArtikel(final String code) {
        progresDialog(false, getResources().getString(R.string.MemuatData));
        StringRequest listdata = new StringRequest(Request.Method.GET,
                ApiReferences.getArticle(code,preferences.getString(Preferences.LANGUAGE, "")),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        dialog.dismiss();
                        if (response.equals("[]")) {
                            dialog(getResources().getString(R.string.GrafikBelumTersedia), getResources().getString(R.string.Perhatian));
                        } else {
                            try {
                                Message msg = Parser.getMessage(response);
                                if (msg.getStatus().equals("0")) {
                                    dialog(msg.getMessages(), getResources().getString(R.string.Perhatian));
                                } else {

                                    Log.i("RESULT JSON", response.toString());
                                    Artikel artikel = Parser.getArtikel(response.toString());


                                    db.deleteArtikel(code);
                                    for (int i = 0; i < artikel.getObjects().size(); i++) {
                                        db.insertArtikel(artikel.getObjects().get(i), code);
                                    }

                                    intentTo(code, response);


                                }
                            } catch (JsonSyntaxException e) {
                                dialog(getResources().getString(R.string.KesalahanServer), getResources().getString(R.string.Perhatian));
                            }
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dialog.dismiss();
                //     dialog(getResources().getString(R.string.NoConnection), getResources().getString(R.string.Perhatian));

                Artikel temp = db.getArtikels(code);
                if (temp.getObjects().size() > 0) {
                    Gson gs = new Gson();
                    intentTo(code, gs.toJson(temp));
                } else {
                    dialog(getResources().getString(R.string.NoConnection), getResources().getString(R.string.Perhatian));
                }
            }
        });
        listdata.setRetryPolicy(new DefaultRetryPolicy(
                20000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        mRequest.add(listdata);

    }


    private void getTable() {
        progresDialog(false, getResources().getString(R.string.MemuatData));
        StringRequest listdata = new StringRequest(Request.Method.POST,
                ApiReferences.postImunisasi(preferences.getString(Preferences.ID, "")),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.i("RESULT JSON", response.toString());
                        dialog.dismiss();
                        Message msg = Parser.getMessage(response);
                        if (msg.getStatus().equals("1")) {
                            Intent i = new Intent(getApplicationContext(), TabelImunisasiActivity.class);
                            i.putExtra("respon", response);
                            startActivity(i);
                            finish();
                        } else {
                            dialog(msg.getMessages(), getResources().getString(R.string.Perhatian));
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //dialog.dismiss();
                //dialog(error.toString(), getResources().getString(R.string.Perhatian));
                Intent i = new Intent(getApplicationContext(), TabelImunisasiActivity.class);
                startActivity(i);
                finish();
            }
        }) {

            protected Map<String, String> getParams() {
                HashMap<String, String> params = new HashMap<String, String>();
                return params;
            }
        };
        listdata.setRetryPolicy(new DefaultRetryPolicy(
                20000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        mRequest.add(listdata);
    }


    private void progresDialog(boolean horizontal, String msg) {
        dialog = new MaterialDialog.Builder(MainActivity.this)
                .title(msg)
                .content(getResources().getString(R.string.MohonTunggu))
                .progress(true, 0)
                .progressIndeterminateStyle(horizontal)
                .cancelable(false)
                .show();
    }


    public void dialog(String content, String msg) {
        new MaterialDialog.Builder(MainActivity.this)
                .title(msg)
                .content(content)
                .positiveText("Ok")
                .callback(new MaterialDialog.ButtonCallback() {
                    @Override
                    public void onPositive(MaterialDialog dialog) {
                    }

                })
                .show();
    }

    public void dialogEmail(String content, String msg, final String kode) {
        new MaterialDialog.Builder(MainActivity.this)
                .title(msg)
                .content(content)
                .autoDismiss(false)
                .positiveText(getResources().getString(R.string.KirimEmail))
                .negativeText(getResources().getString(R.string.tutup))
                .cancelable(false)
                .callback(new MaterialDialog.ButtonCallback() {
                    @Override
                    public void onPositive(MaterialDialog dialog) {
                        Intent sendEmail = new Intent(Intent.ACTION_SEND);
                        sendEmail.setType("text/plain");
                        sendEmail.putExtra(Intent.EXTRA_EMAIL, new String[]{""});
                        sendEmail.putExtra(Intent.EXTRA_SUBJECT, "Kode Pasien");
                        sendEmail.putExtra(Intent.EXTRA_TEXT, "Berikut ini adalah kode pasien anda.\nKode : " + kode);
                        startActivity(Intent.createChooser(sendEmail, "Email With"));
                    }

                    @Override
                    public void onNegative(MaterialDialog dialog) {
                        if (!isalar.equals("")) {
                            getTable();
                        } else {
                            dialog.dismiss();
                        }

                    }
                })
                .show();
    }


    void intentTo(String code, String response) {
        sendAnalytic(code);
        if (code.equalsIgnoreCase("GRAFIK")) {
            Intent i;
            i = new Intent(MainActivity.this, ArtikelPagerGrafikActivity.class);
            i.putExtra("result", response);
            startActivity(i);
            finish();
        }  else if (code.equalsIgnoreCase("GRAFIK_AWAM")) {
            Intent i;
            i = new Intent(MainActivity.this, ArtikelPagerGrafikActivity.class);
            i.putExtra("result", response);
            startActivity(i);
            finish();
        }  else if (code.equalsIgnoreCase("FEN")) {
            Intent i;
            i = new Intent(MainActivity.this, ArtikelPagerGrafikFentonActivity.class);
            i.putExtra("result", response);
            startActivity(i);
            finish();
        } else if (code.equalsIgnoreCase("MAR")) {
            Intent i = new Intent(getApplicationContext(), ArtikelPagerRedFlagActivity.class);
            i.putExtra("result", response);
            startActivity(i);
            finish();
        } else if (code.equals("PGCS")) {
            Intent i = new Intent(getApplicationContext(), ArtikelPagerPgcsActivity.class);
            i.putExtra("result", response);
            startActivity(i);
            finish();
        }else if (code.equals("PSC17")) {
            Intent i = new Intent(getApplicationContext(), ArtikelPagerPsc17Activity.class);
            i.putExtra("result", response);
            startActivity(i);
            finish();
        } else if (code.equals("PELODS")) {
            Intent i = new Intent(getApplicationContext(), ArtikelPagerPelodsActivity.class);
            i.putExtra("result", response);
            startActivity(i);
            finish();
        } else if (code.equals("NBS")) {
            Intent i = new Intent(getApplicationContext(), ArtikelPagerBalardActivity.class);
            i.putExtra("result", response);
            startActivity(i);
            finish();
        } else if (code.equals("BPC")) {
            Intent i = new Intent(getApplicationContext(), ArtikelPagerBloodPressureActivity.class);
            i.putExtra("result", response);
            startActivity(i);
            finish();
        } else if (code.equals("PPA")) {
            Intent i = new Intent(getApplicationContext(), ArtikelPagerTahapPubertasActivity.class);
            i.putExtra("result", response);
            startActivity(i);
            finish();
        } else if (code.equals("BMR")) {
            Intent i = new Intent(getApplicationContext(), ArtikelPagerKalkulatorActivity.class);
            i.putExtra("result", response);
            startActivity(i);
            finish();
        } else if (code.equals("KALORISEHARI")) {
            Intent i = new Intent(getApplicationContext(), ArtikelPagerKaloriSehariActivity.class);
            i.putExtra("result", response);
            startActivity(i);
            finish();
        }else if (code.equalsIgnoreCase("LFG")) {
            Intent i = new Intent(getApplicationContext(), ArtikelPagerLfgActivity.class);
            i.putExtra("result", response);
            startActivity(i);
            finish();
        } else if (code.equalsIgnoreCase("KMME")) {
            Intent i = new Intent(getApplicationContext(), ArtikelPagerKmmeActivity.class);
            i.putExtra("result", response);
            startActivity(i);
            finish();
        } else if (code.equalsIgnoreCase("GPPH")) {
            Intent i = new Intent(getApplicationContext(), ArtikelPagerGpphActivity.class);
            i.putExtra("result", response);
            startActivity(i);
            finish();
        } else if (code.equalsIgnoreCase("KPSP")) {
            Intent i = new Intent(getApplicationContext(), ArtikelPagerKpspActivity.class);
            i.putExtra("result", response);
            startActivity(i);
            finish();
        } else if (code.equalsIgnoreCase("ALVARADO")) {
            Intent i;
            i = new Intent(MainActivity.this, ArtikelPagerAlvaradoActivity.class);
            i.putExtra("result", response);
            startActivity(i);
            finish();
        } else if (code.equalsIgnoreCase("VITAL")) {
            Intent i;
            i = new Intent(MainActivity.this, ArtikelPagerTandaVitalActivity.class);
            i.putExtra("result", response);
            startActivity(i);
            finish();
        }
    }


    void intentTo2(String code, String response) {
        sendAnalytic(code);
        if (code.equalsIgnoreCase("MEOT")) {
            Intent i = new Intent(getApplicationContext(), EdukasiOrangtuaActivity.class);
            i.putExtra("respon", response.toString());
            startActivity(i);
            finish();
        } else if (code.equalsIgnoreCase("TPA-SP")) {
            Intent i = new Intent(getApplicationContext(), StimulasiPerkembanganActivity.class);
            i.putExtra("respon", response.toString());
            startActivity(i);
            finish();
        } else if (code.equalsIgnoreCase("MU")) {
            Intent i = new Intent(getApplicationContext(), MasalahUmumActivity.class);
            i.putExtra("respon", response.toString());
            startActivity(i);
            finish();
        } else if (code.equalsIgnoreCase("KSKD")) {
            Intent i = new Intent(getApplicationContext(), KuesionerSebelumKedokterActivity.class);
            i.putExtra("respon", response.toString());
            startActivity(i);
            finish();
        } else if (code.equalsIgnoreCase("KASA")) {
            Intent i = new Intent(getApplicationContext(), KuesionerKunjunganAnakSakitActivity.class);
            i.putExtra("respon", response.toString());
            startActivity(i);
            finish();
        } else if (code.equalsIgnoreCase("KASE")) {
            Intent i = new Intent(getApplicationContext(), KuesionerKunjunganAnakSehatActivity.class);
            i.putExtra("respon", response.toString());
            startActivity(i);
            finish();
        } else if (code.equalsIgnoreCase("PKM")) {
            Intent i = new Intent(getApplicationContext(), SkriningMedisPenjelasanKondisiActivity.class);
            i.putExtra("respon", response.toString());
            startActivity(i);
            finish();
        } else if (code.equalsIgnoreCase("SU")) {
            Intent i = new Intent(getApplicationContext(), SkriningMedisUtamaActivity.class);
            i.putExtra("respon", response.toString());
            startActivity(i);
            finish();
        }
    }


    void deleteCalendar() {
        try {
            Uri evuri = CalendarContract.Calendars.CONTENT_URI;
            //substitue your calendar id into the 0
            long calid = Long.valueOf(preferences.getString(Preferences.ID_CALENDAR, ""));
            Uri deleteUri = ContentUris.withAppendedId(evuri, calid);
            getContentResolver().delete(deleteUri, null, null);
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
    }

    private void findOrtu(final String email, final TextView email_status) {

        StringRequest postlogin = new StringRequest(Request.Method.POST,
                ApiReferences.getOrangTua(), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    Log.i("RESULT JSON", response.toString());
                    OrangTua ortu = Parser.getOrangTua(response
                            .toString());
                    if (ortu.getStatus().equalsIgnoreCase("1")) {
                        editor.putString(Preferences.ORANGTUA_ID, ortu.getObjects().getId());
                        editor.commit();

                        email_status.setText(ortu.getObjects().getNama() + " " + getResources().getString(R.string.DiTemukan));
                    }
                } catch (JsonSyntaxException e) {
                    dialog(getResources().getString(R.string.KesalahanServer), getResources().getString(R.string.Perhatian));
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                NetworkResponse response = error.networkResponse;
                if (error instanceof ServerError && response != null) {
                    try {
                        String res = new String(response.data,
                                HttpHeaderParser.parseCharset(response.headers, "utf-8"));
                        // Now you can use any deserializer to make sense of data
                        JSONObject obj = new JSONObject(res);
                        email_status.setText(getResources().getString(R.string.OrangtuaTidakDitemukan));
                    } catch (UnsupportedEncodingException e1) {
                        // Couldn't properly decode data to string
                        e1.printStackTrace();
                    } catch (JSONException e2) {
                        // returned data is not JSONObject?
                        e2.printStackTrace();
                    }
                }
            }
        }) {

            protected Map<String, String> getParams() {
                HashMap<String, String> params = new HashMap<String, String>();
                params.put("email", email);
                return params;
            }
        };
        postlogin.setRetryPolicy(new DefaultRetryPolicy(
                20000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        mRequest.add(postlogin);
    }


    private void findPasien(final String idAnak, final TextView idAnakTextView) {

        StringRequest postlogin = new StringRequest(Request.Method.POST,
                ApiReferences.getPasien(), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    Log.i("RESULT JSON", response.toString());
                    Pasien pasien = Parser.getPasien(response
                            .toString());
                    if (pasien.getStatus().equalsIgnoreCase("1")) {
                        editor.putString(Preferences.ID, pasien.getObjects().getId());
                        editor.commit();

                        idAnakTextView.setText(pasien.getObjects().getId()+ getResources().getString(R.string.DiTemukan));
                    }
                } catch (JsonSyntaxException e) {
                    dialog(getResources().getString(R.string.KesalahanServer), getResources().getString(R.string.Perhatian));
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                NetworkResponse response = error.networkResponse;
                if (error instanceof ServerError && response != null) {
                    try {
                        String res = new String(response.data,
                                HttpHeaderParser.parseCharset(response.headers, "utf-8"));
                        // Now you can use any deserializer to make sense of data
                        JSONObject obj = new JSONObject(res);
                        idAnakTextView.setText(getResources().getString(R.string.PasienTidakDitemukan));
                    } catch (UnsupportedEncodingException e1) {
                        // Couldn't properly decode data to string
                        e1.printStackTrace();
                    } catch (JSONException e2) {
                        // returned data is not JSONObject?
                        e2.printStackTrace();
                    }
                }
            }
        }) {

            protected Map<String, String> getParams() {
                HashMap<String, String> params = new HashMap<String, String>();
                params.put("child_id", idAnak);
                return params;
            }
        };
        postlogin.setRetryPolicy(new DefaultRetryPolicy(
                20000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        mRequest.add(postlogin);
    }

    private void getPdfImunisasi(final String ID, final String code) {
        progresDialog(false, getResources().getString(R.string.MemuatData));
        StringRequest listdata = new StringRequest(Request.Method.GET,
                ApiReferences.getPdf(code, ID, preferences.getString(Preferences.LANGUAGE, "")),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        dialog.dismiss();
                        try {
                            if (response.equals("[]")) {
                                dialog(getResources().getString(R.string.BelumAdaData), getResources().getString(R.string.Pesan));
                            } else {
                                Message msg = Parser.getMessage(response);
                                if (msg.getStatus().equals("1")) {
                                    Log.i("RESULT JSON", response.toString());
                                    Pdf pdf = Parser.getPdf(response);
                                    Intent i = new Intent(getApplicationContext(), ImunisasiIdaiPdf.class);
                                    i.putExtra("url", pdf.getObjects().getFilename());
                                    i.putExtra("id", pdf.getObjects().getId());
                                    i.putExtra("menu", pdf.getObjects().getMenu());
                                    i.putExtra("category", pdf.getObjects().getCategory());
                                    startActivity(i);
                                } else {
                                    dialog(msg.getMessages(), getResources().getString(R.string.Pesan));
                                }
                            }
                        } catch (JsonSyntaxException e) {
                            dialog(getResources().getString(R.string.KesalahanServer), getResources().getString(R.string.Perhatian));
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dialog.dismiss();
                Pdf pdf = db.getFilesObject(Integer.valueOf(ID));
                if (!pdf.getObjects().equals("") && pdf.getObjects() != null) {
                    Intent i = new Intent(getApplicationContext(), MchatrfPdfActivity.class);
                    i.putExtra("url", pdf.getObjects().getFilename());
                    i.putExtra("id", pdf.getObjects().getId());
                    i.putExtra("menu", pdf.getObjects().getMenu());
                    i.putExtra("category", pdf.getObjects().getCategory());
                    startActivity(i);
                } else {
                    dialog(getResources().getString(R.string.NoConnection), getResources().getString(R.string.Perhatian));
                }
            }
        });
        listdata.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        mRequest.add(listdata);
    }

}
