package com.prima.activities;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;

import com.prima.MainActivity;
import com.prima.Preferences;
import com.prima.controllers.ApiReferences;
import com.prima.controllers.Parser;
import com.prima.entities.Message;
import com.prima.entities.Question;
import com.prima.helpers.DatabaseHandler;
import com.prima.helpers.DateHelper;
import prima.test.prima.R;

/**
 * Created by Codelabs on 06/08/2015.
 */
public class PgcsActivity extends BaseActivity implements View.OnClickListener {
    private LinearLayout box;
    private ImageView close;
    private Button ok;
    private Button source;
    private TextView title;
    MaterialDialog dialog;
    Question question;
    private RequestQueue mRequest;
    private TextView txtSource, content;
    RelativeLayout source_wraper;
    DateHelper dateHelper;
    DatabaseHandler db;

    @Override
    public void initView() {
        dateHelper = new DateHelper();
        db = new DatabaseHandler(context);
        content = (TextView) findViewById(R.id.content);
        source_wraper = (RelativeLayout) findViewById(R.id.source_wraper);
        close = (ImageView) findViewById(R.id.close);
        source = (Button) findViewById(R.id.btn_source);
        txtSource = (TextView) findViewById(R.id.source);
        txtSource.setVisibility(View.GONE);
        title = (TextView) findViewById(R.id.title);
        mRequest = Volley.newRequestQueue(getApplicationContext());
        box = (LinearLayout) findViewById(R.id.box);
        close = (ImageView) findViewById(R.id.close);
        ok = (Button) findViewById(R.id.lanjut);

        title = (TextView) findViewById(R.id.title);
        //set font
        Typeface tf = Typeface.createFromAsset(getApplicationContext().getAssets(), "Brevia-ExtraBlackItalic.otf");
        title.setTypeface(tf);


    }

    @Override
    public void setUICallbacks() {
        ok.setOnClickListener(this);
        close.setOnClickListener(this);
        source.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        if (v == ok) {
            if (preferences.getString(Preferences.ID, "").equalsIgnoreCase("") || preferences.getString(Preferences.JENIS_KELAMIN, "").equalsIgnoreCase("")) {
                // get prompts.xml view
                LayoutInflater li = LayoutInflater.from(getApplicationContext());
                View dialogView = li.inflate(R.layout.dialog_sesi_pasien_baru, null);
                final Dialog dialog3;
                dialog3 = new Dialog(context);
                dialog3.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog3.setContentView(dialogView);
                //make transparent dialog_sesi_offline_pelod
                dialog3.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                dialog3.show();


                Button dialogLanjut = (Button) dialogView.findViewById(R.id.ok);
                final RadioButton rad1 = (RadioButton) dialogView.findViewById(R.id.rad1);
                final RadioButton rad2 = (RadioButton) dialogView.findViewById(R.id.rad2);
                rad1.setChecked(true);

                rad1.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        rad1.setChecked(true);
                        rad2.setChecked(false);
                    }
                });

                rad2.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        rad2.setChecked(true);
                        rad1.setChecked(false);
                    }
                });

                final EditText edt = (EditText) dialogView.findViewById(R.id.edt1);
                final LinearLayout lin1 = (LinearLayout) dialogView.findViewById(R.id.lin1);
                final LinearLayout lin2 = (LinearLayout) dialogView.findViewById(R.id.lin2);
                lin1.setVisibility(View.GONE);
                lin2.setVisibility(View.GONE);

                edt.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        showDatepickerDialog(edt);
                    }
                });
                dialogLanjut.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (edt.getText().toString().equals("")) {
                            pdialog2(getResources().getString(R.string.IsiSeluruhIsian), getResources().getString(R.string.Perhatian));
                        } else {
                            String jk;
                            if (rad1.isChecked()) {
                                jk = "1";
                            } else if (rad2.isChecked()) {
                                jk = "0";
                            } else {
                                jk = "";
                            }
                            editor.putString(Preferences.BIRTH_DAY, edt.getText().toString());
                            editor.putString(Preferences.JENIS_KELAMIN, jk);
                            editor.putString(Preferences.ID, "1");
                            editor.commit();
                            getQuestion();
                            //  saveSesiPasien(jk, edt.getText().toString(), edt3.getText().toString(), edt2.getText().toString());
                            dialog3.dismiss();
                        }
                    }
                });

            } else {
                getQuestion();
            }
        } else if (v == close) {
            Intent i = new Intent(getApplicationContext(), MainActivity.class);
            startActivity(i);
            finish();
        } else if (v == source) {
            if (txtSource.getVisibility() == View.GONE) {
                txtSource.setVisibility(View.VISIBLE);
            } else {
                txtSource.setVisibility(View.GONE);
            }
        }
    }


    private void getQuestion() {
        final String code = "PGCS";
        progresDialog(false, getResources().getString(R.string.MemuatData));
        String uri;
        if (preferences.getString(Preferences.ID, "").equals("1")) {
            uri = ApiReferences.getQuestionnaireByBirth(preferences.getString(Preferences.BIRTH_DAY, ""), code, preferences.getString(Preferences.LANGUAGE, ""));
        } else {
            uri = ApiReferences.getQuestion(preferences.getString(Preferences.ID, ""), code, preferences.getString(Preferences.LANGUAGE, ""));
        }

        StringRequest listdata = new StringRequest(Request.Method.GET, uri,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        if (response.equals("[]")) {
                            dialog(getResources().getString(R.string.BelumAdaData), getResources().getString(R.string.Pesan));
                        } else {
                            Message msg = Parser.getMessage(response);
                            if (msg.getStatus().equals("1")) {
                                Log.i("RESULT JSON", response.toString());
                                question = new Question();
                                question = Parser.getQuestion(response);


                                for (int j = 0; j < question.getObjects().size(); j++) {
                                    db.deleteQuestion(question.getObjects().get(j).getId(), question.getObjects().get(j).getMin_age(), question.getObjects().get(j).getMax_age(), "");
                                    db.deleteAnswer(question.getObjects().get(j).getId());
                                    db.insertQuestion(question.getObjects().get(j), code);
                                    for (int k = 0; k < question.getObjects().get(j).getAnswer_option().size(); k++) {
                                        db.insertAnswer(question.getObjects().get(j).getAnswer_option().get(k));
                                    }
                                }

                                Intent i = new Intent(getApplicationContext(), PgcsQuestionActivity.class);
                                i.putExtra("respon", response.toString());
                                startActivity(i);


                            } else {
                                dialog(msg.getMessages(), getResources().getString(R.string.Pesan));
                                if (preferences.getString(Preferences.ID, "").equals("1")) {
                                    editor.putString(Preferences.BIRTH_DAY, "");
                                    editor.putString(Preferences.JENIS_KELAMIN, "");
                                    editor.putString(Preferences.BERAT, "");
                                    editor.putString(Preferences.TINGGI, "");
                                    editor.putString(Preferences.ID, "");
                                    editor.commit();
                                }
                            }
                        }
                        dialog.dismiss();
                    }

                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dialog.dismiss();
                Question temp = db.getQuestion(code, getMonth());
                Gson gson = new Gson();
                Log.i("RESULT JSON2", gson.toJson(temp));
                if (temp.getObjects().size() > 0) {
                    Intent i = new Intent(getApplicationContext(), PgcsQuestionActivity.class);
                    i.putExtra("respon", gson.toJson(temp));
                    startActivity(i);
                } else {
                    dialog(getResources().getString(R.string.NoConnection), getResources().getString(R.string.Perhatian));
                }
            }
        });
        listdata.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        mRequest.add(listdata);
    }


    @Override
    public int getLayout() {
        return R.layout.activity_glasgow_coma_scale;
    }

    @Override
    public void updateUI() {
//        Bundle bn = getIntent().getExtras();
//        if (bn != null) {
//            String result = bn.getString("result");
//            Artikel artikel = Parser.getArtikel(result);
//            content.setText(artikel.getObjects().getDeskripsi());
//            Spannable s = (Spannable) Html.fromHtml("<a href=\"" + artikel.getObjects().getSumber() + "\">" + artikel.getObjects().getSumber() + "</a>");
//            URLSpan[] spans = s.getSpans(0, s.length(), URLSpan.class);
//            for (URLSpan span : spans) {
//                int start = s.getSpanStart(span);
//                int end = s.getSpanEnd(span);
//                s.removeSpan(span);
//                span = new URLSpan(span.getURL());
//                s.setSpan(span, start, end, 0);
//            }
//            txtSource.setText(s);
//            txtSource.setMovementMethod(LinkMovementMethod.getInstance());
//
//            if (artikel.getObjects().getSumber().trim().equals("")) {
//                txtSource.setVisibility(View.GONE);
//                source_wraper.setVisibility(View.GONE);
//                source.setVisibility(View.GONE);
//
//            }
//
//        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

    }


    private void progresDialog(boolean horizontal, String msg) {
        dialog = new MaterialDialog.Builder(PgcsActivity.this)
                .title(msg)
                .content(getResources().getString(R.string.MohonTunggu))
                .progress(true, 0)
                .progressIndeterminateStyle(horizontal)
                .cancelable(false)
                .show();
    }

    public void dialog(String content, String msg) {
        new MaterialDialog.Builder(PgcsActivity.this)
                .title(msg)
                .content(content)
                .positiveText(getResources().getString(R.string.MuatUlang))
                .negativeText(getResources().getString(R.string.Kembali))
                .callback(new MaterialDialog.ButtonCallback() {
                    @Override
                    public void onPositive(MaterialDialog dialog) {
                        getQuestion();
                    }

                    @Override
                    public void onNegative(MaterialDialog dialog) {
                        super.onNegative(dialog);
                    }

                })
                .show();
    }


    public void pdialog2(String content, String msg) {
        new MaterialDialog.Builder(PgcsActivity.this)
                .title(msg)
                .content(content)
                .positiveText("OK")
                .callback(new MaterialDialog.ButtonCallback() {
                    @Override
                    public void onPositive(MaterialDialog dialog) {

                    }

                })
                .show();
    }


    private void showDatepickerDialog(final EditText txt) {
        dateHelper.showDatePicker(this, System.currentTimeMillis(), new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                int _year = year;
                int _month = monthOfYear + 1;
                int _day = dayOfMonth;
                String date = _year + "-" + _month + "-" + _day;
                String birthDate = dateHelper.formatDate(date);
                txt.setText(birthDate);
            }
        });
    }


    @Override
    public void onBackPressed() {
        Intent i = new Intent(getApplicationContext(), MainActivity.class);
        startActivity(i);
        finish();
    }
}
