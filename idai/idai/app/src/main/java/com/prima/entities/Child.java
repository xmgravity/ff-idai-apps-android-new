package com.prima.entities;

/**
 * Created by Codelabs on 23/08/2015.
 */
public class Child extends Message {

        private ObjectChild child;

        public ObjectChild getChild() {
            return child;
        }

        public void setObjects(ObjectChild child) {
            this.child = child;
        }

        public class ObjectChild {
            private String id;
            private String name;
            private String dob;
            private String gender;
            private String height;
            private String weight;

            public String getId() {
                return id;
            }
            public void setId(String id) {
                this.id = id;
            }

            public String getName() {
                return name;
            }
            public void setName(String name) {
                this.name = name;
            }

            public String getDob() {
                return dob;
            }
            public void setDob(String dob) {
                this.dob = dob;
            }

            public String getGender() {
                return gender;
            }
            public void setGender(String gender) {
                this.gender = gender;
            }

            public String getTinggi() { return height; }
            public void setTinggi(String height) { this.height = height
            ; }

            public String getBerat() { return weight; }
            public void setBerat(String weight) { this.weight = weight; }

        }


    }

