package com.prima.fragments;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;

import com.prima.controllers.ApiReferences;
import com.prima.entities.MateriEdukasi;
import prima.test.prima.R;

/**
 * Created by Codelabs on 07/08/2015.
 */
public class EdukasiOrangtuaFragment extends BaseFragment implements View.OnClickListener {

    private Context mContext;
    public static final String ARG_PAGE = "page 2";
    private ViewPager mPager;
    private int i;
    private MateriEdukasi materi;
    private TextView content;
    private ImageView img;
    ProgressBar progres;

    @SuppressLint("ValidFragment")
    public EdukasiOrangtuaFragment() {}
    @SuppressLint("ValidFragment")

    public EdukasiOrangtuaFragment(int i, MateriEdukasi materi) {
        this.i = i;
        this.materi = materi;
    }


    @Override
    public void initView(View view) {
        img = (ImageView) view.findViewById(R.id.img);
        content = (TextView) view.findViewById(R.id.content);
        mContext = getActivity();
        mPager = (ViewPager) getActivity().findViewById(R.id.pager);
        content.setText(materi.getObjects().get(i).getContent());
        progres = (ProgressBar) view.findViewById(R.id.progres);

        LinearLayout ll = (LinearLayout) view.findViewById(R.id.wraperlinear);
        ll.setOrientation(LinearLayout.VERTICAL);

        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);

        layoutParams.setMargins(15, 15, 15, 15);
        ll.setLayoutParams(layoutParams);


        Glide.with(getActivity())
                .load(ApiReferences.getImageUrl() + materi.getObjects().get(i).getHeader_picture())
                .asBitmap()
                .into(new SimpleTarget<Bitmap>() {
                    @Override
                    public void onResourceReady(Bitmap bitmap, GlideAnimation anim) {
                        Bitmap bp = bitmap;

                        img.setImageBitmap(bp);
                        progres.setVisibility(View.GONE);
                    }
                });


    }

    @Override
    public void setUICallbacks() {

    }

    @Override
    public void updateUI() {

    }

    @Override
    public String getPageTitle() {
        return "Account Summary";
    }


    @Override
    public int getFragmentLayout() {
        return R.layout.fragment_edukasi_orangtua;
    }

    @Override
    public void onClick(View v) {

    }


}