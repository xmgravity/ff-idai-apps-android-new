package com.prima.activities;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.prima.adapters.ListHariAdapter;
import com.prima.controllers.Parser;
import com.prima.entities.JadwalPraktek;
import com.prima.entities.JadwalPraktekSingle;
import com.prima.helpers.TimeHelper;

import java.util.ArrayList;

import prima.test.prima.R;

/**
 * Created by Codelabs on 24/08/2015.
 */
public class PraktikPickHariActivity extends BaseActivity implements View.OnClickListener {

    TextView title;
    ImageView close, back;
    ArrayList<String> listItem = new ArrayList<String>();
    ListView list;
    Context mContext;
    EditText jamMulai;
    EditText jamSelesai;
    private TimeHelper timeHelper;
    private JadwalPraktekSingle jdwlPraktek;
    private Button simpan;
    JadwalPraktekSingle jdwl = new JadwalPraktekSingle();


    @Override
    public void initView() {
        title = (TextView) findViewById(R.id.tv_actionbar_title);
        close = (ImageView) findViewById(R.id.close);
        back = (ImageView) findViewById(R.id.back);
        list = (ListView) findViewById(R.id.listView);
        simpan = (Button) findViewById(R.id.simpan);

        close.setVisibility(View.GONE);
        mContext = PraktikPickHariActivity.this;
        //set font
        Typeface tf = Typeface.createFromAsset(getApplicationContext().getAssets(), "BREVIA-BLACK.OTF");
        title.setTypeface(tf);
        title.setText(getResources().getString(R.string.JadwalPraktik2));

        listItem.add("Senin");
        listItem.add("Selasa");
        listItem.add("Rabu");
        listItem.add("Kamis");
        listItem.add("Jumat");
        listItem.add("Sabtu");
        listItem.add("Minggu");


        Bundle bn = getIntent().getExtras();
        if (bn != null) {
            try {
                if (!bn.getString("result").equals("")) {
                    jdwlPraktek = Parser.getJadwalSingle(bn.getString("result"));
                    Log.d("dbgbg", "result");
                }
            } catch (NullPointerException e) {
                e.printStackTrace();
                if (!bn.getString("respon").equals("")) {
                    jdwlPraktek = Parser.getJadwalSingle(bn.getString("respon"));
                    Log.d("dbgbg", "respon");
                }
            }

        }


        ListHariAdapter adapterList = new ListHariAdapter(
                listItem, mContext, jdwlPraktek);
        list.setAdapter(adapterList);
        timeHelper = new TimeHelper();
        jdwl.getObjects().setJadwal(adapterList.getArrJdwl());


    }

    @Override
    public void setUICallbacks() {
        back.setOnClickListener(this);
        simpan.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        if (v == back) {
            //        Intent i = new Intent(getApplicationContext(), ProfileActivity.class);
            //       startActivity(i);
            finish();
        } else if (v == simpan) {
            Intent i = new Intent(PraktikPickHariActivity.this, PraktikActivity.class);
            Gson gson = new Gson();
            i.putExtra("result", gson.toJson(jdwl));
            setResult(1, i);
            finish();
            //   finish();
        }
    }


    @Override
    public int getLayout() {
        return R.layout.activity_pick_hari;
    }

    @Override
    public void updateUI() {

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

    }


    @Override
    public void onBackPressed() {
        //   Intent i = new Intent(getApplicationContext(), ProfileActivity.class);
        //  startActivity(i);
        finish();
    }
}
