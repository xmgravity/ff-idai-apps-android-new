package com.prima.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import com.prima.entities.Pubertas;
import prima.test.prima.R;

/**
 * Created by Codelabs on 07/08/2015.
 */
public class ListPubertasMateriAdapter extends BaseAdapter {
    private ArrayList<Pubertas.Item> _data;
    Context _context;


    public ListPubertasMateriAdapter(ArrayList<Pubertas.Item> data,
                                     Context context) {
        _data = data;
        _context = context;
    }

    @Override
    public int getCount() {

        return _data.size();
    }

    @Override
    public Object getItem(int position) {

        return _data.get(position);
    }

    @Override
    public long getItemId(int position) {

        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        if (v == null) {
            LayoutInflater infLayout = (LayoutInflater) _context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = infLayout.inflate(R.layout.list_item_materi_pubertas, null);
        }

        TextView phase = (TextView) v.findViewById(R.id.phase);
        TextView description = (TextView) v.findViewById(R.id.description);

        phase.setText(_data.get(position).getPhase());
        description.setText(_data.get(position).getDescription());
        return v;
    }
}
