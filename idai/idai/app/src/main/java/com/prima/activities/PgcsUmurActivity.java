package com.prima.activities;

import android.content.Intent;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.RadioButton;

import java.util.ArrayList;
import java.util.List;

import com.prima.MainActivity;
import prima.test.prima.R;

/**
 * Created by Codelabs on 07/08/2015.
 */
public class PgcsUmurActivity extends BaseActivity implements View.OnClickListener {
    private ImageView close;
    private ImageView back;
    private Button ok;
    List<RadioButton> radioButtons;

    @Override
    public void initView() {
        close = (ImageView) findViewById(R.id.close);
        back = (ImageView) findViewById(R.id.back);
        ok = (Button) findViewById(R.id.lanjut);
        close.setVisibility(View.GONE);

        radioButtons = new ArrayList<RadioButton>();

        radioButtons.add((RadioButton) findViewById(R.id.rad1));
        radioButtons.add((RadioButton) findViewById(R.id.rad2));
        radioButtons.add((RadioButton) findViewById(R.id.rad3));
        radioButtons.add((RadioButton) findViewById(R.id.rad4));
        radioButtons.add((RadioButton) findViewById(R.id.rad5));
        radioButtons.add((RadioButton) findViewById(R.id.rad6));
        radioButtons.add((RadioButton) findViewById(R.id.rad7));
        radioButtons.add((RadioButton) findViewById(R.id.rad8));
        radioButtons.add((RadioButton) findViewById(R.id.rad9));
        radioButtons.add((RadioButton) findViewById(R.id.rad10));
        radioButtons.add((RadioButton) findViewById(R.id.rad11));
        radioButtons.add((RadioButton) findViewById(R.id.rad12));
        radioButtons.add((RadioButton) findViewById(R.id.rad13));
        radioButtons.add((RadioButton) findViewById(R.id.rad14));
        radioButtons.add((RadioButton) findViewById(R.id.rad15));
        radioButtons.add((RadioButton) findViewById(R.id.rad16));
        radioButtons.add((RadioButton) findViewById(R.id.rad17));
        radioButtons.add((RadioButton) findViewById(R.id.rad18));


        //checked
        for (RadioButton button : radioButtons) {
            button.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if (isChecked) processRadioButtonClick(buttonView);
                }
            });

        }
    }

    @Override
    public void setUICallbacks() {
        ok.setOnClickListener(this);
        close.setOnClickListener(this);
        back.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        if (v == ok) {
            Intent i = new Intent(getApplicationContext(), PgcsQuestionActivity.class);
            startActivity(i);
            finish();
        } else if(v==back){
            Intent i = new Intent(getApplicationContext(), MainActivity.class);
            startActivity(i);
            finish();
        }
    }


    @Override
    public int getLayout() {
        return R.layout.activity_glasgow_pilih_umur;
    }

    @Override
    public void updateUI() {

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

    }


    private void processRadioButtonClick(CompoundButton buttonView) {

        for (RadioButton button : radioButtons) {
            if (button != buttonView) button.setChecked(false);
        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent i = new Intent(getApplicationContext(), MainActivity.class);
        startActivity(i);
        finish();
    }
}
