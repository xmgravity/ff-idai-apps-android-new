/**
 * @author Codelabs
 */

package com.prima.helpers;

import android.content.Context;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.Validator.ValidationListener;

import java.util.List;

public abstract class ValidationHelper implements ValidationListener {
	private Context context;
	private Validator validator;
	public ValidationHelper(Context context,Object controller) {
		this.context = context;
		validator = new Validator(controller);
		validator.setValidationListener(this);
	}

	@Override
	public void onValidationSucceeded() {

	}

	@Override
	public void onValidationFailed(List<ValidationError> errors) {
		for (ValidationError error : errors) {
			View view = error.getView();
			String message = error.getCollatedErrorMessage(context);
			// Display error messages ;)
			if (view instanceof EditText) {
				((EditText) view).setError(message);
			} else {
				Toast.makeText(context, message, Toast.LENGTH_LONG).show();
			}
		}
	}

	public Validator getValidator() {
		return validator;
	}

}
