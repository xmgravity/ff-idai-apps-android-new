package com.prima.activities;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.JsonSyntaxException;
import com.prima.MainActivity;
import com.prima.Preferences;
import com.prima.adapters.PagerAdapter;
import com.prima.controllers.ApiReferences;
import com.prima.controllers.Parser;
import com.prima.entities.Artikel;
import com.prima.entities.Grafik;
import com.prima.entities.Message;
import com.prima.fragments.ArtikelPinkFragment;
import com.prima.helpers.DateHelper;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import prima.test.prima.R;

/**
 * Created by Codelabs on 06/10/2015.
 */
public class ArtikelPagerKaloriSehariActivity extends BaseActivity implements View.OnClickListener {
    private Button ok;
    private Context mContext;
    private ViewPager mPager;
    private PagerAdapter mPagerAdapter;
    private static int currentPage;
    PageListener pageListener;
    static TextView title;
    List<Fragment> fragmentList;
    static LinearLayout indikatorWrapper, rel_lingkar_kepala;
    private Artikel artikel;


    ///
    private ImageView close;
    private RequestQueue mRequest;
    private Button dialogLanjut;
    List<CheckBox> checkBoxs;
    ArrayList<String> selected;
    ArrayList<String> grafikName;
    ArrayList<String> interpretasi;
    ArrayList<String> label;
    String id;
    static MaterialDialog dialog;
    Dialog dialog2;
    String result;
    RelativeLayout source_wraper;
    DateHelper dateHelper;
    int circumference_graph;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = getApplicationContext();
    }

    @Override
    public void initView() {
        indikatorWrapper = (LinearLayout) findViewById(R.id.indikator_pager);
        mPager = (ViewPager) findViewById(R.id.pager);

        title = (TextView) findViewById(R.id.title);
        pageListener = new PageListener();
        mPager.setOnPageChangeListener(pageListener);
        ok = (Button) findViewById(R.id.lanjut);
        Typeface tf = Typeface.createFromAsset(getApplicationContext().getAssets(), "BREVIA-BLACK.OTF");
        title.setTypeface(tf);

        Typeface tfAriali = Typeface.createFromAsset(getApplicationContext().getAssets(), "ariali.ttf");
        Typeface tfArialb = Typeface.createFromAsset(getApplicationContext().getAssets(), "arialbd.ttf");

        TextView trademark = (TextView) findViewById(R.id.trademark);
        TextView trademark2 = (TextView) findViewById(R.id.trademark2);
        TextView trademark3 = (TextView) findViewById(R.id.trademark3);
        trademark.setTypeface(tfAriali);
        trademark2.setTypeface(tfArialb);
        trademark3.setTypeface(tfAriali);

        if(preferences.getString(Preferences.TIPE_USER, "").equalsIgnoreCase("awam")){
            trademark.setVisibility(View.GONE);
            trademark2.setVisibility(View.GONE);
            trademark3.setVisibility(View.GONE);
        }

        dateHelper = new DateHelper();
        close = (ImageView) findViewById(R.id.close);
        mRequest = Volley.newRequestQueue(getApplicationContext());
        id = preferences.getString(Preferences.ID, "");

    }

    @Override
    public void setUICallbacks() {
        ok.setOnClickListener(this);
        close.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        if (v == ok) {
            if (!id.equalsIgnoreCase("")) {
                showDialog();
            } else {
                // get prompts.xml view
                LayoutInflater li = LayoutInflater.from(getApplicationContext());
                View dialogView = li.inflate(R.layout.dialog_offline_kalkulator, null);
                final Dialog dialog3;
                dialog3 = new Dialog(context);
                dialog3.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog3.setContentView(dialogView);
                //make transparent dialog_sesi_offline_pelod
                dialog3.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                dialog3.show();


                Button dialogLanjut = (Button) dialogView.findViewById(R.id.ok);
                final RadioButton rad1 = (RadioButton) dialogView.findViewById(R.id.rad1);
                final RadioButton rad2 = (RadioButton) dialogView.findViewById(R.id.rad2);
                rad1.setChecked(true);

                rad1.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        rad1.setChecked(true);
                        rad2.setChecked(false);
                    }
                });

                rad2.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        rad2.setChecked(true);
                        rad1.setChecked(false);
                    }
                });

                final TextView edt = (TextView) dialogView.findViewById(R.id.edt1);
                final EditText edt2 = (EditText) dialogView.findViewById(R.id.edt2);
                final EditText edt3 = (EditText) dialogView.findViewById(R.id.edt3);


                edt.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        showDatepickerDialog(edt);
                    }
                });
                dialogLanjut.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (edt.getText().toString().equals("") ||
                                edt2.getText().toString().equals("") ||
                                edt3.getText().toString().equals("")) {
                            dialog(getResources().getString(R.string.IsiSeluruhIsian), getResources().getString(R.string.Perhatian));
                        } else {
                            String jk;
                            if (rad1.isChecked()) {
                                jk = "m";
                            } else if (rad2.isChecked()) {
                                jk = "f";
                            } else {
                                jk = "";
                            }
                            saveSesiPasienOffline(jk, edt.getText().toString(), edt3.getText().toString(), edt2.getText().toString());
                            dialog3.dismiss();
                        }
                    }
                });
            }

        } else if (v == close) {
            Intent i = new Intent(this, MainActivity.class);
            startActivity(i);
            finish();
        }
    }


    @Override
    public int getLayout() {
        return R.layout.activity_kalori_sehari;
    }

    @Override
    public void updateUI() {
        indikatorWrapper.removeAllViewsInLayout();
        fragmentList = new ArrayList<Fragment>();
        selected = new ArrayList<String>();
        grafikName = new ArrayList<String>();
        interpretasi = new ArrayList<String>();
        label = new ArrayList<String>();
        Bundle bn = getIntent().getExtras();
        if (bn != null) {
            String respon = bn.getString("result");
            artikel = Parser.getArtikel(respon);
            //   title.setText(artikel.getObjects().get(0).getTitle());
            for (int i = 0; i < artikel.getObjects().size(); i++) {
                fragmentList.add(new ArtikelPinkFragment(i, artikel));
                View child = getLayoutInflater().inflate(R.layout.indikator_pager, null);
                View child2 = getLayoutInflater().inflate(R.layout.indikator_pager_blue, null);
                if (i == 0) {
                    indikatorWrapper.addView(child2);
                } else {
                    indikatorWrapper.addView(child);
                }
            }

            setupFrae();
            if (artikel.getObjects().size() > 1) {
                indikatorWrapper.setVisibility(View.VISIBLE);
            } else {
                indikatorWrapper.setVisibility(View.GONE);
            }
        }


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

    }


    void showDialog() {
        // get prompts.xml view

        int i = getYear();
        if (i > 5) {
            BeratThdPjgTinggi();
        }else{
            dialog(getResources().getString(R.string.UmurHarusDiatas5Th), getResources().getString(R.string.Perhatian));
        }
    }


    @Override
    public void onBackPressed() {
        Intent i = new Intent(this, MainActivity.class);
        startActivity(i);
        finish();
    }

    private class PageListener extends ViewPager.SimpleOnPageChangeListener {
        public void onPageSelected(int position) {
            currentPage = position;
            // title.setText(artikel.getObjects().get(position).getTitle());
            indikatorWrapper.removeAllViewsInLayout();
            for (int i = 0; i < artikel.getObjects().size(); i++) {
                View child = getLayoutInflater().inflate(R.layout.indikator_pager, null);
                View child2 = getLayoutInflater().inflate(R.layout.indikator_pager_blue, null);
                if (i == position) {
                    indikatorWrapper.addView(child2);
                } else {
                    indikatorWrapper.addView(child);
                }
            }

        }
    }


    private void setupFrae() {
        mPagerAdapter = new PagerAdapter(getSupportFragmentManager(),
                ArtikelPagerKaloriSehariActivity.this, fragmentList);
        mPager.setAdapter(mPagerAdapter);

    }


    private void BeratThdPjgTinggi() {
        if(!preferences.getString(Preferences.USER_ONLINE, "").equalsIgnoreCase("false")) {

            //   progresDialog(false, "Memuat Grafik");
            StringRequest listdata = new StringRequest(Request.Method.GET,
                    ApiReferences.generateBeratTPanjangTinggi(id, "",""),
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try {
                                if (response.equals("[]")) {
                                    dialog(getResources().getString(R.string.GrafikBelumTersedia), getResources().getString(R.string.Perhatian));
                                } else {
                                    Message msg = Parser.getMessage(response);
                                    if (msg.getStatus().equals("0")) {
                                        dialog(msg.getMessages(), getResources().getString(R.string.Perhatian));
                                    } else {
                                        Log.i("RESULT JSON", response.toString());
                                        Grafik graf = new Grafik();
                                        graf = Parser.getGrafik(response
                                                .toString());
                                        if (graf.getStatus().equals("1")) {

                                            selected.add(graf.getObjects().getImage_url());
                                            interpretasi.add(graf.getObjects().getInterpretation());
                                            grafikName.add(getResources().getString(R.string.heightWeight));
                                            label.add(graf.getObjects().getLabel());

                                            Intent i = new Intent(ArtikelPagerKaloriSehariActivity.this, KaloriSehariInputActivity.class);
                                            i.putStringArrayListExtra("page", selected);
                                            i.putStringArrayListExtra("title", grafikName);
                                            i.putStringArrayListExtra("interpretasi", interpretasi);
                                            i.putStringArrayListExtra("label", label);
                                            i.putExtra("result", result);
                                            startActivity(i);
                                        }
                                    }
                                }
                            } catch (JsonSyntaxException e) {
                                dialog.dismiss();
                                dialog(getResources().getString(R.string.KesalahanServer), getResources().getString(R.string.Perhatian));
                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    dialog.dismiss();
                    dialog(getResources().getString(R.string.NoConnection), getResources().getString(R.string.Perhatian));
                }
            });
            listdata.setRetryPolicy(new DefaultRetryPolicy(
                    20000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            mRequest.add(listdata);
        }else{
            //   progresDialog(false, "Memuat Grafik");
            System.out.println("nandha " + preferences.getString(Preferences.BERAT, ""));
            StringRequest listdata = new StringRequest(Request.Method.POST,
                    ApiReferences.generateBeratTPanjangTinggiOffline(),
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try {
                                if (response.equals("[]")) {
                                    dialog(getResources().getString(R.string.GrafikBelumTersedia), getResources().getString(R.string.Perhatian));
                                } else {
                                    Message msg = Parser.getMessage(response);
                                    if (msg.getStatus().equals("0")) {
                                        dialog(msg.getMessages(), getResources().getString(R.string.Perhatian));
                                    } else {
                                        Log.i("RESULT JSON", response.toString());
                                        Grafik graf = new Grafik();
                                        graf = Parser.getGrafik(response
                                                .toString());
                                        if (graf.getStatus().equals("1")) {

                                            selected.add(graf.getObjects().getImage_url());
                                            interpretasi.add(graf.getObjects().getInterpretation());
                                            grafikName.add(getResources().getString(R.string.heightWeight));
                                            label.add(graf.getObjects().getLabel());

                                            Intent i = new Intent(ArtikelPagerKaloriSehariActivity.this, KaloriSehariInputActivity.class);
                                            i.putStringArrayListExtra("page", selected);
                                            i.putStringArrayListExtra("title", grafikName);
                                            i.putStringArrayListExtra("interpretasi", interpretasi);
                                            i.putStringArrayListExtra("label", label);
                                            i.putExtra("result", result);
                                            startActivity(i);
                                        }
                                    }
                                }
                            } catch (JsonSyntaxException e) {
                                dialog.dismiss();
                                dialog(getResources().getString(R.string.KesalahanServer), getResources().getString(R.string.Perhatian));
                            }
                        }
                    }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        NetworkResponse response = error.networkResponse;
                        if (error instanceof ServerError && response != null) {
                            try {
                                String res = new String(response.data,
                                        HttpHeaderParser.parseCharset(response.headers, "utf-8"));
                                // Now you can use any deserializer to make sense of data
                                JSONObject obj = new JSONObject(res);
                                dialog(res.toString(), getResources().getString(R.string.Perhatian));
                            } catch (UnsupportedEncodingException e1) {
                                // Couldn't properly decode data to string
                                e1.printStackTrace();
                            } catch (JSONException e2) {
                                // returned data is not JSONObject?
                                e2.printStackTrace();
                            }
                        }


                    }
                }) {

                    protected Map<String, String> getParams() {
                        HashMap<String, String> params = new HashMap<String, String>();
                        params.put("dob", preferences.getString(Preferences.BIRTH_DAY, ""));
                        params.put("gender", preferences.getString(Preferences.JENIS_KELAMIN, ""));
                        params.put("weight", preferences.getString(Preferences.BERAT, ""));
                        params.put("height", preferences.getString(Preferences.TINGGI, ""));
                        return params;

                    }
                };
            listdata.setRetryPolicy(new DefaultRetryPolicy(
                    20000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            mRequest.add(listdata);
        }
    }


    private void saveSesiPasienOffline(final String jenis_kelamin, final String tanggal_lahir, final String berat, final String tinggi) {

        editor.putString(Preferences.BERAT, berat);
        editor.putString(Preferences.TINGGI, tinggi);
        editor.putString(Preferences.JENIS_KELAMIN, jenis_kelamin);
        editor.putString(Preferences.BIRTH_DAY, tanggal_lahir);
        editor.putString(Preferences.USER_ONLINE, "false");
        editor.commit();
        id = preferences.getString(Preferences.ID, "");


        showDialog();
    }

    public void dialogEmail(String content, String msg, final String kode) {
        new MaterialDialog.Builder(this)
                .title(msg)
                .content(content)
                .autoDismiss(false)
                .cancelable(false)
                .positiveText(getResources().getString(R.string.KirimEmail))
                .negativeText(getResources().getString(R.string.tutup))
                .callback(new MaterialDialog.ButtonCallback() {
                    @Override
                    public void onPositive(MaterialDialog dialog) {
                        Intent sendEmail = new Intent(Intent.ACTION_SEND);
                        sendEmail.setType("text/plain");
                        sendEmail.putExtra(Intent.EXTRA_EMAIL, new String[]{""});
                        sendEmail.putExtra(Intent.EXTRA_SUBJECT, "Kode Pasien");
                        sendEmail.putExtra(Intent.EXTRA_TEXT, "Berikut ini adalah kode pasien anda.\nKode : " + kode);
                        startActivity(Intent.createChooser(sendEmail, "Email With"));
                    }

                    @Override
                    public void onNegative(MaterialDialog dialog) {
                        dialog.dismiss();
                        showDialog();
                    }
                })
                .show();
    }

    private void showDatepickerDialog(final TextView txt) {
        dateHelper.showDatePicker(this, System.currentTimeMillis(), new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                int _year = year;
                int _month = monthOfYear + 1;
                int _day = dayOfMonth;
                String date = _year + "-" + _month + "-" + _day;
                String birthDate = dateHelper.formatDate(date);
                txt.setText(birthDate);
            }
        });
    }

    private void progresDialog(boolean horizontal, String msg) {
        dialog = new MaterialDialog.Builder(this)
                .title(msg)
                .content(getResources().getString(R.string.MohonTunggu))
                .progress(true, 0)
                .progressIndeterminateStyle(horizontal)
                .cancelable(false)
                .show();
    }

    @Override
    protected void onDestroy() {
        try {
            if (dialog.isShowing()) {
                dialog.cancel();
            }
        } catch (NullPointerException e) {

        }
        super.onDestroy();
    }


    public void dialog(String content, String msg) {
        new MaterialDialog.Builder(this)
                .title(msg)
                .content(content)
                .positiveText("Ok")
                .callback(new MaterialDialog.ButtonCallback() {
                    @Override
                    public void onPositive(MaterialDialog dialog) {
                    }

                })
                .show();
    }

}
