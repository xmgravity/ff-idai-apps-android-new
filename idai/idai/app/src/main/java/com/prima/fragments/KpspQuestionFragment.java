package com.prima.fragments;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.GlideDrawableImageViewTarget;

import java.util.ArrayList;
import java.util.List;

import com.prima.activities.KpspQuestionActivity;
import com.prima.activities.KpspResultActivity;
import com.prima.controllers.ApiReferences;
import com.prima.entities.Question;
import prima.test.prima.R;

/**
 * Created by Codelabs on 07/08/2015.
 */
public class KpspQuestionFragment extends BaseFragment implements View.OnClickListener {
    private Context mContext;
    public static final String ARG_PAGE = "page 2";
    private ArrayList<String> listItem = new ArrayList<String>();
    private ViewPager mPager;
    List<Fragment> fragmentList = new ArrayList<Fragment>();
    Question.Kuesioner kuesioner;
    String selected = "";
    int indeks;
    private ArrayList<String> ansewer = new ArrayList<String>();
    TextView question, title_question;
    Button ya, tidak;
    ImageView image_question;
    LinearLayout img;
    ProgressBar progres;

    @SuppressLint("ValidFragment")
    public KpspQuestionFragment() {}
    @SuppressLint("ValidFragment")
    public KpspQuestionFragment(Question.Kuesioner kuesioner, int indeks) {
        this.kuesioner = kuesioner;
        this.indeks = indeks;
    }

    @Override
    public void initView(View view) {
        mContext = getActivity();
        progres = (ProgressBar) view.findViewById(R.id.progres);
        question = (TextView) view.findViewById(R.id.question);
        title_question = (TextView) getActivity().findViewById(R.id.title_question);
        ya = (Button) view.findViewById(R.id.ya);
        tidak = (Button) view.findViewById(R.id.tidak);
        mPager = (ViewPager) getActivity().findViewById(R.id.pager);
        Activity act = getActivity();
        image_question = (ImageView) view.findViewById(R.id.image_question);
        img = (LinearLayout) view.findViewById(R.id.img);


        question.setText(kuesioner.getTitle());
        if (kuesioner.getType().equals("Text")) {
            img.setVisibility(View.INVISIBLE);
        } else if (kuesioner.getType().equals("Image")) {
            img.setVisibility(View.VISIBLE);
            Glide.with(getActivity())
                    .load(ApiReferences.getImageUrl() + kuesioner.getPath())
                    .into(new GlideDrawableImageViewTarget(image_question) {
                        @Override
                        public void onResourceReady(GlideDrawable drawable, GlideAnimation anim) {
                            super.onResourceReady(drawable, anim);
                            progres.setVisibility(View.GONE);
                        }
                    });
        } else {
            img.setVisibility(View.INVISIBLE);
        }


        if (act instanceof KpspQuestionActivity) {
            fragmentList = ((KpspQuestionActivity) act).getFragmentList();
            ansewer = ((KpspQuestionActivity) act).getAnsewer();
        }

        ya.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ansewer.set(indeks, "1");
                int current = mPager.getCurrentItem();
                if (current != fragmentList.size() - 1) {
                    mPager.setCurrentItem(current + 1, true);
                } else {
                    int score = 0;
                    for (int j = 0; j < ansewer.size(); j++) {
                        if (ansewer.get(j).replace(".00", "").equals("")) {
                            score = score + 0;
                        } else {
                            score = score + Integer.valueOf(ansewer.get(j).replace(".00", ""));
                        }
                    }
                    Intent i = new Intent(mContext, KpspResultActivity.class);
                    i.putExtra("score", score);
                    i.putExtra("soal",String.valueOf(ansewer.size()));
                    startActivity(i);
                    getActivity().finish();
                }

            }
        });

        tidak.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ansewer.set(indeks, "0");
                int current = mPager.getCurrentItem();
                if (current != fragmentList.size() - 1) {
                    mPager.setCurrentItem(current + 1, true);
                } else {
                    int score = 0;
                    for (int j = 0; j < ansewer.size(); j++) {
                        score = score + Integer.valueOf(ansewer.get(j).replace(".00",""));
                    }
                    Intent i = new Intent(mContext, KpspResultActivity.class);
                    i.putExtra("score", score);
                    i.putExtra("soal",String.valueOf(ansewer.size()));
                    startActivity(i);
                    getActivity().finish();
                }

            }
        });


    }

    @Override
    public void setUICallbacks() {

    }

    @Override
    public void updateUI() {

    }

    @Override
    public String getPageTitle() {
        return "Account Summary";
    }


    @Override
    public int getFragmentLayout() {
        return R.layout.fragment_kpsp_question;
    }

    @Override
    public void onClick(View v) {

    }


}