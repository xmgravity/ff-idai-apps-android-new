/**
 * Text Helper
 * @author Codelabs
 */
package com.prima.helpers;

import android.content.Context;
import android.graphics.Typeface;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

public class TextHelper {
	public static final String FONT_DIR = "fonts/";
	private static TextHelper textHelper;
	private Context context;

	public static TextHelper getInstance(Context context){
		if (textHelper == null) {
			textHelper = new TextHelper();
		}
		textHelper.context = context;
		return textHelper;
	}
	
	public void setFont(ViewGroup group,Click2CatchFont catchFont) {
		String fontType = FONT_DIR + catchFont.getFont();
		Typeface font = Typeface.createFromAsset(context.getAssets(),fontType);
		int count = group.getChildCount();
		View v;
		for (int i = 0; i < count; i++) {
			v = group.getChildAt(i);
			if (v instanceof TextView || v instanceof Button)
				((TextView) v).setTypeface(font);
			else if (v instanceof ViewGroup)
				setFont((ViewGroup) v,catchFont);
		}
	}
	
	public enum Click2CatchFont {
		Roboto,Uniqlo;
		
		public String getFont() {
			switch (this) {
			case Roboto:
				return "Roboto-Bold.ttf";
			case Uniqlo:
				return "UniqloBold.ttf";
			default:
				return "Roboto-Bold.ttf";
			}
		}
	}
}
