package com.prima.callbacks;

import android.net.Uri;

/**
 * Created by Codelabs on 2/22/15.
 */
public interface OnPictureSelect {
    public void successSelectPicture(int selectType, Uri uri);
}
