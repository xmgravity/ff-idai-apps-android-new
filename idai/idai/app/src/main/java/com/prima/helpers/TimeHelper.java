package com.prima.helpers;

import android.app.TimePickerDialog;
import android.content.Context;

import java.util.Calendar;

/**
 * Created by Codelabs on 3/25/2015.
 */
public class TimeHelper {

    private static TimeHelper instance;
    public static TimeHelper getInstance(){
        if (instance == null) {
            instance = new TimeHelper();
        }
        return instance;
    }

    public void showDateTimePicker(Context context, TimePickerDialog.OnTimeSetListener onTimeSetListener){
        final Calendar c = Calendar.getInstance();
        int hour = c.get(Calendar.HOUR_OF_DAY);
        int minute = c.get(Calendar.MINUTE);

        TimePickerDialog timePickerDialog = new TimePickerDialog(context, onTimeSetListener, hour, minute, true);

        timePickerDialog.show();
    }
}
