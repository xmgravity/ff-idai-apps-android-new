/**
 * Base Activity
 *
 * @author Codelabs
 * 27 October 2014
 */
package com.prima.activities;

import android.app.ActionBar;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.res.Configuration;
import android.database.Cursor;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Logger;
import com.google.android.gms.analytics.Tracker;
import com.prima.Preferences;
import com.prima.fragments.BaseFragment;
import com.prima.helpers.FragmentHelper;
import com.prima.helpers.GoogleAnalyticsApp;
import com.prima.interfaces.ActivityInterface;

import org.joda.time.LocalDate;
import org.joda.time.Minutes;
import org.joda.time.Months;
import org.joda.time.Weeks;
import org.joda.time.Years;

import java.io.File;
import java.util.List;
import java.util.Locale;

public abstract class BaseActivity extends FragmentActivity implements ActivityInterface {
    private final int TRANSITION_REQUEST_CODE = 391;
    protected SharedPreferences preferences;
    protected Editor editor;
    protected Context context;
    private View actionBarView;
    private FragmentHelper fragmentHelper;
    final String filename = "my_picture.jpg";
    final String fileDir = "/images/";


    @Nullable
    @Override
    public View onCreateView(String name, Context context, AttributeSet attrs) {
        View v = tryInflate(name, context, attrs);
        if (v instanceof TextView) {
            setTypeFace((TextView) v);
        }


        //Dir for file
        File main = new File("/sdcard/prima");
        File pdf = new File("/sdcard/prima/pdf");
        File image = new File("/sdcard/prima/image");
        File sound = new File("/sdcard/prima/sound");
        // have the object build the directory structure, if needed.
        main.mkdirs();
        pdf.mkdirs();
        image.mkdir();
        sound.mkdirs();

        return v;

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        fragmentHelper = FragmentHelper.getInstance(getSupportFragmentManager());
        context = this;
        initSharedPreference();
        setContentView(getLayout());
        initView();
        setUICallbacks();
        showCustomActionBar();

    }


    private void initSharedPreference() {
        preferences = PreferenceManager.getDefaultSharedPreferences(this);
        editor = preferences.edit();

        Locale locale;
        try {
            locale = new Locale(preferences.getString(Preferences.LANGUAGE, ""));
        }catch (Exception e) {
            Log.e("Language", "Language problem!", e);
            locale = new Locale("id");
        }

        Locale.setDefault(locale);
        Configuration config = getBaseContext().getResources().getConfiguration();
        config.locale = locale;
        getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());

    }

    @Override
    protected void onResume() {
        super.onResume();
        updateUI();
    }

    protected void showCustomActionBar() {
        ActionBar actionBar = getActionBar();
        if (actionBar != null) {
            @SuppressWarnings("unused")
            LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
            actionBar.setCustomView(actionBarView);
            actionBar.show();
        }

    }

    public SharedPreferences getPreferences() {
        return preferences;
    }

    public Editor getEditor() {
        return editor;
    }

    public void setActionBarColor(int resColor) {
        actionBarView.setBackgroundResource(resColor);
    }

    @Override
    public void onBackPressed() {
        if (!returnBackStackImmediate(getSupportFragmentManager())) {
            super.onBackPressed();
        }
    }

    private boolean returnBackStackImmediate(FragmentManager fm) {
        List<Fragment> fragments = fm.getFragments();
        if (fragments != null && fragments.size() > 0) {
            for (Fragment fragment : fragments) {
                if (fragment != null) {
                    if (fragment.getChildFragmentManager().getBackStackEntryCount() > 0) {
                        if (fragment.getChildFragmentManager().popBackStackImmediate()) {
                            return true;
                        } else {
                            return returnBackStackImmediate(fragment.getChildFragmentManager());
                        }
                    }
                }
            }
        }
        return false;
    }

    public FragmentHelper getFragmentHelper() {
        return fragmentHelper;
    }

    public void popFragment(String fragName) {
        fragmentHelper.getFragmentManager().popBackStack(fragName, 0);
    }

    public void popAllFragment() {
        fragmentHelper.getFragmentManager().popBackStack(0, FragmentManager.POP_BACK_STACK_INCLUSIVE);
    }

    public void replaceFragment(int container, BaseFragment fragment, boolean addBackToStack) {
        fragmentHelper.replaceFragment(container, fragment, addBackToStack);
    }

    public void replaceFragment(int container, Fragment fragment, boolean addBackToStack) {
        fragmentHelper.replaceFragment(container, fragment, addBackToStack);
    }

    public void replaceFragment(int container, BaseFragment fragment, boolean addBackToStack, String fragmentTag) {
        if (fragmentHelper.getFragmentManager().findFragmentByTag(fragmentTag + "tag") == null) {
            fragmentHelper.replaceFragment(container, fragment, addBackToStack, fragmentTag);
        } else {
            fragmentHelper.replaceFragment(container, fragment, false, fragmentTag);
        }
    }

    public void addFragment(int container, BaseFragment fragment, boolean addBackToStack) {
        fragmentHelper.addFragment(container, fragment, addBackToStack);
    }

    public void addFragment(int container, Fragment fragment, boolean addBackToStack) {
        fragmentHelper.addFragment(container, fragment, addBackToStack);
    }

    public void changeActivity(Class<?> destination) {
        changeActivity(destination, false, null, 0);
    }

    public void changeActivity(Class<?> destination, int flags) {
        changeActivity(destination, false, null, flags);
    }

    public void changeActivity(Class<?> destination, boolean killActivity) {
        changeActivity(destination, killActivity, null, 0);
    }

    public void changeActivity(Class<?> destination, Bundle extra) {
        changeActivity(destination, false, extra, 0);
    }

    public void changeActivity(Class<?> destination, boolean killActivity, Bundle extra, int flags) {
        Intent intent = new Intent(context, destination);
        if (extra != null) {
            intent.putExtras(extra);
        }
        if (flags != 0) {
            intent.setFlags(flags);
        }
        startActivityForResult(intent, TRANSITION_REQUEST_CODE);
        if (killActivity) {
            finish();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == TRANSITION_REQUEST_CODE) {

        }
    }


    public void setSlider(Boolean show) {

    }

    public void setActionbarTitle(String title) {

    }


    private View tryInflate(String name, Context context, AttributeSet attrs) {
        LayoutInflater li = LayoutInflater.from(context);
        View v = null;
        try {
            v = li.createView(name, null, attrs);
        } catch (Exception e) {
            try {
                v = li.createView("android.widget." + name, null, attrs);
            } catch (Exception e1) {
            }
        }
        return v;
    }

    public int getMonth() {
        String date = preferences.getString(Preferences.BIRTH_DAY, "");
        if (date.equals("")) {
            return 0;
        } else {
            Months month = Months.monthsBetween(new LocalDate(date), new LocalDate());
            return month.getMonths();
        }
    }


    public int getYear() {
        String date = preferences.getString(Preferences.BIRTH_DAY, "");
        if (date.equals("")) {
            return 0;
        } else {
            Years year = Years.yearsBetween(new LocalDate(date), new LocalDate());
            System.out.println("umur nandha"+year);
            return year.getYears();
        }
    }

    public int getWeek() {
        String date = preferences.getString(Preferences.BIRTH_DAY, "");
        if (date.equals("")) {
            return 0;
        } else {
            Weeks week = Weeks.weeksBetween(new LocalDate(date), new LocalDate());
            return week.getWeeks();
        }
    }



    public int getMinutes(LocalDate tgl1,LocalDate tgl2) {
        Minutes minutes = Minutes.minutesBetween(tgl1, tgl2);
        return minutes.getMinutes();
    }


    public String getFilename(String url) {
        String fileName = url.substring(url.lastIndexOf('/') + 1);
        return fileName;
    }

    private void setTypeFace(TextView tv) {
        Typeface tf = Typeface.createFromAsset(getApplicationContext().getAssets(), "BREVIA-SEMIBOLD.OTF");
        tv.setTypeface(tf);
    }

    public void deletePdf(String path) {
        File fdelete = new File(path);
        if (fdelete.exists()) {
            if (fdelete.delete()) {
                System.out.println("file Deleted :" + path);
            } else {
                System.out.println("file not Deleted :" + path);
            }
        }
    }
    public void sendAnalytic(String title) {
        Tracker t = ((GoogleAnalyticsApp) getApplication()).getTracker(GoogleAnalyticsApp.TrackerName.APP_TRACKER);
        t.setScreenName(title);
        t.send(new HitBuilders.AppViewBuilder().build());
        GoogleAnalytics.getInstance(this).getLogger().setLogLevel(Logger.LogLevel.VERBOSE);
    }

    public boolean fileExists(String filename) {
        File file = new File(this.getExternalFilesDir(null), filename);
        return file.exists();
    }

    public boolean deleteFile(String filename) {
        File file = new File(this.getExternalFilesDir(null), filename);
        return file.delete();
    }

}
