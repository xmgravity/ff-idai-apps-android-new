package com.prima.activities;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.joanzapata.pdfview.PDFView;
import com.joanzapata.pdfview.listener.OnPageChangeListener;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.prima.Preferences;
import com.prima.controllers.ApiReferences;
import com.prima.helpers.DatabaseHandler;

import java.io.File;

import prima.test.prima.R;

/**
 * Created by Codelabs on 13/08/2015.
 */
public class SkriningPenjelasanKondisiPdfActivity extends BaseActivity implements View.OnClickListener {
    private ImageView close;
    private ImageView back;
    private Context mContext;
    private Button email;
    TextView title;
    private PDFView pdfView;
    TextView page;
    ProgressBar progres;
    MaterialDialog dialog;
    String url, id, menu,category;
    DatabaseHandler db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = getApplicationContext();
        db = new DatabaseHandler(context);

    }

    @Override
    public void initView() {
        back = (ImageView) findViewById(R.id.back);
        close = (ImageView) findViewById(R.id.close);
        pdfView = (PDFView) findViewById(R.id.pdfview);
        email = (Button) findViewById(R.id.email);
        page = (TextView) findViewById(R.id.page);
        title = (TextView) findViewById(R.id.tv_actionbar_title);
        progres = (ProgressBar) findViewById(R.id.progres);
        Typeface tf = Typeface.createFromAsset(getApplicationContext().getAssets(), "BREVIA-BLACK.OTF");
        title.setTypeface(tf);
        title.setText(getResources().getString(R.string.PenjelasanKondisiMedis));
        try {
            getPdf();
        } catch (RuntimeException e){
            dialog(getResources().getString(R.string.Pesan), getResources().getString(R.string.Perhatian));
            e.printStackTrace();
        }


    }

    @Override
    public void setUICallbacks() {
        back.setOnClickListener(this);
        close.setOnClickListener(this);
        email.setOnClickListener(this);
    }

    void getPdf() {
        progresDialog(false, getResources().getString(R.string.MengunduhPdf));
        Bundle bn = getIntent().getExtras();
        if (bn != null) {
            url = bn.getString("url");
            menu = bn.getString("menu");
            id = bn.getString("id");
            category = bn.getString("category");
            Ion.with(context)
                    .load(ApiReferences.getImageUrl() + url)
                    .write(new File("/sdcard/prima/pdf/" + "temp" + getFilename(ApiReferences.getImageUrl() + url)))
                    .setCallback(new FutureCallback<File>() {
                        @Override
                        public void onCompleted(Exception e, File fsile) {
                            // download done...
                            // do stuff with the File or error
                            File file = fsile;

                            try {
                                if (e.toString().contains("ETIMEDOUT")) {
                                    deletePdf("/sdcard/prima/pdf/" + "temp" + getFilename(ApiReferences.getImageUrl() + url));
                                    file = null;
                                }
                            } catch (NullPointerException err) {
                                err.printStackTrace();
                            }

                            dialog.dismiss();
                            if (file == null) {
                                if (!db.getFile(Integer.valueOf(category)).equals("")) {
                                    File f = new File("/sdcard/prima/pdf/" + db.getFile(Integer.valueOf(category)));
                                    pdfView.fromFile(f)
                                            .defaultPage(1)
                                            .showMinimap(false)
                                            .enableSwipe(true)
                                            .onPageChange(new OnPageChangeListener() {
                                                @Override
                                                public void onPageChanged(int i, int i1) {

                                                }
                                            }).load();
                                } else {
                                    dialog(getResources().getString(R.string.GagalMengunduhPdf), getResources().getString(R.string.Perhatian));
                                }
                            } else {
                                deletePdf("/sdcard/prima/pdf/" + db.getFile(Integer.valueOf(category)));
                                db.deleteFile(Integer.valueOf(id));

                                File from = new File("/sdcard/prima/pdf/", "temp" + getFilename(ApiReferences.getImageUrl() + url));
                                File to = new File("/sdcard/prima/pdf/", getFilename(getFilename(ApiReferences.getImageUrl() + url)));
                                from.renameTo(to);

                                db.insertFile(Integer.valueOf(id), menu, getFilename(ApiReferences.getImageUrl() + url), category);
                                File f = new File("/sdcard/prima/pdf/" + db.getFile(Integer.valueOf(category)));

                                pdfView.fromFile(f)
                                        .defaultPage(1)
                                        .showMinimap(false)
                                        .enableSwipe(true)
                                        .onPageChange(new OnPageChangeListener() {
                                            @Override
                                            public void onPageChanged(int i, int i1) {

                                            }
                                        })
                                        .load();
                            }
                        }
                    });


        }
    }

    @Override
    public void onClick(View v) {
        if (v == back) {
            //   Intent i = new Intent(getApplicationContext(), StimulasiPerkembanganActivity.class);
            //    startActivity(i);
            finish();
        } else if (v == close) {
            //     Intent i = new Intent(getApplicationContext(), MainActivity.class);
            //  startActivity(i);
            finish();
        } else if (v == email) {
            String badanEmail;
            String jk;
            if (preferences.getString(Preferences.ID, "").equalsIgnoreCase("") || preferences.getString(Preferences.JENIS_KELAMIN, "").equalsIgnoreCase("")) {
                badanEmail = "";
            } else {
                if (preferences.getString(Preferences.JENIS_KELAMIN, "").equals("0")) {
                    jk = "Perempuan";
                } else {
                    jk = "Laki-Laki";
                }

                badanEmail = "Kode Pasien : " + preferences.getString(Preferences.ID, "") +
                        "\nTanggal Lahir : " + preferences.getString(Preferences.BIRTH_DAY, "") +
                        "\nBerat Badan : " + preferences.getString(Preferences.BERAT, "") +
                        "\nTinggi Badan : " + preferences.getString(Preferences.TINGGI, "") +
                        "\nJenis Kelamin : " + jk;
            }
            Intent sendEmail = new Intent(Intent.ACTION_SEND);
            sendEmail.setType("application/pdf");
            sendEmail.putExtra(Intent.EXTRA_EMAIL, new String[]{""});
            sendEmail.putExtra(Intent.EXTRA_SUBJECT, "Lampiran Skrining Penjelasan Kodisi Medis");
            sendEmail.putExtra(Intent.EXTRA_TEXT, "Berikut ini adalah lampiran penjelasan kondisi medis\n\n"+badanEmail);
            sendEmail.putExtra(Intent.EXTRA_STREAM, Uri.parse("file:///sdcard/prima/pdf/" + getFilename(ApiReferences.getImageUrl() + url)));
            startActivity(Intent.createChooser(sendEmail, "Email With"));
        }
    }


    @Override
    public int getLayout() {
        return R.layout.activity_skrining_medis_utama_pdf;
    }

    @Override
    public void updateUI() {


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

    }


    @Override
    public void onBackPressed() {
        // Intent i = new Intent(getApplicationContext(), StimulasiPerkembanganActivity.class);
        //  startActivity(i);
        finish();
    }

    public void dialog(String content, String msg) {
        new MaterialDialog.Builder(this)
                .title(msg)
                .content(content)
                .positiveText(getResources().getString(R.string.MuatUlang))
                .negativeText(getResources().getString(R.string.Kembali))
                .callback(new MaterialDialog.ButtonCallback() {
                    @Override
                    public void onPositive(MaterialDialog dialog) {
                        getPdf();
                    }

                    @Override
                    public void onNegative(MaterialDialog dialog) {
                        super.onNegative(dialog);
                        finish();
                    }

                })
                .show();
    }

    private void progresDialog(boolean horizontal, String msg) {
        dialog = new MaterialDialog.Builder(this)
                .title(msg)
                .content(getResources().getString(R.string.MohonTunggu))
                .progress(true, 0)
                .progressIndeterminateStyle(horizontal)
                .cancelable(false)
                .show();
    }


}
