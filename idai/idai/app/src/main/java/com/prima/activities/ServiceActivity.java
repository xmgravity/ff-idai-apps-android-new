package com.prima.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import com.prima.helpers.ServiceHelper;

/**
 * Created by Codelabs on 16/09/2015.
 */
public class ServiceActivity extends Activity {
    Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        startService(new Intent(this, ServiceHelper.class));
        intent = new Intent(getBaseContext(), SplashActivity.class);
        startActivity(intent);
        finish();
    }
}
