package com.prima.activities;

import android.content.Intent;
import android.graphics.Typeface;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.prima.MainActivity;
import com.prima.Preferences;
import com.prima.controllers.ApiReferences;
import com.prima.controllers.Parser;
import com.prima.entities.Artikel;
import com.prima.helpers.DatabaseHandler;

import java.util.ArrayList;
import java.util.List;

import prima.test.prima.R;

/**
 * Created by Codelabs on 18/08/2015.
 */
public class MchatActivity extends BaseActivity implements View.OnClickListener {
    private ImageView close;
    private ImageView back;
    private Button lanjut;
    List<RadioButton> radioButtons;
    TextView title;
    MaterialDialog dialog;
    RequestQueue mRequest;
    DatabaseHandler db;

    @Override
    public void initView() {
        db = new DatabaseHandler(context);
        mRequest = Volley.newRequestQueue(getApplicationContext());
        close = (ImageView) findViewById(R.id.close);
        back = (ImageView) findViewById(R.id.back);
        lanjut = (Button) findViewById(R.id.lanjut);
        //   close.setVisibility(View.GONE);
        title = (TextView) findViewById(R.id.tv_actionbar_title);
        //set font
        Typeface tf = Typeface.createFromAsset(getApplicationContext().getAssets(), "Brevia-ExtraBlackItalic.otf");
        title.setTypeface(tf);
        title.setText("M-CHAT");
        radioButtons = new ArrayList<RadioButton>();

        radioButtons.add((RadioButton) findViewById(R.id.radio1));
        radioButtons.add((RadioButton) findViewById(R.id.radio2));

        //checked
        for (RadioButton button : radioButtons) {
            button.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if (isChecked) processRadioButtonClick(buttonView);
                }
            });

        }
    }

    @Override
    public void setUICallbacks() {
        lanjut.setOnClickListener(this);
        close.setOnClickListener(this);
        back.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        if (v == lanjut) {
            if (radioButtons.get(0).isChecked()) {
                getArtikel("M-CHAT-R");
            } else if (radioButtons.get(1).isChecked()) {
                // getPdf("M-CHAT-F","0");
                getArtikel("M-CHAT-F");
            } else {
                Toast.makeText(getApplicationContext(), getResources().getString(R.string.PilihSalahSatuChat), Toast.LENGTH_LONG).show();
            }

        } else if (v == back) {
            Intent i = new Intent(getApplicationContext(), MainActivity.class);
            startActivity(i);
            finish();
        } else if (v == close) {
            Intent i = new Intent(getApplicationContext(), MainActivity.class);
            startActivity(i);
            finish();
        }
    }


    @Override
    public int getLayout() {
        return R.layout.activity_skrining_choose_mchat;
    }

    @Override
    public void updateUI() {

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

    }


    private void processRadioButtonClick(CompoundButton buttonView) {

        for (RadioButton button : radioButtons) {
            if (button != buttonView) button.setChecked(false);
        }

    }

    @Override
    public void onBackPressed() {
        Intent i = new Intent(getApplicationContext(), MainActivity.class);
        startActivity(i);
        finish();
    }


    public void dialog2(String content, String msg) {
        new MaterialDialog.Builder(this)
                .title(msg)
                .content(content)
                .negativeText("OK")
                .callback(new MaterialDialog.ButtonCallback() {
                    @Override
                    public void onPositive(MaterialDialog dialog) {

                    }

                    @Override
                    public void onNegative(MaterialDialog dialog) {
                        super.onNegative(dialog);
                    }

                })
                .show();
    }

    public void dialog(String content, String msg) {
        new MaterialDialog.Builder(this)
                .title(msg)
                .content(content)
                .positiveText("Ok")
                .callback(new MaterialDialog.ButtonCallback() {
                    @Override
                    public void onPositive(MaterialDialog dialog) {
                        super.onPositive(dialog);
                    }
                })
                .show();
    }


    private void progresDialog(boolean horizontal, String msg) {
        dialog = new MaterialDialog.Builder(this)
                .title(msg)
                .content(getResources().getString(R.string.MohonTunggu))
                .progress(true, 0)
                .progressIndeterminateStyle(horizontal)
                .cancelable(false)
                .show();
    }


    private void getArtikel(final String code) {
        progresDialog(false, getResources().getString(R.string.MemuatData));
        StringRequest listdata = new StringRequest(Request.Method.GET,
                ApiReferences.getArticle(code, preferences.getString(Preferences.LANGUAGE, "")),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        dialog.dismiss();
                        try {
                            if (response.equals("[]")) {
                                dialog(getResources().getString(R.string.GrafikBelumTersedia), getResources().getString(R.string.Perhatian));
                            } else {
                                Log.i("RESULT JSON", response.toString());
                                Artikel artikel = Parser.getArtikel(response.toString());
                                db.deleteArtikel(code);
                                for (int i = 0; i < artikel.getObjects().size(); i++) {
                                    db.insertArtikel(artikel.getObjects().get(i), code);
                                }


                                intentTo(code, response);

                            }
                        } catch (JsonSyntaxException e) {
                            dialog(getResources().getString(R.string.KesalahanServer), getResources().getString(R.string.Perhatian));
                        }
                    }

                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dialog.dismiss();
                //     dialog(getResources().getString(R.string.NoConnection), getResources().getString(R.string.Perhatian));

                Artikel temp = db.getArtikels(code);
                if (temp.getObjects().size() > 0) {
                    Gson gs = new Gson();
                    intentTo(code, gs.toJson(temp));
                } else {
                    dialog(getResources().getString(R.string.NoConnection), getResources().getString(R.string.Perhatian));
                }
            }
        });
        listdata.setRetryPolicy(new DefaultRetryPolicy(
                20000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        mRequest.add(listdata);

    }


    void intentTo(String code, String response) {
        sendAnalytic(code);
        if (code.equalsIgnoreCase("M-CHAT-R")) {
            Intent i = new Intent(getApplicationContext(), ArtikelPagerMchatrActivity.class);
            i.putExtra("result", response);
            startActivity(i);
            //   finish();
        } else if (code.equals("M-CHAT-F")) {
            Intent i = new Intent(getApplicationContext(), ArtikelPagerMchatrfActivity.class);
            i.putExtra("result", response);
            startActivity(i);
        }
    }


}
