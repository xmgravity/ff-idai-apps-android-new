package com.prima.activities;

import android.content.Intent;
import android.graphics.Typeface;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import prima.test.prima.R;

/**
 * Created by Codelabs on 29/08/2015.
 */
public class SplashSelamatDatangActivity extends BaseActivity implements View.OnClickListener {

TextView title;
    @Override
    public void initView() {
        title = (TextView) findViewById(R.id.title);
        Typeface tf = Typeface.createFromAsset(getApplicationContext().getAssets(), "BREVIA-BLACK.OTF");
        title.setTypeface(tf);
        Thread background = new Thread() {
            public void run() {
                try {
                    // Thread will sleep for 5 seconds
                    sleep(2000);
                    Intent i = new Intent(getBaseContext(), LoginActivity.class);
                    startActivity(i);
                    finish();
                } catch (Exception e) {
                    Log.v("activity", "splash catch");
                }
            }
        };

        // start thread
        background.start();
    }

    @Override
    public void setUICallbacks() {
    }


    @Override
    public void onClick(View v) {


    }


    @Override
    public int getLayout() {
        return R.layout.activity_splash_selamat_datang;
    }

    @Override
    public void updateUI() {

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}