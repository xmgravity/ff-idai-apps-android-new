package com.prima.activities;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.prima.MainActivity;
import com.prima.Preferences;

import prima.test.prima.R;

/**
 * Created by Codelabs on 11/08/2015.
 */
public class KalkulatorBasalFaktorResultActivity extends BaseActivity implements View.OnClickListener {

    Button kembali;
    TextView title, result;

    @Override
    public void initView() {
        kembali = (Button) findViewById(R.id.kembali);
        title = (TextView) findViewById(R.id.title);
        result = (TextView) findViewById(R.id.result);
        //set font
        Typeface tf = Typeface.createFromAsset(getApplicationContext().getAssets(), "Brevia-ExtraBlackItalic.otf");
        Typeface tf2 = Typeface.createFromAsset(getApplicationContext().getAssets(), "BREVIA-BLACK.OTF");
        title.setTypeface(tf);
        result.setTypeface(tf2);

        Typeface tfAriali = Typeface.createFromAsset(getApplicationContext().getAssets(), "ariali.ttf");
        Typeface tfArialb = Typeface.createFromAsset(getApplicationContext().getAssets(), "arialbd.ttf");

        TextView trademark = (TextView) findViewById(R.id.trademark);
        TextView trademark2 = (TextView) findViewById(R.id.trademark2);
        TextView trademark3 = (TextView) findViewById(R.id.trademark3);
        trademark.setTypeface(tfAriali);
        trademark2.setTypeface(tfArialb);
        trademark3.setTypeface(tfAriali);

        if(preferences.getString(Preferences.TIPE_USER, "").equalsIgnoreCase("awam")){
            trademark.setVisibility(View.GONE);
            trademark2.setVisibility(View.GONE);
            trademark3.setVisibility(View.GONE);
        }
    }

    @Override
    public void setUICallbacks() {
        kembali.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        if (v==kembali){
            Intent i = new Intent(getApplicationContext(), MainActivity.class);
            startActivity(i);
            /*
            if (preferences.getString(Preferences.ID,"").equals("1")) {
                editor.putString(Preferences.BIRTH_DAY, "");
                editor.putString(Preferences.JENIS_KELAMIN, "");
                editor.putString(Preferences.BERAT, "");
                editor.putString(Preferences.TINGGI, "");
                editor.putString(Preferences.ID, "");
                editor.commit();
            }*/
            finish();
        }
    }


    @Override
    public int getLayout() {
        return R.layout.activity_result_faktor_kalkulator_basal;
    }

    @Override
    public void updateUI() {
        Bundle bn = getIntent().getExtras();
        if (bn != null) {
            Float score = bn.getFloat("result");
            result.setText(Math.round(score)+"");
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

    }



    @Override
    public void onBackPressed() {
        Intent i = new Intent(getApplicationContext(), MainActivity.class);
        startActivity(i);
        /*if (preferences.getString(Preferences.ID,"").equals("1")) {
            editor.putString(Preferences.BIRTH_DAY, "");
            editor.putString(Preferences.JENIS_KELAMIN, "");
            editor.putString(Preferences.BERAT, "");
            editor.putString(Preferences.TINGGI, "");
            editor.putString(Preferences.ID, "");
            editor.commit();
        }*/
        finish();
    }
}
