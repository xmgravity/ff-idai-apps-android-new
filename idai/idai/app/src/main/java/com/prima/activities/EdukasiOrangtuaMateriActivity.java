package com.prima.activities;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import com.prima.adapters.PagerAdapter;
import com.prima.controllers.Parser;
import com.prima.entities.MateriEdukasi;
import com.prima.fragments.EdukasiOrangtuaFragment;

import prima.test.prima.R;

/**
 * Created by Codelabs on 07/08/2015.
 */
public class EdukasiOrangtuaMateriActivity extends BaseActivity implements View.OnClickListener {
    private Button ok;
    private Context mContext;
    private ViewPager mPager;
    private PagerAdapter mPagerAdapter;
    private static int currentPage;
    PageListener pageListener;
    static TextView title;
    List<Fragment> fragmentList = new ArrayList<Fragment>();
    static LinearLayout indikatorWrapper;
    private MateriEdukasi materi;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = getApplicationContext();
    }

    @Override
    public void initView() {
        indikatorWrapper = (LinearLayout) findViewById(R.id.indikator_pager);
        mPager = (ViewPager) findViewById(R.id.pager);
        title = (TextView) findViewById(R.id.title);
        pageListener = new PageListener();
        mPager.setOnPageChangeListener(pageListener);
        ok = (Button) findViewById(R.id.lanjut);
        Typeface tf = Typeface.createFromAsset(getApplicationContext().getAssets(), "BREVIA-BLACK.OTF");
        title.setTypeface(tf);

    }

    @Override
    public void setUICallbacks() {
        ok.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        if (v == ok) {
            int current = mPager.getCurrentItem();
            if (current != fragmentList.size() - 1) {
                mPager.setCurrentItem(current + 1, true);
                if (current == fragmentList.size() - 2) {
                    ok.setText("OK");
                } else {
                    ok.setText(getString(R.string.Lanjut));
                }
            } else {
                //   Intent i = new Intent(mContext, EdukasiOrangtuaActivity.class);
                // startActivity(i);
                finish();
            }
        }
    }


    @Override
    public int getLayout() {
        return R.layout.activity_edukasi_orangtua;
    }

    @Override
    public void updateUI() {
        indikatorWrapper.removeAllViewsInLayout();
        fragmentList.clear();
        Bundle bn = getIntent().getExtras();
        if (bn != null) {
            String respon = bn.getString("respon");
            materi = Parser.getMateriEdukasi(respon);
            title.setText(materi.getObjects().get(0).getTitle());
            for (int i = 0; i < materi.getObjects().size(); i++) {
                fragmentList.add(new EdukasiOrangtuaFragment(i, materi));
                View child = getLayoutInflater().inflate(R.layout.indikator_pager, null);
                View child2 = getLayoutInflater().inflate(R.layout.indikator_pager_blue, null);
                if (i == 0) {
                    indikatorWrapper.addView(child2);
                } else {
                    indikatorWrapper.addView(child);
                }
            }

            setupFrae();
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

    }


    @Override
    public void onBackPressed() {
        if (currentPage > 0) {
            currentPage--;
            mPager.setCurrentItem(currentPage, true);
            ok.setText(getString(R.string.Lanjut));

        } else {
            //   Intent i = new Intent(getApplicationContext(), EdukasiOrangtuaActivity.class);
            //   startActivity(i);
            finish();
        }
    }

    private class PageListener extends ViewPager.SimpleOnPageChangeListener {
        public void onPageSelected(int position) {
            currentPage = position;
            title.setText(materi.getObjects().get(position).getTitle());
            indikatorWrapper.removeAllViewsInLayout();
            for (int i = 0; i < materi.getObjects().size(); i++) {
                View child = getLayoutInflater().inflate(R.layout.indikator_pager, null);
                View child2 = getLayoutInflater().inflate(R.layout.indikator_pager_blue, null);
                if (i == position) {
                    indikatorWrapper.addView(child2);
                } else {
                    indikatorWrapper.addView(child);
                }
            }

        }
    }


    private void setupFrae() {
        mPagerAdapter = new PagerAdapter(getSupportFragmentManager(),
                EdukasiOrangtuaMateriActivity.this, fragmentList);
        mPager.setAdapter(mPagerAdapter);

    }

}
