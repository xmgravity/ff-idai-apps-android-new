package com.prima.entities;

/**
 * Created by Codelabs on 07/09/2015.
 */
public class Pdf extends Message {
    private PdfObjects data;

    public PdfObjects getObjects() {
        return data;
    }

    public void setObjects(PdfObjects objects) {
        this.data = objects;
    }

    public class PdfObjects {
        private String id;
        private String menu_code;
        private String age_id;
        private String filename;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getMenu() {
            return menu_code;
        }

        public void setMenu(String menu) {
            this.menu_code = menu;
        }

        public String getCategory() {
            return age_id;
        }

        public void setCategory(String category) {
            this.age_id = category;
        }

        public String getFilename() {
            return filename;
        }

        public void setFilename(String filename) {
            this.filename = filename;
        }
    }
}
