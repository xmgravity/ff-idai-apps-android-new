package com.prima.activities;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.JsonSyntaxException;
import com.prima.MainActivity;
import com.prima.Preferences;
import com.prima.adapters.PagerAdapter;
import com.prima.controllers.ApiReferences;
import com.prima.controllers.Parser;
import com.prima.entities.Artikel;
import com.prima.entities.Pubertas;
import com.prima.fragments.ArtikelBlueFragment;
import com.prima.helpers.DateHelper;

import java.util.ArrayList;
import java.util.List;

import prima.test.prima.R;

/**
 * Created by Codelabs on 06/10/2015.
 */
public class ArtikelPagerTahapPubertasActivity extends BaseActivity implements View.OnClickListener {
    private Button ok;
    private Context mContext;
    private ViewPager mPager;
    private PagerAdapter mPagerAdapter;
    private static int currentPage;
    PageListener pageListener;
    static TextView title;
    List<Fragment> fragmentList = new ArrayList<Fragment>();
    static LinearLayout indikatorWrapper;
    private Artikel artikel;

    //
    private ImageView close;
    public Pubertas pubertas;
    MaterialDialog dialog;
    private RequestQueue mRequest;
    DateHelper dateHelper;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = getApplicationContext();
    }

    @Override
    public void initView() {
        indikatorWrapper = (LinearLayout) findViewById(R.id.indikator_pager);
        mPager = (ViewPager) findViewById(R.id.pager);
        title = (TextView) findViewById(R.id.title);
        pageListener = new PageListener();
        mPager.setOnPageChangeListener(pageListener);
        ok = (Button) findViewById(R.id.lanjut);
        Typeface tf = Typeface.createFromAsset(getApplicationContext().getAssets(), "BREVIA-BLACK.OTF");
        title.setTypeface(tf);

        dateHelper = new DateHelper();
        mRequest = Volley.newRequestQueue(getApplicationContext());
        close = (ImageView) findViewById(R.id.close);

    }

    @Override
    public void setUICallbacks() {
        ok.setOnClickListener(this);
        close.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        if (v == ok) {
            if (preferences.getString(Preferences.ID, "").equalsIgnoreCase("") || preferences.getString(Preferences.JENIS_KELAMIN, "").equalsIgnoreCase("")) {
                // get prompts.xml view
                LayoutInflater li = LayoutInflater.from(getApplicationContext());
                View dialogView = li.inflate(R.layout.dialog_sesi_pasien_puberty, null);
                final Dialog dialog3;
                dialog3 = new Dialog(context);
                dialog3.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog3.setContentView(dialogView);
                //make transparent dialog_sesi_offline_pelod
                dialog3.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                dialog3.show();


                Button dialogLanjut = (Button) dialogView.findViewById(R.id.ok);
                final RadioButton rad1 = (RadioButton) dialogView.findViewById(R.id.rad1);
                final RadioButton rad2 = (RadioButton) dialogView.findViewById(R.id.rad2);
                rad1.setChecked(true);

                rad1.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        rad1.setChecked(true);
                        rad2.setChecked(false);
                    }
                });

                rad2.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        rad2.setChecked(true);
                        rad1.setChecked(false);
                    }
                });

                final TextView edt = (TextView) dialogView.findViewById(R.id.edt1);
                final LinearLayout lin1 = (LinearLayout) dialogView.findViewById(R.id.lin1);
                final LinearLayout lin2 = (LinearLayout) dialogView.findViewById(R.id.lin2);
                lin1.setVisibility(View.GONE);
                lin2.setVisibility(View.GONE);

                edt.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        showDatepickerDialog(edt);
                    }
                });
                dialogLanjut.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (edt.getText().toString().equals("")) {
                            dialog2(getResources().getString(R.string.IsiSeluruhIsian), getResources().getString(R.string.Perhatian));
                        } else {
                            String jk;
                            if (rad1.isChecked()) {
                                jk = "m";
                            } else if (rad2.isChecked()) {
                                jk = "f";
                            } else {
                                jk = "";
                            }

                            editor.putString(Preferences.JENIS_KELAMIN, jk);
                            editor.putString(Preferences.ID, "1");
                            editor.commit();
                            //   saveSesiPasien(jk, edt.getText().toString(), edt3.getText().toString(), edt2.getText().toString());
                            getMateri();
                            dialog3.dismiss();
                        }
                    }
                });

            } else {
                getMateri();
            }
        } else if (v == close) {
            Intent i = new Intent(getApplicationContext(), MainActivity.class);
            startActivity(i);
            finish();
        }
    }


    @Override
    public int getLayout() {
        return R.layout.activity_tahap_pubertas;
    }

    @Override
    public void updateUI() {
        indikatorWrapper.removeAllViewsInLayout();
        fragmentList.clear();
        Bundle bn = getIntent().getExtras();
        if (bn != null) {
            String respon = bn.getString("result");
            artikel = Parser.getArtikel(respon);
            //   title.setText(artikel.getObjects().get(0).getTitle());
            for (int i = 0; i < artikel.getObjects().size(); i++) {
                fragmentList.add(new ArtikelBlueFragment(i, artikel));
                View child = getLayoutInflater().inflate(R.layout.indikator_pager, null);
                View child2 = getLayoutInflater().inflate(R.layout.indikator_pager_blue, null);
                if (i == 0) {
                    indikatorWrapper.addView(child2);
                } else {
                    indikatorWrapper.addView(child);
                }
            }

            setupFrae();
            if (artikel.getObjects().size() > 1) {
                indikatorWrapper.setVisibility(View.VISIBLE);
            } else {
                indikatorWrapper.setVisibility(View.GONE);
            }
        }


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

    }


    private void getMateri() {
        progresDialog(false, getResources().getString(R.string.MemuatData));
        StringRequest listdata = new StringRequest(Request.Method.GET,
                ApiReferences.getPubertas(preferences.getString(Preferences.LANGUAGE, "")),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        dialog.dismiss();
                        try {
                            pubertas = new Pubertas();
                            pubertas = Parser.getPubertas(response);
                            Log.i("RESULT JSON", pubertas.getObjects().get(0).getObject().get(0).getTitle());
                            Intent i = new Intent(getApplicationContext(), TahapPubertasMateri.class);
                            i.putExtra("respon", response.toString());
                            startActivity(i);
                            //  finish();
                        } catch (JsonSyntaxException e) {
                            dialog(getResources().getString(R.string.KesalahanServer), getResources().getString(R.string.Perhatian));
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dialog.dismiss();
                dialog(getResources().getString(R.string.NoConnection), getResources().getString(R.string.Perhatian));
            }
        });
        listdata.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        mRequest.add(listdata);
    }


    public void dialogEmail(String content, String msg, final String kode) {
        new MaterialDialog.Builder(this)
                .title(msg)
                .content(content)
                .autoDismiss(false)
                .cancelable(false)
                .positiveText(getResources().getString(R.string.KirimEmail))
                .negativeText(getResources().getString(R.string.tutup))
                .callback(new MaterialDialog.ButtonCallback() {
                    @Override
                    public void onPositive(MaterialDialog dialog) {
                        Intent sendEmail = new Intent(Intent.ACTION_SEND);
                        sendEmail.setType("text/plain");
                        sendEmail.putExtra(Intent.EXTRA_EMAIL, new String[]{""});
                        sendEmail.putExtra(Intent.EXTRA_SUBJECT, "Kode Pasien");
                        sendEmail.putExtra(Intent.EXTRA_TEXT, "Berikut ini adalah kode pasien anda.\nKode : " + kode);
                        startActivity(Intent.createChooser(sendEmail, "Email With"));
                    }

                    @Override
                    public void onNegative(MaterialDialog dialog) {
                        dialog.dismiss();
                        getMateri();
                    }
                })
                .show();
    }


    private void progresDialog(boolean horizontal, String msg) {
        dialog = new MaterialDialog.Builder(this)
                .title(msg)
                .content(getResources().getString(R.string.MohonTunggu))
                .progress(true, 0)
                .progressIndeterminateStyle(horizontal)
                .cancelable(false)
                .show();
    }

    public void dialog(String content, String msg) {
        new MaterialDialog.Builder(this)
                .title(msg)
                .content(content)
                .positiveText(getResources().getString(R.string.MuatUlang))
                .negativeText(getResources().getString(R.string.Kembali))
                .callback(new MaterialDialog.ButtonCallback() {
                    @Override
                    public void onPositive(MaterialDialog dialog) {
                        getMateri();
                    }

                    @Override
                    public void onNegative(MaterialDialog dialog) {
                        super.onNegative(dialog);
                    }

                })
                .show();
    }


    public void dialog2(String content, String msg) {
        new MaterialDialog.Builder(this)
                .title(msg)
                .content(content)
                .negativeText("OK")
                .callback(new MaterialDialog.ButtonCallback() {
                    @Override
                    public void onPositive(MaterialDialog dialog) {

                    }


                })
                .show();
    }

    private void showDatepickerDialog(final TextView txt) {
        dateHelper.showDatePicker(this, System.currentTimeMillis(), new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                int _year = year;
                int _month = monthOfYear + 1;
                int _day = dayOfMonth;
                String date = _year + "-" + _month + "-" + _day;
                String birthDate = dateHelper.formatDate(date);
                txt.setText(birthDate);
            }
        });
    }


    @Override
    public void onBackPressed() {
        Intent i = new Intent(getApplicationContext(), MainActivity.class);
        startActivity(i);
        finish();
    }

    private class PageListener extends ViewPager.SimpleOnPageChangeListener {
        public void onPageSelected(int position) {
            currentPage = position;
            // title.setText(artikel.getObjects().get(position).getTitle());
            indikatorWrapper.removeAllViewsInLayout();
            for (int i = 0; i < artikel.getObjects().size(); i++) {
                View child = getLayoutInflater().inflate(R.layout.indikator_pager, null);
                View child2 = getLayoutInflater().inflate(R.layout.indikator_pager_blue, null);
                if (i == position) {
                    indikatorWrapper.addView(child2);
                } else {
                    indikatorWrapper.addView(child);
                }
            }

        }
    }


    private void setupFrae() {
        mPagerAdapter = new PagerAdapter(getSupportFragmentManager(),
                ArtikelPagerTahapPubertasActivity.this, fragmentList);
        mPager.setAdapter(mPagerAdapter);

    }

}
