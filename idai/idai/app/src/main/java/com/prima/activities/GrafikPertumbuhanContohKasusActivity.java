package com.prima.activities;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.davemorrissey.labs.subscaleview.SubsamplingScaleImageView;

import prima.test.prima.R;

/**
 * Created by Codelabs on 31/08/2015.
 */
public class GrafikPertumbuhanContohKasusActivity extends BaseActivity implements View.OnClickListener {
    private SubsamplingScaleImageView img;
    private ImageView close;
    private TextView subtitle, title, contohKasusTitle, contohKasusContentTitle, contohKasusContentContent;
    private String hasilKesimpulan;
    private Button tabelInterpretasi;

    @Override
    public void initView() {
        img = (SubsamplingScaleImageView) findViewById(R.id.img);
        close = (ImageView) findViewById(R.id.close);
        title = (TextView) findViewById(R.id.title);
        subtitle = (TextView) findViewById(R.id.subtitle);
        contohKasusTitle = (TextView) findViewById(R.id.contohKasusTitle);
        contohKasusContentTitle = (TextView) findViewById(R.id.contohKasusContentTitle);
        contohKasusContentContent = (TextView) findViewById(R.id.contohKasusContentContent);
        tabelInterpretasi = (Button) findViewById(R.id.tabelInterpretasi);


        Typeface tf0 = Typeface.createFromAsset(this.getAssets(), "BREVIA-BLACK.OTF");
        Typeface tf = Typeface.createFromAsset(this.getAssets(), "BREVIA-MEDIUM.OTF");

        subtitle.setTypeface(tf);
        title.setTypeface(tf0);
        contohKasusTitle.setTypeface(tf0);
        contohKasusContentTitle.setTypeface(tf);
        contohKasusContentContent.setTypeface(tf);
        tabelInterpretasi.setTypeface(tf);

        // Asynchronously load the image and set the thumbnail and pager view
        Bundle bn = getIntent().getExtras();
        if (bn != null) {
            hasilKesimpulan = bn.getString("titleKasus");
            subtitle.setText(hasilKesimpulan);

            if(hasilKesimpulan.equalsIgnoreCase(getResources().getString(R.string.grafikHeightAge))){
                contohKasusContentTitle.setText(getResources().getString(R.string.ContohKasusTitle));
                contohKasusContentContent.setText(getResources().getString(R.string.FirstExample) +
                        getResources().getString(R.string.FirstExample2) +
                        getResources().getString(R.string.FirstExample3) +
                        getResources().getString(R.string.FirstExample4)

                );
            }else if(hasilKesimpulan.equalsIgnoreCase(getResources().getString(R.string.grafikWeightAge))){
                contohKasusContentTitle.setText(getResources().getString(R.string.ContohKasusTitle2));
                contohKasusContentContent.setText(getResources().getString(R.string.SecondExample) +
                        getResources().getString(R.string.SecondExample2) +
                        getResources().getString(R.string.SecondExample3) +
                        getResources().getString(R.string.SecondExample4)
                );
            } else if(hasilKesimpulan.equalsIgnoreCase(getResources().getString(R.string.grafikWeightHeight))){
                contohKasusContentTitle.setText(getResources().getString(R.string.ContohKasusTitle3));
                contohKasusContentContent.setText(
                        getResources().getString(R.string.ThirdExample) +
                        getResources().getString(R.string.ThirdExample2) +
                        getResources().getString(R.string.ThirdExample3)
                );
            } else if(hasilKesimpulan.equalsIgnoreCase(getResources().getString(R.string.grafikBodyMassIndex))){
                contohKasusContentTitle.setText(getResources().getString(R.string.ContohKasusTitle4));
                contohKasusContentContent.setText("" +
                        getResources().getString(R.string.FourthExample) +
                        getResources().getString(R.string.FourthExample2) +
                        getResources().getString(R.string.FourthExample3) +
                        getResources().getString(R.string.FourthExample4) +
                        getResources().getString(R.string.FourthExample5) +
                        getResources().getString(R.string.FourthExample6)
                );
            }

        }
    }


    @Override
    public void setUICallbacks() {

        close.setOnClickListener(this);
        tabelInterpretasi.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if (v == close) {
            finish();
        }else if (v == tabelInterpretasi){
            Intent i = new Intent(context, ImagePreviewActivity.class);
            i.putExtra("image", hasilKesimpulan);
            startActivity(i);
        }
    }


    @Override
    public int getLayout() {
        return R.layout.activity_grafik_pertumbuhan_contoh_kasus;
    }

    @Override
    public void updateUI() {

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

    }


    @Override
    public void onBackPressed() {
        finish();
    }


}
