package com.prima.entities;

import java.util.ArrayList;

/**
 * Created by Codelabs on 06/09/2015.
 */
public class KalenderImunisasi extends Message {

    private ArrayList<Vaksin> objects;

    public String getFile() {
        return file;
    }

    public void setFile(String file) {
        this.file = file;
    }

    private String file;

    public ArrayList<Vaksin> getObjects() {
        return objects;
    }

    public void setObjects(ArrayList<Vaksin> objects) {
        this.objects = objects;
    }

    public class Vaksin {
        private String id_vaksin;
        private String jenis_vaksin;
        private String tahapan;
        private String pesan;
        private ArrayList<JadwalImunisasi> object;

        public ArrayList<JadwalImunisasi> getObject() {
            return object;
        }

        public void setObject(ArrayList<JadwalImunisasi> object) {
            this.object = object;
        }

        public String getId_vaksin() {
            return id_vaksin;
        }

        public void setId_vaksin(String id_vaksin) {
            this.id_vaksin = id_vaksin;
        }

        public String getJenis_vaksin() {
            return jenis_vaksin;
        }

        public void setJenis_vaksin(String jenis_vaksin) {
            this.jenis_vaksin = jenis_vaksin;
        }

        public String getTahapan() {
            return tahapan;
        }

        public void setTahapan(String tahapan) {
            this.tahapan = tahapan;
        }

        public String getPesan() {
            return pesan;
        }

        public void setPesan(String pesan) {
            this.pesan = pesan;
        }


    }

    public class JadwalImunisasi {
        private String id;
        private String id_vaksin;
        private String tahap;
        private String min_age;
        private String max_age;
        private String catatan;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getId_vaksin() {
            return id_vaksin;
        }

        public void setId_vaksin(String id_vaksin) {
            this.id_vaksin = id_vaksin;
        }

        public String getTahap() {
            return tahap;
        }

        public void setTahap(String tahap) {
            this.tahap = tahap;
        }

        public String getMin_age() {
            return min_age;
        }

        public void setMin_age(String min_age) {
            this.min_age = min_age;
        }

        public String getMax_age() {
            return max_age;
        }

        public void setMax_age(String max_age) {
            this.max_age = max_age;
        }

        public String getCatatan() {
            return catatan;
        }

        public void setCatatan(String catatan) {
            this.catatan = catatan;
        }
    }
}
