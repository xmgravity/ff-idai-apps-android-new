package com.prima.activities;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.prima.Preferences;
import com.prima.controllers.Parser;
import com.prima.entities.Question;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;

import at.grabner.circleprogress.CircleProgressView;
import at.grabner.circleprogress.TextMode;
import prima.test.prima.R;

/**
 * Created by Codelabs on 10/08/2015.
 */
public class PelodResultActivity extends BaseActivity implements View.OnClickListener {

    Button ok;
    TextView title, result, interpretasi;
    TextView grafik;
    CircleProgressView circleView;

    @Override
    public void initView() {
        circleView = (CircleProgressView) findViewById(R.id.circleView);
        ok = (Button) findViewById(R.id.ok);
        title = (TextView) findViewById(R.id.title);
        result = (TextView) findViewById(R.id.result);
        interpretasi = (TextView) findViewById(R.id.interpretasi);
        //set font
        Typeface tf = Typeface.createFromAsset(getApplicationContext().getAssets(), "BREVIA-BLACK.OTF");
        title.setTypeface(tf);
        circleView.setTextTypeface(tf);
        Typeface tfAriali = Typeface.createFromAsset(getApplicationContext().getAssets(), "ariali.ttf");
        Typeface tfArialb = Typeface.createFromAsset(getApplicationContext().getAssets(), "arialbd.ttf");

        TextView trademark = (TextView) findViewById(R.id.trademark);
        TextView trademark2 = (TextView) findViewById(R.id.trademark2);
        TextView trademark3 = (TextView) findViewById(R.id.trademark3);
        trademark.setTypeface(tfAriali);
        trademark2.setTypeface(tfArialb);
        trademark3.setTypeface(tfAriali);

        if(preferences.getString(Preferences.TIPE_USER, "").equalsIgnoreCase("awam")){
            trademark.setVisibility(View.GONE);
            trademark2.setVisibility(View.GONE);
            trademark3.setVisibility(View.GONE);
        }
    }

    @Override
    public void setUICallbacks() {
        ok.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        if (v == ok) {
            //   Intent i = new Intent(getApplicationContext(), PelodActivity.class);
            //   startActivity(i);
            finish();
        }
    }


    @Override
    public int getLayout() {
        return R.layout.activity_result_pelod;
    }

    @Override
    public void updateUI() {
        Bundle bn = getIntent().getExtras();
        if (bn != null) {
            ArrayList<String> answer = bn.getStringArrayList("list_score");
            Question question = Parser.getQuestion(bn.getString("question"));

            ArrayList<String> tempTitle = new ArrayList<String>();
            for (int i = 0; i < question.getObjects().size(); i++) {
                tempTitle.add(question.getObjects().get(i).getTitle());
            }

            //remove same value
            ArrayList<String> title = new ArrayList<String>(new HashSet<String>(tempTitle));
            Collections.sort(title);

            int newScore = 0;
            for (int i = 0; i < title.size(); i++) {
                int temp = 0;
                for (int j = 0; j < question.getObjects().size(); j++) {
                    if (question.getObjects().get(j).getTitle().equals(title.get(i))) {
                        if (temp < Integer.valueOf(answer.get(j).replace(".00",""))) {
                            temp = Integer.valueOf(answer.get(j).replace(".00", ""));
                        }
                    }
                }
                newScore = newScore + temp;
            }


            int score = bn.getInt("score");
            int max_score = bn.getInt("max_score");


            float e = 2.72f;
            float Logit = -6.61f + 0.47f * (score);
            float rate = (float) (1 / (1 + Math.pow(e, -Logit)));
            interpretasi.setText(getResources().getString(R.string.PerkiraanTingkatMortalitas) + "\n\n" + String.format("%.02f", rate*100).replace(".", ",") + "%");
            result.setText(score + "");

            Log.i("HADUH LOgit", newScore + "");

            System.out.println("nandha total score"+score);
            System.out.println("nandha max score"+max_score);

            int windowContainerWidth = this.getResources().getDisplayMetrics().widthPixels;
            circleView.getLayoutParams().width = windowContainerWidth / 2;
            circleView.getLayoutParams().height = windowContainerWidth / 2;

            circleView.setMaxValue(max_score);
            circleView.setValue(score);
            circleView.setText(score + "");
            circleView.setTextMode(TextMode.TEXT);
            circleView.setSeekModeEnabled(false);
        }


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

    }


    @Override
    public void onBackPressed() {
        //  Intent i = new Intent(getApplicationContext(), PelodActivity.class);
        //   startActivity(i);
        finish();
    }
}
