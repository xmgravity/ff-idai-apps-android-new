package com.prima.fragments;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.prima.Preferences;
import com.prima.activities.Psc17QuestionActivity;
import com.prima.activities.Psc17Result;
import com.prima.adapters.ListAdapter;
import com.prima.entities.Question;

import java.util.ArrayList;
import java.util.List;

import prima.test.prima.R;


/**
 * Created by Codelabs on 07/08/2015.
 */
public class Psc17QuesionerFragment extends BaseFragment implements View.OnClickListener {

    private Context mContext;
    public static final String ARG_PAGE = "page 1";
    private ArrayList<String> listItem = new ArrayList<String>();
    private ArrayList<String> listItem2 = new ArrayList<String>();
    private ListView list;
    private ViewPager mPager;
    String selected = "";
    int indeks;
    List<Fragment> fragmentList = new ArrayList<Fragment>();
    Question.Kuesioner kuesioner;
    private ArrayList<String> ansewer = new ArrayList<String>();
    private ArrayList<String> tipe = new ArrayList<String>();
    private ArrayList<String> answerInternalisasi = new ArrayList<String>();
    private ArrayList<String> answerEksternalisasi = new ArrayList<String>();
    private ArrayList<String> answerPerhatian = new ArrayList<String>();


    private ArrayList<String> kode = new ArrayList<String>();
    int totalScore = 0;

    @SuppressLint("ValidFragment")
    public Psc17QuesionerFragment() {}
    @SuppressLint("ValidFragment")
    public Psc17QuesionerFragment(Question.Kuesioner kuesioner, int indeks) {
        this.kuesioner = kuesioner;
        this.indeks = indeks;
    }

    @Override
    public void initView(View view) {
        list = (ListView) view.findViewById(R.id.listView);
        mPager = (ViewPager) getActivity().findViewById(R.id.pager);
        mContext = getActivity();
        final Activity act = getActivity();


        if (act instanceof Psc17QuestionActivity) {
            fragmentList = ((Psc17QuestionActivity) act).getFragmentList();
            ansewer = ((Psc17QuestionActivity) act).getAnsewer();
            answerInternalisasi = ((Psc17QuestionActivity) act).getAnswerInternalisasi();
            answerEksternalisasi = ((Psc17QuestionActivity) act).getAnswerEksternalisasi();
            answerPerhatian = ((Psc17QuestionActivity) act).getAnswerPerhatian();
            kode = ((Psc17QuestionActivity) act).getKode();
            tipe = ((Psc17QuestionActivity) act).getTipe();
        }


        final ListAdapter adapterList = new ListAdapter(
                kuesioner.getAnswer_option(), mContext, selected);
        list.setAdapter(adapterList);


        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                ansewer.set(indeks, kuesioner.getAnswer_option().get(position).getScore());
                kode.set(indeks, kuesioner.getKode() + kuesioner.getAnswer_option().get(position).getScore().replace(".00", ""));
                tipe.set(indeks, kuesioner.getSub_title());


                selected = String.valueOf(kuesioner.getAnswer_option().get(position).getScore());
                adapterList.setSelected(selected);
                adapterList.notifyDataSetChanged();
                int current = mPager.getCurrentItem();
                if (current != fragmentList.size() - 1) {
                    mPager.setCurrentItem(current + 1, true);
                } else {
                    int score = 0;
                    int scoreInternalisasi = 0;
                    int scoreEksternalisasi = 0;
                    int scorePerhatian = 0;

                    String interpretasi = " ";
                    for (int j = 0; j < ansewer.size(); j++) {
                        if (ansewer.get(j).replace(".00", "").equals("")) {
                            score = score + 0;
                        } else {
                            score = score + Integer.valueOf(ansewer.get(j).replace(".00", ""));
                        }
                        interpretasi = interpretasi + kode.get(j)+ " ";


                        if(tipe.get(j).equalsIgnoreCase("Internalisasi")){
                            scoreInternalisasi = scoreInternalisasi + Integer.valueOf(ansewer.get(j).replace(".00", ""));
                            System.out.println("nandha internalisasi :" + scoreInternalisasi);
                        }else if(tipe.get(j).equalsIgnoreCase("Eksternalisasi")){
                            scoreEksternalisasi = scoreEksternalisasi + Integer.valueOf(ansewer.get(j).replace(".00", ""));
                            System.out.println("nandha eksternalisasi :" + scoreEksternalisasi);
                        }else if(tipe.get(j).equalsIgnoreCase(getResources().getString(R.string.Perhatian))){
                            scorePerhatian = scorePerhatian + Integer.valueOf(ansewer.get(j).replace(".00", ""));
                            System.out.println("nandha perhatian :" + scorePerhatian);
                        }


                    }
                    System.out.println("nandha internalisasi total : " + scoreInternalisasi);
                    System.out.println("nandha eksternalisasi total : " + scoreEksternalisasi);
                    System.out.println("nandha perhatian total: " + scorePerhatian);


                    if (preferences.getString(Preferences.ID, "").equals("1")) {
                        editor.putString(Preferences.BIRTH_DAY, "");
                        editor.putString(Preferences.JENIS_KELAMIN, "");
                        editor.putString(Preferences.BERAT, "");
                        editor.putString(Preferences.TINGGI, "");
                        editor.putString(Preferences.ID, "");
                        editor.commit();
                    }

                    if (act instanceof Psc17QuestionActivity) {
                        totalScore = ((Psc17QuestionActivity) act).getMax_score();
                    }

                    Intent i = new Intent(mContext, Psc17Result.class);
                    i.putExtra("score", score);
                    i.putExtra("scoreInternalisasi", scoreInternalisasi);
                    i.putExtra("scoreEksternalisasi", scoreEksternalisasi);
                    i.putExtra("scorePerhatian", scorePerhatian);
                    i.putExtra("interpretasi", interpretasi);
                    i.putExtra("max_score",totalScore);
                    startActivity(i);
                    getActivity().finish();
                }

            }
        });
    }

    @Override
    public void setUICallbacks() {

    }

    @Override
    public void updateUI() {

    }

    @Override
    public String getPageTitle() {
        return "Account Summary";
    }


    @Override
    public int getFragmentLayout() {
        return R.layout.list_view;
    }

    @Override
    public void onClick(View v) {

    }

}