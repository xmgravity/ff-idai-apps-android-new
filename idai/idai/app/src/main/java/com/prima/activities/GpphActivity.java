package com.prima.activities;

import android.content.Intent;
import android.graphics.Typeface;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.prima.MainActivity;
import com.prima.helpers.DateHelper;
import prima.test.prima.R;

/**
 * Created by Codelabs on 18/08/2015.
 */
public class GpphActivity extends BaseActivity implements View.OnClickListener {
    private LinearLayout box;
    private Button source;
    private TextView txtSource;
    private ImageView close;
    private Button lanjut;
    private TextView title;
    private TextView content;
    RelativeLayout source_wraper;
    DateHelper dateHelper;


    @Override
    public void initView() {
        dateHelper = new DateHelper();
        content = (TextView) findViewById(R.id.content);
        source_wraper = (RelativeLayout) findViewById(R.id.source_wraper);

        source = (Button) findViewById(R.id.btn_source);
        txtSource = (TextView) findViewById(R.id.source);
        box = (LinearLayout) findViewById(R.id.box);
        close = (ImageView) findViewById(R.id.close);
        lanjut = (Button) findViewById(R.id.lanjut);
        txtSource.setVisibility(View.GONE);
        title = (TextView) findViewById(R.id.title);
        //set font
        Typeface tf = Typeface.createFromAsset(getApplicationContext().getAssets(), "Brevia-ExtraBlackItalic.otf");
        title.setTypeface(tf);


    }

    @Override
    public void setUICallbacks() {
        source.setOnClickListener(this);
        lanjut.setOnClickListener(this);
        close.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        if (v == source) {
            if (txtSource.getVisibility() == View.GONE) {
                txtSource.setVisibility(View.VISIBLE);
            } else {
                txtSource.setVisibility(View.GONE);
            }
        } else if (v == lanjut) {
            Intent i = new Intent(getApplicationContext(), GpphInterpretasiActivity.class);
            startActivity(i);
          //  finish();
        }
        if (v == close) {
            Intent i = new Intent(getApplicationContext(), MainActivity.class);
            startActivity(i);
            finish();
        }
    }


    @Override
    public int getLayout() {
        return R.layout.activity_gpph;
    }

    @Override
    public void updateUI() {
//        Bundle bn = getIntent().getExtras();
//        if (bn != null) {
//            String result = bn.getString("result");
//            Artikel artikel = Parser.getArtikel(result);
//            content.setText(artikel.getObjects().getDeskripsi());
//            Spannable s = (Spannable) Html.fromHtml("<a href=\"" + artikel.getObjects().getSumber() + "\">" + artikel.getObjects().getSumber() + "</a>");
//            URLSpan[] spans = s.getSpans(0, s.length(), URLSpan.class);
//            for (URLSpan span : spans) {
//                int start = s.getSpanStart(span);
//                int end = s.getSpanEnd(span);
//                s.removeSpan(span);
//                span = new URLSpan(span.getURL());
//                s.setSpan(span, start, end, 0);
//            }
//            txtSource.setText(s);
//            txtSource.setMovementMethod(LinkMovementMethod.getInstance());
//
//            if (artikel.getObjects().getSumber().trim().equals("")) {
//                txtSource.setVisibility(View.GONE);
//                source_wraper.setVisibility(View.GONE);
//                source.setVisibility(View.GONE);
//
//            }
//
//        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

    }


    @Override
    public void onBackPressed() {
        Intent i = new Intent(getApplicationContext(), MainActivity.class);
        startActivity(i);
        finish();
    }
}
