package com.prima.activities;

import android.content.Intent;
import android.graphics.Typeface;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.JsonSyntaxException;
import com.prima.MainActivity;
import com.prima.Preferences;
import com.prima.controllers.ApiReferences;
import com.prima.controllers.Parser;
import com.prima.entities.Message;
import com.prima.helpers.DateHelper;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

import prima.test.prima.R;

/**
 * Created by Codelabs on 29/08/2015.
 */
public class AwamChangePasswordActivity extends BaseActivity implements View.OnClickListener {
    TextView title;
    Button setuju, tidak_setuju;
    DateHelper dateHelper;
    RelativeLayout register;
    EditText oldPassword;
    EditText newPassword;
    EditText newPasswordConfirm;
    private ImageView back, close;

    private RequestQueue mRequest;
    MaterialDialog dialog;

    @Override
    public void initView() {
        close = (ImageView) findViewById(R.id.close);
        close.setVisibility(View.GONE);
        back = (ImageView) findViewById(R.id.back);
        title = (TextView) findViewById(R.id.tv_actionbar_title);
        Typeface tf = Typeface.createFromAsset(getApplicationContext().getAssets(), "BREVIA-BLACK.OTF");
        title.setTypeface(tf);
        title.setText(getResources().getString(R.string.titleUbahKataSandi));

        mRequest = Volley.newRequestQueue(this);

        dateHelper = new DateHelper();
        register = (RelativeLayout) findViewById(R.id.register);
        oldPassword = (EditText) findViewById(R.id.oldPassword);
        newPassword = (EditText) findViewById(R.id.newPassword);
        newPasswordConfirm = (EditText) findViewById(R.id.newPasswordConfirm);

    }


    @Override
    public void setUICallbacks() {
        back.setOnClickListener(this);
        register.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {

        if (v == register) {

            //TODO Validation untuk form

            if (newPassword.getText().toString().equals(newPasswordConfirm.getText().toString())) {

                changePassword(oldPassword.getText().toString(),
                        newPassword.getText().toString(),
                        newPasswordConfirm.getText().toString()
                );
            } else if (!newPassword.getText().toString().equals(newPasswordConfirm.getText().toString())) {
                dialog(getResources().getString(R.string.KataSandiBaruDanKonfirmasiHarusSama), getResources().getString(R.string.Perhatian));
            } else if (newPassword.length() < 8 || newPasswordConfirm.length() < 8) {
                dialog(getResources().getString(R.string.KataSandiLebihAtauSamaDengan8), getResources().getString(R.string.Perhatian));
            } else {
                Intent i = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(i);
                finish();
            } if (v == back) {

                Intent i = new Intent(this, MainActivity.class);
                startActivity(i);
                finish();
            }
        }
    }

    private void changePassword(final String oldpassword,
                          final String newPassword,
                          final String newPasswordConfirm) {

        System.out.println("oldpassword :"+oldpassword+", newPassword :"+newPassword+", newPasswordConfirm :"+newPasswordConfirm);


        StringRequest listdata = new StringRequest(Request.Method.POST,
                ApiReferences.changePassword(),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.i("RESULT JSON", response.toString());

                        try {
                            Message msg = Parser.getMessage(response);
                            dialog(msg.getMessages().toString(), getResources().getString(R.string.Perhatian));

                        } catch (JsonSyntaxException e) {
                            dialog(getResources().getString(R.string.KesalahanServer), getResources().getString(R.string.Perhatian));
                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {

                NetworkResponse response = error.networkResponse;
                if (error instanceof ServerError && response != null) {
                    try {
                        String res = new String(response.data,
                                HttpHeaderParser.parseCharset(response.headers, "utf-8"));
                        // Now you can use any deserializer to make sense of data
                        JSONObject obj = new JSONObject(res);

                        String b = obj.toString().substring(1, obj.toString().length() -1);
                        String c = b.toString().replace("messages", "");
                        String d = c.toString().replace("\"", "");
                        String e = d.toString().replace(":", "");
                        String f = e.toString().replace("\\", "");
                        String g = f.toString().replace("[", "");
                        String h = g.toString().replace("]", "");

                        dialog(h, getResources().getString(R.string.Perhatian));
                    } catch (UnsupportedEncodingException e1) {
                        // Couldn't properly decode data to string
                        e1.printStackTrace();
                    } catch (JSONException e2) {
                        // returned data is not JSONObject?
                        e2.printStackTrace();
                    }
                }


            }
        }) {

            public Map<String, String> getHeaders() {
                Map<String, String>  params = new HashMap<String, String>();
                params.put("token", preferences.getString(Preferences.ACCESS_TOKEN, ""));

                return params;
            }

            protected Map<String, String> getParams() {
                HashMap<String, String> params = new HashMap<String, String>();
                params.put("old_password", oldpassword);
                params.put("password", newPasswordConfirm);
                return params;


            }
        };

        listdata.setRetryPolicy(new DefaultRetryPolicy(
                20000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        mRequest.add(listdata);


    }



    @Override
    public int getLayout() {
        return R.layout.activity_awam_change_password;
    }

    @Override
    public void updateUI() {

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

    }


    @Override
    public void onBackPressed() {
        Intent i = new Intent(getBaseContext(), MainActivity.class);
        startActivity(i);
        finish();
    }

    public void dialog(String content, String msg) {
        new MaterialDialog.Builder(this)
                .title(msg)
                .content(content)
                .positiveText("Ok")
                .callback(new MaterialDialog.ButtonCallback() {
                    @Override
                    public void onPositive(MaterialDialog dialog) {

                    }

                })
                .show();
    }

}