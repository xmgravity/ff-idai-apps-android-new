package com.prima.fragments;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.support.v4.view.ViewPager;
import android.view.View;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.davemorrissey.labs.subscaleview.ImageSource;
import com.davemorrissey.labs.subscaleview.SubsamplingScaleImageView;

import java.util.ArrayList;

import com.prima.activities.GrafikPertumbuhanResultActivity;
import com.prima.activities.ImagePreviewActivity;
import com.prima.controllers.ApiReferences;
import prima.test.prima.R;

/**
 * Created by Codelabs on 19/08/2015.
 */
public class GrafikPertumbuhanFragment extends BaseFragment implements View.OnClickListener {

    @SuppressLint("ValidFragment")
    public GrafikPertumbuhanFragment() {}
    @SuppressLint("ValidFragment")
    public GrafikPertumbuhanFragment(int iterasi){
        this.iterasi = iterasi;
    }
    private Context mContext;
    public static final String ARG_PAGE = "page 1";
    SubsamplingScaleImageView img;
    ArrayList<String> urlList = new ArrayList<String>();
    private ViewPager mPager;
    private int iterasi;

    @Override
    public void initView(View view) {
        Activity act = getActivity();
        mPager = (ViewPager) getActivity().findViewById(R.id.pager);
        if(act instanceof GrafikPertumbuhanResultActivity) {
            urlList = ((GrafikPertumbuhanResultActivity) act).getData();
        }

        mContext = getActivity();
        int current = mPager.getCurrentItem();
        img = (SubsamplingScaleImageView) view.findViewById(R.id.img);


        // Asynchronously load the image and set the thumbnail and pager view
        Glide.get(getActivity()).clearMemory();
        Glide.with(getActivity())
                .load(ApiReferences.getImageUrl()+urlList.get(iterasi))
                .asBitmap()
                .into(new SimpleTarget<Bitmap>() {
                    @Override
                    public void onResourceReady(Bitmap bitmap, GlideAnimation anim) {
                        img.setImage(ImageSource.bitmap(bitmap));
                    }
                });

        System.out.println("nandha image"+ ApiReferences.getImageUrl()+urlList.get(iterasi));

        img.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                Intent i = new Intent(getActivity(), ImagePreviewActivity.class);
                i.putExtra("url",urlList.get(iterasi));
                getActivity().startActivity(i);

                return true;
            }
        });



    }

    @Override
    public void setUICallbacks() {

    }

    @Override
    public void updateUI() {

    }

    @Override
    public String getPageTitle() {
        return "Account Summary";
    }


    @Override
    public int getFragmentLayout() {
        return R.layout.fragment_grafik_pertumbuhan;
    }

    @Override
    public void onClick(View v) {

    }



}
