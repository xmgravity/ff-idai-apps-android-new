package com.prima.entities;

/**
 * Created by Codelabs on 14/08/2015.
 */
public class MyCalendar {

    public String name;
    public String id;

    public MyCalendar(String _name, String _id) {
        name = _name;
        id = _id;
    }

    @Override
    public String toString() {
        return name;
    }
}
