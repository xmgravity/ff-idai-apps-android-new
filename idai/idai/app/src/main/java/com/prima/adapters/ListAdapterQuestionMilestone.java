package com.prima.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.prima.entities.Question;

import java.util.ArrayList;

import prima.test.prima.R;

/**
 * Created by Codelabs on 07/08/2015.
 */
public class ListAdapterQuestionMilestone extends BaseAdapter {
    private ArrayList<Question.Kuesioner> _data;
    Context _context;
    ArrayList<Integer> answer = new ArrayList<Integer>();
    Boolean redflag=false;
    ImageView redIcon;
    private ArrayList<Integer> answered = new ArrayList<Integer>();

    public ArrayList<Integer> getAnswered() {
        return answered;
    }

    public ListAdapterQuestionMilestone(Question data,
                                        Context context, ImageView red) {
        redIcon = red;
        _data = data.getObjects();
        _context = context;
        for(int i=0;i<_data.size();i++){
            answer.add(0);
        }

        for(int i=0;i<_data.size();i++){
            answered.add(0);
        }
    }

    @Override
    public int getCount() {

        return _data.size();
    }

    @Override
    public Object getItem(int position) {
        return _data.get(position);
    }

    @Override
    public long getItemId(int position) {

        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View v = convertView;
        if (v == null) {
            LayoutInflater infLayout = (LayoutInflater) _context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = infLayout.inflate(R.layout.list_item_kuesioner_checkbox, null);
        }

        final ImageView rad1 = (ImageView) v.findViewById(R.id.radio1);
        final ImageView rad2 = (ImageView) v.findViewById(R.id.radio2);

        TextView text_quesioner = (TextView) v.findViewById(R.id.text_quesioner);

        text_quesioner.setText(_data.get(position).getTitle());

        if (answer.get(position) == 0) {
            rad1.setBackgroundResource(R.mipmap.icn_boxuncheck);
            rad2.setBackgroundResource(R.mipmap.icn_boxuncheck);
        } else if (answer.get(position) == 1) {
            rad1.setBackgroundResource(R.mipmap.ic_checked);
            rad2.setBackgroundResource(R.mipmap.icn_boxuncheck);
        } else if (answer.get(position) == 2) {
            rad1.setBackgroundResource(R.mipmap.icn_boxuncheck);
            rad2.setBackgroundResource(R.mipmap.ic_checked);
        }

        rad1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                redflag=false;
                answer.set(position, 1);
                answered.set(position, 1);
                rad1.setBackgroundResource(R.mipmap.ic_checked);
                rad2.setBackgroundResource(R.mipmap.icn_boxuncheck);
                for(int i=0;i<_data.size();i++){
                    if(answer.get(i)==2){
                     redflag = true;
                    }
                }

                if (redflag){
                    redIcon.setVisibility(View.VISIBLE);
                }else {
                    redIcon.setVisibility(View.INVISIBLE);
                }

            }
        });

        rad2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                redflag=false;
                answer.set(position, 2);
                answered.set(position, 1);
                rad1.setBackgroundResource(R.mipmap.icn_boxuncheck);
                rad2.setBackgroundResource(R.mipmap.ic_checked);
                for(int i=0;i<_data.size();i++){
                    if(answer.get(i)==2){
                        redflag = true;
                    }
                }
                if (redflag){
                    redIcon.setVisibility(View.VISIBLE);
                }else {
                    redIcon.setVisibility(View.INVISIBLE);
                }

            }
        });

        return v;
    }


}
