package com.prima.activities;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.prima.Preferences;

import at.grabner.circleprogress.CircleProgressView;
import at.grabner.circleprogress.TextMode;
import prima.test.prima.R;

/**
 * Created by Codelabs on 10/08/2015.
 */
public class BallardResultActivity extends BaseActivity implements View.OnClickListener {

    Button ok;
    TextView title, result, interpretasi;
    CircleProgressView circleView;

    @Override
    public void initView() {
        circleView = (CircleProgressView) findViewById(R.id.circleView);
        ok = (Button) findViewById(R.id.ok);
        title = (TextView) findViewById(R.id.title);
        result = (TextView) findViewById(R.id.result);
        interpretasi = (TextView) findViewById(R.id.interpretasi);
        //set font
        Typeface tf = Typeface.createFromAsset(getApplicationContext().getAssets(), "BREVIA-BLACK.OTF");
        title.setTypeface(tf);
        result.setTypeface(tf);
        circleView.setTextTypeface(tf);

        Typeface tfAriali = Typeface.createFromAsset(getApplicationContext().getAssets(), "ariali.ttf");
        Typeface tfArialb = Typeface.createFromAsset(getApplicationContext().getAssets(), "arialbd.ttf");

        TextView trademark = (TextView) findViewById(R.id.trademark);
        TextView trademark2 = (TextView) findViewById(R.id.trademark2);
        TextView trademark3 = (TextView) findViewById(R.id.trademark3);
        trademark.setTypeface(tfAriali);
        trademark2.setTypeface(tfArialb);
        trademark3.setTypeface(tfAriali);

        if(preferences.getString(Preferences.TIPE_USER, "").equalsIgnoreCase("awam")){
            trademark.setVisibility(View.GONE);
            trademark2.setVisibility(View.GONE);
            trademark3.setVisibility(View.GONE);
        }
    }

    @Override
    public void setUICallbacks() {
        ok.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        if (v == ok) {
           // Intent i = new Intent(getApplicationContext(), BallardScoreActivity.class);
         //   startActivity(i);
            finish();
        }
    }


    @Override
    public int getLayout() {
        return R.layout.activity_result_ballard;
    }

    @Override
    public void updateUI() {
        Bundle bn = getIntent().getExtras();
        if (bn != null) {
            int score = bn.getInt("score");
            int max_score = bn.getInt("max_score");
            result.setText(String.valueOf(score));

            if (score >= 50) {
                interpretasi.setText(getResources().getString(R.string.UsiaKehamilan44Minggu));
            } else if (score >= 48) {
                interpretasi.setText(getResources().getString(R.string.UsiaKehamilan43Minggu));
            } else if (score >= 45) {
                interpretasi.setText(getResources().getString(R.string.UsiaKehamilan42Minggu));
            } else if (score >= 43) {
                interpretasi.setText(getResources().getString(R.string.UsiaKehamilan41Minggu));
            } else if (score >= 40) {
                interpretasi.setText(getResources().getString(R.string.UsiaKehamilan40Minggu));
            } else if (score >= 38) {
                interpretasi.setText(getResources().getString(R.string.UsiaKehamilan39Minggu));
            } else if (score >= 35) {
                interpretasi.setText(getResources().getString(R.string.UsiaKehamilan38Minggu));
            } else if (score >= 33) {
                interpretasi.setText(getResources().getString(R.string.UsiaKehamilan37Minggu));
            } else if (score >= 30) {
                interpretasi.setText(getResources().getString(R.string.UsiaKehamilan36Minggu));
            } else if (score >= 28) {
                interpretasi.setText(getResources().getString(R.string.UsiaKehamilan35Minggu));
            } else if (score >= 25) {
                interpretasi.setText(getResources().getString(R.string.UsiaKehamilan34Minggu));
            } else if (score >= 23) {
                interpretasi.setText(getResources().getString(R.string.UsiaKehamilan33Minggu));
            } else if (score >= 20) {
                interpretasi.setText(getResources().getString(R.string.UsiaKehamilan32Minggu));
            } else if (score >= 18) {
                interpretasi.setText(getResources().getString(R.string.UsiaKehamilan31Minggu));
            } else if (score >= 15) {
                interpretasi.setText(getResources().getString(R.string.UsiaKehamilan30Minggu));
            } else if (score >= 13) {
                interpretasi.setText(getResources().getString(R.string.UsiaKehamilan29Minggu));
            } else if (score >= 10) {
                interpretasi.setText(getResources().getString(R.string.UsiaKehamilan28Minggu));
            } else if (score >= 8) {
                interpretasi.setText(getResources().getString(R.string.UsiaKehamilan27Minggu));
            } else if (score >= 5) {
                interpretasi.setText(getResources().getString(R.string.UsiaKehamilan26Minggu));
            } else if (score >= 3) {
                interpretasi.setText(getResources().getString(R.string.UsiaKehamilan25Minggu));
            } else if (score >= 0) {
                interpretasi.setText(getResources().getString(R.string.UsiaKehamilan24Minggu));
            } else if (score >= -2) {
                interpretasi.setText(getResources().getString(R.string.UsiaKehamilan23Minggu));
            } else if (score >= -5) {
                interpretasi.setText(getResources().getString(R.string.UsiaKehamilan22Minggu));
            } else if (score >= -7) {
                interpretasi.setText(getResources().getString(R.string.UsiaKehamilan21Minggu));
            } else if (score >= -10) {
                interpretasi.setText(getResources().getString(R.string.UsiaKehamilan20Minggu));
            } else {
                interpretasi.setText(getResources().getString(R.string.UsiaKehamilanDibawah20Minggu));
            }


            int windowContainerWidth = this.getResources().getDisplayMetrics().widthPixels;
            circleView.getLayoutParams().width = windowContainerWidth/2;
            circleView.getLayoutParams().height = windowContainerWidth/2;

            circleView.setMaxValue(max_score);
            circleView.setValue(score);
            circleView.setText(score + "");
            circleView.setTextMode(TextMode.TEXT);
            circleView.setSeekModeEnabled(false);
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

    }


    @Override
    public void onBackPressed() {
        //  Intent i = new Intent(getApplicationContext(), BallardScoreActivity.class);
        //  startActivity(i);
        finish();
    }
}
