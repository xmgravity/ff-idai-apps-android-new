/**
 * Base Entity
 *
 * @author Codelabs
 * 27 October 2014
 */
package com.prima.entities;

import com.j256.ormlite.field.DatabaseField;

import java.io.Serializable;

@SuppressWarnings("serial")
public class BaseEntity implements Serializable {
    public static final String UPDATED_DATE = "updated_date";
    public static final String CREATED_DATE = "created_date";
    public static final String IS_DELETED = "is_deleted";
    public static final String  VERSION = "version";


    @DatabaseField(id = true)
    protected String id;
    @DatabaseField(columnName = UPDATED_DATE)
    protected Long updateDate;
    @DatabaseField(columnName = CREATED_DATE)
    protected Long createdDate;
    @DatabaseField(columnName = IS_DELETED)
    protected boolean isDeleted;
    @DatabaseField (columnName = VERSION)
    protected String version;

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public BaseEntity() {
    }

    public BaseEntity(long updateDate) {
        setUpdateDate(updateDate);
    }

    public boolean isDeleted() {
        return isDeleted;
    }

    public void setDeleted(boolean isDeleted) {
        this.isDeleted = isDeleted;
    }

    public Long getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Long createdDate) {
        this.createdDate = createdDate;
    }

    public Long getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Long updateDate) {
        this.updateDate = updateDate;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }


}
