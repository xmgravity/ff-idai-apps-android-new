package com.prima.activities;

import android.content.Intent;
import android.graphics.Typeface;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.prima.MainActivity;
import prima.test.prima.R;

/**
 * Created by Codelabs on 26/08/2015.
 */
public class SettingActivity extends BaseActivity implements View.OnClickListener {

    private TextView title;
    private ImageView close,back;
    private RelativeLayout email;

    @Override
    public void initView() {

        close = (ImageView) findViewById(R.id.close);
        back = (ImageView) findViewById(R.id.back);
        email = (RelativeLayout) findViewById(R.id.email);

        close.setVisibility(View.GONE);
        //set font
        title = (TextView) findViewById(R.id.tv_actionbar_title);
        Typeface tf = Typeface.createFromAsset(getApplicationContext().getAssets(), "BREVIA-BLACK.OTF");
        title.setTypeface(tf);
        title.setText("PENGATURAN");
    }

    @Override
    public void setUICallbacks() {

        back.setOnClickListener(this);
        email.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        if (v==back){
            Intent i = new Intent(getApplicationContext(), MainActivity.class);
            startActivity(i);
            finish();
        } else if (v==email){
//            Intent i = new Intent(getApplicationContext(), EditEmailActivity.class);
//            startActivity(i);
//            finish();
        }
    }


    @Override
    public int getLayout() {
        return R.layout.activity_setting;
    }

    @Override
    public void updateUI() {

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

    }


    @Override
    public void onBackPressed() {
        Intent i = new Intent(getApplicationContext(), MainActivity.class);
        startActivity(i);
        finish();
    }
}
