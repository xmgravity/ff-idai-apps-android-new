package com.prima.fragments;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import com.prima.activities.MchatrQuestionActivity;
import com.prima.activities.MchatrResultActivity;
import com.prima.entities.Question;
import prima.test.prima.R;

/**
 * Created by Codelabs on 19/08/2015.
 */
public class MchatrQuestionFragment extends BaseFragment implements View.OnClickListener {
    private Context mContext;
    public static final String ARG_PAGE = "page 2";
    private ArrayList<String> listItem = new ArrayList<String>();
    private ViewPager mPager;
    List<Fragment> fragmentList = new ArrayList<Fragment>();
    Question.Kuesioner kuesioner;
    String selected = "";
    int indeks;
    private ArrayList<String> ansewer = new ArrayList<String>();
    TextView question;
    Button ya, tidak;
    ImageView image_question;


    @SuppressLint("ValidFragment")
    public MchatrQuestionFragment() {}
    @SuppressLint("ValidFragment")
    public MchatrQuestionFragment(Question.Kuesioner kuesioner, int indeks) {
        this.kuesioner = kuesioner;
        this.indeks = indeks;
    }

    @Override
    public void initView(View view) {
        ProgressBar progres = (ProgressBar) view.findViewById(R.id.progres);
        progres.setVisibility(View.GONE);
        mContext = getActivity();
        question = (TextView) view.findViewById(R.id.question);
        ya = (Button) view.findViewById(R.id.ya);
        tidak = (Button) view.findViewById(R.id.tidak);
        mPager = (ViewPager) getActivity().findViewById(R.id.pager);
        image_question = (ImageView) view.findViewById(R.id.image_question);
        image_question.setVisibility(View.GONE);
        question.setText(kuesioner.getTitle());
        Activity act = getActivity();

        if (act instanceof MchatrQuestionActivity) {
            fragmentList = ((MchatrQuestionActivity) act).getFragmentList();
            ansewer = ((MchatrQuestionActivity) act).getAnsewer();
        }

        ya.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ansewer.set(indeks, kuesioner.getAnswer_option().get(0).getScore());
                int current = mPager.getCurrentItem();
                if (current != fragmentList.size() - 1) {
                    mPager.setCurrentItem(current + 1, true);
                } else {
                    int score = 0;
                    for (int j = 0; j < ansewer.size(); j++) {
                        if (ansewer.get(j).replace(".00", "").equals("")) {
                            score = score + 0;
                        } else {
                            score = score + Integer.valueOf(ansewer.get(j).replace(".00", ""));
                        }
                    }
                    Intent i = new Intent(mContext, MchatrResultActivity.class);
                    i.putExtra("score", score);
                    i.putExtra("soal", String.valueOf(ansewer.size()));
                    startActivity(i);
                    getActivity().finish();
                }

            }
        });

        tidak.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ansewer.set(indeks, kuesioner.getAnswer_option().get(1).getScore());
                int current = mPager.getCurrentItem();
                if (current != fragmentList.size() - 1) {
                    mPager.setCurrentItem(current + 1, true);
                } else {
                    int score = 0;
                    for (int j = 0; j < ansewer.size(); j++) {
                        if (ansewer.get(j).replace(".00", "").equals("")) {
                            score = score + 0;
                        } else {
                            score = score + Integer.valueOf(ansewer.get(j).replace(".00", ""));
                        }
                    }
                    Intent i = new Intent(mContext, MchatrResultActivity.class);

                    i.putExtra("score", score);
                    i.putExtra("soal", String.valueOf(ansewer.size()));
                    startActivity(i);
                    getActivity().finish();
                }

            }
        });


    }

    @Override
    public void setUICallbacks() {

    }

    @Override
    public void updateUI() {

    }

    @Override
    public String getPageTitle() {
        return "Account Summary";
    }


    @Override
    public int getFragmentLayout() {
        return R.layout.fragment_kpsp_question;
    }

    @Override
    public void onClick(View v) {

    }


}