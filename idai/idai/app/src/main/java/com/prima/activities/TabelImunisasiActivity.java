package com.prima.activities;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.WebView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.JsonSyntaxException;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.prima.MainActivity;
import com.prima.Preferences;
import com.prima.adapters.AdapterLisImunisasi;
import com.prima.controllers.ApiReferences;
import com.prima.controllers.Parser;
import com.prima.entities.DaftarAnak;
import com.prima.entities.KalenderImunisasi;
import com.prima.entities.Message;
import com.prima.entities.VaksinPasien;
import com.prima.helpers.DateHelper;

import org.json.JSONException;
import org.json.JSONObject;
import org.lucasr.twowayview.TwoWayView;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import prima.test.prima.R;

import static prima.test.prima.R.id.item_vaksin_anak;

/**
 * Created by Codelabs on 03/09/2015.
 */
public class TabelImunisasiActivity extends BaseActivity implements View.OnClickListener {

    DateHelper dateHelper;
    private ArrayAdapter<String> childListAdapter;
    private RequestQueue mRequest;
    private TextView title, txtmerk, txttanggal, txtjenisvaksin;
    private ImageView back;
    private ImageView close;
    MaterialDialog dialog;
    private Button bagikan, keterangan, buttonJadwal, buttonJadwalSudah;
    String url;
    private ScrollView scrollTabelImunisasi, scrollTabelPostImunisasi, scrollTabelConfirmPostImunisasi, scrollTabelListImunisasi;
    private Button buttonImunisasiOk, buttonImunisasiNo, buttonSimpanImunisasi;
    String selectedVaccineTxt,vaksinId, vaksinIdtxt;
    Spinner spinnernamavaksin, spinner_child_name_imunisasi;
    EditText merekvaksintxt;
    RelativeLayout selectedVaccine,
            hepa1,
            hepa2,
            hepa3,
            polio1,
            polio2,
            polio3,
            polio4,
            polio5,
            polio6,
            bcg1,
            dtp1,
            dtp2,
            dtp3,
            dtp4,
            dtp5,
            dtp6,
            dtp7,
            hib1,
            hib2,
            hib3,
            hib4,
            pcv1,
            pcv2,
            pcv3,
            pcv4,
            rotavirus1,
            rotavirus2,
            rotavirus3,
            influenza1,
            campak1,
            campak2,
            campak3,
            mmr1,
            mmr2,
            tifoid1,
            hepatitisa1,
            varisela1,
            hpv1;

    ArrayList<RelativeLayout> list = new ArrayList<RelativeLayout>();

    ArrayList<String> daftarNamaAnak = new ArrayList<String>();

    LinearLayout listDaftarAnak, listDaftarAnakVaksin, linearNamaAnak, edit_hapus_vaksin;

    String tempJenisVaksin, TempTanggal, TempMerekVaksin;

    @Override
    public void initView() {

        dateHelper = new DateHelper();

        mRequest = Volley.newRequestQueue(getApplicationContext());
        listDaftarAnak = (LinearLayout) findViewById(R.id.list_daftar_anak);

        selectedVaccineTxt = "";

        scrollTabelImunisasi = (ScrollView) findViewById(R.id.scrollTabelImunisasi);
        scrollTabelPostImunisasi = (ScrollView) findViewById(R.id.scrollTabelPostImunisasi);
        scrollTabelConfirmPostImunisasi = (ScrollView) findViewById(R.id.scrollTabelConfirmPostImunisasi);
        scrollTabelListImunisasi = (ScrollView) findViewById(R.id.scrollTabelListImunisasi);

        scrollTabelImunisasi.setVisibility(View.VISIBLE);
        scrollTabelPostImunisasi.setVisibility(View.GONE);
        scrollTabelConfirmPostImunisasi.setVisibility(View.GONE);
        scrollTabelListImunisasi.setVisibility(View.GONE);

        spinnernamavaksin = (Spinner) findViewById(R.id.spinnernamavaksin);
        edit_hapus_vaksin = (LinearLayout) findViewById(R.id.edit_hapus_vaksin);

        linearNamaAnak = (LinearLayout) findViewById(R.id.linearNamaAnak);
        if(preferences.getString(Preferences.TIPE_USER, "").equalsIgnoreCase("awam")){
            linearNamaAnak.setVisibility(View.VISIBLE);
        }else{
            linearNamaAnak.setVisibility(View.GONE);
        }

        merekvaksintxt = (EditText) findViewById(R.id.merekvaksintxt);

        buttonImunisasiOk = (Button) findViewById(R.id.buttonImunisasiOk);
        buttonImunisasiNo = (Button) findViewById(R.id.buttonImunisasiNo);
        buttonSimpanImunisasi = (Button) findViewById(R.id.buttonSimpanImunisasi);

        buttonJadwal = (Button) findViewById(R.id.buttonJadwal);
        buttonJadwalSudah  = (Button) findViewById(R.id.buttonJadwalSudah);

        //txtmerk = (TextView) findViewById(R.id.txtmerk);
        txttanggal = (TextView) findViewById(R.id.txttanggal);
        //txtjenisvaksin = (TextView) findViewById(R.id.txtjenisvaksin);

        hepa1 = (RelativeLayout) findViewById(R.id.hepa1);
        hepa2 = (RelativeLayout) findViewById(R.id.hepa2);
        hepa3 = (RelativeLayout) findViewById(R.id.hepa3);
        polio1 = (RelativeLayout) findViewById(R.id.polio1);
        polio2 = (RelativeLayout) findViewById(R.id.polio2);
        polio3 = (RelativeLayout) findViewById(R.id.polio3);
        polio4 = (RelativeLayout) findViewById(R.id.polio4);
        polio5 = (RelativeLayout) findViewById(R.id.polio5);
        polio6 = (RelativeLayout) findViewById(R.id.polio6);
        bcg1 = (RelativeLayout) findViewById(R.id.bcg1);
        dtp1 = (RelativeLayout) findViewById(R.id.dtp1);
        dtp2 = (RelativeLayout) findViewById(R.id.dtp2);
        dtp3 = (RelativeLayout) findViewById(R.id.dtp3);
        dtp4 = (RelativeLayout) findViewById(R.id.dtp4);
        dtp5 = (RelativeLayout) findViewById(R.id.dtp5);
        dtp6 = (RelativeLayout) findViewById(R.id.dtp6);
        dtp7 = (RelativeLayout) findViewById(R.id.dtp7);
        hib1 = (RelativeLayout) findViewById(R.id.hib1);
        hib2 = (RelativeLayout) findViewById(R.id.hib2);
        hib3 = (RelativeLayout) findViewById(R.id.hib3);
        hib4 = (RelativeLayout) findViewById(R.id.hib4);
        pcv1 = (RelativeLayout) findViewById(R.id.pcv1);
        pcv2 = (RelativeLayout) findViewById(R.id.pcv2);
        pcv3 = (RelativeLayout) findViewById(R.id.pcv3);
        pcv4 = (RelativeLayout) findViewById(R.id.pcv4);
        rotavirus1 = (RelativeLayout) findViewById(R.id.rotavirus1);
        rotavirus2 = (RelativeLayout) findViewById(R.id.rotavirus2);
        rotavirus3 = (RelativeLayout) findViewById(R.id.rotavirus3);
        influenza1 = (RelativeLayout) findViewById(R.id.influenza);
        campak1 = (RelativeLayout) findViewById(R.id.campak1);
        campak2 = (RelativeLayout) findViewById(R.id.campak2);
        campak3 = (RelativeLayout) findViewById(R.id.campak3);
        mmr1 = (RelativeLayout) findViewById(R.id.mmr1);
        mmr2 = (RelativeLayout) findViewById(R.id.mmr2);
        tifoid1 = (RelativeLayout) findViewById(R.id.tifoid1);
        hepatitisa1 = (RelativeLayout) findViewById(R.id.hepatitisa1);
        varisela1 = (RelativeLayout) findViewById(R.id.varisela1);
        hpv1 = (RelativeLayout) findViewById(R.id.hpv1);

        hepa1.setBackgroundResource(R.drawable.table_border_blue_imunisasi);
        hepa2.setBackgroundResource(R.drawable.table_border_blue_imunisasi);
        hepa3.setBackgroundResource(R.drawable.table_border_blue_imunisasi);
        polio1.setBackgroundResource(R.drawable.table_border_blue_imunisasi);
        polio2.setBackgroundResource(R.drawable.table_border_blue_imunisasi);
        polio3.setBackgroundResource(R.drawable.table_border_blue_imunisasi);
        polio4.setBackgroundResource(R.drawable.table_border_blue_imunisasi);
        polio5.setBackgroundResource(R.drawable.table_border_blue_imunisasi);
        polio6.setBackgroundResource(R.drawable.table_border_blue_imunisasi);
        bcg1.setBackgroundResource(R.drawable.table_border_blue_imunisasi);
        dtp1.setBackgroundResource(R.drawable.table_border_blue_imunisasi);
        dtp2.setBackgroundResource(R.drawable.table_border_blue_imunisasi);
        dtp3.setBackgroundResource(R.drawable.table_border_blue_imunisasi);
        dtp4.setBackgroundResource(R.drawable.table_border_blue_imunisasi);
        dtp5.setBackgroundResource(R.drawable.table_border_blue_imunisasi);
        dtp6.setBackgroundResource(R.drawable.table_border_blue_imunisasi);
        dtp7.setBackgroundResource(R.drawable.table_border_blue_imunisasi);
        hib1.setBackgroundResource(R.drawable.table_border_blue_imunisasi);
        hib2.setBackgroundResource(R.drawable.table_border_blue_imunisasi);
        hib3.setBackgroundResource(R.drawable.table_border_blue_imunisasi);
        hib4.setBackgroundResource(R.drawable.table_border_blue_imunisasi);
        pcv1.setBackgroundResource(R.drawable.table_border_blue_imunisasi);
        pcv2.setBackgroundResource(R.drawable.table_border_blue_imunisasi);
        pcv3.setBackgroundResource(R.drawable.table_border_blue_imunisasi);
        pcv4.setBackgroundResource(R.drawable.table_border_blue_imunisasi);
        rotavirus1.setBackgroundResource(R.drawable.table_border_blue_imunisasi);
        rotavirus2.setBackgroundResource(R.drawable.table_border_blue_imunisasi);
        rotavirus3.setBackgroundResource(R.drawable.table_border_blue_imunisasi);
        influenza1.setBackgroundResource(R.drawable.table_border_blue_imunisasi);
        campak1.setBackgroundResource(R.drawable.table_border_blue_imunisasi);
        campak2.setBackgroundResource(R.drawable.table_border_blue_imunisasi);
        campak3.setBackgroundResource(R.drawable.table_border_blue_imunisasi);
        mmr1.setBackgroundResource(R.drawable.table_border_blue_imunisasi);
        mmr2.setBackgroundResource(R.drawable.table_border_blue_imunisasi);
        tifoid1.setBackgroundResource(R.drawable.table_border_blue_imunisasi);
        hepatitisa1.setBackgroundResource(R.drawable.table_border_blue_imunisasi);
        varisela1.setBackgroundResource(R.drawable.table_border_blue_imunisasi);
        hpv1.setBackgroundResource(R.drawable.table_border_blue_imunisasi);

        list.add(hepa1);
        list.add(hepa2);
        list.add(hepa3);
        list.add(polio1);
        list.add(polio2);
        list.add(polio3);
        list.add(polio4);
        list.add(polio5);
        list.add(polio6);
        list.add(bcg1);
        list.add(dtp1);
        list.add(dtp2);
        list.add(dtp3);
        list.add(dtp4);
        list.add(dtp5);
        list.add(dtp6);
        list.add(dtp7);
        list.add(hib1);
        list.add(hib2);
        list.add(hib3);
        list.add(hib4);
        list.add(pcv1);
        list.add(pcv2);
        list.add(pcv3);
        list.add(pcv4);
        list.add(rotavirus1);
        list.add(rotavirus2);
        list.add(rotavirus3);
        list.add(influenza1);
        list.add(campak1);
        list.add(campak2);
        list.add(campak3);
        list.add(mmr1);
        list.add(mmr2);
        list.add(tifoid1);
        list.add(hepatitisa1);
        list.add(varisela1);
        list.add(hpv1);

        for (int j = 0; j < list.size(); j++) {
            for (int i = 0; i < list.get(j).getChildCount(); i++) {
                TextView v = (TextView) list.get(j).getChildAt(i);
                //v.setTextColor(Color.WHITE);
            }
        }

/*
        if (getMonth() >= 0) {
            hepa1.setBackgroundResource(R.drawable.table_green_black_border);
            polio1.setBackgroundResource(R.drawable.table_green_black_border);
            bcg1.setBackgroundResource(R.drawable.table_green_black_border);

            changeColor(hepa1);
            changeColor(polio1);
            changeColor(bcg1);
        }

        if (getMonth() >= 0 && getMonth() >= 1) {
            hepa2.setBackgroundResource(R.drawable.table_green_black_border);
            polio1.setBackgroundResource(R.drawable.table_green_black_border);

            changeColor(hepa2);
            changeColor(polio1);
        }

        if (getMonth() >= 0 && getMonth() >= 2) {
            polio2.setBackgroundResource(R.drawable.table_green_black_border);
            dtp1.setBackgroundResource(R.drawable.table_green_black_border);
            hib1.setBackgroundResource(R.drawable.table_green_black_border);
            pcv1.setBackgroundResource(R.drawable.table_green_black_border);
            rotavirus1.setBackgroundResource(R.drawable.table_green_black_border);

            changeColor(polio2);
            changeColor(dtp1);
            changeColor(hib1);
            changeColor(pcv1);
            changeColor(rotavirus1);
        }

        if (getMonth() >= 0 && getMonth() >= 4) {
            polio3.setBackgroundResource(R.drawable.table_green_black_border);
            dtp2.setBackgroundResource(R.drawable.table_green_black_border);
            hib2.setBackgroundResource(R.drawable.table_green_black_border);
            pcv2.setBackgroundResource(R.drawable.table_green_black_border);
            rotavirus2.setBackgroundResource(R.drawable.table_green_black_border);

            changeColor(polio3);
            changeColor(dtp2);
            changeColor(hib2);
            changeColor(pcv2);
            changeColor(rotavirus2);
        }

        if (getMonth() >= 0 && getMonth() >= 6) {
            hepa3.setBackgroundResource(R.drawable.table_green_black_border);
            polio4.setBackgroundResource(R.drawable.table_green_black_border);
            dtp3.setBackgroundResource(R.drawable.table_green_black_border);
            hib3.setBackgroundResource(R.drawable.table_green_black_border);
            pcv3.setBackgroundResource(R.drawable.table_green_black_border);
            rotavirus3.setBackgroundResource(R.drawable.table_green_black_border);
            influenza1.setBackgroundResource(R.drawable.table_green_black_border);

            changeColor(hepa3);
            changeColor(polio4);
            changeColor(dtp3);
            changeColor(hib3);
            changeColor(pcv3);
            changeColor(rotavirus3);
            changeColor(influenza1);
        }

        if (getMonth() >= 0 && getMonth() >= 9) {
            campak1.setBackgroundResource(R.drawable.table_green_black_border);
            changeColor(campak1);
        }

        if (getMonth() >= 0 && getMonth() >= 12) {
            pcv4.setBackgroundResource(R.drawable.table_green_black_border);
            varisela1.setBackgroundResource(R.drawable.table_green_black_border);
            changeColor(pcv4);
            changeColor(varisela1);
        }

        if (getMonth() >= 0 && getMonth() >= 15) {
            hib4.setBackgroundResource(R.drawable.table_green_black_border);
            mmr1.setBackgroundResource(R.drawable.table_green_black_border);
            changeColor(hib4);
            changeColor(mmr1);
        }

        if (getMonth() >= 0 && getMonth() >= 18) {
            polio5.setBackgroundResource(R.drawable.table_green_black_border);
            dtp4.setBackgroundResource(R.drawable.table_green_black_border);
            changeColor(polio5);
            changeColor(dtp4);
        }

        if (getMonth() >= 0 && getMonth() >= 24) {
            campak2.setBackgroundResource(R.drawable.table_green_black_border);
            tifoid1.setBackgroundResource(R.drawable.table_green_black_border);
            hepatitisa1.setBackgroundResource(R.drawable.table_green_black_border);

            changeColor(campak2);
            changeColor(tifoid1);
            changeColor(hepatitisa1);
        }

        if (getMonth() >= 0 && getMonth() >= 60) {
            polio6.setBackgroundResource(R.drawable.table_green_black_border);
            dtp5.setBackgroundResource(R.drawable.table_green_black_border);
            mmr2.setBackgroundResource(R.drawable.table_green_black_border);

            changeColor(polio6);
            changeColor(dtp5);
            changeColor(mmr2);
        }

        if (getMonth() >= 0 && getMonth() >= 72) {
            campak3.setBackgroundResource(R.drawable.table_green_black_border);
            changeColor(campak3);
        }

        if (getMonth() >= 0 && getMonth() >= 120) {
            dtp6.setBackgroundResource(R.drawable.table_green_black_border);
            changeColor(dtp6);
        }

        if (getMonth() >= 0 && getMonth() >= 216) {
            dtp7.setBackgroundResource(R.drawable.table_green_black_border);
            changeColor(dtp7);
        }
*/

        keterangan = (Button) findViewById(R.id.keterangan);
        bagikan = (Button) findViewById(R.id.bagikan);
        close = (ImageView) findViewById(R.id.close);
        back = (ImageView) findViewById(R.id.back);
        close.setVisibility(View.GONE);
        bagikan.setVisibility(View.GONE);
        title = (TextView) findViewById(R.id.tv_actionbar_title);
        Typeface tf = Typeface.createFromAsset(getApplicationContext().getAssets(), "BREVIA-BLACK.OTF");
        title.setTypeface(tf);
        title.setText(getResources().getString(R.string.jadwalImunisasi));


    }


    void getKalender() {
        progresDialog(false, getResources().getString(R.string.Mengunduh));
        Bundle bn = getIntent().getExtras();
        if (bn != null) {
            Ion.with(context)
                    .load(ApiReferences.getImageUrl() + url)
                    .write(new File("/sdcard/prima/image/tabel_imunisasi.png"))
                    .setCallback(new FutureCallback<File>() {
                        @Override
                        public void onCompleted(Exception e, File files) {
                            // download done...
                            // do stuff with the File or error
                            dialog.dismiss();
                            File file = files;
                            try {
                                if (e.toString().contains("ETIMEDOUT")) {
                                    deletePdf("/sdcard/prima/image/tabel_imunisasi.png");
                                    file = null;
                                }
                            } catch (NullPointerException err) {
                                err.printStackTrace();
                            }
                            if (file == null) {
                                dialog(getResources().getString(R.string.GagalMengunduhPdf), getResources().getString(R.string.Perhatian));
                            } else {
                                String badanEmail;
                                String jk;
                                if (preferences.getString(Preferences.ID, "").equalsIgnoreCase("") || preferences.getString(Preferences.JENIS_KELAMIN, "").equalsIgnoreCase("")) {
                                    badanEmail = "";
                                } else {
                                    if (preferences.getString(Preferences.JENIS_KELAMIN, "").equals("0")) {
                                        jk = "Perempuan";
                                    } else {
                                        jk = "Laki-Laki";
                                    }

                                    badanEmail = "Kode Pasien : " + preferences.getString(Preferences.ID, "") +
                                            "\nTanggal Lahir : " + preferences.getString(Preferences.BIRTH_DAY, "") +
                                            "\nBerat Badan : " + preferences.getString(Preferences.BERAT, "") +
                                            "\nTinggi Badan : " + preferences.getString(Preferences.TINGGI, "") +
                                            "\nJenis Kelamin : " + jk;
                                }
                                Intent sendEmail = new Intent(Intent.ACTION_SEND);
                                sendEmail.setType("jpeg/image");
                                sendEmail.putExtra(Intent.EXTRA_EMAIL, new String[]{""});
                                sendEmail.putExtra(Intent.EXTRA_SUBJECT, "Kalender Imunisasi");
                                sendEmail.putExtra(Intent.EXTRA_TEXT, "Berikut ini adalah kalender imunisasi\n\n" + badanEmail);
                                sendEmail.putExtra(Intent.EXTRA_STREAM, Uri.parse("file:///sdcard/prima/image/tabel_imunisasi.png"));
                                startActivity(Intent.createChooser(sendEmail, "Email With"));
                            }
                        }
                    });
        }
    }


    @Override
    public void setUICallbacks() {
        back.setOnClickListener(this);
        bagikan.setOnClickListener(this);
        keterangan.setOnClickListener(this);

        buttonImunisasiOk.setOnClickListener(this);
        buttonImunisasiNo.setOnClickListener(this);
        buttonSimpanImunisasi.setOnClickListener(this);

        txttanggal.setOnClickListener(this);


        if(preferences.getString(Preferences.TIPE_USER, "").equalsIgnoreCase("awam")) {
            hepa1.setOnClickListener(this);
            hepa2.setOnClickListener(this);
            hepa3.setOnClickListener(this);
            polio1.setOnClickListener(this);
            polio2.setOnClickListener(this);
            polio3.setOnClickListener(this);
            polio4.setOnClickListener(this);
            polio5.setOnClickListener(this);
            polio6.setOnClickListener(this);
            bcg1.setOnClickListener(this);
            dtp1.setOnClickListener(this);
            dtp2.setOnClickListener(this);
            dtp3.setOnClickListener(this);
            dtp4.setOnClickListener(this);
            dtp5.setOnClickListener(this);
            dtp6.setOnClickListener(this);
            dtp7.setOnClickListener(this);
            hib1.setOnClickListener(this);
            hib2.setOnClickListener(this);
            hib3.setOnClickListener(this);
            hib4.setOnClickListener(this);
            pcv1.setOnClickListener(this);
            pcv2.setOnClickListener(this);
            pcv3.setOnClickListener(this);
            pcv4.setOnClickListener(this);
            rotavirus1.setOnClickListener(this);
            rotavirus2.setOnClickListener(this);
            rotavirus3.setOnClickListener(this);
            influenza1.setOnClickListener(this);
            campak1.setOnClickListener(this);
            campak2.setOnClickListener(this);
            campak3.setOnClickListener(this);
            mmr1.setOnClickListener(this);
            mmr2.setOnClickListener(this);
            tifoid1.setOnClickListener(this);
            hepatitisa1.setOnClickListener(this);
            varisela1.setOnClickListener(this);
            hpv1.setOnClickListener(this);
            buttonJadwal.setOnClickListener(this);
            buttonJadwalSudah.setOnClickListener(this);

        } else {
            buttonJadwal.setOnClickListener(this);
            buttonJadwalSudah.setOnClickListener(this);
        }
    }



    @Override
    public void onClick(View v) {
        if (v == back) {
            Intent i = new Intent(TabelImunisasiActivity.this, MainActivity.class);
            startActivity(i);
            finish();
        } else if (v == bagikan) {
            getKalender();
        } else if (v == keterangan) {
            // get prompts.xml view
            LayoutInflater li = LayoutInflater.from(getApplicationContext());
            View dialogView = li.inflate(R.layout.dialog_keterangan_imunisasi, null);
            final Dialog dialog3;
            dialog3 = new Dialog(context);
            dialog3.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog3.setContentView(dialogView);
            //make transparent dialog_sesi_offline_pelod

            if (preferences.getString(Preferences.LANGUAGE, "").equalsIgnoreCase("id")) {
                WebView wv;
                wv = (WebView) dialogView.findViewById(R.id.webview);
                wv.loadUrl("file:///android_asset/webview.html");
            } else {
                WebView wv;
                wv = (WebView) dialogView.findViewById(R.id.webview);
                wv.loadUrl("file:///android_asset/webviewen.html");
            }

            dialog3.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
            lp.copyFrom(dialog3.getWindow().getAttributes());
            lp.width = WindowManager.LayoutParams.MATCH_PARENT;
            lp.height = WindowManager.LayoutParams.MATCH_PARENT;
            dialog3.show();
            dialog3.getWindow().setAttributes(lp);
            Button dialogLanjut = (Button) dialogView.findViewById(R.id.lanjut);

            dialogLanjut.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog3.dismiss();
                }
            });
        }else if (v == hepa1) {
            scrollTabelImunisasi.setVisibility(View.GONE);
            scrollTabelPostImunisasi.setVisibility(View.VISIBLE);
            scrollTabelConfirmPostImunisasi.setVisibility(View.GONE);
            selectedVaccine = hepa1;
            vaksinId = "hepa1";
            selectedVaccineTxt = "Hepatitis B";
            spinnernamavaksin.setSelection(3);
        }else if (v == hepa2) {
            scrollTabelImunisasi.setVisibility(View.GONE);
            scrollTabelPostImunisasi.setVisibility(View.VISIBLE);
            scrollTabelConfirmPostImunisasi.setVisibility(View.GONE);
            selectedVaccine = hepa2;
            vaksinId = "hepa2";
            selectedVaccineTxt = "Hepatitis B";
            spinnernamavaksin.setSelection(3);

        }else if (v == hepa3) {
            scrollTabelImunisasi.setVisibility(View.GONE);
            scrollTabelPostImunisasi.setVisibility(View.VISIBLE);
            scrollTabelConfirmPostImunisasi.setVisibility(View.GONE);
            selectedVaccine = hepa3;
            vaksinId = "hepa3";
            selectedVaccineTxt = "Hepatitis B";
            spinnernamavaksin.setSelection(3);

        }else if (v == polio1) {
            scrollTabelImunisasi.setVisibility(View.GONE);
            scrollTabelPostImunisasi.setVisibility(View.VISIBLE);
            scrollTabelConfirmPostImunisasi.setVisibility(View.GONE);
            selectedVaccine = polio1;
            vaksinId = "polio1";
            selectedVaccineTxt = "Polio";
            spinnernamavaksin.setSelection(1);

        }else if (v == polio2) {
            scrollTabelImunisasi.setVisibility(View.GONE);
            scrollTabelPostImunisasi.setVisibility(View.VISIBLE);
            scrollTabelConfirmPostImunisasi.setVisibility(View.GONE);
            selectedVaccine = polio2;
            vaksinId = "polio2";
            selectedVaccineTxt = "Polio";
            spinnernamavaksin.setSelection(1);

        }else if (v == polio3) {
            scrollTabelImunisasi.setVisibility(View.GONE);
            scrollTabelPostImunisasi.setVisibility(View.VISIBLE);
            scrollTabelConfirmPostImunisasi.setVisibility(View.GONE);
            selectedVaccine = polio3;
            vaksinId = "polio3";
            selectedVaccineTxt = "Polio";
            spinnernamavaksin.setSelection(1);

        }else if (v == polio4) {
            scrollTabelImunisasi.setVisibility(View.GONE);
            scrollTabelPostImunisasi.setVisibility(View.VISIBLE);
            scrollTabelConfirmPostImunisasi.setVisibility(View.GONE);
            selectedVaccine = polio4;
            vaksinId = "polio4";
            selectedVaccineTxt = "Polio";
            spinnernamavaksin.setSelection(1);

        }else if (v == polio5) {
            scrollTabelImunisasi.setVisibility(View.GONE);
            scrollTabelPostImunisasi.setVisibility(View.VISIBLE);
            scrollTabelConfirmPostImunisasi.setVisibility(View.GONE);
            selectedVaccine = polio5;
            vaksinId = "polio5";
            selectedVaccineTxt = "Polio";
            spinnernamavaksin.setSelection(1);

        }else if (v == polio6) {
            scrollTabelImunisasi.setVisibility(View.GONE);
            scrollTabelPostImunisasi.setVisibility(View.VISIBLE);
            scrollTabelConfirmPostImunisasi.setVisibility(View.GONE);
            selectedVaccine = polio5;
            vaksinId = "polio6";
            selectedVaccineTxt = "Polio";
            spinnernamavaksin.setSelection(1);

        }else if (v == bcg1) {
            scrollTabelImunisasi.setVisibility(View.GONE);
            scrollTabelPostImunisasi.setVisibility(View.VISIBLE);
            scrollTabelConfirmPostImunisasi.setVisibility(View.GONE);
            selectedVaccine = bcg1;
            vaksinId = "bcg1";
            selectedVaccineTxt = "BCG";
            spinnernamavaksin.setSelection(0);

        }else if (v == dtp1) {
            scrollTabelImunisasi.setVisibility(View.GONE);
            scrollTabelPostImunisasi.setVisibility(View.VISIBLE);
            scrollTabelConfirmPostImunisasi.setVisibility(View.GONE);
            selectedVaccine = dtp1;
            vaksinId = "dtp1";
            selectedVaccineTxt = "DTP";
            spinnernamavaksin.setSelection(2);

        }else if (v == dtp2) {
            scrollTabelImunisasi.setVisibility(View.GONE);
            scrollTabelPostImunisasi.setVisibility(View.VISIBLE);
            scrollTabelConfirmPostImunisasi.setVisibility(View.GONE);
            selectedVaccine = dtp2;
            vaksinId = "dtp2";
            selectedVaccineTxt = "DPT";
            spinnernamavaksin.setSelection(2);

        }else if (v == dtp3) {
            scrollTabelImunisasi.setVisibility(View.GONE);
            scrollTabelPostImunisasi.setVisibility(View.VISIBLE);
            scrollTabelConfirmPostImunisasi.setVisibility(View.GONE);
            selectedVaccine = dtp3;
            vaksinId = "dtp3";
            selectedVaccineTxt = "DPT";
            spinnernamavaksin.setSelection(2);

        }else if (v == dtp4) {
            scrollTabelImunisasi.setVisibility(View.GONE);
            scrollTabelPostImunisasi.setVisibility(View.VISIBLE);
            scrollTabelConfirmPostImunisasi.setVisibility(View.GONE);
            selectedVaccine = dtp4;
            vaksinId = "dtp4";
            selectedVaccineTxt = "DPT";
            spinnernamavaksin.setSelection(2);

        }else if (v == dtp5) {
            scrollTabelImunisasi.setVisibility(View.GONE);
            scrollTabelPostImunisasi.setVisibility(View.VISIBLE);
            scrollTabelConfirmPostImunisasi.setVisibility(View.GONE);
            selectedVaccine = dtp5;
            vaksinId = "dtp5";
            selectedVaccineTxt = "DPT";
            spinnernamavaksin.setSelection(2);

        }else if (v == dtp6) {
            scrollTabelImunisasi.setVisibility(View.GONE);
            scrollTabelPostImunisasi.setVisibility(View.VISIBLE);
            scrollTabelConfirmPostImunisasi.setVisibility(View.GONE);
            selectedVaccine = dtp6;
            vaksinId = "dtp6";
            selectedVaccineTxt = "DPT";
            spinnernamavaksin.setSelection(2);

        }else if (v == dtp7) {
            scrollTabelImunisasi.setVisibility(View.GONE);
            scrollTabelPostImunisasi.setVisibility(View.VISIBLE);
            scrollTabelConfirmPostImunisasi.setVisibility(View.GONE);
            selectedVaccine = dtp7;
            vaksinId = "dtp7";
            selectedVaccineTxt = "DPT";
            spinnernamavaksin.setSelection(2);

        }else if (v == hib1) {
            scrollTabelImunisasi.setVisibility(View.GONE);
            scrollTabelPostImunisasi.setVisibility(View.VISIBLE);
            scrollTabelConfirmPostImunisasi.setVisibility(View.GONE);
            selectedVaccine = hib1;
            vaksinId = "hib1";
            selectedVaccineTxt = "Hib";
            spinnernamavaksin.setSelection(5);

        }else if (v == hib2) {
            scrollTabelImunisasi.setVisibility(View.GONE);
            scrollTabelPostImunisasi.setVisibility(View.VISIBLE);
            scrollTabelConfirmPostImunisasi.setVisibility(View.GONE);
            selectedVaccine = hib2;
            vaksinId = "hib2";
            selectedVaccineTxt = "Hib";
            spinnernamavaksin.setSelection(5);

        }else if (v == hib3) {
            scrollTabelImunisasi.setVisibility(View.GONE);
            scrollTabelPostImunisasi.setVisibility(View.VISIBLE);
            scrollTabelConfirmPostImunisasi.setVisibility(View.GONE);
            selectedVaccine = hib3;
            vaksinId = "hib3";
            selectedVaccineTxt = "Hib";
            spinnernamavaksin.setSelection(5);

        }else if (v == hib4) {
            scrollTabelImunisasi.setVisibility(View.GONE);
            scrollTabelPostImunisasi.setVisibility(View.VISIBLE);
            scrollTabelConfirmPostImunisasi.setVisibility(View.GONE);
            selectedVaccine = hib4;
            vaksinId = "hib4";
            selectedVaccineTxt = "Hib";
            spinnernamavaksin.setSelection(5);

        }else if (v == pcv1) {
            scrollTabelImunisasi.setVisibility(View.GONE);
            scrollTabelPostImunisasi.setVisibility(View.VISIBLE);
            scrollTabelConfirmPostImunisasi.setVisibility(View.GONE);
            selectedVaccine = pcv1;
            vaksinId = "pcv1";
            selectedVaccineTxt = "PCV";
            spinnernamavaksin.setSelection(11);

        }else if (v == pcv2) {
            scrollTabelImunisasi.setVisibility(View.GONE);
            scrollTabelPostImunisasi.setVisibility(View.VISIBLE);
            scrollTabelConfirmPostImunisasi.setVisibility(View.GONE);
            selectedVaccine = pcv2;
            vaksinId = "pcv2";
            selectedVaccineTxt = "PCV";
            spinnernamavaksin.setSelection(11);

        }else if (v == pcv3) {
            scrollTabelImunisasi.setVisibility(View.GONE);
            scrollTabelPostImunisasi.setVisibility(View.VISIBLE);
            scrollTabelConfirmPostImunisasi.setVisibility(View.GONE);
            selectedVaccine = pcv3;
            vaksinId = "pcv3";
            selectedVaccineTxt = "PCV";
            spinnernamavaksin.setSelection(11);

        }else if (v == pcv4) {
            scrollTabelImunisasi.setVisibility(View.GONE);
            scrollTabelPostImunisasi.setVisibility(View.VISIBLE);
            scrollTabelConfirmPostImunisasi.setVisibility(View.GONE);
            selectedVaccine = pcv4;
            vaksinId = "pcv4";
            selectedVaccineTxt = "PCV";
            spinnernamavaksin.setSelection(11);

        }else if (v == rotavirus1) {
            scrollTabelImunisasi.setVisibility(View.GONE);
            scrollTabelPostImunisasi.setVisibility(View.VISIBLE);
            scrollTabelConfirmPostImunisasi.setVisibility(View.GONE);
            selectedVaccine = rotavirus1;
            vaksinId = "rotavirus1";
            selectedVaccineTxt = "Rotarvirus";
            spinnernamavaksin.setSelection(13);

        }else if (v == rotavirus2) {
            scrollTabelImunisasi.setVisibility(View.GONE);
            scrollTabelPostImunisasi.setVisibility(View.VISIBLE);
            scrollTabelConfirmPostImunisasi.setVisibility(View.GONE);
            selectedVaccine = rotavirus2;
            vaksinId = "rotavirus2";
            selectedVaccineTxt = "Rotarvirus";
            spinnernamavaksin.setSelection(13);

        }else if (v == rotavirus3) {
            scrollTabelImunisasi.setVisibility(View.GONE);
            scrollTabelPostImunisasi.setVisibility(View.VISIBLE);
            scrollTabelConfirmPostImunisasi.setVisibility(View.GONE);
            selectedVaccine = rotavirus3;
            vaksinId = "rotavirus3";
            selectedVaccineTxt = "Rotarvirus";
            spinnernamavaksin.setSelection(13);

        }else if (v == influenza1) {
            scrollTabelImunisasi.setVisibility(View.GONE);
            scrollTabelPostImunisasi.setVisibility(View.VISIBLE);
            scrollTabelConfirmPostImunisasi.setVisibility(View.GONE);
            selectedVaccine = influenza1;
            vaksinId = "influenza1";
            selectedVaccineTxt = "Influenza";
            spinnernamavaksin.setSelection(10);

        }else if (v == campak1) {scrollTabelImunisasi.setVisibility(View.GONE);
            scrollTabelPostImunisasi.setVisibility(View.VISIBLE);
            scrollTabelConfirmPostImunisasi.setVisibility(View.GONE);
            selectedVaccine = campak1;
            vaksinId = "campak1";
            selectedVaccineTxt = "Campak";
            spinnernamavaksin.setSelection(4);

        }else if (v == campak2) {
            scrollTabelImunisasi.setVisibility(View.GONE);
            scrollTabelPostImunisasi.setVisibility(View.VISIBLE);
            scrollTabelConfirmPostImunisasi.setVisibility(View.GONE);
            selectedVaccine = campak2;
            vaksinId = "campak2";
            selectedVaccineTxt = "Campak";
            spinnernamavaksin.setSelection(4);

        }else if (v == campak3) {
            scrollTabelImunisasi.setVisibility(View.GONE);
            scrollTabelPostImunisasi.setVisibility(View.VISIBLE);
            scrollTabelConfirmPostImunisasi.setVisibility(View.GONE);
            selectedVaccine = campak3;
            vaksinId = "campak3";
            selectedVaccineTxt = "Campak";
            spinnernamavaksin.setSelection(4);

        }else if (v == mmr1) {
            scrollTabelImunisasi.setVisibility(View.GONE);
            scrollTabelPostImunisasi.setVisibility(View.VISIBLE);
            scrollTabelConfirmPostImunisasi.setVisibility(View.GONE);
            selectedVaccine = mmr1;
            vaksinId = "mmr1";
            selectedVaccineTxt = "MMR";
            spinnernamavaksin.setSelection(9);

        }else if (v == mmr2) {
            scrollTabelImunisasi.setVisibility(View.GONE);
            scrollTabelPostImunisasi.setVisibility(View.VISIBLE);
            scrollTabelConfirmPostImunisasi.setVisibility(View.GONE);
            selectedVaccine = mmr2;
            vaksinId = "mmr2";
            selectedVaccineTxt = "MMR";
            spinnernamavaksin.setSelection(9);

        }else if (v == tifoid1) {
            scrollTabelImunisasi.setVisibility(View.GONE);
            scrollTabelPostImunisasi.setVisibility(View.VISIBLE);
            scrollTabelConfirmPostImunisasi.setVisibility(View.GONE);
            selectedVaccine = tifoid1;
            vaksinId = "tifoid1";
            selectedVaccineTxt = "Tifoid";
            spinnernamavaksin.setSelection(6);

        }else if (v == hepatitisa1) {
            scrollTabelImunisasi.setVisibility(View.GONE);
            scrollTabelPostImunisasi.setVisibility(View.VISIBLE);
            scrollTabelConfirmPostImunisasi.setVisibility(View.GONE);
            selectedVaccine = hepatitisa1;
            vaksinId = "hepatitisa1";
            selectedVaccineTxt = "Hepatitis A";
            spinnernamavaksin.setSelection(7);

        }else if (v == varisela1) {
            scrollTabelImunisasi.setVisibility(View.GONE);
            scrollTabelPostImunisasi.setVisibility(View.VISIBLE);
            scrollTabelConfirmPostImunisasi.setVisibility(View.GONE);
            selectedVaccine = varisela1;
            vaksinId = "varisela1";
            selectedVaccineTxt = "Varisela";
            spinnernamavaksin.setSelection(8);

        }else if (v == hpv1) {
            scrollTabelImunisasi.setVisibility(View.GONE);
            scrollTabelPostImunisasi.setVisibility(View.VISIBLE);
            scrollTabelConfirmPostImunisasi.setVisibility(View.GONE);
            selectedVaccine = hpv1;
            vaksinId = "hpv1";
            selectedVaccineTxt = "HPV";
            spinnernamavaksin.setSelection(12);

        }else if (v == buttonImunisasiOk) {
            scrollTabelImunisasi.setVisibility(View.GONE);
            scrollTabelPostImunisasi.setVisibility(View.GONE);
            scrollTabelConfirmPostImunisasi.setVisibility(View.VISIBLE);
            scrollTabelListImunisasi.setVisibility(View.GONE);
        }else if (v == buttonImunisasiNo) {
            scrollTabelImunisasi.setVisibility(View.VISIBLE);
            scrollTabelPostImunisasi.setVisibility(View.GONE);
            scrollTabelConfirmPostImunisasi.setVisibility(View.GONE);
            scrollTabelListImunisasi.setVisibility(View.GONE);
        }else if (v == buttonSimpanImunisasi) {

            if(!spinner_child_name_imunisasi.getSelectedItem().toString().equalsIgnoreCase(getResources().getString(R.string.pilihNamaAnak))){
                scrollTabelImunisasi.setVisibility(View.VISIBLE);
                scrollTabelPostImunisasi.setVisibility(View.GONE);
                scrollTabelConfirmPostImunisasi.setVisibility(View.GONE);
                scrollTabelListImunisasi.setVisibility(View.GONE);
                simpanImunisasiAnak();
                changeColor(selectedVaccine);
            }else{
                dialog(getResources().getString(R.string.PilihNamaAnakTerlebihDahulu), getResources().getString(R.string.Perhatian));
            }

        }else if (v == buttonJadwal) {
            scrollTabelImunisasi.setVisibility(View.VISIBLE);
            scrollTabelPostImunisasi.setVisibility(View.GONE);
            scrollTabelConfirmPostImunisasi.setVisibility(View.GONE);
            scrollTabelListImunisasi.setVisibility(View.GONE);
        }else if (v == buttonJadwalSudah) {
            scrollTabelImunisasi.setVisibility(View.GONE);
            scrollTabelPostImunisasi.setVisibility(View.GONE);
            scrollTabelConfirmPostImunisasi.setVisibility(View.GONE);
            scrollTabelListImunisasi.setVisibility(View.VISIBLE);
        }else if (v == txttanggal) {
            showDatePickerForBirthDay(txttanggal);
        }




        buttonImunisasiOk.setOnClickListener(this);
        buttonImunisasiNo.setOnClickListener(this);
        buttonSimpanImunisasi.setOnClickListener(this);

    }

    //nandha
    private void simpanImunisasiAnak() {

        if(vaksinId.equalsIgnoreCase("hepa1")){
            vaksinIdtxt = "1";
        }else if(vaksinId.equalsIgnoreCase("hepa2")){
            vaksinIdtxt = "2";
        }else if(vaksinId.equalsIgnoreCase("hepa3")){
            vaksinIdtxt = "3";
        }else if(vaksinId.equalsIgnoreCase("polio1")){
            vaksinIdtxt = "4";
        }else if(vaksinId.equalsIgnoreCase("polio2")){
            vaksinIdtxt = "5";
        }else if(vaksinId.equalsIgnoreCase("polio3")){
            vaksinIdtxt = "6";
        }else if(vaksinId.equalsIgnoreCase("polio4")){
            vaksinIdtxt = "7";
        }else if(vaksinId.equalsIgnoreCase("polio5")){
            vaksinIdtxt = "8";
        }else if(vaksinId.equalsIgnoreCase("polio6")){
            vaksinIdtxt = "9";
        }else if(vaksinId.equalsIgnoreCase("bcg1")){
            vaksinIdtxt = "10";
        }else if(vaksinId.equalsIgnoreCase("dtp1")){
            vaksinIdtxt = "11";
        }else if(vaksinId.equalsIgnoreCase("dtp2")){
            vaksinIdtxt = "12";
        }else if(vaksinId.equalsIgnoreCase("dtp3")){
            vaksinIdtxt = "13";
        }else if(vaksinId.equalsIgnoreCase("dtp4")){
            vaksinIdtxt = "14";
        }else if(vaksinId.equalsIgnoreCase("dtp5")){
            vaksinIdtxt = "15";
        }else if(vaksinId.equalsIgnoreCase("dtp6")){
            vaksinIdtxt = "16";
        }else if(vaksinId.equalsIgnoreCase("dtp7")){
            vaksinIdtxt = "17";
        }else if(vaksinId.equalsIgnoreCase("hib1")){
            vaksinIdtxt = "18";
        }else if(vaksinId.equalsIgnoreCase("hib2")){
            vaksinIdtxt = "19";
        }else if(vaksinId.equalsIgnoreCase("hib3")){
            vaksinIdtxt = "20";
        }else if(vaksinId.equalsIgnoreCase("hib4")){
            vaksinIdtxt = "21";
        }else if(vaksinId.equalsIgnoreCase("pcv1")){
            vaksinIdtxt = "22";
        }else if(vaksinId.equalsIgnoreCase("pcv2")){
            vaksinIdtxt = "23";
        }else if(vaksinId.equalsIgnoreCase("pcv3")){
            vaksinIdtxt = "24";
        }else if(vaksinId.equalsIgnoreCase("pcv4")){
            vaksinIdtxt = "25";
        }else if(vaksinId.equalsIgnoreCase("rotavirus1")){
            vaksinIdtxt = "26";
        }else if(vaksinId.equalsIgnoreCase("rotavirus2")){
            vaksinIdtxt = "27";
        }else if(vaksinId.equalsIgnoreCase("rotavirus3")){
            vaksinIdtxt = "28";
        }else if(vaksinId.equalsIgnoreCase("influenza1")){
            vaksinIdtxt = "29";
        }else if(vaksinId.equalsIgnoreCase("campak1")){
            vaksinIdtxt = "30";
        }else if(vaksinId.equalsIgnoreCase("campak2")){
            vaksinIdtxt = "31";
        }else if(vaksinId.equalsIgnoreCase("campak3")){
            vaksinIdtxt = "32";
        }else if(vaksinId.equalsIgnoreCase("mmr1")){
            vaksinIdtxt = "33";
        }else if(vaksinId.equalsIgnoreCase("mmr2") ){
            vaksinIdtxt = "34";
        }else if(vaksinId.equalsIgnoreCase("tifoid1") ){
            vaksinIdtxt = "35";
        }else if(vaksinId.equalsIgnoreCase("hepatitisa1") ){
            vaksinIdtxt = "36";
        }else if(vaksinId.equalsIgnoreCase("varisela1") ){
            vaksinIdtxt = "37";
        }else if(vaksinId.equalsIgnoreCase("hpv1") ){
            vaksinIdtxt = "38";
        }

        System.out.println("name"+spinner_child_name_imunisasi.getSelectedItem().toString()+
                " tanggal" + txttanggal.getText() +
                " vaksin" + spinnernamavaksin.getSelectedItem().toString()+
                " selected vaksin id " + vaksinIdtxt +
                " selected vaksin" + selectedVaccine.getClass().getName().toString() +
                " merek" +merekvaksintxt.getText());


        String namaAnak = spinner_child_name_imunisasi.getSelectedItem().toString();

        StringRequest postlogin = new StringRequest(Request.Method.POST,
                ApiReferences.addVaccineAnak(namaAnak), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    Log.i("RESULT JSON", response.toString());
                    Message message = Parser.getMessage(response
                            .toString());
                    if (message.getStatus().equalsIgnoreCase("1")) {

                        dialog(getResources().getString(R.string.DataBerhasilDitambahkan), getResources().getString(R.string.Perhatian));
                        getData();

                    }
                } catch (JsonSyntaxException e) {
                    dialog(getResources().getString(R.string.KesalahanServer), getResources().getString(R.string.Perhatian));
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                NetworkResponse response = error.networkResponse;
                if (error instanceof ServerError && response != null) {
                    try {
                        String res = new String(response.data,
                                HttpHeaderParser.parseCharset(response.headers, "utf-8"));
                        // Now you can use any deserializer to make sense of data
                        JSONObject obj = new JSONObject(res);
                        dialog(res.toString(), getResources().getString(R.string.Perhatian));
                    } catch (UnsupportedEncodingException e1) {
                        // Couldn't properly decode data to string
                        e1.printStackTrace();
                    } catch (JSONException e2) {
                        // returned data is not JSONObject?
                        e2.printStackTrace();
                    }
                }
            }
        }) {

            protected Map<String, String> getParams() {
                HashMap<String, String> params = new HashMap<String, String>();
                params.put("vaccine_step_id", vaksinIdtxt);
                params.put("vaccine_date", txttanggal.getText().toString());
                params.put("vaccine_brand", merekvaksintxt.getText().toString());
                return params;
            }
        };
        postlogin.setRetryPolicy(new DefaultRetryPolicy(
                20000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        mRequest.add(postlogin);




    }

    private void showDatePickerForBirthDay(final TextView txt) {
        dateHelper.showDatePicker(TabelImunisasiActivity.this, System.currentTimeMillis(), new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                int _year = year;
                int _month = monthOfYear + 1;
                int _day = dayOfMonth;
                String date = _year + "-" + _month + "-" + _day;
                String birthDate = dateHelper.formatDate(date);
                txt.setText(birthDate);
            }
        });
    }

    @Override
    public int getLayout() {
        return R.layout.activity_table_imunisasi_new;
    }

    @Override
    public void updateUI() {

        daftarNamaAnak.clear();

        childListAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, daftarNamaAnak);
        daftarNamaAnak.add(getResources().getString(R.string.pilihNamaAnak));
        childListAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_child_name_imunisasi = (Spinner)findViewById(R.id.spinner_child_name_imunisasi);
        spinner_child_name_imunisasi.setAdapter(childListAdapter);

        spinner_child_name_imunisasi.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener(){

            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int pos, long id){

                String selectedItemText = parent.getItemAtPosition(pos).toString();

                if(pos > 0){
                    resetColor();
                    getDataIntoTable(selectedItemText);
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent){
                //Another interface callback
            }

        });


        Bundle bn = getIntent().getExtras();
        if (bn != null) {

            String respon = bn.getString("respon");
            KalenderImunisasi kalender = Parser.getKalenderImunisasi(respon);
            url = kalender.getFile();
            ArrayList<String> items = new ArrayList<String>();
            for (int i = 0; i < kalender.getObjects().size(); i++) {
                items.add("0");
                items.add("1");
                items.add("2");
                items.add("3");
                items.add("4");
                items.add("5");
                items.add("6");
                items.add("9");
                items.add("12");
                items.add("15");
                items.add("18");
                items.add("24");
                items.add("36");
                items.add("60");
                items.add("72");
                items.add("84");
                items.add("96");
                items.add("120");
                items.add("144");
                items.add("216");
            }

            AdapterLisImunisasi aItems = new AdapterLisImunisasi(items, this, kalender, getMonth());
            TwoWayView lvTest = (TwoWayView) findViewById(R.id.lvItems);
            lvTest.setAdapter(aItems);
            lvTest.smoothScrollToPosition(0, 0);

        }


        if(preferences.getString(Preferences.TIPE_USER, "").equalsIgnoreCase("awam")){
            getData();
        }

    }

    private void getDataIntoTable(String child_detail_name) {
        progresDialog(false, getResources().getString(R.string.MemuatData));
        StringRequest listdata = new StringRequest(Request.Method.GET,
                ApiReferences.getChildIndividualVaccine(child_detail_name),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        dialog.dismiss();
                        try {
                            if (response.equals("[]")) {
                                dialog(getResources().getString(R.string.DataBelumTersedia), getResources().getString(R.string.Perhatian));
                            } else {

                                Message msg = Parser.getMessage(response);
                                if (msg.getStatus().equals("0")) {
                                    dialog(msg.getMessages(), getResources().getString(R.string.Perhatian));
                                } else {
                                    Log.i("RESULT JSON", response.toString());
                                    final VaksinPasien daftarVaksinAnak = Parser.getDaftarVaksinAnak(response);

                                    for (int i = 0; i < daftarVaksinAnak.getObjects().size(); i++) {

                                        String vaksinName = daftarVaksinAnak.getObjects().get(i).getvaccine_step_id();

                                        if(vaksinName.equalsIgnoreCase("1")){
                                            changeColor(hepa1);
                                        }else if(vaksinName.equalsIgnoreCase("2")){
                                            changeColor(hepa2);
                                        }else if(vaksinName.equalsIgnoreCase("3")){
                                            changeColor(hepa3);
                                        }else if(vaksinName.equalsIgnoreCase("4")){
                                            changeColor(polio1);
                                        }else if(vaksinName.equalsIgnoreCase("5")){
                                            changeColor(polio2);
                                        }else if(vaksinName.equalsIgnoreCase("6")){
                                            changeColor(polio3);
                                        }else if(vaksinName.equalsIgnoreCase("7")){
                                            changeColor(polio4);
                                        }else if(vaksinName.equalsIgnoreCase("8")){
                                            changeColor(polio5);
                                        }else if(vaksinName.equalsIgnoreCase("9")){
                                            changeColor(polio6);
                                        }else if(vaksinName.equalsIgnoreCase("10")){
                                            changeColor(bcg1);
                                        }else if(vaksinName.equalsIgnoreCase("11")){
                                            changeColor(dtp1);
                                        }else if(vaksinName.equalsIgnoreCase("12")){
                                            changeColor(dtp2);
                                        }else if(vaksinName.equalsIgnoreCase("13")){
                                            changeColor(dtp3);
                                        }else if(vaksinName.equalsIgnoreCase("14")){
                                            changeColor(dtp4);
                                        }else if(vaksinName.equalsIgnoreCase("15")){
                                            changeColor(dtp5);
                                        }else if(vaksinName.equalsIgnoreCase("16")){
                                            changeColor(dtp6);
                                        }else if(vaksinName.equalsIgnoreCase("17")){
                                            changeColor(dtp7);
                                        }else if(vaksinName.equalsIgnoreCase("18")){
                                            changeColor(hib1);
                                        }else if(vaksinName.equalsIgnoreCase("19")){
                                            changeColor(hib2);
                                        }else if(vaksinName.equalsIgnoreCase("20")){
                                            changeColor(hib3);
                                        }else if(vaksinName.equalsIgnoreCase("21")){
                                            changeColor(hib4);
                                        }else if(vaksinName.equalsIgnoreCase("22")){
                                            changeColor(pcv1);
                                        }else if(vaksinName.equalsIgnoreCase("23")){
                                            changeColor(pcv2);
                                        }else if(vaksinName.equalsIgnoreCase("24")){
                                            changeColor(pcv3);
                                        }else if(vaksinName.equalsIgnoreCase("25")){
                                            changeColor(pcv4);
                                        }else if(vaksinName.equalsIgnoreCase("26")){
                                            changeColor(rotavirus1);
                                        }else if(vaksinName.equalsIgnoreCase("27")){
                                            changeColor(rotavirus2);
                                        }else if(vaksinName.equalsIgnoreCase("28")){
                                            changeColor(rotavirus3);
                                        }else if(vaksinName.equalsIgnoreCase("29")){
                                            changeColor(influenza1);
                                        }else if(vaksinName.equalsIgnoreCase("30")){
                                            changeColor(campak1);
                                        }else if(vaksinName.equalsIgnoreCase("31")){
                                            changeColor(campak2);
                                        }else if(vaksinName.equalsIgnoreCase("32")){
                                            changeColor(campak3);
                                        }else if(vaksinName.equalsIgnoreCase("33")){
                                            changeColor(mmr1);
                                        }else if(vaksinName.equalsIgnoreCase("34") ){
                                            changeColor(mmr2);
                                        }else if(vaksinName.equalsIgnoreCase("35") ){
                                            changeColor(tifoid1);
                                        }else if(vaksinName.equalsIgnoreCase("36") ){
                                            changeColor(hepatitisa1);
                                        }else if(vaksinName.equalsIgnoreCase("37") ){
                                            changeColor(varisela1);
                                        }else if(vaksinName.equalsIgnoreCase("38") ){
                                            changeColor(hpv1);
                                        }

                                    }
                                }
                            }
                        } catch (JsonSyntaxException e) {
                            dialog(getResources().getString(R.string.KesalahanServer), getResources().getString(R.string.Perhatian));
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                NetworkResponse response = error.networkResponse;
                if (error instanceof ServerError && response != null) {
                    try {
                        String res = new String(response.data,
                                HttpHeaderParser.parseCharset(response.headers, "utf-8"));
                        // Now you can use any deserializer to make sense of data
                        JSONObject obj = new JSONObject(res);
                        dialog(res.toString(), getResources().getString(R.string.Perhatian));
                    } catch (UnsupportedEncodingException e1) {
                        // Couldn't properly decode data to string
                        e1.printStackTrace();
                    } catch (JSONException e2) {
                        // returned data is not JSONObject?
                        e2.printStackTrace();
                    }
                }
            }
        }) {

            public Map<String, String> getHeaders() {
                Map<String, String>  params = new HashMap<String, String>();
                params.put("token", preferences.getString(Preferences.ACCESS_TOKEN, ""));

                return params;
            }
        };

        listdata.setRetryPolicy(new DefaultRetryPolicy(
                20000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        mRequest.add(listdata);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

    }

    private void progresDialog(boolean horizontal, String msg) {
        dialog = new MaterialDialog.Builder(this)
                .title(msg)
                .content(getResources().getString(R.string.MohonTunggu))
                .progress(true, 0)
                .progressIndeterminateStyle(horizontal)
                .cancelable(false)
                .show();
    }

    public void dialog(String content, String msg) {
        new MaterialDialog.Builder(this)
                .title(msg)
                .content(content)
                .positiveText(getResources().getString(R.string.MuatUlang))
                .negativeText(getResources().getString(R.string.Kembali))
                .callback(new MaterialDialog.ButtonCallback() {
                    @Override
                    public void onPositive(MaterialDialog dialog) {
                       // getKalender();
                        updateUI();
                    }

                    @Override
                    public void onNegative(MaterialDialog dialog) {
                       // super.onNegative(dialog);
                       // finish();
                        updateUI();
                    }

                })
                .show();
    }

    public void changeColor(RelativeLayout rel) {
        for (int i = 0; i < rel.getChildCount(); i++) {
            TextView v = (TextView) rel.getChildAt(i);
            //v.setTextColor(Color.BLACK);

        }
        rel.setBackgroundResource(R.drawable.table_green_black_border);
    }

    public void resetColor() {
        for (int j = 0; j < list.size(); j++) {
            for (int i = 0; i < list.get(j).getChildCount(); i++) {
                TextView v = (TextView) list.get(j).getChildAt(i);
                //v.setTextColor(Color.WHITE);
            }
        }
        hepa1.setBackgroundResource(R.drawable.table_border_blue_imunisasi);
        hepa2.setBackgroundResource(R.drawable.table_border_blue_imunisasi);
        hepa3.setBackgroundResource(R.drawable.table_border_blue_imunisasi);
        polio1.setBackgroundResource(R.drawable.table_border_blue_imunisasi);
        polio2.setBackgroundResource(R.drawable.table_border_blue_imunisasi);
        polio3.setBackgroundResource(R.drawable.table_border_blue_imunisasi);
        polio4.setBackgroundResource(R.drawable.table_border_blue_imunisasi);
        polio5.setBackgroundResource(R.drawable.table_border_blue_imunisasi);
        polio6.setBackgroundResource(R.drawable.table_border_blue_imunisasi);
        bcg1.setBackgroundResource(R.drawable.table_border_blue_imunisasi);
        dtp1.setBackgroundResource(R.drawable.table_border_blue_imunisasi);
        dtp2.setBackgroundResource(R.drawable.table_border_blue_imunisasi);
        dtp3.setBackgroundResource(R.drawable.table_border_blue_imunisasi);
        dtp4.setBackgroundResource(R.drawable.table_border_blue_imunisasi);
        dtp5.setBackgroundResource(R.drawable.table_border_blue_imunisasi);
        dtp6.setBackgroundResource(R.drawable.table_border_blue_imunisasi);
        dtp7.setBackgroundResource(R.drawable.table_border_blue_imunisasi);
        hib1.setBackgroundResource(R.drawable.table_border_blue_imunisasi);
        hib2.setBackgroundResource(R.drawable.table_border_blue_imunisasi);
        hib3.setBackgroundResource(R.drawable.table_border_blue_imunisasi);
        hib4.setBackgroundResource(R.drawable.table_border_blue_imunisasi);
        pcv1.setBackgroundResource(R.drawable.table_border_blue_imunisasi);
        pcv2.setBackgroundResource(R.drawable.table_border_blue_imunisasi);
        pcv3.setBackgroundResource(R.drawable.table_border_blue_imunisasi);
        pcv4.setBackgroundResource(R.drawable.table_border_blue_imunisasi);
        rotavirus1.setBackgroundResource(R.drawable.table_border_blue_imunisasi);
        rotavirus2.setBackgroundResource(R.drawable.table_border_blue_imunisasi);
        rotavirus3.setBackgroundResource(R.drawable.table_border_blue_imunisasi);
        influenza1.setBackgroundResource(R.drawable.table_border_blue_imunisasi);
        campak1.setBackgroundResource(R.drawable.table_border_blue_imunisasi);
        campak2.setBackgroundResource(R.drawable.table_border_blue_imunisasi);
        campak3.setBackgroundResource(R.drawable.table_border_blue_imunisasi);
        mmr1.setBackgroundResource(R.drawable.table_border_blue_imunisasi);
        mmr2.setBackgroundResource(R.drawable.table_border_blue_imunisasi);
        tifoid1.setBackgroundResource(R.drawable.table_border_blue_imunisasi);
        hepatitisa1.setBackgroundResource(R.drawable.table_border_blue_imunisasi);
        varisela1.setBackgroundResource(R.drawable.table_border_blue_imunisasi);
        hpv1.setBackgroundResource(R.drawable.table_border_blue_imunisasi);
    }



    @Override
    public void onBackPressed() {
        Intent i = new Intent(TabelImunisasiActivity.this, MainActivity.class);
        startActivity(i);
        finish();
    }

    private void getData() {
        progresDialog(false, getResources().getString(R.string.MemuatData));
        StringRequest listdata = new StringRequest(Request.Method.GET,
                ApiReferences.getPasienForAwam(),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        dialog.dismiss();
                        try {
                            if (response.equals("[]")) {
                                dialog(getResources().getString(R.string.DataBelumTersedia), getResources().getString(R.string.Perhatian));
                            } else {
                                Message msg = Parser.getMessage(response);
                                if (msg.getStatus().equals("0")) {
                                    dialog(msg.getMessages(), getResources().getString(R.string.Perhatian));
                                } else {
                                    Log.i("RESULT JSON", response.toString());
                                    final DaftarAnak daftarAnakResult = Parser.getDaftarAnak(response);

                                    listDaftarAnak.removeAllViews();

                                    for (int i = 0; i < daftarAnakResult.getObjects().size(); i++) {
                                        View child = getLayoutInflater().inflate(R.layout.list_item_anak_imunisasi, null);
                                        TextView child_detail_name = (TextView) child.findViewById(R.id.child_detail_name);

                                        final LinearLayout childLinear = (LinearLayout) child.findViewById(item_vaksin_anak);


                                        //childLinear.setOnClickListener(new View.OnClickListener() {

                                        /* final LinearLayout edit_hapus_vaksin = (LinearLayout) child.findViewById(R.id.edit_hapus_vaksin);
                                         edit_hapus_vaksin.setOnClickListener(new View.OnClickListener() { */
                                        /*    @Override
                                            public void onClick(View v) {

                                                //nandha edit vaksin
                                                LayoutInflater li = LayoutInflater.from(getApplicationContext());
                                                View dialogView = li.inflate(R.layout.dialog_edit_hapus_imunisasi, null);

                                                Dialog dialog = new Dialog(context);
                                                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                                                dialog.setContentView(dialogView);

                                                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

                                                dialog.setCancelable(true);
                                                dialog.show();

                                                final Button edit_data = (Button) dialogView.findViewById(R.id.edit_data);
                                                final Button hapus_data = (Button) dialogView.findViewById(R.id.hapus_data);

                                                edit_data.setText("edit nandha");

                                                edit_data.setOnClickListener(new View.OnClickListener() {
                                                    @Override
                                                    public void onClick(View v) {
                                                        if (v == edit_data) {
                                                            Intent i = new Intent(TabelImunisasiActivity.this, EditImunisasiAnakActivity.class);
                                                            startActivity(i);
                                                            // finish();
                                                        }else if (v == hapus_data) {

                                                        }
                                                    }
                                                });
                                                }
                                            });
                                        */
                                        child_detail_name.setId(i);
                                        child_detail_name.setText(daftarAnakResult.getObjects().get(i).getId());
                                        child.setId(i);

                                        /* edit_hapus_vaksin.setId(i); */

                                        daftarNamaAnak.add(daftarAnakResult.getObjects().get(i).getId());
                                        childListAdapter.notifyDataSetChanged();
                                        listDaftarAnak.addView(child);

                                        //add child into vaccine list
                                        getChildIndividualVaccine(daftarAnakResult.getObjects().get(i).getId(), childLinear);


                                    }
                                }
                            }
                        } catch (JsonSyntaxException e) {
                            dialog(getResources().getString(R.string.KesalahanServer), getResources().getString(R.string.Perhatian));
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                NetworkResponse response = error.networkResponse;
                if (error instanceof ServerError && response != null) {
                    try {
                        String res = new String(response.data,
                                HttpHeaderParser.parseCharset(response.headers, "utf-8"));
                        // Now you can use any deserializer to make sense of data
                        JSONObject obj = new JSONObject(res);
                        dialog(res.toString(), getResources().getString(R.string.Perhatian));
                    } catch (UnsupportedEncodingException e1) {
                        // Couldn't properly decode data to string
                        e1.printStackTrace();
                    } catch (JSONException e2) {
                        // returned data is not JSONObject?
                        e2.printStackTrace();
                    }
                }
            }
        }) {

            public Map<String, String> getHeaders() {
                Map<String, String>  params = new HashMap<String, String>();
                params.put("token", preferences.getString(Preferences.ACCESS_TOKEN, ""));

                return params;
            }
        };

        listdata.setRetryPolicy(new DefaultRetryPolicy(
                20000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        mRequest.add(listdata);
    }

    private void addChildVaccine(String childId) {
        scrollTabelImunisasi.setVisibility(View.GONE);
        scrollTabelPostImunisasi.setVisibility(View.GONE);
        scrollTabelConfirmPostImunisasi.setVisibility(View.VISIBLE);
        scrollTabelListImunisasi.setVisibility(View.GONE);



    }

    private void getChildIndividualVaccine(String child_detail_name, final LinearLayout parent) {
        StringRequest listdata = new StringRequest(Request.Method.GET,
                ApiReferences.getChildIndividualVaccine(child_detail_name),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        dialog.dismiss();
                        try {
                            if (response.equals("[]")) {
                                dialog(getResources().getString(R.string.DataBelumTersedia), getResources().getString(R.string.Perhatian));
                            } else {

                                Message msg = Parser.getMessage(response);
                                if (msg.getStatus().equals("0")) {
                                    dialog(msg.getMessages(), getResources().getString(R.string.Perhatian));
                                } else {
                                    Log.i("RESULT JSON", response.toString());
                                    final VaksinPasien daftarVaksinAnak = Parser.getDaftarVaksinAnak(response);

                                    for (int i = 0; i < daftarVaksinAnak.getObjects().size(); i++) {
                                        View child = getLayoutInflater().inflate(R.layout.list_item_anak_vaksin_imunisasi, null);
                                        TextView item_anak_imunisasi_jenis_vaksin = (TextView) child.findViewById(R.id.item_anak_imunisasi_jenis_vaksin);
                                        TextView item_anak_imunisasi_tanggal = (TextView) child.findViewById(R.id.item_anak_imunisasi_tanggal);
                                        TextView item_anak_imunisasi_merek = (TextView) child.findViewById(R.id.item_anak_imunisasi_merek);

                                        String vaksinName = daftarVaksinAnak.getObjects().get(i).getvaccine_step_id();

                                        if(vaksinName.equalsIgnoreCase("1") || vaksinName.equalsIgnoreCase("2") || vaksinName.equalsIgnoreCase("3")){
                                            item_anak_imunisasi_jenis_vaksin.setText("HEPATITIS B");
                                        }else if(vaksinName.equalsIgnoreCase("4") || vaksinName.equalsIgnoreCase("5") || vaksinName.equalsIgnoreCase("6") || vaksinName.equalsIgnoreCase("7") || vaksinName.equalsIgnoreCase("8") || vaksinName.equalsIgnoreCase("9")){
                                            item_anak_imunisasi_jenis_vaksin.setText("POLIO");
                                        }else if(vaksinName.equalsIgnoreCase("10")){
                                            item_anak_imunisasi_jenis_vaksin.setText("BCG");
                                        }else if(vaksinName.equalsIgnoreCase("11") || vaksinName.equalsIgnoreCase("12") || vaksinName.equalsIgnoreCase("13") || vaksinName.equalsIgnoreCase("14") || vaksinName.equalsIgnoreCase("15") || vaksinName.equalsIgnoreCase("16") || vaksinName.equalsIgnoreCase("17")){
                                            item_anak_imunisasi_jenis_vaksin.setText("DTP");
                                        }else if(vaksinName.equalsIgnoreCase("18") || vaksinName.equalsIgnoreCase("19") || vaksinName.equalsIgnoreCase("20") || vaksinName.equalsIgnoreCase("21") ){
                                            item_anak_imunisasi_jenis_vaksin.setText("HIB");
                                        }else if(vaksinName.equalsIgnoreCase("22") || vaksinName.equalsIgnoreCase("23") || vaksinName.equalsIgnoreCase("24") || vaksinName.equalsIgnoreCase("25") ){
                                            item_anak_imunisasi_jenis_vaksin.setText("PCV");
                                        }else if(vaksinName.equalsIgnoreCase("26") || vaksinName.equalsIgnoreCase("27") || vaksinName.equalsIgnoreCase("28") ){
                                            item_anak_imunisasi_jenis_vaksin.setText("ROTAVIRUS");
                                        }else if(vaksinName.equalsIgnoreCase("29") ){
                                            item_anak_imunisasi_jenis_vaksin.setText("INFLUENZA");
                                        }else if(vaksinName.equalsIgnoreCase("30") || vaksinName.equalsIgnoreCase("31") || vaksinName.equalsIgnoreCase("32") ){
                                            item_anak_imunisasi_jenis_vaksin.setText("CAMPAK");
                                        }else if(vaksinName.equalsIgnoreCase("33") || vaksinName.equalsIgnoreCase("34") ){
                                            item_anak_imunisasi_jenis_vaksin.setText("MMR");
                                        }else if(vaksinName.equalsIgnoreCase("35") ){
                                            item_anak_imunisasi_jenis_vaksin.setText("TIFOID");
                                        }else if(vaksinName.equalsIgnoreCase("36") ){
                                            item_anak_imunisasi_jenis_vaksin.setText("HEPATITIS A");
                                        }else if(vaksinName.equalsIgnoreCase("37") ){
                                            item_anak_imunisasi_jenis_vaksin.setText("VARISELA");
                                        }else if(vaksinName.equalsIgnoreCase("38") ){
                                            item_anak_imunisasi_jenis_vaksin.setText("HPV");
                                        }

                                        list.add(hepa1);

                                        item_anak_imunisasi_tanggal.setText(daftarVaksinAnak.getObjects().get(i).getvaccine_date());
                                        item_anak_imunisasi_merek.setText(daftarVaksinAnak.getObjects().get(i).getvaccine_brand());

                                        JSONObject tempVaccine = new JSONObject();
                                        try {
                                            tempVaccine.put("id", daftarVaksinAnak.getObjects().get(i).getId());
                                            tempVaccine.put("vaccine_step_id", daftarVaksinAnak.getObjects().get(i).getvaccine_step_id());
                                            tempVaccine.put("vaccine_brand", daftarVaksinAnak.getObjects().get(i).getvaccine_brand());
                                            tempVaccine.put("vaccine_date", daftarVaksinAnak.getObjects().get(i).getvaccine_date());


                                        } catch (JSONException e) {
                                            // TODO Auto-generated catch block
                                            e.printStackTrace();
                                        }

                                        child.setTag(tempVaccine.toString());

                                        child.setOnClickListener(new View.OnClickListener() {

                                        /* final LinearLayout edit_hapus_vaksin = (LinearLayout) child.findViewById(R.id.edit_hapus_vaksin);
                                         edit_hapus_vaksin.setOnClickListener(new View.OnClickListener() { */
                                        @Override
                                        public void onClick(View v) {

                                            //nandha edit vaksin
                                            LayoutInflater li = LayoutInflater.from(getApplicationContext());
                                            final View dialogView = li.inflate(R.layout.dialog_edit_hapus_imunisasi, null);

                                            Dialog dialog = new Dialog(context);
                                            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                                            dialog.setContentView(dialogView);

                                            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

                                            dialog.setCancelable(true);
                                            dialog.show();

                                            final Button edit_data = (Button) dialogView.findViewById(R.id.edit_data);
                                            final Button hapus_data = (Button) dialogView.findViewById(R.id.hapus_data);

                                            try {
                                                JSONObject tempVaccine = new JSONObject(v.getTag().toString());
                                                //edit_data.setText("edit nandha"+ tempVaccine.get("id"));
                                                edit_data.setTag(tempVaccine.toString());
                                                hapus_data.setTag(tempVaccine.get("id"));
                                            } catch (JSONException e) {
                                                // TODO Auto-generated catch block
                                                e.printStackTrace();
                                            }


                                            edit_data.setOnClickListener(new View.OnClickListener() {
                                                @Override
                                                public void onClick(View v) {
                                                    if (v == edit_data) {
                                                        Intent i = new Intent(TabelImunisasiActivity.this, EditImunisasiAnakActivity.class);
                                                        i.putExtra("VAKSIN_OBJECT", v.getTag().toString());
                                                        startActivity(i);
                                                        // finish();
                                                    }
                                                }
                                            });

                                            hapus_data.setOnClickListener(new View.OnClickListener() {
                                                @Override
                                                public void onClick(View v) {
                                                    if (v == hapus_data) {
                                                        //System.out.println("nandha hapus "+v.getTag());
                                                        StringRequest postlogin = new StringRequest(Request.Method.DELETE,
                                                                ApiReferences.deleteVaccine(v.getTag().toString()), new Response.Listener<String>() {
                                                            @Override
                                                            public void onResponse(String response) {
                                                                try {
                                                                    Log.i("RESULT JSON", response.toString());
                                                                    Message message = Parser.getMessage(response
                                                                            .toString());
                                                                    if (message.getStatus().equalsIgnoreCase("1")) {

                                                                        dialog(getResources().getString(R.string.DataBerhasilDiubah), getResources().getString(R.string.Perhatian));

                                                                    }
                                                                } catch (JsonSyntaxException e) {
                                                                    dialog(getResources().getString(R.string.KesalahanServer), getResources().getString(R.string.Perhatian));
                                                                }
                                                            }
                                                        }, new Response.ErrorListener() {
                                                            @Override
                                                            public void onErrorResponse(VolleyError error) {
                                                                NetworkResponse response = error.networkResponse;
                                                                if (error instanceof ServerError && response != null) {
                                                                    try {
                                                                        String res = new String(response.data,
                                                                                HttpHeaderParser.parseCharset(response.headers, "utf-8"));
                                                                        // Now you can use any deserializer to make sense of data
                                                                        JSONObject obj = new JSONObject(res);
                                                                        dialog(res.toString(), getResources().getString(R.string.Perhatian));
                                                                    } catch (UnsupportedEncodingException e1) {
                                                                        // Couldn't properly decode data to string
                                                                        e1.printStackTrace();
                                                                    } catch (JSONException e2) {
                                                                        // returned data is not JSONObject?
                                                                        e2.printStackTrace();
                                                                    }
                                                                }
                                                            }
                                                        }) {

                                                            public Map<String, String> getHeaders() {
                                                                Map<String, String>  params = new HashMap<String, String>();
                                                                params.put("token", preferences.getString(Preferences.ACCESS_TOKEN, ""));

                                                                return params;
                                                            }


                                                        };
                                                        postlogin.setRetryPolicy(new DefaultRetryPolicy(
                                                                20000,
                                                                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                                                                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                                                        mRequest.add(postlogin);

                                                    }
                                                }
                                            });



                                            }
                                        });


                                        parent.addView(child);

                                    }
                                }
                            }
                        } catch (JsonSyntaxException e) {
                            dialog(getResources().getString(R.string.KesalahanServer), getResources().getString(R.string.Perhatian));
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                NetworkResponse response = error.networkResponse;
                if (error instanceof ServerError && response != null) {
                    try {
                        String res = new String(response.data,
                                HttpHeaderParser.parseCharset(response.headers, "utf-8"));
                        // Now you can use any deserializer to make sense of data
                        JSONObject obj = new JSONObject(res);
                        dialog(res.toString(), getResources().getString(R.string.Perhatian));
                    } catch (UnsupportedEncodingException e1) {
                        // Couldn't properly decode data to string
                        e1.printStackTrace();
                    } catch (JSONException e2) {
                        // returned data is not JSONObject?
                        e2.printStackTrace();
                    }
                }
            }
        }) {

            public Map<String, String> getHeaders() {
                Map<String, String>  params = new HashMap<String, String>();
                params.put("token", preferences.getString(Preferences.ACCESS_TOKEN, ""));

                return params;
            }
        };

        listdata.setRetryPolicy(new DefaultRetryPolicy(
                20000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        mRequest.add(listdata);
    }


}
