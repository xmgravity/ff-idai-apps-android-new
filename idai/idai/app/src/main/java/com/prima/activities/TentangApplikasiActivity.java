package com.prima.activities;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.prima.MainActivity;

import prima.test.prima.BuildConfig;
import prima.test.prima.R;

/**
 * Created by Codelabs on 21/09/2015.
 */
public class TentangApplikasiActivity extends BaseActivity implements View.OnClickListener{
    private ImageView back;
    private ImageView close;
    private TextView title, title1, subtitle1,title2, subtitle2;
    //private Button buttonUpdate;

    @Override
    public void initView() {

        close = (ImageView) findViewById(R.id.close);
        close.setVisibility(View.GONE);
        back = (ImageView) findViewById(R.id.back);
        title1 = (TextView) findViewById(R.id.title1);
        title2 = (TextView) findViewById(R.id.title2);
        subtitle1 = (TextView) findViewById(R.id.subtitle1);
        subtitle2 = (TextView) findViewById(R.id.subtitle2);
        //buttonUpdate = (Button) findViewById(R.id.update);

        title = (TextView) findViewById(R.id.tv_actionbar_title);
        Typeface tf = Typeface.createFromAsset(getApplicationContext().getAssets(), "BREVIA-BLACK.OTF");
        Typeface tf1 = Typeface.createFromAsset(getApplicationContext().getAssets(), "BREVIA-SEMIBOLD.OTF");

        title.setTypeface(tf);
        title2.setTypeface(tf1);
        subtitle1.setTypeface(tf1);
        subtitle2.setTypeface(tf1);
        //buttonUpdate.setTypeface(tf1);

        //title.setText("Tentang Aplikasi");
        title.setText(getResources().getString(R.string.titleTentangApplikasi));


        try {
            String versionName = context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionName;
            subtitle2.setText("Versi "+versionName + " copyright (c)\nIkatan Dokter Anak Indonesia, 2015. All \nright reserved");


        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }


    }


    @Override
    public void setUICallbacks() {
        back.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if (v == back) {
            Intent i = new Intent(this, MainActivity.class);
            startActivity(i);
            finish();
        }
    }


    @Override
    public int getLayout() {
        return R.layout.activity_tentang_applikasi;
    }

    @Override
    public void updateUI() {

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

    }


    @Override
    public void onBackPressed() {
        Intent i = new Intent(this, MainActivity.class);
        startActivity(i);
        finish();
    }




}
