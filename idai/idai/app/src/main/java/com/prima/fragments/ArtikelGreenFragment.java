package com.prima.fragments;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Typeface;
import android.support.v4.view.ViewPager;
import android.text.Html;
import android.text.Spannable;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.prima.entities.Artikel;
import prima.test.prima.R;

/**
 * Created by Codelabs on 06/10/2015.
 */
public class ArtikelGreenFragment extends BaseFragment implements View.OnClickListener {

    private Context mContext;
    public static final String ARG_PAGE = "page 2";
    private ViewPager mPager;
    private int i;
    private Artikel artikel;
    private TextView content;


    private Button source;
    private TextView txtSource;
    RelativeLayout source_wraper;

    @SuppressLint("ValidFragment")
    public ArtikelGreenFragment() {}
    @SuppressLint("ValidFragment")
    public ArtikelGreenFragment(int i, Artikel artikel) {
        this.i = i;
        this.artikel = artikel;
    }


    @Override
    public void initView(View view) {
        content = (TextView) view.findViewById(R.id.content);
        source_wraper = (RelativeLayout) view.findViewById(R.id.source_wraper);
        source = (Button) view.findViewById(R.id.btn_source);
        txtSource = (TextView) view.findViewById(R.id.source);
        txtSource.setVisibility(View.GONE);

        if (i < artikel.getObjects().size() - 1) {
            source.setVisibility(View.GONE);
            source_wraper.setVisibility(View.GONE);
        }

        Typeface tf2 = Typeface.createFromAsset(getActivity().getAssets(), "BREVIA-SEMIBOLD.OTF");
        content.setTypeface(tf2);

        content.setText(artikel.getObjects().get(i).getDeskripsi());
        Spannable s = (Spannable) Html.fromHtml(artikel.getObjects().get(i).getSumber());
        txtSource.setText(s);

        if (artikel.getObjects().get(i).getSumber().trim().equals("")) {
            txtSource.setVisibility(View.GONE);
            source_wraper.setVisibility(View.GONE);
            source.setVisibility(View.GONE);

        }
    }

    @Override
    public void setUICallbacks() {
        source.setOnClickListener(this);
    }

    @Override
    public void updateUI() {

    }

    @Override
    public String getPageTitle() {
        return "Account Summary";
    }


    @Override
    public int getFragmentLayout() {
        return R.layout.fragment_artikel_green;
    }

    @Override
    public void onClick(View v) {
        if (v == source) {
            if (txtSource.getVisibility() == View.GONE) {
                txtSource.setVisibility(View.VISIBLE);
            } else {
                txtSource.setVisibility(View.GONE);
            }
        }
    }

}
