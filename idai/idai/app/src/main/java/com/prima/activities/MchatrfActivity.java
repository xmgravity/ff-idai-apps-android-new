package com.prima.activities;

import android.content.Intent;
import android.graphics.Typeface;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.JsonSyntaxException;

import com.prima.MainActivity;
import com.prima.Preferences;
import com.prima.controllers.ApiReferences;
import com.prima.controllers.Parser;
import com.prima.entities.Message;
import com.prima.entities.Pdf;
import com.prima.entities.Question;
import com.prima.helpers.DatabaseHandler;
import prima.test.prima.R;

/**
 * Created by Codelabs on 18/08/2015.
 */
public class MchatrfActivity extends BaseActivity implements View.OnClickListener {
    private LinearLayout box;
    private ImageView close;
    private Button ok;
    private Button source;
    private TextView title;
    MaterialDialog dialog;
    Question question;
    private RequestQueue mRequest;
    private TextView txtSource, content;
    RelativeLayout source_wraper;
    DatabaseHandler db;

    @Override
    public void initView() {
        db = new DatabaseHandler(context);
        content = (TextView) findViewById(R.id.content);
        source_wraper = (RelativeLayout) findViewById(R.id.source_wraper);
        close = (ImageView) findViewById(R.id.close);
        source = (Button) findViewById(R.id.btn_source);
        txtSource = (TextView) findViewById(R.id.source);
        txtSource.setVisibility(View.GONE);
        title = (TextView) findViewById(R.id.title);
        mRequest = Volley.newRequestQueue(getApplicationContext());
        box = (LinearLayout) findViewById(R.id.box);
        close = (ImageView) findViewById(R.id.close);
        ok = (Button) findViewById(R.id.lanjut);

        title = (TextView) findViewById(R.id.title);
        //set font
        Typeface tf = Typeface.createFromAsset(getApplicationContext().getAssets(), "Brevia-ExtraBlackItalic.otf");
        title.setTypeface(tf);


    }

    @Override
    public void setUICallbacks() {
        ok.setOnClickListener(this);
        close.setOnClickListener(this);
        source.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        if (v == ok) {
//            if (preferences.getString(Preferences.ID, "").equalsIgnoreCase("") || preferences.getString(Preferences.BIRTH_DAY, "").equalsIgnoreCase("")) {
//                dialog2("tidak ada pasien yang terdaftar, daftarkan terlebih dahulu pada menu Sesi Pasien", "Tidak dapat dilanjutkan");
//            } else {
            getPdf("0", "M-CHAT-F");
        }
//        }
        if (v == close) {
            Intent i = new Intent(getApplicationContext(), MainActivity.class);
            startActivity(i);
            finish();
        } else if (v == source) {
            if (txtSource.getVisibility() == View.GONE) {
                txtSource.setVisibility(View.VISIBLE);
            } else {
                txtSource.setVisibility(View.GONE);
            }
        }
    }


    private void getPdf(final String ID, final String code) {
        progresDialog(false, getResources().getString(R.string.MemuatData));
        StringRequest listdata = new StringRequest(Request.Method.GET,
                ApiReferences.getPdf(code, ID, preferences.getString(Preferences.LANGUAGE, "")),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        dialog.dismiss();
                        try {
                            if (response.equals("[]")) {
                                dialog(getResources().getString(R.string.BelumAdaData), getResources().getString(R.string.Pesan));
                            } else {
                                Message msg = Parser.getMessage(response);
                                if (msg.getStatus().equals("1")) {
                                    Log.i("RESULT JSON", response.toString());
                                    Pdf pdf = Parser.getPdf(response);
                                    Intent i = new Intent(getApplicationContext(), MchatrfPdfActivity.class);
                                    i.putExtra("url", pdf.getObjects().getFilename());
                                    i.putExtra("id", pdf.getObjects().getId());
                                    i.putExtra("menu", pdf.getObjects().getMenu());
                                    i.putExtra("category", pdf.getObjects().getCategory());
                                    startActivity(i);
                                } else {
                                    dialog(msg.getMessages(), getResources().getString(R.string.Pesan));
                                }
                            }
                        } catch (JsonSyntaxException e) {
                            dialog(getResources().getString(R.string.KesalahanServer), getResources().getString(R.string.Perhatian));
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dialog.dismiss();
                Pdf pdf = db.getFilesObject(Integer.valueOf(ID));
                if (!pdf.getObjects().equals("") && pdf.getObjects() != null) {
                    Intent i = new Intent(getApplicationContext(), MchatrfPdfActivity.class);
                    i.putExtra("url", pdf.getObjects().getFilename());
                    i.putExtra("id", pdf.getObjects().getId());
                    i.putExtra("menu", pdf.getObjects().getMenu());
                    i.putExtra("category", pdf.getObjects().getCategory());
                    startActivity(i);
                } else {
                    dialog(getResources().getString(R.string.NoConnection), getResources().getString(R.string.Perhatian));
                }
            }
        });
        listdata.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        mRequest.add(listdata);
    }


    @Override
    public int getLayout() {
        return R.layout.activity_mchat_rf;
    }

    @Override
    public void updateUI() {
//        Bundle bn = getIntent().getExtras();
//        if (bn != null) {
//            String result = bn.getString("result");
//            Artikel artikel = Parser.getArtikel(result);
//            content.setText(artikel.getObjects().getDeskripsi());
//            Spannable s = (Spannable) Html.fromHtml("<a href=\"" + artikel.getObjects().getSumber() + "\">" + artikel.getObjects().getSumber() + "</a>");
//            URLSpan[] spans = s.getSpans(0, s.length(), URLSpan.class);
//            for (URLSpan span : spans) {
//                int start = s.getSpanStart(span);
//                int end = s.getSpanEnd(span);
//                s.removeSpan(span);
//                span = new URLSpan(span.getURL());
//                s.setSpan(span, start, end, 0);
//            }
//            txtSource.setText(s);
//            txtSource.setMovementMethod(LinkMovementMethod.getInstance());
//
//            if (artikel.getObjects().getSumber().trim().equals("")) {
//                txtSource.setVisibility(View.GONE);
//                source_wraper.setVisibility(View.GONE);
//                source.setVisibility(View.GONE);
//
//            }
//
//        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

    }

    private void progresDialog(boolean horizontal, String msg) {
        dialog = new MaterialDialog.Builder(this)
                .title(msg)
                .content(getResources().getString(R.string.MohonTunggu))
                .progress(true, 0)
                .progressIndeterminateStyle(horizontal)
                .cancelable(false)
                .show();
    }

    public void dialog(String content, String msg) {
        new MaterialDialog.Builder(this)
                .title(msg)
                .content(content)
                .positiveText(getResources().getString(R.string.MuatUlang))
                .negativeText(getResources().getString(R.string.Kembali))
                .callback(new MaterialDialog.ButtonCallback() {
                    @Override
                    public void onPositive(MaterialDialog dialog) {

                    }

                    @Override
                    public void onNegative(MaterialDialog dialog) {
                        super.onNegative(dialog);
                    }

                })
                .show();
    }


    public void dialog2(String content, String msg) {
        new MaterialDialog.Builder(this)
                .title(msg)
                .content(content)
                .positiveText("OK")
                .callback(new MaterialDialog.ButtonCallback() {
                    @Override
                    public void onPositive(MaterialDialog dialog) {

                    }

                })
                .show();
    }


    @Override
    public void onBackPressed() {
        // Intent i = new Intent(getApplicationContext(), MainActivity.class);
        //  startActivity(i);
        finish();
    }
}