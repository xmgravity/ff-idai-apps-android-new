package com.prima.activities;

import android.content.Intent;
import android.graphics.Typeface;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.prima.Preferences;
import com.prima.controllers.ApiReferences;
import com.prima.controllers.Parser;
import com.prima.entities.Agenda;
import com.prima.entities.Message;
import com.prima.helpers.DatabaseHandler;

import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.List;

import prima.test.prima.R;

/**
 * Created by Codelabs on 15/09/2015.
 */
public class ListAgendaActivity extends BaseActivity implements View.OnClickListener {

    TextView title;
    ImageView close, back;
    LinearLayout list;
    private MaterialDialog dialog;
    private RequestQueue mRequest;
    ArrayList<LinearLayout> days = new ArrayList<LinearLayout>();
    LinearLayout listAgenda;
    DatabaseHandler db;

    @Override
    public void initView() {
        db = new DatabaseHandler(context);
        mRequest = Volley.newRequestQueue(getApplicationContext());
        list = (LinearLayout) findViewById(R.id.list_jadwal_praktik);
        close = (ImageView) findViewById(R.id.close);
        back = (ImageView) findViewById(R.id.back);

        title = (TextView) findViewById(R.id.tv_actionbar_title);
        Typeface tf = Typeface.createFromAsset(getApplicationContext().getAssets(), "BREVIA-BLACK.OTF");
        title.setTypeface(tf);
        title.setText(getResources().getString(R.string.Kunjungan));
        //    getData();

        //  save(data);
    }

    @Override
    public void setUICallbacks() {
        close.setOnClickListener(this);
        back.setOnClickListener(this);
    }


    private void getData() {
        progresDialog(false, getResources().getString(R.string.MemuatData));
        StringRequest listdata = new StringRequest(Request.Method.GET,
                ApiReferences.getAllAgendaByDate(preferences.getString(Preferences.ID_USER, "")),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        dialog.dismiss();
                        try {
                            if (response.equals("[]")) {
                                dialog(getResources().getString(R.string.DataBelumTersedia), getResources().getString(R.string.Perhatian));
                            } else {
                                Message msg = Parser.getMessage(response);
                                if (msg.getStatus().equals("0")) {
                                    dialog(msg.getMessages(), getResources().getString(R.string.Perhatian));
                                } else {
                                    Log.i("RESULT JSON", response.toString());
                                    final Agenda agnda = Parser.getAgenda(response);

                                    List<String> dates = new ArrayList<String>();
                                    for (int j = 0; j < agnda.getObjects().size(); j++) {
                                        dates.add(agnda.getObjects().get(j).getTanggal_kunjungan());
                                    }

                                    for (int i = 0; i < dates.size(); i++) {
                                        View child = getLayoutInflater().inflate(R.layout.list_item_agenda, null);
                                        TextView date = (TextView) child.findViewById(R.id.date);
                                        TextView bulan = (TextView) child.findViewById(R.id.bulan);
                                        TextView tanggal = (TextView) child.findViewById(R.id.tgl);
                                        listAgenda = (LinearLayout) child.findViewById(R.id.list_agenda);
                                        date.setId(i);
                                        bulan.setId(i);
                                        tanggal.setId(i);
                                        listAgenda.setId(i);
                                        days.add(listAgenda);
                                        bulan.setText(getBulan(dates.get(i)));
                                        tanggal.setText(getHari(dates.get(i)));
                                        date.setText(getHariStrng(dates.get(i)) + ", " + getHari(dates.get(i)) + " " + getBulanStringLong(dates.get(i)));
                                        child.setId(i);
                                        list.addView(child);
                                    }


                                    for (int i = 0; i < agnda.getObjects().size(); i++) {
                                        for (int j = 0; j < agnda.getObjects().get(i).getJadwal().size(); j++) {
                                            View child = getLayoutInflater().inflate(R.layout.list_item_agenda_days, null);
                                            TextView title = (TextView) child.findViewById(R.id.title);
                                            TextView waktu = (TextView) child.findViewById(R.id.waktu);
                                            title.setId(Integer.valueOf(agnda.getObjects().get(i).getJadwal().get(j).getId()));
                                            waktu.setId(Integer.valueOf(agnda.getObjects().get(i).getJadwal().get(j).getId()));
                                            child.setId(Integer.valueOf(agnda.getObjects().get(i).getJadwal().get(j).getId()));
                                            title.setText(agnda.getObjects().get(i).getJadwal().get(j).getNama_pasien());
                                            waktu.setText(agnda.getObjects().get(i).getJadwal().get(j).getJam_mulai().substring(0, 5) + " - " + agnda.getObjects().get(i).getJadwal().get(j).getJam_akhir().substring(0, 5));

                                            final int finalI = i;
                                            final int finalJ = j;
                                            title.setOnClickListener(new View.OnClickListener() {
                                                @Override
                                                public void onClick(View v) {
                                                    getDataEdit(agnda.getObjects().get(finalI).getJadwal().get(finalJ).getId());
                                                }
                                            });
                                            days.get(i).addView(child);
                                        }
                                    }

                                }
                            }

                        } catch (JsonSyntaxException e) {
                            dialog(getResources().getString(R.string.KesalahanServer), getResources().getString(R.string.Perhatian));
                        }
                    }
                }

                , new Response.ErrorListener()

        {
            @Override
            public void onErrorResponse(VolleyError error) {
                dialog.dismiss();
                final Agenda agnda = db.getListAgenda();
                try {
                    List<String> dates = new ArrayList<String>();
                    for (int j = 0; j < agnda.getObjects().size(); j++) {
                        dates.add(agnda.getObjects().get(j).getTanggal_kunjungan());
                    }

                    for (int i = 0; i < dates.size(); i++) {
                        View child = getLayoutInflater().inflate(R.layout.list_item_agenda, null);
                        TextView date = (TextView) child.findViewById(R.id.date);
                        TextView bulan = (TextView) child.findViewById(R.id.bulan);
                        TextView tanggal = (TextView) child.findViewById(R.id.tgl);
                        listAgenda = (LinearLayout) child.findViewById(R.id.list_agenda);
                        date.setId(i);
                        bulan.setId(i);
                        tanggal.setId(i);
                        listAgenda.setId(i);
                        days.add(listAgenda);
                        bulan.setText(getBulan(dates.get(i)));
                        tanggal.setText(getHari(dates.get(i)));
                        date.setText(getHariStrng(dates.get(i)) + ", " + getHari(dates.get(i)) + " " + getBulanStringLong(dates.get(i)));
                        child.setId(i);
                        list.addView(child);
                    }


                    for (int i = 0; i < agnda.getObjects().size(); i++) {
                        for (int j = 0; j < agnda.getObjects().get(i).getJadwal().size(); j++) {
                            View child = getLayoutInflater().inflate(R.layout.list_item_agenda_days, null);
                            TextView title = (TextView) child.findViewById(R.id.title);
                            TextView waktu = (TextView) child.findViewById(R.id.waktu);
                            title.setId(Integer.valueOf(agnda.getObjects().get(i).getJadwal().get(j).getId()));
                            waktu.setId(Integer.valueOf(agnda.getObjects().get(i).getJadwal().get(j).getId()));
                            child.setId(Integer.valueOf(agnda.getObjects().get(i).getJadwal().get(j).getId()));
                            title.setText(agnda.getObjects().get(i).getJadwal().get(j).getNama_pasien());
                            waktu.setText(agnda.getObjects().get(i).getJadwal().get(j).getJam_mulai().substring(0, 5) + " - " + agnda.getObjects().get(i).getJadwal().get(j).getJam_akhir().substring(0, 5));

                            final int finalI = i;
                            final int finalJ = j;
                            title.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    getDataEdit(agnda.getObjects().get(finalI).getJadwal().get(finalJ).getId());
                                }
                            });
                            days.get(i).addView(child);
                        }
                    }
                } catch (NullPointerException e) {
                    dialog(getResources().getString(R.string.NoConnection), getResources().getString(R.string.Perhatian));
                }


            }
        }

        );
        listdata.setRetryPolicy(new

                        DefaultRetryPolicy(
                        20000,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)

        );
        mRequest.add(listdata);

    }


    private void getDataEdit(final String id) {
        progresDialog(false, getResources().getString(R.string.MemuatData));
        StringRequest listdata = new StringRequest(Request.Method.GET,
                ApiReferences.getAgenda(id),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        dialog.dismiss();
                        try {
                            if (response.equals("[]")) {
                                dialog(getResources().getString(R.string.DataBelumTersedia), getResources().getString(R.string.Perhatian));
                            } else {
                                Message msg = Parser.getMessage(response);
                                if (msg.getStatus().equals("0")) {
                                    dialog(msg.getMessages(), getResources().getString(R.string.Perhatian));
                                } else {
                                    Log.i("RESULT JSON", response.toString());
                                    Intent i = new Intent(getApplicationContext(), AgendaEditActivity.class);
                                    i.putExtra("respon", response);
                                    startActivity(i);
                                }
                            }
                        } catch (JsonSyntaxException e) {
                            dialog(getResources().getString(R.string.KesalahanServer), getResources().getString(R.string.Perhatian));
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dialog.dismiss();
                Agenda.Agendas temp = new Agenda().new Agendas();
                temp.setObjects(db.getAgenda(id));
                Gson gson = new Gson();
                try {
                    String id = temp.getId();
                    Intent i = new Intent(getApplicationContext(), AgendaEditActivity.class);
                    i.putExtra("respon", gson.toJson(temp));
                    startActivity(i);
                } catch (NullPointerException e) {
                    dialog(getResources().getString(R.string.NoConnection), getResources().getString(R.string.Perhatian));
                }


            }
        });
        listdata.setRetryPolicy(new DefaultRetryPolicy(
                20000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        mRequest.add(listdata);

    }


    public void dialog(String content, String msg) {
        new MaterialDialog.Builder(this)
                .title(msg)
                .content(content)
                .positiveText("Ok")
                .callback(new MaterialDialog.ButtonCallback() {
                    @Override
                    public void onPositive(MaterialDialog dialog) {
                    }

                })
                .show();
    }

    public void dialog2(String content, String msg) {
        new MaterialDialog.Builder(this)
                .title(msg)
                .content(content)
                .negativeText("OK")
                .callback(new MaterialDialog.ButtonCallback() {
                    @Override
                    public void onPositive(MaterialDialog dialog) {

                    }

                    @Override
                    public void onNegative(MaterialDialog dialog) {
                        super.onNegative(dialog);
                    }

                })
                .show();
    }

    private void progresDialog(boolean horizontal, String msg) {
        dialog = new MaterialDialog.Builder(this)
                .title(msg)
                .content(getResources().getString(R.string.MohonTunggu))
                .progress(true, 0)
                .progressIndeterminateStyle(horizontal)
                .cancelable(false)
                .show();
    }


    @Override
    public void onClick(View v) {
        if (v == back) {
            Intent i = new Intent(getApplicationContext(), AgendaActivity.class);
            startActivity(i);
            finish();
        } else if (v == close) {
            Intent i = new Intent(getApplicationContext(), AgendaAddActivity.class);
            startActivity(i);
            //    finish();
        }
    }


    @Override
    public int getLayout() {
        return R.layout.activity_jadwal_praktik_list;
    }

    @Override
    public void updateUI() {
        list.removeAllViewsInLayout();
        days.clear();
        getData();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

    }


    String getBulan(String tanggal) {
        String bulan = tanggal.substring(5, 7);
        if (bulan.equals("01")) {
            bulan = "JAN";
        }
        if (bulan.equals("02")) {
            bulan = "FEB";
        }
        if (bulan.equals("03")) {
            bulan = "MAR";
        }
        if (bulan.equals("04")) {
            bulan = "APR";
        }
        if (bulan.equals("05")) {
            bulan = "MEI";
        }
        if (bulan.equals("06")) {
            bulan = "JUN";
        }
        if (bulan.equals("07")) {
            bulan = "JUL";
        }
        if (bulan.equals("08")) {
            bulan = "AGS";
        }
        if (bulan.equals("09")) {
            bulan = "SEP";
        }
        if (bulan.equals("10")) {
            bulan = "OKT";
        }
        if (bulan.equals("11")) {
            bulan = "NOV";
        }
        if (bulan.equals("12")) {
            bulan = "DES";
        }


        return bulan;
    }

    public String getHari(String tanggal) {
        String hari = tanggal.substring(8, 10);
        return hari;
    }


    @Override
    public void onBackPressed() {
        Intent i = new Intent(getApplicationContext(), AgendaActivity.class);
        startActivity(i);
        finish();
    }

    public String getHariStrng(String date) {
        DateTime dt = new DateTime(date);
        int iDoW = dt.getDayOfWeek();

        if (iDoW == 1) {
            return "Senin";
        } else if (iDoW == 2) {
            return "Selasa";
        } else if (iDoW == 3) {
            return "Rabu";
        } else if (iDoW == 4) {
            return "Kamis";
        } else if (iDoW == 5) {
            return "Jumat";
        } else if (iDoW == 6) {
            return "Sabtu";
        } else {
            return "Minggu";
        }

    }


    public String getBulanStringLong(String date) {
        String bulan = date.substring(5, 7);
        if (bulan.equals("01")) {
            bulan = "Januari";
        }
        if (bulan.equals("02")) {
            bulan = "Februari";
        }
        if (bulan.equals("03")) {
            bulan = "Maret";
        }
        if (bulan.equals("04")) {
            bulan = "April";
        }
        if (bulan.equals("05")) {
            bulan = "Mei";
        }
        if (bulan.equals("06")) {
            bulan = "Juni";
        }
        if (bulan.equals("07")) {
            bulan = "Juli";
        }
        if (bulan.equals("08")) {
            bulan = "Agustus";
        }
        if (bulan.equals("09")) {
            bulan = "September";
        }
        if (bulan.equals("10")) {
            bulan = "Oktober";
        }
        if (bulan.equals("11")) {
            bulan = "November";
        }
        if (bulan.equals("12")) {
            bulan = "Desember";
        }


        return bulan;
    }


}
