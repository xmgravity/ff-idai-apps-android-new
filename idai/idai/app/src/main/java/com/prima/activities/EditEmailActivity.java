package com.prima.activities;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.mobsandgeeks.saripaar.Validator;

import java.util.regex.Pattern;

import com.prima.Preferences;
import com.prima.helpers.HexagonMaskView;
import prima.test.prima.R;

/**
 * Created by Codelabs on 26/08/2015.
 */
public class EditEmailActivity extends BaseActivity implements View.OnClickListener {

    TextView title, username;
    ImageView close, back;
    Button simpan;
    ImageView error1, error2, icon;
    Validator validator;
    EditText email_lama;
    EditText email_baru;
    boolean iscorrect = false;

    private String EMAIL_REGEX = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
    HexagonMaskView hexa;

    @Override
    public void initView() {
        error1 = (ImageView) findViewById(R.id.error1);
        icon = (ImageView) findViewById(R.id.icon);
        error2 = (ImageView) findViewById(R.id.error2);
        email_lama = (EditText) findViewById(R.id.email_lama);
        email_baru = (EditText) findViewById(R.id.email_baru);
        simpan = (Button) findViewById(R.id.simpan);
        close = (ImageView) findViewById(R.id.close);
        back = (ImageView) findViewById(R.id.back);
        close.setVisibility(View.GONE);
        //set font
        title = (TextView) findViewById(R.id.tv_actionbar_title);
        Typeface tf = Typeface.createFromAsset(getApplicationContext().getAssets(), "BREVIA-BLACK.OTF");
        title.setTypeface(tf);
        title.setText("EDIT EMAIL");

        username = (TextView) findViewById(R.id.username);
        username.setText(preferences.getString(Preferences.NAME, ""));
        hexa = new HexagonMaskView();
        Glide.with(EditEmailActivity.this)
                .load(preferences.getString(Preferences.FOTO, ""))
                .asBitmap()
                .override(200,200)
                .into(new SimpleTarget<Bitmap>() {
                    @Override
                    public void onResourceReady(Bitmap bitmap, GlideAnimation anim) {
                        Bitmap b = hexa.getHexagonShape(hexa.scaleCenterCrop(bitmap, 200, 200));
                        icon.setImageBitmap(b);
                    }
                });
        TextWatcher validationTextWatcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (Pattern.matches(EMAIL_REGEX, email_lama.getText().toString().trim())) {
                    error1.setBackgroundResource(R.mipmap.ic_checklist);
                    iscorrect = true;
                } else {
                    error1.setBackgroundResource(R.mipmap.ic_redcross);
                    iscorrect = false;
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        };


        TextWatcher validationTextWatcher2 = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (Pattern.matches(EMAIL_REGEX, email_baru.getText().toString().trim())) {
                    error2.setBackgroundResource(R.mipmap.ic_checklist);
                    iscorrect = true;
                } else {
                    error2.setBackgroundResource(R.mipmap.ic_redcross);
                    iscorrect = false;
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        };
        email_baru.addTextChangedListener(validationTextWatcher2);
        email_lama.addTextChangedListener(validationTextWatcher);
    }

    @Override
    public void setUICallbacks() {
        back.setOnClickListener(this);
        simpan.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        if (v == back) {
            Intent i = new Intent(getApplicationContext(), SettingActivity.class);
            startActivity(i);
            finish();

        } else if (v == simpan) {

            if ((!email_lama.getText().toString().trim().equals("") && !email_baru.getText().toString().trim().equals(""))) {
                if (iscorrect) {
                    Intent i = new Intent(getApplicationContext(), EditEmailResultActivity.class);
                    startActivity(i);
                    finish();
                } else {
                    Toast.makeText(getApplicationContext(), "Email tidak sesuai format.", Toast.LENGTH_LONG).show();
                }
            } else {
                Toast.makeText(getApplicationContext(), "Harap Isi email lama dan email baru anda untuk melanjutkan.", Toast.LENGTH_LONG).show();
            }
        }
    }


    @Override
    public int getLayout() {
        return R.layout.activity_edit_email;
    }

    @Override
    public void updateUI() {

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

    }


    @Override
    public void onBackPressed() {
        Intent i = new Intent(getApplicationContext(), SettingActivity.class);
        startActivity(i);
        finish();
    }

}
