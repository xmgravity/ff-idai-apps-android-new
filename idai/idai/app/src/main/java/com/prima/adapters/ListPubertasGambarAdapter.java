package com.prima.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.GlideDrawableImageViewTarget;

import java.util.ArrayList;

import com.prima.controllers.ApiReferences;
import com.prima.entities.Pubertas;
import prima.test.prima.R;

/**
 * Created by Codelabs on 07/08/2015.
 */
public class ListPubertasGambarAdapter extends BaseAdapter {
    private ArrayList<Pubertas.Item> _data;
    Context _context;


    public ListPubertasGambarAdapter(ArrayList<Pubertas.Item> data,
                                     Context context) {
        _data = data;
        _context = context;
    }

    @Override
    public int getCount() {

        return _data.size();
    }

    @Override
    public Object getItem(int position) {

        return _data.get(position);
    }

    @Override
    public long getItemId(int position) {

        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        if (v == null) {
            LayoutInflater infLayout = (LayoutInflater) _context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = infLayout.inflate(R.layout.list_item_pubertas_image, null);

        }
        ImageView img = (ImageView) v.findViewById(R.id.img);
        TextView phase = (TextView) v.findViewById(R.id.phase);
        final ProgressBar progress = (ProgressBar) v.findViewById(R.id.progress);
        phase.setText(_data.get(position).getPhase());




//        int windowContainerWidth = _context.getResources().getDisplayMetrics().widthPixels;
//        img.getLayoutParams().width = windowContainerWidth-50;
        Glide.with(_context)
                .load(ApiReferences.getImageUrl()+_data.get(position).getImage())
                .fitCenter()
                .override(200,200)
                .into(new GlideDrawableImageViewTarget(img) {
                    @Override
                    public void onResourceReady(GlideDrawable drawable, GlideAnimation anim) {
                        super.onResourceReady(drawable, anim);
                        progress.setVisibility(View.GONE);
                    }
                });
        return v;
    }
}
