package com.prima.activities;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.provider.CalendarContract;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.JsonSyntaxException;
import com.prima.MainActivity;
import com.prima.Preferences;
import com.prima.controllers.ApiReferences;
import com.prima.controllers.Parser;
import com.prima.entities.Agenda;
import com.prima.entities.Message;
import com.prima.helpers.DatabaseHandler;
import com.roomorama.caldroid.CaldroidFragment;
import com.roomorama.caldroid.CaldroidListener;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import prima.test.prima.R;

/**
 * Created by Codelabs on 17/08/2015.
 */
public class AgendaActivity extends BaseActivity implements View.OnClickListener {
    private ImageView close;
    private ImageView back;
    CaldroidFragment caldroidFragment;
    Calendar cal;
    TextView title, jadwal_praktik_text;
    LinearLayout kunjungan, jadwal_praktik, catatan;
    MaterialDialog dialog;
    long eventID;
    private RequestQueue mRequest;
    RelativeLayout jadwal_praktik_layout, catatan_layout;
    DatabaseHandler db;

    @Override
    public void initView() {
        db = new DatabaseHandler(context);
        mRequest = Volley.newRequestQueue(getApplicationContext());
        jadwal_praktik = (LinearLayout) findViewById(R.id.jadwal_praktik);
        jadwal_praktik_layout = (RelativeLayout) findViewById(R.id.jadwal_praktik_layout);
        kunjungan = (LinearLayout) findViewById(R.id.kunjungan);
        close = (ImageView) findViewById(R.id.close);
        back = (ImageView) findViewById(R.id.back);
        close.setVisibility(View.GONE);
        title = (TextView) findViewById(R.id.tv_actionbar_title);
        jadwal_praktik_text = (TextView) findViewById(R.id.jadwal_praktik_text);
        catatan = (LinearLayout) findViewById(R.id.catatan);
        catatan_layout = (RelativeLayout) findViewById(R.id.catatan_layout);

        Typeface tf = Typeface.createFromAsset(getApplicationContext().getAssets(), "BREVIA-BLACK.OTF");
        title.setTypeface(tf);
        title.setText("AGENDA");


        if(preferences.getString(Preferences.TIPE_USER, "").equalsIgnoreCase("awam")){
            jadwal_praktik_text.setText(getResources().getString(R.string.JadwalPraktik));
            jadwal_praktik_layout.setVisibility(View.GONE);

        }


    }

    @Override
    public void setUICallbacks() {
        jadwal_praktik.setOnClickListener(this);
        kunjungan.setOnClickListener(this);
        close.setOnClickListener(this);
        back.setOnClickListener(this);
        catatan.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        if (v == back) {
            Intent i = new Intent(getApplicationContext(), MainActivity.class);
            startActivity(i);
            finish();
        } else if (v == kunjungan) {
            sendAnalytic("Kunjungan");
            Intent i = new Intent(getApplicationContext(), ListAgendaActivity.class);
            startActivity(i);
            finish();
        } else if (v == jadwal_praktik) {
            sendAnalytic("Praktik");
            Intent i = new Intent(getApplicationContext(), ListJadwalPraktikActivity.class);
            startActivity(i);
        } else if (v == catatan) {
            sendAnalytic("Catatan");
            Intent i = new Intent(getApplicationContext(), CatatanActivity.class);
            startActivity(i);
        }
    }

        @Override
        public int getLayout () {
            return R.layout.activity_agenda;
        }

        @Override
        public void updateUI () {
            caldroidFragment = new CaldroidFragment();
            Bundle args = new Bundle();
            cal = Calendar.getInstance();
            args.putInt(CaldroidFragment.MONTH, cal.get(Calendar.MONTH) + 1);
            args.putInt(CaldroidFragment.YEAR, cal.get(Calendar.YEAR));
            caldroidFragment.setArguments(args);


            final CaldroidListener listener = new CaldroidListener() {

                @Override
                public void onSelectDate(Date date, View view) {

                }

                @Override
                public void onChangeMonth(int month, int year) {
                    // Refresh view
                    getCalenderData(month, year);
                    caldroidFragment.refreshView();
                }

                @Override
                public void onLongClickDate(Date date, View view) {

                }

                @Override
                public void onCaldroidViewCreated() {
                    Button leftButton = caldroidFragment.getLeftArrowButton();
                    Button rightButton = caldroidFragment.getRightArrowButton();
                    String MyColor = "#498FCD";
                    leftButton.getBackground().setColorFilter(Color.parseColor(MyColor), PorterDuff.Mode.SRC_IN);
                    rightButton.getBackground().setColorFilter(Color.parseColor(MyColor), PorterDuff.Mode.SRC_IN);
                }

            };

            caldroidFragment.setCaldroidListener(listener);
            android.support.v4.app.FragmentTransaction t = getSupportFragmentManager().beginTransaction();
            t.replace(R.id.frag_kalender, caldroidFragment);
            t.commit();
            getCalenderData(cal.get(Calendar.MONTH), cal.get(Calendar.YEAR));
            caldroidFragment.refreshView();
            if (preferences.getString(Preferences.LOADED, "").equals("")) {
                getData();
                getCalenderData(cal.get(Calendar.MONTH), cal.get(Calendar.YEAR));
                caldroidFragment.refreshView();
            }
        }




    @Override
    protected void onActivityResult ( int requestCode, int resultCode, Intent data){

    }

    void getCalenderData(int month, int year) {
        try {
            getCalendar();


            String[] projection = new String[]{
                    CalendarContract.Calendars._ID,
                    CalendarContract.Calendars.NAME,
                    CalendarContract.Calendars.ACCOUNT_NAME,
                    CalendarContract.Calendars.CALENDAR_DISPLAY_NAME
            };

            Cursor cursor = getContentResolver().query(
                    CalendarContract.Calendars.CONTENT_URI,
                    projection,
                    null,
                    null,
                    CalendarContract.Calendars._ID + " ASC");

            String idKalender = null;

            if (cursor.moveToFirst()) {

                do {
                    long id = cursor.getLong(0);
                    Log.i("getid", "id:  " + id);
                    String CALENDAR_DISPLAY_NAME = cursor.getString(3);
                    Log.i("getid", "displayname:  " + CALENDAR_DISPLAY_NAME);

                    if (CALENDAR_DISPLAY_NAME.contains("IDAI")) {
                        idKalender = String.valueOf(id);
                        editor.putString(Preferences.ID_CALENDAR, idKalender);
                        editor.commit();
                    }
                    // ...
                } while (cursor.moveToNext());
            }


            ////////////////
            final String[] INSTANCE_PROJECTION = new String[]{
                    CalendarContract.Instances.EVENT_ID,      // 0
                    CalendarContract.Instances.BEGIN,         // 1
                    CalendarContract.Instances.TITLE          // 2
            };

            // The indices for the projection array above.
            final int PROJECTION_ID_INDEX = 0;
            final int PROJECTION_BEGIN_INDEX = 1;
            final int PROJECTION_TITLE_INDEX = 2;


            // Specify the date range you want to search for recurring
            // event instances
            Calendar beginTime2 = Calendar.getInstance();
            beginTime2.set(year, month - 1, 0, 0, 0);
            long startMillis2 = beginTime2.getTimeInMillis();
            Calendar endTime2 = Calendar.getInstance();
            endTime2.set(year + 1, month, 1, 1, 0);
            long endMillis2 = endTime2.getTimeInMillis();

            Cursor cur = null;
            ContentResolver cr2 = getContentResolver();

            // The ID of the recurring event whose instances you are searching
            // for in the Instances table
            String selection = CalendarContract.Instances.CALENDAR_ID + " = ?";
            String[] selectionArgs = new String[]{idKalender};

            // Construct the query with the desired date range.
            Uri.Builder builder = CalendarContract.Instances.CONTENT_URI.buildUpon();
            ContentUris.appendId(builder, startMillis2);
            ContentUris.appendId(builder, endMillis2);

            // Submit the query
            cur = cr2.query(builder.build(),
                    INSTANCE_PROJECTION,
                    selection,
                    selectionArgs,
                    null);

            while (cur.moveToNext()) {
                String title = null;
                //long eventID = 0;
                long beginVal = 0;

                // Get the field values
                //  eventID = cur.getLong(PROJECTION_ID_INDEX);
                beginVal = cur.getLong(PROJECTION_BEGIN_INDEX);
                title = cur.getString(PROJECTION_TITLE_INDEX);

                // Do something with the values.
                Log.i("time", "Event:  " + title);
                Calendar calendar = Calendar.getInstance();
                calendar.setTimeInMillis(beginVal);
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat(
                        "yyyy-MM-dd", Locale.getDefault());
                Log.i("time", "Date: " + simpleDateFormat.format(calendar.getTime()));

                try {
                    Date date = simpleDateFormat.parse(simpleDateFormat.format(calendar.getTime()));

                    Date now = simpleDateFormat.parse(simpleDateFormat.format(cal.getTime()));

                    if (date.before(now)) {
                        caldroidFragment.setBackgroundResourceForDate(
                                R.color.gray, date);
                    } else {
                        caldroidFragment.setBackgroundResourceForDate(
                                R.color.blue_slider, date);
                    }
                } catch (ParseException e) {
                    e.printStackTrace();
                }

            }
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
            //  dialog_sesi_offline_pelod(getResources().getString(R.string.KalenderAgendaDialog), getResources().getString(R.string.Perhatian));
        }

    }


    public void createCalendar() {
        try {
            AccountManager manager = AccountManager.get(context);
            Account[] accounts = manager.getAccountsByType("com.google");

            String accountName = "";
            String accountType = "";

            for (Account account : accounts) {
                accountName = account.name;
                accountType = account.type;
                break;
            }

            ContentResolver cr = context.getContentResolver();
            ContentValues values = new ContentValues();

            values.put(CalendarContract.Calendars.ACCOUNT_NAME, accountName);
            //values.put(CalendarContract.Calendars.ACCOUNT_TYPE, accountType);
            values.put(CalendarContract.Calendars.ACCOUNT_TYPE, CalendarContract.ACCOUNT_TYPE_LOCAL);
            values.put(CalendarContract.Calendars.NAME, "Kalender IDAI");
            values.put(CalendarContract.Calendars.CALENDAR_DISPLAY_NAME, "Kalender IDAI");
            values.put(CalendarContract.Calendars.SYNC_EVENTS, 1);
            values.put(CalendarContract.Calendars.VISIBLE, 1);
            values.put(CalendarContract.Calendars.CALENDAR_ACCESS_LEVEL, CalendarContract.Calendars.CAL_ACCESS_OWNER);
            values.put(CalendarContract.Calendars.OWNER_ACCOUNT, accountName);
            values.put(CalendarContract.Calendars.DIRTY, 1);
            values.put(CalendarContract.Calendars.CALENDAR_TIME_ZONE, TimeZone.getDefault().getID());

            Uri calUri = CalendarContract.Calendars.CONTENT_URI;

            calUri = calUri.buildUpon()
                    .appendQueryParameter(CalendarContract.CALLER_IS_SYNCADAPTER, "true")
                    .appendQueryParameter(CalendarContract.Calendars.ACCOUNT_NAME, accountName)
                    .appendQueryParameter(CalendarContract.Calendars.ACCOUNT_TYPE, accountType)
                    .build();

            Uri result = cr.insert(calUri, values);
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
            dialog(getResources().getString(R.string.KalenderAgendaDialog), getResources().getString(R.string.Perhatian));
        }
    }


    void getCalendar() {
        String[] projection = new String[]{
                CalendarContract.Calendars._ID,
                CalendarContract.Calendars.NAME,
                CalendarContract.Calendars.ACCOUNT_NAME,
                CalendarContract.Calendars.CALENDAR_DISPLAY_NAME
        };

        Cursor cursor = getContentResolver().query(
                CalendarContract.Calendars.CONTENT_URI,
                projection,
                null,
                null,
                CalendarContract.Calendars._ID + " ASC");
        boolean kosong = true;

        if (cursor.moveToFirst()) {

            do {
                long id = cursor.getLong(0);
                Log.i("getid", "id:  " + id);
                String CALENDAR_DISPLAY_NAME = cursor.getString(3);
                Log.i("getid", "displayname:  " + CALENDAR_DISPLAY_NAME);

                if (CALENDAR_DISPLAY_NAME.contains("IDAI")) {
                    kosong = false;
                }
                // ...
            } while (cursor.moveToNext());
        }

        if (kosong) {
            createCalendar();
        }

    }


    void deleteCalendar() {
        try {
            Uri evuri = CalendarContract.Calendars.CONTENT_URI;
            //substitue your calendar id into the 0
            long calid = Long.valueOf(preferences.getString(Preferences.ID_CALENDAR, ""));
            Uri deleteUri = ContentUris.withAppendedId(evuri, calid);
            getContentResolver().delete(deleteUri, null, null);
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
    }


    void saveEvent(Agenda.Agendas agenda) {
        try {
            //SAVING EVENT
            long calID = Long.parseLong(preferences.getString(Preferences.ID_CALENDAR, ""));
            long startMillis = 0;
            long endMillis = 0;

            Calendar beginTime = Calendar.getInstance();
            beginTime.set(getTahun(agenda.getTanggal_kunjungan()),
                    getBulani(agenda.getTanggal_kunjungan()),
                    getHari(agenda.getTanggal_kunjungan()),
                    getJam(agenda.getJam_mulai()),
                    getMenit(agenda.getJam_mulai()));
            startMillis = beginTime.getTimeInMillis();
            Calendar endTime = Calendar.getInstance();
            endTime.set(getTahun(agenda.getTanggal_kunjungan()),
                    getBulani(agenda.getTanggal_kunjungan()),
                    getHari(agenda.getTanggal_kunjungan()),
                    getJam(agenda.getJam_akhir()),
                    getMenit(agenda.getJam_akhir()));
            endMillis = endTime.getTimeInMillis();


            ContentResolver cr = getApplicationContext().getContentResolver();
            ContentValues values = new ContentValues();
            values.put(CalendarContract.Events.DTSTART, startMillis);
            values.put(CalendarContract.Events.DTEND, endMillis);
            values.put(CalendarContract.Events.TITLE, "Event Kunjungan");
            values.put(CalendarContract.Events.DESCRIPTION, agenda.getSub_judul());
            values.put(CalendarContract.Events.CALENDAR_ID, calID);
            values.put(CalendarContract.Events.EVENT_LOCATION, agenda.getLokasi());
            if (agenda.getPengingat().equals("0")) {
                values.put(CalendarContract.Events.HAS_ALARM, 0);
            } else {
                values.put(CalendarContract.Events.HAS_ALARM, 1);
            }
            values.put(CalendarContract.Events.EVENT_TIMEZONE, TimeZone.getDefault().toString());
            Uri uri = cr.insert(CalendarContract.Events.CONTENT_URI, values);

            eventID = Long.parseLong(uri.getLastPathSegment());

            if (!agenda.getPengingat().equals("0")) {
                values.clear();
                values.put(CalendarContract.Reminders.EVENT_ID, eventID);
                values.put(CalendarContract.Reminders.METHOD, CalendarContract.Reminders.METHOD_ALERT);
                values.put(CalendarContract.Reminders.MINUTES, Integer.valueOf(agenda.getPengingat()));
                getContentResolver().insert(CalendarContract.Reminders.CONTENT_URI, values);
            }

            Log.i("RunnerBecomes", "event id: " + eventID);
            // DONE SAVING EVENT
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
    }


    public void dialog(String content, String msg) {
        new MaterialDialog.Builder(this)
                .title(msg)
                .content(content)
                .positiveText("Ok")
                .callback(new MaterialDialog.ButtonCallback() {
                    @Override
                    public void onPositive(MaterialDialog dialog) {
                    }

                })
                .show();
    }

    public void dialog2(String content, String msg) {
        new MaterialDialog.Builder(this)
                .title(msg)
                .content(content)
                .negativeText("OK")
                .callback(new MaterialDialog.ButtonCallback() {
                    @Override
                    public void onPositive(MaterialDialog dialog) {

                    }

                    @Override
                    public void onNegative(MaterialDialog dialog) {
                        super.onNegative(dialog);
                    }

                })
                .show();
    }

    private void progresDialog(boolean horizontal, String msg) {
        dialog = new MaterialDialog.Builder(this)
                .title(msg)
                .content(getResources().getString(R.string.MohonTunggu))
                .progress(true, 0)
                .progressIndeterminateStyle(horizontal)
                .cancelable(false)
                .show();
    }

    public int getTahun(String tanggal) {
        int tahun = Integer.valueOf(tanggal.substring(0, 4));
        return tahun;
    }

    public int getBulani(String tanggal) {
        int bulan = Integer.valueOf(tanggal.substring(5, 7));
        return bulan - 1;
    }

    public int getHari(String tanggal) {
        int hari = Integer.valueOf(tanggal.substring(8, 10));
        return hari;
    }

    public int getJam(String time) {
        int jam = Integer.valueOf(time.substring(0, 2));
        return jam;
    }

    public int getMenit(String time) {
        int menit = Integer.valueOf(time.substring(3, 5));
        return menit;
    }


    private void getData() {
        progresDialog(false, getResources().getString(R.string.MemuatData));
        StringRequest listdata = new StringRequest(Request.Method.GET,
                ApiReferences.getAllAgenda(preferences.getString(Preferences.ID_USER, "")),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        dialog.dismiss();
                        try {
                            if (response.equals("[]")) {
                                dialog(getResources().getString(R.string.DataBelumTersedia), getResources().getString(R.string.Perhatian));
                            } else {
                                Message msg = Parser.getMessage(response);
                                if (msg.getStatus().equals("0")) {
                                    dialog(msg.getMessages(), getResources().getString(R.string.Perhatian));

                                    if (msg.getMessages().contains("exist")) {

                                    }

                                } else {
                                    Log.i("RESULT JSON", response.toString());
                                    final Agenda agnda = Parser.getAgenda(response);
                                    if (preferences.getString(Preferences.LOADED, "").equalsIgnoreCase("")) {
                                        deleteCalendar();
                                        getCalenderData(cal.get(Calendar.MONTH), cal.get(Calendar.YEAR));
                                        for (int i = 0; i < agnda.getObjects().size(); i++) {
                                            saveEvent(agnda.getObjects().get(i));
                                            Agenda.Agendas agenda = new Agenda().new Agendas();
                                            agenda.setObjects(agnda.getObjects().get(i));
                                            agenda.getObjects().setId_kalender(preferences.getString(Preferences.ID_CALENDAR, ""));
                                            agenda.getObjects().setId_agenda(String.valueOf(eventID));
                                            db.addAgenda(agenda);
                                        }
                                        editor.putString(Preferences.LOADED, "OK");
                                    }
                                    editor.commit();
                                }
                            }
                        } catch (JsonSyntaxException e) {
                            dialog(getResources().getString(R.string.KesalahanServer), getResources().getString(R.string.Perhatian));
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dialog.dismiss();
                //dialog(getResources().getString(R.string.NoConnection), getResources().getString(R.string.Perhatian));
            }
        });
        listdata.setRetryPolicy(new DefaultRetryPolicy(
                20000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        mRequest.add(listdata);

    }


    @Override
    public void onBackPressed() {
        Intent i = new Intent(getApplicationContext(), MainActivity.class);
        startActivity(i);
        finish();
    }


}
