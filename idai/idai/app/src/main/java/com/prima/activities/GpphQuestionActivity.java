package com.prima.activities;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.prima.adapters.PagerAdapter;
import com.prima.controllers.Parser;
import com.prima.entities.Question;
import com.prima.fragments.GpphQuestionFragment;
import com.prima.helpers.NonSwipeAbleViewPager;

import java.util.ArrayList;
import java.util.List;

import prima.test.prima.R;

/**
 * Created by Codelabs on 18/08/2015.
 */
public class GpphQuestionActivity extends BaseActivity implements View.OnClickListener {
    private ImageView close;
    private ImageView back;
    private Button ok;
    private Context mContext;
    private NonSwipeAbleViewPager mPager;
    private PagerAdapter mPagerAdapter;
    private int currentPage;
    PageListener pageListener;
    static TextView title, page, questionTitle;
    LinearLayout toolbar;
    private ProgressBar progres;
    static int progresValue;
    static Question question;
    static List<Fragment> fragmentList = new ArrayList<Fragment>();
    private ArrayList<String> ansewer = new ArrayList<String>();
    private int max_score = 0;

    public int getMax_score() {
        return max_score;
    }

    public void setMax_score(int max_score) {
        this.max_score = max_score;
    }

    public List<Fragment> getFragmentList() {
        return fragmentList;
    }

    public ArrayList<String> getAnsewer() {
        return ansewer;
    }

    public void setAnsewer(ArrayList<String> ansewer) {
        this.ansewer = ansewer;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = getApplicationContext();
        currentPage = 0;

    }

    @Override
    public void initView() {
        page = (TextView) findViewById(R.id.page);
        progres = (ProgressBar) findViewById(R.id.progresBar);
        progres.setProgress(15);
        back = (ImageView) findViewById(R.id.back);
        mPager = (NonSwipeAbleViewPager) findViewById(R.id.pager);
        close = (ImageView) findViewById(R.id.close);
        pageListener = new PageListener();
        questionTitle = (TextView) findViewById(R.id.question);
        mPager.setOnPageChangeListener(pageListener);
        toolbar = (LinearLayout) findViewById(R.id.toolbar);

        title = (TextView) findViewById(R.id.tv_actionbar_title);
        Typeface tf = Typeface.createFromAsset(getApplicationContext().getAssets(), "BREVIA-BLACK.OTF");
        title.setTypeface(tf);
        title.setText(getResources().getString(R.string.titleGPPH2));


        getData();
    }

    @Override
    public void setUICallbacks() {
        back.setOnClickListener(this);
        close.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        if (v == back) {
            if (currentPage > 0) {
                currentPage--;
                mPager.setCurrentItem(currentPage, true);
            } else {
                //     Intent i = new Intent(getApplicationContext(), GpphActivity.class);
                //    startActivity(i);
                finish();
            }
        } else if (v == close) {
            // Intent i = new Intent(getApplicationContext(), GpphActivity.class);
            //   startActivity(i);
            finish();
        }
    }

    void getData() {
        Bundle bn = getIntent().getExtras();
        if (bn != null) {
            String response = bn.getString("respon");
            setupFrae();
            Log.i("RESULT JSON", response);
            question = new Question();
            question = Parser.getQuestion(response);
            questionTitle.setText(question.getObjects().get(0).getTitle());
            fragmentList.clear();
            for (int i = 0; i < question.getObjects().size(); i++) {
                for (int j = 0; j < question.getObjects().get(i).getAnswer_option().size(); j++) {
                    if (j == question.getObjects().get(i).getAnswer_option().size() - 1) {
                        max_score = max_score + Integer.valueOf(question.getObjects().get(i).getAnswer_option().get(j).getScore().replace(".00",""));
                    }
                }

                fragmentList.add(new GpphQuestionFragment(question.getObjects().get(i), i));
                ansewer.add("0");
            }
            progresValue = 100 / fragmentList.size();
            progres.setProgress(progresValue);
            setupFrae();

            page.setText(page.getResources().getString(R.string.dari2) + " " + fragmentList.size() + " " + page.getResources().getString(R.string.penilaian));
        }
    }


    @Override
    public int getLayout() {
        return R.layout.activity_gpph_question;
    }

    @Override
    public void updateUI() {

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

    }


    @Override
    public void onBackPressed() {
        if (currentPage > 0) {
            currentPage--;
            mPager.setCurrentItem(currentPage, true);
        } else {
            // Intent i = new Intent(getApplicationContext(), GpphActivity.class);
            //     startActivity(i);
            finish();
        }
    }

    private class PageListener extends ViewPager.SimpleOnPageChangeListener {


        public void onPageSelected(int position) {
            currentPage = position;
            questionTitle.setText(question.getObjects().get(position).getTitle());
            page.setText(currentPage + 1 + " " + page.getResources().getString(R.string.dari) + " "+ fragmentList.size() + " " + page.getResources().getString(R.string.penilaian));
            progres.setProgress(progresValue * (position + 1));
            if ((position + 1) == question.getObjects().size()) {
                progres.setProgress(100);
            }
        }
    }


    private void setupFrae() {
        mPagerAdapter = new PagerAdapter(getSupportFragmentManager(),
                GpphQuestionActivity.this, fragmentList);
        mPager.setAdapter(mPagerAdapter);

    }

}
