package com.prima.activities;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.prima.Preferences;
import com.prima.adapters.PagerAdapter;
import com.prima.controllers.Parser;
import com.prima.entities.Question;
import com.prima.fragments.PgcsQuesionerFragment;
import com.prima.helpers.NonSwipeAbleViewPager;

import org.joda.time.LocalDate;
import org.joda.time.Period;
import org.joda.time.PeriodType;

import java.util.ArrayList;
import java.util.List;

import prima.test.prima.R;

/**
 * Created by Codelabs on 07/08/2015.
 */
public class PgcsQuestionActivity extends BaseActivity implements View.OnClickListener {
    private ImageView close;
    private ImageView back;
    private Button ok;
    private Context mContext;
    private NonSwipeAbleViewPager mPager;
    private PagerAdapter mPagerAdapter;
    private static int currentPage;
    private PageListener pageListener;
    private static TextView title, page, title_question, sub_title_question;
    private static ProgressBar progres;
    static int progresValue;
    static List<Fragment> fragmentList = new ArrayList<Fragment>();
    static Question question;
    private ArrayList<String> ansewer = new ArrayList<String>();
    private ArrayList<String> kode = new ArrayList<String>();

    public int getMax_score() {
        return max_score;
    }

    public void setMax_score(int max_score) {
        this.max_score = max_score;
    }

    private int max_score = 0;



    public ArrayList<String> getKode() {
        return kode;
    }

    public void setKode(ArrayList<String> kode) {
        this.kode = kode;
    }

    public List<Fragment> getFragmentList() {
        return fragmentList;
    }

    public ArrayList<String> getAnsewer() {
        return ansewer;
    }

    public void setAnsewer(ArrayList<String> ansewer) {
        this.ansewer = ansewer;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = getApplicationContext();
        currentPage = 0;
    }

    @Override
    public void initView() {
        title_question = (TextView) findViewById(R.id.title_question);
        sub_title_question = (TextView) findViewById(R.id.sub_title_question);
        page = (TextView) findViewById(R.id.page);
        progres = (ProgressBar) findViewById(R.id.progres);
        progres.setProgress(35);
        back = (ImageView) findViewById(R.id.back);
        mPager = (NonSwipeAbleViewPager) findViewById(R.id.pager);
        close = (ImageView) findViewById(R.id.close);
        pageListener = new PageListener();
        mPager.setOnPageChangeListener(pageListener);
        title = (TextView) findViewById(R.id.tv_actionbar_title);
        Typeface tf = Typeface.createFromAsset(getApplicationContext().getAssets(), "BREVIA-BLACK.OTF");
        title.setTypeface(tf);
        title.setText("PGCS");
        getData();


        LocalDate birthdate = new LocalDate(preferences.getString(Preferences.BIRTH_DAY, ""));          //Birth date
        LocalDate today = new LocalDate();                    //Today's date
        Period period = new Period(birthdate, today, PeriodType.yearMonthDay());


        String temp = "";
        if (period.getYears() > 0) {
            temp = temp + period.getYears() + " " + getResources().getString(R.string.tahun);
        }

        if (period.getWeeks() > 0) {
            temp = temp + period.getWeeks() + " " + getResources().getString(R.string.minggu);
        }

        if (period.getMonths() > 0) {
            temp = temp + period.getMonths() + " " + getResources().getString(R.string.bulan);
        }

        if (period.getDays() > 0) {
            temp = temp + period.getDays() + " " + getResources().getString(R.string.Hari);
        }

        if (temp.equals("")) {
            temp = getResources().getString(R.string.Lahir);
        }

        sub_title_question.setText(temp);
    }

    @Override
    public void setUICallbacks() {
        back.setOnClickListener(this);
        close.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        if (v == back) {
            if (currentPage > 0) {
                currentPage--;
                mPager.setCurrentItem(currentPage, true);
            } else {
                if (preferences.getString(Preferences.ID, "").equals("1")) {
                    editor.putString(Preferences.BIRTH_DAY, "");
                    editor.putString(Preferences.JENIS_KELAMIN, "");
                    editor.putString(Preferences.BERAT, "");
                    editor.putString(Preferences.TINGGI, "");
                    editor.putString(Preferences.ID, "");
                    editor.commit();
                }
                //  Intent i = new Intent(getApplicationContext(), PgcsActivity.class);
                // startActivity(i);
                finish();
            }
        } else if (v == close) {
            if (preferences.getString(Preferences.ID, "").equals("1")) {
                editor.putString(Preferences.BIRTH_DAY, "");
                editor.putString(Preferences.JENIS_KELAMIN, "");
                editor.putString(Preferences.BERAT, "");
                editor.putString(Preferences.TINGGI, "");
                editor.putString(Preferences.ID, "");
                editor.commit();
            }
            //  Intent i = new Intent(getApplicationContext(), PgcsActivity.class);
            //   startActivity(i);
            finish();
        }
    }


    void getData() {
        Bundle bn = getIntent().getExtras();
        if (bn != null) {
            String response = bn.getString("respon");
            setupFrae();
            Log.i("RESULT JSON", response);
            question = new Question();
            question = Parser.getQuestion(response);
            title_question.setText(question.getObjects().get(0).getTitle());
            fragmentList.clear();
            for (int i = 0; i < question.getObjects().size(); i++) {
                for (int j = 0; j < question.getObjects().get(i).getAnswer_option().size(); j++) {
                    if (j == 0) {
                        max_score = max_score + Integer.valueOf(question.getObjects().get(i).getAnswer_option().get(j).getScore().replace(".00",""));
                    }
                }

                fragmentList.add(new PgcsQuesionerFragment(question.getObjects().get(i), i));
                ansewer.add("");
                kode.add("");
            }
            progresValue = 100 / fragmentList.size();
            progres.setProgress(progresValue);
            setupFrae();

            page.setText(page.getResources().getString(R.string.dari2) + " " + fragmentList.size() + " " + page.getResources().getString(R.string.penilaian));
        }
    }


    @Override
    public int getLayout() {
        return R.layout.activity_pgcs_question;
    }

    @Override
    public void updateUI() {

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

    }


    private static class PageListener extends ViewPager.SimpleOnPageChangeListener {


        public void onPageSelected(int position) {
            currentPage = position;
            page.setText(currentPage + 1 + " " + page.getResources().getString(R.string.dari) + " "+ fragmentList.size() + " " + page.getResources().getString(R.string.penilaian));
            progres.setProgress(progresValue * (position + 1));
            title_question.setText(question.getObjects().get(position).getTitle());

            if ((position + 1) == question.getObjects().size()) {
                progres.setProgress(100);
            }
        }
    }


    private void setupFrae() {
        mPagerAdapter = new PagerAdapter(getSupportFragmentManager(),
                PgcsQuestionActivity.this, fragmentList);
        mPager.setAdapter(mPagerAdapter);

    }

    @Override
    public void onBackPressed() {
        if (currentPage > 0) {
            currentPage--;
            mPager.setCurrentItem(currentPage, true);
        } else {
            if (preferences.getString(Preferences.ID, "").equals("1")) {
                editor.putString(Preferences.BIRTH_DAY, "");
                editor.putString(Preferences.JENIS_KELAMIN, "");
                editor.putString(Preferences.BERAT, "");
                editor.putString(Preferences.TINGGI, "");
                editor.putString(Preferences.ID, "");
                editor.commit();
            }
            //   Intent i = new Intent(getApplicationContext(), PgcsActivity.class);
            // startActivity(i);
            finish();
        }
    }

}
