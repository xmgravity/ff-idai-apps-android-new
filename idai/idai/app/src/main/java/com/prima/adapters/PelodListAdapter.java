package com.prima.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.prima.entities.Question;

import java.util.ArrayList;

import prima.test.prima.R;

/**
 * Created by Codelabs on 07/08/2015.
 */
public class PelodListAdapter extends BaseAdapter {
    private ArrayList<Question.Answer> _data;
    Context _context;

    public String getSelected() {
        return selected;
    }

    public void setSelected(String selected) {
        this.selected = selected;
    }

    String selected;

    public PelodListAdapter(ArrayList<Question.Answer> data,
                            Context context, String selected) {
        _data = data;
        _context = context;
        this.selected = selected;
    }

    @Override
    public int getCount() {

        return _data.size();
    }

    @Override
    public Object getItem(int position) {

        return _data.get(position);
    }

    @Override
    public long getItemId(int position) {

        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        if (v == null) {
            LayoutInflater infLayout = (LayoutInflater) _context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = infLayout.inflate(R.layout.list_item_pelod_question, null);
        }

        TextView text = (TextView) v.findViewById(R.id.txt_question);
        ImageView img = (ImageView) v.findViewById(R.id.radio);
        LinearLayout layout = (LinearLayout) v.findViewById(R.id.layout);

        text.setText(_data.get(position).getOption());

        if (selected.equals(String.valueOf(_data.get(position).getScore()))) {
            img.setBackgroundResource(R.mipmap.ic_checked);
        } else {
            img.setBackgroundResource(R.mipmap.box_unchecked);
        }

        return v;
    }
}
