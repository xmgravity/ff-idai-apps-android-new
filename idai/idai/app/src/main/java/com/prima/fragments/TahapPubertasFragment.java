package com.prima.fragments;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import com.prima.Preferences;
import com.prima.activities.TahapPubertasMateri;
import com.prima.activities.TahapPubertasResultActivity;
import com.prima.adapters.AdapterTahapPubertas;
import com.prima.entities.Pubertas;

import prima.test.prima.R;

/**
 * Created by Codelabs on 11/08/2015.
 */
public class TahapPubertasFragment extends BaseFragment implements View.OnClickListener {
    private Context mContext;
    public static final String ARG_PAGE = "page 1";
    private ListView list;
    private ViewPager mPager;
    String selected = "";
    ArrayList<Pubertas.Item> items;
    int indeks;
    TextView txt, tahap;
    List<Fragment> fragmentList = new ArrayList<Fragment>();
    private ArrayList<String> ansewer = new ArrayList<String>();

    @SuppressLint("ValidFragment")
    public TahapPubertasFragment() {}
    @SuppressLint("ValidFragment")
    public TahapPubertasFragment(ArrayList<Pubertas.Item> items, int ind) {
        this.items = items;
        this.indeks = ind;
    }

    @Override
    public void initView(View view) {
        txt = (TextView) view.findViewById(R.id.title_materi);
        tahap = (TextView) view.findViewById(R.id.tahap);
        list = (ListView) view.findViewById(R.id.listView);
        mPager = (ViewPager) getActivity().findViewById(R.id.pager);
        mContext = getActivity();
        Activity act = getActivity();


        txt.setText(items.get(indeks).getTitle());
        if (items.get(indeks).getTitle().equalsIgnoreCase("Volume Testis")) {
            tahap.setVisibility(View.GONE);
        }

        if (act instanceof TahapPubertasMateri) {
            fragmentList = ((TahapPubertasMateri) act).getFragmentList();
            ansewer = ((TahapPubertasMateri) act).getAnsewer();
        }


        final AdapterTahapPubertas adapterList = new AdapterTahapPubertas(
                items, mContext, selected);
        list.setAdapter(adapterList);


        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                ansewer.set(indeks, items.get(position).getCode_image());
                selected = String.valueOf(items.get(position).getCode_image());
                adapterList.setSelected(selected);
                adapterList.notifyDataSetChanged();
                int current = mPager.getCurrentItem();
                if (current != fragmentList.size() - 1) {
                    mPager.setCurrentItem(current + 1, true);
                } else {
                    String score = "";
                    for (int j = 0; j < ansewer.size(); j++) {
                        score = score + ansewer.get(j).replace(".00", "");
                    }
                    if (preferences.getString(Preferences.ID, "").equals("1")) {
                        editor.putString(Preferences.BIRTH_DAY, "");
                        editor.putString(Preferences.JENIS_KELAMIN, "");
                        editor.putString(Preferences.BERAT, "");
                        editor.putString(Preferences.TINGGI, "");
                        editor.putString(Preferences.ID, "");
                        editor.commit();
                    }
                    Intent i = new Intent(mContext, TahapPubertasResultActivity.class);
                    i.putExtra("result", score);
                    startActivity(i);
                    getActivity().finish();
                }

            }
        });
    }

    @Override
    public void setUICallbacks() {

    }

    @Override
    public void updateUI() {

    }

    @Override
    public String getPageTitle() {
        return "Account Summary";
    }


    @Override
    public int getFragmentLayout() {
        return R.layout.fragment_tahap_pubertas;
    }

    @Override
    public void onClick(View v) {

    }

}
