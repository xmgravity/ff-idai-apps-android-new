package com.prima.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.prima.entities.Question;

import java.util.ArrayList;

import prima.test.prima.R;

/**
 * Created by Codelabs on 07/08/2015.
 */
public class ListAdapter extends BaseAdapter {
    private ArrayList<Question.Answer> _data;
    Context _context;
    String selected;

    public String getSelected() {
        return selected;
    }

    public void setSelected(String selected) {
        this.selected = selected;
    }


    public ListAdapter(ArrayList<Question.Answer> data,
                       Context context, String selected) {
        _data = data;
        _context = context;
        this.selected = selected;
    }

    @Override
    public int getCount() {

        return _data.size();
    }

    @Override
    public Object getItem(int position) {

        return _data.get(position);
    }

    @Override
    public long getItemId(int position) {

        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        if (v == null) {
            LayoutInflater infLayout = (LayoutInflater) _context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = infLayout.inflate(R.layout.list_item_pertanyaan, null);

        }

        TextView text = (TextView) v.findViewById(R.id.txt_question);
        TextView score = (TextView) v.findViewById(R.id.txt_score);
        RelativeLayout layout = (RelativeLayout) v.findViewById(R.id.layout);
        text.setText(_data.get(position).getOption());
        score.setText(_data.get(position).getScore().replace(".00",""));


        if (selected.equals(String.valueOf(_data.get(position).getScore()))) {
            layout.setBackgroundResource(R.color.blue_muda);
        } else {
            layout.setBackgroundResource(R.color.caldroid_transparent);
        }

        return v;
    }
}
