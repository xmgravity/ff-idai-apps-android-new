package com.prima.activities;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.CalendarContract;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TimePicker;

import com.afollestad.materialdialogs.MaterialDialog;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.JsonSyntaxException;
import com.prima.Preferences;
import com.prima.controllers.ApiReferences;
import com.prima.controllers.Parser;
import com.prima.entities.Agenda;
import com.prima.entities.Message;
import com.prima.helpers.DatabaseHandler;
import com.prima.helpers.DateHelper;
import com.prima.helpers.TimeHelper;

import org.joda.time.Days;
import org.joda.time.LocalDate;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import java.util.TimeZone;

import prima.test.prima.R;

/**
 * Created by Codelabs on 15/09/2015.
 */
public class AgendaEditActivity extends BaseActivity implements View.OnClickListener {
    TextView title, tanggal, tanggal2, waktu2, txt_alamat, txt_pengingat, bulan, keterangan, keterangan2;
    ImageView close, back;
    Context mContext;
    RelativeLayout alamat, reminder;
    LinearLayout waktu;
    private TimeHelper timeHelper;
    private DateHelper dateHelper;
    String mulai = "";
    String selesai = "";
    String latitude = "";
    String longitude = "";
    private MaterialDialog dialog;
    private RequestQueue mRequest;
    Button simpan;
    EditText konsultasi, nama_pasien;
    String pengingat = "0";
    String suara = "0";
    String getar = "0";
    DatabaseHandler db;
    long eventID;
    Agenda.Agendas agenda;

    @Override
    public void initView() {
        db = new DatabaseHandler(getApplicationContext());
        keterangan = (TextView) findViewById(R.id.keterangan);
        keterangan2 = (TextView) findViewById(R.id.keterangan2);
        nama_pasien = (EditText) findViewById(R.id.nama_pasien);
        konsultasi = (EditText) findViewById(R.id.konsultasi);
        simpan = (Button) findViewById(R.id.simpan);
        mRequest = Volley.newRequestQueue(getApplicationContext());
        bulan = (TextView) findViewById(R.id.bulan);
        alamat = (RelativeLayout) findViewById(R.id.alamat);
        reminder = (RelativeLayout) findViewById(R.id.reminder);
        txt_alamat = (TextView) findViewById(R.id.txt_alamat);
        txt_pengingat = (TextView) findViewById(R.id.txt_pengingat);
        waktu2 = (TextView) findViewById(R.id.waktu2);
        tanggal = (TextView) findViewById(R.id.tanggal);
        tanggal2 = (TextView) findViewById(R.id.tanggal2);
        waktu = (LinearLayout) findViewById(R.id.waktu);
        title = (TextView) findViewById(R.id.tv_actionbar_title);
        close = (ImageView) findViewById(R.id.close);
        back = (ImageView) findViewById(R.id.back);
        close.setVisibility(View.GONE);
        mContext = getApplicationContext();
        timeHelper = new TimeHelper();
        dateHelper = new DateHelper();
        //set font
        Typeface tf = Typeface.createFromAsset(getApplicationContext().getAssets(), "BREVIA-BLACK.OTF");
        title.setTypeface(tf);
        title.setText(getResources().getString(R.string.Kunjungan));

        getData();

    }

    @Override
    public void setUICallbacks() {
        back.setOnClickListener(this);
        waktu.setOnClickListener(this);
        tanggal.setOnClickListener(this);
        alamat.setOnClickListener(this);
        reminder.setOnClickListener(this);
        simpan.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        if (v == back) {
            Intent i = new Intent(getApplicationContext(), AgendaActivity.class);
            startActivity(i);
            finish();
        } else if (v == waktu) {
            // get prompts.xml view
            LayoutInflater li = LayoutInflater.from(getApplicationContext());
            View dialogView = li.inflate(R.layout.dialog_waktu, null);
            final Dialog dialog2;
            dialog2 = new Dialog(context);
            dialog2.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog2.setContentView(dialogView);
            //make transparent dialog_sesi_offline_pelod
            dialog2.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            dialog2.show();

            Button simpan = (Button) dialogView.findViewById(R.id.simpan);
            final TextView edt1 = (TextView) dialogView.findViewById(R.id.edt1);
            final TextView edt2 = (TextView) dialogView.findViewById(R.id.edt2);


            edt1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showDateTimePickerDialog(edt1);
                    mulai = edt1.getText().toString();

                }
            });

            edt2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showDateTimePickerDialog(edt2);
                    selesai = edt2.getText().toString();
                }
            });

            simpan.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    waktu2.setText(edt1.getText().toString() + " - " + edt2.getText().toString());
                    mulai = edt1.getText().toString();
                    selesai = edt2.getText().toString();

                    setTanggal(mulai, selesai);
                    dialog2.dismiss();

                }
            });

        } else if (v == tanggal) {
            showDatepickerDialog(tanggal2);
        } else if (v == alamat) {
            Intent i = new Intent(getApplicationContext(), SearchAlamatActivity.class);
            try {
                i.putExtra("longitude", longitude);
                i.putExtra("latitude", latitude);
            } catch (NullPointerException e) {
                e.printStackTrace();
            }
            startActivityForResult(i, 2);
            //    finish();
        } else if (v == reminder) {
            Intent i = new Intent(getApplicationContext(), AgendaPengingatActivity.class);
            i.putExtra("suara", suara);
            i.putExtra("getar", getar);
            i.putExtra("pengingat", pengingat);
            startActivityForResult(i, 1);
            //    finish();
        } else if (v == simpan) {
            if (tanggal2.getText().toString().trim().equals("")
                    || konsultasi.getText().toString().trim().equals("")
                    || mulai.trim().equals("")
                    || selesai.trim().equals("")
                    || latitude.equals("")) {
                dialog(getResources().getString(R.string.LengkapiDataKunjungan), getResources().getString(R.string.Perhatian));
            } else {
                save();
            }
        }
    }


    @Override
    public int getLayout() {
        return R.layout.activity_add_agenda;
    }

    @Override
    public void updateUI() {


    }

    private void save() {
        progresDialog(false, getResources().getString(R.string.MenyimpanData));
        StringRequest listdata = new StringRequest(Request.Method.POST,
                ApiReferences.updateAgenda(agenda.getObjects().getId()),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.i("RESULT JSON", response.toString());
                        dialog.dismiss();
                        try {
                            Message msg = Parser.getMessage(response);
                            if (msg.getStatus().equals("1")) {
                                Agenda.Agendas agenda = Parser.getAgendas(response);
                                deleteEvent(agenda.getObjects().getId());
                                saveEvent();
                                agenda.getObjects().setId_kalender(preferences.getString(Preferences.ID_CALENDAR, ""));
                                agenda.getObjects().setId_agenda(String.valueOf(eventID));
                                db.addAgenda(agenda);
                                finish();
                            } else {
                                dialog(msg.getMessages(), getResources().getString(R.string.Perhatian));
                            }
                        } catch (JsonSyntaxException e) {
                            dialog(getResources().getString(R.string.KesalahanServer), getResources().getString(R.string.Perhatian));
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dialog.dismiss();
                dialog(getResources().getString(R.string.NoConnection), getResources().getString(R.string.Perhatian));
            }
        }) {

            protected Map<String, String> getParams() {
                HashMap<String, String> params = new HashMap<String, String>();
                params.put("tanggal_kunjungan", tanggal2.getText().toString());
                params.put("sub_judul", konsultasi.getText().toString());
                params.put("jam_mulai", mulai);
                params.put("jam_akhir", selesai);
                params.put("lokasi", txt_alamat.getText().toString());
                params.put("pengingat", pengingat);
                params.put("suara", suara);
                params.put("getar", getar);
                params.put("longitude", longitude);
                params.put("latitude", latitude);
                params.put("nama_pasien", nama_pasien.getText().toString());
                return params;
            }
        };
        listdata.setRetryPolicy(new DefaultRetryPolicy(
                20000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        mRequest.add(listdata);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 2) {
            if (data != null) {
                txt_alamat.setText(data.getStringExtra("result"));
                longitude = data.getStringExtra("longitude");
                latitude = data.getStringExtra("latitude");
            }
        }

        if (requestCode == 1) {
            if (data != null) {
                String temp = data.getStringExtra("reminder");
                suara = data.getStringExtra("suara");
                getar = data.getStringExtra("getar");

                if (temp.equals("1")) {
                    pengingat = "1440";
                    txt_pengingat.setText(getResources().getString(R.string.Reminder1));
                }
                if (temp.equals("2")) {
                    pengingat = "180";
                    txt_pengingat.setText(getResources().getString(R.string.Reminder2));
                }
                if (temp.equals("3")) {
                    pengingat = "120";
                    txt_pengingat.setText(getResources().getString(R.string.Reminder3));
                }
                if (temp.equals("4")) {
                    pengingat = "60";
                    txt_pengingat.setText(getResources().getString(R.string.Reminder4));
                }
                if (temp.equals("5")) {
                    pengingat = "30";
                    txt_pengingat.setText(getResources().getString(R.string.Reminder5));
                }
                if (temp.equals("6")) {
                    pengingat = "15";
                    txt_pengingat.setText(getResources().getString(R.string.Reminder6));
                }
                if (temp.equals("7")) {
                    pengingat = "5";
                    txt_pengingat.setText(getResources().getString(R.string.Reminder7));
                }


            }
        }
    }


    private void showDateTimePickerDialog(final TextView txtView) {
        timeHelper.showDateTimePicker(this, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                String hours = String.valueOf(hourOfDay);
                String minutes = String.valueOf(minute);
                String realHours = null;
                String realMinutes = null;

                if (hours.length() == 1) {
                    realHours = "0" + hourOfDay;
                } else {
                    realHours = hours;
                }

                if (minutes.length() == 1) {
                    realMinutes = "0" + minute;
                } else {
                    realMinutes = minutes;
                }
                txtView.setText(realHours + ":" + String.valueOf(realMinutes));
            }
        });
    }

    private void showDatepickerDialog(final TextView txt) {
        dateHelper.showDatePicker(this, System.currentTimeMillis(), new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                int _year = year;
                int _month = monthOfYear + 1;
                int _day = dayOfMonth;
                String date = _year + "-" + _month + "-" + _day;
                String birthDate = dateHelper.formatDate(date);
                txt.setText(birthDate);
                tanggal.setText(birthDate.substring(8, 10));
                bulan.setText(getBulan(birthDate));

                if (getDays() == 0) {
                    keterangan.setText(getResources().getString(R.string.HariIni));
                } else if (getDays() == 1) {
                    keterangan.setText(getResources().getString(R.string.Besok));
                } else if (getDays() < 0) {
                    keterangan.setText(Math.abs(getDays()) + " " + getResources().getString(R.string.HariYgLalu));
                } else {
                    keterangan.setText(Math.abs(getDays()) + " " + getResources().getString(R.string.Hari));
                }


            }
        });
    }


    void saveEvent() {
        //SAVING EVENT
        long calID = Long.parseLong(preferences.getString(Preferences.ID_CALENDAR, ""));
        long startMillis = 0;
        long endMillis = 0;

        Calendar beginTime = Calendar.getInstance();
        beginTime.set(getTahun(tanggal2.getText().toString()),
                getBulani(tanggal2.getText().toString()),
                getHari(tanggal2.getText().toString()),
                getJam(mulai),
                getMenit(mulai));
        startMillis = beginTime.getTimeInMillis();
        Calendar endTime = Calendar.getInstance();
        endTime.set(getTahun(tanggal2.getText().toString()),
                getBulani(tanggal2.getText().toString()),
                getHari(tanggal2.getText().toString()),
                getJam(selesai),
                getMenit(selesai));
        endMillis = endTime.getTimeInMillis();


        ContentResolver cr = getApplicationContext().getContentResolver();
        ContentValues values = new ContentValues();
        values.put(CalendarContract.Events.DTSTART, startMillis);
        values.put(CalendarContract.Events.DTEND, endMillis);
        values.put(CalendarContract.Events.TITLE, "Event Kunjungan");
        values.put(CalendarContract.Events.DESCRIPTION, konsultasi.getText().toString());
        values.put(CalendarContract.Events.CALENDAR_ID, calID);
        values.put(CalendarContract.Events.EVENT_LOCATION, txt_alamat.getText().toString());
        if (pengingat.equals("0")) {
            values.put(CalendarContract.Events.HAS_ALARM, 0);
        } else {
            values.put(CalendarContract.Events.HAS_ALARM, 1);
        }
        values.put(CalendarContract.Events.EVENT_TIMEZONE, TimeZone.getDefault().toString());
        Uri uri = cr.insert(CalendarContract.Events.CONTENT_URI, values);

        eventID = Long.parseLong(uri.getLastPathSegment());

        if (!pengingat.equals("0")) {
            values.clear();
            values.put(CalendarContract.Reminders.EVENT_ID, eventID);
            values.put(CalendarContract.Reminders.METHOD, CalendarContract.Reminders.METHOD_ALERT);
            values.put(CalendarContract.Reminders.MINUTES, Integer.valueOf(pengingat));
            getContentResolver().insert(CalendarContract.Reminders.CONTENT_URI, values);
        }

        Log.i("RunnerBecomes", "event id: " + eventID);
        // DONE SAVING EVENT
    }


    public void dialog(String content, String msg) {
        new MaterialDialog.Builder(this)
                .title(msg)
                .content(content)
                .positiveText("Ok")
                .callback(new MaterialDialog.ButtonCallback() {
                    @Override
                    public void onPositive(MaterialDialog dialog) {
                    }

                })
                .show();
    }

    public void dialog2(String content, String msg) {
        new MaterialDialog.Builder(this)
                .title(msg)
                .content(content)
                .negativeText("OK")
                .callback(new MaterialDialog.ButtonCallback() {
                    @Override
                    public void onPositive(MaterialDialog dialog) {

                    }

                    @Override
                    public void onNegative(MaterialDialog dialog) {
                        super.onNegative(dialog);
                    }

                })
                .show();
    }

    private void progresDialog(boolean horizontal, String msg) {
        dialog = new MaterialDialog.Builder(this)
                .title(msg)
                .content(getResources().getString(R.string.MohonTunggu))
                .progress(true, 0)
                .progressIndeterminateStyle(horizontal)
                .cancelable(false)
                .show();
    }


    @Override
    public void onBackPressed() {
        Intent i = new Intent(getApplicationContext(), AgendaActivity.class);
        startActivity(i);
        finish();
    }


    String getBulan(String tanggal) {
        String bulan = tanggal.substring(5, 7);
        if (bulan.equals("01")) {
            bulan = "JAN";
        }
        if (bulan.equals("02")) {
            bulan = "FEB";
        }
        if (bulan.equals("03")) {
            bulan = "MAR";
        }
        if (bulan.equals("04")) {
            bulan = "APR";
        }
        if (bulan.equals("05")) {
            bulan = "MEI";
        }
        if (bulan.equals("06")) {
            bulan = "JUN";
        }
        if (bulan.equals("07")) {
            bulan = "JUL";
        }
        if (bulan.equals("08")) {
            bulan = "AGS";
        }
        if (bulan.equals("09")) {
            bulan = "SEP";
        }
        if (bulan.equals("10")) {
            bulan = "OKT";
        }
        if (bulan.equals("11")) {
            bulan = "NOV";
        }
        if (bulan.equals("12")) {
            bulan = "DES";
        }


        return bulan;
    }

    public int getTahun(String tanggal) {
        int tahun = Integer.valueOf(tanggal.substring(0, 4));
        return tahun;
    }

    public int getBulani(String tanggal) {
        int bulan = Integer.valueOf(tanggal.substring(5, 7));
        return bulan - 1;
    }

    public int getHari(String tanggal) {
        int hari = Integer.valueOf(tanggal.substring(8, 10));
        return hari;
    }

    public int getJam(String time) {
        int jam = Integer.valueOf(time.substring(0, 2));
        return jam;
    }

    public int getMenit(String time) {
        int menit = Integer.valueOf(time.substring(3, 5));
        return menit;
    }


    public int getDays() {
        Days days = Days.daysBetween(new LocalDate(), new LocalDate(tanggal2.getText().toString()));
        return days.getDays();
    }


    public void setTanggal(String mulai, String selesai) {
        int jam1 = getJam(mulai);
        int menit1 = getMenit(mulai);
        int jam2 = getJam(selesai);
        int menit2 = getMenit(selesai);

        int dif_jam = jam2 - jam1;
        int dif_menit = menit2 - menit1;

        if (dif_jam == 0 && dif_menit == 0) {
            keterangan2.setText("Durasi : 0 Menit");
        } else if (dif_jam > 0 && dif_menit > 0) {
            keterangan2.setText("Durasi : " + dif_jam + " Jam " + dif_menit + " Menit");
        } else if (dif_jam > 0 && dif_menit <= 0) {
            keterangan2.setText("Durasi : " + dif_jam + " Jam");
        } else if (dif_jam <= 0 && dif_menit > 0) {
            keterangan2.setText("Durasi : " + dif_menit + " menit");
        }


    }


    void getData() {
        Bundle bn = getIntent().getExtras();
        if (bn != null) {
            agenda = Parser.getAgendas(bn.getString("respon"));
            tanggal2.setText(agenda.getObjects().getTanggal_kunjungan());
            konsultasi.setText(agenda.getObjects().getSub_judul());
            mulai = agenda.getObjects().getJam_mulai();
            selesai = agenda.getObjects().getJam_akhir();
            txt_alamat.setText(agenda.getObjects().getLokasi());
            pengingat = agenda.getObjects().getPengingat();
            suara = agenda.getObjects().getSuara();
            getar = agenda.getObjects().getGetar();
            longitude = agenda.getObjects().getLongitude();
            latitude = agenda.getObjects().getLatitude();
            nama_pasien.setText(agenda.getObjects().getNama_pasien());
            setTanggal(mulai, selesai);

            tanggal.setText(tanggal2.getText().toString().substring(8, 10));
            bulan.setText(getBulan(tanggal2.getText().toString()));
            if (getDays() == 0) {
                keterangan.setText(getResources().getString(R.string.HariIni));
            } else if (getDays() == 1) {
                keterangan.setText(getResources().getString(R.string.Besok));
            } else if (getDays() < 0) {
                keterangan.setText(Math.abs(getDays()) + " " + getResources().getString(R.string.HariYgLalu));
            } else {
                keterangan.setText(Math.abs(getDays()) + " " + getResources().getString(R.string.Hari));
            }

            waktu2.setText(agenda.getObjects().getJam_mulai().substring(0, 5) + " - " + agenda.getObjects().getJam_akhir().substring(0, 5));

            if (pengingat.equals("1440")) {
                pengingat = "1440";
                txt_pengingat.setText(getResources().getString(R.string.Reminder1));
            }
            if (pengingat.equals("180")) {
                pengingat = "180";
                txt_pengingat.setText(getResources().getString(R.string.Reminder2));
            }
            if (pengingat.equals("120")) {
                pengingat = "120";
                txt_pengingat.setText(getResources().getString(R.string.Reminder3));
            }
            if (pengingat.equals("60")) {
                pengingat = "60";
                txt_pengingat.setText(getResources().getString(R.string.Reminder4));
            }
            if (pengingat.equals("30")) {
                pengingat = "30";
                txt_pengingat.setText(getResources().getString(R.string.Reminder5));
            }
            if (pengingat.equals("15")) {
                pengingat = "15";
                txt_pengingat.setText(getResources().getString(R.string.Reminder6));
            }
            if (pengingat.equals("5")) {
                pengingat = "5";
                txt_pengingat.setText(getResources().getString(R.string.Reminder7));
            }
        }
    }


    void deleteEvent(String id) {
        Agenda.Agendas agendas = db.getAgenda(id);
        long eventID = Long.valueOf(agendas.getObjects().getId_agenda());
        Uri deleteUri = null;
        deleteUri = ContentUris.withAppendedId(CalendarContract.Events.CONTENT_URI, eventID);
        int rows = getContentResolver().delete(deleteUri, null, null);
        Log.i("DELETEDDDD", "Rows deleted: " + rows);
        db.deleteAgenda(id);
    }
}