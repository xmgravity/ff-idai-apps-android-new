package com.prima.entities;

/**
 * Created by Codelabs on 19/10/2015.
 */
public class OrangTua extends Message {
    private ObjectOrtu data;

    public ObjectOrtu getObjects() {
        return data;
    }

    public void setObjects(ObjectOrtu data) {
        this.data = data;
    }

    public class ObjectOrtu {
        private String id;
        private String name;
        private String email;
        private String dob;
        private String gender;
        private String profile_picture;
        private String mobile_phone;
        private String address;

        private ObjectUserToken userToken;

        public ObjectUserToken getUserToken() {
            return userToken;
        }

        public void setUserToken(ObjectUserToken userToken) {
            this.userToken = userToken;
        }

        public class ObjectUserToken {
            private String token;

            public String getToken() {
                return token;
            }

            public void setToken(String token) {
                this.token = token;
            }

        }

        public String getGender() {
            return gender;
        }

        public void setGender(String gender) {
            this.gender = gender;
        }

        public String getDob() {
            return dob;
        }

        public void setDob(String dob) {
            this.dob = dob;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getNama() {
            return name;
        }

        public void setNama(String name) {this.name = name;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getHandphone() { return mobile_phone; }

        public void setHandphone(String handphone) { this.mobile_phone = handphone; }

        public String getAddress() { return address; }

        public void setAddress(String address) { this.address = address; }

        public String getFoto() {
            return profile_picture;
        }

        public void setFoto(String profile_picture) {
            this.profile_picture = profile_picture;
        }
    }

}
