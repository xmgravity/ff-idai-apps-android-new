package com.prima.entities;

import java.lang.reflect.Array;
import java.util.ArrayList;

/**
 * Created by Codelabs on 18/09/2015.
 */
public class AllRedflag extends Message {
    private ArrayList<AllRedflags> data;

    public ArrayList<AllRedflags> getObjects() {
        return data;
    }

    public void setObjects(ArrayList<AllRedflags> objects) {
        this.data = objects;
    }

    public class AllRedflags {
        private String id;
        private String questionnaire_age_id;
        private ArrayList<Question.Kuesioner> data;
        private ArrayList<Answer> options;
        private Age age;

        public ArrayList<Answer> getOptions() {
            return options;
        }

        public void setOptions(ArrayList<Answer> options) {
            this.options = options;
        }

        public ArrayList<Question.Kuesioner> getData() {
            return data;
        }

        public void setData(ArrayList<Question.Kuesioner> data) {
            this.data = data;
        }

        public Age getAge(){return age;}
        public void setAge(Age age){this.age = age;}

        public int getMin_age() {
            return age.getMinAge();
        }

        public void setMin_age(int min_age) {
            this.age.setMinAge(min_age);
        }

        public int getMax_age() {
            return age.getMaxAge();
        }

        public void setMax_age(int max_age) {
            this.age.setMaxAge(max_age);
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getId_age() {
            return questionnaire_age_id;
        }

        public void setId_age(String id_age) {
            this.questionnaire_age_id = id_age;
        }

        public String getAge_string() {
            return age.getRangeAge();
        }

        public void setAge_string(String age_string) {
            this.age.setRangeAge(age_string);
        }

    }

    public class Age{
        private String type;
        private int min_age;
        private int max_age;
        private String code;
        private String filename;
        private String range_age;
        private String name;

        public String getName() {
            return name;
        }
        public void setName(String name) {
            this.name = name;
        }

        public String getRangeAge() {
            return range_age;
        }
        public void setRangeAge(String range_age) {
            this.range_age = range_age;
        }

        public String getType() {
            return type;
        }
        public void setType(String type) {
            this.type = type;
        }

        public int getMinAge() {
            return min_age;
        }
        public void setMinAge(int min_age) {
            this.min_age = min_age;
        }

        public int getMaxAge() {
            return max_age;
        }
        public void setMaxAge(int max_age) {
            this.max_age = max_age;
        }

        public String getCode() {return code;}
        public void setCode(String code) {
            this.code = code;
        }

        public String getFilename() {return filename;}
        public void setFilename(String filename) {
            this.filename = filename;
        }

    }

    public class Answer {
        private String id;
        private String question_id;
        private String type;
        private String option;
        private String score;
        private String version;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getQuestionnaire_id() {
            return this.question_id;
        }

        public void setQuestionnaire_id(String questionnaire_id) {
            this.question_id = questionnaire_id;
        }

        public String getOption_type() {
            return type;
        }

        public void setOption_type(String option_type) {
            this.type = option_type;
        }

        public String getOption() {
            return option;
        }

        public void setOption(String option) {
            this.option = option;
        }

        public String getScore() {
            return score;
        }

        public void setScore(String score) {
            this.score = score;
        }

        public String getAnswer_version() {
            return version;
        }

        public void setAnswer_version(String answer_version) {
            this.version = answer_version;
        }
    }
}
