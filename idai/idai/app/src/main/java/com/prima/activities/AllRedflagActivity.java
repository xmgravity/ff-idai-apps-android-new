package com.prima.activities;

import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.prima.controllers.Parser;
import com.prima.entities.AllRedflag;
import prima.test.prima.R;

/**
 * Created by Codelabs on 18/09/2015.
 */
public class AllRedflagActivity extends BaseActivity implements View.OnClickListener {
    private ImageView close;
    private ImageView back;
    private LinearLayout menu;
    private Button lanjut;
    TextView title;
    AllRedflag allRedflag;

    @Override
    public void initView() {
        menu = (LinearLayout) findViewById(R.id.menu);
        back = (ImageView) findViewById(R.id.back);
        close = (ImageView) findViewById(R.id.close);
        lanjut = (Button) findViewById(R.id.lanjut);
        title = (TextView) findViewById(R.id.tv_actionbar_title);
        Typeface tf = Typeface.createFromAsset(getApplicationContext().getAssets(), "BREVIA-BLACK.OTF");
        title.setTypeface(tf);
        title.setText(getResources().getString(R.string.TahapandanTandaBahayaPerkembangan));

        Bundle bn = getIntent().getExtras();
        if (bn != null) {
            String respon = bn.getString("respon");
            allRedflag = Parser.getAllRedFlags(respon);
            for (int i = 0; i < allRedflag.getObjects().size(); i++) {

                View child1 = getLayoutInflater().inflate(R.layout.list_item_all_redflags, null);
                LinearLayout list1 = (LinearLayout) child1.findViewById(R.id.list);
                TextView txt = (TextView) child1.findViewById(R.id.redflag_age);
                txt.setId(Integer.valueOf(i));
                list1.setId(Integer.valueOf(i));
                txt.setText(getResources().getString(R.string.TandaBahayaPerkembanganUsia) + " " + allRedflag.getObjects().get(i).getAge().getRangeAge());

                View child2 = getLayoutInflater().inflate(R.layout.list_item_all_redfrag_inside, null);
                LinearLayout list2 = (LinearLayout) child2.findViewById(R.id.list2);
                TextView txt2 = (TextView) child2.findViewById(R.id.txt2);
                txt2.setId(Integer.valueOf(allRedflag.getObjects().get(i).getId()));
                list2.setId(Integer.valueOf(allRedflag.getObjects().get(i).getId()));
                txt2.setText(allRedflag.getObjects().get(i).getAge().getName());
                list1.addView(child2);


                for (int k = 0; k < allRedflag.getObjects().get(i).getOptions().size(); k++) {
                    View child3 = getLayoutInflater().inflate(R.layout.list_item_all_redflag_inside2, null);
                    TextView txt3 = (TextView) child3.findViewById(R.id.txt1);
                    txt3.setId(Integer.valueOf(allRedflag.getObjects().get(i).getOptions().get(k).getId()));
                    txt3.setText(allRedflag.getObjects().get(i).getOptions().get(k).getOption());

                    list2.addView(child3);
                }
                /*
                for (int j = 0; j < allRedflag.getObjects().get(i).getData().size(); j++) {
                    View child2 = getLayoutInflater().inflate(R.layout.list_item_all_redfrag_inside, null);
                    LinearLayout list2 = (LinearLayout) child2.findViewById(R.id.list2);
                    TextView txt2 = (TextView) child2.findViewById(R.id.txt2);
                    txt2.setId(Integer.valueOf(allRedflag.getObjects().get(i).getData().get(j).getId()));
                    list2.setId(Integer.valueOf(allRedflag.getObjects().get(i).getData().get(j).getId()));
                    txt2.setText(allRedflag.getObjects().get(i).getData().get(j).getTitle());


                    for (int k = 0; k < allRedflag.getObjects().get(i).getData().get(j).getAnswer_option().size(); k++) {
                        View child3 = getLayoutInflater().inflate(R.layout.list_item_all_redflag_inside2, null);
                        TextView txt3 = (TextView) child3.findViewById(R.id.txt1);
                        txt3.setId(Integer.valueOf(allRedflag.getObjects().get(i).getData().get(j).getAnswer_option().get(k).getId()));
                        txt3.setText(allRedflag.getObjects().get(i).getData().get(j).getAnswer_option().get(k).getOption());

                        list2.addView(child3);
                    }

                    list1.addView(child2);


                }*/
                menu.addView(child1);
            }
        }

    }

    @Override
    public void setUICallbacks() {
        back.setOnClickListener(this);
        close.setOnClickListener(this);
        lanjut.setOnClickListener(this);
    }

    @Override
    public int getLayout() {
        return R.layout.activity_milestone_all_redflags;
    }

    @Override
    public void updateUI() {

    }

    @Override
    public void onClick(View v) {
        if (v == back) {
            finish();
        } else if (v == close) {
            finish();
        } else if (v == lanjut) {
            finish();
        }
    }
}
