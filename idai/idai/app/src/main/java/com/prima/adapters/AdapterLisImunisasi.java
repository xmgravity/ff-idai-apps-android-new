package com.prima.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RelativeLayout;

import com.afollestad.materialdialogs.MaterialDialog;

import java.util.ArrayList;

import com.prima.entities.KalenderImunisasi;
import prima.test.prima.R;

/**
 * Created by Codelabs on 07/08/2015.
 */
public class AdapterLisImunisasi extends BaseAdapter {
    private ArrayList<String> _data;
    Context _context;
    KalenderImunisasi kalender;
    int month;

    public AdapterLisImunisasi(ArrayList<String> data, Context context, KalenderImunisasi kalender, int month) {
        _data = data;
        _context = context;
        this.kalender = kalender;
        this.month = month;
    }

    @Override
    public int getCount() {

        return _data.size();
    }

    @Override
    public Object getItem(int position) {

        return _data.get(position);
    }

    @Override
    public long getItemId(int position) {

        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View v = convertView;
        if (v == null) {
            LayoutInflater infLayout = (LayoutInflater) _context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = infLayout.inflate(R.layout.list_item_kalender_imunisasi, null);
        }

        ArrayList<RelativeLayout> rels = new ArrayList<RelativeLayout>();

        RelativeLayout hepatitisb = (RelativeLayout) v.findViewById(R.id.hepatitisb);
        RelativeLayout polio = (RelativeLayout) v.findViewById(R.id.polio);
        RelativeLayout bcg = (RelativeLayout) v.findViewById(R.id.bcg);
        RelativeLayout dtp = (RelativeLayout) v.findViewById(R.id.dtp);
        RelativeLayout hib = (RelativeLayout) v.findViewById(R.id.hib);
        RelativeLayout pcv = (RelativeLayout) v.findViewById(R.id.pcv);
        RelativeLayout rotavirus = (RelativeLayout) v.findViewById(R.id.rotavirus);
        RelativeLayout influenza = (RelativeLayout) v.findViewById(R.id.influenza);
        RelativeLayout campak = (RelativeLayout) v.findViewById(R.id.campak);
        RelativeLayout mmr = (RelativeLayout) v.findViewById(R.id.mmr);
        RelativeLayout tifoid = (RelativeLayout) v.findViewById(R.id.tifoid);
        RelativeLayout hepatitisa = (RelativeLayout) v.findViewById(R.id.hepatitisa);
        RelativeLayout varisela = (RelativeLayout) v.findViewById(R.id.varisela);
        RelativeLayout hpv = (RelativeLayout) v.findViewById(R.id.hpv);

        rels.add(hepatitisb);
        rels.add(polio);
        rels.add(bcg);
        rels.add(dtp);
        rels.add(hib);
        rels.add(pcv);
        rels.add(rotavirus);
        rels.add(influenza);
        rels.add(campak);
        rels.add(mmr);
        rels.add(tifoid);
        rels.add(hepatitisa);
        rels.add(varisela);
        rels.add(hpv);

        for (int i = 0; i < kalender.getObjects().size(); i++) {
            for (int j = 0; j < kalender.getObjects().get(i).getObject().size(); j++) {
                //for
                //if (kalender.getObjects().get(i).getObject().get(j).getId_vaksin().equals(String.valueOf(i+1))) {
                if (Integer.valueOf(_data.get(position)) >= Integer.valueOf(kalender.getObjects().get(i).getObject().get(j).getMin_age())
                        && Integer.valueOf(_data.get(position)) <= Integer.valueOf(kalender.getObjects().get(i).getObject().get(j).getMax_age())) {
                    if (kalender.getObjects().get(i).getTahapan().equalsIgnoreCase("Normal")) {
                        rels.get(Integer.valueOf(kalender.getObjects().get(i).getObject().get(j).getId_vaksin()) - 1).setBackgroundResource(R.drawable.table_border_blue_imunisasi);
                        final int finalI = i;
                        final int finalJ = j;
                        rels.get(Integer.valueOf(kalender.getObjects().get(i).getObject().get(j).getId_vaksin()) - 1).setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                if (!kalender.getObjects().get(finalI).getObject().get(finalJ).getCatatan().equals("")) {
                                    // Toast.makeText(_context, kalender.getObjects().get(finalI).getObject().get(finalJ).getNote(), Toast.LENGTH_LONG).show();
                                    dialog2(kalender.getObjects().get(finalI).getObject().get(finalJ).getCatatan(), "Info");
                                }
                            }
                        });

                        if (Integer.valueOf(_data.get(position)) <= month) {
                            rels.get(Integer.valueOf(kalender.getObjects().get(i).getObject().get(j).getId_vaksin()) - 1).setBackgroundResource(R.drawable.table_green_black_border);
                        }

                    } else if (kalender.getObjects().get(i).getTahapan().equalsIgnoreCase("Catch Up")) {
                        rels.get(Integer.valueOf(kalender.getObjects().get(i).getObject().get(j).getId_vaksin()) - 1).setBackgroundResource(R.drawable.table_red_black_border);
                        final int finalI = i;
                        final int finalJ = j;
                        rels.get(Integer.valueOf(kalender.getObjects().get(i).getObject().get(j).getId_vaksin()) - 1).setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                if (!kalender.getObjects().get(finalI).getObject().get(finalJ).getCatatan().equals("")) {
                                    //Toast.makeText(_context, kalender.getObjects().get(finalI).getObject().get(finalJ).getNote(), Toast.LENGTH_LONG).show();
                                    dialog2(kalender.getObjects().get(finalI).getObject().get(finalJ).getCatatan(), "Info");
                                }
                            }
                        });
                    }
                }
                //}
                //for
            }
        }


        return v;
    }


    public void dialog2(String content, String msg) {
        new MaterialDialog.Builder(_context)
                .title(msg)
                .content(content)
                .negativeText("OK")
                .callback(new MaterialDialog.ButtonCallback() {
                    @Override
                    public void onPositive(MaterialDialog dialog) {

                    }

                    @Override
                    public void onNegative(MaterialDialog dialog) {
                        super.onNegative(dialog);
                    }

                })
                .show();
    }
}
