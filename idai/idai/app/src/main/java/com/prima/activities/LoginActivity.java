package com.prima.activities;

import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import prima.test.prima.R;

/**
 * Created by Codelabs on 29/08/2015.
 */
public class LoginActivity extends BaseActivity implements View.OnClickListener {

    private TextView forgot, peran;
    private RelativeLayout login;
    TextView title, registered, unregistered,txt;
    ImageView back;

    @Override
    public void initView() {
        back = (ImageView) findViewById(R.id.back);
        title = (TextView) findViewById(R.id.title);
        registered = (TextView) findViewById(R.id.registered);
        unregistered = (TextView) findViewById(R.id.unregistered);
        txt = (TextView) findViewById(R.id.txt);
        peran = (TextView) findViewById(R.id.peran);
        Typeface tf = Typeface.createFromAsset(getApplicationContext().getAssets(), "BREVIA-BLACK.OTF");
        Typeface tfItalic = Typeface.createFromAsset(getApplicationContext().getAssets(), "Brevia-ExtraBlackItalic.otf");
        txt.setTypeface(tf);
        title.setTypeface(tf);
        peran.setTypeface(tf);
        registered.setTypeface(tf);
        unregistered.setTypeface(tf);
        forgot = (TextView) findViewById(R.id.forgot);
        login = (RelativeLayout) findViewById(R.id.login);
        forgot.setTypeface(tfItalic);
        back.setVisibility(View.GONE);
    }

    @Override
    public void setUICallbacks() {
        forgot.setOnClickListener(this);
        login.setOnClickListener(this);
        back.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        if (v == forgot) {
//            Intent i = new Intent(LoginActivity.this, ForgotPasswordActivity.class);
//            startActivity(i);
//            finish();
            String url = "http://prima.or.id/user/lupa-password";
            Intent i = new Intent(Intent.ACTION_VIEW);
            i.setData(Uri.parse(url));
            startActivity(i);
        } else if (v == login) {
            Intent i = new Intent(LoginActivity.this, SplashPernyataanActivity.class);
            startActivity(i);
            finish();
        } else if (v==back){
            finish();
        }

    }


    @Override
    public int getLayout() {
        return R.layout.activity_login;
    }

    @Override
    public void updateUI() {

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

    }


    @Override
    public void onBackPressed() {
        finish();
    }
}