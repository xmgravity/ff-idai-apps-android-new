package com.prima.entities;

import java.util.ArrayList;

/**
 * Created by Codelabs on 30/08/2015.
 */
public class Pubertas extends Message {
    private ArrayList<PubertasMaterei> data;

    public class PubertasMaterei {
        private String gender;
        private ArrayList<Item> data;

        public String getType() {
            return gender;
        }

        public void setType(String type) {
            type = type;
        }

        public ArrayList<Item> getObject() {
            return data;
        }

        public void setObject(ArrayList<Item> object) {
            this.data = object;
        }
    }

    public class Item {
        private String image_code;
        private String header;
        private String title;
        private String phase;
        private String description;
        private String image;
        private String version;


        public String getCode_image() {
            return image_code;
        }

        public void setCode_image(String code_image) {
            this.image_code = code_image;
        }

        public String getHeader() {
            return header;
        }

        public void setHeader(String header) {
            this.header = header;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getPhase() {
            return phase;
        }

        public void setPhase(String phase) {
            this.phase = phase;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public String getVersion() {
            return version;
        }

        public void setVersion(String version) {
            this.version = version;
        }
    }

    public ArrayList<PubertasMaterei> getObjects() {
        return data;
    }

    public void setObjects(ArrayList<PubertasMaterei> objects) {
        this.data = objects;
    }

}
