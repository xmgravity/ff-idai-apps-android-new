package com.prima.activities;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.JsonSyntaxException;
import com.prima.MainActivity;
import com.prima.Preferences;
import com.prima.controllers.ApiReferences;
import com.prima.controllers.Parser;
import com.prima.entities.Grafik;
import com.prima.entities.Message;
import com.prima.entities.Pasien;
import com.prima.helpers.DateHelper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import prima.test.prima.R;

/**
 * Created by Codelabs on 05/08/2015.
 */
public class GrafikPertumbuhanActivity extends BaseActivity implements View.OnClickListener {

    private Button source;
    private TextView txtSource, content;
    private LinearLayout box;
    private ImageView close;
    private RequestQueue mRequest;
    private Button lanjut;
    private Button dialogLanjut;
    List<CheckBox> checkBoxs;
    ArrayList<String> selected;
    ArrayList<String> grafikName;
    ArrayList<String> interpretasi;
    ArrayList<String> label;
    private TextView title;
    String id;
    static MaterialDialog dialog;
    Dialog dialog2;
    String result;
    RelativeLayout source_wraper;
    DateHelper dateHelper;

    @Override
    public void initView() {
        dateHelper = new DateHelper();
        source = (Button) findViewById(R.id.btn_source);
        txtSource = (TextView) findViewById(R.id.source);
        box = (LinearLayout) findViewById(R.id.box);
        close = (ImageView) findViewById(R.id.close);
        lanjut = (Button) findViewById(R.id.lanjut);
        txtSource.setVisibility(View.GONE);
        title = (TextView) findViewById(R.id.title);
        content = (TextView) findViewById(R.id.content);
        source_wraper = (RelativeLayout) findViewById(R.id.source_wraper);

        //set font
        Typeface tf = Typeface.createFromAsset(getApplicationContext().getAssets(), "Brevia-ExtraBlackItalic.otf");
        title.setTypeface(tf);
        content.setTypeface(tf);
        mRequest = Volley.newRequestQueue(getApplicationContext());
        id = preferences.getString(Preferences.ID, "");

    }

    @Override
    public void setUICallbacks() {
        source.setOnClickListener(this);
        lanjut.setOnClickListener(this);
        close.setOnClickListener(this);

    }


    @Override
    public void onClick(View v) {
        if (v == source) {
            if (txtSource.getVisibility() == View.GONE) {
                txtSource.setVisibility(View.VISIBLE);
            } else {
                txtSource.setVisibility(View.GONE);
            }
        } else if (v == lanjut) {
            if (!id.equalsIgnoreCase("")) {
                showDialog();
            } else {
                // get prompts.xml view
                LayoutInflater li = LayoutInflater.from(getApplicationContext());
                View dialogView = li.inflate(R.layout.dialog_offline_kalkulator, null);
                final Dialog dialog3;
                dialog3 = new Dialog(context);
                dialog3.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog3.setContentView(dialogView);
                //make transparent dialog_sesi_offline_pelod
                dialog3.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                dialog3.show();


                Button dialogLanjut = (Button) dialogView.findViewById(R.id.ok);
                final RadioButton rad1 = (RadioButton) dialogView.findViewById(R.id.rad1);
                final RadioButton rad2 = (RadioButton) dialogView.findViewById(R.id.rad2);
                rad1.setChecked(true);

                rad1.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        rad1.setChecked(true);
                        rad2.setChecked(false);
                    }
                });

                rad2.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        rad2.setChecked(true);
                        rad1.setChecked(false);
                    }
                });

                final EditText edt = (EditText) dialogView.findViewById(R.id.edt1);
                final EditText edt2 = (EditText) dialogView.findViewById(R.id.edt2);
                final EditText edt3 = (EditText) dialogView.findViewById(R.id.edt3);


                edt.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        showDatepickerDialog(edt);
                    }
                });
                dialogLanjut.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (edt.getText().toString().equals("") ||
                                edt2.getText().toString().equals("") ||
                                edt3.getText().toString().equals("")) {
                            dialog(getResources().getString(R.string.IsiSeluruhIsian), getResources().getString(R.string.Perhatian));
                        } else {
                            String jk;
                            if (rad1.isChecked()) {
                                jk = "1";
                            } else if (rad2.isChecked()) {
                                jk = "0";
                            } else {
                                jk = "";
                            }
                            saveSesiPasien(jk, edt.getText().toString(), edt3.getText().toString(), edt2.getText().toString());
                            dialog3.dismiss();
                        }
                    }
                });
            }

        } else if (v == close) {
            Intent i = new Intent(GrafikPertumbuhanActivity.this, MainActivity.class);
            startActivity(i);
            finish();
        }
    }


    void showDialog() {
        // get prompts.xml view
        LayoutInflater li = LayoutInflater.from(getApplicationContext());
        View dialogView = li.inflate(R.layout.dialog_grafik_pertumbuhan, null);

        dialog2 = new Dialog(context);
        dialog2.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog2.setContentView(dialogView);
        //make transparent dialog_sesi_offline_pelod
        dialog2.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog2.show();


        checkBoxs = new ArrayList<CheckBox>();
        selected = new ArrayList<String>();
        grafikName = new ArrayList<String>();
        interpretasi = new ArrayList<String>();
        label = new ArrayList<String>();

        checkBoxs.add((CheckBox) dialogView.findViewById(R.id.panjang_thd_umur));
        checkBoxs.add((CheckBox) dialogView.findViewById(R.id.berat_thd_umur));
        checkBoxs.add((CheckBox) dialogView.findViewById(R.id.berat_thd_panjang));
        checkBoxs.add((CheckBox) dialogView.findViewById(R.id.indeks_masa_tubuh));
        checkBoxs.add((CheckBox) dialogView.findViewById(R.id.lingkar_kepala));

        dialogLanjut = (Button) dialogView.findViewById(R.id.lanjut);
        dialogLanjut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (checkBoxs.get(4).isChecked()) {
                    /*
                    // get prompts.xml view
                    LayoutInflater li = LayoutInflater.from(getApplicationContext());
                    View dialogView = li.inflate(R.layout.dialog_lingkar_kepala, null);
                    final Dialog dialog3;
                    dialog3 = new Dialog(context);
                    dialog3.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    dialog3.setContentView(dialogView);
                    //make transparent dialog_sesi_offline_pelod
                    dialog3.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                    dialog3.show();
                    Button dialogLanjut = (Button) dialogView.findViewById(R.id.lanjut);
                    final EditText edt = (EditText) dialogView.findViewById(R.id.edt1);
                    dialogLanjut.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (edt.getText().toString().trim().equals("")) {
                                dialog("Harap isi data masukan lingkar kepala", "Tidak dapat dilanjutkan");
                            } else {
                                dialog3.dismiss();
                                editor.putString(Preferences.LINGKAR_KEPALA, edt.getText().toString());
                                editor.commit();
                                boolean ketemu = false;
                                for (int i = 0; i < checkBoxs.size(); i++) {
                                    if (checkBoxs.get(i).isChecked()) {
                                        ketemu = true;
                                    }
                                }
                                if (ketemu) {
                                    pjgTinggiThdUmur();
                                } else {
                                    Toast.makeText(getApplicationContext(), "Pilih Grafik Untuk Ditampilkan", Toast.LENGTH_LONG).show();
                                }
                            }
                        }
                    });
*/
                } else {
                    boolean ketemu = false;
                    for (int i = 0; i < checkBoxs.size(); i++) {
                        if (checkBoxs.get(i).isChecked()) {
                            ketemu = true;
                        }
                    }
                    if (ketemu) {
                        pjgTinggiThdUmur();
                    } else {
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.PilihGrafik), Toast.LENGTH_LONG).show();
                    }
                }
            }
        });
    }


    @Override
    public int getLayout() {
        return R.layout.activity_grafik_pertumbuhan;
    }

    @Override
    public void updateUI() {
//        checkBoxs = new ArrayList<CheckBox>();
//        selected = new ArrayList<String>();
//        grafikName = new ArrayList<String>();
//        interpretasi = new ArrayList<String>();
//        Bundle bn = getIntent().getExtras();
//        if (bn != null) {
//            result = bn.getString("result");
//            Artikel artikel = Parser.getArtikel(result);
//            content.setText(artikel.getObjects().getDeskripsi());
//            Spannable s = (Spannable) Html.fromHtml("<a href=\"" + artikel.getObjects().getSumber() + "\">" + artikel.getObjects().getSumber() + "</a>");
//            URLSpan[] spans = s.getSpans(0, s.length(), URLSpan.class);
//            for (URLSpan span : spans) {
//                int start = s.getSpanStart(span);
//                int end = s.getSpanEnd(span);
//                s.removeSpan(span);
//                span = new URLSpan(span.getURL());
//                s.setSpan(span, start, end, 0);
//            }
//
//            txtSource.setText(s);
//            txtSource.setMovementMethod(LinkMovementMethod.getInstance());
//
//            if (artikel.getObjects().getSumber().trim().equals("")) {
//                txtSource.setVisibility(View.GONE);
//                source_wraper.setVisibility(View.GONE);
//                source.setVisibility(View.GONE);
//
//            }
//
//        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

    }


    @Override
    public void onBackPressed() {
        Intent i = new Intent(GrafikPertumbuhanActivity.this, MainActivity.class);
        startActivity(i);
        finish();
    }

    private void pjgTinggiThdUmur() {
        progresDialog(false, getResources().getString(R.string.MemuatGrafik));
        if (checkBoxs.get(0).isChecked()) {
            StringRequest listdata = new StringRequest(Request.Method.GET,
                    ApiReferences.generatePanjangTUmur(id),
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            Log.i("RESULT JSON", response.toString());

                            if (response.equals("[]")) {
                                dialog(getResources().getString(R.string.GrafikBelumTersedia), getResources().getString(R.string.Perhatian));
                            } else {
                                Message msg = Parser.getMessage(response);
                                if (msg.getStatus().equals("0")) {
                                    dialog(msg.getMessages(), getResources().getString(R.string.Perhatian));
                                } else {
                                    Grafik graf = new Grafik();
                                    graf = Parser.getGrafik(response
                                            .toString());
                                    if (graf.getStatus().equals("1")) {

                                        selected.add(graf.getObjects().getImage_url());
                                        interpretasi.add(graf.getObjects().getInterpretation());
                                        grafikName.add(getResources().getString(R.string.grafikHeightAge));
                                        label.add(graf.getObjects().getLabel());
                                        beratThdUmur();
                                    }
                                }
                            }

                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    dialog.dismiss();
                    dialog(getResources().getString(R.string.NoConnection), getResources().getString(R.string.Perhatian));

                }
            });
            listdata.setRetryPolicy(new DefaultRetryPolicy(
                    20000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            mRequest.add(listdata);
        } else {
            beratThdUmur();
        }
    }

    private void beratThdUmur() {
        if (checkBoxs.get(1).isChecked()) {
            //  progresDialog(false, "Memuat Grafik");
            StringRequest listdata = new StringRequest(Request.Method.GET,
                    ApiReferences.generateBeratTUmur(id),
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {

                            if (response.equals("[]")) {
                                dialog(getResources().getString(R.string.GrafikBelumTersedia), getResources().getString(R.string.Perhatian));
                            } else {
                                Message msg = Parser.getMessage(response);
                                if (msg.getStatus().equals("0")) {
                                    dialog(msg.getMessages(), getResources().getString(R.string.Perhatian));
                                } else {
                                    Log.i("RESULT JSON", response.toString());
                                    Grafik graf = new Grafik();
                                    graf = Parser.getGrafik(response
                                            .toString());
                                    if (graf.getStatus().equals("1")) {
                                        selected.add(graf.getObjects().getImage_url());
                                        interpretasi.add(graf.getObjects().getInterpretation());
                                        grafikName.add(getResources().getString(R.string.grafikHeightAge));
                                        label.add(graf.getObjects().getLabel());
                                        BeratThdPjgTinggi();
                                    }
                                }
                            }

                        }
                        //   dialog_sesi_offline_pelod.dismiss();

                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    dialog.dismiss();
                    dialog(getResources().getString(R.string.NoConnection), getResources().getString(R.string.Perhatian));
                }
            });
            listdata.setRetryPolicy(new DefaultRetryPolicy(
                    20000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            mRequest.add(listdata);
        } else {
            BeratThdPjgTinggi();
        }
    }

    private void BeratThdPjgTinggi() {
        if (checkBoxs.get(2).isChecked()) {
            //   progresDialog(false, "Memuat Grafik");
            StringRequest listdata = new StringRequest(Request.Method.GET,
                    ApiReferences.generateBeratTPanjangTinggi(id, "", ""),
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {

                            if (response.equals("[]")) {
                                dialog(getResources().getString(R.string.GrafikBelumTersedia), getResources().getString(R.string.Perhatian));
                            } else {
                                Message msg = Parser.getMessage(response);
                                if (msg.getStatus().equals("0")) {
                                    dialog(msg.getMessages(), getResources().getString(R.string.Perhatian));
                                } else {
                                    Log.i("RESULT JSON", response.toString());
                                    Grafik graf = new Grafik();
                                    graf = Parser.getGrafik(response
                                            .toString());
                                    if (graf.getStatus().equals("1")) {

                                        selected.add(graf.getObjects().getImage_url());
                                        interpretasi.add(graf.getObjects().getInterpretation());
                                        grafikName.add(getResources().getString(R.string.grafikWeightHeight));
                                        label.add(graf.getObjects().getLabel());
                                        imt();
                                    }
                                }
                            }

                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    dialog.dismiss();
                    dialog(getResources().getString(R.string.NoConnection), getResources().getString(R.string.Perhatian));
                }
            });
            listdata.setRetryPolicy(new DefaultRetryPolicy(
                    20000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            mRequest.add(listdata);
        } else {
            imt();
        }
    }

    private void imt() {
        if (checkBoxs.get(3).isChecked()) {
            // progresDialog(false, "Memuat Grafik");
            StringRequest listdata = new StringRequest(Request.Method.GET,
                    ApiReferences.generateIMTTUmur(id),
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {

                            if (response.equals("[]")) {
                                dialog(getResources().getString(R.string.GrafikBelumTersedia), getResources().getString(R.string.Perhatian));
                            } else {
                                Message msg = Parser.getMessage(response);
                                if (msg.getStatus().equals("0")) {
                                    dialog(msg.getMessages(), getResources().getString(R.string.Perhatian));
                                } else {
                                    Log.i("RESULT JSON", response.toString());
                                    Grafik graf = new Grafik();
                                    graf = Parser.getGrafik(response
                                            .toString());
                                    if (graf.getStatus().equals("1")) {
                                        selected.add(graf.getObjects().getImage_url());
                                        interpretasi.add(graf.getObjects().getInterpretation());
                                        grafikName.add(getResources().getString(R.string.grafikBodyMassIndex));
                                        label.add(graf.getObjects().getLabel());
                                        lingkarKepala();
                                    }
                                }
                            }
                        }

                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    dialog.dismiss();
                    dialog(getResources().getString(R.string.NoConnection), getResources().getString(R.string.Perhatian));
                }
            });
            listdata.setRetryPolicy(new DefaultRetryPolicy(
                    20000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            mRequest.add(listdata);
        } else {
            lingkarKepala();
        }
    }

    private void lingkarKepala() {
        System.out.println("masuk lingkar kepala");
        //  progresDialog(false, "Memuat Grafik");
        String lingkarKepala = preferences.getString(Preferences.LINGKAR_KEPALA, "");
        if (checkBoxs.get(4).isChecked()) {
            StringRequest listdata = new StringRequest(Request.Method.GET,
                    ApiReferences.generateLingkarKepalaTUmur(id, lingkarKepala),
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            if (response.equals("[]")) {
                                dialog(getResources().getString(R.string.GrafikBelumTersedia), getResources().getString(R.string.Perhatian));
                            } else {
                                Message msg = Parser.getMessage(response);
                                if (msg.getStatus().equals("0")) {
                                    dialog(msg.getMessages(), getResources().getString(R.string.Perhatian));
                                } else {
                                    Log.i("RESULT JSON", response.toString());
                                    Grafik graf = new Grafik();
                                    graf = Parser.getGrafik(response
                                            .toString());
                                    if (graf.getStatus().equals("1")) {
                                        selected.add(graf.getObjects().getImage_url());
                                        interpretasi.add(graf.getObjects().getInterpretation());
                                        label.add(graf.getObjects().getLabel());
                                        grafikName.add(getResources().getString(R.string.grafikLingkarKepalaUmur));

                                    }
                                    dialog.dismiss();
                                    dialog2.dismiss();
                                    Intent i = new Intent(GrafikPertumbuhanActivity.this, GrafikPertumbuhanResultActivity.class);
                                    i.putStringArrayListExtra("page", selected);
                                    i.putStringArrayListExtra("title", grafikName);
                                    i.putStringArrayListExtra("interpretasi", interpretasi);
                                    i.putStringArrayListExtra("label", label);
                                    i.putExtra("result", result);
                                    startActivity(i);
                                    //    finish();
                                }
                            }
                            dialog.dismiss();
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    dialog.dismiss();
                    dialog(getResources().getString(R.string.NoConnection), getResources().getString(R.string.Perhatian));
                }
            });
            listdata.setRetryPolicy(new DefaultRetryPolicy(
                    20000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            mRequest.add(listdata);
        } else {
            dialog.dismiss();
            dialog2.dismiss();
            Intent i = new Intent(GrafikPertumbuhanActivity.this, GrafikPertumbuhanResultActivity.class);
            i.putStringArrayListExtra("page", selected);
            i.putStringArrayListExtra("title", grafikName);
            i.putStringArrayListExtra("interpretasi", interpretasi);
            i.putStringArrayListExtra("label", label);
            i.putExtra("result", result);
            startActivity(i);
            //  finish();
        }
    }


    private void saveSesiPasien(final String jenis_kelamin, final String tanggal_lahir, final String berat, final String tinggi) {
        progresDialog(false, getResources().getString(R.string.MenyimpanDataPasien));
        StringRequest postlogin = new StringRequest(Request.Method.POST,
                ApiReferences.insertPasien(), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                dialog.dismiss();
                try {
                    Log.i("RESULT JSON", response.toString());
                    Pasien pasien = Parser.getPasien(response
                            .toString());
                    if (pasien.getStatus().equalsIgnoreCase("1")) {
                        editor.putString(Preferences.ID, pasien.getObjects().getId());
                        editor.putString(Preferences.BERAT, pasien.getObjects().getBerat());
                        editor.putString(Preferences.TINGGI, pasien.getObjects().getTinggi());
                        editor.putString(Preferences.JENIS_KELAMIN, pasien.getObjects().getJenis_kelamin());
                        editor.putString(Preferences.BIRTH_DAY, pasien.getObjects().getTanggal_lahir());
                        editor.commit();

                        id = preferences.getString(Preferences.ID, "");
                        dialogEmail("Kode : " + pasien.getObjects().getId() + "\nSimpan Code ini untuk pemeriksaan berikutnya.", "Pendaftaran Sesi Pasien Berhasil", pasien.getObjects().getId());
                    }
                } catch (JsonSyntaxException e) {
                    dialog(getResources().getString(R.string.KesalahanServer), getResources().getString(R.string.Perhatian));
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                dialog.dismiss();
                dialog(getResources().getString(R.string.NoConnection), getResources().getString(R.string.Perhatian));
            }
        }) {

            protected Map<String, String> getParams() {
                HashMap<String, String> params = new HashMap<String, String>();
                params.put("jenis_kelamin", jenis_kelamin);
                params.put("tanggal_lahir", tanggal_lahir);
                params.put("berat", berat);
                params.put("tinggi", tinggi);
                return params;
            }
        };
        postlogin.setRetryPolicy(new DefaultRetryPolicy(
                20000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        mRequest.add(postlogin);
    }


    public void dialogEmail(String content, String msg, final String kode) {
        new MaterialDialog.Builder(this)
                .title(msg)
                .content(content)
                .autoDismiss(false)
                .cancelable(false)
                .positiveText(getResources().getString(R.string.KirimEmail))
                .negativeText(getResources().getString(R.string.tutup))
                .callback(new MaterialDialog.ButtonCallback() {
                    @Override
                    public void onPositive(MaterialDialog dialog) {
                        Intent sendEmail = new Intent(Intent.ACTION_SEND);
                        sendEmail.setType("text/plain");
                        sendEmail.putExtra(Intent.EXTRA_EMAIL, new String[]{""});
                        sendEmail.putExtra(Intent.EXTRA_SUBJECT, "Kode Pasien");
                        sendEmail.putExtra(Intent.EXTRA_TEXT, "Berikut ini adalah kode pasien anda.\nKode : " + kode);
                        startActivity(Intent.createChooser(sendEmail, "Email With"));
                    }

                    @Override
                    public void onNegative(MaterialDialog dialog) {
                        dialog.dismiss();
                        showDialog();
                    }
                })
                .show();
    }

    private void showDatepickerDialog(final EditText txt) {
        dateHelper.showDatePicker(this, System.currentTimeMillis(), new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                int _year = year;
                int _month = monthOfYear + 1;
                int _day = dayOfMonth;
                String date = _year + "-" + _month + "-" + _day;
                String birthDate = dateHelper.formatDate(date);
                txt.setText(birthDate);
            }
        });
    }

    private void progresDialog(boolean horizontal, String msg) {
        dialog = new MaterialDialog.Builder(GrafikPertumbuhanActivity.this)
                .title(msg)
                .content(getResources().getString(R.string.MohonTunggu))
                .progress(true, 0)
                .progressIndeterminateStyle(horizontal)
                .cancelable(false)
                .show();
    }

    @Override
    protected void onDestroy() {
        try {
            if (dialog.isShowing()) {
                dialog.cancel();
            }
        } catch (NullPointerException e) {

        }
        super.onDestroy();
    }


    public void dialog(String content, String msg) {
        new MaterialDialog.Builder(GrafikPertumbuhanActivity.this)
                .title(msg)
                .content(content)
                .positiveText("Ok")
                .callback(new MaterialDialog.ButtonCallback() {
                    @Override
                    public void onPositive(MaterialDialog dialog) {
                    }

                })
                .show();
    }

}
