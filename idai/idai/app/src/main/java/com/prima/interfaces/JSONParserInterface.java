/**
 * @author Codelabs
 */
package com.prima.interfaces;

import org.json.JSONException;

public interface JSONParserInterface {
	public boolean parse(String json) throws JSONException;
}
