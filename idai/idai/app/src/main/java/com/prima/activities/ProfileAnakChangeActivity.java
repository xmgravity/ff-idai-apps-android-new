package com.prima.activities;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.JsonSyntaxException;
import com.prima.Preferences;
import com.prima.controllers.ApiReferences;
import com.prima.controllers.Parser;
import com.prima.entities.Message;
import com.prima.helpers.DateHelper;
import com.prima.helpers.HexagonMaskView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

import prima.test.prima.R;

/**
 * Created by Codelabs on 19/08/2015.
 */
public class ProfileAnakChangeActivity extends BaseActivity implements View.OnClickListener {

    TextView title, sub, dob_value;
    ImageView close, back, profile_image;
    LinearLayout ubah_profile_anak;
    MaterialDialog dialog;
    Spinner gender_value;
    EditText fullname_value, tinggi_value, berat_value;
    DateHelper dateHelper;
    HexagonMaskView hexa;
    Button simpan;
    private RequestQueue mRequest;
    String current_id;

    @Override
    public void initView() {
        dateHelper = new DateHelper();
        mRequest = Volley.newRequestQueue(getApplicationContext());

        ubah_profile_anak = (LinearLayout) findViewById(R.id.ubah_profile_anak);
        simpan = (Button) findViewById(R.id.simpan);
        dob_value = (TextView) findViewById(R.id.profil_anak_dob_value);
        gender_value = (Spinner) findViewById(R.id.profil_anak_gender_value);
        fullname_value = (EditText) findViewById(R.id.profil_anak_fullname_value);
        tinggi_value = (EditText) findViewById(R.id.profil_anak_tinggi_value);
        berat_value = (EditText) findViewById(R.id.profil_anak_berat_value);
        title = (TextView) findViewById(R.id.tv_actionbar_title);
        sub = (TextView) findViewById(R.id.sub);
        close = (ImageView) findViewById(R.id.close);
        back = (ImageView) findViewById(R.id.back);
        close.setVisibility(View.GONE);
        //set font
        Typeface tf = Typeface.createFromAsset(getApplicationContext().getAssets(), "BREVIA-BLACK.OTF");
        title.setTypeface(tf);
        title.setText("PROFIL");

        current_id = preferences.getString(Preferences.ID, "");
        if (!preferences.getString(Preferences.CHILD_NAME, "").equals("")){
            fullname_value.setText(preferences.getString(Preferences.CHILD_NAME, ""));
        }
        if (!preferences.getString(Preferences.DOB, "").equals("")) {
            dob_value.setText(preferences.getString(Preferences.DOB, ""));
        }
        if (!preferences.getString(Preferences.GENDER, "").equals("")) {
            if(preferences.getString(Preferences.GENDER, "").equalsIgnoreCase("m")){
                gender_value.setSelection(0);
            }else{
                gender_value.setSelection(1);
            }

        }


    }

    @Override
    public void setUICallbacks() {
        back.setOnClickListener(this);
        dob_value.setOnClickListener(this);
        simpan.setOnClickListener(this);

    }


    @Override
    public void onClick(View v) {
        if (v == back) {
            Intent i = new Intent(getApplicationContext(), ProfileActivity.class);
            startActivity(i);
            finish();
        } else if (v == dob_value) {
            showDatePickerForBirthDay(dob_value);
        } else if (v == simpan) {
                updateProfile();
            }

        }



    private void updateProfile() {



        final String genderToDB;
        if(gender_value.getSelectedItem().toString().equalsIgnoreCase("Laki-laki")){
            genderToDB = "m";
        }else{
            genderToDB = "f";
        }

        System.out.println("fullname :"+fullname_value.getText().toString()+", height :"+""+", weight :"+""+", dob :"+dob_value.getText().toString()+", gender :"+genderToDB+"");



        String child_name = fullname_value.getText().toString();
        Log.d("dbgbg", "id :"+current_id+"fullname :"+child_name+", height :"+""+", weight :"+""+", dob :"+dob_value.getText().toString()+", gender :"+genderToDB+"");

        StringRequest postlogin = new StringRequest(Request.Method.POST,
                ApiReferences.updateProfileAnak(current_id), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    Log.i("RESULT JSON", response.toString());
                    Message message = Parser.getMessage(response
                            .toString());
                    if (message.getStatus().equalsIgnoreCase("1")) {
                        dialog(getResources().getString(R.string.DataBerhasilDiubah), getResources().getString(R.string.Perhatian));

                        editor.putString(Preferences.ID, fullname_value.getText().toString());
                        editor.putString(Preferences.DOB, dob_value.getText().toString());
                        editor.putString(Preferences.GENDER, genderToDB);
                        editor.commit();

                        Intent i = new Intent(getApplicationContext(), ProfileActivity.class);
                        startActivity(i);
                        finish();
                    }
                } catch (JsonSyntaxException e) {
                    dialog(getResources().getString(R.string.KesalahanServer), getResources().getString(R.string.Perhatian));
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                NetworkResponse response = error.networkResponse;
                if (error instanceof ServerError && response != null) {
                    try {
                        String res = new String(response.data,
                                HttpHeaderParser.parseCharset(response.headers, "utf-8"));
                        // Now you can use any deserializer to make sense of data
                        JSONObject obj = new JSONObject(res);
                        dialog(res.toString(), getResources().getString(R.string.Perhatian));
                    } catch (UnsupportedEncodingException e1) {
                        // Couldn't properly decode data to string
                        e1.printStackTrace();
                    } catch (JSONException e2) {
                        // returned data is not JSONObject?
                        e2.printStackTrace();
                    }
                }
            }
        }) {

            public Map<String, String> getHeaders() {
                Map<String, String>  params = new HashMap<String, String>();
                params.put("token", preferences.getString(Preferences.ACCESS_TOKEN, ""));

                return params;
            }

            protected Map<String, String> getParams() {
                HashMap<String, String> params = new HashMap<String, String>();
                params.put("name", fullname_value.getText().toString());
                params.put("gender", genderToDB);
                params.put("dob", dob_value.getText().toString());
                return params;
            }
        };
        postlogin.setRetryPolicy(new DefaultRetryPolicy(
                20000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        mRequest.add(postlogin);
    }

    private void showDatePickerForBirthDay(final TextView txt) {
        dateHelper.showDatePicker(ProfileAnakChangeActivity.this, System.currentTimeMillis(), new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                int _year = year;
                int _month = monthOfYear + 1;
                int _day = dayOfMonth;
                String date = _year + "-" + _month + "-" + _day;
                String birthDate = dateHelper.formatDate(date);
                txt.setText(birthDate);
            }
        });
    }

    @Override
    public int getLayout() {
        return R.layout.activity_profileanak_update;
    }

    @Override
    public void updateUI() {

        if (!preferences.getString(Preferences.CHILD_NAME, "").equals("")) {
            fullname_value.setText(preferences.getString(Preferences.CHILD_NAME, ""));
        }
        if (!preferences.getString(Preferences.DOB, "").equals("")) {
            dob_value.setText(preferences.getString(Preferences.DOB, ""));
        }
        if (!preferences.getString(Preferences.GENDER, "").equals("")) {
            if(preferences.getString(Preferences.GENDER, "").equalsIgnoreCase("m")){
                gender_value.setSelection(0);
            }else{
                gender_value.setSelection(1);
            }

        }

    }


    @Override
    public void onBackPressed() {
        Intent i = new Intent(getApplicationContext(), ProfileActivity.class);
        startActivity(i);
        finish();
    }

    public void dialog(String content, String msg) {
        new MaterialDialog.Builder(this)
                .title(msg)
                .content(content)
                .positiveText(getResources().getString(R.string.MuatUlang))
                .negativeText(getResources().getString(R.string.Kembali))
                .callback(new MaterialDialog.ButtonCallback() {
                    @Override
                    public void onPositive(MaterialDialog dialog) {
                        // getKalender();
                    }

                    @Override
                    public void onNegative(MaterialDialog dialog) {
                        // super.onNegative(dialog);
                        // finish();
                    }

                })
                .show();
    }


}