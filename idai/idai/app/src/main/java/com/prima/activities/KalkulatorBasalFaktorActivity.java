package com.prima.activities;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.prima.Preferences;

import java.util.ArrayList;

import prima.test.prima.R;

/**
 * Created by Codelabs on 11/08/2015.
 */
public class KalkulatorBasalFaktorActivity extends BaseActivity implements View.OnClickListener {

    float scoreInitial;
    Button lanjut;
    TextView title;
    EditText factor1,factor2,factor3,factor4,factor5,factor6,factor7,factor8,factor9,factor10,factor11;
    private ArrayList<Integer> answered = new ArrayList<Integer>();

    @Override
    public void initView() {


        lanjut = (Button) findViewById(R.id.lanjut);
        title = (TextView) findViewById(R.id.title);
        //set font
        Typeface tf = Typeface.createFromAsset(getApplicationContext().getAssets(), "Brevia-ExtraBlackItalic.otf");
        Typeface tf2 = Typeface.createFromAsset(getApplicationContext().getAssets(), "BREVIA-BLACK.OTF");
        title.setTypeface(tf);

        Typeface tfAriali = Typeface.createFromAsset(getApplicationContext().getAssets(), "ariali.ttf");
        Typeface tfArialb = Typeface.createFromAsset(getApplicationContext().getAssets(), "arialbd.ttf");

        factor1 = (EditText) findViewById(R.id.factor1);
        factor2 = (EditText) findViewById(R.id.factor2);
        factor3 = (EditText) findViewById(R.id.factor3);
        factor4 = (EditText) findViewById(R.id.factor4);
        factor5 = (EditText) findViewById(R.id.factor5);
        factor6 = (EditText) findViewById(R.id.factor6);
        factor7 = (EditText) findViewById(R.id.factor7);
        factor8 = (EditText) findViewById(R.id.factor8);
        factor9 = (EditText) findViewById(R.id.factor9);
        factor10 = (EditText) findViewById(R.id.factor10);
        factor11 = (EditText) findViewById(R.id.factor11);
    }

    @Override
    public void setUICallbacks() {
        lanjut.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        if (v==lanjut){
            if (preferences.getString(Preferences.ID,"").equals("1")) {
                editor.putString(Preferences.BIRTH_DAY, "");
                editor.putString(Preferences.JENIS_KELAMIN, "");
                editor.putString(Preferences.BERAT, "");
                editor.putString(Preferences.TINGGI, "");
                editor.putString(Preferences.ID, "");
                editor.commit();
            }
            float scoreFinal = scoreInitial;

            int factorAktivitasCount = 0;
            int factorStresCount = 0;

            if(!factor1.getText().toString().equals("")){
                scoreFinal = scoreFinal * Float.parseFloat(factor1.getText().toString());
                factorAktivitasCount = factorAktivitasCount+1;
            }if(!factor2.getText().toString().equals("")){
                scoreFinal = scoreFinal * Float.parseFloat(factor2.getText().toString());
                factorAktivitasCount = factorAktivitasCount+1;
            }if(!factor3.getText().toString().equals("")){
                scoreFinal = scoreFinal * Float.parseFloat(factor3.getText().toString());
                factorAktivitasCount = factorAktivitasCount+1;
            }if(!factor4.getText().toString().equals("")){
                scoreFinal = scoreFinal * Float.parseFloat(factor4.getText().toString());
                factorStresCount = factorStresCount+1;
            }if(!factor5.getText().toString().equals("")){
                scoreFinal = scoreFinal * Float.parseFloat(factor5.getText().toString());
                factorStresCount = factorStresCount+1;
            }if(!factor6.getText().toString().equals("")){
                scoreFinal = scoreFinal * Float.parseFloat(factor6.getText().toString());
                factorStresCount = factorStresCount+1;
            }if(!factor7.getText().toString().equals("")){
                scoreFinal = scoreFinal * Float.parseFloat(factor7.getText().toString());
                factorStresCount = factorStresCount+1;
            }if(!factor8.getText().toString().equals("")){
                scoreFinal = scoreFinal * Float.parseFloat(factor8.getText().toString());
                factorStresCount = factorStresCount+1;
            }if(!factor9.getText().toString().equals("")){
                scoreFinal = scoreFinal * Float.parseFloat(factor9.getText().toString());
                factorStresCount = factorStresCount+1;
            }if(!factor10.getText().toString().equals("")){
                scoreFinal = scoreFinal * Float.parseFloat(factor10.getText().toString());
                factorStresCount = factorStresCount+1;
            }if(!factor11.getText().toString().equals("")){
                scoreFinal = scoreFinal * Float.parseFloat(factor11.getText().toString());
                factorStresCount = factorStresCount+1;
            }

            System.out.println("nandha "+scoreFinal);


            if(factor1.getText().toString().equals("") &&
                    factor2.getText().toString().equals("") &&
                    factor3.getText().toString().equals("") &&
                    factor4.getText().toString().equals("") &&
                    factor5.getText().toString().equals("") &&
                    factor6.getText().toString().equals("") &&
                    factor7.getText().toString().equals("") &&
                    factor8.getText().toString().equals("") &&
                    factor9.getText().toString().equals("") &&
                    factor10.getText().toString().equals("") &&
                    factor11.getText().toString().equals("")){
                dialog(getResources().getString(R.string.IsiSalahSatuFaktor), getResources().getString(R.string.Perhatian));
            }else if(factorAktivitasCount>1){
                dialog(getResources().getString(R.string.MaxIsi1FaktorAktivitas), getResources().getString(R.string.Perhatian));
            }else if(factorStresCount>1){
                dialog(getResources().getString(R.string.MaxIsi1FaktorStress), getResources().getString(R.string.Perhatian));
            }else{
                Intent i = new Intent(getApplicationContext(), KalkulatorBasalFaktorResultActivity.class);
                i.putExtra("result", scoreFinal);
                startActivity(i);
            }
        }
    }

    public void dialog(String content, String msg) {
        new MaterialDialog.Builder(this)
                .title(msg)
                .content(content)
                .positiveText("OK")
                .callback(new MaterialDialog.ButtonCallback() {
                    @Override
                    public void onPositive(MaterialDialog dialog) {

                    }
                })
                .show();
    }


    @Override
    public int getLayout() {
        return R.layout.activity_kalkulator_basal_faktor;
    }

    @Override
    public void updateUI() {
        Bundle bn = getIntent().getExtras();
        if (bn != null) {
            scoreInitial = Float.parseFloat(bn.getString("result"));
            //result.setText(score);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

    }



    @Override
    public void onBackPressed() {
      //  Intent i = new Intent(getApplicationContext(), KalkulatorBasalActivity.class);
     //   startActivity(i);
        if (preferences.getString(Preferences.ID,"").equals("1")) {
            editor.putString(Preferences.BIRTH_DAY, "");
            editor.putString(Preferences.JENIS_KELAMIN, "");
            editor.putString(Preferences.BERAT, "");
            editor.putString(Preferences.TINGGI, "");
            editor.putString(Preferences.ID, "");
            editor.commit();
        }

        finish();
    }
}
