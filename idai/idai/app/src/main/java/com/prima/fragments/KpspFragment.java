package com.prima.fragments;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.view.View;

import prima.test.prima.R;

/**
 * Created by Codelabs on 07/08/2015.
 */
public class KpspFragment extends BaseFragment implements View.OnClickListener {

    private Context mContext;
    public static final String ARG_PAGE = "page 2";
    private ViewPager mPager;


    @Override
    public void initView(View view) {
        mContext = getActivity();
        mPager = (ViewPager) getActivity().findViewById(R.id.pager);
    }

    @Override
    public void setUICallbacks() {

    }

    @Override
    public void updateUI() {

    }

    @Override
    public String getPageTitle() {
        return "Account Summary";
    }


    @Override
    public int getFragmentLayout() {
        return R.layout.fragment_kpsp;
    }

    @Override
    public void onClick(View v) {

    }


}