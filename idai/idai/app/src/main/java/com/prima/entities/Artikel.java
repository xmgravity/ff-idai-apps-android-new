package com.prima.entities;

import java.util.ArrayList;

/**
 * Created by Codelabs on 02/09/2015.
 */
public class Artikel extends Message {
    private ArrayList<Oartiker> data;

    public ArrayList<Oartiker> getObjects() {
        return data;
    }

    public void setObjects(ArrayList<Oartiker> objects) {
        this.data = objects;
    }

    public class Oartiker {
        private String id;
        private String menu_id;
        private String description;
        private String source;
        private String version;
        private String code;
        private String order;

        public String getSlide() {
            return order;
        }

        public void setSlide(String slide) {
            this.order = slide;
        }

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getId_menu() {
            return menu_id;
        }

        public void setId_menu(String id_menu) {
            this.menu_id = id_menu;
        }

        public String getDeskripsi() {
            return description;
        }

        public void setDeskripsi(String deskripsi) {
            this.description = deskripsi;
        }

        public String getSumber() {
            return source;
        }

        public void setSumber(String sumber) {
            this.source = sumber;
        }

        public String getVersi() {
            return version;
        }

        public void setVersi(String versi) {
            this.version = versi;
        }
    }
}
