package com.prima.entities;

import java.util.ArrayList;

/**
 * Created by Codelabs on 07/09/2015.
 */
public class MateriEdukasi extends Message {
    private ArrayList<Materi> data;

    public ArrayList<Materi> getObjects() {
        return data;
    }

    public void setObjects(ArrayList<Materi> objects) {
        this.data = objects;
    }

    public class Materi {
        private String title;
        private String header_picture;
        private String content;
        private String version;
        private String id;
        private String age_id;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getAge_id() {
            return age_id;
        }

        public void setAge_id(String age_id) {
            this.age_id = age_id;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getHeader_picture() {
            return header_picture;
        }

        public void setHeader_picture(String header_picture) {
            this.header_picture = header_picture;
        }

        public String getContent() {
            return content;
        }

        public void setContent(String content) {
            this.content = content;
        }

        public String getEducation_content_version() {
            return version;
        }

        public void setEducation_content_version(String education_content_version) {
            this.version = education_content_version;
        }
    }
}
