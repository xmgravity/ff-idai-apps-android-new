package com.prima.controllers;

import android.content.SharedPreferences;

/**
 * Created by Codelabs on 20/08/2015.
 */
public class ApiReferences {

    private static final String BASE_SITENAME = "http://54.169.50.255/revamp/api/v1/";
    private static final String BASE_URL = "http://54.169.50.255/revamp/";
    protected SharedPreferences preferences;


    //private static final String BASE_URL = "http://54.169.50.255/public";

    /*
    private static final String BASE_SITENAME = "http://52.77.116.217/public/index.php/api/";
    private static final String BASE_URL = "http://52.77.116.217/public";
*/


    private static final String SITENAME_GOOGLE = "http://maps.googleapis.com/maps/api/geocode/";


    public static String getLokasi(String lokasi) {
        return SITENAME_GOOGLE + "json?address=" + lokasi.replace(" ", "%20") + "&sensor=true_or_false";
    }

    public static String getLokasiByLatlng(String lat, String lng) {
        return SITENAME_GOOGLE + "json?latlng=" + lat + "," + lng;
    }

    public static String generateBeratTPanjangTinggi(String id, String mom_height, String dad_height) {
        if(Double.parseDouble(mom_height)>0){
            System.out.println("nandha mom"+mom_height);
            return BASE_SITENAME + "weightHeight/" + id +"?mom_height="+mom_height+"&dad_height="+dad_height;
        }
        return BASE_SITENAME + "weightHeight/" + id;
    }

    public static String generateBeratTPanjangTinggiOffline() {
        return BASE_SITENAME + "graph/onePoint/weightHeight";
    }

    public static String generatePanjangTUmur(String id) {
        return BASE_SITENAME + "heightAge/" + id;
    }

    public static String generatePanjangTUmurOffline() {
        return BASE_SITENAME + "graph/onePoint/heightAge";
    }

    public static String generateIMTTUmur(String id) {
        return BASE_SITENAME + "bodyMassIndexAge/" + id;
    }

    public static String generateIMTTUmurOffline() {
        return BASE_SITENAME + "graph/onePoint/bodyMassIndexAge";
    }

    public static String generateLingkarKepalaTUmur(String id, String lk) {
        return BASE_SITENAME + "headCircumference/"+ id ;
    }

    public static String generateLingkarKepalaTUmurOffline() {
        return BASE_SITENAME + "graph/onePoint/headCircumference" ;
    }

    public static String generateLingkarKepalaTUmurNellhausOffline() {
        return BASE_SITENAME + "graph/onePoint/nellhaus" ;
    }

    public static String generateBeratTUmur(String id) {
        return BASE_SITENAME + "weightAge/" + id;
    }

    public static String generateBeratTUmurOffline() {
        return BASE_SITENAME + "graph/onePoint/weightAge";
    }


    public static String register() {
        return BASE_SITENAME + "doctor";
    }

    public static String updateProfile(String id) {
        return BASE_SITENAME + "doctor/" + id + "/update";
    }

    public static String createPraktik(String id) {
        return BASE_SITENAME + "office/" + id;
    }

    public static String getAllPraktek(String id) {
        return BASE_SITENAME + "getOffice/" + id;
    }

    public static String getAllPraktekByDay(String id) {
        return BASE_SITENAME + "doctor/" + id + "/getOfficeByDay";
    }

    public static String getPraktekByPlace(String id) {
        return BASE_SITENAME + "office/" + id;
    }

    public static String updatePraktik(String id) {
        return BASE_SITENAME + "office/" + id + "/update";
    }

    public static String getArticle(String code, String lang) {

        if(lang.equalsIgnoreCase("en")){
            return BASE_SITENAME + "article/" + code + "?lang=en";
        }else{
            return BASE_SITENAME + "article/" + code;
        }

    }

    public static String getPdf(String code, String id, String lang) {
        if(lang.equalsIgnoreCase("en")){
            return BASE_SITENAME + "pdf/" + code + "/" + id+"?lang=en";
        }else{
            return BASE_SITENAME + "pdf/" + code + "/" + id;
        }
    }

    public static String postImunisasi(String id) {
        return BASE_SITENAME + "postImunisasi/id=" + id;
    }

    public static String getMenuAge(String code, String lang) {
        if(lang.equalsIgnoreCase("en")){
            return BASE_SITENAME + "getMenuAge/" + code +"?lang=en";
        }else{
            return BASE_SITENAME + "getMenuAge/" + code;
        }
    }

    public static String createAgenda(String idprofil) {
        return BASE_SITENAME + "createAgenda/idprofil=" + idprofil;
    }

    public static String updateAgenda(String id) {
        return BASE_SITENAME + "updateAgenda/id=" + id;
    }

    public static String getAllAgenda(String idprofil) {
        //return BASE_SITENAME + "getAllAgenda/idprofil=" + idprofil;
        return BASE_SITENAME + "doctor/"+idprofil+"/office";
    }

    public static String getAllAgendaByDate(String idprofil) {
        return BASE_SITENAME + "getAllAgendaByDate/idprofil=" + idprofil;
    }

    public static String getAgenda(String id) {
        return BASE_SITENAME + "getAgenda/id=" + id;
    }

    public static String getTanyaJawab(String type, String lang) {
        //return BASE_SITENAME + "getTanyaJawab";

        if(lang.equalsIgnoreCase("en")){
            return BASE_SITENAME + "faq/"+type+"?lang=en";
        }else{
            return BASE_SITENAME + "faq/"+type;
        }
    }

    public static String getEdukasi(String id, String lang) {
        //return BASE_SITENAME + "getEducation/id=" + id;

        if(lang.equalsIgnoreCase("en")){
            return BASE_SITENAME + "education/" + id+"?lang=en";
        }else{
            return BASE_SITENAME + "education/" + id;
        }
    }

    public static String getPubertas(String lang) {

        //return BASE_SITENAME + "getPubertas";

        if(lang.equalsIgnoreCase("en")){
            return BASE_SITENAME + "puberty?lang=en";
        }else{
            return BASE_SITENAME + "puberty";
        }

    }

    public static String getQuestion(String id, String code, String lang) {

        if(lang.equalsIgnoreCase("en")){
            return BASE_SITENAME + "questionnaire/getByChild/" + code + "/" + id +"?lang=en";
        }else{
            return BASE_SITENAME + "questionnaire/getByChild/" + code + "/" + id;
        }
    }

    public static String getQuestionnaireByBirth(String birth, String code, String lang) {
        //return BASE_SITENAME + "getQuestionnaireByBirth/birth=" + birth + "&code=" + code;

        if(lang.equalsIgnoreCase("en")){
            return BASE_SITENAME + "questionnaire/getByBirth/"+code +"/"+ birth +"?lang=en";
        }else{
            return BASE_SITENAME + "questionnaire/getByBirth/"+code +"/"+ birth ;
        }

    }

    public static String getAllQuestionnaireByAge(String code, String lang) {
        //return BASE_SITENAME + "getAllQuestionnaireByAge/code=" + code;

        if(lang.equalsIgnoreCase("en")){
            return BASE_SITENAME + "questionnaire/getAll/"+code+"?lang=en";
        }else{
            return BASE_SITENAME + "questionnaire/getAll/"+code;
        }

    }

    public static String getQuestionnaireByRangeAge(String code, String min, String max, String lang) {

        //return BASE_SITENAME + "getQuestionnaireByRangeAge/code=" + code + "&min=" + min + "&max=" + max;

        if(lang.equalsIgnoreCase("en")){
            return BASE_SITENAME + "questionnaire/getByRangeAge/" + code + "/" + min + "/" + max+"?lang=en";
        }else{
            return BASE_SITENAME + "questionnaire/getByRangeAge/" + code + "/" + min + "/" + max;
        }
    }

    public static String login(String key, String action, String username, String password) {
        return "http://idai.or.id/api-x?key="+key+"&action="+action+"&username="+username+"&password="+password;
    }

    public static String getPasien() {
        return BASE_SITENAME + "child/findById";
    }

    public static String getPasienForAwam() {
        return BASE_SITENAME + "child";
    }

    public static String insertPasien() {
        return BASE_SITENAME + "child/createByDoctor";
    }

    public static String insertAnak() {
        return BASE_SITENAME + "child";
    }

    public static String insertCatatan() {
        return BASE_SITENAME + "note";
    }

    public static String getCatatan() { return BASE_SITENAME + "note"; }

    public static String editPasien(String id) {
        return BASE_SITENAME + "child/createHistoryByDoctor/" + id;
    }

    public static String getImageUrl() {
        return BASE_URL;
    }

    public static String registerUserAwam() {
        return BASE_SITENAME + "register";
    }

    public static String loginAwam() {
        return BASE_SITENAME + "login";
    }

    public static String forgetPassword() {
        return BASE_SITENAME + "requestForgotPassword";
    }

    public static String getOrangTua() {
        return BASE_SITENAME + "user/findByEmail";
    }

    public static String changePassword() {
        return BASE_SITENAME + "changePassword";
    }

    public static String getChildIndividualVaccine(String child_detail_name) {
        return BASE_SITENAME + "child/" + child_detail_name + "/vaccine";
    }

    public static String addVaccineAnak(String child_detail_name) {
        return BASE_SITENAME+"child/"+child_detail_name+"/vaccine";
    }

    public static String updateVaccine(String vaccine_id) {
        return BASE_SITENAME+"child/vaccine/"+vaccine_id+"/update";
    }

    public static String deleteVaccine(String vaccine_id) {
        return BASE_SITENAME+"child/vaccine/"+vaccine_id;
    }

    public static String updateProfileAwam() {
        return BASE_SITENAME+"user/updateProfile";
    }

    public static String updateProfileAnak(String child_name) {
        return BASE_SITENAME+"child/"+child_name+"/update";
    }

    public static String deleteChild(String id) {
        return BASE_SITENAME+"child/"+id;
    }

    public static String updateProfilePicture() {
        return BASE_SITENAME+"user/updateProfilePicture";
    }

    public static String fenton(){
        return BASE_SITENAME + "graph/onePoint/fenton";
    }
}

