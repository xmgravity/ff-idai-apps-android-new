package com.prima.activities;

import android.content.Intent;
import android.graphics.Typeface;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.JsonSyntaxException;

import com.prima.MainActivity;
import com.prima.Preferences;
import com.prima.adapters.AdapterListTanyaJawab;
import com.prima.controllers.ApiReferences;
import com.prima.controllers.Parser;
import com.prima.entities.Message;
import com.prima.entities.TanyaJawab;
import prima.test.prima.R;

/**
 * Created by Codelabs on 25/08/2015.
 */
public class QuestionAndAnswerActivity extends BaseActivity implements View.OnClickListener {

    TextView title;
    ImageView close, back;
    MaterialDialog dialog;
    ListView list;
    private RequestQueue mRequest;
    //   WebView web;

    @Override
    public void initView() {
        mRequest = Volley.newRequestQueue(getApplicationContext());
        close = (ImageView) findViewById(R.id.close);
        back = (ImageView) findViewById(R.id.back);
        //web = (WebView) findViewById(R.id.web);
        close.setVisibility(View.GONE);
        list = (ListView) findViewById(R.id.listView);
        //set font
        title = (TextView) findViewById(R.id.tv_actionbar_title);

        Typeface tf = Typeface.createFromAsset(getApplicationContext().getAssets(), "BREVIA-BLACK.OTF");
        title.setTypeface(tf);
        title.setText(getResources().getString(R.string.TanyaJawab));
        //    web.loadUrl("file:///android_asset/index.html");

        if(preferences.getString(Preferences.TIPE_USER, "").equalsIgnoreCase("awam")) {
            getTanyaJawabParent();
        }else{
            getTanyaJawabDoctor();
        }


    }

    @Override
    public void setUICallbacks() {
        back.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        if (v == back) {
            Intent i = new Intent(getApplicationContext(), MainActivity.class);
            startActivity(i);
            finish();
        }
    }


    @Override
    public int getLayout() {
        return R.layout.activity_question_and_answer;
    }

    @Override
    public void updateUI() {

    }


    private void getTanyaJawabDoctor() {
        progresDialog(false, getResources().getString(R.string.MemuatData));
        // TODO Tanya Jawab FAQ ada di database, tinggal ganti di database
        StringRequest listdata = new StringRequest(Request.Method.GET,
                ApiReferences.getTanyaJawab("doctor", preferences.getString(Preferences.LANGUAGE, "")),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        dialog.dismiss();
                        try {
                            if (response.equals("[]")) {
                                dialog(getResources().getString(R.string.DataBelumTersedia), getResources().getString(R.string.Perhatian));
                            } else {
                                Message msg = Parser.getMessage(response);
                                if (msg.getStatus().equals("0")) {
                                    dialog(msg.getMessages(), getResources().getString(R.string.Perhatian));
                                } else {
                                    Log.i("RESULT JSON", response.toString());
                                    TanyaJawab tanyaJawab = Parser.getTanyaJawab(response);

                                    AdapterListTanyaJawab adapterList = new AdapterListTanyaJawab(
                                            tanyaJawab.getObjects(), QuestionAndAnswerActivity.this);
                                    list.setAdapter(adapterList);

                                }
                            }
                        } catch (JsonSyntaxException e) {
                            dialog(getResources().getString(R.string.KesalahanServer), getResources().getString(R.string.Perhatian));
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dialog.dismiss();
                dialog(getResources().getString(R.string.NoConnection), getResources().getString(R.string.Perhatian));
            }
        });
        listdata.setRetryPolicy(new DefaultRetryPolicy(
                20000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        mRequest.add(listdata);

    }

    private void getTanyaJawabParent() {
        progresDialog(false, getResources().getString(R.string.MemuatData));
        // TODO Tanya Jawab FAQ ada di database, tinggal ganti di database
        StringRequest listdata = new StringRequest(Request.Method.GET,
                ApiReferences.getTanyaJawab("parent", preferences.getString(Preferences.LANGUAGE, "")),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        dialog.dismiss();
                        try {
                            if (response.equals("[]")) {
                                dialog(getResources().getString(R.string.DataBelumTersedia), getResources().getString(R.string.Perhatian));
                            } else {
                                Message msg = Parser.getMessage(response);
                                if (msg.getStatus().equals("0")) {
                                    dialog(msg.getMessages(), getResources().getString(R.string.Perhatian));
                                } else {
                                    Log.i("RESULT JSON", response.toString());
                                    TanyaJawab tanyaJawab = Parser.getTanyaJawab(response);

                                    AdapterListTanyaJawab adapterList = new AdapterListTanyaJawab(
                                            tanyaJawab.getObjects(), QuestionAndAnswerActivity.this);
                                    list.setAdapter(adapterList);

                                }
                            }
                        } catch (JsonSyntaxException e) {
                            dialog(getResources().getString(R.string.KesalahanServer), getResources().getString(R.string.Perhatian));
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dialog.dismiss();
                dialog(getResources().getString(R.string.NoConnection), getResources().getString(R.string.Perhatian));
            }
        });
        listdata.setRetryPolicy(new DefaultRetryPolicy(
                20000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        mRequest.add(listdata);

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

    }


    private void progresDialog(boolean horizontal, String msg) {
        dialog = new MaterialDialog.Builder(this)
                .title(msg)
                .content(getResources().getString(R.string.MohonTunggu))
                .progress(true, 0)
                .progressIndeterminateStyle(horizontal)
                .cancelable(false)
                .show();
    }


    public void dialog(String content, String msg) {
        new MaterialDialog.Builder(this)
                .title(msg)
                .content(content)
                .positiveText("Ok")
                .callback(new MaterialDialog.ButtonCallback() {
                    @Override
                    public void onPositive(MaterialDialog dialog) {
                    }

                })
                .show();
    }

    @Override
    public void onBackPressed() {
        Intent i = new Intent(getApplicationContext(), MainActivity.class);
        startActivity(i);
        finish();
    }
}
