package com.prima.activities;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.prima.Preferences;

import prima.test.prima.R;

/**
 * Created by Codelabs on 17/09/2015.
 */
public class TahapPubertasResultActivity extends BaseActivity implements View.OnClickListener {

    Button ok;
    TextView title, result, mamae, genitalia;

    @Override
    public void initView() {
        ok = (Button) findViewById(R.id.ok);
        title = (TextView) findViewById(R.id.title);
        genitalia = (TextView) findViewById(R.id.genitalia);
        mamae = (TextView) findViewById(R.id.mamae);
        result = (TextView) findViewById(R.id.result);
        //set font
        Typeface tf = Typeface.createFromAsset(getApplicationContext().getAssets(), "Brevia-ExtraBlackItalic.otf");
        Typeface tf2 = Typeface.createFromAsset(getApplicationContext().getAssets(), "BREVIA-BLACK.OTF");
        title.setTypeface(tf);
        result.setTypeface(tf2);

        Typeface tfAriali = Typeface.createFromAsset(getApplicationContext().getAssets(), "ariali.ttf");
        Typeface tfArialb = Typeface.createFromAsset(getApplicationContext().getAssets(), "arialbd.ttf");

        TextView trademark = (TextView) findViewById(R.id.trademark);
        TextView trademark2 = (TextView) findViewById(R.id.trademark2);
        TextView trademark3 = (TextView) findViewById(R.id.trademark3);
        trademark.setTypeface(tfAriali);
        trademark2.setTypeface(tfArialb);
        trademark3.setTypeface(tfAriali);

        if(preferences.getString(Preferences.TIPE_USER, "").equalsIgnoreCase("awam")){
            trademark.setVisibility(View.GONE);
            trademark2.setVisibility(View.GONE);
            trademark3.setVisibility(View.GONE);
        }
    }

    @Override
    public void setUICallbacks() {
        ok.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        if (v == ok) {
            //  Intent i = new Intent(getApplicationContext(), KalkulatorBasalActivity.class);
            //  startActivity(i);
            finish();
        }
    }


    @Override
    public int getLayout() {
        return R.layout.activity_tahap_pubertas_result;
    }

    @Override
    public void updateUI() {
        Bundle bn = getIntent().getExtras();
        if (bn != null) {
            String score = bn.getString("result");
            if (score.contains("M")) {
                genitalia.setVisibility(View.GONE);
            }
            if (score.contains("G")) {
                mamae.setVisibility(View.GONE);
            }
            result.setText(score);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

    }


    @Override
    public void onBackPressed() {
        //  Intent i = new Intent(getApplicationContext(), KalkulatorBasalActivity.class);
        //   startActivity(i);
        finish();
    }
}
