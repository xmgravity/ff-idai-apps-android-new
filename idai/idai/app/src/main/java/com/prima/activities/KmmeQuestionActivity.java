package com.prima.activities;

import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.prima.adapters.PagerAdapter;
import com.prima.controllers.Parser;
import com.prima.entities.Question;
import com.prima.fragments.KmmeQuestionFragment;
import com.prima.helpers.NonSwipeAbleViewPager;

import java.util.ArrayList;
import java.util.List;

import prima.test.prima.R;

/**
 * Created by Codelabs on 18/08/2015.
 */
public class KmmeQuestionActivity extends  BaseActivity implements View.OnClickListener {
    private ImageView close;
    private ImageView back;
    private Button ok;
    private static Context mContext;
    private NonSwipeAbleViewPager mPager;
    private PagerAdapter mPagerAdapter;
    private static int currentPage;
    PageListener pageListener;
    static TextView title, page;
    LinearLayout toolbar;
    private static ProgressBar progres;
    static int progresValue;
    static Question question;
    static List<Fragment> fragmentList = new ArrayList<Fragment>();
    private ArrayList<String> ansewer = new ArrayList<String>();


    public List<Fragment> getFragmentList() {
        return fragmentList;
    }

    public ArrayList<String> getAnsewer() {
        return ansewer;
    }

    public void setAnsewer(ArrayList<String> ansewer) {
        this.ansewer = ansewer;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = getApplicationContext();
        currentPage = 0;

    }

    @Override
    public void initView() {
        page = (TextView) findViewById(R.id.page);
        progres = (ProgressBar) findViewById(R.id.progressBar);
        progres.setProgress(15);
        back = (ImageView) findViewById(R.id.back);
        mPager = (NonSwipeAbleViewPager) findViewById(R.id.pager);
        close = (ImageView) findViewById(R.id.close);
        pageListener = new PageListener();
        mPager.setOnPageChangeListener(pageListener);
        toolbar = (LinearLayout) findViewById(R.id.toolbar);

        title = (TextView) findViewById(R.id.tv_actionbar_title);
        Typeface tf = Typeface.createFromAsset(getApplicationContext().getAssets(), "BREVIA-BLACK.OTF");
        title.setTypeface(tf);
        title.setText(getResources().getString(R.string.KuesionerMasalahMentalEmosional));


        getData();


    }

    @Override
    public void setUICallbacks() {
        back.setOnClickListener(this);
        close.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        if (v == back) {
            if (currentPage > 0) {
                currentPage--;
                mPager.setCurrentItem(currentPage, true);
            } else {
                //Intent i = new Intent(getApplicationContext(), BallardScoreActivity.class);
                //  startActivity(i);
                finish();
            }
        } else if (v == close) {
            //  Intent i = new Intent(getApplicationContext(), BallardScoreActivity.class);
            //  startActivity(i);
            finish();
        }
    }


    @Override
    public int getLayout() {
        return R.layout.activity_kmme_question;
    }

    @Override
    public void updateUI() {

    }

    void getData() {
        Bundle bn = getIntent().getExtras();
        if (bn != null) {
            String response = bn.getString("respon");
            setupFrae();
            Log.i("RESULT JSON", response);
            question = new Question();
            question = Parser.getQuestion(response);
            fragmentList.clear();
            for (int i = 0; i < question.getObjects().size(); i++) {
                fragmentList.add(new KmmeQuestionFragment(question.getObjects().get(i),i));
                ansewer.add("");
            }
            progresValue = 100 / fragmentList.size();
            progres.setProgress(progresValue);
            setupFrae();

            page.setText(page.getResources().getString(R.string.dari2) + " " + fragmentList.size() + " " + page.getResources().getString(R.string.penilaian));
        }
    }


    @Override
    public void onBackPressed() {
        if (currentPage > 0) {
            currentPage--;
            mPager.setCurrentItem(currentPage, true);
        } else {
            //  Intent i = new Intent(getApplicationContext(), BallardScoreActivity.class);
            //   startActivity(i);
            finish();
        }
    }

    private static class PageListener extends ViewPager.SimpleOnPageChangeListener {


        public void onPageSelected(int position) {
            currentPage = position;
            page.setText(currentPage + 1 + " " + page.getResources().getString(R.string.dari) + " "+ fragmentList.size() + " " + page.getResources().getString(R.string.penilaian));
            progres.setProgress(progresValue * (position + 1));
            if ((position+1)==question.getObjects().size()){
                progres.setProgress(100);
            }

        }
    }


    private void setupFrae() {
        mPagerAdapter = new PagerAdapter(getSupportFragmentManager(),
                KmmeQuestionActivity.this, fragmentList);
        mPager.setAdapter(mPagerAdapter);

    }

}