package com.prima.entities;

/**
 * Created by Codelabs on 19/10/2015.
 */
public class Dokter extends Message {
    private ObjectDokter data;

    public ObjectDokter getObjects() {
        return data;
    }

    public void setObjects(ObjectDokter data) {
        this.data = data;
    }

    public class ObjectDokter {
        private String id;
        private String name;
        private String email;
        private String automatic_email;
        private String profile_picture;
        private String npa;
        private String sub_specialist;

        public String getSub_spesialisasi() {
            return sub_specialist;
        }

        public void setSub_spesialisasi(String sub_spesialisasi) {
            this.sub_specialist = sub_spesialisasi;
        }

        public String getNpa() {
            return npa;
        }

        public void setNpa(String npa) {
            this.npa = npa;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getNama_dokter() {
            return name;
        }

        public void setNama_dokter(String name) {
            this.name = name;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getEmail_otomatis() {
            return automatic_email;
        }

        public void setEmail_otomatis(String automatic_email) {
            this.automatic_email = automatic_email;
        }

        public String getFoto() {
            return profile_picture;
        }

        public void setFoto(String profile_picture) {
            this.profile_picture = profile_picture;
        }
    }
}
