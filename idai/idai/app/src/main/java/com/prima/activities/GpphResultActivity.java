package com.prima.activities;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.prima.Preferences;

import at.grabner.circleprogress.CircleProgressView;
import at.grabner.circleprogress.TextMode;
import prima.test.prima.R;

/**
 * Created by Codelabs on 18/08/2015.
 */
public class GpphResultActivity extends BaseActivity implements View.OnClickListener {

    Button lanjut;
    TextView title, result, score;
    CircleProgressView circleView;

    @Override
    public void initView() {
        circleView = (CircleProgressView) findViewById(R.id.circleView);
        lanjut = (Button) findViewById(R.id.lanjut);
        title = (TextView) findViewById(R.id.title);
        result = (TextView) findViewById(R.id.interpretasi);
        score = (TextView) findViewById(R.id.score);
        //set font
        Typeface tf = Typeface.createFromAsset(getApplicationContext().getAssets(), "BREVIA-BLACK.OTF");
        title.setTypeface(tf);
        result.setTypeface(tf);
        circleView.setTextTypeface(tf);

        Typeface tfAriali = Typeface.createFromAsset(getApplicationContext().getAssets(), "ariali.ttf");
        Typeface tfArialb = Typeface.createFromAsset(getApplicationContext().getAssets(), "arialbd.ttf");

        TextView trademark = (TextView) findViewById(R.id.trademark);
        TextView trademark2 = (TextView) findViewById(R.id.trademark2);
        TextView trademark3 = (TextView) findViewById(R.id.trademark3);
        trademark.setTypeface(tfAriali);
        trademark2.setTypeface(tfArialb);
        trademark3.setTypeface(tfAriali);

        if(preferences.getString(Preferences.TIPE_USER, "").equalsIgnoreCase("awam")){
            trademark.setVisibility(View.GONE);
            trademark2.setVisibility(View.GONE);
            trademark3.setVisibility(View.GONE);
        }
    }

    @Override
    public void setUICallbacks() {
        lanjut.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        if (v == lanjut) {
            //      Intent i = new Intent(getApplicationContext(), GpphActivity.class);
            //  startActivity(i);
            finish();
        }
    }


    @Override
    public int getLayout() {
        return R.layout.activity_result_gpph;
    }

    @Override
    public void updateUI() {
        Bundle bn = getIntent().getExtras();
        if (bn != null) {
            int scr = bn.getInt("score");
            int max_score = bn.getInt("max_score");
            score.setText(String.valueOf(scr));
            if (scr >= 13) {
                result.setText(getResources().getString(R.string.GPPHResult));
            } else {
                result.setText(getResources().getString(R.string.GPPHResult2));
            }


            //cover header
            int windowContainerWidth = this.getResources().getDisplayMetrics().widthPixels;
            circleView.getLayoutParams().width = windowContainerWidth/2;
            circleView.getLayoutParams().height = windowContainerWidth/2;

            circleView.setMaxValue(max_score);
            circleView.setValue(scr);
            circleView.setText(scr + "");
            circleView.setTextMode(TextMode.TEXT);
            circleView.setSeekModeEnabled(false);
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

    }


    @Override
    public void onBackPressed() {
        //   Intent i = new Intent(getApplicationContext(), GpphActivity.class);
        //     startActivity(i);
        finish();
    }
}
