package com.prima.activities;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.prima.MainActivity;
import com.prima.Preferences;

import prima.test.prima.R;

/**
 * Created by Codelabs on 09/08/2015.
 */
public class Psc17Result extends BaseActivity implements View.OnClickListener {

    Button lanjut;
    TextView title, interpretasi,
            scoreIntTxt,
            scoreEksTxt,
            scorePerTxt,
            scoretotalTxt,
            interpretasiInternalisasi,
            interpretasiEksternalisasi,
            interpretasiTotal,
            interpretasiPerhatian;
    ImageView back, close;
    LinearLayout totalNilai;

    @Override
    public void initView() {

        totalNilai = (LinearLayout) findViewById(R.id.totalNilai);
        lanjut = (Button) findViewById(R.id.lanjut);
        title = (TextView) findViewById(R.id.title);
        interpretasi = (TextView) findViewById(R.id.interpretasi);
        back = (ImageView) findViewById(R.id.back);
        close = (ImageView) findViewById(R.id.close);

        scoreIntTxt = (TextView) findViewById(R.id.scoreIntTxt);
        scoreEksTxt = (TextView) findViewById(R.id.scoreEksTxt);
        scorePerTxt = (TextView) findViewById(R.id.scorePerTxt);
        scoretotalTxt = (TextView) findViewById(R.id.scoreTotalTxt);

        interpretasiInternalisasi = (TextView) findViewById(R.id.interpretasiInternalisasi);
        interpretasiEksternalisasi = (TextView) findViewById(R.id.interpretasiEksternalisasi);
        interpretasiPerhatian = (TextView) findViewById(R.id.interpretasiPerhatian);
        interpretasiTotal = (TextView) findViewById(R.id.interpretasiTotal);
        //set font
        Typeface tf = Typeface.createFromAsset(getApplicationContext().getAssets(), "BREVIA-BLACK.OTF");
        title.setTypeface(tf);

        Typeface tfAriali = Typeface.createFromAsset(getApplicationContext().getAssets(), "ariali.ttf");
        Typeface tfArialb = Typeface.createFromAsset(getApplicationContext().getAssets(), "arialbd.ttf");

        TextView trademark = (TextView) findViewById(R.id.trademark);
        TextView trademark2 = (TextView) findViewById(R.id.trademark2);
        TextView trademark3 = (TextView) findViewById(R.id.trademark3);
        trademark.setTypeface(tfAriali);
        trademark2.setTypeface(tfArialb);
        trademark3.setTypeface(tfAriali);

        if(preferences.getString(Preferences.TIPE_USER, "").equalsIgnoreCase("awam")){
            trademark.setVisibility(View.GONE);
            trademark2.setVisibility(View.GONE);
            trademark3.setVisibility(View.GONE);
        }
    }

    @Override
    public void setUICallbacks() {
        lanjut.setOnClickListener(this);
        close.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        if (v == lanjut) {
            Intent i = new Intent(getApplicationContext(),MainActivity.class);
            startActivity(i);
            finish();
            finish();
        }else if (v == close);
            Intent i = new Intent(getApplicationContext(),MainActivity.class);
            startActivity(i);
            finish();
    }


    @Override
    public int getLayout() {
        return R.layout.activity_result_psc17;
    }

    @Override
    public void updateUI() {
        Bundle bn = getIntent().getExtras();
        if (bn != null) {
            int score = bn.getInt("score");
            String inter = bn.getString("interpretasi");
            int max_score = bn.getInt("max_score");

            int scoreInternalisasi = bn.getInt("scoreInternalisasi");
            int scoreEksternalisasi = bn.getInt("scoreEksternalisasi");
            int scorePerhatian = bn.getInt("scorePerhatian");


            System.out.println("nandha internalisasi total : " + scoreInternalisasi);
            System.out.println("nandha eksternalisasi total : " + scoreEksternalisasi);
            System.out.println("nandha perhatian total: " + scorePerhatian);


//            if (score >= 13 && score <= 15) {
//                interpretasi.setText("Cedera Otak Ringan\nGCS 13-15");
//            } else if (score >= 9 && score <= 12) {
//                interpretasi.setText("Cedera Otak Sedang\nGCS 9-12");
//            } else {
//                interpretasi.setText("Cedera Otak Berat\nGCS 3-8");
//            }
            interpretasi.setText(inter);
            /*
            scoreInternalisasiTxt.setText(bn.getInt("scoreInternalisasi"));
            scoreEksternalisasiTxt.setText(bn.getInt("scoreEksternalisasi"));
            scorePerhatianTxt.setText(bn.getInt("scorePerhatian"));
            */

            scoreIntTxt.setText(Integer.toString(scoreInternalisasi));
            scoreEksTxt.setText(Integer.toString(scoreEksternalisasi));
            scorePerTxt.setText(Integer.toString(scorePerhatian));
            scoretotalTxt.setText(Integer.toString(score));

            if (scoreInternalisasi >= 5 ) {
                interpretasiInternalisasi.setText(getResources().getString(R.string.SuspekGangguanInternalisasi));
            }
            if (scoreEksternalisasi >= 7 ) {
                interpretasiEksternalisasi.setText(getResources().getString(R.string.SuspekGangguanEksternalisasi));
            }
            if (scorePerhatian >= 7 ) {
                interpretasiPerhatian.setText(getResources().getString(R.string.SuspekGangguanPerhatian));
            }
            if (score >= 15 ) {
                interpretasiTotal.setText(getResources().getString(R.string.SuspekGangguanPsikososial));
            }

            if (score < 15 ) {
                totalNilai.setVisibility(View.GONE);
                interpretasiTotal.setText(getResources().getString(R.string.SuspekGangguanPsikososial));
            }




        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

    }


    @Override
    public void onBackPressed() {
        //  Intent i = new Intent(getApplicationContext(), PgcsActivity.class);
        //  startActivity(i);
        finish();
    }
}
