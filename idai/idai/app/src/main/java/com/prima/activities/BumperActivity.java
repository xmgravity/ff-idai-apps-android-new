package com.prima.activities;

import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Handler;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.VideoView;

import com.prima.MainActivity;
import com.prima.Preferences;

import prima.test.prima.R;

/**
 * Created by Codelabs on 28/08/2015.
 */
public class BumperActivity extends BaseActivity implements View.OnClickListener {
    VideoView video_player_view;
    RelativeLayout skip;
    TextView title, subtitle;
    ImageView backgroundimage;
    Uri video;

    @Override
    public void initView() {
        title = (TextView) findViewById(R.id.title);
        subtitle = (TextView) findViewById(R.id.subtitle);
        backgroundimage = (ImageView) findViewById(R.id.background_image);

        Typeface tf = Typeface.createFromAsset(getApplicationContext().getAssets(), "BREVIA-BLACK.OTF");
        Typeface tf1 = Typeface.createFromAsset(getApplicationContext().getAssets(), "BREVIA-SEMIBOLD.OTF");
        Typeface tf2 = Typeface.createFromAsset(this.getAssets(), "Coheadline.TTF");


        title.setTypeface(tf2);
        //subtitle.setTypeface(tf2);

        if(preferences.getString(Preferences.TIPE_USER, "").equalsIgnoreCase("awam")){
            backgroundimage.setBackground(getResources().getDrawable(R.mipmap.background_bumper_new));
        }else{
            backgroundimage.setBackground(getResources().getDrawable(R.mipmap.background_bumper_profesional_new));
        }

        delay(2);

        /*Intent i = new Intent(getBaseContext(), MainActivity.class);
        startActivity(i);
        finish();*/

        /*if (preferences.getString(Preferences.ISLOGOUT, "").equalsIgnoreCase("LOGOUT")) {
            video = Uri.parse("android.resource://" + getPackageName() + "/"
                    + R.raw.bumper_keluar);
        } else {
            video = Uri.parse("android.resource://" + getPackageName() + "/"
                    + R.raw.bumper_masuk);
        }

        video_player_view = (VideoView) findViewById(R.id.video_player_view);
        video_player_view.setVideoURI(video);
        video_player_view.start();
        video_player_view.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                if (preferences.getString(Preferences.ISLOGOUT, "").equalsIgnoreCase("LOGOUT")) {
                    editor.putString(Preferences.ISLOGOUT, "");
                    editor.commit();
                    Intent i = new Intent(getBaseContext(), SplashPagerActivity.class);
                    startActivity(i);
                    finish();
                } else {
                    Intent i = new Intent(getBaseContext(), MainActivity.class);
                    startActivity(i);
                    finish();
                }
            }
        });*/


    }

    @Override
    public void setUICallbacks() {
        //skip.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        /*
        if (v == skip) {
            if (preferences.getString(Preferences.ISLOGOUT, "").equalsIgnoreCase("LOGOUT")) {
                editor.putString(Preferences.ISLOGOUT, "");
                editor.commit();
                Intent i = new Intent(getBaseContext(), SplashPagerActivity.class);
                startActivity(i);
                finish();
            } else {
                Intent i = new Intent(getBaseContext(), MainActivity.class);
                startActivity(i);
                finish();
            }
        }*/
    }


    @Override
    public int getLayout() {
        return R.layout.activity_bumper;
    }

    @Override
    public void updateUI() {
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    public void delay(int seconds){
        final int milliseconds = seconds * 1000;
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                final Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Intent i = new Intent(getBaseContext(), MainActivity.class);
                        startActivity(i);
                        finish();              //add your code here
                    }
                }, milliseconds);
            }
        });
    }
}