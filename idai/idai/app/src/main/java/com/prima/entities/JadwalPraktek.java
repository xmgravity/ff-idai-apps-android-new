package com.prima.entities;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Codelabs on 09/09/2015.
 */
public class JadwalPraktek /*extends Message*/ implements Serializable {
    private ArrayList<Jadwal> schedule;
    private ArrayList<Praktek> data;

    public ArrayList<Jadwal> getJadwals() {
        return schedule;
    }

    public void setJadwals(ArrayList<Jadwal> schedule) {
        this.schedule = schedule;
    }
    private String status;
    private String messages;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessages() {
        return messages;
    }

    public void setMessages(String messages) {
        this.messages = messages;
    }

    public ArrayList<Praktek> getObjects() {
        return data;
    }

    public void setObjects(ArrayList<Praktek> data) {
        this.data = data;
    }


    public class GetPraktek /*extends Message*/ {
        private Praktek objects;

        public Praktek getObjects() {
            return objects;
        }

        public void setObjects(Praktek objects) {
            this.objects = objects;
        }
    }

    public class Praktek {
        private String id;
        private String doctor_id;
        private String name;
        private String address;
        private String phone;
        private ArrayList<Jadwal> hours;
        private ArrayList<Jadwal> schedules;
        private String lng;
        private String lat;

        public String getHari() {
            return day;
        }

        public void setHari(String day) {
            this.day = day;
        }

        private String day;

        public String getLongitude() {
            return lng;
        }

        public void setLongitude(String lng) {
            this.lng = lng;
        }

        public String getLatitude() {
            return lat;
        }

        public void setLatitude(String lat) {
            this.lat = lat;
        }


        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getId_profil() {
            return doctor_id;
        }

        public void setId_profil(String doctor_id) {
            this.doctor_id = doctor_id;
        }

        public String getNama_praktik() {
            return name;
        }

        public void setNama_praktik(String name) {
            this.name = name;
        }

        public String getAlamat() {
            return address;
        }

        public void setAlamat(String address) {
            this.address = address;
        }

        public String getTelp() {
            return phone;
        }

        public void setTelp(String phone) {
            this.phone = phone;
        }

        public ArrayList<Jadwal> getJadwal() {
            return hours;
        }

        public void setJadwal(ArrayList<Jadwal> hours) {
            this.hours = hours;
        }

        public ArrayList<Jadwal> getSchedules() {
            return schedules;
        }

        public void setSchedules(ArrayList<Jadwal> schedules) {
            this.schedules = schedules;
        }
    }

    public class Jadwal {
        private String id;
        private String doctor_office_id;
        private String start_time;
        private String day;
        private String end_time;
        private String lng;
        private String lat;

        public String getTelp() {
            return phone;
        }

        public void setTelp(String phone) {
            this.phone = phone;
        }

        private String phone;
        private String address;
        private String status;

        public String getNama_praktik() {
            return name;
        }

        public void setNama_praktik(String name) {
            this.name = name;
        }

        private String name;

        public String getLongitude() {
            return lng;
        }

        public void setLongitude(String lng) {
            this.lng = lng;
        }

        public String getLatitude() {
            return lat;
        }

        public void setLatitude(String lat) {
            this.lat = lat;
        }

        public String getAlamat() {
            return address;
        }

        public void setAlamat(String address) {
            this.address = address;
        }

        public String getHari() {
            return day;
        }

        public void setHari(String day) {
            this.day = day;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getId_profil_praktik() {
            return doctor_office_id;
        }

        public void setId_profil_praktik(String doctor_office_id) {
            this.doctor_office_id = doctor_office_id;
        }

        public String getJam_mulai() {
            return start_time;
        }

        public void setJam_mulai(String start_time) {
            this.start_time = start_time;
        }

        public String getJam_akhir() {
            return end_time;
        }

        public void setJam_akhir(String end_time) {
            this.end_time = end_time;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }
    }
}
