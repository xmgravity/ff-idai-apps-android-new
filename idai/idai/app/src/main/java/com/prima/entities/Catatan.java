package com.prima.entities;

/**
 * Created by Codelabs on 23/08/2015.
 */
public class Catatan extends Message {

    private ObjectCatatan catatan;

    public ObjectCatatan getCatatan() {
        return catatan;
    }

    public void setObjects(ObjectCatatan catatan) {
        this.catatan = catatan;
    }

    public class ObjectCatatan {
        private String title;
        private String description;
        private String date_time;
        private String photo;




        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getDate_time() { return date_time; }

        public void setDate_time(String date_time) {
            this.date_time = date_time;
        }

        public String getPhoto() {
            return photo;
        }

        public void setPhoto(String photo) {
            this.photo = photo;
        }

    }

}
