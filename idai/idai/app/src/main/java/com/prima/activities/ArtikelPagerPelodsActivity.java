package com.prima.activities;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.prima.MainActivity;
import com.prima.Preferences;
import com.prima.adapters.PagerAdapter;
import com.prima.controllers.ApiReferences;
import com.prima.controllers.Parser;
import com.prima.entities.Artikel;
import com.prima.entities.Message;
import com.prima.entities.Pasien;
import com.prima.entities.Question;
import com.prima.fragments.ArtikelPinkFragment;
import com.prima.helpers.DatabaseHandler;
import com.prima.helpers.DateHelper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import prima.test.prima.R;

/**
 * Created by Codelabs on 06/10/2015.
 */
public class ArtikelPagerPelodsActivity extends BaseActivity implements View.OnClickListener {
    private Button ok;
    private Context mContext;
    private ViewPager mPager;
    private PagerAdapter mPagerAdapter;
    private static int currentPage;
    PageListener pageListener;
    static TextView title;
    List<Fragment> fragmentList = new ArrayList<Fragment>();
    static LinearLayout indikatorWrapper;
    private Artikel artikel;

    //

    private ImageView close;
    MaterialDialog dialog;
    Question question;
    private RequestQueue mRequest;
    DateHelper dateHelper;
    DatabaseHandler db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = getApplicationContext();
    }

    @Override
    public void initView() {
        indikatorWrapper = (LinearLayout) findViewById(R.id.indikator_pager);
        mPager = (ViewPager) findViewById(R.id.pager);
        title = (TextView) findViewById(R.id.title);
        pageListener = new PageListener();
        mPager.setOnPageChangeListener(pageListener);
        ok = (Button) findViewById(R.id.lanjut);
        Typeface tf = Typeface.createFromAsset(getApplicationContext().getAssets(), "BREVIA-BLACK.OTF");
        title.setTypeface(tf);

        //
        db = new DatabaseHandler(context);
        dateHelper = new DateHelper();
        mRequest = Volley.newRequestQueue(getApplicationContext());
        close = (ImageView) findViewById(R.id.close);

    }

    @Override
    public void setUICallbacks() {
        ok.setOnClickListener(this);
        close.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        if (v == ok) {
            if (preferences.getString(Preferences.ID, "").equalsIgnoreCase("") || preferences.getString(Preferences.JENIS_KELAMIN, "").equalsIgnoreCase("")) {
                // get prompts.xml view
                LayoutInflater li = LayoutInflater.from(getApplicationContext());
                View dialogView = li.inflate(R.layout.dialog_offline_kalkulator, null);
                final Dialog dialog3;
                dialog3 = new Dialog(context);
                dialog3.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog3.setContentView(dialogView);
                //make transparent dialog_sesi_offline_pelod
                dialog3.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                dialog3.show();


                Button dialogLanjut = (Button) dialogView.findViewById(R.id.ok);
                final RadioButton rad1 = (RadioButton) dialogView.findViewById(R.id.rad1);
                final RadioButton rad2 = (RadioButton) dialogView.findViewById(R.id.rad2);
                rad1.setChecked(true);

                rad1.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        rad1.setChecked(true);
                        rad2.setChecked(false);
                    }
                });

                rad2.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        rad2.setChecked(true);
                        rad1.setChecked(false);
                    }
                });

                final TextView edt = (TextView) dialogView.findViewById(R.id.edt1);
                final EditText edt2 = (EditText) dialogView.findViewById(R.id.edt2);
                final EditText edt3 = (EditText) dialogView.findViewById(R.id.edt3);

                final LinearLayout lin1 = (LinearLayout) dialogView.findViewById(R.id.lin1);
                final LinearLayout lin2 = (LinearLayout) dialogView.findViewById(R.id.lin2);
                lin1.setVisibility(View.GONE);
                lin2.setVisibility(View.GONE);

                edt.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        showDatepickerDialog(edt);
                    }
                });
                dialogLanjut.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (edt.getText().toString().equals("")) {
                            dialog2(getResources().getString(R.string.IsiSeluruhIsian), getResources().getString(R.string.Perhatian));
                        } else {
                            String jk;
                            if (rad1.isChecked()) {
                                jk = "1";
                            } else if (rad2.isChecked()) {
                                jk = "0";
                            } else {
                                jk = "";
                            }
                            //    saveSesiPasien(jk, edt.getText().toString(), edt3.getText().toString(), edt2.getText().toString());

                            editor.putString(Preferences.TINGGI, edt2.getText().toString());
                            editor.putString(Preferences.BERAT, edt3.getText().toString());
                            editor.putString(Preferences.BIRTH_DAY, edt.getText().toString());
                            editor.putString(Preferences.JENIS_KELAMIN, jk);
                            editor.putString(Preferences.ID, "1");
                            editor.commit();

                            getQuestion();
                            dialog3.dismiss();
                        }
                    }
                });

            } else {
                getQuestion();
            }
        } else if (v == close) {
            Intent i = new Intent(getApplicationContext(), MainActivity.class);
            startActivity(i);
            finish();
        }
    }


    @Override
    public int getLayout() {
        return R.layout.activity_pelod;
    }

    @Override
    public void updateUI() {
        indikatorWrapper.removeAllViewsInLayout();
        fragmentList.clear();
        Bundle bn = getIntent().getExtras();
        if (bn != null) {
            String respon = bn.getString("result");
            artikel = Parser.getArtikel(respon);
            //   title.setText(artikel.getObjects().get(0).getTitle());
            for (int i = 0; i < artikel.getObjects().size(); i++) {
                fragmentList.add(new ArtikelPinkFragment(i, artikel));
                View child = getLayoutInflater().inflate(R.layout.indikator_pager, null);
                View child2 = getLayoutInflater().inflate(R.layout.indikator_pager_blue, null);
                if (i == 0) {
                    indikatorWrapper.addView(child2);
                } else {
                    indikatorWrapper.addView(child);
                }
            }

            setupFrae();
            if (artikel.getObjects().size() > 1) {
                indikatorWrapper.setVisibility(View.VISIBLE);
            } else {
                indikatorWrapper.setVisibility(View.GONE);
            }
        }


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

    }


    @Override
    public void onBackPressed() {
        Intent i = new Intent(getApplicationContext(), MainActivity.class);
        startActivity(i);
        finish();
    }

    private class PageListener extends ViewPager.SimpleOnPageChangeListener {
        public void onPageSelected(int position) {
            currentPage = position;
            // title.setText(artikel.getObjects().get(position).getTitle());
            indikatorWrapper.removeAllViewsInLayout();
            for (int i = 0; i < artikel.getObjects().size(); i++) {
                View child = getLayoutInflater().inflate(R.layout.indikator_pager, null);
                View child2 = getLayoutInflater().inflate(R.layout.indikator_pager_blue, null);
                if (i == position) {
                    indikatorWrapper.addView(child2);
                } else {
                    indikatorWrapper.addView(child);
                }
            }

        }
    }


    private void setupFrae() {
        mPagerAdapter = new PagerAdapter(getSupportFragmentManager(),
                ArtikelPagerPelodsActivity.this, fragmentList);
        mPager.setAdapter(mPagerAdapter);

    }


    private void getQuestion() {
        final String code = "PELODS";
        progresDialog(false, getResources().getString(R.string.MemuatData));
        String uri;

        if (preferences.getString(Preferences.ID, "").equals("1")) {
            uri = ApiReferences.getQuestionnaireByBirth(preferences.getString(Preferences.BIRTH_DAY, ""), code, preferences.getString(Preferences.LANGUAGE, ""));
        } else {
            uri = ApiReferences.getQuestion(preferences.getString(Preferences.ID, ""), code, preferences.getString(Preferences.LANGUAGE, ""));
        }

        StringRequest listdata = new StringRequest(Request.Method.GET, uri,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        dialog.dismiss();
                        try {
                            if (response.equals("[]")) {
                                dialog(getResources().getString(R.string.BelumAdaData), getResources().getString(R.string.Pesan));
                            } else {
                                Message msg = Parser.getMessage(response);
                                if (msg.getStatus().equals("1")) {
                                    Log.i("RESULT JSON", response.toString());
                                    question = new Question();
                                    question = Parser.getQuestion(response);


                                    for (int j = 0; j < question.getObjects().size(); j++) {
                                        db.deleteQuestion(question.getObjects().get(j).getId(), question.getObjects().get(j).getMin_age(), question.getObjects().get(j).getMax_age(), "");
                                        db.deleteAnswer(question.getObjects().get(j).getId());
                                        db.insertQuestion(question.getObjects().get(j), code);
                                        for (int k = 0; k < question.getObjects().get(j).getAnswer_option().size(); k++) {
                                            db.insertAnswer(question.getObjects().get(j).getAnswer_option().get(k));
                                        }
                                    }


                                    Intent i = new Intent(getApplicationContext(), PelodQuestionActivity.class);
                                    i.putExtra("respon", response.toString());
                                    startActivity(i);
                                    //   finish();

                                } else {
                                    dialog(msg.getMessages(), getResources().getString(R.string.Pesan));
                                    if (preferences.getString(Preferences.ID, "").equals("1")) {
                                        editor.putString(Preferences.BIRTH_DAY, "");
                                        editor.putString(Preferences.JENIS_KELAMIN, "");
                                        editor.putString(Preferences.BERAT, "");
                                        editor.putString(Preferences.TINGGI, "");
                                        editor.putString(Preferences.ID, "");
                                        editor.commit();
                                    }
                                }
                            }
                        } catch (JsonSyntaxException e) {
                            dialog(getResources().getString(R.string.KesalahanServer), getResources().getString(R.string.Perhatian));
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                dialog.dismiss();
                Question temp = db.getQuestion(code, getMonth());
                Gson gson = new Gson();
                Log.i("RESULT JSON2", gson.toJson(temp));
                if (temp.getObjects().size() > 0) {
                    Intent i = new Intent(getApplicationContext(), PelodQuestionActivity.class);
                    i.putExtra("respon", gson.toJson(temp));
                    startActivity(i);
                } else {
                    dialog(getResources().getString(R.string.NoConnection), getResources().getString(R.string.Perhatian));
                }

            }
        });
        listdata.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        mRequest.add(listdata);
    }


    private void saveSesiPasien(final String jenis_kelamin, final String tanggal_lahir, final String berat, final String tinggi) {
        progresDialog(false, getResources().getString(R.string.MenyimpanDataPasien));
        StringRequest postlogin = new StringRequest(Request.Method.POST,
                ApiReferences.insertPasien(), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                Log.i("RESULT JSON", response.toString());
                Pasien pasien = Parser.getPasien(response
                        .toString());
                if (pasien.getStatus().equalsIgnoreCase("1")) {
                    editor.putString(Preferences.ID, pasien.getObjects().getId());
                    editor.putString(Preferences.BERAT, pasien.getObjects().getBerat());
                    editor.putString(Preferences.TINGGI, pasien.getObjects().getTinggi());
                    editor.putString(Preferences.JENIS_KELAMIN, pasien.getObjects().getJenis_kelamin());
                    editor.putString(Preferences.BIRTH_DAY, pasien.getObjects().getTanggal_lahir());
                    editor.commit();
                    dialog.dismiss();
                    dialogEmail("Kode : " + pasien.getObjects().getId() + "\nSimpan Code ini untuk pemeriksaan berikutnya.", "Pendaftaran Sesi Pasien Berhasil", pasien.getObjects().getId());
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                dialog.dismiss();
                dialog(getResources().getString(R.string.NoConnection), getResources().getString(R.string.Perhatian));
            }
        }) {

            protected Map<String, String> getParams() {
                HashMap<String, String> params = new HashMap<String, String>();
                params.put("jenis_kelamin", jenis_kelamin);
                params.put("tanggal_lahir", tanggal_lahir);
                params.put("berat", berat);
                params.put("tinggi", tinggi);
                return params;
            }
        };
        postlogin.setRetryPolicy(new DefaultRetryPolicy(
                20000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        mRequest.add(postlogin);
    }


    public void dialogEmail(String content, String msg, final String kode) {
        new MaterialDialog.Builder(this)
                .title(msg)
                .content(content)
                .autoDismiss(false)
                .cancelable(false)
                .positiveText(getResources().getString(R.string.KirimEmail))
                .negativeText(getResources().getString(R.string.tutup))
                .callback(new MaterialDialog.ButtonCallback() {
                    @Override
                    public void onPositive(MaterialDialog dialog) {
                        Intent sendEmail = new Intent(Intent.ACTION_SEND);
                        sendEmail.setType("text/plain");
                        sendEmail.putExtra(Intent.EXTRA_EMAIL, new String[]{""});
                        sendEmail.putExtra(Intent.EXTRA_SUBJECT, "Kode Pasien");
                        sendEmail.putExtra(Intent.EXTRA_TEXT, "Berikut ini adalah kode pasien anda.\nKode : " + kode);
                        startActivity(Intent.createChooser(sendEmail, "Email With"));
                    }

                    @Override
                    public void onNegative(MaterialDialog dialog) {
                        dialog.dismiss();
                        getQuestion();
                    }
                })
                .show();
    }

    private void showDatepickerDialog(final TextView txt) {
        dateHelper.showDatePicker(this, System.currentTimeMillis(), new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                int _year = year;
                int _month = monthOfYear + 1;
                int _day = dayOfMonth;
                String date = _year + "-" + _month + "-" + _day;
                String birthDate = dateHelper.formatDate(date);
                txt.setText(birthDate);
            }
        });
    }


    private void progresDialog(boolean horizontal, String msg) {
        dialog = new MaterialDialog.Builder(this)
                .title(msg)
                .content(getResources().getString(R.string.MohonTunggu))
                .progress(true, 0)
                .progressIndeterminateStyle(horizontal)
                .cancelable(false)
                .show();
    }

    public void dialog(String content, String msg) {
        new MaterialDialog.Builder(this)
                .title(msg)
                .content(content)
                .positiveText(getResources().getString(R.string.MuatUlang))
                .negativeText(getResources().getString(R.string.Kembali))
                .callback(new MaterialDialog.ButtonCallback() {
                    @Override
                    public void onPositive(MaterialDialog dialog) {
                        getQuestion();
                    }

                    @Override
                    public void onNegative(MaterialDialog dialog) {
                        super.onNegative(dialog);
                    }

                })
                .show();
    }


    public void dialog2(String content, String msg) {
        new MaterialDialog.Builder(this)
                .title(msg)
                .content(content)
                .negativeText("OK")
                .callback(new MaterialDialog.ButtonCallback() {
                    @Override
                    public void onPositive(MaterialDialog dialog) {

                    }

                    @Override
                    public void onNegative(MaterialDialog dialog) {
                        super.onNegative(dialog);
                    }

                })
                .show();
    }


}
