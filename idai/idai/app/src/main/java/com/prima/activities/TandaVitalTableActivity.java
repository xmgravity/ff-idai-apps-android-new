package com.prima.activities;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.prima.Preferences;
import com.prima.entities.Question;
import com.prima.helpers.DatabaseHandler;

import java.util.ArrayList;

import prima.test.prima.R;

/**
 * Created by Codelabs on 12/08/2015.
 */
public class TandaVitalTableActivity extends BaseActivity implements View.OnClickListener {
    private ImageView close;
    private ImageView back;
    private Button lanjut;
    TextView title;
    private ListView list;
    private ArrayList<String> listItem = new ArrayList<String>();
    private ArrayList<Integer> answered = new ArrayList<Integer>();
    MaterialDialog dialog;
    private RequestQueue mRequest;
    Question question;
    DatabaseHandler db;

    EditText lajuNadi, lajuNafas, suhu;

    private Button startButton;
    private Button pauseButton;

    private TextView timerSecValueTxt, timerMinValueTxt, timerMicroSecValueTxt, timerValue;

    private long startTime = 0L;

    private Handler customHandler = new Handler();

    long timeInMilliseconds = 0L;
    long timeSwapBuff = 0L;
    long updatedTime = 0L;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }

    @Override
    public void initView() {
        db = new DatabaseHandler(context);
        mRequest = Volley.newRequestQueue(getApplicationContext());
        back = (ImageView) findViewById(R.id.back);
        close = (ImageView) findViewById(R.id.close);
        lanjut = (Button) findViewById(R.id.lanjut);
        title = (TextView) findViewById(R.id.tv_actionbar_title);
        Typeface tf = Typeface.createFromAsset(getApplicationContext().getAssets(), "BREVIA-BLACK.OTF");
        title.setTypeface(tf);
        title.setText(getResources().getString(R.string.TandaVital));



    }


    @Override
    public void setUICallbacks() {
        back.setOnClickListener(this);
        close.setOnClickListener(this);
        lanjut.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        if (v == back) {
            //   Intent i = new Intent(getApplicationContext(), MilestoneRedflagActivity.class);
            //    startActivity(i);
            if (preferences.getString(Preferences.ID, "").equals("1")) {
                editor.putString(Preferences.BIRTH_DAY, "");
                editor.putString(Preferences.JENIS_KELAMIN, "");
                editor.putString(Preferences.BERAT, "");
                editor.putString(Preferences.TINGGI, "");
                editor.putString(Preferences.ID, "");
                editor.commit();
            }
            finish();
        } else if (v == close) {
            //    Intent i = new Intent(getApplicationContext(), MilestoneRedflagActivity.class);
            //    startActivity(i);
            if (preferences.getString(Preferences.ID, "").equals("1")) {
                editor.putString(Preferences.BIRTH_DAY, "");
                editor.putString(Preferences.JENIS_KELAMIN, "");
                editor.putString(Preferences.BERAT, "");
                editor.putString(Preferences.TINGGI, "");
                editor.putString(Preferences.ID, "");
                editor.commit();
            }
            finish();
        } else if (v == lanjut) {
            Intent i = new Intent(getApplicationContext(), TandaVitalResultActivity.class);
            startActivity(i);
            finish();
        }
    }



    @Override
    public int getLayout() {
        return R.layout.activity_tanda_vital_table;
    }

    @Override
    public void updateUI() {

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

    }


    @Override
    public void onBackPressed() {
        //   Intent i = new Intent(getApplicationContext(), MilestoneRedflagActivity.class);
        // startActivity(i);
        if (preferences.getString(Preferences.ID, "").equals("1")) {
            editor.putString(Preferences.BIRTH_DAY, "");
            editor.putString(Preferences.JENIS_KELAMIN, "");
            editor.putString(Preferences.BERAT, "");
            editor.putString(Preferences.TINGGI, "");
            editor.putString(Preferences.ID, "");
            editor.commit();
        }
        finish();
    }


    public void dialog2(String content, String msg) {
        new MaterialDialog.Builder(this)
                .title(msg)
                .content(content)
                .negativeText("OK")
                .callback(new MaterialDialog.ButtonCallback() {
                    @Override
                    public void onPositive(MaterialDialog dialog) {

                    }

                    @Override
                    public void onNegative(MaterialDialog dialog) {
                        super.onNegative(dialog);
                    }

                })
                .show();
    }

}
