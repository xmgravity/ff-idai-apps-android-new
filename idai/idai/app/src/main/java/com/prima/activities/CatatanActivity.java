package com.prima.activities;

import android.content.Intent;
import android.graphics.Typeface;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.JsonSyntaxException;
import com.prima.Preferences;
import com.prima.controllers.ApiReferences;
import com.prima.controllers.Parser;
import com.prima.entities.DaftarCatatan;
import com.prima.entities.Message;
import com.prima.helpers.DatabaseHandler;
import com.prima.helpers.DateHelper;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import prima.test.prima.R;

/**
 * Created by Codelabs on 17/08/2015.
 */
public class CatatanActivity extends BaseActivity implements View.OnClickListener {
    private ImageView close, back;
    ArrayList<LinearLayout> days = new ArrayList<LinearLayout>();
    TextView title, date_time;
    EditText judul_catatan, buat_catatan;
    LinearLayout list, headerContainer;
    String text;
    Button Simpan;
    MaterialDialog dialog;
    private RequestQueue mRequest;
    DatabaseHandler db;
    DateHelper dateHelper;


    @Override
    public void initView() {
        db = new DatabaseHandler(context);
        mRequest = Volley.newRequestQueue(getApplicationContext());
        judul_catatan = (EditText) findViewById(R.id.judul_catatan);
        buat_catatan = (EditText) findViewById(R.id.buat_catatan);
        date_time = (TextView) findViewById(R.id.note_date_time);
        close = (ImageView) findViewById(R.id.close);
        back = (ImageView) findViewById(R.id.back);
        Simpan = (Button) findViewById(R.id.Simpan);
        list = (LinearLayout) findViewById(R.id.list_catatan);
        title = (TextView) findViewById(R.id.tv_actionbar_title);
        headerContainer = (LinearLayout) findViewById(R.id.header_container);

        headerContainer.setVisibility(View.VISIBLE);

        Typeface tf = Typeface.createFromAsset(getApplicationContext().getAssets(), "BREVIA-BLACK.OTF");
        title.setTypeface(tf);
        title.setText(getResources().getString(R.string.Catatan2));

        dateHelper = new DateHelper();



    }

    @Override
    public void setUICallbacks() {
        Simpan.setOnClickListener(this);
        close.setOnClickListener(this);
        back.setOnClickListener(this);
    }



    @Override
    public void onClick(View v) {
        if (v == back) {
            Intent i = new Intent(getApplicationContext(), AgendaActivity.class);
            startActivity(i);
            finish();
        } else if (v == close) {
            Intent i = new Intent(getApplicationContext(), BuatCatatanActivity.class);
            startActivity(i);
            finish();
        }
    }

    @Override
    public int getLayout() {
        return R.layout.activity_catatan_list;
    }

    @Override
    public void updateUI() {

        list.removeAllViewsInLayout();
        days.clear();
        getData();
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

    }

    @Override
    public void onBackPressed() {
        Intent i = new Intent(getBaseContext(), AgendaActivity.class);
        startActivity(i);
        finish();
    }

    private void getData() {
        progresDialog(false, getResources().getString(R.string.MemuatData));
        StringRequest listdata = new StringRequest(Request.Method.GET,
                ApiReferences.getCatatan(),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        dialog.dismiss();
                        try {
                            if (response.equals("[]")) {
                                dialog(getResources().getString(R.string.DataBelumTersedia), getResources().getString(R.string.Perhatian));
                            } else {
                                Message msg = Parser.getMessage(response);
                                if (msg.getStatus().equals("0")) {
                                    dialog(msg.getMessages(), getResources().getString(R.string.Perhatian));
                                } else {
                                    Log.i("RESULT JSON", response.toString());
                                    final DaftarCatatan daftarCatatanResult = Parser.getDaftarCatatan(response);


                                    for (int i = 0; i < daftarCatatanResult.getObjects().size(); i++) {
                                        View catatan = getLayoutInflater().inflate(R.layout.list_item_catatan, null);
                                        TextView title = (TextView) catatan.findViewById(R.id.title);
                                        TextView date_time = (TextView) catatan.findViewById(R.id.note_date_time);
                                        LinearLayout day = (LinearLayout) catatan.findViewById(R.id.day);

                                        title.setId(i);
                                        title.setText(daftarCatatanResult.getObjects().get(i).getTitle());
                                        date_time.setId(i);
                                        date_time.setText(daftarCatatanResult.getObjects().get(i).getDate_time());

                                        catatan.setId(i);

                                        Calendar calendar = Calendar.getInstance();
                                        System.out.println("date_time "+calendar.getTime());
                                        SimpleDateFormat df = new SimpleDateFormat("yyyyMMdd_HHmmss");
                                        System.out.println("date_time " +df.format(calendar.getTime()));



                                        list.addView(catatan);

                                    }

                                }
                            }
                        } catch (JsonSyntaxException e) {
                            dialog(getResources().getString(R.string.KesalahanServer), getResources().getString(R.string.Perhatian));
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                NetworkResponse response = error.networkResponse;
                if (error instanceof ServerError && response != null) {
                    try {
                        String res = new String(response.data,
                                HttpHeaderParser.parseCharset(response.headers, "utf-8"));
                        // Now you can use any deserializer to make sense of data
                        JSONObject obj = new JSONObject(res);
                        dialog(res.toString(), getResources().getString(R.string.Perhatian));
                    } catch (UnsupportedEncodingException e1) {
                        // Couldn't properly decode data to string
                        e1.printStackTrace();
                    } catch (JSONException e2) {
                        // returned data is not JSONObject?
                        e2.printStackTrace();
                    }
                }
            }
        }) {

            public Map<String, String> getHeaders() {
                Map<String, String>  params = new HashMap<String, String>();
                params.put("token", preferences.getString(Preferences.ACCESS_TOKEN, ""));


                return params;
            }
        };

        listdata.setRetryPolicy(new DefaultRetryPolicy(
                20000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        mRequest.add(listdata);
    }






    public void dialog(String content, String msg) {
        new MaterialDialog.Builder(this)
                .title(msg)
                .content(content)
                .positiveText("Ok")
                .callback(new MaterialDialog.ButtonCallback() {
                    @Override
                    public void onPositive(MaterialDialog dialog) {
                    }

                })
                .show();
    }

    private void progresDialog(boolean horizontal, String msg) {
        dialog = new MaterialDialog.Builder(this)
                .title(msg)
                .content(getResources().getString(R.string.MohonTunggu))
                .progress(true, 0)
                .progressIndeterminateStyle(horizontal)
                .cancelable(false)
                .show();
    }
    public static String convertStreamToString(InputStream is) throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();
        String line = null;
        while ((line = reader.readLine()) != null) {
            sb.append(line).append("\n");
        }
        reader.close();
        return sb.toString();
    }

    public static String getStringFromFile (String filePath) throws Exception {
        File fl = new File(filePath);
        FileInputStream fin = new FileInputStream(fl);
        String ret = convertStreamToString(fin);
        //Make sure you close all streams.
        fin.close();
        return ret;
    }
}