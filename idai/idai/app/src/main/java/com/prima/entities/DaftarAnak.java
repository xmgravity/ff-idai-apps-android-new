package com.prima.entities;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Codelabs on 09/09/2015.
 */
public class DaftarAnak extends Message implements Serializable {
    private ArrayList<Child> data;
    public ArrayList<Child> getObjects() {
        return data;
    }
    public void setObjects(ArrayList<Child> objects) {
        this.data = objects;
    }


    public class GetChild extends Message {
        private Child objects;

        public Child getObjects() {
            return objects;
        }

        public void setObjects(Child objects) {
            this.objects = objects;
        }
    }

    public class Child {
        private String id;
        private String name;
        private String dob;
        private String gender;
        private String profile_picture;
        private String height;
        private String weight;


        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getDob() {
            return dob;
        }

        public void setDob(String dob) {
            this.dob = dob;
        }

        public String getTinggi() { return height; }

        public void setTinggi(String height) { this.height = height; }

        public String getBerat() { return weight; }

        public void setBerat(String weight) { this.weight = weight;}

        public String getGender() {
            return gender;
        }

        public void setGender(String gender) {
            this.gender = gender;
        }

        public String getProfilePicture() {
            return profile_picture;
        }

        public void setProfilePicture(String profile_picture) {
            this.profile_picture = profile_picture;
        }

    }
}