package com.prima.activities;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.NotificationCompat;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;

import com.crashlytics.android.Crashlytics;
import io.fabric.sdk.android.Fabric;
import org.joda.time.DateTime;
import org.joda.time.Minutes;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import com.prima.MainActivity;
import com.prima.Preferences;
import com.prima.adapters.PagerAdapter;
import com.prima.entities.Agenda;
import com.prima.entities.Notif;
import com.prima.fragments.LoginFragment;
import com.prima.fragments.SplashFragment;
import com.prima.fragments.SplashSelamatDatangFragment;
import com.prima.helpers.DatabaseHandler;
import prima.test.prima.R;

/**
 * Created by Codelabs on 30/09/2015.
 */
public class SplashPagerActivity extends BaseActivity implements View.OnClickListener {
    private Context mContext;
    private ViewPager mPager;
    private PagerAdapter mPagerAdapter;
    PageListener pageListener;
    List<Fragment> fragmentList = new ArrayList<Fragment>();
    private DatabaseHandler db;
    Agenda agenda;


    public List<Fragment> getFragmentList() {
        return fragmentList;
    }

    public void setFragmentList(List<Fragment> fragmentList) {
        this.fragmentList = fragmentList;
    }

    static LinearLayout indikatorWrapper;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        mContext = getApplicationContext();
        db = new DatabaseHandler(getApplicationContext());


        final long timeInterval = 1000 * 60;
        Runnable runnable = new Runnable() {

            public void run() {
                while (true) {
                    agenda = db.getAllAgenda();
                    Log.e("Log", "Running");
                    Calendar c = Calendar.getInstance();
                    System.out.println("Current time => " + c.getTime());

                    SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
                    SimpleDateFormat df2 = new SimpleDateFormat("HH:mm");
                    String formattedDate = df.format(c.getTime());
                    String formattedDate2 = df2.format(c.getTime());
                    Log.e("Log", "" + formattedDate + " " + formattedDate2);

                    for (int i = 0; i < agenda.getObjects().size(); i++) {
                        String waktu1 = formattedDate + " " + formattedDate2;
                        String waktu2 = agenda.getObjects().get(i).getTanggal_kunjungan() + " " + agenda.getObjects().get(i).getJam_mulai().substring(0, 5);

                        if (getMinutes(waktu1, waktu2) == Integer.valueOf(agenda.getObjects().get(i).getPengingat())) {
                            NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                            Intent i1 = new Intent(getApplicationContext(), NotifikasiActivity.class);
                            PendingIntent contentIntent1 = PendingIntent.getActivity(getApplicationContext(), 1, i1, PendingIntent.FLAG_UPDATE_CURRENT);


                            Notif temp = new Notif();
                            temp.setId_agenda(agenda.getObjects().get(i).getId_agenda());
                            temp.setIs_read("0");
                            db.insertNotif(temp);

                            if (agenda.getObjects().get(i).getGetar().equals("0")
                                    && agenda.getObjects().get(i).getSuara().equals("0")) {
                                NotificationCompat.Builder mBuilder1 =
                                        new NotificationCompat.Builder(getApplicationContext())
                                                .setSmallIcon(R.mipmap.ic_launcher)
                                                .setAutoCancel(true)
                                                .setLights(Color.RED, 3000, 3000)
                                                .setContentTitle("Event Kunjungan")
                                                .setContentText("Konsultasi " + agenda.getObjects().get(i).getNama_pasien() + "\nTanggal : " + agenda.getObjects().get(i).getTanggal_kunjungan() + "\nPukul :" + agenda.getObjects().get(i).getJam_mulai() + "-" + agenda.getObjects().get(i).getJam_akhir());
                                mBuilder1.setContentIntent(contentIntent1);
                                mNotificationManager.notify(1, mBuilder1.build());
                            } else if (agenda.getObjects().get(i).getGetar().equals("1")
                                    && agenda.getObjects().get(i).getSuara().equals("0")) {
                                NotificationCompat.Builder mBuilder1 =
                                        new NotificationCompat.Builder(getApplicationContext())
                                                .setSmallIcon(R.mipmap.ic_launcher)
                                                .setAutoCancel(true)
                                                .setVibrate(new long[]{1000, 1000, 1000, 1000, 1000})
                                                .setLights(Color.RED, 3000, 3000)
                                                .setContentTitle("Event Kunjungan")
                                                .setContentText("Konsultasi " + agenda.getObjects().get(i).getNama_pasien() + "\nTanggal : " + agenda.getObjects().get(i).getTanggal_kunjungan() + "\nPukul :" + agenda.getObjects().get(i).getJam_mulai() + "-" + agenda.getObjects().get(i).getJam_akhir());
                                mBuilder1.setContentIntent(contentIntent1);
                                mNotificationManager.notify(1, mBuilder1.build());
                            } else if (agenda.getObjects().get(i).getGetar().equals("0")
                                    && agenda.getObjects().get(i).getSuara().equals("1")) {
                                NotificationCompat.Builder mBuilder1 =
                                        new NotificationCompat.Builder(getApplicationContext())
                                                .setSmallIcon(R.mipmap.ic_launcher)
                                                .setAutoCancel(true)
                                                .setLights(Color.RED, 3000, 3000)
                                                .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                                                .setContentTitle("Event Kunjungan")
                                                .setContentText("Konsultasi " + agenda.getObjects().get(i).getNama_pasien() + "\nTanggal : " + agenda.getObjects().get(i).getTanggal_kunjungan() + "\nPukul :" + agenda.getObjects().get(i).getJam_mulai() + "-" + agenda.getObjects().get(i).getJam_akhir());
                                mBuilder1.setContentIntent(contentIntent1);
                                mNotificationManager.notify(1, mBuilder1.build());
                            } else if (agenda.getObjects().get(i).getGetar().equals("1")
                                    && agenda.getObjects().get(i).getSuara().equals("1")) {
                                NotificationCompat.Builder mBuilder1 =
                                        new NotificationCompat.Builder(getApplicationContext())
                                                .setSmallIcon(R.mipmap.ic_launcher)
                                                .setAutoCancel(true)
                                                .setVibrate(new long[]{1000, 1000, 1000, 1000, 1000})
                                                .setLights(Color.RED, 3000, 3000)
                                                .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                                                .setContentTitle("Event Kunjungan")
                                                .setContentText("Konsultasi " + agenda.getObjects().get(i).getNama_pasien() + "\nTanggal : " + agenda.getObjects().get(i).getTanggal_kunjungan() + "\nPukul :" + agenda.getObjects().get(i).getJam_mulai() + "-" + agenda.getObjects().get(i).getJam_akhir());
                                mBuilder1.setContentIntent(contentIntent1);
                                mNotificationManager.notify(1, mBuilder1.build());
                            } else {
                                NotificationCompat.Builder mBuilder1 =
                                        new NotificationCompat.Builder(getApplicationContext())
                                                .setSmallIcon(R.mipmap.ic_launcher)
                                                .setAutoCancel(true)
                                                .setVibrate(new long[]{1000, 1000, 1000, 1000, 1000})
                                                .setLights(Color.RED, 3000, 3000)
                                                .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                                                .setContentTitle("Event Kunjungan")
                                                .setContentText("Konsultasi " + agenda.getObjects().get(i).getNama_pasien() + "\nTanggal : " + agenda.getObjects().get(i).getTanggal_kunjungan() + "\nPukul :" + agenda.getObjects().get(i).getJam_mulai() + "-" + agenda.getObjects().get(i).getJam_akhir());
                                mBuilder1.setContentIntent(contentIntent1);
                                mNotificationManager.notify(1, mBuilder1.build());
                            }

                        }


                        Log.e("Log", "" + getMinutes(waktu1, waktu2));
                    }
                    try {

                        Thread.sleep(timeInterval);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        };

        Thread thread = new Thread(runnable);
        thread.start();


        if (preferences.getString(Preferences.ISLOGIN, "").equals("true")) {
            Intent i = new Intent(context, MainActivity.class);
            startActivity(i);
            finish();
        }

    }

    public int getMinutes(String waktu, String waktu2) {
        final DateTimeFormatter formatter1 = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm");
        final DateTime dateTime1 = formatter1.parseDateTime(waktu);
        final DateTime dateTime2 = formatter1.parseDateTime(waktu2);
        Minutes minutes = Minutes.minutesBetween(dateTime1, dateTime2);
        return minutes.getMinutes();
    }

    @Override
    public void initView() {
        indikatorWrapper = (LinearLayout) findViewById(R.id.indikator_pager);
        mPager = (ViewPager) findViewById(R.id.pager);
        pageListener = new PageListener();
        mPager.setOnPageChangeListener(pageListener);

        indikatorWrapper.removeAllViewsInLayout();
        fragmentList.clear();
        fragmentList.add(new SplashFragment());
        fragmentList.add(new SplashSelamatDatangFragment());
        fragmentList.add(new LoginFragment());
        Bundle bn = getIntent().getExtras();

        //  Toast.makeText(getApplicationContext(), String.valueOf(data.size()), Toast.LENGTH_LONG).show();
        for (int i = 0; i < fragmentList.size(); i++) {

            View child = getLayoutInflater().inflate(R.layout.indikator_pager, null);
            View child2 = getLayoutInflater().inflate(R.layout.indikator_pager_blue, null);
            if (i == 0) {
                indikatorWrapper.addView(child2);
            } else {
                indikatorWrapper.addView(child);
            }
        }
        setupFrae();

    }

    @Override
    public void setUICallbacks() {
    }


    @Override
    public void onClick(View v) {

    }


    @Override
    public int getLayout() {
        return R.layout.activity_splash_slide;
    }

    @Override
    public void updateUI() {


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

    }

    private class PageListener extends ViewPager.SimpleOnPageChangeListener {
        public void onPageSelected(int position) {
            indikatorWrapper.removeAllViewsInLayout();
            for (int i = 0; i < fragmentList.size(); i++) {
                View child = getLayoutInflater().inflate(R.layout.indikator_pager, null);
                View child2 = getLayoutInflater().inflate(R.layout.indikator_pager_blue, null);
                if (i == position) {
                    indikatorWrapper.addView(child2);
                } else {
                    indikatorWrapper.addView(child);
                }
            }

        }
    }


    @Override
    public void onBackPressed() {
        finish();
    }

    private void setupFrae() {
        mPagerAdapter = new PagerAdapter(getSupportFragmentManager(),
                SplashPagerActivity.this, fragmentList);
        mPager.setAdapter(mPagerAdapter);

    }
}
