package com.prima.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import com.prima.entities.Alamat;
import prima.test.prima.R;

/**
 * Created by Codelabs on 07/08/2015.
 */
public class AdapterListAlamat extends BaseAdapter {
    private ArrayList<Alamat.ListItemAlamat> _data;
    Context _context;

    public AdapterListAlamat(Alamat data,
                             Context context) {
        _data = data.getResults();
        _context = context;
    }

    @Override
    public int getCount() {

        return _data.size();
    }

    @Override
    public Object getItem(int position) {

        return _data.get(position);
    }

    @Override
    public long getItemId(int position) {

        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        if (v == null) {
            LayoutInflater infLayout = (LayoutInflater) _context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = infLayout.inflate(R.layout.list_item_text, null);
        }
        TextView textItem = (TextView) v.findViewById(R.id.alamat);
        textItem.setText(_data.get(position).getFormatted_address());
        return v;
    }
}
