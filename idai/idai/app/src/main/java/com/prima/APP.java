/**
 * Base Application
 * @author Codelabs
 * 8 September 2014
 */
package com.prima;

import android.app.Application;
import android.content.Context;
import android.util.Log;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;




public class APP extends Application {

	public static String TAG = Config.APP_NAME;
	private static Context mContext;

	@Override
	public void onCreate() {
		super.onCreate();
		mContext = getApplicationContext();
		// Set Application Mode
		Config.setMode(Config.MODE.DEVELOPMENT);
		setImageLoaderConfig();// Init ImageLoader
//		setFacebookLoginConfig(); // Init SimpleFacebook
	}

	public static void log(String message) {
		if (Config.isDevelopment) {
			Log.i(Config.APP_NAME, message);
		}
	}

	public static DisplayImageOptions imageOptions() {
		DisplayImageOptions options = new DisplayImageOptions.Builder()
				.cacheInMemory(true)
				.cacheOnDisc(true).build();
		return options;
	}

	public static Context getContext() {
		return mContext;
	}

	private void setImageLoaderConfig() {
		ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(
				getApplicationContext())
				.defaultDisplayImageOptions(imageOptions())
				.build();
		ImageLoader.getInstance().init(config);
	}
	
//	private void setFacebookLoginConfig(){
//		Permission[] permissions = new Permission[] {
//			    Permission.USER_PHOTOS,
//			    Permission.EMAIL,
//			    Permission.PUBLIC_PROFILE,
//			    Permission.USER_BIRTHDAY
//			};
//		SimpleFacebookConfiguration configuration = new SimpleFacebookConfiguration.Builder()
//				.setAppId(getString(R.string.fb_app_id))
//				.setNamespace(getString(R.string.fb_namespace))
//				.setPermissions(permissions).build();
//		SimpleFacebook.setConfiguration(configuration);
//	}
	
}
