package com.prima.model;

import android.content.Context;

import com.prima.entities.Pasien;

public class PasienModel extends BaseModel<Pasien> {
    public PasienModel(Context context) {
        super(context, Pasien.class);
    }
}
