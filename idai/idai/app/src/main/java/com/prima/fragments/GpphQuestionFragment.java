package com.prima.fragments;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

import com.prima.activities.GpphQuestionActivity;
import com.prima.activities.GpphResultActivity;
import com.prima.adapters.GpphListAdapter;
import com.prima.entities.Question;
import prima.test.prima.R;

/**
 * Created by Codelabs on 18/08/2015.
 */
public class GpphQuestionFragment extends BaseFragment implements View.OnClickListener {

    private Context mContext;
    public static final String ARG_PAGE = "page 1";

    private ListView list;
    private ViewPager mPager;
    String selected = "";
    int indeks;
    List<Fragment> fragmentList = new ArrayList<Fragment>();
    Question.Kuesioner kuesioner;
    private ArrayList<String> ansewer = new ArrayList<String>();
    int totalScore;

    @SuppressLint("ValidFragment")
    public GpphQuestionFragment() {}
    @SuppressLint("ValidFragment")
    public GpphQuestionFragment(Question.Kuesioner kuesioner, int indeks) {
        this.kuesioner = kuesioner;
        this.indeks = indeks;
    }

    @Override
    public void initView(View view) {
        list = (ListView) view.findViewById(R.id.listView);
        mPager = (ViewPager) getActivity().findViewById(R.id.pager);
        mContext = getActivity();
        final Activity act = getActivity();
        //    ProgressBar progres = (ProgressBar) view.findViewById(R.id.progres);
        // progres.setVisibility(View.GONE);


        if (act instanceof GpphQuestionActivity) {
            fragmentList = ((GpphQuestionActivity) act).getFragmentList();
            ansewer = ((GpphQuestionActivity) act).getAnsewer();
        }


        final GpphListAdapter adapterList = new GpphListAdapter(
                kuesioner.getAnswer_option(), mContext, selected);
        list.setAdapter(adapterList);


        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                ansewer.set(indeks, kuesioner.getAnswer_option().get(position).getScore());
                selected = String.valueOf(kuesioner.getAnswer_option().get(position).getScore());
                adapterList.setSelected(selected);
                adapterList.notifyDataSetChanged();
                int current = mPager.getCurrentItem();
                if (current != fragmentList.size() - 1) {
                    mPager.setCurrentItem(current + 1, true);
                } else {
                    int score = 0;
                    for (int j = 0; j < ansewer.size(); j++) {
                        if (ansewer.get(j).replace(".00", "").equals("")) {
                            score = score + 0;
                        } else {
                            score = score + Integer.valueOf(ansewer.get(j).replace(".00", ""));
                        }
                    }
                    Intent i = new Intent(mContext, GpphResultActivity.class);
                    i.putExtra("score", score);

                    if (act instanceof GpphQuestionActivity) {
                        totalScore = ((GpphQuestionActivity) act).getMax_score();
                    }
                    i.putExtra("max_score",totalScore);
                    startActivity(i);
                    getActivity().finish();
                }

            }
        });
    }

    @Override
    public void setUICallbacks() {

    }

    @Override
    public void updateUI() {

    }

    @Override
    public String getPageTitle() {
        return "Account Summary";
    }


    @Override
    public int getFragmentLayout() {
        return R.layout.list_view;
    }

    @Override
    public void onClick(View v) {

    }

}