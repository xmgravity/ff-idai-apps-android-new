package com.prima.activities;

import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.davemorrissey.labs.subscaleview.SubsamplingScaleImageView;

import com.prima.MainActivity;

import prima.test.prima.R;


/**
 * Created by Codelabs on 05/08/2015.
 */
public class ImunisasiActivity extends BaseActivity implements View.OnClickListener {
    private SubsamplingScaleImageView img;
    private ImageView back;
    private Button bagikan;
    private ImageView close;
    private TextView title;

    @Override
    public void initView() {
        img = (SubsamplingScaleImageView) findViewById(R.id.img);
       // img.setImage(ImageSource.resource(R.mipmap.alarmimunegraph));
        close = (ImageView) findViewById(R.id.close);
        close.setVisibility(View.GONE);
        bagikan = (Button) findViewById(R.id.bagikan);
        back = (ImageView) findViewById(R.id.back);

        title = (TextView) findViewById(R.id.tv_actionbar_title);
        Typeface tf = Typeface.createFromAsset(getApplicationContext().getAssets(), "BREVIA-BLACK.OTF");
        title.setTypeface(tf);
        title.setText(getResources().getString(R.string.jadwalImunisasi));




    }


    @Override
    public void setUICallbacks() {
        bagikan.setOnClickListener(this);
        back.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if (v == bagikan) {
            Intent sendEmail = new Intent(Intent.ACTION_SEND);
            sendEmail.setType("jpeg/image");
            sendEmail.putExtra(Intent.EXTRA_EMAIL, new String[]{""});
            sendEmail.putExtra(Intent.EXTRA_SUBJECT, "Kalender Imunisasi");
            sendEmail.putExtra(Intent.EXTRA_TEXT, "Berikut ini adalah kalender imunisasi");
            sendEmail.putExtra(Intent.EXTRA_STREAM, Uri.parse("file:///sdcard/tutorial/Image.jpg"));
            startActivity(Intent.createChooser(sendEmail, "Email With"));
        } else if (v == back) {
            Intent i = new Intent(ImunisasiActivity.this, MainActivity.class);
            startActivity(i);
            finish();
        }
    }


    @Override
    public int getLayout() {
        return R.layout.activity_imunisasi_idai;
    }

    @Override
    public void updateUI() {

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

    }


    @Override
    public void onBackPressed() {
        Intent i = new Intent(ImunisasiActivity.this, MainActivity.class);
        startActivity(i);
        finish();
    }


}
