package com.prima.adapters;

import android.app.DatePickerDialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;

import java.util.ArrayList;

import com.prima.helpers.DateHelper;
import prima.test.prima.R;

/**
 * Created by Codelabs on 05/09/2015.
 */
public class AdapterImunisasiInsert extends BaseAdapter {
    private ArrayList<String> _data;
    Context _context;
    DateHelper dateHelper;

    public AdapterImunisasiInsert(ArrayList<String> data,
                                  Context context) {
        _data = data;
        _context = context;
    }

    @Override
    public int getCount() {

        return _data.size();
    }

    @Override
    public Object getItem(int position) {

        return _data.get(position);
    }

    @Override
    public long getItemId(int position) {

        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        if (v == null) {
            LayoutInflater infLayout = (LayoutInflater) _context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = infLayout.inflate(R.layout.list_item_input_imunisasi, null);
        }

        final EditText tanggal =  (EditText) v.findViewById(R.id.tanggal);
        Spinner jml_vaksin = (Spinner) v.findViewById(R.id.jml_vaksin);

        dateHelper = new DateHelper();

        tanggal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDatepickerDialog(tanggal);
            }
        });


        return v;
    }



    void showDatepickerDialog(final EditText txt) {
        dateHelper.showDatePicker(_context, System.currentTimeMillis(), new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                int _year = year;
                int _month = monthOfYear + 1;
                int _day = dayOfMonth;
                String date = _year + "-" + _month + "-" + _day;
                String birthDate = dateHelper.formatDate(date);
                txt.setText(birthDate);
            }
        });
    }
}
