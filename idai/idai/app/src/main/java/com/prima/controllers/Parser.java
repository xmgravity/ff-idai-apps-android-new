package com.prima.controllers;

import com.google.gson.Gson;
import com.prima.entities.Agenda;
import com.prima.entities.Alamat;
import com.prima.entities.AllRedflag;
import com.prima.entities.Artikel;
import com.prima.entities.Catatan;
import com.prima.entities.Child;
import com.prima.entities.DaftarAnak;
import com.prima.entities.DaftarCatatan;
import com.prima.entities.Dokter;
import com.prima.entities.Grafik;
import com.prima.entities.JadwalPraktek;
import com.prima.entities.JadwalPraktekSingle;
import com.prima.entities.KalenderImunisasi;
import com.prima.entities.Login;
import com.prima.entities.MateriEdukasi;
import com.prima.entities.MenuAge;
import com.prima.entities.Message;
import com.prima.entities.OrangTua;
import com.prima.entities.Pasien;
import com.prima.entities.Pdf;
import com.prima.entities.Pubertas;
import com.prima.entities.Question;
import com.prima.entities.TanyaJawab;
import com.prima.entities.VaksinPasien;

/**
 * Created by Codelabs on 20/08/2015.
 */
public class Parser {
    public static Alamat getParserAlamat(String respon) {
        System.out.println(respon);
        Gson gs = new Gson();
        Alamat model = gs.fromJson(respon, Alamat.class);
        return model;
    }

    public static Grafik getGrafik(String respon) {
        System.out.println(respon);
        Gson gs = new Gson();
        Grafik model = gs.fromJson(respon, Grafik.class);
        return model;
    }

    public static Pasien getPasien(String respon) {
        System.out.println(respon);
        Gson gs = new Gson();
        Pasien model = gs.fromJson(respon, Pasien.class);
        return model;
    }

    public static Pubertas getPubertas(String respon) {
        System.out.println(respon);
        Gson gs = new Gson();
        Pubertas model = gs.fromJson(respon, Pubertas.class);
        return model;
    }


    public static Question getQuestion(String respon) {
        System.out.println(respon);
        Gson gs = new Gson();
        Question model = gs.fromJson(respon, Question.class);
        return model;
    }

    public static Message getMessage(String respon) {
        Gson gs = new Gson();
        Message model = gs.fromJson(respon, Message.class);
        return model;
    }


    public static Artikel getArtikel(String respon) {
        Gson gs = new Gson();
        Artikel model = gs.fromJson(respon, Artikel.class);
        return model;
    }

    public static KalenderImunisasi getKalenderImunisasi(String respon) {
        Gson gs = new Gson();
        KalenderImunisasi model = gs.fromJson(respon, KalenderImunisasi.class);
        return model;
    }

    public static MenuAge getMenuAge(String respon) {
        Gson gs = new Gson();
        MenuAge model = gs.fromJson(respon, MenuAge.class);
        return model;
    }

    public static MateriEdukasi getMateriEdukasi(String respon) {
        Gson gs = new Gson();
        MateriEdukasi model = gs.fromJson(respon, MateriEdukasi.class);
        return model;
    }


    public static Pdf getPdf(String respon) {
        Gson gs = new Gson();
        Pdf model = gs.fromJson(respon, Pdf.class);
        return model;
    }


    public static JadwalPraktek getJadwal(String respon) {
        Gson gs = new Gson();
        JadwalPraktek model = gs.fromJson(respon, JadwalPraktek.class);
        return model;
    }

    public static JadwalPraktekSingle getJadwalSingle(String respon) {
        Gson gs = new Gson();
        JadwalPraktekSingle model = gs.fromJson(respon, JadwalPraktekSingle.class);
        return model;
    }

    public static DaftarAnak getDaftarAnak(String respon) {
        Gson gs = new Gson();
        DaftarAnak model = gs.fromJson(respon, DaftarAnak.class);
        return model;
    }

    public static VaksinPasien getDaftarVaksinAnak(String respon) {
        Gson gs = new Gson();
        VaksinPasien model = gs.fromJson(respon, VaksinPasien.class);
        return model;
    }

    public static JadwalPraktek.GetPraktek getJadwalPraktek(String respon) {
        Gson gs = new Gson();
        JadwalPraktek.GetPraktek model = gs.fromJson(respon, JadwalPraktek.GetPraktek.class);
        return model;
    }

    public static JadwalPraktek.Praktek getOnlyJadwal(String respon) {
        Gson gs = new Gson();
        JadwalPraktek.Praktek model = gs.fromJson(respon, JadwalPraktek.Praktek.class);
        return model;
    }

    public static Agenda getAgenda(String respon) {
        Gson gs = new Gson();
        Agenda model = gs.fromJson(respon, Agenda.class);
        return model;
    }

    public static Agenda.Agendas getAgendas(String respon) {
        Gson gs = new Gson();
        Agenda.Agendas model = gs.fromJson(respon, Agenda.Agendas.class);
        return model;
    }

    public static TanyaJawab getTanyaJawab(String respon) {
        Gson gs = new Gson();
        TanyaJawab model = gs.fromJson(respon, TanyaJawab.class);
        return model;
    }


    public static AllRedflag getAllRedFlags(String respon) {
        Gson gs = new Gson();
        AllRedflag model = gs.fromJson(respon, AllRedflag.class);
        return model;
    }


    public static Login getLogin(String respon) {
        Gson gs = new Gson();
        Login model = gs.fromJson(respon, Login.class);
        return model;
    }

    public static Dokter getRegister(String respon) {
        Gson gs = new Gson();
        Dokter model = gs.fromJson(respon, Dokter.class);
        return model;
    }

    public static OrangTua getOrangTua(String respon) {
        Gson gs = new Gson();
        OrangTua model = gs.fromJson(respon, OrangTua.class);
        return model;
    }

    public static Child getChild(String respon) {
        Gson gs = new Gson();
        Child model = gs.fromJson(respon, Child.class);
        return model;
    }

    public static Catatan getCatatan(String respon) {
        Gson gs = new Gson();
        Catatan model = gs.fromJson(respon, Catatan.class);
        return model;
    }

    public static DaftarCatatan getDaftarCatatan(String respon) {
        Gson gs = new Gson();
        DaftarCatatan model = gs.fromJson(respon, DaftarCatatan.class);
        return model;
    }
}
