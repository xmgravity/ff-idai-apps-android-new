package com.prima.activities;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.JsonSyntaxException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import com.prima.MainActivity;
import com.prima.Preferences;
import com.prima.controllers.ApiReferences;
import com.prima.controllers.Parser;
import com.prima.entities.Message;
import com.prima.entities.Pasien;
import com.prima.helpers.DateHelper;
import prima.test.prima.R;

/**
 * Created by Codelabs on 05/09/2015.
 */
public class ImunisasiInsertDataActivity extends BaseActivity implements View.OnClickListener {
    private ImageView back;
    private ImageView close;
    private TextView title;
    private MaterialDialog dialog;
    // private ListView list;


    private CheckBox ck1,
            ck2,
            ck3,
            ck4,
            ck5,
            ck6,
            ck7,
            ck8,
            ck9,
            ck10,
            ck11,
            ck12,
            ck13,
            ck14;

    private TextView edt1,
            edt2,
            edt3,
            edt4,
            edt5,
            edt6,
            edt7,
            edt8,
            edt9,
            edt10,
            edt11,
            edt12,
            edt13,
            edt14;


    private Spinner spin1,
            spin2,
            spin4,
            spin5,
            spin6,
            spin7,
            spin9,
            spin10;
    DateHelper dateHelper = new DateHelper();
    String[] number = {"0", "1", "2", "3", "4", "5", "6", "7"};
    ArrayAdapter<String> adapterArray;
    ArrayList<Selected> data;
    Button lanjut;
    private RequestQueue mRequest;

    @Override
    public void initView() {
        close = (ImageView) findViewById(R.id.close);
        back = (ImageView) findViewById(R.id.back);
        close.setVisibility(View.GONE);
        //checkbox
        mRequest = Volley.newRequestQueue(getApplicationContext());
        ck1 = (CheckBox) findViewById(R.id.ck1);
        ck2 = (CheckBox) findViewById(R.id.ck2);
        ck3 = (CheckBox) findViewById(R.id.ck3);
        ck4 = (CheckBox) findViewById(R.id.ck4);
        ck5 = (CheckBox) findViewById(R.id.ck5);
        ck6 = (CheckBox) findViewById(R.id.ck6);
        ck7 = (CheckBox) findViewById(R.id.ck7);
        ck8 = (CheckBox) findViewById(R.id.ck8);
        ck9 = (CheckBox) findViewById(R.id.ck9);
        ck10 = (CheckBox) findViewById(R.id.ck10);
        ck11 = (CheckBox) findViewById(R.id.ck11);
        ck12 = (CheckBox) findViewById(R.id.ck12);
        ck13 = (CheckBox) findViewById(R.id.ck13);
        ck14 = (CheckBox) findViewById(R.id.ck14);
        //Button

        edt1 = (TextView) findViewById(R.id.edt1);
        edt2 = (TextView) findViewById(R.id.edt2);
        edt3 = (TextView) findViewById(R.id.edt3);
        edt4 = (TextView) findViewById(R.id.edt4);
        edt5 = (TextView) findViewById(R.id.edt5);
        edt6 = (TextView) findViewById(R.id.edt6);
        edt7 = (TextView) findViewById(R.id.edt7);
        edt8 = (TextView) findViewById(R.id.edt8);
        edt9 = (TextView) findViewById(R.id.edt9);
        edt10 = (TextView) findViewById(R.id.edt10);
        edt11 = (TextView) findViewById(R.id.edt11);
        edt12 = (TextView) findViewById(R.id.edt12);
        edt13 = (TextView) findViewById(R.id.edt13);
        edt14 = (TextView) findViewById(R.id.edt14);

        //Spinner
        spin1 = (Spinner) findViewById(R.id.spin1);
        spin2 = (Spinner) findViewById(R.id.spin2);
        spin4 = (Spinner) findViewById(R.id.spin4);
        spin5 = (Spinner) findViewById(R.id.spin5);
        spin6 = (Spinner) findViewById(R.id.spin6);
        spin7 = (Spinner) findViewById(R.id.spin7);
        spin9 = (Spinner) findViewById(R.id.spin9);
        spin10 = (Spinner) findViewById(R.id.spin10);

        lanjut = (Button) findViewById(R.id.lanjut);

        adapterArray = new ArrayAdapter<String>(this, R.layout.spinner_item, number);
        adapterArray.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spin1.setAdapter(adapterArray);
        spin2.setAdapter(adapterArray);
        spin4.setAdapter(adapterArray);
        spin5.setAdapter(adapterArray);
        spin6.setAdapter(adapterArray);
        spin7.setAdapter(adapterArray);
        spin9.setAdapter(adapterArray);
        spin10.setAdapter(adapterArray);


        close = (ImageView) findViewById(R.id.close);
        close.setVisibility(View.GONE);
        back = (ImageView) findViewById(R.id.back);
        //  list = (ListView) findViewById(R.id.listView);
        title = (TextView) findViewById(R.id.tv_actionbar_title);
        Typeface tf = Typeface.createFromAsset(getApplicationContext().getAssets(), "BREVIA-BLACK.OTF");
        title.setTypeface(tf);
        title.setText("Imunisasi IDAI");


    }


    @Override
    public void setUICallbacks() {
        back.setOnClickListener(this);
        edt1.setOnClickListener(this);
        edt2.setOnClickListener(this);
        edt3.setOnClickListener(this);
        edt4.setOnClickListener(this);
        edt5.setOnClickListener(this);
        edt6.setOnClickListener(this);
        edt7.setOnClickListener(this);
        edt8.setOnClickListener(this);
        edt9.setOnClickListener(this);
        edt10.setOnClickListener(this);
        edt11.setOnClickListener(this);
        edt12.setOnClickListener(this);
        edt13.setOnClickListener(this);
        edt14.setOnClickListener(this);
        lanjut.setOnClickListener(this);
        back.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if (v == back) {
            Intent i = new Intent(ImunisasiInsertDataActivity.this, MainActivity.class);
            startActivity(i);
            finish();
        } else if (v == edt1) {
            showDatepickerDialog(edt1);
        } else if (v == edt2) {
            showDatepickerDialog(edt2);
        } else if (v == edt3) {
            showDatepickerDialog(edt3);
        } else if (v == edt4) {
            showDatepickerDialog(edt4);
        } else if (v == edt5) {
            showDatepickerDialog(edt5);
        } else if (v == edt6) {
            showDatepickerDialog(edt6);
        } else if (v == edt7) {
            showDatepickerDialog(edt7);
        } else if (v == edt8) {
            showDatepickerDialog(edt8);
        } else if (v == edt9) {
            showDatepickerDialog(edt9);
        } else if (v == edt10) {
            showDatepickerDialog(edt10);
        } else if (v == edt11) {
            showDatepickerDialog(edt11);
        } else if (v == edt12) {
            showDatepickerDialog(edt12);
        } else if (v == edt13) {
            showDatepickerDialog(edt13);
        } else if (v == edt14) {
            showDatepickerDialog(edt14);
        } else if (v == lanjut) {
            if (ck1.isChecked() && (edt1.getText().toString().trim().equals("") || spin1.getSelectedItem().toString().equals("0"))) {
                Toast.makeText(this, "Harap Isi tanggal imunisasi dan juga Tahap imunisasi yang telah dilakukan", Toast.LENGTH_LONG).show();
            } else if (ck2.isChecked() && (edt2.getText().toString().trim().equals("") || spin2.getSelectedItem().toString().equals("0"))) {
                Toast.makeText(this, "Harap Isi tanggal imunisasi dan juga Tahap imunisasi yang telah dilakukan", Toast.LENGTH_LONG).show();
            } else if (ck3.isChecked() && (edt3.getText().toString().trim().equals(""))) {
                Toast.makeText(this, "Harap Isi tanggal imunisasi yang telah dilakukan", Toast.LENGTH_LONG).show();
            } else if (ck4.isChecked() && (edt4.getText().toString().trim().equals("") || spin4.getSelectedItem().toString().equals("0"))) {
                Toast.makeText(this, "Harap Isi tanggal imunisasi dan juga Tahap imunisasi yang telah dilakukan", Toast.LENGTH_LONG).show();
            } else if (ck5.isChecked() && (edt5.getText().toString().trim().equals("") || spin5.getSelectedItem().toString().equals("0"))) {
                Toast.makeText(this, "Harap Isi tanggal imunisasi dan juga Tahap imunisasi yang telah dilakukan", Toast.LENGTH_LONG).show();
            } else if (ck6.isChecked() && (edt6.getText().toString().trim().equals("") || spin6.getSelectedItem().toString().equals("0"))) {
                Toast.makeText(this, "Harap Isi tanggal imunisasi dan juga Tahap imunisasi yang telah dilakukan", Toast.LENGTH_LONG).show();
            } else if (ck7.isChecked() && (edt7.getText().toString().trim().equals("") || spin7.getSelectedItem().toString().equals("0"))) {
                Toast.makeText(this, "Harap Isi tanggal imunisasi dan juga Tahap imunisasi yang telah dilakukan", Toast.LENGTH_LONG).show();
            } else if (ck8.isChecked() && (edt8.getText().toString().trim().equals(""))) {
                Toast.makeText(this, "Harap Isi tanggal imunisasi yang telah dilakukan", Toast.LENGTH_LONG).show();
            } else if (ck9.isChecked() && (edt9.getText().toString().trim().equals("") || spin9.getSelectedItem().toString().equals("0"))) {
                Toast.makeText(this, "Harap Isi tanggal imunisasi dan juga Tahap imunisasi yang telah dilakukan", Toast.LENGTH_LONG).show();
            } else if (ck10.isChecked() && (edt10.getText().toString().trim().equals("") || spin10.getSelectedItem().toString().equals("0"))) {
                Toast.makeText(this, "Harap Isi tanggal imunisasi dan juga Tahap imunisasi yang telah dilakukan", Toast.LENGTH_LONG).show();
            } else if (ck11.isChecked() && (edt11.getText().toString().trim().equals(""))) {
                Toast.makeText(this, "Harap Isi tanggal imunisasi yang telah dilakukan", Toast.LENGTH_LONG).show();
            } else if (ck12.isChecked() && (edt12.getText().toString().trim().equals(""))) {
                Toast.makeText(this, "Harap Isi tanggal imunisasi yang telah dilakukan", Toast.LENGTH_LONG).show();
            } else if (ck13.isChecked() && (edt13.getText().toString().trim().equals(""))) {
                Toast.makeText(this, "Harap Isi tanggal imunisasi yang telah dilakukan", Toast.LENGTH_LONG).show();
            } else if (ck14.isChecked() && (edt14.getText().toString().trim().equals(""))) {
                Toast.makeText(this, "Harap Isi tanggal imunisasi yang telah dilakukan", Toast.LENGTH_LONG).show();
            } else if (!ck1.isChecked()
                    && !ck2.isChecked()
                    && !ck3.isChecked()
                    && !ck4.isChecked()
                    && !ck5.isChecked()
                    && !ck6.isChecked()
                    && !ck7.isChecked()
                    && !ck8.isChecked()
                    && !ck9.isChecked()
                    && !ck10.isChecked()
                    && !ck11.isChecked()
                    && !ck12.isChecked()
                    && !ck13.isChecked()
                    && !ck14.isChecked()
                    ) {
                Toast.makeText(this, "Harap Isi data imunisasi terlebih dahulu untuk melanjutkan.", Toast.LENGTH_LONG).show();
            } else {
                if (preferences.getString(Preferences.ID, "").equalsIgnoreCase("") || preferences.getString(Preferences.JENIS_KELAMIN, "").equalsIgnoreCase("")) {
                    // get prompts.xml view
                    LayoutInflater li = LayoutInflater.from(getApplicationContext());
                    View dialogView = li.inflate(R.layout.dialog_sesi_pasien_baru, null);
                    final Dialog dialog3;
                    dialog3 = new Dialog(context);
                    dialog3.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    dialog3.setContentView(dialogView);
                    //make transparent dialog_sesi_offline_pelod
                    dialog3.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                    dialog3.show();


                    Button dialogLanjut = (Button) dialogView.findViewById(R.id.ok);
                    final RadioButton rad1 = (RadioButton) dialogView.findViewById(R.id.rad1);
                    final RadioButton rad2 = (RadioButton) dialogView.findViewById(R.id.rad2);
                    rad1.setChecked(true);

                    rad1.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            rad1.setChecked(true);
                            rad2.setChecked(false);
                        }
                    });

                    rad2.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            rad2.setChecked(true);
                            rad1.setChecked(false);
                        }
                    });

                    final EditText edt = (EditText) dialogView.findViewById(R.id.edt1);
                    final EditText edt2 = (EditText) dialogView.findViewById(R.id.edt2);
                    final EditText edt3 = (EditText) dialogView.findViewById(R.id.edt3);


                    edt.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            showDatepickerDialog(edt);
                        }
                    });
                    dialogLanjut.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (edt.getText().toString().equals("") ||
                                    edt2.getText().toString().equals("") ||
                                    edt3.getText().toString().equals("")) {
                                dialog2(getResources().getString(R.string.IsiSeluruhIsian), getResources().getString(R.string.Perhatian));
                            } else {
                                String jk;
                                if (rad1.isChecked()) {
                                    jk = "1";
                                } else if (rad2.isChecked()) {
                                    jk = "0";
                                } else {
                                    jk = "";
                                }
                                saveSesiPasien(jk, edt.getText().toString(), edt3.getText().toString(), edt2.getText().toString());
                                dialog3.dismiss();
                            }
                        }
                    });
                } else {
                    save();
                }
            }

        }
    }


    @Override
    public int getLayout() {
        return R.layout.activity_input_data_imunisasi;
    }

    @Override
    public void updateUI() {

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

    }

    void save() {
        data = new ArrayList<Selected>();

        if (ck1.isChecked()) {
            Selected selected = new Selected();
            selected.setId_vaksin("1");
            selected.setTahap(spin1.getSelectedItem().toString());
            selected.setTangga_vaksin(edt1.getText().toString());
            data.add(selected);
        }

        if (ck2.isChecked()) {
            Selected selected = new Selected();
            selected.setId_vaksin("2");
            selected.setTahap(spin2.getSelectedItem().toString());
            selected.setTangga_vaksin(edt2.getText().toString());
            data.add(selected);
        }

        if (ck3.isChecked()) {
            Selected selected = new Selected();
            selected.setId_vaksin("3");
            selected.setTahap("-1");
            selected.setTangga_vaksin(edt3.getText().toString());
            data.add(selected);
        }

        if (ck4.isChecked()) {
            Selected selected = new Selected();
            selected.setId_vaksin("4");
            selected.setTahap(spin4.getSelectedItem().toString());
            selected.setTangga_vaksin(edt4.getText().toString());
            data.add(selected);
        }

        if (ck5.isChecked()) {
            Selected selected = new Selected();
            selected.setId_vaksin("5");
            selected.setTahap(spin5.getSelectedItem().toString());
            selected.setTangga_vaksin(edt5.getText().toString());
            data.add(selected);
        }

        if (ck6.isChecked()) {
            Selected selected = new Selected();
            selected.setId_vaksin("6");
            selected.setTahap(spin6.getSelectedItem().toString());
            selected.setTangga_vaksin(edt6.getText().toString());
            data.add(selected);
        }

        if (ck7.isChecked()) {
            Selected selected = new Selected();
            selected.setId_vaksin("7");
            selected.setTahap(spin7.getSelectedItem().toString());
            selected.setTangga_vaksin(edt7.getText().toString());
            data.add(selected);
        }

        if (ck8.isChecked()) {
            Selected selected = new Selected();
            selected.setId_vaksin("8");
            selected.setTahap("-1");
            selected.setTangga_vaksin(edt8.getText().toString());
            data.add(selected);
        }

        if (ck9.isChecked()) {
            Selected selected = new Selected();
            selected.setId_vaksin("9");
            selected.setTahap(spin9.getSelectedItem().toString());
            selected.setTangga_vaksin(edt9.getText().toString());
            data.add(selected);
        }

        if (ck10.isChecked()) {
            Selected selected = new Selected();
            selected.setId_vaksin("10");
            selected.setTahap(spin10.getSelectedItem().toString());
            selected.setTangga_vaksin(edt10.getText().toString());
            data.add(selected);
        }

        if (ck11.isChecked()) {
            Selected selected = new Selected();
            selected.setId_vaksin("11");
            selected.setTahap("-1");
            selected.setTangga_vaksin(edt11.getText().toString());
            data.add(selected);
        }

        if (ck12.isChecked()) {
            Selected selected = new Selected();
            selected.setId_vaksin("12");
            selected.setTahap("-1");
            selected.setTangga_vaksin(edt12.getText().toString());
            data.add(selected);
        }

        if (ck13.isChecked()) {
            Selected selected = new Selected();
            selected.setId_vaksin("13");
            selected.setTahap("-1");
            selected.setTangga_vaksin(edt13.getText().toString());
            data.add(selected);
        }

        if (ck14.isChecked()) {
            Selected selected = new Selected();
            selected.setId_vaksin("14");
            selected.setTahap("-1");
            selected.setTangga_vaksin(edt14.getText().toString());
            data.add(selected);
        }
        editSesi(data);
    }


    private void editSesi(final ArrayList<Selected> data) {
        progresDialog(false, getResources().getString(R.string.MemuatData));
        StringRequest listdata = new StringRequest(Request.Method.POST,
                ApiReferences.postImunisasi(preferences.getString(Preferences.ID, "")),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.i("RESULT JSON", response.toString());
                        dialog.dismiss();
                        Message msg = Parser.getMessage(response);
                        if (msg.getStatus().equals("1")) {
                            Intent i = new Intent(getApplicationContext(), TabelImunisasiActivity.class);
                            i.putExtra("respon", response);
                            startActivity(i);
                            finish();
                        } else {
                            dialog(msg.getMessages(), getResources().getString(R.string.Perhatian));
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dialog.dismiss();
                dialog(error.toString(), getResources().getString(R.string.Perhatian));
                Intent i = new Intent(getApplicationContext(), TabelImunisasiActivity.class);
                startActivity(i);
                finish();
            }
        }) {

            protected Map<String, String> getParams() {
                HashMap<String, String> params = new HashMap<String, String>();
                for (int i = 0; i < data.size(); i++) {
                    params.put("id_vaksin[" + i + "]", data.get(i).getId_vaksin());
                    params.put("tanggal_vaksin[" + i + "]", data.get(i).getTangga_vaksin());
                    params.put("tahap[" + i + "]", data.get(i).getTahap());
                }
                return params;
            }
        };
        listdata.setRetryPolicy(new DefaultRetryPolicy(
                20000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        mRequest.add(listdata);
    }


    void showDatepickerDialog(final TextView txt) {
        dateHelper.showDatePicker(this, System.currentTimeMillis(), new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                int _year = year;
                int _month = monthOfYear + 1;
                int _day = dayOfMonth;
                String date = _year + "-" + _month + "-" + _day;
                String birthDate = dateHelper.formatDate(date);
                txt.setText(birthDate);
            }
        });
    }


    private void saveSesiPasien(final String jenis_kelamin, final String tanggal_lahir, final String berat, final String tinggi) {
        progresDialog(false, "Menyimpan Data Pasien");
        StringRequest postlogin = new StringRequest(Request.Method.POST,
                ApiReferences.insertPasien(), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                dialog.dismiss();
                try {
                    Log.i("RESULT JSON", response.toString());
                    Pasien pasien = Parser.getPasien(response
                            .toString());
                    if (pasien.getStatus().equalsIgnoreCase("1")) {
                        editor.putString(Preferences.ID, pasien.getObjects().getId());
                        editor.putString(Preferences.BERAT, pasien.getObjects().getBerat());
                        editor.putString(Preferences.TINGGI, pasien.getObjects().getTinggi());
                        editor.putString(Preferences.JENIS_KELAMIN, pasien.getObjects().getJenis_kelamin());
                        editor.putString(Preferences.BIRTH_DAY, pasien.getObjects().getTanggal_lahir());
                        editor.commit();
                        dialogEmail("Kode : " + pasien.getObjects().getId() + "\nSimpan Code ini untuk pemeriksaan berikutnya.", "Pendaftaran Sesi Pasien Berhasil", pasien.getObjects().getId());
                    }
                } catch (JsonSyntaxException e) {
                    dialog(getResources().getString(R.string.KesalahanServer), getResources().getString(R.string.Perhatian));
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                dialog.dismiss();
                dialog(getResources().getString(R.string.NoConnection), getResources().getString(R.string.Perhatian));
            }
        }) {

            protected Map<String, String> getParams() {
                HashMap<String, String> params = new HashMap<String, String>();
                params.put("jenis_kelamin", jenis_kelamin);
                params.put("tanggal_lahir", tanggal_lahir);
                params.put("berat", berat);
                params.put("tinggi", tinggi);
                return params;
            }
        };
        postlogin.setRetryPolicy(new DefaultRetryPolicy(
                20000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        mRequest.add(postlogin);
    }


    public void dialogEmail(String content, String msg, final String kode) {
        new MaterialDialog.Builder(this)
                .title(msg)
                .content(content)
                .autoDismiss(false)
                .cancelable(false)
                .positiveText(getResources().getString(R.string.KirimEmail))
                .negativeText(getResources().getString(R.string.tutup))
                .callback(new MaterialDialog.ButtonCallback() {
                    @Override
                    public void onPositive(MaterialDialog dialog) {
                        Intent sendEmail = new Intent(Intent.ACTION_SEND);
                        sendEmail.setType("text/plain");
                        sendEmail.putExtra(Intent.EXTRA_EMAIL, new String[]{""});
                        sendEmail.putExtra(Intent.EXTRA_SUBJECT, "Kode Pasien");
                        sendEmail.putExtra(Intent.EXTRA_TEXT, "Berikut ini adalah kode pasien anda.\nKode : " + kode);
                        startActivity(Intent.createChooser(sendEmail, "Email With"));
                    }

                    @Override
                    public void onNegative(MaterialDialog dialog) {
                        dialog.dismiss();
                        save();
                    }
                })
                .show();
    }


    @Override
    public void onBackPressed() {
        Intent i = new Intent(ImunisasiInsertDataActivity.this, MainActivity.class);
        startActivity(i);
        finish();
    }


    public void dialog(String content, String msg) {
        new MaterialDialog.Builder(this)
                .title(msg)
                .content(content)
                .positiveText("Ok")
                .callback(new MaterialDialog.ButtonCallback() {
                    @Override
                    public void onPositive(MaterialDialog dialog) {
                    }

                })
                .show();
    }

    public void dialog2(String content, String msg) {
        new MaterialDialog.Builder(this)
                .title(msg)
                .content(content)
                .negativeText("OK")
                .callback(new MaterialDialog.ButtonCallback() {
                    @Override
                    public void onPositive(MaterialDialog dialog) {

                    }

                    @Override
                    public void onNegative(MaterialDialog dialog) {
                        super.onNegative(dialog);
                    }

                })
                .show();
    }

    private void progresDialog(boolean horizontal, String msg) {
        dialog = new MaterialDialog.Builder(this)
                .title(msg)
                .content(getResources().getString(R.string.MohonTunggu))
                .progress(true, 0)
                .progressIndeterminateStyle(horizontal)
                .cancelable(false)
                .show();
    }


    public class Selected {
        private String id_vaksin;
        private String tangga_vaksin;
        private String tahap;

        public String getId_vaksin() {
            return id_vaksin;
        }

        public void setId_vaksin(String id_vaksin) {
            this.id_vaksin = id_vaksin;
        }

        public String getTangga_vaksin() {
            return tangga_vaksin;
        }

        public void setTangga_vaksin(String tangga_vaksin) {
            this.tangga_vaksin = tangga_vaksin;
        }

        public String getTahap() {
            return tahap;
        }

        public void setTahap(String tahap) {
            this.tahap = tahap;
        }
    }

}
