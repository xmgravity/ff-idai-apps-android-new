package com.prima.fragments;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import com.prima.activities.SplashPagerActivity;
import prima.test.prima.R;

/**
 * Created by Codelabs on 30/09/2015.
 */
public class SplashFragment extends BaseFragment implements View.OnClickListener {
    private Context mContext;
    public static final String ARG_PAGE = "page 1";
    private ViewPager mPager;
    List<Fragment> fragmentList = new ArrayList<Fragment>();
    TextView title,subtitle ;

    @Override
    public void initView(View view) {
        mPager = (ViewPager) getActivity().findViewById(R.id.pager);
        mContext = getActivity();
        Activity act = getActivity();

        title = (TextView) view.findViewById(R.id.title);
        subtitle = (TextView) view.findViewById(R.id.subtitle);
        Typeface tf = Typeface.createFromAsset(getActivity().getAssets(), "BREVIA-BLACK.OTF");
        Typeface tf2 = Typeface.createFromAsset(getActivity().getAssets(), "BREVIA-SEMIBOLD.OTF");
        Typeface tf5 = Typeface.createFromAsset(getActivity().getAssets(), "Coheadline.TTF");
        title.setTypeface(tf);
        subtitle.setTypeface(tf5);

        if (act instanceof SplashPagerActivity) {
            fragmentList = ((SplashPagerActivity) act).getFragmentList();
        }
    }

    @Override
    public void setUICallbacks() {

    }

    @Override
    public void updateUI() {

    }

    @Override
    public String getPageTitle() {
        return "Account Summary";
    }


    @Override
    public int getFragmentLayout() {
        return R.layout.activity_splash_screen;
    }

    @Override
    public void onClick(View v) {

    }

}