package com.prima.activities;

import android.content.Intent;
import android.graphics.Typeface;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import prima.test.prima.R;

/**
 * Created by Codelabs on 26/08/2015.
 */
public class EditEmailResultActivity extends BaseActivity implements View.OnClickListener {

    TextView title;
    ImageView close,back;
Button tutup;
    @Override
    public void initView() {
        tutup = (Button) findViewById(R.id.tutup);
        close = (ImageView) findViewById(R.id.close);
        back = (ImageView) findViewById(R.id.back);
        close.setVisibility(View.GONE);
        //set font
        title = (TextView) findViewById(R.id.tv_actionbar_title);
        Typeface tf = Typeface.createFromAsset(getApplicationContext().getAssets(), "BREVIA-BLACK.OTF");
        title.setTypeface(tf);
        title.setText("EDIT EMAIL");
    }

    @Override
    public void setUICallbacks() {
        back.setOnClickListener(this);
        tutup.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        if (v==back){
            Intent i = new Intent(getApplicationContext(), SettingActivity.class);
            startActivity(i);
            finish();
        } else if (v==tutup){
            Intent i = new Intent(getApplicationContext(), SettingActivity.class);
            startActivity(i);
            finish();
        }
    }


    @Override
    public int getLayout() {
        return R.layout.activity_edit_email_result;
    }

    @Override
    public void updateUI() {

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

    }


    @Override
    public void onBackPressed() {
        Intent i = new Intent(getApplicationContext(), SettingActivity.class);
        startActivity(i);
        finish();
    }
}
