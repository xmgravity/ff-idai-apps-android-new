package com.prima.entities;

import java.util.ArrayList;

/**
 * Created by Codelabs on 15/09/2015.
 */
public class TanyaJawab extends Message{

    private ArrayList<TanyaJawabs> data;

    public ArrayList<TanyaJawabs> getObjects() {
        return data;
    }

    public void setObjects(ArrayList<TanyaJawabs> objects) {
        this.data = objects;
    }

    public class TanyaJawabs {
        private String id;
        private String question;
        private String answer;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getPertanyaan() {
            return question;
        }

        public void setPertanyaan(String pertanyaan) {
            this.question = pertanyaan;
        }

        public String getJawaban() {
            return answer;
        }

        public void setJawaban(String jawaban) {
            this.answer = jawaban;
        }
    }
}
