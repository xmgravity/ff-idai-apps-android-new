package com.prima.adapters;

import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TimePicker;

import java.util.ArrayList;

import com.prima.entities.JadwalPraktek;
import com.prima.entities.JadwalPraktekSingle;
import com.prima.helpers.TimeHelper;
import prima.test.prima.R;

/**
 * Created by Codelabs on 07/08/2015.
 */
public class ListHariAdapter extends BaseAdapter {
    private ArrayList<String> _data;
    Context _context;
    TextView jamMulai;
    TextView jamSelesai;
    private TimeHelper timeHelper;
    private ArrayList<JadwalPraktek.Jadwal> arrJdwl ;
    JadwalPraktekSingle jdwlPraktek;

    public ArrayList<JadwalPraktek.Jadwal> getArrJdwl() {
        return arrJdwl;
    }

    public ListHariAdapter(ArrayList<String> data,
                           Context context, JadwalPraktekSingle jdwlPraktek) {
        _data = data;
        _context = context;
        this.jdwlPraktek = jdwlPraktek;

        try {
            arrJdwl = new ArrayList<JadwalPraktek.Jadwal>();
            ArrayList<JadwalPraktek.Jadwal> temp = jdwlPraktek.getObjects().getJadwal();
            for (int i = 0; i < 7; i++) {
                JadwalPraktek.Jadwal jdw = new JadwalPraktek().new Jadwal();
                jdw.setJam_mulai("");
                jdw.setJam_akhir("");
                jdw.setStatus("0");
                jdw.setHari(String.valueOf(i));
                arrJdwl.add(jdw);
            }
            for (int j = 0; j < temp.size(); j++) {
                JadwalPraktek.Jadwal jdw = new JadwalPraktek().new Jadwal();
                jdw.setJam_mulai(temp.get(j).getJam_mulai());
                jdw.setJam_akhir(temp.get(j).getJam_akhir());
                jdw.setStatus("1");
                jdw.setHari(temp.get(j).getHari());
                arrJdwl.set(Integer.valueOf(temp.get(j).getHari()), jdw);
            }
        } catch (NullPointerException e) {
            e.printStackTrace();
            arrJdwl = new ArrayList<JadwalPraktek.Jadwal>();
            for (int i = 0; i < 7; i++) {
                JadwalPraktek.Jadwal jdw = new JadwalPraktek().new Jadwal();
                jdw.setJam_mulai("");
                jdw.setJam_akhir("");
                jdw.setStatus("0");
                jdw.setHari(String.valueOf(i));
                arrJdwl.add(jdw);
            }
        }

    }

    @Override
    public int getCount() {

        return _data.size();
    }

    @Override
    public Object getItem(int position) {

        return _data.get(position);
    }

    @Override
    public long getItemId(int position) {

        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View v = convertView;
        if (v == null) {
            LayoutInflater infLayout = (LayoutInflater) _context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = infLayout.inflate(R.layout.list_item_pertanyaan_checkbox_blue, null);
        }
        timeHelper = new TimeHelper();
        TextView txt_question = (TextView) v.findViewById(R.id.txt_question);
        RelativeLayout item = (RelativeLayout) v.findViewById(R.id.item);
        txt_question.setText(_data.get(position));

        final CheckBox ck = (CheckBox) v.findViewById(R.id.ck);
        ck.setFocusable(false);
        if (arrJdwl.get(position).getHari().equals(String.valueOf(position)) && arrJdwl.get(position).getStatus().equals("1")) {
            ck.setChecked(true);
        } else {
            ck.setChecked(false);
        }

        item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!ck.isChecked()) {
                    ck.setChecked(true);

                    // get prompts.xml view
                    LayoutInflater li = LayoutInflater.from(_context);
                    View dialogView = li.inflate(R.layout.dialog_set_jam_praktek, null);
                    final Dialog dialog;
                    dialog = new Dialog(_context);
                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    dialog.setContentView(dialogView);
                    dialog.setCancelable(false);
                    //make transparent dialog_sesi_offline_pelod
                    dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                    dialog.show();

                    Button simpan = (Button) dialogView.findViewById(R.id.simpan);
                    TextView txt_hari = (TextView) dialogView.findViewById(R.id.txt_hari);

                    jamMulai = (TextView) dialogView.findViewById(R.id.edt1);
                    jamSelesai = (TextView) dialogView.findViewById(R.id.edt2);

                    jamMulai.setText("00:00");
                    jamSelesai.setText("00:00");

                    jamMulai.setFocusable(false);
                    jamSelesai.setFocusable(false);

                    txt_hari.setText(_data.get(position));
                    jamMulai.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            jamMulaiPicker();
                        }
                    });

                    jamSelesai.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            setJamSelesaiPicker();
                        }
                    });

                    simpan.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog.dismiss();
                            arrJdwl.get(position).setJam_akhir(jamSelesai.getText().toString());
                            arrJdwl.get(position).setJam_mulai(jamMulai.getText().toString());
                            arrJdwl.get(position).setHari(String.valueOf(position));
                            arrJdwl.get(position).setStatus("1");
                        }
                    });
                } else {
                    arrJdwl.get(position).setStatus("0");
                    ck.setChecked(false);
                }
            }
        });


        return v;
    }

    private void jamMulaiPicker() {
        timeHelper.showDateTimePicker(_context, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                String hours = String.valueOf(hourOfDay);
                String minutes = String.valueOf(minute);
                String realHours = null;
                String realMinutes = null;

                if (hours.length() == 1) {
                    realHours = "0" + hourOfDay;
                } else {
                    realHours = hours;
                }

                if (minutes.length() == 1) {
                    realMinutes = "0" + minute;
                } else {
                    realMinutes = minutes;
                }
                jamMulai.setText(realHours + ":" + String.valueOf(realMinutes));
            }
        });
    }

    private void setJamSelesaiPicker() {
        timeHelper.showDateTimePicker(_context, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                String hours = String.valueOf(hourOfDay);
                String minutes = String.valueOf(minute);
                String realHours = null;
                String realMinutes = null;

                if (hours.length() == 1) {
                    realHours = "0" + hourOfDay;
                } else {
                    realHours = hours;
                }

                if (minutes.length() == 1) {
                    realMinutes = "0" + minute;
                } else {
                    realMinutes = minutes;
                }
                jamSelesai.setText(realHours + ":" + String.valueOf(realMinutes));
            }
        });
    }

}
