package com.prima.fragments;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

import com.prima.Preferences;
import com.prima.activities.PgcsQuestionActivity;
import com.prima.entities.Question;
import prima.test.prima.R;
import com.prima.activities.PgcsResult;
import com.prima.adapters.ListAdapter;


/**
 * Created by Codelabs on 07/08/2015.
 */
public class PgcsQuesionerFragment extends BaseFragment implements View.OnClickListener {

    private Context mContext;
    public static final String ARG_PAGE = "page 1";
    private ArrayList<String> listItem = new ArrayList<String>();
    private ArrayList<String> listItem2 = new ArrayList<String>();
    private ListView list;
    private ViewPager mPager;
    String selected = "";
    int indeks;
    List<Fragment> fragmentList = new ArrayList<Fragment>();
    Question.Kuesioner kuesioner;
    private ArrayList<String> ansewer = new ArrayList<String>();
    private ArrayList<String> kode = new ArrayList<String>();
    int totalScore = 0;

    @SuppressLint("ValidFragment")
    public PgcsQuesionerFragment() {}
    @SuppressLint("ValidFragment")
    public PgcsQuesionerFragment(Question.Kuesioner kuesioner, int indeks) {
        this.kuesioner = kuesioner;
        this.indeks = indeks;
    }

    @Override
    public void initView(View view) {
        list = (ListView) view.findViewById(R.id.listView);
        mPager = (ViewPager) getActivity().findViewById(R.id.pager);
        mContext = getActivity();
        final Activity act = getActivity();


        if (act instanceof PgcsQuestionActivity) {
            fragmentList = ((PgcsQuestionActivity) act).getFragmentList();
            ansewer = ((PgcsQuestionActivity) act).getAnsewer();
            kode = ((PgcsQuestionActivity) act).getKode();
        }


        final ListAdapter adapterList = new ListAdapter(
                kuesioner.getAnswer_option(), mContext, selected);
        list.setAdapter(adapterList);


        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                ansewer.set(indeks, kuesioner.getAnswer_option().get(position).getScore());
                kode.set(indeks, kuesioner.getKode() + kuesioner.getAnswer_option().get(position).getScore().replace(".00", ""));

                selected = String.valueOf(kuesioner.getAnswer_option().get(position).getScore());
                adapterList.setSelected(selected);
                adapterList.notifyDataSetChanged();
                int current = mPager.getCurrentItem();
                if (current != fragmentList.size() - 1) {
                    mPager.setCurrentItem(current + 1, true);
                } else {
                    int score = 0;
                    String interpretasi = " ";
                    for (int j = 0; j < ansewer.size(); j++) {
                        if (ansewer.get(j).replace(".00", "").equals("")) {
                            score = score + 0;
                        } else {
                            score = score + Integer.valueOf(ansewer.get(j).replace(".00", ""));
                        }
                        interpretasi = interpretasi + kode.get(j)+ " ";
                    }
                    if (preferences.getString(Preferences.ID, "").equals("1")) {
                        editor.putString(Preferences.BIRTH_DAY, "");
                        editor.putString(Preferences.JENIS_KELAMIN, "");
                        editor.putString(Preferences.BERAT, "");
                        editor.putString(Preferences.TINGGI, "");
                        editor.putString(Preferences.ID, "");
                        editor.commit();
                    }

                    if (act instanceof PgcsQuestionActivity) {
                        totalScore = ((PgcsQuestionActivity) act).getMax_score();
                    }

                    Intent i = new Intent(mContext, PgcsResult.class);
                    i.putExtra("score", score);
                    i.putExtra("interpretasi", interpretasi);
                    i.putExtra("max_score",totalScore);
                    startActivity(i);
                    getActivity().finish();
                }

            }
        });
    }

    @Override
    public void setUICallbacks() {

    }

    @Override
    public void updateUI() {

    }

    @Override
    public String getPageTitle() {
        return "Account Summary";
    }


    @Override
    public int getFragmentLayout() {
        return R.layout.list_view;
    }

    @Override
    public void onClick(View v) {

    }

}