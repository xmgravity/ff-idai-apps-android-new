package com.prima.activities;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.prima.Preferences;
import com.prima.controllers.ApiReferences;
import com.prima.controllers.Parser;
import com.prima.entities.JadwalPraktek;
import com.prima.entities.Message;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import prima.test.prima.R;

/**
 * Created by Codelabs on 19/08/2015.
 */
public class PraktikActivity extends BaseActivity implements View.OnClickListener {

    private TextView title, txt_alamat;
    private ImageView close, back;
    private ArrayList<String> listItem = new ArrayList<String>();
    private Context mContext;
    private RelativeLayout alamat, jadwal;
    private Button simpan;
    private MaterialDialog dialog;
    private RequestQueue mRequest;
    private ArrayList<JadwalPraktek.Jadwal> praktek = new ArrayList<JadwalPraktek.Jadwal>();
    private EditText edt_nama_praktek, edt_tlp;
    LinearLayout list;
    private JadwalPraktek.GetPraktek jdwlPraktek = new JadwalPraktek().new GetPraktek();
    private JadwalPraktek objJadwal;
    String latitude = "";
    String longitude = "";

    @Override
    public void initView() {
        list = (LinearLayout) findViewById(R.id.day);
        mRequest = Volley.newRequestQueue(getApplicationContext());
        edt_nama_praktek = (EditText) findViewById(R.id.edt_nama_praktek);
        edt_tlp = (EditText) findViewById(R.id.edt_tlp);
        alamat = (RelativeLayout) findViewById(R.id.alamat);
        jadwal = (RelativeLayout) findViewById(R.id.jadwal);
        title = (TextView) findViewById(R.id.tv_actionbar_title);
        txt_alamat = (TextView) findViewById(R.id.txt_alamat);
        close = (ImageView) findViewById(R.id.close);
        back = (ImageView) findViewById(R.id.back);
        simpan = (Button) findViewById(R.id.simpan);
        close.setVisibility(View.GONE);
        mContext = getApplicationContext();
        //set font
        Typeface tf = Typeface.createFromAsset(getApplicationContext().getAssets(), "BREVIA-BLACK.OTF");
        title.setTypeface(tf);
        title.setText(getResources().getString(R.string.Praktik));
        for (int i = 0; i < 10; i++) {
            listItem.add("Text");
        }
        JadwalPraktek.Praktek prak = new JadwalPraktek().new Praktek();
        jdwlPraktek.setObjects(prak);


    }

    @Override
    public void setUICallbacks() {
        simpan.setOnClickListener(this);
        back.setOnClickListener(this);
        alamat.setOnClickListener(this);
        jadwal.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        if (v == back) {
            // Intent i = new Intent(getApplicationContext(), ProfileActivity.class);
            //    startActivity(i);
            finish();
        } else if (v == alamat) {
            Intent i = new Intent(getApplicationContext(), SearchAlamatActivity.class);
            try {
                i.putExtra("longitude", longitude);
                i.putExtra("latitude", latitude);
            } catch (NullPointerException e) {
                e.printStackTrace();
            }
            startActivityForResult(i, 2);
            //    finish();
        } else if (v == jadwal) {
            Intent i = new Intent(getApplicationContext(), PraktikPickHariActivity.class);
            Gson gson = new Gson();
            try {
                i.putExtra("result", gson.toJson(jdwlPraktek));
            } catch (NullPointerException e) {
                e.printStackTrace();
            }
            startActivityForResult(i, 1);
            //   finish();
        } else if (v == simpan) {
            if (edt_nama_praktek.getText().toString().trim().equals("") ||
                    txt_alamat.getText().toString().trim().equals("") ||
                    edt_tlp.getText().toString().trim().equals("")) {
                dialog(getResources().getString(R.string.LengkapiDataJadwalPraktik), getResources().getString(R.string.Perhatian));
            } else {
                try {
                    save(objJadwal.getJadwals());
                } catch (NullPointerException e) {
                    e.printStackTrace();
                    dialog(getResources().getString(R.string.IsiJadwalPraktik), getResources().getString(R.string.Perhatian));
                }
            }
        }
    }


    private void save(final ArrayList<JadwalPraktek.Jadwal> data) {
        progresDialog(false, getResources().getString(R.string.MenyimpanData));
        Log.d("dbgbg", "user: " + preferences.getString(Preferences.ID_USER, ""));
        StringRequest listdata = new StringRequest(Request.Method.POST,
                ApiReferences.createPraktik(preferences.getString(Preferences.ID_USER, "")),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.i("RESULT JSON", response.toString());
                        dialog.dismiss();
                        try {
                            Message msg = Parser.getMessage(response);
                            if (msg.getStatus().equals("1")) {
                                finish();
                            } else {
                                dialog(msg.getMessages(), getResources().getString(R.string.Perhatian));
                            }
                        } catch (JsonSyntaxException e) {
                            dialog(getResources().getString(R.string.KesalahanServer), getResources().getString(R.string.Perhatian));
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("dbgbg", "Create jadwal error: " + error);
                dialog.dismiss();
                dialog(getResources().getString(R.string.NoConnection), getResources().getString(R.string.Perhatian));
            }
        }) {

            protected Map<String, String> getParams() {
                HashMap<String, String> params = new HashMap<String, String>();
                for (int i = 0; i < data.size(); i++) {
                    params.put("nama_praktik", edt_nama_praktek.getText().toString());
                    params.put("alamat", txt_alamat.getText().toString());
                    params.put("telp", edt_tlp.getText().toString());
                    params.put("longitude", longitude);
                    params.put("latitude", latitude);
                    //if (/*data.get(i).getStatus().equals("0")*/true) {
                        params.put("hari[" + i + "]", String.valueOf(i));
                        params.put("jam_mulai[" + i + "]", "");
                        params.put("jam_akhir[" + i + "]", "");
                        params.put("status[" + i + "]", "");
                    /*} else if (data.get(i).getStatus().equals("1")) {
                        params.put("hari[" + i + "]", data.get(i).getHari());
                        params.put("jam_mulai[" + i + "]", data.get(i).getJam_mulai());
                        params.put("jam_akhir[" + i + "]", data.get(i).getJam_akhir());
                        params.put("status[" + i + "]", data.get(i).getStatus());
                    }*/
                }
                return params;
            }
        };
        listdata.setRetryPolicy(new DefaultRetryPolicy(
                20000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        mRequest.add(listdata);
    }


    @Override
    public int getLayout() {
        return R.layout.activity_praktik;
    }

    @Override
    public void updateUI() {

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 1) {
            if (data != null) {
                list.removeAllViews();
                objJadwal = Parser.getJadwal(data.getStringExtra("result"));
                //     Toast.makeText(this, data.getStringExtra("result"), Toast.LENGTH_LONG).show();

                jdwlPraktek.getObjects().setJadwal(objJadwal.getJadwals());
                for (int k = 0; k < objJadwal.getJadwals().size(); k++) {
                    if (objJadwal.getStatus().equals("1")) {
                        View chidlHari = getLayoutInflater().inflate(R.layout.list_item_day, null);
                        TextView waktu = (TextView) chidlHari.findViewById(R.id.waktu);
                        TextView lokasi = (TextView) chidlHari.findViewById(R.id.lokasi);
                        waktu.setId(22222 + k);
                        lokasi.setId(11111 + k);
                        lokasi.setText(objJadwal.getJadwals().get(k).getJam_mulai().substring(0, 5) + "-" + objJadwal.getJadwals().get(k).getJam_akhir().substring(0, 5));

                        if (objJadwal.getJadwals().get(k).getHari().equals("0")) {
                            waktu.setText(getResources().getString(R.string.Senin));
                        }
                        if (objJadwal.getJadwals().get(k).getHari().equals("1")) {
                            waktu.setText(getResources().getString(R.string.Selasa));
                        }
                        if (objJadwal.getJadwals().get(k).getHari().equals("2")) {
                            waktu.setText(getResources().getString(R.string.Rabu));
                        }
                        if (objJadwal.getJadwals().get(k).getHari().equals("3")) {
                            waktu.setText(getResources().getString(R.string.Kamis));
                        }
                        if (objJadwal.getJadwals().get(k).getHari().equals("4")) {
                            waktu.setText(getResources().getString(R.string.Jumat));
                        }
                        if (objJadwal.getJadwals().get(k).getHari().equals("5")) {
                            waktu.setText(getResources().getString(R.string.Sabtu));
                        }
                        if (objJadwal.getJadwals().get(k).getHari().equals("6")) {
                            waktu.setText(getResources().getString(R.string.Minggu));
                        }
                        list.addView(chidlHari);
                    }
                }
            }
        } else if (requestCode == 2) {
            if (data != null) {
                txt_alamat.setText(data.getStringExtra("result"));
                longitude = data.getStringExtra("longitude");
                latitude = data.getStringExtra("latitude");
            }
        }
    }


    public void dialog(String content, String msg) {
        new MaterialDialog.Builder(this)
                .title(msg)
                .content(content)
                .positiveText("Ok")
                .callback(new MaterialDialog.ButtonCallback() {
                    @Override
                    public void onPositive(MaterialDialog dialog) {
                    }

                })
                .show();
    }

    public void dialog2(String content, String msg) {
        new MaterialDialog.Builder(this)
                .title(msg)
                .content(content)
                .negativeText("OK")
                .callback(new MaterialDialog.ButtonCallback() {
                    @Override
                    public void onPositive(MaterialDialog dialog) {

                    }

                    @Override
                    public void onNegative(MaterialDialog dialog) {
                        super.onNegative(dialog);
                    }

                })
                .show();
    }

    private void progresDialog(boolean horizontal, String msg) {
        dialog = new MaterialDialog.Builder(this)
                .title(msg)
                .content(getResources().getString(R.string.MohonTunggu))
                .progress(true, 0)
                .progressIndeterminateStyle(horizontal)
                .cancelable(false)
                .show();
    }


    @Override
    public void onBackPressed() {
        //   Intent i = new Intent(getApplicationContext(), ProfileActivity.class);
        //   startActivity(i);
        finish();
    }
}
