package com.prima.activities;

import android.content.Intent;
import android.graphics.Typeface;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.JsonSyntaxException;
import com.prima.Preferences;
import com.prima.controllers.ApiReferences;
import com.prima.controllers.Parser;
import com.prima.entities.JadwalPraktek;
import com.prima.entities.Message;
import com.prima.helpers.DatabaseHandler;

import java.util.ArrayList;

import prima.test.prima.R;

/**
 * Created by Codelabs on 02/10/2015.
 */
public class ListJadwalActivity2 extends BaseActivity implements View.OnClickListener {

    TextView title;
    ImageView close, back;
    LinearLayout list;
    private MaterialDialog dialog;
    private RequestQueue mRequest;
    ArrayList<LinearLayout> days = new ArrayList<LinearLayout>();
    DatabaseHandler db;


    @Override
    public void initView() {
        db = new DatabaseHandler(context);
        mRequest = Volley.newRequestQueue(getApplicationContext());
        list = (LinearLayout) findViewById(R.id.list_jadwal_praktik);
        close = (ImageView) findViewById(R.id.close);
        back = (ImageView) findViewById(R.id.back);
        // close.setVisibility(View.GONE);
        //set font
        title = (TextView) findViewById(R.id.tv_actionbar_title);
        Typeface tf = Typeface.createFromAsset(getApplicationContext().getAssets(), "BREVIA-BLACK.OTF");
        title.setTypeface(tf);

        if(preferences.getString(Preferences.TIPE_USER, "").equalsIgnoreCase("awam")){
            title.setText(getResources().getString(R.string.Catatan2));
        }else{
            title.setText(getResources().getString(R.string.JadwalPraktik2));
        }


        //    getData();

        //  save(data);
    }

    @Override
    public void setUICallbacks() {
        close.setOnClickListener(this);
        back.setOnClickListener(this);
    }


    private void getData() {
        progresDialog(false, getResources().getString(R.string.MemuatData));
        StringRequest listdata = new StringRequest(Request.Method.GET,
                ApiReferences.getAllPraktek(preferences.getString(Preferences.ID_USER, "")),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        dialog.dismiss();
                        try {
                            if (response.equals("[]")) {
                                dialog(getResources().getString(R.string.DataBelumTersedia), getResources().getString(R.string.Perhatian));
                            } else {
                                Message msg = Parser.getMessage(response);
                                if (msg.getStatus().equals("0")) {
                                    dialog(msg.getMessages(), getResources().getString(R.string.Perhatian));
                                } else {
                                    Log.i("RESULT JSON", response.toString());
                                    final JadwalPraktek jdwl = Parser.getJadwal(response);


                                    for (int i = 0; i < 7; i++) {
                                        View child = getLayoutInflater().inflate(R.layout.list_item_praktik, null);
                                        TextView hari = (TextView) child.findViewById(R.id.hari);
                                        LinearLayout day = (LinearLayout) child.findViewById(R.id.day);
                                        hari.setId(i);
                                        child.setId(i);
                                        if (i == 0) {
                                            day.setId(i);
                                            days.add(day);
                                            hari.setText(getResources().getString(R.string.Senin));
                                        }
                                        if (i == 1) {
                                            day.setId(i);
                                            days.add(day);
                                            hari.setText(getResources().getString(R.string.Selasa));
                                        }
                                        if (i == 2) {
                                            day.setId(i);
                                            days.add(day);
                                            hari.setText(getResources().getString(R.string.Rabu));
                                        }
                                        if (i == 3) {
                                            day.setId(i);
                                            days.add(day);
                                            hari.setText(getResources().getString(R.string.Kamis));
                                        }
                                        if (i == 4) {
                                            day.setId(i);
                                            days.add(day);
                                            hari.setText(getResources().getString(R.string.Jumat));
                                        }
                                        if (i == 5) {
                                            day.setId(i);
                                            days.add(day);
                                            hari.setText(getResources().getString(R.string.Sabtu));
                                        }
                                        if (i == 6) {
                                            day.setId(i);
                                            days.add(day);
                                            hari.setText(getResources().getString(R.string.Minggu));
                                        }
                                        list.addView(child);

                                    }

                                    for (int i = 0; i < jdwl.getObjects().size(); i++) {
                                        db.deletePraktik(jdwl.getObjects().get(i).getId());
                                        db.addPraktek(jdwl.getObjects().get(i));
                                        for (int j = 0; j < jdwl.getObjects().get(i).getJadwal().size(); j++) {
                                            ///
                                            for (int k = 0; k < 7; k++) {
                                                if (jdwl.getObjects().get(i).getJadwal().get(j).getHari().equals(String.valueOf(k))
                                                        /*&& !jdwl.getObjects().get(i).getJadwal().get(j).getStatus().equals("0")*/) {
                                                    View chidlHari = getLayoutInflater().inflate(R.layout.list_item_day, null);
                                                    TextView waktu = (TextView) chidlHari.findViewById(R.id.waktu);
                                                    TextView lokasi = (TextView) chidlHari.findViewById(R.id.lokasi);
                                                    waktu.setId(22222 + Integer.valueOf(jdwl.getObjects().get(i).getId()));
                                                    lokasi.setId(11111 + Integer.valueOf(jdwl.getObjects().get(i).getId()));
                                                    waktu.setText(jdwl.getObjects().get(i).getJadwal().get(j).getJam_mulai().substring(0, 5) + "-" + jdwl.getObjects().get(i).getJadwal().get(j).getJam_akhir().substring(0, 5));
                                                    lokasi.setText(jdwl.getObjects().get(i).getNama_praktik());
                                                    days.get(k).addView(chidlHari);

                                                    final int finalI = i;
                                                    lokasi.setOnClickListener(new View.OnClickListener() {
                                                        @Override
                                                        public void onClick(View v) {
                                                            getDataEdit(jdwl.getObjects().get(finalI).getId());
                                                        }
                                                    });
                                                }
                                            }
                                            ///
                                        }
                                    }//

                                    for (int i = 0; i < 7; i++) {
                                        View v = list.getChildAt(i);
                                        if (days.get(i).getChildCount() == 0) {
                                            list.removeView(v);
                                        }
                                    }

                                }
                            }

                            JadwalPraktek jad = db.getPraktik();
                        } catch (JsonSyntaxException e) {
                            dialog(getResources().getString(R.string.KesalahanServer), getResources().getString(R.string.Perhatian));
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                final JadwalPraktek jdwl = db.getPraktik();
                try {

                    for (int i = 0; i < 7; i++) {
                        View child = getLayoutInflater().inflate(R.layout.list_item_praktik, null);
                        TextView hari = (TextView) child.findViewById(R.id.hari);
                        LinearLayout day = (LinearLayout) child.findViewById(R.id.day);
                        hari.setId(i);
                        child.setId(i);
                        if (i == 0) {
                            day.setId(i);
                            days.add(day);
                            hari.setText(getResources().getString(R.string.Senin));
                        }
                        if (i == 1) {
                            day.setId(i);
                            days.add(day);
                            hari.setText(getResources().getString(R.string.Selasa));
                        }
                        if (i == 2) {
                            day.setId(i);
                            days.add(day);
                            hari.setText(getResources().getString(R.string.Rabu));
                        }
                        if (i == 3) {
                            day.setId(i);
                            days.add(day);
                            hari.setText(getResources().getString(R.string.Kamis));
                        }
                        if (i == 4) {
                            day.setId(i);
                            days.add(day);
                            hari.setText(getResources().getString(R.string.Jumat));
                        }
                        if (i == 5) {
                            day.setId(i);
                            days.add(day);
                            hari.setText(getResources().getString(R.string.Sabtu));
                        }
                        if (i == 6) {
                            day.setId(i);
                            days.add(day);
                            hari.setText(getResources().getString(R.string.Minggu));
                        }
                        list.addView(child);

                    }

                    for (int i = 0; i < jdwl.getObjects().size(); i++) {
                        for (int j = 0; j < jdwl.getObjects().get(i).getJadwal().size(); j++) {
                            ///
                            for (int k = 0; k < 7; k++) {
                                if (jdwl.getObjects().get(i).getJadwal().get(j).getHari().equals(String.valueOf(k))
                                        /*&& !jdwl.getObjects().get(i).getJadwal().get(j).getStatus().equals("0")*/) {
                                    View chidlHari = getLayoutInflater().inflate(R.layout.list_item_day, null);
                                    TextView waktu = (TextView) chidlHari.findViewById(R.id.waktu);
                                    TextView lokasi = (TextView) chidlHari.findViewById(R.id.lokasi);
                                    waktu.setId(22222 + Integer.valueOf(jdwl.getObjects().get(i).getId()));
                                    lokasi.setId(11111 + Integer.valueOf(jdwl.getObjects().get(i).getId()));
                                    waktu.setText(jdwl.getObjects().get(i).getJadwal().get(j).getJam_mulai().substring(0, 5) + "-" + jdwl.getObjects().get(i).getJadwal().get(j).getJam_akhir().substring(0, 5));
                                    lokasi.setText(jdwl.getObjects().get(i).getNama_praktik());
                                    days.get(k).addView(chidlHari);

                                    final int finalI = i;
                                    lokasi.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            getDataEdit(jdwl.getObjects().get(finalI).getId());
                                        }
                                    });
                                }
                            }
                            ///
                        }
                    }//

                    for (int i = 0; i < 7; i++) {
                        View v = list.getChildAt(i);
                        if (days.get(i).getChildCount() == 0) {
                            list.removeView(v);
                        }
                    }

                } catch (NullPointerException e) {
                    dialog(getResources().getString(R.string.NoConnection), getResources().getString(R.string.Perhatian));
                }
                dialog.dismiss();
            }
        });
        listdata.setRetryPolicy(new DefaultRetryPolicy(
                20000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        mRequest.add(listdata);

    }


    private void getDataEdit(String id) {
        progresDialog(false, getResources().getString(R.string.MemuatData));
        StringRequest listdata = new StringRequest(Request.Method.GET,
                ApiReferences.getPraktekByPlace(id),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        dialog.dismiss();
                        try {
                            if (response.equals("[]")) {
                                dialog(getResources().getString(R.string.DataBelumTersedia), getResources().getString(R.string.Perhatian));
                            } else {
                                Message msg = Parser.getMessage(response);
                                if (msg.getStatus().equals("0")) {
                                    dialog(msg.getMessages(), getResources().getString(R.string.Perhatian));
                                } else {
                                    Log.i("RESULT JSON", response.toString());
                                    Intent i = new Intent(getApplicationContext(), EditJadwalActivity.class);
                                    i.putExtra("result", response);
                                    startActivity(i);
                                }
                            }
                        } catch (JsonSyntaxException e) {
                            dialog(getResources().getString(R.string.KesalahanServer), getResources().getString(R.string.Perhatian));
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dialog.dismiss();
                dialog(getResources().getString(R.string.NoConnection), getResources().getString(R.string.Perhatian));
            }
        });
        listdata.setRetryPolicy(new DefaultRetryPolicy(
                20000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        mRequest.add(listdata);

    }


    public void dialog(String content, String msg) {
        new MaterialDialog.Builder(this)
                .title(msg)
                .content(content)
                .positiveText("Ok")
                .callback(new MaterialDialog.ButtonCallback() {
                    @Override
                    public void onPositive(MaterialDialog dialog) {
                    }

                })
                .show();
    }

    public void dialog2(String content, String msg) {
        new MaterialDialog.Builder(this)
                .title(msg)
                .content(content)
                .negativeText("OK")
                .callback(new MaterialDialog.ButtonCallback() {
                    @Override
                    public void onPositive(MaterialDialog dialog) {

                    }

                    @Override
                    public void onNegative(MaterialDialog dialog) {
                        super.onNegative(dialog);
                    }

                })
                .show();
    }

    private void progresDialog(boolean horizontal, String msg) {
        dialog = new MaterialDialog.Builder(this)
                .title(msg)
                .content(getResources().getString(R.string.MohonTunggu))
                .progress(true, 0)
                .progressIndeterminateStyle(horizontal)
                .cancelable(false)
                .show();
    }


    @Override
    public void onClick(View v) {
        if (v == back) {
            //  Intent i = new Intent(getApplicationContext(), MainActivity.class);
            //   startActivity(i);
            finish();
        } else if (v == close) {
            Intent i = new Intent(getApplicationContext(), PraktikActivity.class);
            startActivity(i);
            //   finish();
        }
    }


    @Override
    public int getLayout() {
        return R.layout.activity_jadwal_praktik_list;
    }

    @Override
    public void updateUI() {
        list.removeAllViewsInLayout();
        days.clear();
        getData();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

    }


    @Override
    public void onBackPressed() {
        // Intent i = new Intent(getApplicationContext(), MainActivity.class);
        //  startActivity(i);
        finish();
    }


}
