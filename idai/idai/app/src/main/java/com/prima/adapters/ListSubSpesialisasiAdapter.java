package com.prima.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;

import prima.test.prima.R;

/**
 * Created by Codelabs on 07/08/2015.
 */
public class ListSubSpesialisasiAdapter extends BaseAdapter {
    private ArrayList<String> _data;
    private Context _context;
    private String selected;

    public String getSelected() {
        return selected;
    }

    public void setSelected(String selected) {
        this.selected = selected;
    }

    public ListSubSpesialisasiAdapter(ArrayList<String> data,
                                      Context context, String selected) {
        _data = data;
        _context = context;
        this.selected = selected;
    }

    @Override
    public int getCount() {

        return _data.size();
    }

    @Override
    public Object getItem(int position) {

        return _data.get(position);
    }

    @Override
    public long getItemId(int position) {

        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        if (v == null) {
            LayoutInflater infLayout = (LayoutInflater) _context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = infLayout.inflate(R.layout.list_item_subspesialisasi, null);
        }
        RelativeLayout item = (RelativeLayout) v.findViewById(R.id.item);
        TextView txt_question = (TextView) v.findViewById(R.id.txt_question);
        txt_question.setText(_data.get(position));
        ImageView ck = (ImageView) v.findViewById(R.id.ck);

        if (selected.equals(_data.get(position))) {
            ck.setBackgroundResource(R.mipmap.ic_checked);
        } else {
            ck.setBackgroundResource(R.mipmap.ic_uncheck);
        }


        return v;
    }
}
