package com.prima.adapters;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Codelabs on 07/08/2015.
 */
public class PagerAdapter extends FragmentStatePagerAdapter {
    Context mContext;
    List<Fragment> fragmentList = new ArrayList<Fragment>();

    public PagerAdapter(FragmentManager fm, Context context,List<Fragment> frag) {
        super(fm);
        mContext = context;
        fragmentList = frag;
    }

    @Override
    public Fragment getItem(int arg0) {
        return fragmentList.get(arg0);
    }

    @Override
    public int getCount() {
        return fragmentList.size();
    }

}
