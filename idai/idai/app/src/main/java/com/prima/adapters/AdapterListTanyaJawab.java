package com.prima.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import com.prima.entities.TanyaJawab;
import prima.test.prima.R;

/**
 * Created by Codelabs on 15/09/2015.
 */
public class AdapterListTanyaJawab extends BaseAdapter {
    private ArrayList<TanyaJawab.TanyaJawabs> _data;
    Context _context;

    public AdapterListTanyaJawab(ArrayList<TanyaJawab.TanyaJawabs> data,
                             Context context) {
        this._data = data;
        this._context = context;
    }

    @Override
    public int getCount() {

        return _data.size();
    }

    @Override
    public Object getItem(int position) {

        return _data.get(position);
    }

    @Override
    public long getItemId(int position) {

        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        if (v == null) {
            LayoutInflater infLayout = (LayoutInflater) _context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = infLayout.inflate(R.layout.list_item_tanya_jawab, null);
        }
        TextView   number = (TextView) v.findViewById(R.id.number);
        TextView   question = (TextView) v.findViewById(R.id.question);
        TextView  answer = (TextView) v.findViewById(R.id.answer);

        number.setText((position+1)+".");
        question.setText(_data.get(position).getPertanyaan());
        answer.setText(_data.get(position).getJawaban());
        return v;
    }
}
