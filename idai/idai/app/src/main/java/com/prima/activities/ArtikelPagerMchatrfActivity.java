package com.prima.activities;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.JsonSyntaxException;
import com.prima.MainActivity;
import com.prima.Preferences;
import com.prima.adapters.PagerAdapter;
import com.prima.controllers.ApiReferences;
import com.prima.controllers.Parser;
import com.prima.entities.Artikel;
import com.prima.entities.Message;
import com.prima.entities.Pdf;
import com.prima.entities.Question;
import com.prima.fragments.ArtikelBlueFragment;
import com.prima.helpers.DatabaseHandler;

import java.util.ArrayList;
import java.util.List;

import prima.test.prima.R;

/**
 * Created by Codelabs on 06/10/2015.
 */
public class ArtikelPagerMchatrfActivity extends BaseActivity implements View.OnClickListener {
    private Button ok;
    private Context mContext;
    private ViewPager mPager;
    private PagerAdapter mPagerAdapter;
    private static int currentPage;
    PageListener pageListener;
    static TextView title;
    List<Fragment> fragmentList = new ArrayList<Fragment>();
    static LinearLayout indikatorWrapper;
    private Artikel artikel;


    //

    private ImageView close;
    MaterialDialog dialog;
    Question question;
    private RequestQueue mRequest;
    DatabaseHandler db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = getApplicationContext();
    }

    @Override
    public void initView() {
        indikatorWrapper = (LinearLayout) findViewById(R.id.indikator_pager);
        mPager = (ViewPager) findViewById(R.id.pager);
        title = (TextView) findViewById(R.id.title);
        pageListener = new PageListener();
        mPager.setOnPageChangeListener(pageListener);
        ok = (Button) findViewById(R.id.ok);
        Typeface tf = Typeface.createFromAsset(getApplicationContext().getAssets(), "BREVIA-BLACK.OTF");
        title.setTypeface(tf);

        //
        db = new DatabaseHandler(context);
        close = (ImageView) findViewById(R.id.close);
        mRequest = Volley.newRequestQueue(getApplicationContext());
        close = (ImageView) findViewById(R.id.close);


    }

    @Override
    public void setUICallbacks() {
        ok.setOnClickListener(this);
        close.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        if (v == ok) {
//            if (preferences.getString(Preferences.ID, "").equalsIgnoreCase("") || preferences.getString(Preferences.BIRTH_DAY, "").equalsIgnoreCase("")) {
//                dialog2("tidak ada pasien yang terdaftar, daftarkan terlebih dahulu pada menu Sesi Pasien", "Tidak dapat dilanjutkan");
//            } else {
            getPdf("0", "M-CHAT-F");
        }
//        }
        if (v == close) {
            Intent i = new Intent(getApplicationContext(), MainActivity.class);
            startActivity(i);
            finish();
        }
    }


    private void getPdf(final String ID, final String code) {
        progresDialog(false, getResources().getString(R.string.MemuatData));
        StringRequest listdata = new StringRequest(Request.Method.GET,
                ApiReferences.getPdf(code, ID, preferences.getString(Preferences.LANGUAGE, "")),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        dialog.dismiss();
                        try {
                            if (response.equals("[]")) {
                                dialog(getResources().getString(R.string.BelumAdaData), getResources().getString(R.string.Pesan));
                            } else {
                                Message msg = Parser.getMessage(response);
                                if (msg.getStatus().equals("1")) {
                                    Log.i("RESULT JSON", response.toString());
                                    Pdf pdf = Parser.getPdf(response);
                                    Intent i = new Intent(getApplicationContext(), MchatrfPdfActivity.class);
                                    i.putExtra("url", pdf.getObjects().getFilename());
                                    i.putExtra("id", pdf.getObjects().getId());
                                    i.putExtra("menu", pdf.getObjects().getMenu());
                                    i.putExtra("category", pdf.getObjects().getCategory());
                                    startActivity(i);
                                } else {
                                    dialog(msg.getMessages(), getResources().getString(R.string.Pesan));
                                }
                            }
                        } catch (JsonSyntaxException e) {
                            dialog(getResources().getString(R.string.KesalahanServer), getResources().getString(R.string.Perhatian));
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dialog.dismiss();
                Pdf pdf = db.getFilesObject(Integer.valueOf(ID));
                if (!pdf.getObjects().equals("") && pdf.getObjects() != null) {
                    Intent i = new Intent(getApplicationContext(), MchatrfPdfActivity.class);
                    i.putExtra("url", pdf.getObjects().getFilename());
                    i.putExtra("id", pdf.getObjects().getId());
                    i.putExtra("menu", pdf.getObjects().getMenu());
                    i.putExtra("category", pdf.getObjects().getCategory());
                    startActivity(i);
                } else {
                    dialog(getResources().getString(R.string.NoConnection), getResources().getString(R.string.Perhatian));
                }
            }
        });
        listdata.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        mRequest.add(listdata);
    }

    @Override
    public int getLayout() {
        return R.layout.activity_mchat_rf;
    }

    @Override
    public void updateUI() {
        indikatorWrapper.removeAllViewsInLayout();
        fragmentList.clear();
        Bundle bn = getIntent().getExtras();
        if (bn != null) {
            String respon = bn.getString("result");
            artikel = Parser.getArtikel(respon);
            //   title.setText(artikel.getObjects().get(0).getTitle());
            for (int i = 0; i < artikel.getObjects().size(); i++) {
                fragmentList.add(new ArtikelBlueFragment(i, artikel));
                View child = getLayoutInflater().inflate(R.layout.indikator_pager, null);
                View child2 = getLayoutInflater().inflate(R.layout.indikator_pager_blue, null);
                if (i == 0) {
                    indikatorWrapper.addView(child2);
                } else {
                    indikatorWrapper.addView(child);
                }
            }

            setupFrae();
            if (artikel.getObjects().size() > 1) {
                indikatorWrapper.setVisibility(View.VISIBLE);
            } else {
                indikatorWrapper.setVisibility(View.GONE);
            }
        }


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

    }

    private void progresDialog(boolean horizontal, String msg) {
        dialog = new MaterialDialog.Builder(this)
                .title(msg)
                .content(getResources().getString(R.string.MohonTunggu))
                .progress(true, 0)
                .progressIndeterminateStyle(horizontal)
                .cancelable(false)
                .show();
    }

    public void dialog(String content, String msg) {
        new MaterialDialog.Builder(this)
                .title(msg)
                .content(content)
                .positiveText(getResources().getString(R.string.MuatUlang))
                .negativeText(getResources().getString(R.string.Kembali))
                .callback(new MaterialDialog.ButtonCallback() {
                    @Override
                    public void onPositive(MaterialDialog dialog) {

                    }

                    @Override
                    public void onNegative(MaterialDialog dialog) {
                        super.onNegative(dialog);
                    }

                })
                .show();
    }


    public void dialog2(String content, String msg) {
        new MaterialDialog.Builder(this)
                .title(msg)
                .content(content)
                .positiveText("OK")
                .callback(new MaterialDialog.ButtonCallback() {
                    @Override
                    public void onPositive(MaterialDialog dialog) {

                    }

                })
                .show();
    }


    @Override
    public void onBackPressed() {
        // Intent i = new Intent(getApplicationContext(), MainActivity.class);
        //  startActivity(i);
        finish();
    }

    private class PageListener extends ViewPager.SimpleOnPageChangeListener {
        public void onPageSelected(int position) {
            currentPage = position;
            // title.setText(artikel.getObjects().get(position).getTitle());
            indikatorWrapper.removeAllViewsInLayout();
            for (int i = 0; i < artikel.getObjects().size(); i++) {
                View child = getLayoutInflater().inflate(R.layout.indikator_pager, null);
                View child2 = getLayoutInflater().inflate(R.layout.indikator_pager_blue, null);
                if (i == position) {
                    indikatorWrapper.addView(child2);
                } else {
                    indikatorWrapper.addView(child);
                }
            }

        }
    }


    private void setupFrae() {
        mPagerAdapter = new PagerAdapter(getSupportFragmentManager(),
                ArtikelPagerMchatrfActivity.this, fragmentList);
        mPager.setAdapter(mPagerAdapter);

    }

}
