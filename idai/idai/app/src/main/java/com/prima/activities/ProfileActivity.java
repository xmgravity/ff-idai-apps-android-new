package com.prima.activities;

import android.app.Dialog;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.google.gson.JsonObject;
import com.google.gson.JsonSyntaxException;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.prima.MainActivity;
import com.prima.Preferences;
import com.prima.controllers.ApiReferences;
import com.prima.controllers.Parser;
import com.prima.entities.DaftarAnak;
import com.prima.entities.Message;
import com.prima.helpers.DatabaseHandler;
import com.prima.helpers.HexagonMaskView;
import com.soundcloud.android.crop.Crop;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import prima.test.prima.R;

/**
 * Created by Codelabs on 19/08/2015.
 */
public class ProfileActivity extends BaseActivity implements View.OnClickListener {
    //anak
    private RequestQueue mRequest;
    ArrayList<LinearLayout> days = new ArrayList<LinearLayout>();
    DatabaseHandler db;

    //ortu
    TextView address_value, gender_value, dob_value, fullname_value, handphone_value, email_value;

    //default
    TextView title, username, sub;
    LinearLayout subspesialisasi, profil_orangtua_layout, list, headerContainer, tambah_profil_anak;
    ScrollView profil_anak_layout;
    RelativeLayout praktik, profil_orangtua, profil_anak;
    ImageView close, back, profile_image, add;
    Button buttonOrangtua, buttonAnak, ubahprofile;
    MaterialDialog dialog;
    private Uri selectedImage;
    private String imagePath;
    private File file;
    HexagonMaskView hexa;
    private Uri resultImage;


    @Override
    public void initView() {

        db = new DatabaseHandler(context);
        mRequest = Volley.newRequestQueue(getApplicationContext());
        list = (LinearLayout) findViewById(R.id.list_daftar_anak);

        address_value = (TextView) findViewById(R.id.profil_orangtua_address_value);
        dob_value = (TextView) findViewById(R.id.profil_orangtua_dob_value);
        gender_value = (TextView) findViewById(R.id.profil_orangtua_gender_value);
        fullname_value = (TextView) findViewById(R.id.profil_orangtua_fullname_value);
        email_value = (TextView) findViewById(R.id.profil_orangtua_email_value);
        handphone_value = (TextView) findViewById(R.id.profil_orangtua_handphone_value);
        subspesialisasi = (LinearLayout) findViewById(R.id.subspesialisasi);
        profil_orangtua_layout = (LinearLayout) findViewById(R.id.profil_orangtua_layout);
        tambah_profil_anak = (LinearLayout) findViewById(R.id.tambah_profil_anak);
        profil_anak_layout = (ScrollView) findViewById(R.id.profil_anak_layout);
        praktik = (RelativeLayout) findViewById(R.id.praktik);
        profil_orangtua = (RelativeLayout) findViewById(R.id.profil_orangtua);
        profil_anak = (RelativeLayout) findViewById(R.id.profil_anak);
        title = (TextView) findViewById(R.id.tv_actionbar_title);
        sub = (TextView) findViewById(R.id.sub);
        close = (ImageView) findViewById(R.id.close);
        back = (ImageView) findViewById(R.id.back);
        profile_image = (ImageView) findViewById(R.id.profile_image);
        ubahprofile = (Button) findViewById(R.id.ubahprofile);
        buttonOrangtua = (Button) findViewById(R.id.buttonOrangtua);
        buttonAnak = (Button) findViewById(R.id.buttonAnak);
        headerContainer = (LinearLayout) findViewById(R.id.header_container);

        headerContainer.setVisibility(View.GONE);
        //set font
        Typeface tf = Typeface.createFromAsset(getApplicationContext().getAssets(), "BREVIA-BLACK.OTF");
        title.setTypeface(tf);
        title.setText("PROFIL");
        hexa = new HexagonMaskView();
        Glide.with(ProfileActivity.this)
                .load(preferences.getString(Preferences.FOTO, ""))
                .asBitmap()
                .override(200, 200)
                .into(new SimpleTarget<Bitmap>() {
                    @Override
                    public void onResourceReady(Bitmap bitmap, GlideAnimation anim) {
                        Bitmap b = hexa.getHexagonShape(hexa.scaleCenterCrop(bitmap, 200, 200));
                        profile_image.setImageBitmap(b);
                    }
                });
        username = (TextView) findViewById(R.id.username);
        if (preferences.getString(Preferences.TIPE_USER, "").equals("awam")) {
            username.setText(preferences.getString(Preferences.NAME, ""));
        }else{
            username.setText(getResources().getString(R.string.PendiriIdai));
        }

        if (!preferences.getString(Preferences.ADDRESS, "").equals("")) {
            address_value.setText(preferences.getString(Preferences.ADDRESS, ""));
        }

        if (!preferences.getString(Preferences.FULLNAME, "").equals("")) {
            fullname_value.setText(preferences.getString(Preferences.FULLNAME, ""));
        }

        if (!preferences.getString(Preferences.EMAIL, "").equals("")) {
            email_value.setText(preferences.getString(Preferences.EMAIL, ""));
        }

        if (!preferences.getString(Preferences.HANDPHONE, "").equals("")) {
            handphone_value.setText(preferences.getString(Preferences.HANDPHONE, ""));
        }

        if (!preferences.getString(Preferences.DOB, "").equals("")) {
            dob_value.setText(preferences.getString(Preferences.DOB, ""));
        }

        if (!preferences.getString(Preferences.GENDER, "").equals("")) {
            if(preferences.getString(Preferences.GENDER, "").equalsIgnoreCase("m")){
                gender_value.setText("Laki-laki");
            }else{
                gender_value.setText("Perempuan");
            }

        }

        if (!preferences.getString(Preferences.SUBSPESIALISASI, "").equals("")) {
            sub.setText(subSpesialisasi(preferences.getString(Preferences.SUBSPESIALISASI, "")));
        }


        profil_orangtua_layout.setVisibility(View.GONE);

        if (preferences.getString(Preferences.TIPE_USER, "").equals("awam")) {
            subspesialisasi.setVisibility(View.GONE);
            praktik.setVisibility(View.GONE);
            buttonAnak.setVisibility(View.VISIBLE);
            buttonOrangtua.setVisibility(View.VISIBLE);
            tambah_profil_anak.setVisibility(View.VISIBLE);
        }else{
            profil_orangtua.setVisibility(View.GONE);
            profil_anak.setVisibility(View.GONE);
            buttonAnak.setVisibility(View.GONE);
            buttonOrangtua.setVisibility(View.GONE);
            tambah_profil_anak.setVisibility(View.GONE);
            subspesialisasi.setVisibility(View.VISIBLE);
            praktik.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void setUICallbacks() {
        profile_image.setOnClickListener(this);
        subspesialisasi.setOnClickListener(this);
        praktik.setOnClickListener(this);
        profil_anak.setOnClickListener(this);
        profil_orangtua.setOnClickListener(this);
        back.setOnClickListener(this);
        buttonOrangtua.setOnClickListener(this);
        buttonAnak.setOnClickListener(this);
        ubahprofile.setOnClickListener(this);
        tambah_profil_anak.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        if (v == subspesialisasi) {
            Intent i = new Intent(getApplicationContext(), SubSpesialisasiChooseActivity.class);
            startActivity(i);
            //  finish();
        } else if (v == praktik) {
            sendAnalytic("Praktik");
            Intent i = new Intent(getApplicationContext(), ListJadwalPraktikActivity.class);
            startActivity(i);
            // finish();
        } else if (v == back) {
            Intent i = new Intent(getApplicationContext(), MainActivity.class);
            startActivity(i);
            finish();
        } else if (v == profile_image) {
            showPickPhotoDialog();
        } else if (v == profil_anak) {
            Intent i = new Intent(getApplicationContext(), ProfileAnakActivity.class);
            startActivity(i);
            finish();
        } else if (v == profil_orangtua) {
            Intent i = new Intent(getApplicationContext(), ProfileOrangtuaActivity.class);
            startActivity(i);
            finish();
        } else if (v == buttonAnak) {
            profil_orangtua_layout.setVisibility(View.GONE);
            profil_anak_layout.setVisibility(View.VISIBLE);
        } else if (v == buttonOrangtua) {
            profil_orangtua_layout.setVisibility(View.VISIBLE);
            profil_anak_layout.setVisibility(View.GONE);
        } else if (v == tambah_profil_anak) {
            Intent i = new Intent(getApplicationContext(), TambahAnakActivity.class);
            startActivity(i);
            finish();
        }else if (v == ubahprofile) {
            Intent i = new Intent(getApplicationContext(), ProfileOrangtuaChangeActivity.class);
            startActivity(i);
            finish();
        }
    }


    @Override
    public int getLayout() {
        return R.layout.activity_profile;
    }

    @Override
    public void updateUI() {
        if (!preferences.getString(Preferences.SUBSPESIALISASI, "").equals("")) {
            sub.setText(subSpesialisasi(preferences.getString(Preferences.SUBSPESIALISASI, "")));
        }

        list.removeAllViewsInLayout();
        days.clear();
        if (preferences.getString(Preferences.TIPE_USER, "").equals("awam")){
            getData();
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1) {
            if (resultCode == RESULT_OK && data != null) {

                selectedImage = data.getData();
                String[] filePathColumn = {MediaStore.Images.Media.DATA};

                Cursor cursor = getContentResolver().query(selectedImage,
                        filePathColumn, null, null, null);
                cursor.moveToFirst();

                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                String picturePath = cursor.getString(columnIndex);
                imagePath = picturePath;
                cursor.close();

                if (imagePath != null) {
                    cropPicture();
                }

            }
        } else if (requestCode == 0) {
            if (resultCode == RESULT_OK) {

                final String[] imageColumns = {MediaStore.Images.Media._ID,
                        MediaStore.Images.Media.DATA};
                final String imageOrderBy = MediaStore.Images.Media._ID
                        + " DESC";
                Cursor cursor = getContentResolver().query(
                        MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                        imageColumns, null, null, imageOrderBy);
                int column_index_data = cursor
                        .getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
                cursor.moveToFirst();

                imagePath = cursor.getString(column_index_data);
                cursor.close();

                if (imagePath != null) {
                    cropPicture();
                } else {
                    String text = getResources().getString(R.string.FotoUlang);
                    Toast.makeText(context, text, Toast.LENGTH_LONG).show();
                }
            }
        } else if (requestCode == Crop.REQUEST_CROP && resultCode == RESULT_OK) {
            imagePath = this.getExternalFilesDir(null)+fileDir+filename;
            Ion.with(context)
                    .load("http://54.169.50.255/revamp/api/v1/user/updateProfilePicture")
                    .setHeader("token", preferences.getString(Preferences.ACCESS_TOKEN, ""))
                    .setTimeout(60 * 60 * 1000)
                    .setMultipartFile("profile_picture", new File(imagePath))
                    .asJsonObject()
                    .setCallback(new FutureCallback<JsonObject>() {
                        @Override
                        public void onCompleted(Exception e, JsonObject result) {
                            if (e != null) {
                                Toast.makeText(context, "Error uploading file", Toast.LENGTH_LONG).show();
                                return;
                            }
                            Toast.makeText(context, "File upload complete", Toast.LENGTH_LONG).show();
                            imagePath = context.getString(R.string.baseURL) + result.getAsJsonObject("data").get("profile_picture").getAsString();
                            editor.putString(Preferences.FOTO, imagePath);
                            Log.d("dbgbg", "FOTO: " + imagePath);
                            editor.commit();
                            Glide.with(ProfileActivity.this)
                                    .load(preferences.getString(Preferences.FOTO, ""))
                                    .asBitmap()
                                    .override(200, 200)
                                    .into(new SimpleTarget<Bitmap>() {
                                        @Override
                                        public void onResourceReady(Bitmap bitmap, GlideAnimation anim) {
                                            Bitmap b = hexa.getHexagonShape(hexa.scaleCenterCrop(bitmap, 200, 200));
                                            profile_image.setImageBitmap(b);
                                        }
                                    });
                        }
                    });
        }
    }


    @Override
    public void onBackPressed() {
        Intent i = new Intent(getApplicationContext(), MainActivity.class);
        startActivity(i);
        finish();
    }


    public String subSpesialisasi(String id) {
        if (id.equals("0")) {
            return "Alergi Imunologi";
        } else if (id.equals("1")) {
            return "Emergensi dan Rawat Intensif Anak (ERIA)";
        } else if (id.equals("2")) {
            return "Endokrinologi";
        } else if (id.equals("3")) {
            return "Gastrohepatologi";
        } else if (id.equals("4")) {
            return "Hermatologi - Onkologi";
        } else if (id.equals("5")) {
            return "Infeksi dan Penyakit Tropis";
        } else if (id.equals("6")) {
            return "Kardiologi";
        } else if (id.equals("7")) {
            return "Nefrologi";
        } else if (id.equals("8")) {
            return "Neonatologi";
        } else if (id.equals("9")) {
            return "Neurologi";
        } else if (id.equals("10")) {
            return "Nutrisi dan Penyakit Metabolik";
        } else if (id.equals("11")) {
            return "Pencitraan";
        } else if (id.equals("12")) {
            return "Respirologi";
        } else if (id.equals("13")) {
            return "Tumbuh Kembang Ped.Sos";
        } else if (id.equals("14")) {
            return "Umum";
        } else {
            return "";
        }
    }


    public void showPickPhotoDialog() {

        dialog = new MaterialDialog.Builder(this)
                .title("Pick Photo")
                .negativeText("Cancel")
                .items(R.array.photo)
                .itemsCallback(new MaterialDialog.ListCallback() {
                    @Override
                    public void onSelection(MaterialDialog dialog, View view, int which, CharSequence text) {
                        if (which == 0) {
                            pickFromCamera();
                        } else {
                            pickFromGalery();
                        }
                    }
                })
                .show();
    }


    private void pickFromCamera() {
        int requestCode = 0;
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss")
                .format(new Date());
        String imageFileName = "PHOTO" + timeStamp + "_";

        ContentValues values = new ContentValues();
        values.put(MediaStore.Images.Media.TITLE, imageFileName);
        selectedImage = getContentResolver().insert(
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);

        // create Intent to take a picture and return control to the calling
        // application

        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        intent.putExtra(MediaStore.EXTRA_OUTPUT, selectedImage);

        // start the image capture Intent
        startActivityForResult(intent, requestCode);
        dialog.dismiss();
    }

    private void pickFromGalery() {
        int requestCode = 1;
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss")
                .format(new Date());
        String imageFileName = "PHOTO" + timeStamp + "_";

        ContentValues values = new ContentValues();
        values.put(MediaStore.Images.Media.TITLE, imageFileName);
        selectedImage = getContentResolver().insert(
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);

        // create Intent to take a picture and return control to the calling
        // application

        Intent intent = new Intent(Intent.ACTION_PICK);
        intent.setType("image/*");
        intent.putExtra(MediaStore.EXTRA_OUTPUT, selectedImage);

        startActivityForResult(intent, requestCode);
        dialog.dismiss();
    }


    private void getData() {
        progresDialog(false, getResources().getString(R.string.MemuatData));
        StringRequest listdata = new StringRequest(Request.Method.GET,
                ApiReferences.getPasienForAwam(),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        dialog.dismiss();
                        try {
                            if (response.equals("[]")) {
                                dialog(getResources().getString(R.string.DataBelumTersedia), getResources().getString(R.string.Perhatian));
                            } else {
                                Message msg = Parser.getMessage(response);
                                if (msg.getStatus().equals("0")) {
                                    dialog(msg.getMessages(), getResources().getString(R.string.Perhatian));
                                } else {
                                    Log.i("RESULT JSON", response.toString());
                                    final DaftarAnak daftarAnakResult = Parser.getDaftarAnak(response);

                                    for (int i = 0; i < daftarAnakResult.getObjects().size(); i++) {
                                        View child = getLayoutInflater().inflate(R.layout.list_item_anak, null);
                                        final TextView child_detail_name = (TextView) child.findViewById(R.id.child_detail_name);
                                        TextView child_detail_gender = (TextView) child.findViewById(R.id.child_detail_gender);
                                        TextView child_name = (TextView) child.findViewById(R.id.child_name);
                                        TextView child_detail_age = (TextView) child.findViewById(R.id.child_detail_age);
                                        TextView child_detail_height = (TextView) child.findViewById(R.id.child_detail_height);
                                        TextView child_detail_weight = (TextView) child.findViewById(R.id.child_detail_weight);

                                        final Button ubahprofilanak = (Button) child.findViewById(R.id.ubahprofilanak);
                                        final String id_anak = daftarAnakResult.getObjects().get(i).getId();
                                        final String nama = daftarAnakResult.getObjects().get(i).getName();
                                        Log.d("dbgbg", "detail name: "+id_anak);
                                        ubahprofilanak.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {
                                                LayoutInflater li = LayoutInflater.from(getApplicationContext());
                                                View dialogView = li.inflate(R.layout.dialog_edit_hapus_imunisasi, null);

                                                Dialog dialog = new Dialog(context);
                                                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                                                dialog.setContentView(dialogView);

                                                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

                                                dialog.setCancelable(true);
                                                dialog.show();

                                                final Button edit_data = (Button) dialogView.findViewById(R.id.edit_data);
                                                final Button hapus_data = (Button) dialogView.findViewById(R.id.hapus_data);

                                                edit_data.setOnClickListener(new View.OnClickListener() {
                                                    @Override
                                                    public void onClick(View v) {
                                                        editor.putString(Preferences.ID, id_anak);
                                                        editor.putString(Preferences.CHILD_NAME, nama);
                                                        editor.commit();
                                                        Intent i = new Intent(ProfileActivity.this, ProfileAnakChangeActivity.class);
                                                        startActivity(i);
                                                        finish();
                                                    }
                                                });
                                                hapus_data.setOnClickListener(new View.OnClickListener() {
                                                    @Override
                                                    public void onClick(View v) {
                                                        Log.d("dbgbg", "Hapus anak");
                                                        deleteChild(id_anak);
                                                    }
                                                });
                                            }

                                        });
                                        LinearLayout day = (LinearLayout) child.findViewById(R.id.day);

                                        child_detail_name.setId(i);
                                        child_detail_name.setText(daftarAnakResult.getObjects().get(i).getId());
                                        child_name.setId(i);
                                        child_name.setText(daftarAnakResult.getObjects().get(i).getName());
                                        child_detail_height.setId(i);
                                        child_detail_height.setText(daftarAnakResult.getObjects().get(i).getTinggi());
                                        child_detail_weight.setId(i);
                                        child_detail_weight.setText(daftarAnakResult.getObjects().get(i).getBerat());
                                        child_detail_gender.setId(i);
                                        if(daftarAnakResult.getObjects().get(i).getGender().toString().equals("m")){
                                            child_detail_gender.setText("Laki-laki");
                                        }else {
                                            child_detail_gender.setText("Perempuan");
                                        }

                                        child_detail_age.setId(i);
                                        child_detail_age.setText(daftarAnakResult.getObjects().get(i).getDob());

                                        child.setId(i);

                                        list.addView(child);


                                    }


                                    for (int i = 0; i < days.size(); i++) {
                                        View v = list.getChildAt(i);
                                        if (days.get(i).getChildCount() == 0) {
                                            list.removeView(v);
                                        }
                                    }

                                }
                            }
                        } catch (JsonSyntaxException e) {
                            dialog(getResources().getString(R.string.KesalahanServer), getResources().getString(R.string.Perhatian));
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                NetworkResponse response = error.networkResponse;
                if (error instanceof ServerError && response != null) {
                    try {
                        String res = new String(response.data,
                                HttpHeaderParser.parseCharset(response.headers, "utf-8"));
                        // Now you can use any deserializer to make sense of data
                        JSONObject obj = new JSONObject(res);
                        dialog(res.toString(), getResources().getString(R.string.Perhatian));
                    } catch (UnsupportedEncodingException e1) {
                        // Couldn't properly decode data to string
                        e1.printStackTrace();
                    } catch (JSONException e2) {
                        // returned data is not JSONObject?
                        e2.printStackTrace();
                    }
                }
            }
        }) {

            public Map<String, String> getHeaders() {
                Map<String, String>  params = new HashMap<String, String>();
                params.put("token", preferences.getString(Preferences.ACCESS_TOKEN, ""));

                return params;
            }
        };

        listdata.setRetryPolicy(new DefaultRetryPolicy(
                20000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        mRequest.add(listdata);
    }

    public void deleteChild(String child_name) {
        Log.d("dbgbg", "delete child called");
        StringRequest postlogin = new StringRequest(Request.Method.DELETE,
                ApiReferences.deleteChild(child_name), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    Log.i("RESULT JSON", response.toString());
                    Message message = Parser.getMessage(response
                            .toString());
                    if (message.getStatus().equalsIgnoreCase("1")) {
                        dialog(getResources().getString(R.string.DataBerhasilDiubah), getResources().getString(R.string.Perhatian));

                        Intent i = new Intent(getApplicationContext(), ProfileActivity.class);
                        startActivity(i);
                        finish();
                    }
                } catch (JsonSyntaxException e) {
                    dialog(getResources().getString(R.string.KesalahanServer), getResources().getString(R.string.Perhatian));
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                NetworkResponse response = error.networkResponse;
                if (error instanceof ServerError && response != null) {
                    try {
                        String res = new String(response.data,
                                HttpHeaderParser.parseCharset(response.headers, "utf-8"));
                        // Now you can use any deserializer to make sense of data
                        JSONObject obj = new JSONObject(res);
                        dialog(res.toString(), getResources().getString(R.string.Perhatian));
                    } catch (UnsupportedEncodingException e1) {
                        // Couldn't properly decode data to string
                        e1.printStackTrace();
                    } catch (JSONException e2) {
                        // returned data is not JSONObject?
                        e2.printStackTrace();
                    }
                }
            }
        }) {

            public Map<String, String> getHeaders() {
                Map<String, String>  params = new HashMap<String, String>();
                params.put("token", preferences.getString(Preferences.ACCESS_TOKEN, ""));

                return params;
            }

            protected Map<String, String> getParams() {
                HashMap<String, String> params = new HashMap<String, String>();
                return params;
            }
        };
        postlogin.setRetryPolicy(new DefaultRetryPolicy(
                20000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        mRequest.add(postlogin);
        Log.d("dbgbg", "delete child ended");
    }

    public void dialog(String content, String msg) {
        new MaterialDialog.Builder(this)
                .title(msg)
                .content(content)
                .positiveText("Ok")
                .callback(new MaterialDialog.ButtonCallback() {
                    @Override
                    public void onPositive(MaterialDialog dialog) {
                    }

                })
                .show();
    }

    private void progresDialog(boolean horizontal, String msg) {
        dialog = new MaterialDialog.Builder(this)
                .title(msg)
                .content(getResources().getString(R.string.MohonTunggu))
                .progress(true, 0)
                .progressIndeterminateStyle(horizontal)
                .cancelable(false)
                .show();
    }

    private void cropPicture() {
        if (!fileExists(fileDir)) {
            File newDir = new File(this.getExternalFilesDir(null)+fileDir);
            newDir.mkdirs();
        }
        if (fileExists(fileDir+filename)) {
            deleteFile(fileDir+filename);
        }
        File imgFile = new File(this.getExternalFilesDir(null)+fileDir, filename);
        resultImage = Uri.fromFile(imgFile);
        Crop.of(selectedImage, resultImage).asSquare().start(this);
    }

    public static String convertStreamToString(InputStream is) throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();
        String line = null;
        while ((line = reader.readLine()) != null) {
            sb.append(line).append("\n");
        }
        reader.close();
        return sb.toString();
    }

    public static String getStringFromFile (String filePath) throws Exception {
        File fl = new File(filePath);
        FileInputStream fin = new FileInputStream(fl);
        String ret = convertStreamToString(fin);
        //Make sure you close all streams.
        fin.close();
        return ret;
    }
}
