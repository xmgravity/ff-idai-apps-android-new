package com.prima.activities;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.prima.Preferences;
import com.prima.adapters.PagerAdapter;
import com.prima.controllers.Parser;
import com.prima.entities.Question;
import com.prima.fragments.Psc17QuesionerFragment;
import com.prima.helpers.NonSwipeAbleViewPager;

import java.util.ArrayList;
import java.util.List;

import prima.test.prima.R;

/**
 * Created by Codelabs on 07/08/2015.
 */
public class Psc17QuestionActivity extends BaseActivity implements View.OnClickListener {
    private ImageView close;
    private ImageView back;
    private Button ok;
    private Context mContext;
    private NonSwipeAbleViewPager mPager;
    private PagerAdapter mPagerAdapter;
    private static int currentPage;
    private PageListener pageListener;
    private static TextView title, page, title_question, sub_title_question;
    private static ProgressBar progres;
    static int progresValue;
    static List<Fragment> fragmentList = new ArrayList<Fragment>();
    static Question question;
    private ArrayList<String> ansewer = new ArrayList<String>();
    private ArrayList<String> answerInternalisasi = new ArrayList<String>();
    private ArrayList<String> answerEksternalisasi = new ArrayList<String>();
    private ArrayList<String> answerPerhatian = new ArrayList<String>();
    private ArrayList<String> kode = new ArrayList<String>();
    private ArrayList<String> tipe = new ArrayList<String>();

    public int getMax_score() {
        return max_score;
    }

    public void setMax_score(int max_score) {
        this.max_score = max_score;
    }

    private int max_score = 0;



    public ArrayList<String> getKode() {
        return kode;
    }

    public void setKode(ArrayList<String> kode) {
        this.kode = kode;
    }

    public ArrayList<String> getTipe() {
        return kode;
    }

    public void setTipe(ArrayList<String> tipe) {
        this.tipe = tipe;
    }

    public List<Fragment> getFragmentList() {
        return fragmentList;
    }

    public ArrayList<String> getAnsewer() {
        return ansewer;
    }

    public void setAnsewer(ArrayList<String> ansewer) {
        this.ansewer = ansewer;
    }

    public ArrayList<String> getAnswerInternalisasi() {
        return answerInternalisasi;
    }

    public void setAnswerInternalisasi(ArrayList<String> answerInternalisasi) {
        this.answerInternalisasi = answerInternalisasi;
    }

    public ArrayList<String> getAnswerEksternalisasi() {
        return answerInternalisasi;
    }

    public void setAnswerEksternalisasi(ArrayList<String> answerEksternalisasi) {
        this.answerEksternalisasi = answerEksternalisasi;
    }

    public ArrayList<String> getAnswerPerhatian() {
        return answerPerhatian;
    }

    public void setAnswerPerhatian(ArrayList<String> answerPerhatian) {
        this.answerPerhatian = answerPerhatian;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = getApplicationContext();
        currentPage = 0;
    }

    @Override
    public void initView() {
        title_question = (TextView) findViewById(R.id.title_question);
        sub_title_question = (TextView) findViewById(R.id.sub_title_question);
        page = (TextView) findViewById(R.id.page);
        progres = (ProgressBar) findViewById(R.id.progres);
        progres.setProgress(35);
        back = (ImageView) findViewById(R.id.back);
        mPager = (NonSwipeAbleViewPager) findViewById(R.id.pager);
        close = (ImageView) findViewById(R.id.close);
        pageListener = new PageListener();
        mPager.setOnPageChangeListener(pageListener);
        title = (TextView) findViewById(R.id.tv_actionbar_title);
        Typeface tf = Typeface.createFromAsset(getApplicationContext().getAssets(), "BREVIA-BLACK.OTF");
        title.setTypeface(tf);
        title.setText("PSC-17");
        getData();

    }

    @Override
    public void setUICallbacks() {
        back.setOnClickListener(this);
        close.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        if (v == back) {
            if (currentPage > 0) {
                currentPage--;
                mPager.setCurrentItem(currentPage, true);
            } else {
                if (preferences.getString(Preferences.ID, "").equals("1")) {
                    editor.putString(Preferences.BIRTH_DAY, "");
                    editor.putString(Preferences.JENIS_KELAMIN, "");
                    editor.putString(Preferences.BERAT, "");
                    editor.putString(Preferences.TINGGI, "");
                    editor.putString(Preferences.ID, "");
                    editor.commit();
                }
                //  Intent i = new Intent(getApplicationContext(), PgcsActivity.class);
                // startActivity(i);
                finish();
            }
        } else if (v == close) {
            if (preferences.getString(Preferences.ID, "").equals("1")) {
                editor.putString(Preferences.BIRTH_DAY, "");
                editor.putString(Preferences.JENIS_KELAMIN, "");
                editor.putString(Preferences.BERAT, "");
                editor.putString(Preferences.TINGGI, "");
                editor.putString(Preferences.ID, "");
                editor.commit();
            }
            //  Intent i = new Intent(getApplicationContext(), PgcsActivity.class);
            //   startActivity(i);
            finish();
        }
    }


    void getData() {
        Bundle bn = getIntent().getExtras();
        if (bn != null) {
            String response = bn.getString("respon");
            setupFrae();
            Log.i("RESULT JSON", response);
            question = new Question();
            question = Parser.getQuestion(response);
            title_question.setText(question.getObjects().get(0).getTitle());
            sub_title_question.setText(sub_title_question.getResources().getString(R.string.subskalaPerilaku2) + " " + question.getObjects().get(0).getSub_title());
            fragmentList.clear();
            for (int i = 0; i < question.getObjects().size(); i++) {
                for (int j = 0; j < question.getObjects().get(i).getAnswer_option().size(); j++) {
                    if (j == 0) {
                        max_score = max_score + Integer.valueOf(question.getObjects().get(i).getAnswer_option().get(j).getScore().replace(".00",""));
                    }
                }

                fragmentList.add(new Psc17QuesionerFragment(question.getObjects().get(i), i));
                ansewer.add("");
                answerInternalisasi.add("");
                answerEksternalisasi.add("");
                answerPerhatian.add("");
                kode.add("");
            }
            progresValue = 100 / fragmentList.size();
            progres.setProgress(progresValue);
            setupFrae();

            page.setText(page.getResources().getString(R.string.dari2) + " " + fragmentList.size() + " " + page.getResources().getString(R.string.penilaian));
        }
    }


    @Override
    public int getLayout() {
        return R.layout.activity_psc17_question;
    }

    @Override
    public void updateUI() {

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

    }


    private static class PageListener extends ViewPager.SimpleOnPageChangeListener {


        public void onPageSelected(int position) {
            currentPage = position;
            page.setText(currentPage + 1 + " " + page.getResources().getString(R.string.dari) + " "+ fragmentList.size() + " " + page.getResources().getString(R.string.penilaian));
            progres.setProgress(progresValue * (position + 1));
            title_question.setText(question.getObjects().get(position).getTitle());
            sub_title_question.setText(sub_title_question.getResources().getString(R.string.subskalaPerilaku2) + " " + question.getObjects().get(position).getSub_title());

            if ((position + 1) == question.getObjects().size()) {
                progres.setProgress(100);
            }
        }
    }


    private void setupFrae() {
        mPagerAdapter = new PagerAdapter(getSupportFragmentManager(),
                Psc17QuestionActivity.this, fragmentList);
        mPager.setAdapter(mPagerAdapter);

    }

    @Override
    public void onBackPressed() {
        if (currentPage > 0) {
            currentPage--;
            mPager.setCurrentItem(currentPage, true);
        } else {
            if (preferences.getString(Preferences.ID, "").equals("1")) {
                editor.putString(Preferences.BIRTH_DAY, "");
                editor.putString(Preferences.JENIS_KELAMIN, "");
                editor.putString(Preferences.BERAT, "");
                editor.putString(Preferences.TINGGI, "");
                editor.putString(Preferences.ID, "");
                editor.commit();
            }
            //   Intent i = new Intent(getApplicationContext(), PgcsActivity.class);
            // startActivity(i);
            finish();
        }
    }

}
