package com.prima.activities;

import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.view.View;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;

import com.prima.MainActivity;
import com.prima.Preferences;

import java.util.Locale;

import prima.test.prima.R;

/**
 * Created by Codelabs on 21/09/2015.
 */
public class BahasaActivity extends BaseActivity implements View.OnClickListener{
    private ImageView back;
    private ImageView close;
    private TextView title, titletext1, titletext2, subtitle, subtitle1, titletext, subtitletext;

    @Override
    public void initView() {

        close = (ImageView) findViewById(R.id.close);
        close.setVisibility(View.GONE);
        back = (ImageView) findViewById(R.id.back);
        subtitletext = (TextView) findViewById(R.id.subtitle);
        titletext = (TextView) findViewById(R.id.title);
        title = (TextView) findViewById(R.id.tv_actionbar_title);
        final RadioButton rad1 = (RadioButton) findViewById(R.id.rad1);
        final RadioButton rad2 = (RadioButton) findViewById(R.id.rad2);

        Typeface tf = Typeface.createFromAsset(getApplicationContext().getAssets(), "BREVIA-BLACK.OTF");
        Typeface tf1 = Typeface.createFromAsset(getApplicationContext().getAssets(), "BREVIA-SEMIBOLD.OTF");

        title.setTypeface(tf);
        rad1.setTypeface(tf1);
        rad2.setTypeface(tf1);
        title.setText(getResources().getString(R.string.Bahasa));

        System.out.println("nandha bahasa "+preferences.getString(Preferences.LANGUAGE, ""));

        if (preferences.getString(Preferences.LANGUAGE, "").equalsIgnoreCase("id")) {
            rad1.setChecked(true);
            rad2.setChecked(false);
        } else {
            rad1.setChecked(false);
            rad2.setChecked(true);
        }


        rad1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println("Language changed to indonesian");
                rad1.setChecked(true);
                rad2.setChecked(false);
                editor.putString(Preferences.LANGUAGE, "id");
                editor.commit();

                Locale locale = new Locale(preferences.getString(Preferences.LANGUAGE, ""));
                Locale.setDefault(locale);
                Configuration config = getBaseContext().getResources().getConfiguration();
                config.locale = locale;
                getBaseContext().getResources().updateConfiguration(config,getBaseContext().getResources().getDisplayMetrics());


            }
        });

        rad2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println("Language changed to english");
                rad1.setChecked(false);
                rad2.setChecked(true);
                editor.putString(Preferences.LANGUAGE, "en");
                editor.commit();

                Locale locale = new Locale(preferences.getString(Preferences.LANGUAGE, ""));
                Locale.setDefault(locale);
                Configuration config = getBaseContext().getResources().getConfiguration();
                config.locale = locale;
                getBaseContext().getResources().updateConfiguration(config,getBaseContext().getResources().getDisplayMetrics());
            }
        });

    }


    @Override
    public void setUICallbacks() {
        back.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if (v == back) {
            Intent i = new Intent(this, MainActivity.class);
            startActivity(i);
            finish();

        }

    }



    @Override
    public int getLayout() {
        return R.layout.activity_bahasa;
    }

    @Override
    public void updateUI() {

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

    }


    @Override
    public void onBackPressed() {
        Intent i = new Intent(this, MainActivity.class);
        startActivity(i);
        finish();
    }




}
