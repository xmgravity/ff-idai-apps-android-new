package com.prima.activities;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.prima.MainActivity;
import com.prima.Preferences;
import com.prima.controllers.ApiReferences;
import com.prima.controllers.Parser;
import com.prima.entities.Pasien;
import com.prima.helpers.DateHelper;

import java.util.HashMap;
import java.util.Map;

import prima.test.prima.R;

/**
 * Created by Codelabs on 10/08/2015.
 */
public class BloodPressureActivity extends BaseActivity implements View.OnClickListener {
    private ImageView close;
    private Button lanjut;
    private Button source;
    private TextView txtSource, title;
    private TextView content;
    RelativeLayout source_wraper;
    DateHelper dateHelper;
    private RequestQueue mRequest;
    private MaterialDialog dialog;
    String sisto, diasto;


    @Override
    public void initView() {
        dateHelper = new DateHelper();
        mRequest = Volley.newRequestQueue(getApplicationContext());
        content = (TextView) findViewById(R.id.content);
        source_wraper = (RelativeLayout) findViewById(R.id.source_wraper);
        lanjut = (Button) findViewById(R.id.lanjut);
        close = (ImageView) findViewById(R.id.close);
        source = (Button) findViewById(R.id.btn_source);
        txtSource = (TextView) findViewById(R.id.source);
        txtSource.setVisibility(View.GONE);

        title = (TextView) findViewById(R.id.title);
        //set font
        Typeface tf = Typeface.createFromAsset(getApplicationContext().getAssets(), "Brevia-ExtraBlackItalic.otf");
        title.setTypeface(tf);
    }

    @Override
    public void setUICallbacks() {
        source.setOnClickListener(this);
        lanjut.setOnClickListener(this);
        close.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        if (v == lanjut) {
            if (preferences.getString(Preferences.ID, "").equalsIgnoreCase("") || preferences.getString(Preferences.BIRTH_DAY, "").equalsIgnoreCase("")) {
                // get prompts.xml view
                LayoutInflater li = LayoutInflater.from(getApplicationContext());
                View dialogView = li.inflate(R.layout.dialog_sesi_offline_blood_pressure, null);
                final Dialog dialog3;
                dialog3 = new Dialog(context);
                dialog3.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog3.setContentView(dialogView);
                //make transparent dialog_sesi_offline_pelod
                dialog3.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                dialog3.show();




                Button dialogLanjut = (Button) dialogView.findViewById(R.id.ok);
                final RadioButton rad1 = (RadioButton) dialogView.findViewById(R.id.rad1);
                final RadioButton rad2 = (RadioButton) dialogView.findViewById(R.id.rad2);
                rad1.setChecked(true);

                rad1.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        rad1.setChecked(true);
                        rad2.setChecked(false);
                    }
                });

                rad2.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        rad2.setChecked(true);
                        rad1.setChecked(false);
                    }
                });

                final EditText edt = (EditText) dialogView.findViewById(R.id.edt1);
                final EditText edt2 = (EditText) dialogView.findViewById(R.id.edt2);
                final EditText sistolik = (EditText) dialogView.findViewById(R.id.sistolik);
                final EditText diastolik = (EditText) dialogView.findViewById(R.id.diastolik);


                edt.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        showDatepickerDialog(edt);
                    }
                });

                dialogLanjut.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (edt.getText().toString().trim().equals("") ||
                                edt2.getText().toString().trim().equals("") ||
                                sistolik.getText().toString().trim().equals("") ||
                                diastolik.getText().toString().trim().equals("")) {
                            dialog(getResources().getString(R.string.IsiSeluruhIsian), getResources().getString(R.string.Perhatian));
                        } else {
                            String jk;
                            if (rad1.isChecked()) {
                                jk = "1";
                            } else if (rad2.isChecked()) {
                                jk = "0";
                            } else {
                                jk = "";
                            }
                            editor.putString(Preferences.TINGGI, edt2.getText().toString());
                            editor.putString(Preferences.BIRTH_DAY, edt.getText().toString());
                            editor.putString(Preferences.JENIS_KELAMIN, jk);
                            editor.putString(Preferences.ID, "1");
                            editor.commit();
                            sisto = sistolik.getText().toString().trim();
                            diasto = diastolik.getText().toString().trim();

                            if (!sisto.equals("") &&
                                    !diasto.equals("")) {
                                Intent i = new Intent(BloodPressureActivity.this, BloodPressureResultActivity.class);
                                i.putExtra("sistolik", sisto);
                                i.putExtra("diastolik", diasto);
                                startActivity(i);
                                dialog3.dismiss();
                            } else {
                                dialog(getResources().getString(R.string.IsiTekananDarah), getResources().getString(R.string.Perhatian));
                            }

                            // saveSesiPasien(jk, edt.getText().toString(), edt3.getText().toString(), edt2.getText().toString());

                        }
                    }
                });
            } else {
                // get prompts.xml view
                LayoutInflater li = LayoutInflater.from(getApplicationContext());
                View dialogView = li.inflate(R.layout.dialog_blood_pressure, null);

                final Dialog dialog;
                dialog = new Dialog(context);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(dialogView);
                //make transparent dialog_sesi_offline_pelod
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                dialog.show();
                final EditText sistolik = (EditText) dialogView.findViewById(R.id.sistolik);
                final EditText diastolik = (EditText) dialogView.findViewById(R.id.diastolik);

                Button dialogLanjut = (Button) dialogView.findViewById(R.id.lanjut);
                dialogLanjut.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (!sistolik.getText().toString().trim().equals("") &&
                                !diastolik.getText().toString().trim().equals("")) {
                            Intent i = new Intent(BloodPressureActivity.this, BloodPressureResultActivity.class);
                            i.putExtra("sistolik", sistolik.getText().toString());
                            i.putExtra("diastolik", diastolik.getText().toString());
                            sisto = sistolik.getText().toString().trim();
                            diasto = diastolik.getText().toString().trim();
                            startActivity(i);
                            //     finish();
                            dialog.dismiss();
                        } else {
                            dialog(getResources().getString(R.string.IsiTekananDarah), getResources().getString(R.string.Perhatian));
                        }

                    }
                });

            }
        } else if (v == close) {
            Intent i = new Intent(BloodPressureActivity.this, MainActivity.class);
            startActivity(i);
            finish();
        } else if (v == source) {
            if (txtSource.getVisibility() == View.GONE) {
                txtSource.setVisibility(View.VISIBLE);
            } else {
                txtSource.setVisibility(View.GONE);
            }
        }
    }


    @Override
    public int getLayout() {
        return R.layout.activity_kalkulator_hipertensi;
    }

    @Override
    public void updateUI() {
//        Bundle bn = getIntent().getExtras();
//        if (bn != null) {
//            String result = bn.getString("result");
//            Artikel artikel = Parser.getArtikel(result);
//            content.setText(artikel.getObjects().getDeskripsi());
//            Spannable s = (Spannable) Html.fromHtml("<a href=\"" + artikel.getObjects().getSumber() + "\">" + artikel.getObjects().getSumber() + "</a>");
//            URLSpan[] spans = s.getSpans(0, s.length(), URLSpan.class);
//            for (URLSpan span : spans) {
//                int start = s.getSpanStart(span);
//                int end = s.getSpanEnd(span);
//                s.removeSpan(span);
//                span = new URLSpan(span.getURL());
//                s.setSpan(span, start, end, 0);
//            }
//            txtSource.setText(s);
//            txtSource.setMovementMethod(LinkMovementMethod.getInstance());
//
//            if (artikel.getObjects().getSumber().trim().equals("")) {
//                txtSource.setVisibility(View.GONE);
//                source_wraper.setVisibility(View.GONE);
//                source.setVisibility(View.GONE);
//
//            }
//
//        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

    }

    private void saveSesiPasien(final String jenis_kelamin, final String tanggal_lahir, final String berat, final String tinggi) {
        progresDialog(false, getResources().getString(R.string.MenyimpanDataPasien));
        StringRequest postlogin = new StringRequest(Request.Method.POST,
                ApiReferences.insertPasien(), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                Log.i("RESULT JSON", response.toString());
                Pasien pasien = Parser.getPasien(response
                        .toString());
                if (pasien.getStatus().equalsIgnoreCase("1")) {
                    editor.putString(Preferences.ID, pasien.getObjects().getId());
                    editor.putString(Preferences.TINGGI, pasien.getObjects().getTinggi());
                    editor.putString(Preferences.JENIS_KELAMIN, pasien.getObjects().getJenis_kelamin());
                    editor.putString(Preferences.BIRTH_DAY, pasien.getObjects().getTanggal_lahir());
                    editor.commit();
                    dialog.dismiss();
                    dialogEmail("Kode : " + pasien.getObjects().getId() + "\nSimpan Code ini untuk pemeriksaan berikutnya.", "Pendaftaran Sesi Pasien Berhasil", pasien.getObjects().getId());
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                dialog.dismiss();
                dialog(getResources().getString(R.string.NoConnection), getResources().getString(R.string.Perhatian));
            }
        }) {

            protected Map<String, String> getParams() {
                HashMap<String, String> params = new HashMap<String, String>();
                params.put("jenis_kelamin", jenis_kelamin);
                params.put("tanggal_lahir", tanggal_lahir);
                params.put("tinggi", tinggi);
                return params;
            }
        };
        postlogin.setRetryPolicy(new DefaultRetryPolicy(
                20000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        mRequest.add(postlogin);
    }


    public void dialogEmail(String content, String msg, final String kode) {
        new MaterialDialog.Builder(this)
                .title(msg)
                .content(content)
                .autoDismiss(false)
                .cancelable(false)
                .positiveText(getResources().getString(R.string.KirimEmail))
                .negativeText(getResources().getString(R.string.tutup))
                .callback(new MaterialDialog.ButtonCallback() {
                    @Override
                    public void onPositive(MaterialDialog dialog) {
                        Intent sendEmail = new Intent(Intent.ACTION_SEND);
                        sendEmail.setType("text/plain");
                        sendEmail.putExtra(Intent.EXTRA_EMAIL, new String[]{""});
                        sendEmail.putExtra(Intent.EXTRA_SUBJECT, "Kode Pasien");
                        sendEmail.putExtra(Intent.EXTRA_TEXT, "Berikut ini adalah kode pasien anda.\nKode : " + kode);
                        startActivity(Intent.createChooser(sendEmail, "Email With"));
                    }

                    @Override
                    public void onNegative(MaterialDialog dialog) {
                        dialog.dismiss();
                        if (!sisto.equals("") &&
                                !diasto.equals("")) {
                            Intent i = new Intent(BloodPressureActivity.this, BloodPressureResultActivity.class);
                            i.putExtra("sistolik", sisto);
                            i.putExtra("diastolik", diasto);
                            startActivity(i);
                            //     finish();
                            dialog.dismiss();
                        } else {
                            dialog(getResources().getString(R.string.IsiTekananDarah), getResources().getString(R.string.Perhatian));
                        }

                    }
                })
                .show();
    }

    private void progresDialog(boolean horizontal, String msg) {
        dialog = new MaterialDialog.Builder(this)
                .title(msg)
                .content(getResources().getString(R.string.MohonTunggu))
                .progress(true, 0)
                .progressIndeterminateStyle(horizontal)
                .cancelable(false)
                .show();
    }

    private void showDatepickerDialog(final EditText txt) {
        dateHelper.showDatePicker(this, System.currentTimeMillis(), new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                int _year = year;
                int _month = monthOfYear + 1;
                int _day = dayOfMonth;
                String date = _year + "-" + _month + "-" + _day;
                String birthDate = dateHelper.formatDate(date);
                txt.setText(birthDate);
            }
        });
    }


    @Override
    public void onBackPressed() {
        Intent i = new Intent(BloodPressureActivity.this, MainActivity.class);
        startActivity(i);
        finish();
    }

    public void dialog(String content, String msg) {
        new MaterialDialog.Builder(BloodPressureActivity.this)
                .title(msg)
                .content(content)
                .positiveText("Ok")
                .callback(new MaterialDialog.ButtonCallback() {
                    @Override
                    public void onPositive(MaterialDialog dialog) {
                    }

                })
                .show();
    }
}
