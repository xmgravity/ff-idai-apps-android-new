package com.prima.helpers;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.prima.entities.Agenda;
import com.prima.entities.AllRedflag;
import com.prima.entities.Artikel;
import com.prima.entities.JadwalPraktek;
import com.prima.entities.MateriEdukasi;
import com.prima.entities.MenuAge;
import com.prima.entities.Notif;
import com.prima.entities.Pdf;
import com.prima.entities.Question;

import org.joda.time.LocalDate;
import org.joda.time.Months;

import java.util.ArrayList;

/**
 * Created by Codelabs on 4/15/2015.
 */
public class DatabaseHandler extends SQLiteOpenHelper {

    // All Static variables
    // Database Version
    private static final int DATABASE_VERSION = 20;

    // Database Name
    private static final String DATABASE_NAME = "IDAI";


    //  table name
    private static final String TABLE_AGENDA = "Agenda";
    private static final String TABLE_PRAKTIK = "Praktik";
    private static final String TABLE_LOCAL_NOTIF = "Local_Notif";
    private static final String TABLE_ARTIKEL = "Artikel";
    private static final String TABLE_MENU_AGE = "Menu_Age";
    private static final String TABLE_QUESTIONNAIRE = "Questionnaire";
    private static final String TABLE_ANSWER = "Answer";
    private static final String TABLE_FILE = "File";
    private static final String TABLE_MEOT = "Meot";
    private static final String TABLE_AGE = "Age";


    // Agenda Table Columns names
    private static final String ID = "id";
    private static final String ID_CLAENDAR = "id_calendar";
    private static final String ID_AGENDA = "id_agenda";
    private static final String TANGGAL_KUNJUNGAN = "tanggal_kunjungan";
    private static final String JAM_MULAI = "jam_mulai";
    private static final String JAM_AKHIR = "jam_akhir";
    private static final String LOKASI = "lokasi";
    private static final String PENGINGAT = "pengingat";
    private static final String SUARA = "suara";
    private static final String GETAR = "getar";
    private static final String LATITUDE = "latitude";
    private static final String LONGITUDE = "longitude";
    private static final String NAMA_PASIEN = "nama_pasien";
    private static final String SUB_JUDUL = "sub_judul";

    //Table Notif
    private static final String IS_READ = "is_read";


    //ARTIKEL
    private static final String CODE_MENU = "code_menu";
    private static final String DESKRIPSI = "deskripsi";
    private static final String VERSION = "versi";
    private static final String SLIDE = "slide";
    private static final String SUMBER = "sumber";


    //Age
    private static final String MIN_AGE = "min_age";
    private static final String MAX_AGE = "max_age";
    private static final String AGE_STRING = "age_string";


    //Question
    private static final String QUESTIONNAIRE_ID = "questionnaire_id";
    private static final String TITLE = "title";
    private static final String SUB_TITLE = "subtitle";
    private static final String TYPE = "type";
    private static final String PATH = "path";
    private static final String KODE = "kode";
    private static final String QUESTION_VERSION = "question_version";


    //Answer
    private static final String OPTION_TYPE = "option_type";
    private static final String OPTION = "option";
    private static final String SCORE = "score";


    //UMUR
    private static final String NAME_MENU = "name_menu";
    private static final String RANGE_AGE = "range_age";

    //Materi Edukasi
    private static final String AGE_ID = "age_id";
    private static final String HEADER_PICTURE = "header_picture";
    private static final String CONTENT = "content";


    private static final String CATEGORY = "category";


    //Praktik
    private static final String ID_TEMPAT = "id_tempat";
    private static final String ID_PROFILE = "id_profile";
    private static final String ID_JADWAL = "id_jadwal";
    private static final String ID_SCHEDULES = "id_schedules";
    private static final String HARI = "hari";
    private static final String NAMA_PRAKTIK = "nama_praktik";
    private static final String ALAMAT = "alamat";
    private static final String TELP = "telp";
    private static final String STATUS = "status";
    private static final String IS_SENT = "is_sent";


    public DatabaseHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_PRAKTIK_TABLE = "CREATE TABLE IF NOT EXISTS " + TABLE_PRAKTIK + "("
                + ID + " TEXT PRIMARY KEY,"
                + ID_PROFILE + " TEXT,"
                + ID_SCHEDULES + " TEXT,"
                + NAMA_PRAKTIK + " TEXT,"
                + ALAMAT + " TEXT,"
                + TELP + " TEXT,"
                + LONGITUDE + " TEXT,"
                + LATITUDE + " TEXT,"
                + ID_JADWAL + " TEXT,"
                + HARI + " TEXT,"
                + JAM_MULAI + " TEXT,"
                + JAM_AKHIR + " TEXT)";
                //+ STATUS + " TEXT)";
        db.execSQL(CREATE_PRAKTIK_TABLE);


        String CREATE_AGENDA_TABLE = "CREATE TABLE IF NOT EXISTS " + TABLE_AGENDA + "("
                + ID + " TEXT PRIMARY KEY,"
                + ID_CLAENDAR + " TEXT,"
                + ID_AGENDA + " TEXT,"
                + TANGGAL_KUNJUNGAN + " TEXT,"
                + JAM_MULAI + " TEXT,"
                + JAM_AKHIR + " TEXT,"
                + LOKASI + " TEXT,"
                + PENGINGAT + " TEXT,"
                + SUARA + " TEXT,"
                + GETAR + " TEXT,"
                + LATITUDE + " TEXT,"
                + LONGITUDE + " TEXT,"
                + NAMA_PASIEN + " TEXT,"
                + SUB_JUDUL + " TEXT)";
        db.execSQL(CREATE_AGENDA_TABLE);


        String CREATE_NOTIF_TABLE = "CREATE TABLE IF NOT EXISTS " + TABLE_LOCAL_NOTIF + "("
                + ID_AGENDA + " TEXT,"
                + IS_READ + " TEXT)";
        db.execSQL(CREATE_NOTIF_TABLE);


        String CREATE_ARTIKEL_TABLE = "CREATE TABLE IF NOT EXISTS " + TABLE_ARTIKEL + "("
                + CODE_MENU + " TEXT,"
                + DESKRIPSI + " TEXT,"
                + SUMBER + " TEXT,"
                + VERSION + " TEXT,"
                + SLIDE + " TEXT,"
                + ID + " TEXT PRIMARY KEY)";
        db.execSQL(CREATE_ARTIKEL_TABLE);

        String CREATE_QUESTIONNAIRE_TABLE = "CREATE TABLE IF NOT EXISTS " + TABLE_QUESTIONNAIRE + "("
                + CODE_MENU + " TEXT,"
                + QUESTIONNAIRE_ID + " TEXT PRIMARY KEY,"
                + TITLE + " TEXT,"
                + SUB_TITLE + " TEXT,"
                + TYPE + " TEXT,"
                + PATH + " TEXT NULL,"
                + MIN_AGE + " INTEGER,"
                + MAX_AGE + " INTEGER,"
                + QUESTION_VERSION + " TEXT,"
                + AGE_ID + " TEXT,"
                + KODE + " TEXT NULL)";
        db.execSQL(CREATE_QUESTIONNAIRE_TABLE);

        String CREATE_ANSWER_TABLE = "CREATE TABLE IF NOT EXISTS " + TABLE_ANSWER + "("
                + ID + " INTEGER PRIMARY KEY,"
                + QUESTIONNAIRE_ID + " TEXT,"
                + OPTION_TYPE + " TEXT,"
                + OPTION + " TEXT,"
                + SCORE + " TEXT)";
        db.execSQL(CREATE_ANSWER_TABLE);


        String CREATE_MENU_AGE_TABLE = "CREATE TABLE IF NOT EXISTS " + TABLE_MENU_AGE + "("
                + ID + " INTEGER PRIMARY KEY,"
                + CODE_MENU + " TEXT,"
                + NAME_MENU + " TEXT,"
                + RANGE_AGE + " TEXT,"
                + MIN_AGE + " INTEGER,"
                + MAX_AGE + " INTEGER)";
        db.execSQL(CREATE_MENU_AGE_TABLE);


        String CREATE_FILE_TABLE = "CREATE TABLE IF NOT EXISTS " + TABLE_FILE + "("
                + ID + " INTEGER PRIMARY KEY,"
                + CODE_MENU + " TEXT,"
                + PATH + " TEXT,"
                + CATEGORY + " TEXT)";
        db.execSQL(CREATE_FILE_TABLE);

        String CREATE_MEOT_TABLE = "CREATE TABLE IF NOT EXISTS " + TABLE_MEOT + "("
                + ID + " INTEGER PRIMARY KEY,"
                + AGE_ID + " TEXT,"
                + TITLE + " TEXT,"
                + CONTENT + " TEXT,"
                + VERSION + " TEXT,"
                + PATH + " TEXT)";
        db.execSQL(CREATE_MEOT_TABLE);


        String CREATE_AGE_TABLE = "CREATE TABLE IF NOT EXISTS " + TABLE_AGE + "("
                + ID + " INTEGER PRIMARY KEY,"
                + AGE_ID + " TEXT,"
                + TITLE + " TEXT,"
                + CONTENT + " TEXT,"
                + VERSION + " TEXT,"
                + PATH + " TEXT)";
        db.execSQL(CREATE_AGE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_AGENDA);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_LOCAL_NOTIF);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_ARTIKEL);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_ANSWER);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_QUESTIONNAIRE);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_MENU_AGE);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_FILE);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_MEOT);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_PRAKTIK);
        // Create tables again
        onCreate(db);

    }

    /**
     * All CRUD(Create, Read, Update, Delete) Operations
     */

    public void addAgenda(Agenda.Agendas agenda) {
        SQLiteDatabase db = this.getWritableDatabase();

        if (getAgenda(agenda.getObjects().getId()) == null) {
            ContentValues values = new ContentValues();
            values.put(ID, agenda.getObjects().getId());
            values.put(ID_CLAENDAR, agenda.getObjects().getId_kalender());
            values.put(ID_AGENDA, agenda.getObjects().getId_agenda());
            values.put(TANGGAL_KUNJUNGAN, agenda.getObjects().getTanggal_kunjungan());
            values.put(JAM_MULAI, agenda.getObjects().getJam_mulai());
            values.put(JAM_AKHIR, agenda.getObjects().getJam_akhir());
            values.put(LOKASI, agenda.getObjects().getLokasi());
            values.put(PENGINGAT, agenda.getObjects().getPengingat());
            values.put(SUARA, agenda.getObjects().getSuara());
            values.put(GETAR, agenda.getObjects().getGetar());
            values.put(LATITUDE, agenda.getObjects().getLatitude());
            values.put(LONGITUDE, agenda.getObjects().getLongitude());
            values.put(NAMA_PASIEN, agenda.getObjects().getNama_pasien());
            values.put(SUB_JUDUL, agenda.getObjects().getSub_judul());

            long rowInserted = db.insert(TABLE_AGENDA, null, values);
            if (rowInserted != -1)
                Log.i("DB", "new row added");
            else
                Log.i("DB", "gagal");
            db.close(); // Closing database connection
        } else {
            deleteAgenda(agenda.getObjects().getId());

            ContentValues values = new ContentValues();
            values.put(ID, agenda.getObjects().getId());
            values.put(ID_CLAENDAR, agenda.getObjects().getId_kalender());
            values.put(ID_AGENDA, agenda.getObjects().getId_agenda());
            values.put(TANGGAL_KUNJUNGAN, agenda.getObjects().getTanggal_kunjungan());
            values.put(JAM_MULAI, agenda.getObjects().getJam_mulai());
            values.put(JAM_AKHIR, agenda.getObjects().getJam_akhir());
            values.put(LOKASI, agenda.getObjects().getLokasi());
            values.put(PENGINGAT, agenda.getObjects().getPengingat());
            values.put(SUARA, agenda.getObjects().getSuara());
            values.put(GETAR, agenda.getObjects().getGetar());
            values.put(LATITUDE, agenda.getObjects().getLatitude());
            values.put(LONGITUDE, agenda.getObjects().getLongitude());
            values.put(NAMA_PASIEN, agenda.getObjects().getNama_pasien());
            values.put(SUB_JUDUL, agenda.getObjects().getSub_judul());
            long rowInserted = db.insert(TABLE_AGENDA, null, values);
            if (rowInserted != -1)
                Log.i("DB", "new row added");
            else
                Log.i("DB", "gagal");
            db.close(); // Closing database connection
        }
    }


    public Agenda.Agendas getAgenda(String id) {
        Agenda.Agendas agenda = new Agenda().new Agendas();

        String selectQuery = "SELECT  * FROM " + TABLE_AGENDA
                + " WHERE " + ID + "=" + id;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            do {
                agenda.setId(cursor.getString(0));
                agenda.setId_kalender(cursor.getString(1));
                agenda.setId_agenda(cursor.getString(2));
                agenda.setTanggal_kunjungan(cursor.getString(3));
                agenda.setJam_mulai(cursor.getString(4));
                agenda.setJam_akhir(cursor.getString(5));
                agenda.setLokasi(cursor.getString(6));
                agenda.setPengingat(cursor.getString(7));
                agenda.setSuara(cursor.getString(8));
                agenda.setGetar(cursor.getString(9));
                agenda.setLatitude(cursor.getString(10));
                agenda.setLongitude(cursor.getString(11));
                agenda.setNama_pasien(cursor.getString(12));
                agenda.setSub_judul(cursor.getString(13));
            } while (cursor.moveToNext());
        }
        agenda.setObjects(agenda);
        return agenda;
    }


    public Agenda getListAgenda() {
        Agenda temp = new Agenda();
        ArrayList<Agenda.Agendas> listAgenda = new ArrayList<Agenda.Agendas>();

        String selectQuery = "SELECT  * FROM " + TABLE_AGENDA
                + " GROUP BY " + TANGGAL_KUNJUNGAN + " ORDER BY " + TANGGAL_KUNJUNGAN + " ASC";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);


        if (cursor.moveToFirst()) {
            do {
                ArrayList<Agenda.Agendas> agenda = new ArrayList<Agenda.Agendas>();
                Agenda.Agendas tempAgendas = new Agenda().new Agendas();

                tempAgendas.setTanggal_kunjungan(cursor.getString(3));

                String selectQuery2 = "SELECT  * FROM " + TABLE_AGENDA + " WHERE " + TANGGAL_KUNJUNGAN + "='" + cursor.getString(3) + "' ORDER BY " + TANGGAL_KUNJUNGAN + " ASC";
                SQLiteDatabase db2 = this.getWritableDatabase();
                Cursor cursor2 = db2.rawQuery(selectQuery2, null);

                if (cursor2.moveToFirst()) {
                    do {
                        Agenda.Agendas tempAgenda = new Agenda().new Agendas();
                        tempAgenda.setId(cursor2.getString(0));
                        tempAgenda.setId_kalender(cursor2.getString(1));
                        tempAgenda.setId_agenda(cursor2.getString(2));
                        tempAgenda.setJam_mulai(cursor2.getString(4));
                        tempAgenda.setJam_akhir(cursor2.getString(5));
                        tempAgenda.setLokasi(cursor2.getString(6));
                        tempAgenda.setPengingat(cursor2.getString(7));
                        tempAgenda.setSuara(cursor2.getString(8));
                        tempAgenda.setGetar(cursor2.getString(9));
                        tempAgenda.setLatitude(cursor2.getString(10));
                        tempAgenda.setLongitude(cursor2.getString(11));
                        tempAgenda.setNama_pasien(cursor2.getString(12));
                        tempAgenda.setSub_judul(cursor2.getString(13));

                        agenda.add(tempAgenda);
                    } while (cursor2.moveToNext());
                }

                tempAgendas.setJadwal(agenda);

                listAgenda.add(tempAgendas);
            } while (cursor.moveToNext());
        }

        temp.setObjects(listAgenda);
        return temp;

    }


    public void deleteAgenda(String id) {
        SQLiteDatabase db = this.getWritableDatabase();
        String deleteQuery = "DELETE FROM " + TABLE_AGENDA
                + " WHERE " + ID + "=" + id;
        db.execSQL(deleteQuery);
    }


    public Agenda getAllAgenda() {
        Agenda objAgenda = new Agenda();
        ArrayList<Agenda.Agendas> item = new ArrayList<Agenda.Agendas>();
        String selectQuery = "SELECT  * FROM " + TABLE_AGENDA;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            do {
                Agenda.Agendas agenda = new Agenda().new Agendas();
                agenda.setId(cursor.getString(0));
                agenda.setId_kalender(cursor.getString(1));
                agenda.setId_agenda(cursor.getString(2));
                agenda.setTanggal_kunjungan(cursor.getString(3));
                agenda.setJam_mulai(cursor.getString(4));
                agenda.setJam_akhir(cursor.getString(5));
                agenda.setLokasi(cursor.getString(6));
                agenda.setPengingat(cursor.getString(7));
                agenda.setSuara(cursor.getString(8));
                agenda.setGetar(cursor.getString(9));
                agenda.setLatitude(cursor.getString(10));
                agenda.setLongitude(cursor.getString(11));
                agenda.setNama_pasien(cursor.getString(12));
                agenda.setSub_judul(cursor.getString(13));
                item.add(agenda);
            } while (cursor.moveToNext());
        }

        objAgenda.setObjects(item);
        return objAgenda;
    }


    public Agenda getAllAgendaSortedByTIme() {
        Agenda objAgenda = new Agenda();
        ArrayList<Agenda.Agendas> item = new ArrayList<Agenda.Agendas>();
        String selectQuery = "SELECT  * FROM " + TABLE_AGENDA + " ORDER BY " + TANGGAL_KUNJUNGAN + "," + JAM_MULAI + " ASC";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            do {
                Agenda.Agendas agenda = new Agenda().new Agendas();
                agenda.setId(cursor.getString(0));
                agenda.setId_kalender(cursor.getString(1));
                agenda.setId_agenda(cursor.getString(2));
                agenda.setTanggal_kunjungan(cursor.getString(3));
                agenda.setJam_mulai(cursor.getString(4));
                agenda.setJam_akhir(cursor.getString(5));
                agenda.setLokasi(cursor.getString(6));
                agenda.setPengingat(cursor.getString(7));
                agenda.setSuara(cursor.getString(8));
                agenda.setGetar(cursor.getString(9));
                agenda.setLatitude(cursor.getString(10));
                agenda.setLongitude(cursor.getString(11));
                agenda.setNama_pasien(cursor.getString(12));
                agenda.setSub_judul(cursor.getString(13));
                item.add(agenda);
            } while (cursor.moveToNext());
        }

        objAgenda.setObjects(item);
        return objAgenda;
    }

    public  void truncateAgenda(){
        SQLiteDatabase db = this.getWritableDatabase();
        String deleteQuery = "DELETE FROM " + TABLE_AGENDA;
        db.execSQL(deleteQuery);
    }

    ////////////////////JADWAL PRAKTIK//////////////////////
    public void addPraktek(JadwalPraktek.Praktek praktek) {
        SQLiteDatabase db = this.getWritableDatabase();
        for (int i = 0; i < praktek.getSchedules().size(); i++) {
            ContentValues values = new ContentValues();
            values.put(ID_TEMPAT, praktek.getSchedules().get(i).getId());
            values.put(ID_PROFILE, praktek.getSchedules().get(i).getId_profil_praktik());
            values.put(NAMA_PRAKTIK, praktek.getNama_praktik());
            values.put(ALAMAT, praktek.getAlamat());
            values.put(TELP, praktek.getTelp());
            values.put(LONGITUDE, praktek.getLongitude());
            values.put(LATITUDE, praktek.getLatitude());

            //jadwal
            values.put(ID_JADWAL, praktek.getSchedules().get(i).getId());
            values.put(JAM_MULAI, praktek.getSchedules().get(i).getJam_mulai());
            values.put(JAM_AKHIR, praktek.getSchedules().get(i).getJam_akhir());
            values.put(HARI, praktek.getSchedules().get(i).getHari());
            values.put(JAM_AKHIR, praktek.getSchedules().get(i).getJam_akhir());
            values.put(JAM_AKHIR, praktek.getSchedules().get(i).getJam_akhir());

            long rowInserted = db.insert(TABLE_PRAKTIK, null, values);
            if (rowInserted != -1)
                Log.i("DB", "new row added");
            else
                Log.i("DB", "gagal");
        }
        db.close(); // Closing database connection

    }

    public void deletePraktik(String hari) {
        SQLiteDatabase db = this.getWritableDatabase();
        String deleteQuery = "DELETE FROM " + TABLE_PRAKTIK
                + " WHERE " + HARI + "=" + hari;
        db.execSQL(deleteQuery);
    }


    public JadwalPraktek getPraktik() {
        JadwalPraktek objPraktik = new JadwalPraktek();
        ArrayList<JadwalPraktek.Praktek> item = new ArrayList<JadwalPraktek.Praktek>();

        String selectQuery = "SELECT  * FROM " + TABLE_PRAKTIK + " GROUP BY " + HARI;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);


        if (cursor.moveToFirst()) {
            do {
                ArrayList<JadwalPraktek.Jadwal> jadwal = new ArrayList<JadwalPraktek.Jadwal>();
                JadwalPraktek.Praktek tempPraktek = new JadwalPraktek().new Praktek();

                tempPraktek.setId_profil(cursor.getString(2));
                tempPraktek.getJadwal().get(0).setHari(cursor.getString(9));


                String selectQuery2 = "SELECT  * FROM " + TABLE_PRAKTIK + " WHERE " + HARI + "=" + cursor.getString(9) + " ORDER BY " + JAM_MULAI;
                SQLiteDatabase db2 = this.getWritableDatabase();
                Cursor cursor2 = db2.rawQuery(selectQuery2, null);

                if (cursor2.moveToFirst()) {
                    do {
                        JadwalPraktek.Jadwal tempJadwal = new JadwalPraktek().new Jadwal();

                        tempJadwal.setId_profil_praktik(cursor2.getString(1));
                        tempJadwal.setNama_praktik(cursor2.getString(3));
                        tempJadwal.setAlamat(cursor2.getString(4));
                        tempJadwal.setTelp(cursor2.getString(5));
                        tempJadwal.setLongitude(cursor2.getString(6));
                        tempJadwal.setLatitude(cursor2.getString(7));
                        tempJadwal.setId(cursor2.getString(8));
                        tempJadwal.setJam_mulai(cursor2.getString(10));
                        tempJadwal.setJam_akhir(cursor2.getString(11));
                        //tempJadwal.setStatus(cursor2.getString(12));

                        jadwal.add(tempJadwal);
                    } while (cursor2.moveToNext());
                }

                tempPraktek.setJadwal(jadwal);

                item.add(tempPraktek);
            } while (cursor.moveToNext());
        }

        objPraktik.setObjects(item);
        return objPraktik;
    }


    public JadwalPraktek.GetPraktek getPraktikForEdit(String id) {
        JadwalPraktek.Praktek prak = new JadwalPraktek().new Praktek();
        JadwalPraktek.GetPraktek objPraktik = new JadwalPraktek().new GetPraktek();

        ArrayList<JadwalPraktek.Jadwal> item = new ArrayList<JadwalPraktek.Jadwal>();

        String selectQuery = "SELECT  * FROM " + TABLE_PRAKTIK + " WHERE " + ID_TEMPAT + "=" + id;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);


        if (cursor.moveToFirst()) {
            do {
                JadwalPraktek.Jadwal tempPraktek = new JadwalPraktek().new Jadwal();

                prak.setId(cursor.getString(1));
                prak.setId_profil(cursor.getString(2));
                prak.setNama_praktik(cursor.getString(3));
                prak.setAlamat(cursor.getString(4));
                prak.setTelp(cursor.getString(5));
                prak.setLatitude(cursor.getString(6));
                prak.setLongitude(cursor.getString(7));
                prak.setHari(cursor.getString(9));

                tempPraktek.setId(cursor.getString(8));
                tempPraktek.setId_profil_praktik(cursor.getString(2));
                tempPraktek.setHari(cursor.getString(9));
                tempPraktek.setJam_mulai(cursor.getString(10));
                tempPraktek.setJam_akhir(cursor.getString(11));
                //tempPraktek.setStatus(cursor.getString(12));


                item.add(tempPraktek);
            } while (cursor.moveToNext());
        }

        prak.setJadwal(item);

        objPraktik.setObjects(prak);
        return objPraktik;
    }

    public  void truncateJadwal(){
        SQLiteDatabase db = this.getWritableDatabase();
        String deleteQuery = "DELETE FROM " + TABLE_PRAKTIK;
        db.execSQL(deleteQuery);
    }


    ///////////////////////////NOTIF////////////////////////////////

    public ArrayList<Notif> getNotif() {
        ArrayList<Notif> temp = new ArrayList<Notif>();
        String selectQuery = "SELECT  * FROM " + TABLE_LOCAL_NOTIF;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            do {
                Notif notif = new Notif();
                notif.setId_agenda(cursor.getString(0));
                notif.setIs_read(cursor.getString(1));
                temp.add(notif);
            } while (cursor.moveToNext());
        }

        return temp;
    }


    public ArrayList<Notif> findNotif(String id) {
        ArrayList<Notif> temp = new ArrayList<Notif>();
        String selectQuery = "SELECT  * FROM " + TABLE_LOCAL_NOTIF + " WHERE " + ID_AGENDA + "='" + id + "'";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            do {
                Notif notif = new Notif();
                notif.setId_agenda(cursor.getString(0));
                notif.setIs_read(cursor.getString(1));
                temp.add(notif);
            } while (cursor.moveToNext());
        }

        return temp;
    }

    public void insertNotif(Notif ntf) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(ID_AGENDA, ntf.getId_agenda());
        values.put(IS_READ, ntf.getIs_read());
        long rowInserted = db.insert(TABLE_LOCAL_NOTIF, null, values);
        if (rowInserted != -1)
            Log.i("DB", "new row added");
        else
            Log.i("DB", "gagal");
        db.close(); // Closing database connection
    }


    public void updateGroupNotif(String id, String isread) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("UPDATE " + TABLE_LOCAL_NOTIF + " SET " + IS_READ + "=" + isread + " WHERE " + ID_AGENDA + "=" + id);
    }


    ////////////////////////// Artikel /////////////////////////////////


    public void insertArtikel(Artikel.Oartiker artikel, String code) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(CODE_MENU, code);
        values.put(DESKRIPSI, artikel.getDeskripsi());
        values.put(VERSION, artikel.getVersi());
        values.put(SUMBER, artikel.getSumber());
        values.put(SLIDE, artikel.getSlide());
        long rowInserted = db.insert(TABLE_ARTIKEL, null, values);
        if (rowInserted != -1)
            Log.i("DB", "new row added");
        else
            Log.i("DB", "gagal");
        db.close(); // Closing database connection
    }


    public void deleteArtikel(String code) {
        SQLiteDatabase db = this.getWritableDatabase();
        String deleteQuery = "DELETE FROM " + TABLE_ARTIKEL
                + " WHERE " + CODE_MENU + "='" + code + "'";
        db.execSQL(deleteQuery);
    }


    public Artikel getArtikels(String code) {
        Artikel tempArtikel = new Artikel();
        ArrayList<Artikel.Oartiker> arrTemp = new ArrayList<Artikel.Oartiker>();

        String selectQuery = "SELECT  * FROM " + TABLE_ARTIKEL +
                " WHERE " + CODE_MENU + "='" + code + "' ORDER BY " + SLIDE + " ASC";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            do {
                Artikel.Oartiker temp = new Artikel().new Oartiker();
                temp.setCode(cursor.getString(0));
                temp.setDeskripsi(cursor.getString(1));
                temp.setSumber(cursor.getString(2));
                temp.setVersi(cursor.getString(3));
                temp.setSlide(cursor.getString(4));
                arrTemp.add(temp);
            } while (cursor.moveToNext());
        }

        tempArtikel.setObjects(arrTemp);
        return tempArtikel;
    }


    /////////////////////  QUESTION  //////////////////////
    public void insertQuestion(Question.Kuesioner kuisioner, String code) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(CODE_MENU, code);
        values.put(QUESTIONNAIRE_ID, kuisioner.getId());
        values.put(TITLE, kuisioner.getTitle());
        values.put(SUB_TITLE, kuisioner.getSub_title());
        values.put(TYPE, kuisioner.getType());
        values.put(PATH, kuisioner.getPath());
        values.put(MIN_AGE, kuisioner.getMin_age());
        values.put(MAX_AGE, kuisioner.getMax_age());
        values.put(QUESTION_VERSION, kuisioner.getQuestion_version());
        values.put(KODE, kuisioner.getKode());
        values.put(AGE_ID, kuisioner.getQuestionnaire_age_id());
        long rowInserted = db.insert(TABLE_QUESTIONNAIRE, null, values);
        if (rowInserted != -1)
            Log.i("DB", "new row added " +kuisioner.getId());
        else
            Log.i("DB", "gagal "+kuisioner.getId());
        db.close(); // Closing database connection
    }


    public void insertCmar(Question.Kuesioner kuisioner, String code, String id_age, int min_age, int max_age) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(CODE_MENU, code);
        values.put(QUESTIONNAIRE_ID, kuisioner.getId());
        values.put(TITLE, kuisioner.getTitle());
        values.put(SUB_TITLE, kuisioner.getSub_title());
        values.put(TYPE, kuisioner.getType());
        values.put(PATH, kuisioner.getPath());
        values.put(MIN_AGE, min_age);
        values.put(MAX_AGE, max_age);
        values.put(QUESTION_VERSION, kuisioner.getQuestion_version());
        values.put(AGE_ID, id_age);
        long rowInserted = db.insert(TABLE_QUESTIONNAIRE, null, values);
        if (rowInserted != -1)
            Log.i("DB", "CMAR row added");
        else
            Log.i("DB", "CMAR gagal");
        db.close(); // Closing database connection
    }


    public Question getQuestion(String code, int month) {
        Question temp = new Question();
        ArrayList<Question.Kuesioner> listkuesioner = new ArrayList<Question.Kuesioner>();

        String selectQuery = "SELECT  * FROM " + TABLE_QUESTIONNAIRE +
                " WHERE " + CODE_MENU + "='" + code + "'";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            do {
                ArrayList<Question.Answer> listAnswer = new ArrayList<Question.Answer>();
                if ((month >= cursor.getInt(6) && month <= cursor.getInt(7))) {

                    Question.Kuesioner tempQuestion = new Question().new Kuesioner();
                    tempQuestion.setId(cursor.getString(1));
                    tempQuestion.setTitle(cursor.getString(2));
                    tempQuestion.setSub_title(cursor.getString(3));
                    tempQuestion.setType(cursor.getString(4));
                    tempQuestion.setPath(cursor.getString(5));
                    tempQuestion.setMin_age(cursor.getInt(6));
                    tempQuestion.setMax_age(cursor.getInt(7));
                    tempQuestion.setQuestion_version(cursor.getString(8));
                    tempQuestion.setQuestionnaire_age_id(cursor.getString(9));
                    tempQuestion.setKode(cursor.getString(10));
                    listAnswer = getAnswer(cursor.getString(1));
                    tempQuestion.setAnswer_option(listAnswer);
                    listkuesioner.add(tempQuestion);

                } else if (cursor.getInt(6) == -1 && cursor.getInt(7) == -1) {
                    Question.Kuesioner tempQuestion = new Question().new Kuesioner();
                    tempQuestion.setId(cursor.getString(1));
                    tempQuestion.setTitle(cursor.getString(2));
                    tempQuestion.setSub_title(cursor.getString(3));
                    tempQuestion.setType(cursor.getString(4));
                    tempQuestion.setPath(cursor.getString(5));
                    tempQuestion.setMin_age(cursor.getInt(6));
                    tempQuestion.setMax_age(cursor.getInt(7));
                    tempQuestion.setQuestion_version(cursor.getString(8));
                    tempQuestion.setQuestionnaire_age_id(cursor.getString(9));
                    tempQuestion.setKode(cursor.getString(10));
                    listAnswer = getAnswer(cursor.getString(1));
                    tempQuestion.setAnswer_option(listAnswer);
                    listkuesioner.add(tempQuestion);
                }
            } while (cursor.moveToNext());
        }
        temp.setObjects(listkuesioner);
        return temp;
    }


    public Question getQuestionNosesi(String code, int min, int max) {
        Question temp = new Question();
        ArrayList<Question.Kuesioner> listkuesioner = new ArrayList<Question.Kuesioner>();

        String selectQuery = "SELECT  * FROM " + TABLE_QUESTIONNAIRE +
                " WHERE " + CODE_MENU + "='" + code + "' AND " + min + ">=" + MIN_AGE + " AND " + max + "<=" + MAX_AGE;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            do {
                ArrayList<Question.Answer> listAnswer = new ArrayList<Question.Answer>();
//                if ((min >= cursor.getInt(6) && min <= cursor.getInt(7)) ||
//                        (-1 == cursor.getInt(6) && -1 == cursor.getInt(7))) {
                Question.Kuesioner tempQuestion = new Question().new Kuesioner();
                tempQuestion.setId(cursor.getString(1));
                tempQuestion.setTitle(cursor.getString(2));
                tempQuestion.setSub_title(cursor.getString(3));
                tempQuestion.setType(cursor.getString(4));
                tempQuestion.setPath(cursor.getString(5));
                tempQuestion.setMin_age(cursor.getInt(6));
                tempQuestion.setMax_age(cursor.getInt(7));
                tempQuestion.setQuestion_version(cursor.getString(8));
                tempQuestion.setQuestionnaire_age_id(cursor.getString(9));
                tempQuestion.setKode(cursor.getString(10));
                listAnswer = getAnswer(cursor.getString(1));
                tempQuestion.setAnswer_option(listAnswer);
                listkuesioner.add(tempQuestion);
//                }
            } while (cursor.moveToNext());
        }
        temp.setObjects(listkuesioner);
        return temp;
    }


    public void deleteQuestion(String id, int min, int max, String code) {
        if (min == -1 && max == -1) {
            SQLiteDatabase db = this.getWritableDatabase();
            String deleteQuery = "DELETE FROM " + TABLE_QUESTIONNAIRE
                    + " WHERE " + QUESTIONNAIRE_ID + "=" + id + " AND " + MIN_AGE + "=" + min + " AND " + MAX_AGE + "=" + max;
            db.execSQL(deleteQuery);
        } else {
            SQLiteDatabase db = this.getWritableDatabase();
            String deleteQuery = "DELETE FROM " + TABLE_QUESTIONNAIRE
                    + " WHERE " + CODE_MENU + "='" + code + "' AND " + MIN_AGE + "=" + min + " AND " + MAX_AGE + "=" + max;
            db.execSQL(deleteQuery);
        }
    }


    /////////////////////  ANSWER  //////////////////////
    public void insertAnswer(Question.Answer answer) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(ID, answer.getId());
        values.put(QUESTIONNAIRE_ID, answer.getQuestionnaire_id());
        values.put(OPTION_TYPE, answer.getOption_type());
        values.put(OPTION, answer.getOption());
        values.put(SCORE, answer.getScore());
        long rowInserted = db.insert(TABLE_ANSWER, null, values);
        if (rowInserted != -1)
            Log.i("DB", "new row added" + answer.getQuestionnaire_id());
        else
            Log.i("DB", "gagal" + answer.getQuestionnaire_id());
        db.close(); // Closing database connection
    }


    public ArrayList<Question.Answer> getAnswer(String id) {
        ArrayList<Question.Answer> listAnswer = new ArrayList<Question.Answer>();
        String selectQuery = "SELECT  * FROM " + TABLE_ANSWER +
                " WHERE " + QUESTIONNAIRE_ID + "='" + id + "'";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            do {
                Question.Answer tempAnswer = new Question().new Answer();
                tempAnswer.setId(cursor.getString(1));
                tempAnswer.setOption_type(cursor.getString(2));
                tempAnswer.setOption(cursor.getString(3));
                tempAnswer.setScore(cursor.getString(4));
                listAnswer.add(tempAnswer);
            } while (cursor.moveToNext());
        }

        return listAnswer;


    }


    public void deleteAnswer(String id) {
        SQLiteDatabase db = this.getWritableDatabase();
        String deleteQuery = "DELETE FROM " + TABLE_ANSWER
                + " WHERE " + QUESTIONNAIRE_ID + "='" + id + "'";
        db.execSQL(deleteQuery);
    }


    //////////////////////CMAR////////////////

    public AllRedflag getAllRedflag(String code) {
        AllRedflag temp = new AllRedflag();

        ArrayList<AllRedflag.AllRedflags> listTemp = new ArrayList<AllRedflag.AllRedflags>();


        String selectQuery = "SELECT  " + AGE_ID + "," + MIN_AGE + "," + MAX_AGE + " FROM " + TABLE_QUESTIONNAIRE +
                " WHERE " + CODE_MENU + "='" + code + "' GROUP BY " + AGE_ID + " ORDER BY " + MIN_AGE + "," + MAX_AGE + " ASC";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            do {
                AllRedflag.AllRedflags oTemp = new AllRedflag().new AllRedflags();

                ArrayList<Question.Kuesioner> listkuesioner = new ArrayList<Question.Kuesioner>();

                String selectQuery2 = "SELECT  * FROM " + TABLE_QUESTIONNAIRE +
                        " WHERE " + AGE_ID + "=" + cursor.getString(0) + " ORDER BY " + MIN_AGE + " ASC";

                SQLiteDatabase db2 = this.getWritableDatabase();
                Cursor cursor2 = db2.rawQuery(selectQuery2, null);

                if (cursor2.moveToFirst()) {
                    do {
                        if (cursor2.getInt(6) == -1 || cursor2.getInt(7) == -1) {
                            oTemp.setAge_string("Semua Umur");
                        } else {
                            oTemp.setAge_string(cursor2.getInt(6) + " - " + cursor2.getInt(7) + " Bulan");
                        }
                        oTemp.setMin_age(cursor2.getInt(6));
                        oTemp.setMax_age(cursor2.getInt(7));
                        oTemp.setId_age(cursor2.getString(9));

                        ArrayList<Question.Answer> listAnswer = new ArrayList<Question.Answer>();

                        Question.Kuesioner tempQuestion = new Question().new Kuesioner();
                        tempQuestion.setId(cursor2.getString(1));
                        tempQuestion.setTitle(cursor2.getString(2));
                        tempQuestion.setSub_title(cursor2.getString(3));
                        tempQuestion.setType(cursor2.getString(4));
                        tempQuestion.setPath(cursor2.getString(5));
                        tempQuestion.setMin_age(cursor2.getInt(6));
                        tempQuestion.setMax_age(cursor2.getInt(7));
                        tempQuestion.setQuestion_version(cursor2.getString(8));
                        listAnswer = getAnswer(cursor2.getString(1));
                        tempQuestion.setAnswer_option(listAnswer);
                        listkuesioner.add(tempQuestion);

                        oTemp.setData(listkuesioner);

                    } while (cursor2.moveToNext());
                }

                listTemp.add(oTemp);
            } while (cursor.moveToNext());
        }

        temp.setObjects(listTemp);
        return temp;
    }


    ///////////////////// MENU AGE /////////////////////////////
    public void insertMenuAge(MenuAge.Menu menu) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(ID, menu.getId());
        values.put(RANGE_AGE, menu.getRange_age());
        values.put(CODE_MENU, menu.getCode_menu());
        values.put(NAME_MENU, menu.getName_menu());
        values.put(MIN_AGE, menu.getMin_age());
        values.put(MAX_AGE, menu.getMax_age());
        long rowInserted = db.insert(TABLE_MENU_AGE, null, values);
        if (rowInserted != -1)
            Log.i("DB", "new row added");
        else
            Log.i("DB", "gagal");
        db.close(); // Closing database connection
    }

    public void deleteMenuAge(String code) {
        SQLiteDatabase db = this.getWritableDatabase();
        String deleteQuery = "DELETE FROM " + TABLE_MENU_AGE
                + " WHERE " + CODE_MENU + "='" + code + "'";
        db.execSQL(deleteQuery);
    }


    public ArrayList<MenuAge.Menu> getMenuAge(String code) {
        ArrayList<MenuAge.Menu> list = new ArrayList<MenuAge.Menu>();
        String selectQuery = "SELECT  * FROM " + TABLE_MENU_AGE +
                " WHERE " + CODE_MENU + "='" + code + "'";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            do {
                MenuAge.Menu temp = new MenuAge().new Menu();
                temp.setId(cursor.getString(0));
                temp.setCode_menu(cursor.getString(1));
                temp.setName_menu(cursor.getString(2));
                temp.setRange_age(cursor.getString(3));
                temp.setMin_age(cursor.getString(4));
                temp.setMax_age(cursor.getString(5));
                list.add(temp);
            } while (cursor.moveToNext());
        }

        return list;
    }

    ///////////////////PDF//////////////////////////////

    public void insertFile(int id, String code, String path, String category) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(ID, id);
        values.put(CODE_MENU, code);
        values.put(PATH, path);
        values.put(CATEGORY, category);
        long rowInserted = db.insert(TABLE_FILE, null, values);
        if (rowInserted != -1)
            Log.i("DB", "new row added");
        else
            Log.i("DB", "gagal");
        db.close(); // Closing database connection
    }

    public void deleteFile(int id) {
        SQLiteDatabase db = this.getWritableDatabase();
        String deleteQuery = "DELETE FROM " + TABLE_FILE
                + " WHERE " + ID + "=" + id + "";
        db.execSQL(deleteQuery);
    }


    public String getFile(int category) {
        String temp = "";
        String selectQuery = "SELECT  * FROM " + TABLE_FILE +
                " WHERE " + CATEGORY + "=" + category + "";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            do {
                temp = cursor.getString(2);
            } while (cursor.moveToNext());
        }

        return temp;
    }


    public Pdf getFilesObject(int category) {
        Pdf temp = new Pdf();
        String selectQuery = "SELECT  * FROM " + TABLE_FILE +
                " WHERE " + CATEGORY + "=" + category + "";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            do {
                Pdf.PdfObjects pdfObject = new Pdf().new PdfObjects();
                pdfObject.setId(cursor.getString(0));
                pdfObject.setMenu(cursor.getString(1));
                pdfObject.setFilename(cursor.getString(2));
                pdfObject.setCategory(cursor.getString(3));
                temp.setObjects(pdfObject);
            } while (cursor.moveToNext());
        }

        return temp;
    }

    //////////////////////////////// MEOT /////////////////////////////////

    public void insertMeot(MateriEdukasi.Materi materi) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(AGE_ID, materi.getAge_id());
        values.put(TITLE, materi.getTitle());
        values.put(VERSION, materi.getEducation_content_version());
        values.put(CONTENT, materi.getContent());
        values.put(PATH, materi.getHeader_picture());
        long rowInserted = db.insert(TABLE_MEOT, null, values);
        if (rowInserted != -1)
            Log.i("DB", "new row added");
        else
            Log.i("DB", "gagal");
        db.close(); // Closing database connection
    }

    public void deleteMeot(String id) {
        SQLiteDatabase db = this.getWritableDatabase();
        String deleteQuery = "DELETE FROM " + TABLE_MEOT
                + " WHERE " + AGE_ID + "=" + id + "";
        db.execSQL(deleteQuery);
    }


    public MateriEdukasi getMeot(String id) {
        MateriEdukasi temp = new MateriEdukasi();
        ArrayList<MateriEdukasi.Materi> list = new ArrayList<MateriEdukasi.Materi>();
        String selectQuery = "SELECT  * FROM " + TABLE_MEOT +
                " WHERE " + AGE_ID + "='" + id + "'";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            do {
                MateriEdukasi.Materi materi = new MateriEdukasi().new Materi();
                materi.setAge_id(cursor.getString(1));
                materi.setTitle(cursor.getString(2));
                materi.setContent(cursor.getString(3));
                materi.setEducation_content_version(cursor.getString(4));
                materi.setHeader_picture(cursor.getString(5));
                list.add(materi);
            } while (cursor.moveToNext());
        }
        temp.setObjects(list);
        return temp;


    }


    public int getMonth(String date) {
        Months month = Months.monthsBetween(new LocalDate(date), new LocalDate());
        return month.getMonths();
    }
}
