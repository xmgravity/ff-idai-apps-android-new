package com.prima.fragments;

import android.app.Activity;
import android.app.DownloadManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.net.Uri;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.daimajia.easing.linear.Linear;
import com.google.gson.JsonSyntaxException;
import com.prima.Preferences;
import com.prima.activities.AwamForgotPasswordActivity;
import com.prima.activities.AwamRegistrationActivity;
import com.prima.activities.BaseActivity;
import com.prima.activities.SplashPagerActivity;
import com.prima.activities.SplashPernyataanActivity;
import com.prima.controllers.ApiReferences;
import com.prima.controllers.Parser;
import com.prima.entities.Dokter;
import com.prima.entities.Login;
import com.prima.entities.Message;
import com.prima.entities.OrangTua;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import prima.test.prima.R;

/**
 * Created by Codelabs on 30/09/2015.
 */
public class LoginFragment extends BaseFragment implements View.OnClickListener {
    private Context mContext;
    public static final String ARG_PAGE = "page 1";
    private ViewPager mPager;
    List<Fragment> fragmentList = new ArrayList<Fragment>();
    private TextView forgot;
    private RelativeLayout login, awamLoginLanjut;
    private RelativeLayout doctorLogin;
    private RelativeLayout awamLogin;
    private RelativeLayout awamRegister;
    private TextView awamRegistertxt;
    TextView title, registered, unregistered, peran, txt, awamLoginLanjutTxt, doctorLoginText, awamLoginText, textLanguage;
    Spinner languageSpinner;
    private LinearLayout languageSpinnerLayout;
    MaterialDialog dialog;
    ImageView back;
    private RequestQueue mRequest;
    EditText username, password;

    @Override
    public void initView(View view) {
        mRequest = Volley.newRequestQueue(getBaseActivity());
        mPager = (ViewPager) getActivity().findViewById(R.id.pager);
        mContext = getActivity();
        Activity act = getActivity();
        username = (EditText) view.findViewById(R.id.username);
        password = (EditText) view.findViewById(R.id.password);

        back = (ImageView) view.findViewById(R.id.back);
        title = (TextView) view.findViewById(R.id.title);
        registered = (TextView) view.findViewById(R.id.registered);
        unregistered = (TextView) view.findViewById(R.id.unregistered);
        peran = (TextView) view.findViewById(R.id.peran);
        txt = (TextView) view.findViewById(R.id.txt);
        forgot = (TextView) view.findViewById(R.id.forgot);
        login = (RelativeLayout) view.findViewById(R.id.login);
        doctorLogin = (RelativeLayout) view.findViewById(R.id.doctorLogin);
        awamLogin = (RelativeLayout) view.findViewById(R.id.awamLogin);
        awamLoginLanjut = (RelativeLayout) view.findViewById(R.id.awamloginLanjut);
        awamRegister = (RelativeLayout) view.findViewById(R.id.awamRegister);
        awamRegistertxt = (TextView) view.findViewById(R.id.awamRegistertxt);
        awamLoginLanjutTxt = (TextView) view.findViewById(R.id.awamLoginLanjutTxt);
        doctorLoginText = (TextView) view.findViewById(R.id.doctorLogintxt);
        awamLoginText = (TextView) view.findViewById(R.id.awamLogintxt);
        languageSpinnerLayout = (LinearLayout) view.findViewById(R.id.languageSpinnerLayout);
        languageSpinner = (Spinner) view.findViewById(R.id.languageSpinner);
        textLanguage = (TextView) view.findViewById(R.id.textLanguage);

        Typeface tf = Typeface.createFromAsset(getActivity().getAssets(), "BREVIA-SEMIBOLD.OTF");
        Typeface tfItalic = Typeface.createFromAsset(getActivity().getAssets(), "Brevia-ExtraBlackItalic.otf");
        //txt.setTypeface(tf);
        textLanguage.setTypeface(tf);
        title.setTypeface(tf);
        awamLoginLanjutTxt.setTypeface(tf);
        awamLoginText.setTypeface(tf);
        doctorLoginText.setTypeface(tf);
        awamRegistertxt.setTypeface(tf);
        txt.setTypeface(tf);
        username.setTypeface(tf);
        password.setTypeface(tf);
        registered.setTypeface(tf);
        unregistered.setTypeface(tf);
        peran.setTypeface(tf);

        forgot.setTypeface(tfItalic);
        back.setVisibility(View.GONE);
        peran.setVisibility(View.VISIBLE);
        awamLoginLanjutTxt.setVisibility(View.VISIBLE);




        if (act instanceof SplashPagerActivity) {
            fragmentList = ((SplashPagerActivity) act).getFragmentList();
        }


    }

    @Override
    public void setUICallbacks() {
        forgot.setOnClickListener(this);
        login.setOnClickListener(this);
        awamLogin.setOnClickListener(this);
        doctorLogin.setOnClickListener(this);
        back.setOnClickListener(this);
        awamRegister.setOnClickListener(this);
        awamLoginLanjut.setOnClickListener(this);
    }

    @Override
    public void updateUI() {

    }

    @Override
    public String getPageTitle() {
        return "Account Summary";
    }


    @Override
    public int getFragmentLayout() {
        return R.layout.activity_login;
    }

    @Override
    public void onClick(View v) {
        if (v == forgot) {
            if (preferences.getString(Preferences.TIPE_USER, "")=="doctor") {
//            Intent i = new Intent(getActivity(), ForgotPasswordActivity.class);
//            startActivity(i);
                //   getActivity().finish();

                String url = "http://idai.or.id/user/lupa-password";
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(url));
                startActivity(i);
            }else{
                Intent i = new Intent(getActivity(), AwamForgotPasswordActivity.class);
                startActivity(i);
                getActivity().finish();
            }


        } else if (v == login) {
            //System.out.println("tipe user"+ preferences.getString(Preferences.TIPE_USER, ""));
            if (preferences.getString(Preferences.TIPE_USER, "").equalsIgnoreCase("doctor")) {
                //System.out.println("nandha masuk login dokter");
                String token = "D384DEFE923623618E7B5B972A191";
                if (!username.getText().toString().trim().equals("") &&
                        !password.getText().toString().trim().equals("")) {
                    login(username.getText().toString(),
                            password.getText().toString(), "login", token);
                } else {
                    dialog("Harap isi terlebih dahulu NPA dan Password anda.", getResources().getString(R.string.Perhatian));
                }
            }else{
                //System.out.println("nandha masuk login awam");
                if (!username.getText().toString().trim().equals("") &&
                        !password.getText().toString().trim().equals("")) {
                    loginAwam(  username.getText().toString(),
                                password.getText().toString());
                } else {
                    dialog("Harap isi terlebih dahulu Email dan Password anda.", getResources().getString(R.string.Perhatian));
                }
            }
        } else if (v == doctorLogin) {
            username.setVisibility(View.VISIBLE);
            password.setVisibility(View.VISIBLE);
            title.setVisibility(View.VISIBLE);
            forgot.setVisibility(View.VISIBLE);
            login.setVisibility(View.VISIBLE);
            registered.setVisibility(View.GONE);
            unregistered.setVisibility(View.GONE);
            txt.setVisibility(View.VISIBLE);
            awamRegister.setVisibility(View.GONE);
            awamRegistertxt.setVisibility(View.GONE);
            doctorLogin.setVisibility(View.GONE);
            awamLogin.setVisibility(View.GONE);
            awamLoginLanjut.setVisibility(View.GONE);
            languageSpinnerLayout.setVisibility(View.VISIBLE);
            languageSpinner.setVisibility(View.VISIBLE);
            editor.putString(Preferences.TIPE_USER, "doctor");
            editor.putString(Preferences.SHOW_HINT, "true");
            peran.setVisibility(View.GONE);
            editor.commit();
            //System.out.println("nandha"+ preferences.getString(Preferences.TIPE_USER, ""));
        } else if (v == awamLogin) {
            username.setVisibility(View.GONE);
            username.setHint("Nama Pengguna / Alamat E-mail");
            password.setVisibility(View.GONE);
            title.setVisibility(View.GONE);
            registered.setVisibility(View.VISIBLE);
            unregistered.setVisibility(View.VISIBLE);
            forgot.setVisibility(View.GONE);
            login.setVisibility(View.GONE);
            txt.setVisibility(View.GONE);
            awamLoginLanjut.setVisibility(View.VISIBLE);
            awamLoginLanjutTxt.setVisibility(View.VISIBLE);
            awamRegister.setVisibility(View.VISIBLE);
            awamRegistertxt.setVisibility((View.VISIBLE));
            doctorLogin.setVisibility(View.GONE);
            awamLogin.setVisibility(View.GONE);
            peran.setVisibility(View.GONE);
            editor.putString(Preferences.TIPE_USER, "awam");
            editor.putString(Preferences.SHOW_HINT, "true");
            editor.commit();
            //System.out.println("nandha"+ preferences.getString(Preferences.TIPE_USER, ""));
        } else if (v == awamLoginLanjut) {
            username.setVisibility(View.VISIBLE);
            username.setHint("Nama Pengguna / Alamat E-mail");
            password.setVisibility(View.VISIBLE);
            title.setVisibility(View.VISIBLE);
            registered.setVisibility(View.GONE);
            unregistered.setVisibility(View.GONE);
            forgot.setVisibility(View.VISIBLE);
            login.setVisibility(View.VISIBLE);
            txt.setVisibility(View.VISIBLE);
            awamLoginLanjut.setVisibility(View.GONE);
            awamLoginLanjutTxt.setVisibility(View.GONE);
            awamRegister.setVisibility(View.GONE);
            awamRegistertxt.setVisibility((View.GONE));
            doctorLogin.setVisibility(View.GONE);
            awamLogin.setVisibility(View.GONE);
            peran.setVisibility(View.GONE);
            languageSpinnerLayout.setVisibility(View.VISIBLE);
            languageSpinner.setVisibility(View.VISIBLE);
            editor.putString(Preferences.TIPE_USER, "awam");
            editor.commit();
            //System.out.println("nandha"+ preferences.getString(Preferences.TIPE_USER, ""));
        }else if (v == awamRegister) {
            Intent i = new Intent(getActivity(), AwamRegistrationActivity.class);
            startActivity(i);
            getActivity().finish();

        } else if (v == back) {
            getActivity().finish();
        }
    }


    private void login(final String user, final String pass, final String action, final String key) {
        progresDialog(false, "Login");
        System.out.println(languageSpinner.getSelectedItem().toString());
        StringRequest listdata = new StringRequest(Request.Method.GET,
                ApiReferences.login(key, action, user, pass),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.i("RESULT JSON", response.toString());
                        try {
                            Message login = Parser.getMessage(response.toString());
                            if (login.getSuccess().equals("true")) {
                                Login log = Parser.getLogin(response.toString());
                                editor.putString(Preferences.ID_USER, log.getObject().getUser().getId());
                                Log.d("dbgbg", "ID_USER log: " + preferences.getString(Preferences.ID_USER, "none"));
                                editor.commit();
                                register(username.getText().toString(), log.getObject().getUser().getEmail(), log.getObject().getUser().getFullname(), log.getObject().getUser().getPhoto());
                            } else {
//                                register("00001", "test@test.com", "Dr. Anna");
                                dialog.dismiss();
                                dialog("The username or password you entered is incorrect.", getResources().getString(R.string.Perhatian));
                            }
                            System.out.print("nandha bahasa "+languageSpinner.getSelectedItemPosition());
                            if(languageSpinner.getSelectedItemPosition() == 1){
                                editor.putString(Preferences.LANGUAGE, "en");
                                System.out.print("nandha bahasa masuk en"+languageSpinner.getSelectedItemPosition());
                                editor.commit();
                            }else{
                                editor.putString(Preferences.LANGUAGE, "id");
                                System.out.print("nandha bahasa masuk id"+languageSpinner.getSelectedItemPosition());
                                editor.commit();
                            }

                            Locale locale = new Locale(preferences.getString(Preferences.LANGUAGE, ""));
                            Locale.setDefault(locale);
                            Configuration config = getActivity().getBaseContext().getResources().getConfiguration();
                            config.locale = locale;
                            getActivity().getBaseContext().getResources().updateConfiguration(config,getActivity().getBaseContext().getResources().getDisplayMetrics());

                            editor.commit();
                        } catch (JsonSyntaxException e) {
                            dialog.dismiss();
                            dialog(getResources().getString(R.string.KesalahanServer), getResources().getString(R.string.Perhatian));
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dialog.dismiss();
//                register("00001", "test@test.com", "Dr. Anna");
                dialog(getResources().getString(R.string.NoConnection), getResources().getString(R.string.Perhatian));
            }
        });
        listdata.setRetryPolicy(new DefaultRetryPolicy(
                20000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        mRequest.add(listdata);
    }

    private void loginAwam(final String user, final String pass) {
        System.out.println(languageSpinner.getSelectedItem().toString());
        StringRequest listdata = new StringRequest(Request.Method.POST,
                ApiReferences.loginAwam(),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            OrangTua ortu = Parser.getOrangTua(response.toString());
                            if (ortu.getStatus().equals("1")) {
                                System.out.println("nandha "+ortu.getMessages());


                                Intent i = new Intent(getActivity(), SplashPernyataanActivity.class);
                                startActivity(i);
                                getActivity().finish();


                                //"id":"13","npa":"00001","nama_dokter":"Pendiri IDAI","sub_spesialisasi":"6","email":"01@idai.or.id","email_otomatis":"0","foto":null,"created_at":"2016-05-04 06:10:23","updated_at":"2016-05-04 13:10:23"

                                editor.putString(Preferences.TIPE_USER, "awam");
                                editor.putString(Preferences.ISLOGIN, "true");
                                editor.putString(Preferences.EMAIL, ortu.getObjects().getEmail());
                                editor.putString(Preferences.NAME, ortu.getObjects().getNama());
                                editor.putString(Preferences.FULLNAME, ortu.getObjects().getNama());
                                System.out.print("nandha bahasa "+languageSpinner.getSelectedItemPosition());
                                if(languageSpinner.getSelectedItemPosition() == 1){
                                    editor.putString(Preferences.LANGUAGE, "en");
                                    System.out.print("nandha bahasa masuk en"+languageSpinner.getSelectedItemPosition());
                                    editor.commit();
                                }else{
                                    editor.putString(Preferences.LANGUAGE, "id");
                                    System.out.print("nandha bahasa masuk id"+languageSpinner.getSelectedItemPosition());
                                    editor.commit();
                                }


                                Locale locale = new Locale(preferences.getString(Preferences.LANGUAGE, ""));
                                Locale.setDefault(locale);
                                Configuration config = getActivity().getBaseContext().getResources().getConfiguration();
                                config.locale = locale;
                                getActivity().getBaseContext().getResources().updateConfiguration(config,getActivity().getBaseContext().getResources().getDisplayMetrics());



                                //TODO Set Photo untuk orang tua
                                editor.putString(Preferences.FOTO, mContext.getString(R.string.baseURL) + ortu.getObjects().getFoto());
                                editor.putString(Preferences.ID_USER, ortu.getObjects().getId());
                                editor.putString(Preferences.DOB, ortu.getObjects().getDob());
                                editor.putString(Preferences.GENDER, ortu.getObjects().getGender());
                                editor.putString(Preferences.ACCESS_TOKEN, ortu.getObjects().getUserToken().getToken());
                                editor.putString(Preferences.HANDPHONE, ortu.getObjects().getHandphone());
                                editor.commit();




                                System.out.println("nandha phone: "+ortu.getObjects().getHandphone() );
                                Log.d("dbgbg", "FOTO: " + ortu.getObjects().getFoto());


                            } else {
//                                register("00001", "test@test.com", "Dr. Anna");
                                System.out.println("nandha failed login");
                            }





                        } catch (JsonSyntaxException e) {
                            dialog(getResources().getString(R.string.KesalahanServer), getResources().getString(R.string.Perhatian));
                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {

                NetworkResponse response = error.networkResponse;
                if (error instanceof ServerError && response != null) {
                    try {
                        String res = new String(response.data,
                                HttpHeaderParser.parseCharset(response.headers, "utf-8"));
                        // Now you can use any deserializer to make sense of data
                        JSONObject obj = new JSONObject(res);

                        String b = obj.toString().substring(1, obj.toString().length() -1);
                        String c = b.toString().replace("messages", "");
                        String d = c.toString().replace("\"", "");
                        String e = d.toString().replace(":", "");
                        String f = e.toString().replace("\\", "");
                        String g = f.toString().replace("[", "");
                        String h = g.toString().replace("]", "");

                        dialog(h, getResources().getString(R.string.Perhatian));
                    } catch (UnsupportedEncodingException e1) {
                        // Couldn't properly decode data to string
                        e1.printStackTrace();
                    } catch (JSONException e2) {
                        // returned data is not JSONObject?
                        e2.printStackTrace();
                    }
                }


            }
        }) {

            protected Map<String, String> getParams() {
                HashMap<String, String> params = new HashMap<String, String>();
                params.put("email", user);
                params.put("password", pass);
                return params;
            }
        };

        listdata.setRetryPolicy(new DefaultRetryPolicy(
                20000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        mRequest.add(listdata);
    }


    private void register(final String npa, final String email, final String nama, final String foto) {
        // progresDialog(false, "Login");
        StringRequest listdata = new StringRequest(Request.Method.POST,
                ApiReferences.register(),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        dialog.dismiss();
                        try {
                            Log.i("RESULT JSON", response.toString());
                            Dokter register = Parser.getRegister(response.toString());
                            if (register.getStatus().equalsIgnoreCase("1")) {
                                Intent i = new Intent(getActivity(), SplashPernyataanActivity.class);
                                startActivity(i);
                                getActivity().finish();
                                editor.putString(Preferences.TIPE_USER, "doctor");
                                editor.putString(Preferences.ISLOGIN, "true");
                                editor.putString(Preferences.EMAIL, register.getObjects().getEmail());
                                editor.putString(Preferences.NAME, register.getObjects().getNama_dokter());
                                editor.putString(Preferences.NPA, register.getObjects().getNpa());
                                editor.putString(Preferences.EMAILOTOMATIS, register.getObjects().getEmail_otomatis());
                                editor.putString(Preferences.FOTO, foto);
                                editor.putString(Preferences.SUBSPESIALISASI, register.getObjects().getSub_spesialisasi());
                                editor.putString(Preferences.ID_USER, register.getObjects().getId());
                                Log.d("dbgbg", "ID_USER reg: " + preferences.getString(Preferences.ID_USER, "none"));
                                System.out.print("nandha bahasa "+languageSpinner.getSelectedItemPosition());
                                if(languageSpinner.getSelectedItemPosition() == 1){
                                    editor.putString(Preferences.LANGUAGE, "en");
                                    System.out.print("nandha bahasa masuk en"+languageSpinner.getSelectedItemPosition());
                                    editor.commit();
                                }else{
                                    editor.putString(Preferences.LANGUAGE, "id");
                                    System.out.print("nandha bahasa masuk id"+languageSpinner.getSelectedItemPosition());
                                    editor.commit();
                                }


                                Locale locale = new Locale(preferences.getString(Preferences.LANGUAGE, ""));
                                Locale.setDefault(locale);
                                Configuration config = getActivity().getBaseContext().getResources().getConfiguration();
                                config.locale = locale;
                                getActivity().getBaseContext().getResources().updateConfiguration(config,getActivity().getBaseContext().getResources().getDisplayMetrics());

                                editor.commit();
                            } else {
                                dialog("The username or password you entered is incorrect.", getResources().getString(R.string.Perhatian));
                            }
                        } catch (JsonSyntaxException e) {
                            dialog(getResources().getString(R.string.KesalahanServer), getResources().getString(R.string.Perhatian));
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                NetworkResponse response = error.networkResponse;
                if (error instanceof ServerError && response != null) {
                    try {
                        String res = new String(response.data,
                                HttpHeaderParser.parseCharset(response.headers, "utf-8"));
                        // Now you can use any deserializer to make sense of data
                        JSONObject obj = new JSONObject(res);

                        String b = obj.toString().substring(1, obj.toString().length() -1);
                        String c = b.toString().replace("messages", "");
                        String d = c.toString().replace("\"", "");
                        String e = d.toString().replace(":", "");
                        String f = e.toString().replace("\\", "");
                        String g = f.toString().replace("[", "");
                        String h = g.toString().replace("]", "");

                        dialog(h, getResources().getString(R.string.Perhatian));
                        //dialog(obj.toString(), getResources().getString(R.string.Perhatian));
                    } catch (UnsupportedEncodingException e1) {
                        // Couldn't properly decode data to string
                        e1.printStackTrace();
                    } catch (JSONException e2) {
                        // returned data is not JSONObject?
                        e2.printStackTrace();
                    }
                }
            }
        }) {

            protected Map<String, String> getParams() {
                HashMap<String, String> params = new HashMap<String, String>();
                params.put("npa", npa);
                params.put("email", email);
                params.put("name", nama);
                //params.put("profile_picture", foto);
                return params;
            }
        };
        listdata.setRetryPolicy(new DefaultRetryPolicy(
                20000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        mRequest.add(listdata);
    }


    private void progresDialog(boolean horizontal, String msg) {
        dialog = new MaterialDialog.Builder(getActivity())
                .title(msg)
                .content(getResources().getString(R.string.MohonTunggu))
                .progress(true, 0)
                .progressIndeterminateStyle(horizontal)
                .cancelable(false)
                .show();
    }


    public void dialog(String content, String msg) {
        new MaterialDialog.Builder(getActivity())
                .title(msg)
                .content(content)
                .positiveText("Ok")
                .callback(new MaterialDialog.ButtonCallback() {
                    @Override
                    public void onPositive(MaterialDialog dialog) {
                    }

                })
                .show();
    }

}