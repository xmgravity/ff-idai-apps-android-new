package com.prima.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import org.joda.time.LocalDate;
import org.joda.time.LocalDateTime;
import org.joda.time.Minutes;
import org.joda.time.Period;
import org.joda.time.PeriodType;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import com.prima.entities.Agenda;
import prima.test.prima.R;

/**
 * Created by Codelabs on 07/08/2015.
 */
public class ListNotifAdapter extends BaseAdapter {
    private ArrayList<Agenda.Agendas> _data;
    Context _context;

    public ListNotifAdapter(ArrayList<Agenda.Agendas> data,
                            Context context) {
        _data = data;
        _context = context;
    }

    @Override
    public int getCount() {

        return _data.size();
    }

    @Override
    public Object getItem(int position) {

        return _data.get(position);
    }

    @Override
    public long getItemId(int position) {

        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        if (v == null) {
            LayoutInflater infLayout = (LayoutInflater) _context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = infLayout.inflate(R.layout.list_item_notif, null);
        }


        Calendar c = Calendar.getInstance();

        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat df2 = new SimpleDateFormat("HH:mm");
        String formattedDate = df.format(c.getTime());
        String formattedDate2 = df2.format(c.getTime());

        String waktu1 = formattedDate + " " + formattedDate2;
        String waktu2 = _data.get(position).getTanggal_kunjungan() + " " + _data.get(position).getJam_mulai().substring(0, 5);
        String waktu3 = _data.get(position).getTanggal_kunjungan() + " " + _data.get(position).getJam_akhir().substring(0, 5);

        final DateTimeFormatter formatter1 = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm");

        LocalDateTime jamMulai = new LocalDateTime(formatter1.parseDateTime(waktu2));
        LocalDateTime jamAkhir = new LocalDateTime(formatter1.parseDateTime(waktu3));
        LocalDateTime today = new LocalDateTime(formatter1.parseDateTime(waktu1));

        Period period = new Period(today, jamMulai, PeriodType.yearMonthDayTime());

        Period period2 = new Period(today, jamAkhir, PeriodType.yearMonthDayTime());


        String temp = "";
        String temp2 = "";


        System.out.printf(
                "%d years, %d months, %d days, %d hours, %d minutes, %d seconds%n",
                period.getYears(), period.getMonths(), period.getDays(),
                period.getHours(), period.getMinutes(), period.getSeconds());
        System.out.printf(
                "%d years, %d months, %d days, %d hours, %d minutes, %d seconds%n",
                period2.getYears(), period2.getMonths(), period2.getDays(),
                period2.getHours(), period2.getMinutes(), period.getSeconds());


        //   long delta = getMinutes(today,)

        if (period.getDays() > 0) {
            temp = temp + period.getDays() + " Hari ";
        }


        if (period.getHours() > 0) {
            temp = temp + period.getHours() + " Jam ";
        }


        if (period.getMinutes() > 0) {
            temp = temp + period.getMinutes() + " Menit ";
        }

        if (temp.equals("")) {
            if (period2.getDays() > 0) {
                temp2 = temp2 + period2.getDays() + " Hari ";
            }

            if (period2.getHours() > 0) {
                temp2 = temp2 + period2.getHours() + " Jam ";
            }

            if (period2.getMinutes() > 0) {
                temp2 = temp2 + period2.getMinutes() + " Menit ";
            }

            if (temp2.equals("")) {
                temp = "Telah Selesai";
            } else {
                temp = "Sedang Berlangsung";
            }

        } else {
            temp = "dalam " + temp;
        }


        TextView txt_question = (TextView) v.findViewById(R.id.txt1);
        TextView txt2 = (TextView) v.findViewById(R.id.txt2);
        TextView txt3 = (TextView) v.findViewById(R.id.txt3);
        TextView txt4 = (TextView) v.findViewById(R.id.txt4);

        TextView number = (TextView) v.findViewById(R.id.number);
        number.setText((position + 1) + ".");
        txt_question.setText(_data.get(position).getNama_pasien());
        txt2.setText(_data.get(position).getSub_judul());
        txt3.setText(temp);
        txt4.setText("di " + _data.get(position).getLokasi());
        return v;
    }


    public int getMinutes(LocalDate tgl1, LocalDate tgl2) {
        Minutes minutes = Minutes.minutesBetween(tgl1, tgl2);
        return minutes.getMinutes();
    }

}
