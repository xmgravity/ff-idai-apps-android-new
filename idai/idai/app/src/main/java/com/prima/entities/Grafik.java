package com.prima.entities;

/**
 * Created by Codelabs on 24/08/2015.
 */
public class Grafik {
    private String status;
    private String messages;
    private Object data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessages() {
        return messages;
    }

    public void setMessages(String messages) {
        this.messages = messages;
    }

    public Object getObjects() {
        return data;
    }

    public void setObjects(Object objects) {
        this.data = objects;
    }

    public class Object {
        private String image_url;
        private  String interpretation;
        private String label;

        public String getLabel() {
            return label;
        }

        public void setLabel(String label) {
            this.label = label;
        }

        public String getInterpretation() {
            return interpretation;
        }

        public void setInterpretation(String interpretation) {
            this.interpretation = interpretation;
        }

        public String getImage_url() {
            return image_url;
        }

        public void setImage_url(String image_url) {
            this.image_url = image_url;
        }
    }
}
