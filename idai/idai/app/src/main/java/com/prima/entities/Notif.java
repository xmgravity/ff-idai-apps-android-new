package com.prima.entities;

/**
 * Created by Codelabs on 21/09/2015.
 */
public class Notif {
    private String id_agenda;
    private String is_read;

    public String getIs_read() {
        return is_read;
    }

    public void setIs_read(String is_read) {
        this.is_read = is_read;
    }

    public String getId_agenda() {
        return id_agenda;
    }

    public void setId_agenda(String id_agenda) {
        this.id_agenda = id_agenda;
    }
}
