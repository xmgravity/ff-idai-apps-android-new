package com.prima.entities;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by User on 3/18/2017.
 */

public class JadwalPraktekSingle implements Serializable {
    private JadwalPraktek.Praktek data;

    public JadwalPraktekSingle() {
        data = new JadwalPraktek().new Praktek();
    }

    private String status;
    private String messages;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessages() {
        return messages;
    }

    public void setMessages(String messages) {
        this.messages = messages;
    }

    public JadwalPraktek.Praktek getObjects() {
        return data;
    }

    public void setObjects(JadwalPraktek.Praktek data) {
        this.data = data;
    }
}
