package com.prima.activities;

import android.content.Intent;
import android.graphics.Typeface;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.prima.Preferences;
import com.prima.controllers.ApiReferences;
import com.prima.controllers.Parser;
import com.prima.entities.JadwalPraktek;
import com.prima.entities.Message;
import com.prima.helpers.DatabaseHandler;

import java.util.ArrayList;

import prima.test.prima.R;

/**
 * Created by Codelabs on 09/09/2015.
 */
public class ListJadwalPraktikActivity extends BaseActivity implements View.OnClickListener {

    TextView title;
    ImageView close, back;
    LinearLayout list;
    private MaterialDialog dialog;
    private RequestQueue mRequest;
    ArrayList<LinearLayout> days = new ArrayList<LinearLayout>();
    DatabaseHandler db;


    @Override
    public void initView() {
        db = new DatabaseHandler(context);
        mRequest = Volley.newRequestQueue(getApplicationContext());
        list = (LinearLayout) findViewById(R.id.list_jadwal_praktik);
        close = (ImageView) findViewById(R.id.close);
        back = (ImageView) findViewById(R.id.back);
        // close.setVisibility(View.GONE);
        //set font
        title = (TextView) findViewById(R.id.tv_actionbar_title);
        Typeface tf = Typeface.createFromAsset(getApplicationContext().getAssets(), "BREVIA-BLACK.OTF");
        title.setTypeface(tf);
        title.setText(getResources().getString(R.string.JadwalPraktik2));

        //    getData();

        //  save(data);
    }

    @Override
    public void setUICallbacks() {
        close.setOnClickListener(this);
        back.setOnClickListener(this);
    }


    private void getData() {
        progresDialog(false, getResources().getString(R.string.MemuatData));
        Log.d("dbgbg", "id: " + preferences.getString(Preferences.ID_USER, ""));
        StringRequest listdata = new StringRequest(Request.Method.GET,
                ApiReferences.getAllPraktekByDay(preferences.getString(Preferences.ID_USER, "")),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        dialog.dismiss();
                        try {
                            if (response.equals("[]")) {
                                dialog(getResources().getString(R.string.DataBelumTersedia), getResources().getString(R.string.Perhatian));
                            } else {
                                Message msg = Parser.getMessage(response);
                                if (msg.getStatus().equals("0")) {
                                    dialog(msg.getMessages(), getResources().getString(R.string.Perhatian));
                                } else {
                                    Log.i("RESULT JSON", response.toString());
                                    final JadwalPraktek jdwl = Parser.getJadwal(response);

                                    for (int i = 0; i < jdwl.getObjects().size(); i++) {
                                        View child = getLayoutInflater().inflate(R.layout.list_item_praktik, null);
                                        TextView hari = (TextView) child.findViewById(R.id.hari);
                                        LinearLayout day = (LinearLayout) child.findViewById(R.id.day);
                                        hari.setId(i+1);
                                        child.setId(i+1);
                                        for (int j = 0; j < jdwl.getObjects().get(i).getSchedules().size(); j++) {
                                            Log.d("dbgbg", "schedule: " + j);
                                            ///
                                            //for (int k = 0; k < 7; k++) {
                                            //if (jdwl.getObjects().get(i).getHari().equals(String.valueOf(k))
                                            //        /*&& !jdwl.getObjects().get(i).getJadwal().get(j).getStatus().equals("0")*/) {
                                            View chidlHari = getLayoutInflater().inflate(R.layout.list_item_day, null);
                                            TextView waktu = (TextView) chidlHari.findViewById(R.id.waktu);
                                            TextView lokasi = (TextView) chidlHari.findViewById(R.id.lokasi);
                                            waktu.setId(22222 + Integer.valueOf(jdwl.getObjects().get(i).getSchedules().get(j).getId()));
                                            lokasi.setId(11111 + Integer.valueOf(jdwl.getObjects().get(i).getSchedules().get(j).getId()));
                                            waktu.setText(jdwl.getObjects().get(i).getSchedules().get(j).getJam_mulai().substring(0, 5) + "-" + jdwl.getObjects().get(i).getSchedules().get(j).getJam_akhir().substring(0, 5));
                                            lokasi.setText(jdwl.getObjects().get(i).getSchedules().get(j).getNama_praktik());
                                            day.addView(chidlHari);
                                            if (chidlHari == null)
                                                Log.d("dbgbg", "childHari null");
                                            Log.d("dbgbg", "add at day: " + i + " " + waktu.getText() + " " + lokasi.getText());

                                            final int finalI = i;
                                            final int finalJ = j;
                                            lokasi.setOnClickListener(new View.OnClickListener() {
                                                @Override
                                                public void onClick(View v) {
                                                    getDataEdit(jdwl.getObjects().get(finalI).getSchedules().get(finalJ).getId_profil_praktik());
                                                }
                                            });
                                            //}
                                            //}
                                            ///
                                        }
                                        if (jdwl.getObjects().get(i).getHari().equals("0")) {
                                            day.setId(i+10);
                                            days.add(day);
                                            hari.setText(getResources().getString(R.string.Senin));
                                        }
                                        if (jdwl.getObjects().get(i).getHari().equals("1")) {
                                            day.setId(i+10);
                                            days.add(day);
                                            hari.setText(getResources().getString(R.string.Selasa));
                                        }
                                        if (jdwl.getObjects().get(i).getHari().equals("2")) {
                                            day.setId(i+10);
                                            days.add(day);
                                            hari.setText(getResources().getString(R.string.Rabu));
                                        }
                                        if (jdwl.getObjects().get(i).getHari().equals("3")) {
                                            day.setId(i+10);
                                            days.add(day);
                                            hari.setText(getResources().getString(R.string.Kamis));
                                        }
                                        if (jdwl.getObjects().get(i).getHari().equals("4")) {
                                            day.setId(i+10);
                                            days.add(day);
                                            hari.setText(getResources().getString(R.string.Jumat));
                                        }
                                        if (jdwl.getObjects().get(i).getHari().equals("5")) {
                                            day.setId(i+10);
                                            days.add(day);
                                            hari.setText(getResources().getString(R.string.Sabtu));
                                        }
                                        if (jdwl.getObjects().get(i).getHari().equals("6")) {
                                            day.setId(i+10);
                                            days.add(day);
                                            hari.setText(getResources().getString(R.string.Minggu));
                                        }

                                        Log.d("dbgbg", "child id: " + child.getId() + " day id: " + hari.getId());
                                        list.addView(child);
                                        if (child == null)
                                            Log.d("dbgbg", "child null");
                                        //Log.d("dbgbg", ((TextView) list.findViewById(i+1).findViewById(R.id.hari)).getText().toString());
                                        Log.d("dbgbg", "add view day: " + jdwl.getObjects().get(i).getHari() + " " + hari.getText());


                                    }


                                    /*for (int i = 0; i < jdwl.getObjects().size(); i++) {
                                        Log.d("dbgbg", "day: " + jdwl.getObjects().get(i).getHari());
                                        db.deletePraktik(jdwl.getObjects().get(i).getHari());
                                        db.addPraktek(jdwl.getObjects().get(i));
                                        for (int j = 0; j < jdwl.getObjects().get(i).getSchedules().size(); j++) {
                                            Log.d("dbgbg", "schedule: " + j);
                                            ///
                                            //for (int k = 0; k < 7; k++) {
                                                //if (jdwl.getObjects().get(i).getHari().equals(String.valueOf(k))
                                                //        *//*&& !jdwl.getObjects().get(i).getJadwal().get(j).getStatus().equals("0")*//*) {
                                                    View chidlHari = getLayoutInflater().inflate(R.layout.list_item_day, null);
                                                    TextView waktu = (TextView) chidlHari.findViewById(R.id.waktu);
                                                    TextView lokasi = (TextView) chidlHari.findViewById(R.id.lokasi);
                                                    waktu.setId(22222 + Integer.valueOf(jdwl.getObjects().get(i).getSchedules().get(j).getId_profil_praktik()));
                                                    lokasi.setId(11111 + Integer.valueOf(jdwl.getObjects().get(i).getSchedules().get(j).getId_profil_praktik()));
                                                    waktu.setText(jdwl.getObjects().get(i).getSchedules().get(j).getJam_mulai().substring(0, 5) + "-" + jdwl.getObjects().get(i).getSchedules().get(j).getJam_akhir().substring(0, 5));
                                                    lokasi.setText(jdwl.getObjects().get(i).getSchedules().get(j).getNama_praktik());
                                                    days.get(i).addView(chidlHari);
                                                    Log.d("dbgbg", "add at day: " + i);

                                                    final int finalI = i;
                                                    final int finalJ = j;
                                                    lokasi.setOnClickListener(new View.OnClickListener() {
                                                        @Override
                                                        public void onClick(View v) {
                                                            getDataEdit(jdwl.getObjects().get(finalI).getSchedules().get(finalJ).getId_profil_praktik());
                                                        }
                                                    });
                                                //}
                                            //}
                                            ///
                                        }
                                    }//*/

                                    for (int i = 0; i < days.size(); i++) {
                                        View v = list.getChildAt(i);

                                        if (days.get(i).getChildCount() == 0) {
                                            //list.removeView(v);
                                            Log.d("dbgbg", "remove day: " + i);
                                        }
                                    }

                                }
                            }
                        } catch (JsonSyntaxException e) {
                            dialog(getResources().getString(R.string.KesalahanServer), getResources().getString(R.string.Perhatian));
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                final JadwalPraktek jdwl = db.getPraktik();
                try {

                    for (int i = 0; i < jdwl.getObjects().size(); i++) {
                        View child = getLayoutInflater().inflate(R.layout.list_item_praktik, null);
                        TextView hari = (TextView) child.findViewById(R.id.hari);
                        LinearLayout day = (LinearLayout) child.findViewById(R.id.day);
                        hari.setId(i);
                        child.setId(i);
                        if (jdwl.getObjects().get(i).getHari().equals("0")) {
                            day.setId(i);
                            days.add(day);
                            hari.setText(getResources().getString(R.string.Senin));
                        }
                        if (jdwl.getObjects().get(i).getHari().equals("1")) {
                            day.setId(i);
                            days.add(day);
                            hari.setText(getResources().getString(R.string.Selasa));
                        }
                        if (jdwl.getObjects().get(i).getHari().equals("2")) {
                            day.setId(i);
                            days.add(day);
                            hari.setText(getResources().getString(R.string.Rabu));
                        }
                        if (jdwl.getObjects().get(i).getHari().equals("3")) {
                            day.setId(i);
                            days.add(day);
                            hari.setText(getResources().getString(R.string.Kamis));
                        }
                        if (jdwl.getObjects().get(i).getHari().equals("4")) {
                            day.setId(i);
                            days.add(day);
                            hari.setText(getResources().getString(R.string.Jumat));
                        }
                        if (jdwl.getObjects().get(i).getHari().equals("5")) {
                            day.setId(i);
                            days.add(day);
                            hari.setText(getResources().getString(R.string.Sabtu));
                        }
                        if (jdwl.getObjects().get(i).getHari().equals("6")) {
                            day.setId(i);
                            days.add(day);
                            hari.setText(getResources().getString(R.string.Minggu));
                        }
                        list.addView(child);
                    }


                    for (int i = 0; i < jdwl.getObjects().size(); i++) {
                        for (int j = 0; j < jdwl.getObjects().get(i).getSchedules().size(); j++) {
                            ///
                            for (int k = 0; k < 7; k++) {
                                if (jdwl.getObjects().get(i).getHari().equals(String.valueOf(k))
                                        /*&& !jdwl.getObjects().get(i).getJadwal().get(j).getStatus().equals("0")*/) {
                                    View chidlHari = getLayoutInflater().inflate(R.layout.list_item_day, null);
                                    TextView waktu = (TextView) chidlHari.findViewById(R.id.waktu);
                                    TextView lokasi = (TextView) chidlHari.findViewById(R.id.lokasi);
                                    waktu.setId(22222 + Integer.valueOf(jdwl.getObjects().get(i).getSchedules().get(j).getId_profil_praktik()));
                                    lokasi.setId(11111 + Integer.valueOf(jdwl.getObjects().get(i).getSchedules().get(j).getId_profil_praktik()));
                                    waktu.setText(jdwl.getObjects().get(i).getSchedules().get(j).getJam_mulai().substring(0, 5) + "-" + jdwl.getObjects().get(i).getJadwal().get(j).getJam_akhir().substring(0, 5));
                                    lokasi.setText(jdwl.getObjects().get(i).getSchedules().get(j).getNama_praktik());
                                    days.get(i).addView(chidlHari);
                                    Log.d("dbgbg", "add at day: " + i);

                                    final int finalI = i;
                                    final int finalJ = j;
                                    lokasi.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            getDataEdit(jdwl.getObjects().get(finalI).getSchedules().get(finalJ).getId_profil_praktik());
                                        }
                                    });
                                }
                            }
                            ///
                        }
                    }//

                    for (int i = 0; i < days.size(); i++) {
                        View v = list.getChildAt(i);
                        if (days.get(i).getChildCount() == 0) {
                            list.removeView(v);
                            Log.d("dbgbg", "remove day: " + i);
                        }
                    }

                } catch (NullPointerException e) {
                    dialog(getResources().getString(R.string.NoConnection), getResources().getString(R.string.Perhatian));
                }
                dialog.dismiss();
            }
        });
        listdata.setRetryPolicy(new DefaultRetryPolicy(
                20000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        mRequest.add(listdata);

    }


    private void getDataEdit(final String id) {
        progresDialog(false, getResources().getString(R.string.MemuatData));
        StringRequest listdata = new StringRequest(Request.Method.GET,
                ApiReferences.getPraktekByPlace(id),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        dialog.dismiss();
                        try {
                            if (response.equals("[]")) {
                                dialog(getResources().getString(R.string.DataBelumTersedia), getResources().getString(R.string.Perhatian));
                            } else {
                                Message msg = Parser.getMessage(response);
                                if (msg.getStatus().equals("0")) {
                                    dialog(msg.getMessages(), getResources().getString(R.string.Perhatian));
                                } else {
                                    Log.i("RESULT JSON", response.toString());
                                    Intent i = new Intent(getApplicationContext(), EditJadwalActivity.class);
                                    i.putExtra("result", response);
                                    Log.d("dbgbg", "result: " + response.toString());
                                    startActivity(i);
                                }
                            }
                        } catch (JsonSyntaxException e) {
                            dialog(getResources().getString(R.string.KesalahanServer), getResources().getString(R.string.Perhatian));
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dialog.dismiss();
                JadwalPraktek.GetPraktek temp = db.getPraktikForEdit(id);
                try {
                    Gson gson = new Gson();
                    Intent i = new Intent(getApplicationContext(), EditJadwalActivity.class);
                    i.putExtra("result", gson.toJson(temp));
                    startActivity(i);
                } catch (NullPointerException e) {
                    dialog(getResources().getString(R.string.NoConnection), getResources().getString(R.string.Perhatian));
                }

            }
        });
        listdata.setRetryPolicy(new DefaultRetryPolicy(
                20000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        mRequest.add(listdata);

    }


    public void dialog(String content, String msg) {
        new MaterialDialog.Builder(this)
                .title(msg)
                .content(content)
                .positiveText("Ok")
                .callback(new MaterialDialog.ButtonCallback() {
                    @Override
                    public void onPositive(MaterialDialog dialog) {
                    }

                })
                .show();
    }

    public void dialog2(String content, String msg) {
        new MaterialDialog.Builder(this)
                .title(msg)
                .content(content)
                .negativeText("OK")
                .callback(new MaterialDialog.ButtonCallback() {
                    @Override
                    public void onPositive(MaterialDialog dialog) {

                    }

                    @Override
                    public void onNegative(MaterialDialog dialog) {
                        super.onNegative(dialog);
                    }

                })
                .show();
    }

    private void progresDialog(boolean horizontal, String msg) {
        dialog = new MaterialDialog.Builder(this)
                .title(msg)
                .content(getResources().getString(R.string.MohonTunggu))
                .progress(true, 0)
                .progressIndeterminateStyle(horizontal)
                .cancelable(false)
                .show();
    }


    @Override
    public void onClick(View v) {
        if (v == back) {
            //  Intent i = new Intent(getApplicationContext(), MainActivity.class);
            //   startActivity(i);
            finish();
        } else if (v == close) {
            Intent i = new Intent(getApplicationContext(), PraktikActivity.class);
            startActivity(i);
            //   finish();
        }
    }


    @Override
    public int getLayout() {
        return R.layout.activity_jadwal_praktik_list;
    }

    @Override
    public void updateUI() {
        list.removeAllViewsInLayout();
        days.clear();
        getData();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

    }


    @Override
    public void onBackPressed() {
        // Intent i = new Intent(getApplicationContext(), MainActivity.class);
        //  startActivity(i);
        finish();
    }


}
