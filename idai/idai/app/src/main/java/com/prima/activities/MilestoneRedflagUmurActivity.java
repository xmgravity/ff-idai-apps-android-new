package com.prima.activities;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.prima.Preferences;
import com.prima.controllers.ApiReferences;
import com.prima.controllers.Parser;
import com.prima.entities.MenuAge;
import com.prima.entities.Message;
import com.prima.entities.Question;
import com.prima.helpers.DatabaseHandler;

import java.util.ArrayList;
import java.util.List;

import prima.test.prima.R;

/**
 * Created by Codelabs on 12/08/2015.
 */
public class MilestoneRedflagUmurActivity extends BaseActivity implements View.OnClickListener {
    private ImageView close;
    private ImageView back;
    private Button lanjut;
    TextView title;
    LinearLayout listMenu;
    MenuAge menu;
    List<RadioButton> radioButtons;
    DatabaseHandler db;
    Question question;
    private RequestQueue mRequest;
    MaterialDialog dialog;
    static String id = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mRequest = Volley.newRequestQueue(getApplicationContext());
        db = new DatabaseHandler(context);

    }

    @Override
    public void initView() {
        listMenu = (LinearLayout) findViewById(R.id.menu);
        back = (ImageView) findViewById(R.id.back);
        close = (ImageView) findViewById(R.id.close);
        lanjut = (Button) findViewById(R.id.lanjut);
        title = (TextView) findViewById(R.id.tv_actionbar_title);
        Typeface tf = Typeface.createFromAsset(getApplicationContext().getAssets(), "BREVIA-BLACK.OTF");
        title.setTypeface(tf);
        title.setText(getResources().getString(R.string.TahapandanTandaBahayaPerkembangan));

        radioButtons = new ArrayList<RadioButton>();

//        radioButtons.add((RadioButton) findViewById(R.id.radio1));
//        radioButtons.add((RadioButton) findViewById(R.id.radio2));
//        radioButtons.add((RadioButton) findViewById(R.id.radio3));
//        radioButtons.add((RadioButton) findViewById(R.id.radio4));
//        radioButtons.add((RadioButton) findViewById(R.id.radio5));
//        radioButtons.add((RadioButton) findViewById(R.id.radio6));
//        radioButtons.add((RadioButton) findViewById(R.id.radio7));
//        radioButtons.add((RadioButton) findViewById(R.id.radio8));
//        radioButtons.add((RadioButton) findViewById(R.id.radio9));

        Bundle bn = getIntent().getExtras();
        if (bn != null) {
            String respon = bn.getString("respon");
            menu = Parser.getMenuAge(respon);
            int size = menu.getObjects().size();
            for (int i = 0; i < size; i++) {
                View child = getLayoutInflater().inflate(R.layout.list_item_milestone_age, null);
                RadioButton ck1 = (RadioButton) child.findViewById(R.id.rad1);
                TextView txt1 = (TextView) child.findViewById(R.id.txt1);
                ck1.setId(i);
                txt1.setId(i);
                txt1.setText(menu.getObjects().get(i).getRange_age());
                radioButtons.add(ck1);
                listMenu.addView(child);
            }
            //checked

            for (RadioButton button : radioButtons) {
                button.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        if (isChecked) processRadioButtonClick(buttonView);
                        id = String.valueOf(buttonView.getId());
                        Log.i("ID CHECKBOX", id);
                    }
                });

            }
        }

    }

    @Override
    public void setUICallbacks() {
        back.setOnClickListener(this);
        close.setOnClickListener(this);
        lanjut.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        if (v == back) {
//            Intent i = new Intent(getApplicationContext(), MilestoneRedflagActivity.class);
//            startActivity(i);i
            finish();
        } else if (v == close) {
//            Intent i = new Intent(getApplicationContext(), MilestoneRedflagActivity.class);
//            startActivity(i);
            finish();
        } else if (v == lanjut) {
            boolean found = false;
            for (RadioButton button : radioButtons) {
                if (button.isChecked()) {
                    found = true;
                }
            }
            if (found) {
                if (!id.equals("")) {
                    getQuestion();
                }
            } else {
                dialog(getResources().getString(R.string.PilihSalahSatuUntukDitampilkan), getResources().getString(R.string.Pesan));
            }
        }
    }

    private void getQuestion() {
        final String code = "MAR";
        progresDialog(false, getResources().getString(R.string.MemuatData));
        String uri;


        final String max = menu.getObjects().get(Integer.valueOf(id)).getMax_age();
        final String min = menu.getObjects().get(Integer.valueOf(id)).getMin_age();

//        DateTime dt = new DateTime();
//        DateTime stringDate = dt.minusMonths(Integer.valueOf(max));
//        Log.i("ID", String.valueOf(stringDate));
//        editor.putString(Preferences.BIRTH_DAY, stringDate.toString().substring(0, 10));
//        editor.commit();


        uri = ApiReferences.getQuestionnaireByRangeAge(code, min, max, preferences.getString(Preferences.LANGUAGE, ""));
        StringRequest listdata = new StringRequest(Request.Method.GET, uri,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        dialog.dismiss();
                        try {
                            if (response.equals("[]")) {
                                dialog(getResources().getString(R.string.BelumAdaData), getResources().getString(R.string.Pesan));
                            } else {
                                Message msg = Parser.getMessage(response);
                                if (msg.getStatus().equals("1")) {
                                    Log.i("RESULT JSON", response.toString());
                                    question = new Question();
                                    question = Parser.getQuestion(response);

                                    //TODO Masi bingung
                                    /*for (int j = 0; j < question.getObjects().size(); j++) {
                                        db.deleteQuestion(question.getObjects().get(j).getId(), question.getObjects().get(j).getMin_age(), question.getObjects().get(j).getMax_age(), "");
                                        db.deleteAnswer(question.getObjects().get(j).getId());
                                        db.insertQuestion(question.getObjects().get(j), code);
                                        for (int k = 0; k < question.getObjects().get(j).getAnswer_option().size(); k++) {
                                            db.insertAnswer(question.getObjects().get(j).getAnswer_option().get(k));
                                        }
                                    }*/

                                    Intent i = new Intent(getApplicationContext(), MilestoneRedflagQuestionActivity.class);
                                    i.putExtra("respon", response.toString());
                                    i.putExtra("min", min);
                                    i.putExtra("max", max);


                                    startActivity(i);
                                    finish();

                                } else {
                                    if (preferences.getString(Preferences.ID, "").equals("1")) {
                                        editor.putString(Preferences.BIRTH_DAY, "");
                                        editor.putString(Preferences.JENIS_KELAMIN, "");
                                        editor.putString(Preferences.BERAT, "");
                                        editor.putString(Preferences.TINGGI, "");
                                        editor.putString(Preferences.ID, "");
                                        editor.commit();
                                    }
                                    dialog(msg.getMessages(), getResources().getString(R.string.Pesan));
                                }

                            }
                        } catch (JsonSyntaxException e) {
                            dialog(getResources().getString(R.string.KesalahanServer), getResources().getString(R.string.Perhatian));
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dialog.dismiss();
                Question temp = db.getQuestionNosesi(code, Integer.valueOf(min), Integer.valueOf(max));
                Gson gson = new Gson();
                Log.i("RESULT JSON2", gson.toJson(temp));
                if (temp.getObjects().size() > 0) {
                    Intent i = new Intent(getApplicationContext(), MilestoneRedflagQuestionActivity.class);
                    i.putExtra("respon", gson.toJson(temp));
                    i.putExtra("min", min);
                    i.putExtra("max", max);
                    startActivity(i);
                    finish();
                } else {
                    dialog(getResources().getString(R.string.NoConnection), getResources().getString(R.string.Perhatian));
                }
            }
        });
        listdata.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        mRequest.add(listdata);
    }


    @Override
    public int getLayout() {
        return R.layout.activity_milestone_choose_umur;
    }

    @Override
    public void updateUI() {

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

    }


    @Override
    public void onBackPressed() {
        //  Intent i = new Intent(getApplicationContext(), MilestoneRedflagActivity.class);
        // startActivity(i);
        finish();
    }

    private void processRadioButtonClick(CompoundButton buttonView) {

        for (RadioButton button : radioButtons) {
            if (button != buttonView) button.setChecked(false);
        }

    }

    public void dialog2(String content, String msg) {
        new MaterialDialog.Builder(this)
                .title(msg)
                .content(content)
                .negativeText("OK")
                .callback(new MaterialDialog.ButtonCallback() {
                    @Override
                    public void onPositive(MaterialDialog dialog) {

                    }

                    @Override
                    public void onNegative(MaterialDialog dialog) {
                        super.onNegative(dialog);
                    }

                })
                .show();
    }

    public void dialog(String content, String msg) {
        new MaterialDialog.Builder(this)
                .title(msg)
                .content(content)
                .positiveText(getResources().getString(R.string.MuatUlang))
                .negativeText(getResources().getString(R.string.Kembali))
                .callback(new MaterialDialog.ButtonCallback() {
                    @Override
                    public void onPositive(MaterialDialog dialog) {
                        getQuestion();
                    }

                    @Override
                    public void onNegative(MaterialDialog dialog) {
                        super.onNegative(dialog);
                    }

                })
                .show();
    }


    private void progresDialog(boolean horizontal, String msg) {
        dialog = new MaterialDialog.Builder(this)
                .title(msg)
                .content(getResources().getString(R.string.MohonTunggu))
                .progress(true, 0)
                .progressIndeterminateStyle(horizontal)
                .cancelable(false)
                .show();
    }

}
