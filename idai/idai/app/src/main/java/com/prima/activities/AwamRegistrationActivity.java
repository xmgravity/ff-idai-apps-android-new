package com.prima.activities;

import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.google.gson.JsonObject;
import com.google.gson.JsonSyntaxException;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.prima.Preferences;
import com.prima.controllers.ApiReferences;
import com.prima.controllers.Parser;
import com.prima.entities.Message;
import com.prima.helpers.DateHelper;
import com.prima.helpers.HexagonMaskView;
import com.soundcloud.android.crop.Crop;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import prima.test.prima.R;

/**
 * Created by Codelabs on 29/08/2015.
 */
public class AwamRegistrationActivity extends BaseActivity implements View.OnClickListener {
    TextView title;
    Button lanjut;
    CheckBox izin;
    DateHelper dateHelper;
    TextView edt;
    ImageView upload_foto;
    RelativeLayout register;
    EditText fullname;
    EditText username;
    EditText email;
    EditText handphone;
    EditText password;
    EditText password_2;
    Spinner gender;

    private RequestQueue mRequest;
    MaterialDialog dialog;

    private Uri selectedImage;
    private String imagePath;
    private File file;
    HexagonMaskView hexa;
    private Uri resultImage;

    @Override
    public void initView() {

        mRequest = Volley.newRequestQueue(this);

        dateHelper = new DateHelper();
        upload_foto = (ImageView) findViewById(R.id.upload_foto);
        register = (RelativeLayout) findViewById(R.id.register);
        fullname = (EditText) findViewById(R.id.fullname);
        username = (EditText) findViewById(R.id.username);
        email = (EditText) findViewById(R.id.email);
        handphone = (EditText) findViewById(R.id.handphone);
        password = (EditText) findViewById(R.id.password);
        password_2 = (EditText) findViewById(R.id.password_2);
        edt = (TextView) findViewById(R.id.edt1);
        gender = (Spinner) findViewById(R.id.gender);
        izin = (CheckBox) findViewById(R.id.izin);

        fullname.setVisibility(View.GONE);
        handphone.setVisibility(View.GONE);
        edt.setVisibility(View.GONE);
        gender.setVisibility(View.GONE);


        /*
        lanjut = (Button) findViewById(R.id.lanjut);
        setuju = (CheckBox) findViewById(R.id.setuju);

        title = (TextView) findViewById(R.id.title);
        Typeface tf = Typeface.createFromAsset(getApplicationContext().getAssets(), "BREVIA-BLACK.OTF");
        title.setTypeface(tf);
*/

        hexa = new HexagonMaskView();
        Glide.with(AwamRegistrationActivity.this)
                .load(preferences.getString(Preferences.FOTO, ""))
                .asBitmap()
                .override(200, 200)
                .into(new SimpleTarget<Bitmap>() {
                    @Override
                    public void onResourceReady(Bitmap bitmap, GlideAnimation anim) {
                        Bitmap b = hexa.getHexagonShape(hexa.scaleCenterCrop(bitmap, 200, 200));
                        upload_foto.setImageBitmap(b);
                    }
                });
    }

    /*
    private void showDatePickerForBirthDay(final TextView txt) {
        dateHelper.showDatePicker(AwamRegistrationActivity.this, System.currentTimeMillis(), new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                int _year = year;
                int _month = monthOfYear + 1;
                int _day = dayOfMonth;
                String date = _year + "-" + _month + "-" + _day;
                String birthDate = dateHelper.formatDate(date);
                txt.setText(birthDate);
            }
        });
    } */

    @Override
    public void setUICallbacks() {
        /*
        lanjut.setOnClickListener(this);
        checkBox.setOnClickListener(this);*/
        upload_foto.setOnClickListener(this);
        edt.setOnClickListener(this);
        register.setOnClickListener(this);
        izin.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {

        /*
        if (v == edt) {
            showDatePickerForBirthDay(edt); */
        if ( v == register){

           /* String genderReg;
            if(gender.getSelectedItemPosition() == 0){
                genderReg = "m";
            }else{
                genderReg = "f";
            } */

            if(password.getText().toString().equals(password_2.getText().toString())) {

                register(username.getText().toString(),
                        email.getText().toString(),
                        password.getText().toString(),
                        password_2.getText().toString());


            }else if(password.length() < 8 || password_2.length() < 8) {
                dialog(getResources().getString(R.string.KataSandiLebihAtauSamaDengan8), getResources().getString(R.string.Perhatian));
            }else if(username.length()==0 ) {
                dialog(getResources().getString(R.string.UsernameTdkBolehKosong), getResources().getString(R.string.Perhatian));
            }else if(email.length() ==0) {
                dialog(getResources().getString(R.string.EmailTdkBolehKosong), getResources().getString(R.string.Perhatian));
            }
        }else if (v == upload_foto) {
            showPickPhotoDialog();
        }



        /*
        if (v == lanjut) {
            Intent i = new Intent(getBaseContext(), BumperActivity.class);
            startActivity(i);
            finish();
        }
        */
    }

    private void register(final String username,
                          final String email,
                          final String password,
                          final String password_2) {


        System.out.println("username :"+username+", email :"+email+", password :"+password+", password_2 : "+password_2);


        StringRequest listdata = new StringRequest(Request.Method.POST,
            ApiReferences.registerUserAwam(),
            new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.i("RESULT JSON", response.toString());

                    try {
                        Message msg = Parser.getMessage(response);

                        dialogRegister(msg.getMessages(), getResources().getString(R.string.Perhatian));

                    } catch (JsonSyntaxException e) {
                        dialog(getResources().getString(R.string.KesalahanServer), getResources().getString(R.string.Perhatian));
                    }
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {

                    NetworkResponse response = error.networkResponse;
                    if (error instanceof ServerError && response != null) {
                        try {
                            String res = new String(response.data,
                                    HttpHeaderParser.parseCharset(response.headers, "utf-8"));
                            // Now you can use any deserializer to make sense of data
                            JSONObject obj = new JSONObject(res);

                            String b = obj.toString().substring(1, obj.toString().length() -1);
                            String c = b.toString().replace("messages", "");
                            String d = c.toString().replace("\"", "");
                            String e = d.toString().replace(":", "");
                            String f = e.toString().replace("\\", "");
                            String g = f.toString().replace("[", "");
                            String h = g.toString().replace("]", "");

                            dialog(h, getResources().getString(R.string.Perhatian));
                        } catch (UnsupportedEncodingException e1) {
                            // Couldn't properly decode data to string
                            e1.printStackTrace();
                        } catch (JSONException e2) {
                            // returned data is not JSONObject?
                            e2.printStackTrace();
                        }
                    }


                }
            }) {

                protected Map<String, String> getParams() {
                    HashMap<String, String> params = new HashMap<String, String>();
                    params.put("username", username);
                    params.put("email", email);
                    params.put("password", password);
                    params.put("password_2", password_2);
                    return params;


                    /*
                    params.put("name", "nandha");
                    params.put("username", "nandhaaja9");
                    params.put("email", "nandhaaja9@gmail.com");
                    params.put("mobile_phone", "0817171717");
                    params.put("password", "123456");
                    params.put("password_2", "123456");
                    params.put("dob", "27-05-1985");
                    params.put("gender", "m");
                    return params;
                    */
                }
            };

            listdata.setRetryPolicy(new DefaultRetryPolicy(
                    20000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            mRequest.add(listdata);
    }



    @Override
    public int getLayout() {
        return R.layout.activity_awam_register;
    }

    @Override
    public void updateUI() {

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1) {
            if (resultCode == RESULT_OK && data != null) {

                selectedImage = data.getData();
                String[] filePathColumn = {MediaStore.Images.Media.DATA};

                Cursor cursor = getContentResolver().query(selectedImage,
                        filePathColumn, null, null, null);
                cursor.moveToFirst();

                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                String picturePath = cursor.getString(columnIndex);
                imagePath = picturePath;
                cursor.close();

                if (imagePath != null) {
                    cropPicture();
                }

            }
        } else if (requestCode == 0) {
            if (resultCode == RESULT_OK) {

                final String[] imageColumns = {MediaStore.Images.Media._ID,
                        MediaStore.Images.Media.DATA};
                final String imageOrderBy = MediaStore.Images.Media._ID
                        + " DESC";
                Cursor cursor = getContentResolver().query(
                        MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                        imageColumns, null, null, imageOrderBy);
                int column_index_data = cursor
                        .getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
                cursor.moveToFirst();

                imagePath = cursor.getString(column_index_data);
                cursor.close();

                if (imagePath != null) {
                    cropPicture();
                } else {
                    String text = getResources().getString(R.string.FotoUlang);
                    Toast.makeText(context, text, Toast.LENGTH_LONG).show();
                }
            }
        } else if (requestCode == Crop.REQUEST_CROP && resultCode == RESULT_OK) {
            imagePath = this.getExternalFilesDir(null)+fileDir+filename;
            Ion.with(context)
                    .load("http://54.169.50.255/revamp/api/v1/user/register")
                    .setHeader("token", preferences.getString(Preferences.ACCESS_TOKEN, ""))
                    .setTimeout(60 * 60 * 1000)
                    .setMultipartFile("upload_foto", new File(imagePath))
                    .asJsonObject()
                    .setCallback(new FutureCallback<JsonObject>() {
                        @Override
                        public void onCompleted(Exception e, JsonObject result) {
                            if (e != null) {
                                Toast.makeText(context, "Error uploading file", Toast.LENGTH_LONG).show();
                                return;
                            }
                            Toast.makeText(context, "File upload complete", Toast.LENGTH_LONG).show();
                            imagePath = "http://54.169.50.255/revamp/" + result.getAsJsonObject("data").get("upload_foto").getAsString();
                            editor.putString(Preferences.FOTO, imagePath);
                            editor.commit();
                            Glide.with(AwamRegistrationActivity.this)
                                    .load(preferences.getString(Preferences.FOTO, ""))
                                    .asBitmap()
                                    .override(200, 200)
                                    .into(new SimpleTarget<Bitmap>() {
                                        @Override
                                        public void onResourceReady(Bitmap bitmap, GlideAnimation anim) {
                                            Bitmap b = hexa.getHexagonShape(hexa.scaleCenterCrop(bitmap, 200, 200));
                                            upload_foto.setImageBitmap(b);
                                        }
                                    });
                        }
                    });
        }

    }


    @Override
    public void onBackPressed() {
        Intent i = new Intent(getBaseContext(), SplashPagerActivity.class);
        startActivity(i);
        finish();
    }

    public void showPickPhotoDialog() {

        dialog = new MaterialDialog.Builder(this)
                .title("Pick Photo")
                .negativeText("Cancel")
                .items(R.array.photo)
                .itemsCallback(new MaterialDialog.ListCallback() {
                    @Override
                    public void onSelection(MaterialDialog dialog, View view, int which, CharSequence text) {
                        if (which == 0) {
                            pickFromCamera();
                        } else {
                            pickFromGalery();
                        }
                    }
                })
                .show();
    }


    private void pickFromCamera() {
        int requestCode = 0;
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss")
                .format(new Date());
        String imageFileName = "PHOTO" + timeStamp + "_";

        ContentValues values = new ContentValues();
        values.put(MediaStore.Images.Media.TITLE, imageFileName);
        selectedImage = getContentResolver().insert(
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);

        // create Intent to take a picture and return control to the calling
        // application

        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        intent.putExtra(MediaStore.EXTRA_OUTPUT, selectedImage);

        // start the image capture Intent
        startActivityForResult(intent, requestCode);
        dialog.dismiss();
    }

    private void pickFromGalery() {
        int requestCode = 1;
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss")
                .format(new Date());
        String imageFileName = "PHOTO" + timeStamp + "_";

        ContentValues values = new ContentValues();
        values.put(MediaStore.Images.Media.TITLE, imageFileName);
        selectedImage = getContentResolver().insert(
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);

        // create Intent to take a picture and return control to the calling
        // application

        Intent intent = new Intent(Intent.ACTION_PICK);
        intent.setType("image/*");
        intent.putExtra(MediaStore.EXTRA_OUTPUT, selectedImage);

        startActivityForResult(intent, requestCode);
        dialog.dismiss();
    }

    public void dialog(String content, String msg) {
        new MaterialDialog.Builder(this)
                .title(msg)
                .content(content)
                .positiveText("Ok")
                .callback(new MaterialDialog.ButtonCallback() {
                    @Override
                    public void onPositive(MaterialDialog dialog) {

                    }

                })
                .show();
    }

    private void cropPicture() {
        if (!fileExists(fileDir)) {
            File newDir = new File(this.getExternalFilesDir(null)+fileDir);
            newDir.mkdirs();
        }
        if (fileExists(fileDir+filename)) {
            deleteFile(fileDir+filename);
        }
        File imgFile = new File(this.getExternalFilesDir(null)+fileDir, filename);
        resultImage = Uri.fromFile(imgFile);
        Crop.of(selectedImage, resultImage).asSquare().start(this);
    }

    public static String convertStreamToString(InputStream is) throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();
        String line = null;
        while ((line = reader.readLine()) != null) {
            sb.append(line).append("\n");
        }
        reader.close();
        return sb.toString();
    }

    public static String getStringFromFile (String filePath) throws Exception {
        File fl = new File(filePath);
        FileInputStream fin = new FileInputStream(fl);
        String ret = convertStreamToString(fin);
        //Make sure you close all streams.
        fin.close();
        return ret;
    }

    public void dialogRegister(String content, String msg) {
        new MaterialDialog.Builder(this)
                .title(msg)
                .content(content)
                .positiveText("Ok")
                .callback(new MaterialDialog.ButtonCallback() {
                    @Override
                    public void onPositive(MaterialDialog dialog) {
                        Intent i = new Intent(getApplicationContext(), SplashPagerActivity.class);
                        startActivity(i);
                        finish();
                    }

                })
                .show();
    }
}