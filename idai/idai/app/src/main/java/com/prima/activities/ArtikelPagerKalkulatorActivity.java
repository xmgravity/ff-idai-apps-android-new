package com.prima.activities;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.prima.MainActivity;
import com.prima.Preferences;
import com.prima.adapters.PagerAdapter;
import com.prima.controllers.ApiReferences;
import com.prima.controllers.Parser;
import com.prima.entities.Artikel;
import com.prima.entities.Pasien;
import com.prima.fragments.ArtikelPinkFragment;
import com.prima.helpers.DateHelper;

import org.joda.time.LocalDate;
import org.joda.time.Years;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import prima.test.prima.R;

/**
 * Created by Codelabs on 06/10/2015.
 */
public class ArtikelPagerKalkulatorActivity extends BaseActivity implements View.OnClickListener {
    private Button ok;
    private Context mContext;
    private ViewPager mPager;
    private PagerAdapter mPagerAdapter;
    private static int currentPage;
    PageListener pageListener;
    static TextView title;
    List<Fragment> fragmentList = new ArrayList<Fragment>();
    static LinearLayout indikatorWrapper;
    private Artikel artikel;

    //

    private ImageView close;
    private MaterialDialog dialog;
    DateHelper dateHelper;
    private RequestQueue mRequest;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = getApplicationContext();
    }

    @Override
    public void initView() {
        indikatorWrapper = (LinearLayout) findViewById(R.id.indikator_pager);
        mPager = (ViewPager) findViewById(R.id.pager);
        title = (TextView) findViewById(R.id.title);
        pageListener = new PageListener();
        mPager.setOnPageChangeListener(pageListener);
        ok = (Button) findViewById(R.id.lanjut);
        Typeface tf = Typeface.createFromAsset(getApplicationContext().getAssets(), "BREVIA-BLACK.OTF");
        title.setTypeface(tf);


        Typeface tfAriali = Typeface.createFromAsset(getApplicationContext().getAssets(), "ariali.ttf");
        Typeface tfArialb = Typeface.createFromAsset(getApplicationContext().getAssets(), "arialbd.ttf");

        TextView trademark = (TextView) findViewById(R.id.trademark);
        TextView trademark2 = (TextView) findViewById(R.id.trademark2);
        TextView trademark3 = (TextView) findViewById(R.id.trademark3);
        trademark.setTypeface(tfAriali);
        trademark2.setTypeface(tfArialb);
        trademark3.setTypeface(tfAriali);

        if(preferences.getString(Preferences.TIPE_USER, "").equalsIgnoreCase("awam")){
            trademark.setVisibility(View.GONE);
            trademark2.setVisibility(View.GONE);
            trademark3.setVisibility(View.GONE);
        }
        //
        dateHelper = new DateHelper();
        mRequest = Volley.newRequestQueue(getApplicationContext());
        close = (ImageView) findViewById(R.id.close);

    }

    @Override
    public void setUICallbacks() {
        ok.setOnClickListener(this);
        close.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        if (v == ok) {
            if (preferences.getString(Preferences.ID, "").equalsIgnoreCase("") || preferences.getString(Preferences.JENIS_KELAMIN, "").equalsIgnoreCase("")) {
                // get prompts.xml view
                LayoutInflater li = LayoutInflater.from(getApplicationContext());
                View dialogView = li.inflate(R.layout.dialog_offline_kalkulator, null);
                final Dialog dialog3;
                dialog3 = new Dialog(context);
                dialog3.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog3.setContentView(dialogView);
                //make transparent dialog_sesi_offline_pelod
                dialog3.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                dialog3.show();


                Button dialogLanjut = (Button) dialogView.findViewById(R.id.ok);
                final RadioButton rad1 = (RadioButton) dialogView.findViewById(R.id.rad1);
                final RadioButton rad2 = (RadioButton) dialogView.findViewById(R.id.rad2);
                rad1.setChecked(true);

                rad1.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        rad1.setChecked(true);
                        rad2.setChecked(false);
                    }
                });

                rad2.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        rad2.setChecked(true);
                        rad1.setChecked(false);
                    }
                });

                final TextView edt = (TextView) dialogView.findViewById(R.id.edt1);
                final EditText edt2 = (EditText) dialogView.findViewById(R.id.edt2);
                final EditText edt3 = (EditText) dialogView.findViewById(R.id.edt3);


                edt.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        showDatepickerDialog(edt);
                    }
                });

                dialogLanjut.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (edt.getText().toString().equals("") ||
                                edt3.getText().toString().equals("")) {
                            dialog(getResources().getString(R.string.IsiSeluruhIsian), getResources().getString(R.string.Perhatian));
                        } else {
                            String jk;
                            if (rad1.isChecked()) {
                                jk = "1";
                            } else if (rad2.isChecked()) {
                                jk = "0";
                            } else {
                                jk = "";
                            }
                            //     saveSesiPasien(jk, edt.getText().toString(), edt3.getText().toString(), edt2.getText().toString());
                            editor.putString(Preferences.TINGGI, edt2.getText().toString());
                            editor.putString(Preferences.BERAT, edt3.getText().toString());
                            editor.putString(Preferences.BIRTH_DAY,edt.getText().toString());
                            editor.putString(Preferences.JENIS_KELAMIN,jk);
                            editor.putString(Preferences.ID,"1");
                            editor.commit();
                            calculateBmr();
                            dialog3.dismiss();
                        }
                    }
                });

            } else {
                calculateBmr();
            }
        } else if (v == close) {
            Intent i = new Intent(getApplicationContext(), MainActivity.class);
            startActivity(i);
            finish();
        }
    }


    @Override
    public int getLayout() {
        return R.layout.activity_kalkulator_basal;
    }

    @Override
    public void updateUI() {
        indikatorWrapper.removeAllViewsInLayout();
        fragmentList.clear();
        Bundle bn = getIntent().getExtras();
        if (bn != null) {
            String respon = bn.getString("result");
            artikel = Parser.getArtikel(respon);
            //   title.setText(artikel.getObjects().get(0).getTitle());
            for (int i = 0; i < artikel.getObjects().size(); i++) {
                fragmentList.add(new ArtikelPinkFragment(i, artikel));
                View child = getLayoutInflater().inflate(R.layout.indikator_pager, null);
                View child2 = getLayoutInflater().inflate(R.layout.indikator_pager_blue, null);
                if (i == 0) {
                    indikatorWrapper.addView(child2);
                } else {
                    indikatorWrapper.addView(child);
                }
            }

            setupFrae();
            if (artikel.getObjects().size() > 1) {
                indikatorWrapper.setVisibility(View.VISIBLE);
            } else {
                indikatorWrapper.setVisibility(View.GONE);
            }
        }


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

    }


    void calculateBmr() {
        float bmr = 0.0f;
        float berat = Float.valueOf(preferences.getString(Preferences.BERAT, ""));
        if(!preferences.getString(Preferences.TINGGI, "").equals("")){
            float tinggi = Float.valueOf(preferences.getString(Preferences.TINGGI, ""));

            if (getAge() > 18) {
                dialog(getResources().getString(R.string.KalkulatorBasalDibawah18Th), getResources().getString(R.string.Pesan));
            } else {
                if (getAge() >= 0 && getAge() < 3) {
                    if (preferences.getString(Preferences.JENIS_KELAMIN, "").equals("0")) {
                        bmr = (16.25f * berat) + (1023.2f * (tinggi / 100f)) - 413.5f;
                    } else {
                        bmr = (0.167f * berat) + (1517.4f * (tinggi / 100f)) - 617.6f;
                    }
                } else if (getAge() >= 3 && getAge() <= 10) {
                    if (preferences.getString(Preferences.JENIS_KELAMIN, "").equals("0")) {
                        bmr = (16.97f * berat) + (161.8f * (tinggi / 100f)) + 371.2f;
                    } else {
                        bmr = (19.6f * berat) + (130.3f * (tinggi / 100f)) + 414.9f;
                    }
                } else if (getAge() >= 10 && getAge() <= 18) {
                    if (preferences.getString(Preferences.JENIS_KELAMIN, "").equals("0")) {
                        bmr = (8.365f * berat) + (465f * (tinggi / 100f)) + 200f;
                    } else {
                        bmr = (16.25f * berat) + (137.2f * (tinggi / 100f)) + 515.5f;
                    }
                }

                Intent i = new Intent(getApplicationContext(), KalkulatorBasalResultActivity.class);
                i.putExtra("result", bmr+"");
                i.putExtra("height", "(WH)");
                startActivity(i);
                //finish();
            }

        }else{

            System.out.println("Schofield formula");
            if (getAge() > 18) {
                dialog(getResources().getString(R.string.KalkulatorBasalDibawah18Th), getResources().getString(R.string.Pesan));
            } else {
                if (getAge() >= 0 && getAge() < 3) {
                    if (preferences.getString(Preferences.JENIS_KELAMIN, "").equals("1")) {
                        bmr = (59.48f * berat) - 30.33f;
                    } else {
                        bmr = (58.29f * berat) - 31.05f;
                    }
                } else if (getAge() >= 3 && getAge() <= 10) {
                    if (preferences.getString(Preferences.JENIS_KELAMIN, "").equals("1")) {
                        bmr = (22.7f * berat) + 505f;
                    } else {
                        bmr = (20.3f * berat) + 486f;
                    }
                } else if (getAge() >= 10 && getAge() <= 18) {
                    if (preferences.getString(Preferences.JENIS_KELAMIN, "").equals("1")) {
                        bmr = (13.4f * berat) + 693f;
                    } else {
                        bmr = (17.7f * berat) + 659f;
                    }
                }

                Intent i = new Intent(getApplicationContext(), KalkulatorBasalResultActivity.class);
                i.putExtra("result", Math.round(bmr)+"");
                i.putExtra("height", "(W)");
                startActivity(i);
                //finish();
            }
        }





    }


    public int getAge() {
        Years years = Years.yearsBetween(new LocalDate(preferences.getString(Preferences.BIRTH_DAY, "")), new LocalDate());
        return years.getYears();
    }


    private void saveSesiPasien(final String jenis_kelamin, final String tanggal_lahir, final String berat, final String tinggi) {
        progresDialog(false, getResources().getString(R.string.MenyimpanDataPasien));
        StringRequest postlogin = new StringRequest(Request.Method.POST,
                ApiReferences.insertPasien(), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                Log.i("RESULT JSON", response.toString());
                Pasien pasien = Parser.getPasien(response
                        .toString());
                if (pasien.getStatus().equalsIgnoreCase("1")) {
                    editor.putString(Preferences.ID, pasien.getObjects().getId());
                    editor.putString(Preferences.BERAT, pasien.getObjects().getBerat());
                    editor.putString(Preferences.TINGGI, pasien.getObjects().getTinggi());
                    editor.putString(Preferences.JENIS_KELAMIN, pasien.getObjects().getJenis_kelamin());
                    editor.putString(Preferences.BIRTH_DAY, pasien.getObjects().getTanggal_lahir());
                    editor.commit();
                    dialog.dismiss();
                    dialogEmail("Kode : " + pasien.getObjects().getId() + "\nSimpan Code ini untuk pemeriksaan berikutnya.", "Pendaftaran Sesi Pasien Berhasil", pasien.getObjects().getId());
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                dialog.dismiss();
                dialog(getResources().getString(R.string.NoConnection), getResources().getString(R.string.Perhatian));
            }
        }) {

            protected Map<String, String> getParams() {
                HashMap<String, String> params = new HashMap<String, String>();
                params.put("jenis_kelamin", jenis_kelamin);
                params.put("tanggal_lahir", tanggal_lahir);
                params.put("berat", berat);
                params.put("tinggi", tinggi);
                return params;
            }
        };
        postlogin.setRetryPolicy(new DefaultRetryPolicy(
                20000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        mRequest.add(postlogin);
    }


    public void dialogEmail(String content, String msg, final String kode) {
        new MaterialDialog.Builder(this)
                .title(msg)
                .content(content)
                .autoDismiss(false)
                .cancelable(false)
                .positiveText(getResources().getString(R.string.KirimEmail))
                .negativeText(getResources().getString(R.string.tutup))
                .callback(new MaterialDialog.ButtonCallback() {
                    @Override
                    public void onPositive(MaterialDialog dialog) {
                        Intent sendEmail = new Intent(Intent.ACTION_SEND);
                        sendEmail.setType("text/plain");
                        sendEmail.putExtra(Intent.EXTRA_EMAIL, new String[]{""});
                        sendEmail.putExtra(Intent.EXTRA_SUBJECT, "Kode Pasien");
                        sendEmail.putExtra(Intent.EXTRA_TEXT, "Berikut ini adalah kode pasien anda.\nKode : " + kode);
                        startActivity(Intent.createChooser(sendEmail, "Email With"));
                    }

                    @Override
                    public void onNegative(MaterialDialog dialog) {
                        dialog.dismiss();
                        calculateBmr();
                    }
                })
                .show();
    }

    private void progresDialog(boolean horizontal, String msg) {
        dialog = new MaterialDialog.Builder(this)
                .title(msg)
                .content(getResources().getString(R.string.MohonTunggu))
                .progress(true, 0)
                .progressIndeterminateStyle(horizontal)
                .cancelable(false)
                .show();
    }

    private void showDatepickerDialog(final TextView txt) {
        dateHelper.showDatePicker(this, System.currentTimeMillis(), new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                int _year = year;
                int _month = monthOfYear + 1;
                int _day = dayOfMonth;
                String date = _year + "-" + _month + "-" + _day;
                String birthDate = dateHelper.formatDate(date);
                txt.setText(birthDate);
            }
        });
    }


    public void dialog(String content, String msg) {
        new MaterialDialog.Builder(this)
                .title(msg)
                .content(content)
                .positiveText("OK")
                .callback(new MaterialDialog.ButtonCallback() {
                    @Override
                    public void onPositive(MaterialDialog dialog) {

                    }
                })
                .show();
    }


    @Override
    public void onBackPressed() {
        Intent i = new Intent(getApplicationContext(), MainActivity.class);
        startActivity(i);
        finish();
    }

    private class PageListener extends ViewPager.SimpleOnPageChangeListener {
        public void onPageSelected(int position) {
            currentPage = position;
            // title.setText(artikel.getObjects().get(position).getTitle());
            indikatorWrapper.removeAllViewsInLayout();
            for (int i = 0; i < artikel.getObjects().size(); i++) {
                View child = getLayoutInflater().inflate(R.layout.indikator_pager, null);
                View child2 = getLayoutInflater().inflate(R.layout.indikator_pager_blue, null);
                if (i == position) {
                    indikatorWrapper.addView(child2);
                } else {
                    indikatorWrapper.addView(child);
                }
            }

        }
    }


    private void setupFrae() {
        mPagerAdapter = new PagerAdapter(getSupportFragmentManager(),
                ArtikelPagerKalkulatorActivity.this, fragmentList);
        mPager.setAdapter(mPagerAdapter);

    }

}
