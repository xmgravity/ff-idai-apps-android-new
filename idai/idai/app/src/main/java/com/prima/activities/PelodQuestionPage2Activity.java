package com.prima.activities;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import prima.test.prima.R;

/**
 * Created by Codelabs on 10/08/2015.
 */
public class PelodQuestionPage2Activity extends BaseActivity implements View.OnClickListener {
    private ImageView close;
    private ImageView back;
    private Button viewHasil;
    TextView title;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }

    @Override
    public void initView() {
        back = (ImageView) findViewById(R.id.back);
        close = (ImageView) findViewById(R.id.close);
        viewHasil = (Button) findViewById(R.id.view_hasil);
        title = (TextView) findViewById(R.id.tv_actionbar_title);
        Typeface tf = Typeface.createFromAsset(getApplicationContext().getAssets(), "BREVIA-BLACK.OTF");
        title.setTypeface(tf);
        title.setText("PELOD Score");
    }

    @Override
    public void setUICallbacks() {
        back.setOnClickListener(this);
        close.setOnClickListener(this);
        viewHasil.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        if (v == back) {
            Intent i = new Intent(getApplicationContext(), PelodQuestionActivity.class);
            startActivity(i);
            finish();
        } else if (v == close) {
            Intent i = new Intent(getApplicationContext(), PelodActivity.class);
            startActivity(i);
            finish();
        } else if (v == viewHasil) {
            Intent i = new Intent(getApplicationContext(), PelodResultActivity.class);
            startActivity(i);
            finish();
        }
    }


    @Override
    public int getLayout() {
        return R.layout.activity_pelod_page2;
    }

    @Override
    public void updateUI() {

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

    }


    @Override
    public void onBackPressed() {
        Intent i = new Intent(getApplicationContext(), PelodQuestionActivity.class);
        startActivity(i);
        finish();
    }


}
