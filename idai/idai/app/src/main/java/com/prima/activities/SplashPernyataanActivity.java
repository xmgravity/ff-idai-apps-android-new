package com.prima.activities;

import android.content.Intent;
import android.graphics.Typeface;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;

import com.prima.Preferences;

import prima.test.prima.R;

/**
 * Created by Codelabs on 29/08/2015.
 */
public class SplashPernyataanActivity extends BaseActivity implements View.OnClickListener {
    TextView title, subtitle;
    Button lanjut;
    CheckBox setuju;

    @Override
    public void initView() {
        lanjut = (Button) findViewById(R.id.lanjut);
        setuju = (CheckBox) findViewById(R.id.setuju);

        title = (TextView) findViewById(R.id.title);
        subtitle = (TextView) findViewById(R.id.subtitleText);

        Typeface tf = Typeface.createFromAsset(getApplicationContext().getAssets(), "BREVIA-BLACK.OTF");
        Typeface tf2 = Typeface.createFromAsset(getApplicationContext().getAssets(), "BREVIA-SEMIBOLD.OTF");
        title.setTypeface(tf);
        subtitle.setTypeface(tf2);

//        Thread background = new Thread() {
//            public void run() {
//                try {
//                    // Thread will sleep for 5 seconds
//                    sleep(2000);
//                    Intent i = new Intent(getBaseContext(), BumperActivity.class);
//                    startActivity(i);
//                    finish();
//                } catch (Exception e) {
//                    Log.v("activity", "splash catch");
//                }
//            }
//        };

        if(preferences.getString(Preferences.TIPE_USER, "").equalsIgnoreCase("awam")){
            subtitle.setText(getResources().getString(R.string.PernyataanAwam));
        }else{
            subtitle.setText(getResources().getString(R.string.PernyataanDokter));
        }

//        // start thread
//        background.start();

    }

    @Override
    public void setUICallbacks() {
        lanjut.setOnClickListener(this);
        setuju.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        if (v == lanjut) {
            Intent i = new Intent(getBaseContext(), BumperActivity.class);
            startActivity(i);
            finish();
        }

    }


    @Override
    public int getLayout() {
        return R.layout.activity_splash_pernyataan;
    }

    @Override
    public void updateUI() {

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

    }


    @Override
    public void onBackPressed() {
        Intent i = new Intent(getBaseContext(), SplashPagerActivity.class);
        startActivity(i);
        finish();
    }
}