package com.prima.fragments;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

import com.prima.Preferences;
import com.prima.activities.BallardScoreQuestionActivity;
import com.prima.entities.Question;
import prima.test.prima.R;
import com.prima.activities.BallardResultActivity;
import com.prima.adapters.BallardListAdapter;

/**
 * Created by Codelabs on 07/08/2015.
 */
public class BallardScoreFragment extends BaseFragment implements View.OnClickListener {

    private Context mContext;
    public static final String ARG_PAGE = "page 2";
    private ArrayList<String> listItem = new ArrayList<String>();
    private ListView list;
    private ViewPager mPager;
    List<Fragment> fragmentList = new ArrayList<Fragment>();
    Question.Kuesioner kuesioner;
    String selected = "";
    BallardListAdapter adapterList;
    int indeks;
    private ArrayList<String> ansewer = new ArrayList<String>();
    int totalScore = 0;

    @SuppressLint("ValidFragment")
    public BallardScoreFragment() {}
    @SuppressLint("ValidFragment")
    public BallardScoreFragment(Question.Kuesioner kuesioner, int indeks) {
        this.kuesioner = kuesioner;
        this.indeks = indeks;
    }

    @Override
    public void initView(View view) {
        mContext = getActivity();
        list = (ListView) view.findViewById(R.id.listView);
        mPager = (ViewPager) getActivity().findViewById(R.id.pager);
        final Activity act = getActivity();

        if (act instanceof BallardScoreQuestionActivity) {
            fragmentList = ((BallardScoreQuestionActivity) act).getFragmentList();
            ansewer = ((BallardScoreQuestionActivity) act).getAnsewer();
        }

        adapterList = new BallardListAdapter(
                kuesioner.getAnswer_option(), mContext, selected);
        list.setAdapter(adapterList);


        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                ansewer.set(indeks, kuesioner.getAnswer_option().get(position).getScore());
                selected = String.valueOf(kuesioner.getAnswer_option().get(position).getScore());
                adapterList.setSelected(selected);
                adapterList.notifyDataSetChanged();
                int current = mPager.getCurrentItem();
                if (current != fragmentList.size() - 1) {
                    mPager.setCurrentItem(current + 1, true);
                } else {
                    int score = 0;
                    for (int j = 0; j < ansewer.size(); j++) {
                        score = score + Integer.valueOf(ansewer.get(j).replace(".00", ""));
                    }
                    if (preferences.getString(Preferences.ID, "").equals("1")) {
                        editor.putString(Preferences.BIRTH_DAY, "");
                        editor.putString(Preferences.JENIS_KELAMIN, "");
                        editor.putString(Preferences.BERAT, "");
                        editor.putString(Preferences.TINGGI, "");
                        editor.putString(Preferences.ID, "");
                        editor.commit();
                    }

                    if (act instanceof BallardScoreQuestionActivity) {
                        totalScore = ((BallardScoreQuestionActivity) act).getMax_score();
                    }
                    Intent i = new Intent(mContext, BallardResultActivity.class);
                    i.putExtra("score", score);
                    i.putExtra("max_score",totalScore);
                    startActivity(i);
                    getActivity().finish();
                }

            }
        });
    }

    @Override
    public void setUICallbacks() {

    }

    @Override
    public void updateUI() {
        adapterList.notifyDataSetChanged();
    }

    @Override
    public String getPageTitle() {
        return "Account Summary";
    }


    @Override
    public int getFragmentLayout() {
        return R.layout.list_view;
    }

    @Override
    public void onClick(View v) {

    }


}