package com.prima.activities;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.prima.Preferences;
import com.prima.adapters.ListAdapterQuestionMilestone;
import com.prima.controllers.ApiReferences;
import com.prima.controllers.Parser;
import com.prima.entities.AllRedflag;
import com.prima.entities.Message;
import com.prima.entities.Question;
import com.prima.helpers.DatabaseHandler;

import java.util.ArrayList;

import prima.test.prima.R;

/**
 * Created by Codelabs on 12/08/2015.
 */
public class MilestoneRedflagQuestionActivity extends BaseActivity implements View.OnClickListener {
    private ImageView close;
    private ImageView back;
    private Button lanjut;
    TextView title;
    private ListView list;
    private ArrayList<String> listItem = new ArrayList<String>();
    ImageView redflag;
    private ArrayList<Integer> answered = new ArrayList<Integer>();
    ListAdapterQuestionMilestone adapterList;
    MaterialDialog dialog;
    private RequestQueue mRequest;
    Question question;
    DatabaseHandler db;
    String min, max;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }

    @Override
    public void initView() {
        db = new DatabaseHandler(context);
        mRequest = Volley.newRequestQueue(getApplicationContext());
        back = (ImageView) findViewById(R.id.back);
        close = (ImageView) findViewById(R.id.close);
        lanjut = (Button) findViewById(R.id.lanjut);
        title = (TextView) findViewById(R.id.tv_actionbar_title);
        Typeface tf = Typeface.createFromAsset(getApplicationContext().getAssets(), "BREVIA-BLACK.OTF");
        title.setTypeface(tf);
        redflag = (ImageView) findViewById(R.id.redflag);
        redflag.setVisibility(View.INVISIBLE);
        title.setText(getResources().getString(R.string.TahapandanTandaBahayaPerkembangan));

        list = (ListView) findViewById(R.id.listView);


    }

    @Override
    public void setUICallbacks() {
        back.setOnClickListener(this);
        close.setOnClickListener(this);
        lanjut.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        if (v == back) {
            //   Intent i = new Intent(getApplicationContext(), MilestoneRedflagActivity.class);
            //    startActivity(i);
            if (preferences.getString(Preferences.ID, "").equals("1")) {
                editor.putString(Preferences.BIRTH_DAY, "");
                editor.putString(Preferences.JENIS_KELAMIN, "");
                editor.putString(Preferences.BERAT, "");
                editor.putString(Preferences.TINGGI, "");
                editor.putString(Preferences.ID, "");
                editor.commit();
            }
            finish();
        } else if (v == close) {
            //    Intent i = new Intent(getApplicationContext(), MilestoneRedflagActivity.class);
            //    startActivity(i);
            if (preferences.getString(Preferences.ID, "").equals("1")) {
                editor.putString(Preferences.BIRTH_DAY, "");
                editor.putString(Preferences.JENIS_KELAMIN, "");
                editor.putString(Preferences.BERAT, "");
                editor.putString(Preferences.TINGGI, "");
                editor.putString(Preferences.ID, "");
                editor.commit();
            }
            finish();
        } else if (v == lanjut) {
            answered = adapterList.getAnswered();
            boolean answeredAll = true;
            for (int i = 0; i < answered.size(); i++) {
                if (answered.get(i) == 0) {
                    answeredAll = false;
                }
            }

            if (answeredAll) {
                if (redflag.getVisibility() == View.VISIBLE) {
                    // get prompts.xml view
                    LayoutInflater li = LayoutInflater.from(getApplicationContext());
                    View dialogView = li.inflate(R.layout.dialog_peringatan_redflag, null);

                    final Dialog dialog;
                    dialog = new Dialog(context);
                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    dialog.setContentView(dialogView);
                    //make transparent dialog_sesi_offline_pelod
                    dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                    dialog.show();

                    Button lanjut = (Button) dialogView.findViewById(R.id.lanjut);
                    lanjut.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog.dismiss();
                            if (preferences.getString(Preferences.ID, "").equals("")) {
                                getQuestion();
                            } else {
                                getQuestions();
                            }
                        }
                    });

                } else if (redflag.getVisibility() == View.INVISIBLE) {
                    Intent i = new Intent(getApplicationContext(), MilestoneRedflagResult.class);
                    i.putExtra("status", "OK");
                    startActivity(i);
                    finish();
                }
            } else {
                dialog2(getResources().getString(R.string.JawabSeluruhPertanyaan), getResources().getString(R.string.Perhatian));
            }
        }
    }


    @Override
    public int getLayout() {
        return R.layout.activity_milestone_question;
    }

    @Override
    public void updateUI() {
        Bundle bn = getIntent().getExtras();
        if (bn != null) {
            min = bn.getString("min");
            max = bn.getString("max");
            String response = bn.getString("respon");
            Log.i("RESULT JSON", response);

            Question question = Parser.getQuestion(response);
            adapterList = new ListAdapterQuestionMilestone(
                    question, getApplicationContext(), redflag);
            list.setAdapter(adapterList);


        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

    }


    @Override
    public void onBackPressed() {
        //   Intent i = new Intent(getApplicationContext(), MilestoneRedflagActivity.class);
        // startActivity(i);
        if (preferences.getString(Preferences.ID, "").equals("1")) {
            editor.putString(Preferences.BIRTH_DAY, "");
            editor.putString(Preferences.JENIS_KELAMIN, "");
            editor.putString(Preferences.BERAT, "");
            editor.putString(Preferences.TINGGI, "");
            editor.putString(Preferences.ID, "");
            editor.commit();
        }
        finish();
    }

    private void getQuestions() {
        progresDialog(false, getResources().getString(R.string.MemuatData));
        String uri;
        final String code = "CMAR";
        if (preferences.getString(Preferences.ID, "").equals("")) {
            uri = ApiReferences.getAllQuestionnaireByAge("CMAR", preferences.getString(Preferences.LANGUAGE, ""));
        } else {
            uri = ApiReferences.getQuestion(preferences.getString(Preferences.ID, ""), code, preferences.getString(Preferences.LANGUAGE, ""));
        }

        StringRequest listdata = new StringRequest(Request.Method.GET, uri,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        dialog.dismiss();
                        try {
                            if (response.equals("[]")) {
                                dialog(getResources().getString(R.string.BelumAdaData), getResources().getString(R.string.Pesan));
                            } else {
                                Message msg = Parser.getMessage(response);
                                if (msg.getStatus().equals("1")) {
                                    Log.i("RESULT JSON", response.toString());
                                    question = new Question();
                                    question = Parser.getQuestion(response);

                                    for (int j = 0; j < question.getObjects().size(); j++) {
                                        db.deleteQuestion(question.getObjects().get(j).getId(), question.getObjects().get(j).getMin_age(), question.getObjects().get(j).getMax_age(), "");
                                        db.deleteAnswer(question.getObjects().get(j).getId());
                                        db.insertQuestion(question.getObjects().get(j), code);
                                        for (int k = 0; k < question.getObjects().get(j).getAnswer_option().size(); k++) {
                                            db.insertAnswer(question.getObjects().get(j).getAnswer_option().get(k));
                                        }
                                    }

                                    Intent i = new Intent(getApplicationContext(), MilestoneRedflagSumaryActivity.class);
                                    i.putExtra("respon", response);
                                    startActivity(i);
                                    finish();

                                } else {
                                    dialog(msg.getMessages(), getResources().getString(R.string.Pesan));
                                }
                            }
                        } catch (JsonSyntaxException e) {
                            dialog(getResources().getString(R.string.KesalahanServer), getResources().getString(R.string.Perhatian));
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dialog.dismiss();
                //       dialog(getResources().getString(R.string.NoConnection), getResources().getString(R.string.Perhatian));
                Question temp;
                if (preferences.getString(Preferences.ID, "").equals("")) {
                    temp = db.getQuestionNosesi(code, Integer.valueOf(min), Integer.valueOf(max));
                } else {
                    temp = db.getQuestion(code, getMonth());
                }
                Gson gson = new Gson();
                Log.i("RESULT JSON2", gson.toJson(temp));
                if (temp.getObjects().size() > 0) {
                    Intent i = new Intent(getApplicationContext(), MilestoneRedflagSumaryActivity.class);
                    i.putExtra("respon", gson.toJson(temp));
                    startActivity(i);
                    finish();
                } else {
                    dialog(getResources().getString(R.string.NoConnection), getResources().getString(R.string.Perhatian));
                }
            }
        });
        listdata.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        mRequest.add(listdata);
    }


    public void dialog2(String content, String msg) {
        new MaterialDialog.Builder(this)
                .title(msg)
                .content(content)
                .negativeText("OK")
                .callback(new MaterialDialog.ButtonCallback() {
                    @Override
                    public void onPositive(MaterialDialog dialog) {

                    }

                    @Override
                    public void onNegative(MaterialDialog dialog) {
                        super.onNegative(dialog);
                    }

                })
                .show();
    }

    public void dialog(String content, String msg) {
        new MaterialDialog.Builder(this)
                .title(msg)
                .content(content)
                .positiveText(getResources().getString(R.string.MuatUlang))
                .negativeText(getResources().getString(R.string.Kembali))
                .callback(new MaterialDialog.ButtonCallback() {
                    @Override
                    public void onPositive(MaterialDialog dialog) {
                        getQuestion();
                    }

                    @Override
                    public void onNegative(MaterialDialog dialog) {
                        super.onNegative(dialog);
                    }

                })
                .show();
    }


    private void getQuestion() {
        progresDialog(false, getResources().getString(R.string.MemuatData));
        StringRequest listdata = new StringRequest(Request.Method.GET,
                ApiReferences.getAllQuestionnaireByAge("CMAR", preferences.getString(Preferences.LANGUAGE, "")),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        if (response.equals("[]")) {
                            dialog(getResources().getString(R.string.BelumAdaData), getResources().getString(R.string.Pesan));
                        } else {
                            Message msg = Parser.getMessage(response);
                            if (msg.getStatus().equals("1")) {
                                Log.i("RESULT JSON", response.toString());
                                AllRedflag redflag = Parser.getAllRedFlags(response);



                                /*for (int j = 0; j < redflag.getObjects().size(); j++) {

                                    for (int z = 0; z < redflag.getObjects().get(j).getData().size(); z++) {

                                        db.deleteQuestion(redflag.getObjects().get(j).getData().get(z).getId(), redflag.getObjects().get(j).getData().get(z).getMin_age(), redflag.getObjects().get(j).getData().get(z).getMax_age(), "");
                                        db.deleteAnswer(redflag.getObjects().get(j).getData().get(z).getId());
                                        db.insertCmar(redflag.getObjects().get(j).getData().get(z), "CMAR", redflag.getObjects().get(j).getId_age(), redflag.getObjects().get(j).getMin_age(), redflag.getObjects().get(j).getMax_age());
                                        for (int k = 0; k < redflag.getObjects().get(j).getData().get(z).getAnswer_option().size(); k++) {
                                            db.insertAnswer(redflag.getObjects().get(j).getData().get(z).getAnswer_option().get(k));
                                        }
                                    }
                                }*/

//                                Question temp;
//                                if (preferences.getString(Preferences.ID, "").equals("")) {
//                                    temp = db.getQuestionNosesi("CMAR", Integer.valueOf(min), Integer.valueOf(max));
//                                } else {
//                                    temp = db.getQuestion("CMAR", getMonth());
//                                }
//                                Gson gson = new Gson();
//                                Log.i("RESULT JSON2", gson.toJson(temp));
//                                if (temp.getObjects().size() > 0) {
//                                    Intent i = new Intent(getApplicationContext(), MilestoneRedflagSumaryActivity.class);
//                                    i.putExtra("respon", gson.toJson(temp));
//                                    startActivity(i);
//                                    finish();
//                                }


                                Intent i = new Intent(getApplicationContext(), AllRedflagSummaryActivity.class);
                                i.putExtra("respon", response.toString());
                                startActivity(i);
                                finish();
                            } else {
                                dialog(msg.getMessages(), getResources().getString(R.string.Pesan));
                            }
                        }
                        dialog.dismiss();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dialog.dismiss();
                // dialog(getResources().getString(R.string.NoConnection), getResources().getString(R.string.Perhatian));

                AllRedflag temp = db.getAllRedflag("CMAR");
                Gson gson = new Gson();
                Log.i("RESULT JSON2", gson.toJson(temp));
                if (temp.getObjects().size() > 0) {
                    Intent i = new Intent(getApplicationContext(), AllRedflagSummaryActivity.class);
                    i.putExtra("respon", gson.toJson(temp));
                    startActivity(i);
                    finish();
                } else {
                    dialog(getResources().getString(R.string.NoConnection), getResources().getString(R.string.Perhatian));
                }
            }
        });
        listdata.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        mRequest.add(listdata);
    }


    private void progresDialog(boolean horizontal, String msg) {
        dialog = new MaterialDialog.Builder(this)
                .title(msg)
                .content(getResources().getString(R.string.MohonTunggu))
                .progress(true, 0)
                .progressIndeterminateStyle(horizontal)
                .cancelable(false)
                .show();
    }

}
