/**
 * @author Codelabs
 * 
 */

package com.prima.helpers;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.content.Context;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

@SuppressLint({ "SimpleDateFormat", "DefaultLocale" })
public class DateHelper {
	private static final int SECOND_CONVERT = 1000;
	private static DateHelper instance;
	public static String API_DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";
	public static DateHelper getInstance(){
		if (instance == null) {
			instance = new DateHelper();
		}
		return instance;
	}
	
	public String formatDate(String date){
		return formatDate("yyyy-MM-dd", date, "yyyy-MM-dd");
	}
	
	public String formatDate(String dateFormat, String date,String toFormat) {
		return formatDate(dateFormat, date, toFormat, null, null);
	}

	public String formatDate(String dateFormat, String date,String toFormat, Locale fromLocale, Locale toLocale) {
		String formatted = "";
		DateFormat formatter = fromLocale == null ? new SimpleDateFormat(dateFormat) : new SimpleDateFormat(dateFormat, fromLocale);
		try {
			Date dateStr = formatter.parse(date);
			formatted = formatter.format(dateStr);
			Date formatDate = formatter.parse(formatted);
			formatter = toLocale == null ? new SimpleDateFormat(toFormat):new SimpleDateFormat(toFormat, toLocale);
			formatted = formatter.format(formatDate);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return formatted;
	}
	
	public Date getDate(String dateFormat, String date,String toFormat) {
		String formatted = "";
		DateFormat formatter = new SimpleDateFormat(dateFormat);
		Date formatDate = new Date();
		try {
			Date dateStr = formatter.parse(date);
			formatted = formatter.format(dateStr);
			formatDate= formatter.parse(formatted);
			formatter = new SimpleDateFormat(toFormat);
			formatted = formatter.format(formatDate);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return formatDate;
	}
	
	public long formatDateToMillis(String dateFormat, String date) {
		Date dateStr = null;
		DateFormat formatter = new SimpleDateFormat(dateFormat);
		try {
			formatter.setTimeZone(TimeZone.getTimeZone("GMT"));
			dateStr = formatter.parse(date);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return dateStr.getTime();
	}
	
	public String formatMillisToDate(long milliseconds){
		return formatMillisToDate(milliseconds, "yyyy-MM-dd hh:mm:ss");
	}
	
	public String formatMillisToDate(long milliseconds,String format){
		Date date = new Date(milliseconds);
		DateFormat formatter = new SimpleDateFormat(format);
		return formatter.format(date);
	}
	
	public String getDateTime() {
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		df.setTimeZone(TimeZone.getTimeZone("GMT"));
		return df.format(new Date());
	}

	public String getDate() {
		return getDate("yyyy-MM-dd");
	}
	
	public String getDate(String format){
		DateFormat df = new SimpleDateFormat(format);
//		df.setTimeZone(TimeZone.getTimeZone("GMT"));
		return df.format(new Date());
	}
	
	public String convertSecondToTime(int sec) {
		return String.format("%02d:%02d",
				TimeUnit.SECONDS.toHours(sec),
				TimeUnit.SECONDS.toMinutes(sec) - TimeUnit.HOURS.toMinutes(TimeUnit.SECONDS.toHours(sec)));
	}
	
	public int calculateTime(long start, long end) {
		long duration = end - start;
		float result = duration / SECOND_CONVERT;
		return Math.round(result);
	}
	
	public String getDiffDate(String Date,int datediff) {
		String dateInString = Date;
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Calendar cal = Calendar.getInstance();
		try {
			cal.setTime(sdf.parse(dateInString));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		cal.add(Calendar.DATE, datediff);
		Date resultdate = new Date(cal.getTimeInMillis());
		dateInString = sdf.format(resultdate);
		return dateInString;
	}
	
	public void showDatePicker(Context context,long currentTimeMilis,DatePickerDialog.OnDateSetListener datePickerListener) {
		Calendar cal = Calendar.getInstance();
		cal.setTimeInMillis(currentTimeMilis);
		int year = cal.get(Calendar.YEAR);
		int month = cal.get(Calendar.MONTH);
		int day = cal.get(Calendar.DATE);

		//DatePickerDialog datePicker = new DatePickerDialog(context, datePickerListener,year, month, day);
		DatePickerDialog datePicker = new DatePickerDialog(context,android.R.style.Theme_Holo_Light_Dialog, datePickerListener, year,month, day);

		datePicker.getDatePicker().setCalendarViewShown(false);
		datePicker.show();
	}

	public int getAge(String dateOfBirth) {

		Calendar today = Calendar.getInstance();
		Calendar birthDate = Calendar.getInstance();

		int age = 0;
		long dateOfBirthMilis = formatDateToMillis("yyyy-MM-dd", dateOfBirth);
		birthDate.setTimeInMillis(dateOfBirthMilis);
		if (birthDate.after(today)) {
			throw new IllegalArgumentException("Can't be born in the future");
		}

		age = today.get(Calendar.YEAR) - birthDate.get(Calendar.YEAR);

		// If birth date is greater than todays date (after 2 days adjustment of
		// leap year) then decrement age one year
		if ((birthDate.get(Calendar.DAY_OF_YEAR)
				- today.get(Calendar.DAY_OF_YEAR) > 3)
				|| (birthDate.get(Calendar.MONTH) > today.get(Calendar.MONTH))) {
			age--;

			// If birth date and todays date are of same month and birth day of
			// month is greater than todays day of month then decrement age
		} else if ((birthDate.get(Calendar.MONTH) == today.get(Calendar.MONTH))
				&& (birthDate.get(Calendar.DAY_OF_MONTH) > today
						.get(Calendar.DAY_OF_MONTH))) {
			age--;
		}

		return age;
	}
}
