/**
 * Base Fragment
 * @author Codelabs
 * 27 October 2014
 */
package com.prima.fragments;

import android.app.Activity;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.assist.ImageLoadingListener;
import com.nostra13.universalimageloader.core.assist.ImageSize;

import com.prima.activities.BaseActivity;
import com.prima.interfaces.FragmentInteface;

import java.io.File;


public abstract class BaseFragment extends Fragment implements FragmentInteface {
	private View view;
	protected Activity activity;
	protected ImageLoader imageLoader;
    protected SharedPreferences preferences;
    protected SharedPreferences.Editor editor;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		imageLoader = ImageLoader.getInstance();
        initSharedPreference();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
		view = inflater.inflate(getFragmentLayout(), container, false);
		initView(view);
		return view;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		getBaseActivity().setActionbarTitle(getPageTitle());
		getBaseActivity().setSlider(getSlider());
		setUICallbacks();
		updateUI();
	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		this.activity = activity;
	}

    private void initSharedPreference() {
        preferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
        editor = preferences.edit();
    }

	public BaseActivity getBaseActivity(){
		return ((BaseActivity)activity);
	}

	public void replaceFragment(int container,BaseFragment fragment,boolean addBackToStack){
		if(getBaseActivity().getFragmentManager().findFragmentByTag(fragment.getPageTitle())==null) {
			getBaseActivity().getFragmentHelper().replaceFragment(container, fragment, addBackToStack);
		}else
		{
			getBaseActivity().getFragmentHelper().replaceFragment(container, fragment, false);
		}
	}


	public void replaceFragment(int container,BaseFragment fragment,boolean addBackToStack, String fragmentTag){
		if(getBaseActivity().getFragmentManager().findFragmentByTag(fragmentTag+"tag")==null) {
			getBaseActivity().getFragmentHelper().replaceFragment(container, fragment, addBackToStack, fragmentTag);
		}else
		{
			getBaseActivity().getFragmentHelper().replaceFragment(container, fragment, false, fragmentTag);
		}
	}


	public void popFragment(String fragName){
		getBaseActivity().getFragmentHelper().getFragmentManager().popBackStack(fragName,0);
	}


	public void popFragment(){
		getBaseActivity().getFragmentHelper().getFragmentManager().popBackStackImmediate();
	}

	public void popAllFragment(){
		getBaseActivity().getFragmentHelper().getFragmentManager().popBackStack(0, FragmentManager.POP_BACK_STACK_INCLUSIVE);
	}



	public void replaceFragmentWithoutAnim(int container,BaseFragment fragment,boolean addBackToStack){
        getBaseActivity().getFragmentHelper().replaceFragmentWithoutAnim(container, fragment, addBackToStack);
    }

//    public void replaceFragment(int container,BaseFragment fragment,boolean addBackToStack, String fragmentTag){
//        getBaseActivity().getFragmentHelper().replaceFragment(container, fragment, addBackToStack, fragmentTag);
//    }

	public void addFragment(int container,BaseFragment fragment,boolean addBackToStack){
		getBaseActivity().getFragmentHelper().addFragment(container, fragment, addBackToStack);
	}

//	public void addChildFragment(int container,BaseFragment fragment,boolean addBackToStack){
//		FragmentTransaction ft = getChildFragmentManager().beginTransaction();
//		if (addBackToStack) {
//			ft.addToBackStack(fragment.getPageTitle());
//		}
//		ft.setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_right);
//		ft.add(container, fragment);
//		ft.commitAllowingStateLoss();
//	}

	public void loadImage(String url,final ImageView imageView,final int stubImage){
		loadImage(url, imageView, stubImage, null);
	}

	public void loadImage(String url,final ImageView imageView,final int stubImage,ImageSize imageSize){
		if (url != null && !url.equalsIgnoreCase("")) {
			ImageLoadingListener loadingListener = new ImageLoadingListener() {

				@Override
				public void onLoadingStarted(String imageUri, View view) {
					imageView.setImageResource(stubImage);
				}

				@Override
				public void onLoadingFailed(String imageUri, View view,FailReason failReason) {
					imageView.setImageResource(stubImage);
				}

				@Override
				public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
					imageView.setImageBitmap(loadedImage);
				}

				@Override
				public void onLoadingCancelled(String imageUri, View view) {}
			};
			if (imageSize != null) {
				imageLoader.loadImage(url, imageSize, null, loadingListener);
			}else{
				imageLoader.loadImage(url,loadingListener);
			}
		}
	}

	@Override
	public Boolean getSlider() {
		return true;
	}
	public String checkNullString(String string){
		return (string == null) ? "" : string;
	}

}
