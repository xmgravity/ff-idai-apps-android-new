/**
 * @author Codelabs
 * 8 September 2014
 */
package com.prima.interfaces;

public interface ActivityInterface {
	public void initView();
	public void setUICallbacks();
	public int getLayout();
	public void updateUI();
}
