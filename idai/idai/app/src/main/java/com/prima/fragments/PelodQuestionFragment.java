package com.prima.fragments;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import com.prima.Preferences;
import com.prima.activities.PelodQuestionActivity;
import com.prima.activities.PelodResultActivity;
import com.prima.adapters.ListAdapter;
import com.prima.adapters.PelodListAdapter;
import com.prima.entities.Question;
import prima.test.prima.R;

/**
 * Created by Codelabs on 01/09/2015.
 */
public class PelodQuestionFragment extends BaseFragment implements View.OnClickListener {

    private Context mContext;
    public static final String ARG_PAGE = "page 2";
    private ArrayList<String> listItem = new ArrayList<String>();
    private ListView list;
    private ViewPager mPager;
    Question.Kuesioner kuesioner;
    Question question;
    String selected = "";
    PelodListAdapter adapterList;
    int indeks;
    private ArrayList<String> ansewer = new ArrayList<String>();
    private List<Fragment> fragmentList = new ArrayList<Fragment>();
    int totalScore = 0;

    @SuppressLint("ValidFragment")
    public PelodQuestionFragment() {}
    @SuppressLint("ValidFragment")
    public PelodQuestionFragment(Question.Kuesioner kuesioner, int indeks) {
        this.kuesioner = kuesioner;
        this.indeks = indeks;
    }

    @Override
    public void initView(View view) {
        mContext = getActivity();
        list = (ListView) view.findViewById(R.id.listView);
        mPager = (ViewPager) getActivity().findViewById(R.id.pager);
        final Activity act = getActivity();

        if (act instanceof PelodQuestionActivity) {
            fragmentList = ((PelodQuestionActivity) act).getFragmentList();
            ansewer = ((PelodQuestionActivity) act).getAnsewer();
        }
/*
        adapterList = new PelodListAdapter(
                kuesioner.getAnswer_option(), mContext, selected);
        list.setAdapter(adapterList);
*/
        final ListAdapter adapterList = new ListAdapter(
                kuesioner.getAnswer_option(), mContext, selected);
        list.setAdapter(adapterList);

        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                ansewer.set(indeks, kuesioner.getAnswer_option().get(position).getScore());
                System.out.println("nandha answer: "+ kuesioner.getAnswer_option().get(position).getScore());

                selected = String.valueOf(kuesioner.getAnswer_option().get(position).getScore());
                adapterList.setSelected(selected);
                adapterList.notifyDataSetChanged();
                int current = mPager.getCurrentItem();

                if (current != fragmentList.size() - 1) {
                    mPager.setCurrentItem(current + 1, true);
                } else {
                    int score = 0;
                    for (int j = 0; j < ansewer.size(); j++) {
                        if (ansewer.get(j).replace(".00", "").equals("")) {
                            score = score + 0;
                        } else {
                            score = score + Integer.valueOf(ansewer.get(j).replace(".00", ""));
                        }
                    }
                    if (preferences.getString(Preferences.ID, "").equals("1")) {
                        editor.putString(Preferences.BIRTH_DAY, "");
                        editor.putString(Preferences.JENIS_KELAMIN, "");
                        editor.putString(Preferences.BERAT, "");
                        editor.putString(Preferences.TINGGI, "");
                        editor.putString(Preferences.ID, "");
                        editor.commit();
                    }

                    if (act instanceof PelodQuestionActivity) {
                        totalScore = ((PelodQuestionActivity) act).getMax_score();
                        question = ((PelodQuestionActivity) act).getQuestion();
                    }

                    Gson gson = new Gson();
                    Intent i = new Intent(mContext, PelodResultActivity.class);
                    i.putExtra("score", score);
                    i.putExtra("max_score", totalScore);
                    i.putStringArrayListExtra("list_score", ansewer);
                    i.putExtra("question",gson.toJson(question));
                    startActivity(i);
                    getActivity().finish();
                }

            }
        });
    }

    @Override
    public void setUICallbacks() {
    }

    @Override
    public void updateUI() {
    }

    @Override
    public String getPageTitle() {
        return "Account Summary";
    }


    @Override
    public int getFragmentLayout() {
        return R.layout.list_view;
    }

    @Override
    public void onClick(View v) {

    }


}