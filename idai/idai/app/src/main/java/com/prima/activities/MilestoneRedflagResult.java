package com.prima.activities;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import com.prima.Preferences;
import com.prima.controllers.ApiReferences;
import com.prima.controllers.Parser;
import com.prima.entities.AllRedflag;
import com.prima.entities.Message;
import com.prima.helpers.DatabaseHandler;

import prima.test.prima.R;

/**
 * Created by Codelabs on 21/08/2015.
 */
public class MilestoneRedflagResult extends BaseActivity implements View.OnClickListener {
    private ImageView close;
    private ImageView back;
    private Button lanjut, daftar_redflags;
    TextView title, hasil, keterangan;
    private RequestQueue mRequest;
    MaterialDialog dialog;
    DatabaseHandler db;
    AllRedflag redflag;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void initView() {
        db = new DatabaseHandler(context);
        daftar_redflags = (Button) findViewById(R.id.daftar_redflags);
        mRequest = Volley.newRequestQueue(getApplicationContext());
        back = (ImageView) findViewById(R.id.back);
        close = (ImageView) findViewById(R.id.close);
        lanjut = (Button) findViewById(R.id.ok);
        title = (TextView) findViewById(R.id.tv_actionbar_title);
        hasil = (TextView) findViewById(R.id.hasil);
        keterangan = (TextView) findViewById(R.id.keterangan);

        Typeface tf = Typeface.createFromAsset(getApplicationContext().getAssets(), "BREVIA-BLACK.OTF");
        title.setTypeface(tf);
        title.setText(getResources().getString(R.string.TahapandanTandaBahayaPerkembangan));
        Bundle bn = getIntent().getExtras();
        if (preferences.getString(Preferences.TIPE_USER, "").equalsIgnoreCase("awam")) {
            String status = bn.getString("status");
            if (status.equalsIgnoreCase("OK")) {
                keterangan.setVisibility(View.GONE);
                hasil.setText(getResources().getString(R.string.MilestoneRedflagResult3));
            } else if (status.equalsIgnoreCase("OK2")) {
                keterangan.setVisibility(View.GONE);
                hasil.setText(getResources().getString(R.string.MilestoneRedflagResult2));
            } else if (status.equalsIgnoreCase("NOTOK")) {
                keterangan.setText(getResources().getString(R.string.MilestoneRedflagResult2));
                hasil.setText(getResources().getString(R.string.MilestoneRedflagResult4));
            }
        } else if (bn != null){
            String status = bn.getString("status");
            if (status.equalsIgnoreCase("OK")) {
                keterangan.setVisibility(View.GONE);
                hasil.setText(getResources().getString(R.string.MilestoneRedflagResult));
            } else if (status.equalsIgnoreCase("OK2")) {
                keterangan.setVisibility(View.GONE);
                hasil.setText(getResources().getString(R.string.MilestoneRedflagResult2));
            } else if (status.equalsIgnoreCase("NOTOK")) {
                keterangan.setText(getResources().getString(R.string.MilestoneRedflagResult2));
                hasil.setText(getResources().getString(R.string.MilestoneRedflagResult4));
            }
        }

    }

    @Override
    public void setUICallbacks() {
        back.setOnClickListener(this);
        close.setOnClickListener(this);
        lanjut.setOnClickListener(this);
        daftar_redflags.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        if (v == back) {
            //   Intent i = new Intent(getApplicationContext(), MilestoneRedflagActivity.class);
            //    startActivity(i);
            finish();
        } else if (v == close) {
            //   Intent i = new Intent(getApplicationContext(), MilestoneRedflagActivity.class);
            //    startActivity(i);
            finish();
        } else if (v == lanjut) {
            //   Intent i = new Intent(getApplicationContext(), MilestoneRedflagActivity.class);
            //    startActivity(i);
            finish();
        } else if (v == daftar_redflags) {
            getQuestion();
        }
    }

    private void getQuestion() {
        progresDialog(false, getResources().getString(R.string.MemuatData));
        StringRequest listdata = new StringRequest(Request.Method.GET,
                ApiReferences.getAllQuestionnaireByAge("CMAR", preferences.getString(Preferences.LANGUAGE, "")),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        dialog.dismiss();
                        try {
                            if (response.equals("[]")) {
                                dialog(getResources().getString(R.string.BelumAdaData), getResources().getString(R.string.Pesan));
                            } else {
                                Message msg = Parser.getMessage(response);
                                if (msg.getStatus().equals("1")) {
                                    Log.i("RESULT JSON", response.toString());
                                    redflag = Parser.getAllRedFlags(response);

                                    /*
                                    for (int j = 0; j < redflag.getObjects().size(); j++) {
                                        for (int z = 0; z < redflag.getObjects().get(j).getData().size(); z++) {
                                            db.deleteQuestion(redflag.getObjects().get(j).getData().get(z).getId(), redflag.getObjects().get(j).getData().get(z).getMin_age(), redflag.getObjects().get(j).getData().get(z).getMax_age(), "");
                                            db.deleteAnswer(redflag.getObjects().get(j).getData().get(z).getId());
                                            db.insertCmar(redflag.getObjects().get(j).getData().get(z), "CMAR", redflag.getObjects().get(j).getId_age(), redflag.getObjects().get(j).getMin_age(), redflag.getObjects().get(j).getMax_age());
                                            for (int k = 0; k < redflag.getObjects().get(j).getData().get(z).getAnswer_option().size(); k++) {
                                                db.insertAnswer(redflag.getObjects().get(j).getData().get(z).getAnswer_option().get(k));
                                            }
                                        }
                                    }*/

                                    Intent i = new Intent(getApplicationContext(), AllRedflagActivity.class);
                                    i.putExtra("respon", response.toString());
                                    startActivity(i);
                                    //   finish();
                                } else {
                                    dialog(msg.getMessages(), getResources().getString(R.string.Pesan));
                                }
                            }
                        } catch (JsonSyntaxException e) {
                            dialog(getResources().getString(R.string.KesalahanServer), getResources().getString(R.string.Perhatian));
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dialog.dismiss();
                //  dialog(getResources().getString(R.string.NoConnection), getResources().getString(R.string.Perhatian));


                AllRedflag temp = db.getAllRedflag("CMAR");
                Gson gson = new Gson();
                Log.i("RESULT JSON2", gson.toJson(temp));
                if (temp.getObjects().size() > 0) {
                    Intent i = new Intent(getApplicationContext(), AllRedflagActivity.class);
                    i.putExtra("respon", gson.toJson(temp));
                    startActivity(i);
                } else {
                    dialog(getResources().getString(R.string.NoConnection), getResources().getString(R.string.Perhatian));
                }

            }
        });
        listdata.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        mRequest.add(listdata);
    }

    @Override
    public int getLayout() {
        return R.layout.activity_milestone_hasil;
    }

    @Override
    public void updateUI() {

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

    }

    public void dialog2(String content, String msg) {
        new MaterialDialog.Builder(this)
                .title(msg)
                .content(content)
                .negativeText("OK")
                .callback(new MaterialDialog.ButtonCallback() {
                    @Override
                    public void onPositive(MaterialDialog dialog) {

                    }

                    @Override
                    public void onNegative(MaterialDialog dialog) {
                        super.onNegative(dialog);
                    }

                })
                .show();
    }

    public void dialog(String content, String msg) {
        new MaterialDialog.Builder(this)
                .title(msg)
                .content(content)
                .positiveText(getResources().getString(R.string.MuatUlang))
                .negativeText(getResources().getString(R.string.Kembali))
                .callback(new MaterialDialog.ButtonCallback() {
                    @Override
                    public void onPositive(MaterialDialog dialog) {
                        getQuestion();
                    }

                    @Override
                    public void onNegative(MaterialDialog dialog) {
                        super.onNegative(dialog);
                    }

                })
                .show();
    }


    private void progresDialog(boolean horizontal, String msg) {
        dialog = new MaterialDialog.Builder(this)
                .title(msg)
                .content(getResources().getString(R.string.MohonTunggu))
                .progress(true, 0)
                .progressIndeterminateStyle(horizontal)
                .cancelable(false)
                .show();
    }

    @Override
    public void onBackPressed() {
        //  Intent i = new Intent(getApplicationContext(), MilestoneRedflagActivity.class);
        //  startActivity(i);
        finish();
    }


}
