package com.prima.activities;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.JsonSyntaxException;
import com.prima.MainActivity;
import com.prima.Preferences;
import com.prima.controllers.ApiReferences;
import com.prima.controllers.Parser;
import com.prima.entities.Message;
import com.prima.helpers.DateHelper;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import prima.test.prima.R;

/**
 * Created by Codelabs on 03/09/2015.
 */
public class EditImunisasiAnakActivity extends BaseActivity implements View.OnClickListener {

    DateHelper dateHelper;
    private ArrayAdapter<String> vaccineListAdapter;
    private RequestQueue mRequest;
    private TextView title, txtmerk, txttanggal, txtjenisvaksin;
    private ImageView back;
    private ImageView close;
    private Button buttonSimpanImunisasi;
    private Spinner spinnernamavaksin;
    JSONObject tempVaccine;
    MaterialDialog dialog;
    String selectedVaccineTxt,vaksinId, vaksinIdtxt;
    EditText merekvaksintxt, namavaksintxt;

    RelativeLayout selectedVaccine,
            hepa1,
            hepa2,
            hepa3,
            polio1,
            polio2,
            polio3,
            polio4,
            polio5,
            polio6,
            bcg1,
            dtp1,
            dtp2,
            dtp3,
            dtp4,
            dtp5,
            dtp6,
            dtp7,
            hib1,
            hib2,
            hib3,
            hib4,
            pcv1,
            pcv2,
            pcv3,
            pcv4,
            rotavirus1,
            rotavirus2,
            rotavirus3,
            influenza1,
            campak1,
            campak2,
            campak3,
            mmr1,
            mmr2,
            tifoid1,
            hepatitisa1,
            varisela1,
            hpv1;

    ArrayList<RelativeLayout> list = new ArrayList<RelativeLayout>();

    ArrayList<String> daftarNamaVaksin = new ArrayList<String>();

    LinearLayout listDaftarAnak, listDaftarAnakVaksin, linearNamaAnak, edit_hapus_vaksin;

    @Override
    public void initView() {

        dateHelper = new DateHelper();

        mRequest = Volley.newRequestQueue(getApplicationContext());
        listDaftarAnak = (LinearLayout) findViewById(R.id.list_daftar_anak);

        back = (ImageView) findViewById(R.id.back);
        close = (ImageView) findViewById(R.id.close);

        selectedVaccineTxt = "";

        buttonSimpanImunisasi = (Button) findViewById(R.id.buttonSimpanImunisasi);
        spinnernamavaksin = (Spinner) findViewById(R.id.spinnernamavaksin);
        txttanggal = (TextView) findViewById(R.id.txttanggal);
        merekvaksintxt = (EditText) findViewById(R.id.merekvaksintxt);
        namavaksintxt = (EditText) findViewById(R.id.namavaksintxt);

        Bundle bn = getIntent().getExtras();
        String kodeVaksin = bn.getString("VAKSIN_OBJECT");

        try {
            tempVaccine = new JSONObject(kodeVaksin);
            System.out.println("nandhajson "+tempVaccine.get("id"));
            merekvaksintxt.setText(tempVaccine.get("vaccine_brand").toString());
            txttanggal.setText(tempVaccine.get("vaccine_date").toString());

            String vaksinName = tempVaccine.get("vaccine_step_id").toString();

            if(vaksinName.equalsIgnoreCase("1") || vaksinName.equalsIgnoreCase("2") || vaksinName.equalsIgnoreCase("3")){
                namavaksintxt.setText("HEPATITIS B");
            }else if(vaksinName.equalsIgnoreCase("4") || vaksinName.equalsIgnoreCase("5") || vaksinName.equalsIgnoreCase("6") || vaksinName.equalsIgnoreCase("7") || vaksinName.equalsIgnoreCase("8") || vaksinName.equalsIgnoreCase("9")){
                namavaksintxt.setText("POLIO");
            }else if(vaksinName.equalsIgnoreCase("10")){
                namavaksintxt.setText("BCG");
            }else if(vaksinName.equalsIgnoreCase("11") || vaksinName.equalsIgnoreCase("12") || vaksinName.equalsIgnoreCase("13") || vaksinName.equalsIgnoreCase("14") || vaksinName.equalsIgnoreCase("15") || vaksinName.equalsIgnoreCase("16") || vaksinName.equalsIgnoreCase("17")){
                namavaksintxt.setText("DTP");
            }else if(vaksinName.equalsIgnoreCase("18") || vaksinName.equalsIgnoreCase("19") || vaksinName.equalsIgnoreCase("20") || vaksinName.equalsIgnoreCase("21") ){
                namavaksintxt.setText("HIB");
            }else if(vaksinName.equalsIgnoreCase("22") || vaksinName.equalsIgnoreCase("23") || vaksinName.equalsIgnoreCase("24") || vaksinName.equalsIgnoreCase("25") ){
                namavaksintxt.setText("PCV");
            }else if(vaksinName.equalsIgnoreCase("26") || vaksinName.equalsIgnoreCase("27") || vaksinName.equalsIgnoreCase("28") ){
                namavaksintxt.setText("ROTAVIRUS");
            }else if(vaksinName.equalsIgnoreCase("29") ){
                namavaksintxt.setText("INFLUENZA");
            }else if(vaksinName.equalsIgnoreCase("30") || vaksinName.equalsIgnoreCase("31") || vaksinName.equalsIgnoreCase("32") ){
                namavaksintxt.setText("CAMPAK");
            }else if(vaksinName.equalsIgnoreCase("33") || vaksinName.equalsIgnoreCase("34") ){
                namavaksintxt.setText("MMR");
            }else if(vaksinName.equalsIgnoreCase("35") ){
                namavaksintxt.setText("TIFOID");
            }else if(vaksinName.equalsIgnoreCase("36") ){
                namavaksintxt.setText("HEPATITIS A");
            }else if(vaksinName.equalsIgnoreCase("37") ){
                namavaksintxt.setText("VARISELA");
            }else if(vaksinName.equalsIgnoreCase("38") ){
                namavaksintxt.setText("HPV");
            }
/*
            tempVaccine.put("id", daftarVaksinAnak.getObjects().get(i).getId());
            tempVaccine.put("vaccine_step_id", daftarVaksinAnak.getObjects().get(i).getvaccine_step_id());
            tempVaccine.put("vaccine_brand", daftarVaksinAnak.getObjects().get(i).getvaccine_brand());
            tempVaccine.put("vaccine_date", daftarVaksinAnak.getObjects().get(i).getvaccine_date());
*/
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }


    @Override
    public void setUICallbacks() {
        back.setOnClickListener(this);
        close.setOnClickListener(this);

        txttanggal.setOnClickListener(this);
        buttonSimpanImunisasi.setOnClickListener(this);


        title = (TextView) findViewById(R.id.tv_actionbar_title);
        Typeface tf = Typeface.createFromAsset(getApplicationContext().getAssets(), "BREVIA-BLACK.OTF");
        title.setTypeface(tf);
        title.setText(getResources().getString(R.string.EditDataImunisasi));
    }

    @Override
    public void onClick(View v) {
        if (v == back) {
            Intent i = new Intent(EditImunisasiAnakActivity.this, TabelImunisasiActivity.class);
            startActivity(i);
            finish();
        } else if(v==buttonSimpanImunisasi){
            updateImunisasi();
        } else if (v == txttanggal) {
            showDatePickerForBirthDay(txttanggal);
        }

    }

    private void updateImunisasi() {

        String idVaksin = "";
        vaksinIdtxt = "";
        try{
            System.out.println(" vaksin id" + tempVaccine.get("id") +
                " vaccine_step_id " + tempVaccine.get("vaccine_step_id").toString() +
                " vaccine_date " + txttanggal.getText() +
                " vaccine_type " + "null" +
                " vaccine_brand " +merekvaksintxt.getText());

            idVaksin = tempVaccine.get("id").toString();
            vaksinIdtxt = tempVaccine.get("vaccine_step_id").toString();

        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }


        StringRequest postlogin = new StringRequest(Request.Method.POST,
                ApiReferences.updateVaccine(idVaksin), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    Log.i("RESULT JSON", response.toString());
                    Message message = Parser.getMessage(response
                            .toString());
                    if (message.getStatus().equalsIgnoreCase("1")) {

                        dialog(getResources().getString(R.string.DataBerhasilDiubah), getResources().getString(R.string.Perhatian));

                    }
                } catch (JsonSyntaxException e) {
                    dialog(getResources().getString(R.string.KesalahanServer), getResources().getString(R.string.Perhatian));
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                NetworkResponse response = error.networkResponse;
                if (error instanceof ServerError && response != null) {
                    try {
                        String res = new String(response.data,
                                HttpHeaderParser.parseCharset(response.headers, "utf-8"));
                        // Now you can use any deserializer to make sense of data
                        JSONObject obj = new JSONObject(res);
                        dialog(res.toString(), getResources().getString(R.string.Perhatian));
                    } catch (UnsupportedEncodingException e1) {
                        // Couldn't properly decode data to string
                        e1.printStackTrace();
                    } catch (JSONException e2) {
                        // returned data is not JSONObject?
                        e2.printStackTrace();
                    }
                }
            }
        }) {

            public Map<String, String> getHeaders() {
                Map<String, String>  params = new HashMap<String, String>();
                params.put("token", preferences.getString(Preferences.ACCESS_TOKEN, ""));

                return params;
            }

            protected Map<String, String> getParams() {
                HashMap<String, String> params = new HashMap<String, String>();
                params.put("vaccine_step_id", vaksinIdtxt);
                params.put("vaccine_date", txttanggal.getText().toString());
                params.put("vaccine_brand", merekvaksintxt.getText().toString());
                params.put("vaccine_type", "null");
                return params;
            }


        };
        postlogin.setRetryPolicy(new DefaultRetryPolicy(
                20000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        mRequest.add(postlogin);

    }

    private void showDatePickerForBirthDay(final TextView txt) {
        dateHelper.showDatePicker(EditImunisasiAnakActivity.this, System.currentTimeMillis(), new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                int _year = year;
                int _month = monthOfYear + 1;
                int _day = dayOfMonth;
                String date = _year + "-" + _month + "-" + _day;
                String birthDate = dateHelper.formatDate(date);
                txt.setText(birthDate);
            }
        });
    }

    @Override
    public int getLayout() {
        return R.layout.activity_imunisasi_edit;
    }

    @Override
    public void updateUI() {


        /*vaccineListAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, daftarNamaVaksin);
        daftarNamaVaksin.add(getResources().getString(R.string.pilihJenisVaksin));
        vaccineListAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnernamavaksin = (Spinner)findViewById(R.id.spinnernamavaksin);
        spinnernamavaksin.setAdapter(vaccineListAdapter);

        spinnernamavaksin.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener(){
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int pos, long id){

                String selectedItemText = parent.getItemAtPosition(pos).toString();

                }



            @Override
            public void onNothingSelected(AdapterView<?> parent){
                //Another interface callback
            }

        }); */
        }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

    }

    private void progresDialog(boolean horizontal, String msg) {
        dialog = new MaterialDialog.Builder(this)
                .title(msg)
                .content(getResources().getString(R.string.MohonTunggu))
                .progress(true, 0)
                .progressIndeterminateStyle(horizontal)
                .cancelable(false)
                .show();
    }

    public void dialog(String content, String msg) {
        new MaterialDialog.Builder(this)
                .title(msg)
                .content(content)
                .positiveText(getResources().getString(R.string.MuatUlang))
                .negativeText(getResources().getString(R.string.Kembali))
                .callback(new MaterialDialog.ButtonCallback() {
                    @Override
                    public void onPositive(MaterialDialog dialog) {
                        Intent i = new Intent(EditImunisasiAnakActivity.this, TabelImunisasiActivity.class);
                        startActivity(i);
                        finish();
                    }

                    @Override
                    public void onNegative(MaterialDialog dialog) {
                        // super.onNegative(dialog);
                        // finish();
                    }

                })
                .show();
    }

    @Override
    public void onBackPressed() {
        Intent i = new Intent(EditImunisasiAnakActivity.this, TabelImunisasiActivity.class);
        startActivity(i);
        finish();
    }

   /* private void getChildIndividualVaccine(String child_detail_name, final LinearLayout parent) {
        StringRequest listdata = new StringRequest(Request.Method.GET,
                ApiReferences.getChildIndividualVaccine(child_detail_name),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        dialog.dismiss();
                        try {
                            if (response.equals("[]")) {
                                dialog(getResources().getString(R.string.DataBelumTersedia), getResources().getString(R.string.Perhatian));
                            } else {

                                Message msg = Parser.getMessage(response);
                                if (msg.getStatus().equals("0")) {
                                    dialog(msg.getMessages(), getResources().getString(R.string.Perhatian));
                                } else {
                                    Log.i("RESULT JSON", response.toString());
                                    final VaksinPasien daftarVaksinAnak = Parser.getDaftarVaksinAnak(response);

                                    for (int i = 0; i < daftarVaksinAnak.getObjects().size(); i++) {
                                        View child = getLayoutInflater().inflate(R.layout.list_item_anak_vaksin_imunisasi, null);
                                        TextView item_anak_imunisasi_jenis_vaksin = (TextView) child.findViewById(R.id.item_anak_imunisasi_jenis_vaksin);
                                        TextView item_anak_imunisasi_tanggal = (TextView) child.findViewById(R.id.item_anak_imunisasi_tanggal);
                                        TextView item_anak_imunisasi_merek = (TextView) child.findViewById(R.id.item_anak_imunisasi_merek);


                                        String vaksinName = daftarVaksinAnak.getObjects().get(i).getvaccine_step_id();

                                        if(vaksinName.equalsIgnoreCase("1") || vaksinName.equalsIgnoreCase("2") || vaksinName.equalsIgnoreCase("3")){
                                            item_anak_imunisasi_jenis_vaksin.setText("HEPATITIS B");
                                        }else if(vaksinName.equalsIgnoreCase("4") || vaksinName.equalsIgnoreCase("5") || vaksinName.equalsIgnoreCase("6") || vaksinName.equalsIgnoreCase("7") || vaksinName.equalsIgnoreCase("8") || vaksinName.equalsIgnoreCase("9")){
                                            item_anak_imunisasi_jenis_vaksin.setText("POLIO");
                                        }else if(vaksinName.equalsIgnoreCase("10")){
                                            item_anak_imunisasi_jenis_vaksin.setText("BCG");
                                        }else if(vaksinName.equalsIgnoreCase("11") || vaksinName.equalsIgnoreCase("12") || vaksinName.equalsIgnoreCase("13") || vaksinName.equalsIgnoreCase("14") || vaksinName.equalsIgnoreCase("15") || vaksinName.equalsIgnoreCase("16") || vaksinName.equalsIgnoreCase("17")){
                                            item_anak_imunisasi_jenis_vaksin.setText("DTP");
                                        }else if(vaksinName.equalsIgnoreCase("18") || vaksinName.equalsIgnoreCase("19") || vaksinName.equalsIgnoreCase("20") || vaksinName.equalsIgnoreCase("21") ){
                                            item_anak_imunisasi_jenis_vaksin.setText("HIB");
                                        }else if(vaksinName.equalsIgnoreCase("22") || vaksinName.equalsIgnoreCase("23") || vaksinName.equalsIgnoreCase("24") || vaksinName.equalsIgnoreCase("25") ){
                                            item_anak_imunisasi_jenis_vaksin.setText("PCV");
                                        }else if(vaksinName.equalsIgnoreCase("26") || vaksinName.equalsIgnoreCase("27") || vaksinName.equalsIgnoreCase("28") ){
                                            item_anak_imunisasi_jenis_vaksin.setText("ROTAVIRUS");
                                        }else if(vaksinName.equalsIgnoreCase("29") ){
                                            item_anak_imunisasi_jenis_vaksin.setText("INFLUENZA");
                                        }else if(vaksinName.equalsIgnoreCase("30") || vaksinName.equalsIgnoreCase("31") || vaksinName.equalsIgnoreCase("32") ){
                                            item_anak_imunisasi_jenis_vaksin.setText("CAMPAK");
                                        }else if(vaksinName.equalsIgnoreCase("33") || vaksinName.equalsIgnoreCase("34") ){
                                            item_anak_imunisasi_jenis_vaksin.setText("MMR");
                                        }else if(vaksinName.equalsIgnoreCase("35") ){
                                            item_anak_imunisasi_jenis_vaksin.setText("TIFOID");
                                        }else if(vaksinName.equalsIgnoreCase("36") ){
                                            item_anak_imunisasi_jenis_vaksin.setText("HEPATITIS A");
                                        }else if(vaksinName.equalsIgnoreCase("37") ){
                                            item_anak_imunisasi_jenis_vaksin.setText("VARISELA");
                                        }else if(vaksinName.equalsIgnoreCase("38") ){
                                            item_anak_imunisasi_jenis_vaksin.setText("HPV");
                                        }

                                        list.add(hepa1);

                                        item_anak_imunisasi_tanggal.setText(daftarVaksinAnak.getObjects().get(i).getvaccine_date());
                                        item_anak_imunisasi_merek.setText(daftarVaksinAnak.getObjects().get(i).getvaccine_brand());

                                        parent.addView(child);

                                    }
                                }
                            }
                        } catch (JsonSyntaxException e) {
                            dialog(getResources().getString(R.string.KesalahanServer), getResources().getString(R.string.Perhatian));
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                NetworkResponse response = error.networkResponse;
                if (error instanceof ServerError && response != null) {
                    try {
                        String res = new String(response.data,
                                HttpHeaderParser.parseCharset(response.headers, "utf-8"));
                        // Now you can use any deserializer to make sense of data
                        JSONObject obj = new JSONObject(res);
                        dialog(res.toString(), getResources().getString(R.string.Perhatian));
                    } catch (UnsupportedEncodingException e1) {
                        // Couldn't properly decode data to string
                        e1.printStackTrace();
                    } catch (JSONException e2) {
                        // returned data is not JSONObject?
                        e2.printStackTrace();
                    }
                }
            }
        }) {

            public Map<String, String> getHeaders() {
                Map<String, String>  params = new HashMap<String, String>();
                params.put("token", preferences.getString(Preferences.ACCESS_TOKEN, ""));

                return params;
            }
        };

        listdata.setRetryPolicy(new DefaultRetryPolicy(
                20000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        mRequest.add(listdata);
    } */

}