package com.prima.activities;

import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.prima.MainActivity;

import prima.test.prima.R;

/**
 * Created by Codelabs on 21/09/2015.
 */
public class SaranActivity extends BaseActivity implements View.OnClickListener{
    private ImageView back;
    private ImageView close;
    private TextView title, titletext1, titletext2, subtitle, subtitle1, titletext, subtitletext;

    @Override
    public void initView() {

        close = (ImageView) findViewById(R.id.close);
        close.setVisibility(View.GONE);
        back = (ImageView) findViewById(R.id.back);
        subtitletext = (TextView) findViewById(R.id.subtitle);
        titletext = (TextView) findViewById(R.id.title);
        titletext1 = (TextView) findViewById(R.id.title1);
        titletext2 = (TextView) findViewById(R.id.title2);
        subtitletext = (TextView) findViewById(R.id.subtitle1);
        title = (TextView) findViewById(R.id.tv_actionbar_title);
        subtitle = (TextView) findViewById(R.id.subtitle);
        subtitle1 = (TextView) findViewById(R.id.subtitle1);
        Typeface tf = Typeface.createFromAsset(getApplicationContext().getAssets(), "BREVIA-BLACK.OTF");
        Typeface tf1 = Typeface.createFromAsset(getApplicationContext().getAssets(), "BREVIA-SEMIBOLD.OTF");

        title.setTypeface(tf);
        titletext.setTypeface(tf1);
        subtitletext.setTypeface(tf1);
        titletext.setTypeface(tf1);
        titletext1.setTypeface(tf1);
        titletext2.setTypeface(tf1);
        subtitletext.setTypeface(tf1);
        subtitle.setTypeface(tf1);
        subtitle1.setTypeface(tf1);

        //title.setText("Saran/Pertanyaan");
        title.setText(getResources().getString(R.string.titleSaran));

    }


    @Override
    public void setUICallbacks() {
        back.setOnClickListener(this);
        subtitle.setOnClickListener(this);
        subtitle1.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if (v == back) {
            Intent i = new Intent(this, MainActivity.class);
            startActivity(i);
            finish();

        }else if (v == subtitle) {
            Intent intent = new Intent(Intent.ACTION_SEND);
            intent.setType("text/html");
            intent.putExtra(Intent.EXTRA_EMAIL, "prima.idai@idai.or.id");
            intent.putExtra(Intent.EXTRA_SUBJECT, "PRIMA Apps");
            intent.putExtra(Intent.EXTRA_TEXT, "");

            startActivity(Intent.createChooser(intent, "Send Email"));
            finish();

        }else if (v == subtitle1) {
                Uri webpage = Uri.parse("http://www.idai.or.id/");
                Intent intent = new Intent(Intent.ACTION_VIEW, webpage);
                if (intent.resolveActivity(getPackageManager()) != null) {
                    startActivity(intent);
                }
            }

        }



    @Override
    public int getLayout() {
        return R.layout.activity_saran;
    }

    @Override
    public void updateUI() {

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

    }


    @Override
    public void onBackPressed() {
        Intent i = new Intent(this, MainActivity.class);
        startActivity(i);
        finish();
    }




}
