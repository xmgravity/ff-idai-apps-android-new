package com.prima.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.GlideDrawableImageViewTarget;
import com.prima.controllers.ApiReferences;
import com.prima.entities.Pubertas;

import java.util.ArrayList;

import prima.test.prima.R;

/**
 * Created by Codelabs on 17/09/2015.
 */
public class AdapterTahapPubertas extends BaseAdapter {
    ArrayList<Pubertas.Item> _data;
    Context _context;

    public String getSelected() {
        return selected;
    }

    public void setSelected(String selected) {
        this.selected = selected;
    }

    String selected;

    public AdapterTahapPubertas(ArrayList<Pubertas.Item> data,
                            Context context, String selected) {
        _data = data;
        _context = context;
        this.selected = selected;
    }

    @Override
    public int getCount() {

        return _data.size();
    }

    @Override
    public Object getItem(int position) {

        return _data.get(position);
    }

    @Override
    public long getItemId(int position) {

        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        if (v == null) {
            LayoutInflater infLayout = (LayoutInflater) _context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = infLayout.inflate(R.layout.list_item_materi_pubertas, null);
        }

        TextView phase = (TextView) v.findViewById(R.id.phase);
        ImageView img = (ImageView) v.findViewById(R.id.img);
        final ProgressBar progress = (ProgressBar) v.findViewById(R.id.progress);
        TextView description = (TextView) v.findViewById(R.id.description);
        LinearLayout linear = (LinearLayout) v.findViewById(R.id.gambar);
        phase.setText(_data.get(position).getPhase());
        if (_data.get(position).getDescription().equals("")||
                _data.get(position).getDescription().equalsIgnoreCase("null")){
            //description.setVisibility(View.GONE);
            //phase.setVisibility(View.GONE);
            description.setText(" ");
        }else{
            description.setText(_data.get(position).getDescription());
        }

        Glide.with(_context)
                .load(ApiReferences.getImageUrl() + _data.get(position).getImage())
                .fitCenter()
                .override(220, 220)
                .into(new GlideDrawableImageViewTarget(img) {
                    @Override
                    public void onResourceReady(GlideDrawable drawable, GlideAnimation anim) {
                        super.onResourceReady(drawable, anim);
                        progress.setVisibility(View.GONE);
                    }
                });



        if (selected.equals(String.valueOf(_data.get(position).getCode_image()))&&
                !selected.equals("")) {
            phase.setBackgroundResource(R.drawable.label_fragment_nocorner);
            linear.setBackgroundResource(R.drawable.label_fragment_nocorner);
        } else {
            phase.setBackgroundResource(R.drawable.label_fragment_nocorner);
            linear.setBackgroundResource(R.drawable.label_fragment_nocorner);
        }
        return v;
    }
}
