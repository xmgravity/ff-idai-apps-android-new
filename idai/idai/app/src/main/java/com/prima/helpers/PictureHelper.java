/**
 * @author Codelabs
 */

package com.prima.helpers;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.provider.MediaStore;
import android.provider.MediaStore.Images.Media;
import android.provider.MediaStore.MediaColumns;
import android.support.v4.app.Fragment;
import android.view.View;

import com.afollestad.materialdialogs.MaterialDialog;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.io.IOException;

import com.prima.callbacks.OnPictureSelect;
import prima.test.prima.R;


public class PictureHelper {
	public static final int TAKE_PICTURE = 13579;
	public static final int PICK_FROM_GALLERY = 2468;
	private DateHelper dateHelper;
	private Uri selectedImage;
	MaterialDialog dialog;
	private Context ctx;
	private Fragment fragment;
	private ImageLoader imageLoader;
	private static PictureHelper pictureHelper;
    private OnPictureSelect callback;
	
	public static PictureHelper getInstance(Context context){
		if (pictureHelper == null) {
			pictureHelper = new PictureHelper(context);
		}
		pictureHelper.ctx = context;
		return pictureHelper;
	}
	
	public static PictureHelper getInstance(Context context,Fragment fragment){
		pictureHelper = getInstance(context);
		pictureHelper.fragment = fragment;
		return pictureHelper;
	}
	
	public PictureHelper(Context context) {
		dateHelper = new DateHelper();
		this.ctx = context;
		imageLoader = ImageLoader.getInstance();
	}
	
	public PictureHelper(Context context, Fragment fragment) {
		this(context);
		this.fragment = fragment;
	}

    public PictureHelper(Context context, Fragment fragment, OnPictureSelect callback) {
        this(context);
        this.fragment = fragment;
        this.callback = callback;
    }

	public void showPickPhotoDialog() {

			dialog = new MaterialDialog.Builder(ctx)
					.title("Pick Photo")
					.negativeText("Cancel")
					.items(R.array.photo)
					.itemsCallback(new MaterialDialog.ListCallback() {
						@Override
						public void onSelection(MaterialDialog dialog, View view, int which, CharSequence text) {
							if (which==0){
								pickFromCamera();
							}else {
								pickFromGalery();
							}
						}
					})
					.show();
	}

	private void pickFromCamera() {
		int requestCode = TAKE_PICTURE;
		String fileName = "prima-" + dateHelper.getDateTime() + "-prima";
		Intent takePicture = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
		ContentValues values = new ContentValues();
		values.put(MediaColumns.TITLE, fileName);
		selectedImage = ctx.getContentResolver().insert(Media.EXTERNAL_CONTENT_URI, values);
		takePicture.putExtra(MediaStore.EXTRA_OUTPUT, selectedImage);
		fragment.startActivityForResult(takePicture, requestCode);
//        this.callback.successSelectPicture(requestCode,null);
		dialog.dismiss();
	}

	private void pickFromGalery() {
		int requestCode = PICK_FROM_GALLERY;
		Intent pickPhoto = new Intent(Intent.ACTION_PICK,Media.EXTERNAL_CONTENT_URI);
		fragment.startActivityForResult(pickPhoto, requestCode);
//        this.callback.successSelectPicture(requestCode,null);
		dialog.dismiss();
	}
	
	public String getPath(Context ctx,Uri uri) {
		String[] projection = { MediaColumns.DATA };
		@SuppressWarnings("deprecation")
		Cursor cursor = ((Activity) ctx).managedQuery(uri, projection, null, null, null);
		if (cursor != null) {
			int column_index = cursor.getColumnIndexOrThrow(MediaColumns.DATA);
			cursor.moveToFirst();
			return cursor.getString(column_index);
		} else
			return null;
	}

	public Bitmap rotate(Bitmap sourceBitmap, int degrees) {
		if (degrees != 0 && sourceBitmap != null) {
			Matrix matrix = new Matrix();
			matrix.setRotate(degrees, (float) sourceBitmap.getWidth() / 2,
					(float) sourceBitmap.getHeight() / 2);
			try {
				Bitmap rotateBitmap = Bitmap.createBitmap(sourceBitmap, 0, 0,
						sourceBitmap.getWidth(), sourceBitmap.getHeight(),
						matrix, true);
				sourceBitmap = rotateBitmap;
			} catch (OutOfMemoryError ex) {
				throw ex;
			}
		}
		return sourceBitmap;
	}

	public int getImageRotation(String imagePath) throws IOException {
		ExifInterface exif = new ExifInterface(imagePath);
		int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION,
				ExifInterface.ORIENTATION_NORMAL);
		int rotationAngle = 0;

		switch (orientation) {
		case ExifInterface.ORIENTATION_ROTATE_90:
			rotationAngle = 90;
			break;
		case ExifInterface.ORIENTATION_ROTATE_180:
			rotationAngle = 180;
			break;
		case ExifInterface.ORIENTATION_ROTATE_270:
			rotationAngle = 270;
			break;
		default:
			break;
		}
		return rotationAngle;
	}
	
	public Uri getSelectedImage() {
		return selectedImage;
	}
	
	public ImageLoader getImageLoader() {
		return imageLoader;
	}
}
