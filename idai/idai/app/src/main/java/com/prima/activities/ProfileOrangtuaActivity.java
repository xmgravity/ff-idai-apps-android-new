package com.prima.activities;

import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.net.Uri;
import android.provider.MediaStore;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.prima.Preferences;
import com.prima.helpers.HexagonMaskView;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

import prima.test.prima.R;

/**
 * Created by Codelabs on 19/08/2015.
 */
public class ProfileOrangtuaActivity extends BaseActivity implements View.OnClickListener {

    TextView title, username, sub, fullname_value, email_value, handphone_value, address_value, gender_value, dob_value;
    LinearLayout address, gender, dob, fullname, email, handphone;
    ImageView close, back, profile_image;
    MaterialDialog dialog;
    private Uri selectedImage;
    private String imagePath;
    private File file;
    HexagonMaskView hexa;

    @Override
    public void initView() {

        address_value = (TextView) findViewById(R.id.profil_orangtua_address_value);
        dob_value = (TextView) findViewById(R.id.profil_orangtua_dob_value);
        gender_value = (TextView) findViewById(R.id.profil_orangtua_gender_value);
        fullname_value = (TextView) findViewById(R.id.profil_orangtua_fullname_value);
        email_value = (TextView) findViewById(R.id.profil_orangtua_email_value);
        handphone_value = (TextView) findViewById(R.id.profil_orangtua_handphone_value);
        title = (TextView) findViewById(R.id.tv_actionbar_title);
        sub = (TextView) findViewById(R.id.sub);
        close = (ImageView) findViewById(R.id.close);
        back = (ImageView) findViewById(R.id.back);
        profile_image = (ImageView) findViewById(R.id.profile_image);
        close.setVisibility(View.GONE);
        //set font
        Typeface tf = Typeface.createFromAsset(getApplicationContext().getAssets(), "BREVIA-BLACK.OTF");
        title.setTypeface(tf);
        title.setText("PROFIL");
        hexa = new HexagonMaskView();
        Glide.with(ProfileOrangtuaActivity.this)
                .load(preferences.getString(Preferences.FOTO, ""))
                .asBitmap()
                .override(200, 200)
                .into(new SimpleTarget<Bitmap>() {
                    @Override
                    public void onResourceReady(Bitmap bitmap, GlideAnimation anim) {
                        Bitmap b = hexa.getHexagonShape(hexa.scaleCenterCrop(bitmap, 200, 200));
                        profile_image.setImageBitmap(b);
                    }
                });
        username = (TextView) findViewById(R.id.username);
        username.setText(preferences.getString(Preferences.NAME, ""));

        if (!preferences.getString(Preferences.FULLNAME, "").equals("")) {
            fullname_value.setText(preferences.getString(Preferences.FULLNAME, ""));
        }

        if (!preferences.getString(Preferences.EMAIL, "").equals("")) {
            email_value.setText(preferences.getString(Preferences.EMAIL, ""));
        }

        if (!preferences.getString(Preferences.HANDPHONE, "").equals("")) {
            handphone_value.setText(preferences.getString(Preferences.HANDPHONE, ""));
        }

        if (!preferences.getString(Preferences.ADDRESS, "").equals("")) {
            address_value.setText(preferences.getString(Preferences.ADDRESS, ""));
        }

        if (!preferences.getString(Preferences.DOB, "").equals("")) {
            dob_value.setText(preferences.getString(Preferences.DOB, ""));
        }

        if (!preferences.getString(Preferences.GENDER, "").equals("")) {
            gender_value.setText(preferences.getString(Preferences.GENDER, ""));
        }

        System.out.println("nandha"+preferences.getString(Preferences.ADDRESS, "") +","+ preferences.getString(Preferences.DOB, "") +","+ preferences.getString(Preferences.GENDER, ""));

    }

    @Override
    public void setUICallbacks() {
        profile_image.setOnClickListener(this);
        back.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        if (v == back) {
            Intent i = new Intent(getApplicationContext(), ProfileActivity.class);
            startActivity(i);
            finish();
        } else if (v == profile_image) {
//            showPickPhotoDialog();
        }
    }


    @Override
    public int getLayout() {
        return R.layout.activity_profileorangtua;
    }

    @Override
    public void updateUI() {
        if (!preferences.getString(Preferences.ADDRESS, "").equals("")) {
            address_value.setText(preferences.getString(Preferences.ADDRESS, ""));
        }

        if (!preferences.getString(Preferences.FULLNAME, "").equals("")) {
            fullname_value.setText(preferences.getString(Preferences.FULLNAME, ""));
        }

        if (!preferences.getString(Preferences.EMAIL, "").equals("")) {
            email_value.setText(preferences.getString(Preferences.EMAIL, ""));
        }

        if (!preferences.getString(Preferences.HANDPHONE, "").equals("")) {
            handphone_value.setText(preferences.getString(Preferences.HANDPHONE, ""));
        }

        if (!preferences.getString(Preferences.DOB, "").equals("")) {
            dob_value.setText(preferences.getString(Preferences.DOB, ""));
        }

        if (!preferences.getString(Preferences.GENDER, "").equals("")) {
            if(preferences.getString(Preferences.GENDER, "").equals("male")){
                gender_value.setText("Laki-laki");
            }else {
                gender_value.setText("Perempuan");
            }

        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1) {
            if (resultCode == RESULT_OK && data != null) {

                Uri selectedImage = data.getData();
                String[] filePathColumn = {MediaStore.Images.Media.DATA};

                Cursor cursor = getContentResolver().query(selectedImage,
                        filePathColumn, null, null, null);
                cursor.moveToFirst();

                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                String picturePath = cursor.getString(columnIndex);
                imagePath = picturePath;
                cursor.close();

                if (imagePath != null) {
                    File imgFile = new File(imagePath);
                    if (imgFile.exists()) {
                        Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
                        Bitmap b = hexa.getHexagonShape(hexa.scaleCenterCrop(myBitmap, 200, 200));
                        profile_image.setImageBitmap(b);
                    }

                }

            }
        } else if (requestCode == 0) {
            if (resultCode == RESULT_OK) {

                final String[] imageColumns = {MediaStore.Images.Media._ID,
                        MediaStore.Images.Media.DATA};
                final String imageOrderBy = MediaStore.Images.Media._ID
                        + " DESC";
                Cursor cursor = getContentResolver().query(
                        MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                        imageColumns, null, null, imageOrderBy);
                int column_index_data = cursor
                        .getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
                cursor.moveToFirst();

                imagePath = cursor.getString(column_index_data);
                cursor.close();

                if (imagePath != null) {
                    File imgFile = new File(imagePath);
                    if (imgFile.exists()) {
                        Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
                        Bitmap b = hexa.getHexagonShape(hexa.scaleCenterCrop(myBitmap, 200, 200));
                        profile_image.setImageBitmap(b);
                    }
                } else {
                    String text = "Terjadi kesalahan pada aplikasi kamera. Silakan ulang foto kembali";
                    Toast.makeText(context, text, Toast.LENGTH_LONG).show();
                }
            }
        }
    }


    @Override
    public void onBackPressed() {
        Intent i = new Intent(getApplicationContext(), ProfileActivity.class);
        startActivity(i);
        finish();
    }



    public void showPickPhotoDialog() {

        dialog = new MaterialDialog.Builder(this)
                .title("Pick Photo")
                .negativeText("Cancel")
                .items(R.array.photo)
                .itemsCallback(new MaterialDialog.ListCallback() {
                    @Override
                    public void onSelection(MaterialDialog dialog, View view, int which, CharSequence text) {
                        if (which == 0) {
                            pickFromCamera();
                        } else {
                            pickFromGalery();
                        }
                    }
                })
                .show();
    }


    private void pickFromCamera() {
        int requestCode = 0;
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss")
                .format(new Date());
        String imageFileName = "PHOTO" + timeStamp + "_";

        ContentValues values = new ContentValues();
        values.put(MediaStore.Images.Media.TITLE, imageFileName);
        selectedImage = getContentResolver().insert(
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);

        // create Intent to take a picture and return control to the calling
        // application

        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        intent.putExtra(MediaStore.EXTRA_OUTPUT, selectedImage);

        // start the image capture Intent
        startActivityForResult(intent, requestCode);
        dialog.dismiss();
    }

    private void pickFromGalery() {
        int requestCode = 1;
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss")
                .format(new Date());
        String imageFileName = "PHOTO" + timeStamp + "_";

        ContentValues values = new ContentValues();
        values.put(MediaStore.Images.Media.TITLE, imageFileName);
        selectedImage = getContentResolver().insert(
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);

        // create Intent to take a picture and return control to the calling
        // application

        Intent intent = new Intent(Intent.ACTION_PICK);
        intent.setType("image/*");
        intent.putExtra(MediaStore.EXTRA_OUTPUT, selectedImage);

        startActivityForResult(intent, requestCode);
        dialog.dismiss();
    }


}
