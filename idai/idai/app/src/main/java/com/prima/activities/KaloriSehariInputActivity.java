package com.prima.activities;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.prima.Preferences;
import com.prima.adapters.PagerAdapter;
import com.prima.fragments.GrafikKaloriSehariFragment;

import java.util.ArrayList;
import java.util.List;

import prima.test.prima.R;

/**
 * Created by Codelabs on 09/08/2015.
 */
public class KaloriSehariInputActivity extends BaseActivity implements View.OnClickListener {
    private Context mContext;
    private ImageView close;
    private ImageView back;
    private ViewPager mPager;
    private Button lanjut1, lanjut2;
    private PagerAdapter mPagerAdapter;
    PageListener pageListener;
    List<Fragment> fragmentList;
    private TextView title;
    ArrayList<String> data;
    ArrayList<String> titlegrafik;
    ArrayList<String> listInterpretasi;
    ArrayList<String> listLabel;
    static TextView jenis, keteranganLanjut2;
    static LinearLayout indikatorWrapper, linearLanjut2, linearGrafik;
    String result;
    private int currentPage;
    private String hasilKesimpulan;

    EditText usiaTinggi, bbIdeal;

    public ArrayList<String> getData() {
        return data;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = getApplicationContext();
    }

    @Override
    public void initView() {
        currentPage = 0;
        title = (TextView) findViewById(R.id.title);
        indikatorWrapper = (LinearLayout) findViewById(R.id.indikator_pager);
        linearLanjut2 = (LinearLayout) findViewById(R.id.linearLanjut2);
        linearGrafik = (LinearLayout) findViewById(R.id.linearGrafik);

        usiaTinggi = (EditText) findViewById(R.id.usiaTinggi);
        bbIdeal = (EditText) findViewById(R.id.bbIdeal);

        back = (ImageView) findViewById(R.id.back);
        close = (ImageView) findViewById(R.id.close);
        lanjut1 = (Button) findViewById(R.id.lanjut1);
        lanjut2 = (Button) findViewById(R.id.lanjut2);
        //set font
        Typeface tf0 = Typeface.createFromAsset(this.getAssets(), "BREVIA-BLACK.OTF");
        Typeface tf = Typeface.createFromAsset(this.getAssets(), "BREVIA-MEDIUM.OTF");
        title.setTypeface(tf0);
        lanjut1.setTypeface(tf);
        lanjut2.setTypeface(tf);
        mPager = (ViewPager) findViewById(R.id.pager);
        pageListener = new PageListener();
        mPager.setOnPageChangeListener(pageListener);
        jenis = (TextView) findViewById(R.id.jenis);
        keteranganLanjut2 = (TextView) findViewById(R.id.keteranganlanjut2);
        keteranganLanjut2.setVisibility(View.GONE);


        Typeface tfAriali = Typeface.createFromAsset(getApplicationContext().getAssets(), "ariali.ttf");
        Typeface tfArialb = Typeface.createFromAsset(getApplicationContext().getAssets(), "arialbd.ttf");

        TextView trademark = (TextView) findViewById(R.id.trademark);
        TextView trademark2 = (TextView) findViewById(R.id.trademark2);
        TextView trademark3 = (TextView) findViewById(R.id.trademark3);
        trademark.setTypeface(tfAriali);
        trademark2.setTypeface(tfArialb);
        trademark3.setTypeface(tfAriali);

        if(preferences.getString(Preferences.TIPE_USER, "").equalsIgnoreCase("awam")){
            trademark.setVisibility(View.GONE);
            trademark2.setVisibility(View.GONE);
            trademark3.setVisibility(View.GONE);
        }
    }

    @Override
    public void setUICallbacks() {
        back.setOnClickListener(this);
        close.setOnClickListener(this);
        lanjut1.setOnClickListener(this);
        lanjut2.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        if (v == back) {
            if (currentPage > 0) {
                currentPage--;
                mPager.setCurrentItem(currentPage, true);
            } else {
                fragmentList.clear();
                mPagerAdapter.notifyDataSetChanged();
                finish();
            }
        } else if (v == close) {
            fragmentList.clear();
            mPagerAdapter.notifyDataSetChanged();
            finish();
        } else if (v == lanjut1) {


            /*
            keteranganLanjut2.setVisibility(View.VISIBLE);
            keteranganLanjut2.setText("Usia tinggi (height age) didapatkan dengan menarik TB ke samping hingga memotong P50, lalu dari titik potong tersebut tarik garis ke bawah hingga memotong garis usia.\n" +
                    "\n" +
                    "Berat badan (BB) didapatkan dengan menarik TB ke samping hingga memotong P50, lalu dari titik potong tersebut tarik garis ke bawah hingga memotong P50 pada kurva di bawahnya. Lalu, tarik garis ke samping hingga memotong garis BB.\n" +
                    "\n" +
                    "Masukkan nilai height age dan BB ideal untuk mendapatkan nilai kebutuhan kalori sehari.");


            lanjut1.setVisibility(View.GONE);
            lanjut2.setVisibility(View.VISIBLE);

            jenis.setVisibility(View.GONE);
            linearGrafik.setVisibility(View.GONE);

            mPager.setVisibility(View.GONE);
            linearLanjut2.setVisibility(View.VISIBLE);

            */

            dialog(getResources().getString(R.string.PanduanKalori) +
                    "\n" +
                    getResources().getString(R.string.PanduanKalori2) +
                    "\n" +
                    getResources().getString(R.string.PanduanKalori3), getResources().getString(R.string.Panduan));


        } else if (v == lanjut2) {

            String usiaTinggiValue = usiaTinggi.getText().toString().trim();
            String bbIdealValue = bbIdeal.getText().toString().trim();

            float kalori = 0;


            if(!usiaTinggiValue.equalsIgnoreCase("") && !bbIdealValue.equalsIgnoreCase("")){

                if(Float.parseFloat(usiaTinggiValue)<=0.5){
                    kalori = 108 * Float.parseFloat(bbIdealValue);
                }else if(Float.parseFloat(usiaTinggiValue)>0.5 && Float.parseFloat(usiaTinggiValue)<1){
                    kalori = 98 * Float.parseFloat(bbIdealValue);
                }else if(Float.parseFloat(usiaTinggiValue)>=1 && Float.parseFloat(usiaTinggiValue)<4){
                    kalori = 102 * Float.parseFloat(bbIdealValue);
                }else if(Float.parseFloat(usiaTinggiValue)>=4 && Float.parseFloat(usiaTinggiValue)<7){
                    kalori = 90 * Float.parseFloat(bbIdealValue);
                }else if(Float.parseFloat(usiaTinggiValue)>=7 && Float.parseFloat(usiaTinggiValue)<11){
                    kalori = 70 * Float.parseFloat(bbIdealValue);
                }else if(Float.parseFloat(usiaTinggiValue)>=11 && Float.parseFloat(usiaTinggiValue)<15 && preferences.getString(Preferences.JENIS_KELAMIN, "").equalsIgnoreCase("m")){
                    kalori = 55 * Float.parseFloat(bbIdealValue);
                }else if(Float.parseFloat(usiaTinggiValue)>=15 && Float.parseFloat(usiaTinggiValue)<=18 && preferences.getString(Preferences.JENIS_KELAMIN, "").equalsIgnoreCase("m")){
                    kalori = 45 * Float.parseFloat(bbIdealValue);
                }else if(Float.parseFloat(usiaTinggiValue)>=11 && Float.parseFloat(usiaTinggiValue)<15 && preferences.getString(Preferences.JENIS_KELAMIN, "").equalsIgnoreCase("f")){
                    kalori = 47 * Float.parseFloat(bbIdealValue);
                }else if(Float.parseFloat(usiaTinggiValue)>=15 && Float.parseFloat(usiaTinggiValue)<=18 && preferences.getString(Preferences.JENIS_KELAMIN, "").equalsIgnoreCase("f")){
                    kalori = 40 * Float.parseFloat(bbIdealValue);
                }


                Intent i = new Intent(context, KaloriSehariResultActivity.class);
                i.putExtra("result", kalori);
                startActivity(i);
            }else{
                dialog(getResources().getString(R.string.IsiSeluruhIsian), getResources().getString(R.string.Perhatian));
            }



        }
    }


    @Override
    public int getLayout() {
        return R.layout.activity_kalori_sehari_input;
    }

    @Override
    public void updateUI() {
        currentPage = 0;
        indikatorWrapper.removeAllViewsInLayout();
        fragmentList = new ArrayList<Fragment>();
        data = new ArrayList<String>();
        titlegrafik = new ArrayList<String>();
        listInterpretasi = new ArrayList<String>();
        listLabel = new ArrayList<String>();
        Bundle bn = getIntent().getExtras();
        if (bn != null) {
            data = bn.getStringArrayList("page");
            titlegrafik = bn.getStringArrayList("title");
            listInterpretasi = bn.getStringArrayList("interpretasi");
            listLabel = bn.getStringArrayList("label");
            result = bn.getString("result");
            //  Toast.makeText(getApplicationContext(), String.valueOf(data.size()), Toast.LENGTH_LONG).show();
            for (int i = 0; i < data.size(); i++) {
                fragmentList.add(new GrafikKaloriSehariFragment(i));
                View child = getLayoutInflater().inflate(R.layout.indikator_pager, null);
                View child2 = getLayoutInflater().inflate(R.layout.indikator_pager_blue, null);
                if (i == 0) {
                    indikatorWrapper.addView(child2);
                } else {
                    indikatorWrapper.addView(child);
                }
            }
            jenis.setText(getResources().getString(R.string.PanjangTinggiUmurBerat));


            setupFrae();



            //System.out.println(listInterpretasi.size());
            hasilKesimpulan = "0";
            for (int i = 0; i < listInterpretasi.size(); i++) {
                System.out.println("interpretasi " + i + " :" +titlegrafik.get(i) +" " +listInterpretasi.get(i)+"");
                if (listInterpretasi.get(i).equalsIgnoreCase("Gizi Cukup") || listInterpretasi.get(i).equalsIgnoreCase("Normal")){
                    System.out.println(listInterpretasi.get(i) +" : "+hasilKesimpulan);
                }else{
                    hasilKesimpulan = "1";
                    System.out.println(listInterpretasi.get(i) +" : "+hasilKesimpulan);
                }
                System.out.println("Total Kesimpulan Interpretasi: "+hasilKesimpulan);
            }


        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

    }

    private class PageListener extends ViewPager.SimpleOnPageChangeListener {
        public void onPageSelected(int position) {
            currentPage = position;
            jenis.setText(getResources().getString(R.string.PanjangTinggiUmurBerat));
            title.setText(getResources().getString(R.string.kaloriAnakSehat));
            indikatorWrapper.removeAllViewsInLayout();
            for (int i = 0; i < data.size(); i++) {
                View child = getLayoutInflater().inflate(R.layout.indikator_pager, null);
                View child2 = getLayoutInflater().inflate(R.layout.indikator_pager_blue, null);
                if (i == position) {
                    indikatorWrapper.addView(child2);
                } else {
                    indikatorWrapper.addView(child);
                }
            }


        }
    }



    public void dialog(String content, String msg) {
        new MaterialDialog.Builder(this)
                .title(msg)
                .content(content)
                .positiveText("Ok")
                .callback(new MaterialDialog.ButtonCallback() {
                    @Override
                    public void onPositive(MaterialDialog dialog) {
                    }

                })
                .show();
    }

    @Override
    public void onBackPressed() {
        //    Intent i = new Intent(getApplicationContext(), ArtikelPagerGrafikActivity.class);
        //    i.putExtra("result", result);
        //    startActivity(i);
        fragmentList.clear();
        mPagerAdapter.notifyDataSetChanged();
        finish();
    }

    private void setupFrae() {
        mPagerAdapter = new PagerAdapter(getSupportFragmentManager(),
                KaloriSehariInputActivity.this, fragmentList);
        mPager.setAdapter(mPagerAdapter);

    }


}
