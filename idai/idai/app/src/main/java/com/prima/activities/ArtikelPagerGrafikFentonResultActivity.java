package com.prima.activities;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.prima.MainActivity;
import com.prima.Preferences;
import com.prima.adapters.PagerAdapter;
import com.prima.fragments.GrafikFentonFragment;
import com.prima.fragments.GrafikPertumbuhanFragment;

import java.util.ArrayList;
import java.util.List;

import prima.test.prima.R;

/**
 * Created by Codelabs on 09/08/2015.
 */
public class ArtikelPagerGrafikFentonResultActivity extends BaseActivity implements View.OnClickListener {
    private Context mContext;
    private ImageView close;
    private ImageView back;
    private ViewPager mPager;
    private Button buttonKesimpulan, buttonContohKasus, lanjut;
    private PagerAdapter mPagerAdapter;
    PageListener pageListener;
    List<Fragment> fragmentList;
    private TextView title;
    ArrayList<String> data;
    ArrayList<String> titlegrafik;
    ArrayList<String> listInterpretasi;
    ArrayList<String> listLabel;
    static TextView jenis;
    static LinearLayout indikatorWrapper;
    String result;
    private TextView interpretasi, keterangan, interpretasiText;
    private int currentPage;
    private String hasilKesimpulan;

    public ArrayList<String> getData() {
        return data;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = getApplicationContext();
    }

    @Override
    public void initView() {
        currentPage = 0;
        title = (TextView) findViewById(R.id.title);
        indikatorWrapper = (LinearLayout) findViewById(R.id.indikator_pager);
        interpretasi = (TextView) findViewById(R.id.interpretasi);
        keterangan = (TextView) findViewById(R.id.keterangan);
        interpretasiText = (TextView) findViewById(R.id.interpretasiText);
        back = (ImageView) findViewById(R.id.back);
        close = (ImageView) findViewById(R.id.close);
        buttonKesimpulan = (Button) findViewById(R.id.buttonKesimpulan);
        lanjut = (Button) findViewById(R.id.lanjut);
        buttonContohKasus = (Button) findViewById(R.id.buttonContohKasus);
        //set font
        Typeface tf0 = Typeface.createFromAsset(this.getAssets(), "BREVIA-BLACK.OTF");
        Typeface tf = Typeface.createFromAsset(this.getAssets(), "BREVIA-MEDIUM.OTF");
        title.setTypeface(tf0);
        buttonKesimpulan.setTypeface(tf);
        lanjut.setTypeface(tf);
        buttonContohKasus.setTypeface(tf);
        mPager = (ViewPager) findViewById(R.id.pager);
        pageListener = new PageListener();
        mPager.setOnPageChangeListener(pageListener);
        jenis = (TextView) findViewById(R.id.jenis);


        Typeface tfAriali = Typeface.createFromAsset(getApplicationContext().getAssets(), "ariali.ttf");
        Typeface tfArialb = Typeface.createFromAsset(getApplicationContext().getAssets(), "arialbd.ttf");

        TextView trademark = (TextView) findViewById(R.id.trademark);
        TextView trademark2 = (TextView) findViewById(R.id.trademark2);
        TextView trademark3 = (TextView) findViewById(R.id.trademark3);
        trademark.setTypeface(tfAriali);
        trademark2.setTypeface(tfArialb);
        trademark3.setTypeface(tfAriali);

        if(preferences.getString(Preferences.TIPE_USER, "").equalsIgnoreCase("awam")){
            trademark.setVisibility(View.GONE);
            trademark2.setVisibility(View.GONE);
            trademark3.setVisibility(View.GONE);
        }
    }

    @Override
    public void setUICallbacks() {
        back.setOnClickListener(this);
        close.setOnClickListener(this);
        keterangan.setOnClickListener(this);
        buttonKesimpulan.setOnClickListener(this);
        buttonContohKasus.setOnClickListener(this);
        lanjut.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        if (v == back) {
            if (currentPage > 0) {
                currentPage--;
                mPager.setCurrentItem(currentPage, true);
            } else {
                fragmentList.clear();
                mPagerAdapter.notifyDataSetChanged();
                finish();
            }
        } else if (v == close) {
            fragmentList.clear();
            mPagerAdapter.notifyDataSetChanged();
            finish();
        } else if (v == keterangan) {
            Intent i = new Intent(context, ImagePreviewActivity.class);
            i.putExtra("image", listLabel.get(0));
            startActivity(i);

        } else if (v == buttonKesimpulan) {
            Intent i = new Intent(context, GrafikPertumbuhanKesimpulanActivity.class);
            i.putExtra("hasilKesimpulan", hasilKesimpulan);
            i.putExtra("listInterpretasi", listInterpretasi);
            i.putExtra("titleGrafik", titlegrafik);
            startActivity(i);

        } else if (v == buttonContohKasus) {
            Intent i = new Intent(context, GrafikPertumbuhanContohKasusActivity.class);
            i.putExtra("titleKasus", titlegrafik.get(currentPage));
            startActivity(i);

        } else if (v == lanjut) {
            Intent i = new Intent(context, MainActivity.class);
            startActivity(i);

        }
    }


    @Override
    public int getLayout() {
        return R.layout.activity_fenton_result;
    }

    @Override
    public void updateUI() {
        currentPage = 0;
        indikatorWrapper.removeAllViewsInLayout();
        fragmentList = new ArrayList<Fragment>();
        data = new ArrayList<String>();
        titlegrafik = new ArrayList<String>();
        listInterpretasi = new ArrayList<String>();
        listLabel = new ArrayList<String>();
        Bundle bn = getIntent().getExtras();
        if (bn != null) {
            data = bn.getStringArrayList("page");
            titlegrafik = bn.getStringArrayList("title");
            listInterpretasi = bn.getStringArrayList("interpretasi");
            listLabel = bn.getStringArrayList("label");
            result = bn.getString("result");
            //  Toast.makeText(getApplicationContext(), String.valueOf(data.size()), Toast.LENGTH_LONG).show();
            for (int i = 0; i < data.size(); i++) {
                fragmentList.add(new GrafikFentonFragment(i));
                View child = getLayoutInflater().inflate(R.layout.indikator_pager, null);
                View child2 = getLayoutInflater().inflate(R.layout.indikator_pager_blue, null);
                if (i == 0) {
                    indikatorWrapper.addView(child2);
                } else {
                    indikatorWrapper.addView(child);
                }
            }

            if (listInterpretasi.get(0).equals("")) {
                interpretasi.setText("-");
            } else {
                interpretasi.setText(listInterpretasi.get(0));
            }

            setupFrae();


        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

    }

    private class PageListener extends ViewPager.SimpleOnPageChangeListener {
        public void onPageSelected(int position) {
            currentPage = position;
            jenis.setText(titlegrafik.get(position));
            interpretasi.setText(listInterpretasi.get(position));
            indikatorWrapper.removeAllViewsInLayout();
            for (int i = 0; i < data.size(); i++) {
                View child = getLayoutInflater().inflate(R.layout.indikator_pager, null);
                View child2 = getLayoutInflater().inflate(R.layout.indikator_pager_blue, null);
                if (i == position) {
                    indikatorWrapper.addView(child2);
                } else {
                    indikatorWrapper.addView(child);
                }
            }
        }
    }


    @Override
    public void onBackPressed() {
        //    Intent i = new Intent(getApplicationContext(), ArtikelPagerGrafikActivity.class);
        //    i.putExtra("result", result);
        //    startActivity(i);
        fragmentList.clear();
        mPagerAdapter.notifyDataSetChanged();
        finish();
    }

    private void setupFrae() {
        mPagerAdapter = new PagerAdapter(getSupportFragmentManager(),
                ArtikelPagerGrafikFentonResultActivity.this, fragmentList);
        mPager.setAdapter(mPagerAdapter);

    }
}
