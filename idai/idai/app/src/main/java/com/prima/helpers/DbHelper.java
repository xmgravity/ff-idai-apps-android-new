/**
 * Database configuration and helper
 * @author Codelabs
 * 27 October 2014
 */
package com.prima.helpers;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

import java.sql.SQLException;

import com.prima.Config;
import com.prima.entities.Pasien;


public class DbHelper extends OrmLiteSqliteOpenHelper {

	private static final int DATABASE_VERSION = 1;
	
	public DbHelper(Context context) {
		super(context, Config.DATABASE_NAME, null, DATABASE_VERSION);
//		createTable();
	}

	@Override
	public void onCreate(SQLiteDatabase db, ConnectionSource connectionSource) {
		createTable();
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, ConnectionSource connectionSource,
			int oldVersion, int newVersion) {
		resetDatabase();
		createTable();
	}
	
	private void createTable(){
		try {
			TableUtils.createTableIfNotExists(connectionSource, Pasien.class);
			Log.e("SQLite","Database Created");
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public void resetDatabase() {
		try {
			TableUtils.dropTable(connectionSource, Pasien.class, true);
			Log.e("SQLite", "Database Reset");
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	@Override
	public <D extends Dao<T, ?>, T> D getDao(Class<T> clazz)
			throws SQLException {
		return super.getDao(clazz);
	}
	
	@Override
	public void close() {
		super.close();

	}

	
}
