package com.prima.activities;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.media.RingtoneManager;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import org.joda.time.DateTime;
import org.joda.time.Minutes;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import com.prima.entities.Agenda;
import com.prima.entities.Notif;
import com.prima.helpers.DatabaseHandler;
import prima.test.prima.R;

/**
 * Created by Codelabs on 29/08/2015.
 */
public class SplashActivity extends BaseActivity implements View.OnClickListener {

    TextView title, subtitle;
    private DatabaseHandler db;
    Agenda agenda;


    @Override
    public void initView() {
        db = new DatabaseHandler(getApplicationContext());
        title = (TextView) findViewById(R.id.title);
        subtitle = (TextView) findViewById(R.id.subtitle);

        Typeface tf = Typeface.createFromAsset(getApplicationContext().getAssets(), "BREVIA-BOLD.OTF");
        Typeface tf2 = Typeface.createFromAsset(getApplicationContext().getAssets(), "BREVIA-SEMIBOLD.OTF");
        Typeface tf3 = Typeface.createFromAsset(getApplicationContext().getAssets(), "BREVIA-BLACK.OTF");
        Typeface tf4 = Typeface.createFromAsset(this.getAssets(), "BREVIA-MEDIUM.OTF");
        Typeface tf5 = Typeface.createFromAsset(this.getAssets(), "Coheadline.TTF");

        title.setTypeface(tf4);
        subtitle.setTypeface(tf5);



        Thread background = new Thread() {
            public void run() {
                try {
                    // Thread will sleep for 5 seconds
                    sleep(2000);
                    Intent i = new Intent(getBaseContext(), SplashSelamatDatangActivity.class);
                    startActivity(i);
                    finish();
                } catch (Exception e) {
                    Log.v("activity", "splash catch");
                }
            }
        };

        // start thread
        background.start();

        final long timeInterval = 1000 * 60;
        Runnable runnable = new Runnable() {

            public void run() {
                while (true) {
                    agenda = db.getAllAgenda();
                    Log.e("Log", "Running");
                    Calendar c = Calendar.getInstance();
                    System.out.println("Current time => " + c.getTime());

                    SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
                    SimpleDateFormat df2 = new SimpleDateFormat("HH:mm");
                    String formattedDate = df.format(c.getTime());
                    String formattedDate2 = df2.format(c.getTime());
                    Log.e("Log", "" + formattedDate + " " + formattedDate2);

                    for (int i = 0; i < agenda.getObjects().size(); i++) {
                        String waktu1 = formattedDate + " " + formattedDate2;
                        String waktu2 = agenda.getObjects().get(i).getTanggal_kunjungan() + " " + agenda.getObjects().get(i).getJam_mulai().substring(0, 5);

                        if (getMinutes(waktu1, waktu2) == Integer.valueOf(agenda.getObjects().get(i).getPengingat())) {
                            NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                            Intent i1 = new Intent(getApplicationContext(), ListAgendaActivity.class);
                            PendingIntent contentIntent1 = PendingIntent.getActivity(getApplicationContext(), 1, i1, PendingIntent.FLAG_UPDATE_CURRENT);


                            Notif temp = new Notif();
                            temp.setId_agenda(agenda.getObjects().get(i).getId_agenda());
                            temp.setIs_read("0");
                            db.insertNotif(temp);

                            if (agenda.getObjects().get(i).getGetar().equals("0")
                                    && agenda.getObjects().get(i).getSuara().equals("0")) {
                                NotificationCompat.Builder mBuilder1 =
                                        new NotificationCompat.Builder(getApplicationContext())
                                                .setSmallIcon(R.mipmap.ic_launcher)
                                                .setAutoCancel(true)
                                                .setLights(Color.RED, 3000, 3000)
                                                .setContentTitle("Event Kunjungan")
                                                .setContentText("Konsultasi " + agenda.getObjects().get(i).getNama_pasien() + "\nTanggal : " + agenda.getObjects().get(i).getTanggal_kunjungan() + "\nPukul :" + agenda.getObjects().get(i).getJam_mulai() + "-" + agenda.getObjects().get(i).getJam_akhir());
                                mBuilder1.setContentIntent(contentIntent1);
                                mNotificationManager.notify(1, mBuilder1.build());
                            } else if (agenda.getObjects().get(i).getGetar().equals("1")
                                    && agenda.getObjects().get(i).getSuara().equals("0")) {
                                NotificationCompat.Builder mBuilder1 =
                                        new NotificationCompat.Builder(getApplicationContext())
                                                .setSmallIcon(R.mipmap.ic_launcher)
                                                .setAutoCancel(true)
                                                .setVibrate(new long[]{1000, 1000, 1000, 1000, 1000})
                                                .setLights(Color.RED, 3000, 3000)
                                                .setContentTitle("Event Kunjungan")
                                                .setContentText("Konsultasi " + agenda.getObjects().get(i).getNama_pasien() + "\nTanggal : " + agenda.getObjects().get(i).getTanggal_kunjungan() + "\nPukul :" + agenda.getObjects().get(i).getJam_mulai() + "-" + agenda.getObjects().get(i).getJam_akhir());
                                mBuilder1.setContentIntent(contentIntent1);
                                mNotificationManager.notify(1, mBuilder1.build());
                            } else if (agenda.getObjects().get(i).getGetar().equals("0")
                                    && agenda.getObjects().get(i).getSuara().equals("1")) {
                                NotificationCompat.Builder mBuilder1 =
                                        new NotificationCompat.Builder(getApplicationContext())
                                                .setSmallIcon(R.mipmap.ic_launcher)
                                                .setAutoCancel(true)
                                                .setLights(Color.RED, 3000, 3000)
                                                .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                                                .setContentTitle("Event Kunjungan")
                                                .setContentText("Konsultasi " + agenda.getObjects().get(i).getNama_pasien() + "\nTanggal : " + agenda.getObjects().get(i).getTanggal_kunjungan() + "\nPukul :" + agenda.getObjects().get(i).getJam_mulai() + "-" + agenda.getObjects().get(i).getJam_akhir());
                                mBuilder1.setContentIntent(contentIntent1);
                                mNotificationManager.notify(1, mBuilder1.build());
                            } else if (agenda.getObjects().get(i).getGetar().equals("1")
                                    && agenda.getObjects().get(i).getSuara().equals("1")) {
                                NotificationCompat.Builder mBuilder1 =
                                        new NotificationCompat.Builder(getApplicationContext())
                                                .setSmallIcon(R.mipmap.ic_launcher)
                                                .setAutoCancel(true)
                                                .setVibrate(new long[]{1000, 1000, 1000, 1000, 1000})
                                                .setLights(Color.RED, 3000, 3000)
                                                .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                                                .setContentTitle("Event Kunjungan")
                                                .setContentText("Konsultasi " + agenda.getObjects().get(i).getNama_pasien() + "\nTanggal : " + agenda.getObjects().get(i).getTanggal_kunjungan() + "\nPukul :" + agenda.getObjects().get(i).getJam_mulai() + "-" + agenda.getObjects().get(i).getJam_akhir());
                                mBuilder1.setContentIntent(contentIntent1);
                                mNotificationManager.notify(1, mBuilder1.build());
                            } else {
                                NotificationCompat.Builder mBuilder1 =
                                        new NotificationCompat.Builder(getApplicationContext())
                                                .setSmallIcon(R.mipmap.ic_launcher)
                                                .setAutoCancel(true)
                                                .setVibrate(new long[]{1000, 1000, 1000, 1000, 1000})
                                                .setLights(Color.RED, 3000, 3000)
                                                .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                                                .setContentTitle("Event Kunjungan")
                                                .setContentText("Konsultasi " + agenda.getObjects().get(i).getNama_pasien() + "\nTanggal : " + agenda.getObjects().get(i).getTanggal_kunjungan() + "\nPukul :" + agenda.getObjects().get(i).getJam_mulai() + "-" + agenda.getObjects().get(i).getJam_akhir());
                                mBuilder1.setContentIntent(contentIntent1);
                                mNotificationManager.notify(1, mBuilder1.build());
                            }

                        }


                        Log.e("Log", "" + getMinutes(waktu1, waktu2));
                    }
                    try {

                        Thread.sleep(timeInterval);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        };

        Thread thread = new Thread(runnable);
        thread.start();

    }

    public int getMinutes(String waktu, String waktu2) {
        final DateTimeFormatter formatter1 = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm");
        final DateTime dateTime1 = formatter1.parseDateTime(waktu);
        final DateTime dateTime2 = formatter1.parseDateTime(waktu2);
        Minutes minutes = Minutes.minutesBetween(dateTime1, dateTime2);
        return minutes.getMinutes();
    }

    @Override
    public void setUICallbacks() {
    }


    @Override
    public void onClick(View v) {


    }


    @Override
    public int getLayout() {
        return R.layout.activity_splash_screen;
    }

    @Override
    public void updateUI() {

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}