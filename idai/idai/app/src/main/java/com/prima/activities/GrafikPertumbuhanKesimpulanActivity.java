package com.prima.activities;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.LinearLayout.LayoutParams;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.davemorrissey.labs.subscaleview.ImageSource;
import com.davemorrissey.labs.subscaleview.SubsamplingScaleImageView;

import com.prima.Preferences;
import com.prima.controllers.ApiReferences;

import org.w3c.dom.Text;

import java.util.ArrayList;

import prima.test.prima.R;

/**
 * Created by Codelabs on 31/08/2015.
 */
public class GrafikPertumbuhanKesimpulanActivity extends BaseActivity implements View.OnClickListener {
    private SubsamplingScaleImageView img;
    private ImageView close;
    private TextView kesimpulan, subtitle, title;
    private String hasilKesimpulan;
    private LinearLayout linearKontenKesimpulan;

    ArrayList<String> titlegrafik;
    ArrayList<String> listInterpretasi;


    @Override
    public void initView() {
        img = (SubsamplingScaleImageView) findViewById(R.id.img);
        close = (ImageView) findViewById(R.id.close);
        title = (TextView) findViewById(R.id.title);
        subtitle = (TextView) findViewById(R.id.subtitle);
        kesimpulan = (TextView) findViewById(R.id.kesimpulan);
        linearKontenKesimpulan = (LinearLayout) findViewById(R.id.linearKontenKesimpulan);

        titlegrafik = new ArrayList<String>();
        listInterpretasi = new ArrayList<String>();

        Typeface tf0 = Typeface.createFromAsset(this.getAssets(), "BREVIA-BLACK.OTF");
        Typeface tf = Typeface.createFromAsset(this.getAssets(), "BREVIA-MEDIUM.OTF");

        subtitle.setTypeface(tf);
        kesimpulan.setTypeface(tf);
        title.setTypeface(tf0);

        // Asynchronously load the image and set the thumbnail and pager view
        Bundle bn = getIntent().getExtras();
        if (bn != null) {
            hasilKesimpulan = bn.getString("hasilKesimpulan");
            titlegrafik = bn.getStringArrayList("titleGrafik");
            listInterpretasi = bn.getStringArrayList("listInterpretasi");

            for (int i = 0; i < listInterpretasi.size(); i++) {
                System.out.println("interpretasi " + i + " :" +titlegrafik.get(i) +" " +listInterpretasi.get(i)+"");

                View child = getLayoutInflater().inflate(R.layout.list_grafik_kesimpulan, null);
                TextView titleGrafikTxt = (TextView) child.findViewById(R.id.titleGrafik);
                TextView listInterpretasiTxt = (TextView) child.findViewById(R.id.listInterpretasi);

                try {
                    titleGrafikTxt.setText(titlegrafik.get(i));
                    listInterpretasiTxt.setText(listInterpretasi.get(i));
                } catch (IndexOutOfBoundsException e) {
                    titleGrafikTxt.setVisibility(View.GONE);
                    listInterpretasiTxt.setVisibility(View.GONE);
                }
                linearKontenKesimpulan.addView(child);
            }


            System.out.println("Kesimpulan interpretasi : " + hasilKesimpulan);
            if (hasilKesimpulan != null && hasilKesimpulan.equalsIgnoreCase("1")) {
                kesimpulan.setText("Konsultasikan ke Dokter");

            }
        }
    }


    @Override
    public void setUICallbacks() {
        close.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if (v == close) {
            finish();
        }
    }


    @Override
    public int getLayout() {
        return R.layout.activity_grafik_pertumbuhan_kesimpulan;
    }

    @Override
    public void updateUI() {

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

    }


    @Override
    public void onBackPressed() {
        finish();
    }


}
