package com.prima.activities;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.JsonSyntaxException;
import com.prima.MainActivity;
import com.prima.Preferences;
import com.prima.controllers.ApiReferences;
import com.prima.controllers.Parser;
import com.prima.entities.MenuAge;
import com.prima.entities.Message;
import com.prima.entities.Pdf;
import com.prima.helpers.DatabaseHandler;

import java.util.ArrayList;
import java.util.List;

import prima.test.prima.R;

/**
 * Created by Codelabs on 14/08/2015.
 */
public class SkriningMedisPenjelasanKondisiActivity extends BaseActivity implements View.OnClickListener {
    private ImageView close;
    private ImageView back;
    private Button lanjut;
    TextView title;
    List<RadioButton> radioButtons;
    RequestQueue mRequest;
    MaterialDialog dialog;
    LinearLayout materi;
    static String id = "";
    MenuAge menu;
    DatabaseHandler db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mRequest = Volley.newRequestQueue(getApplicationContext());
        radioButtons = new ArrayList<RadioButton>();
        db = new DatabaseHandler(context);

        Bundle bn = getIntent().getExtras();
        if (bn != null) {
            String respon = bn.getString("respon");
            menu = Parser.getMenuAge(respon);
            int size = (int) Math.ceil(menu.getObjects().size() / 2f);
            for (int i = 0; i < size; i++) {
                View child = getLayoutInflater().inflate(R.layout.list_menu_umur_skrining, null);
                RadioButton ck1 = (RadioButton) child.findViewById(R.id.rad1);
                RadioButton ck2 = (RadioButton) child.findViewById(R.id.rad2);
                TextView txt1 = (TextView) child.findViewById(R.id.txt1);
                TextView txt2 = (TextView) child.findViewById(R.id.txt2);
                int indeks = (int) Math.ceil(menu.getObjects().size() / 2f) + i;

                try {
                    ck2.setId(indeks);
                    txt2.setId(indeks);
                    txt2.setText(menu.getObjects().get(indeks).getRange_age());
                    radioButtons.add(ck2);
                } catch (IndexOutOfBoundsException e) {
                    ck2.setVisibility(View.GONE);
                    txt2.setVisibility(View.GONE);
                }
                ck1.setId(i);
                txt1.setId(i);
                txt1.setText(menu.getObjects().get(i).getRange_age());
                radioButtons.add(ck1);

                materi.addView(child);
            }
            //checked
            for (RadioButton button : radioButtons) {
                button.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        if (isChecked) processRadioButtonClick(buttonView);
                        id = String.valueOf(buttonView.getId());
                        Log.i("ID CHECKBOX", id);
                    }
                });

            }
        }

    }

    @Override
    public void initView() {
        materi = (LinearLayout) findViewById(R.id.menu);
        back = (ImageView) findViewById(R.id.back);
        close = (ImageView) findViewById(R.id.close);
        lanjut = (Button) findViewById(R.id.lanjut);
        title = (TextView) findViewById(R.id.tv_actionbar_title);
        Typeface tf = Typeface.createFromAsset(getApplicationContext().getAssets(), "BREVIA-BLACK.OTF");
        title.setTypeface(tf);
        title.setText(getResources().getString(R.string.PenjelasanKondisiMedis));

    }

    @Override
    public void setUICallbacks() {
        back.setOnClickListener(this);
        close.setOnClickListener(this);
        lanjut.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        if (v == back) {
            Intent i = new Intent(getApplicationContext(), MainActivity.class);
            startActivity(i);
            finish();
        } else if (v == close) {
            Intent i = new Intent(getApplicationContext(), MainActivity.class);
            startActivity(i);
            finish();
        } else if (v == lanjut) {
            boolean found = false;
            for (RadioButton button : radioButtons) {
                if (button.isChecked()) {
                    found = true;
                }
            }
            if (found) {
                if (!id.equals("")) {
                    getPdf(menu.getObjects().get(Integer.valueOf(id)).getId(), "PKM");
                }
            } else {
                dialog(getResources().getString(R.string.PilihGrafik), getResources().getString(R.string.Pesan));
            }


        }
    }


    @Override
    public int getLayout() {
        return R.layout.skrining_utama_umur;
    }

    @Override
    public void updateUI() {

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

    }

    private void processRadioButtonClick(CompoundButton buttonView) {

        for (RadioButton button : radioButtons) {
            if (button != buttonView) button.setChecked(false);
        }

    }


    @Override
    public void onBackPressed() {
        Intent i = new Intent(getApplicationContext(), MainActivity.class);
        startActivity(i);
        finish();
    }


    private void getPdf(final String ID, final String code) {
        progresDialog(false, getResources().getString(R.string.MemuatData));
        StringRequest listdata = new StringRequest(Request.Method.GET,
                ApiReferences.getPdf(code, ID, preferences.getString(Preferences.LANGUAGE, "")),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        dialog.dismiss();
                        try {
                            if (response.equals("[]")) {
                                dialog(getResources().getString(R.string.BelumAdaData), getResources().getString(R.string.Pesan));
                            } else {
                                Message msg = Parser.getMessage(response);
                                if (msg.getStatus().equals("1")) {
                                    Log.i("RESULT JSON", response.toString());
                                    Pdf pdf = Parser.getPdf(response);
                                    Intent i = new Intent(getApplicationContext(), SkriningPenjelasanKondisiPdfActivity.class);
                                    i.putExtra("url", pdf.getObjects().getFilename());
                                    i.putExtra("id", pdf.getObjects().getId());
                                    i.putExtra("menu", pdf.getObjects().getMenu());
                                    i.putExtra("category", pdf.getObjects().getCategory());
                                    startActivity(i);
                                } else {
                                    dialog(msg.getMessages(), getResources().getString(R.string.Pesan));
                                }
                            }
                        } catch (JsonSyntaxException e) {
                            dialog(getResources().getString(R.string.KesalahanServer), getResources().getString(R.string.Perhatian));
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dialog.dismiss();
                Pdf pdf = db.getFilesObject(Integer.valueOf(ID));
                try {
                    Intent i = new Intent(getApplicationContext(), SkriningPenjelasanKondisiPdfActivity.class);
                    i.putExtra("url", pdf.getObjects().getFilename());
                    i.putExtra("id", pdf.getObjects().getId());
                    i.putExtra("menu", pdf.getObjects().getMenu());
                    i.putExtra("category", pdf.getObjects().getCategory());
                    startActivity(i);
                } catch (NullPointerException e) {
                    dialog(getResources().getString(R.string.NoConnection), getResources().getString(R.string.Perhatian));
                }
            }
        });
        listdata.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        mRequest.add(listdata);
    }

    public void dialog2(String content, String msg) {
        new MaterialDialog.Builder(this)
                .title(msg)
                .content(content)
                .negativeText("OK")
                .callback(new MaterialDialog.ButtonCallback() {
                    @Override
                    public void onPositive(MaterialDialog dialog) {

                    }

                    @Override
                    public void onNegative(MaterialDialog dialog) {
                        super.onNegative(dialog);
                    }

                })
                .show();
    }

    public void dialog(String content, String msg) {
        new MaterialDialog.Builder(this)
                .title(msg)
                .content(content)
                .positiveText("Ok")
                .callback(new MaterialDialog.ButtonCallback() {
                    @Override
                    public void onPositive(MaterialDialog dialog) {
                        super.onPositive(dialog);
                    }
                })
                .show();
    }


    private void progresDialog(boolean horizontal, String msg) {
        dialog = new MaterialDialog.Builder(this)
                .title(msg)
                .content(getResources().getString(R.string.MohonTunggu))
                .progress(true, 0)
                .progressIndeterminateStyle(horizontal)
                .cancelable(false)
                .show();
    }


}
