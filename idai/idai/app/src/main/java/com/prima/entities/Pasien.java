package com.prima.entities;

/**
 * Created by Codelabs on 23/08/2015.
 */
public class Pasien {
    private String status;
    private String messages;
    private Object data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessages() {
        return messages;
    }

    public void setMessages(String messages) {
        this.messages = messages;
    }

    public Object getObjects() {
        return data;
    }

    public void setObjects(Object objects) {
        this.data = objects;
    }

    public class Object {
        private String id;
        private String weight;
        private String height;
        private String gender;
        private String dob;

        private ObjectChild child;

        public ObjectChild getChild() {
            return child;
        }

        public void setUserToken(ObjectChild child) {
            this.child = child;
        }

        public class ObjectChild {
            private String id;
            private String name;
            private String dob;
            private String gender;

            public String getId() {
                return id;
            }
            public void setId(String id) {
                this.id = id;
            }

            public String getName() {
                return name;
            }
            public void setName(String name) {
                this.name = name;
            }

            public String getDob() {
                return dob;
            }
            public void setDob(String dob) {
                this.dob = dob;
            }

            public String getGender() {
                return gender;
            }
            public void setGender(String gender) {
                this.gender = gender;
            }

        }


        public String getTanggal_lahir() {
            return dob;
        }

        public void setTanggal_lahir(String dob) {
            this.dob = dob;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getBerat() {
            return weight;
        }

        public void setBerat(String weight) {
            this.weight = weight;
        }

        public String getTinggi() {
            return height;
        }

        public void setTinggi(String height) {
            this.height = height;
        }

        public String getJenis_kelamin() {
            return gender;
        }

        public void setJenis_kelamin(String gender) {
            this.gender = gender;
        }
    }
}
