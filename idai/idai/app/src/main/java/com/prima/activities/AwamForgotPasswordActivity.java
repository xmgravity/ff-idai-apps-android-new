package com.prima.activities;

import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.JsonSyntaxException;
import com.prima.controllers.ApiReferences;
import com.prima.controllers.Parser;
import com.prima.entities.Message;
import com.prima.helpers.DateHelper;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

import prima.test.prima.R;

/**
 * Created by Codelabs on 29/08/2015.
 */
public class AwamForgotPasswordActivity extends BaseActivity implements View.OnClickListener {
    TextView title;
    DateHelper dateHelper;
    RelativeLayout register;
    EditText email;
    EditText newPassword;
    EditText newPasswordConfirm;

    private RequestQueue mRequest;
    MaterialDialog dialog;

    @Override
    public void initView() {

        mRequest = Volley.newRequestQueue(this);

        dateHelper = new DateHelper();
        register = (RelativeLayout) findViewById(R.id.register);
        email = (EditText) findViewById(R.id.email);
        newPassword = (EditText) findViewById(R.id.newPassword);
        newPasswordConfirm = (EditText) findViewById(R.id.newPasswordConfirm);

    }


    @Override
    public void setUICallbacks() {
        register.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {

        if ( v == register){

            //TODO Validation untuk form

            if(newPassword.getText().toString().equals(newPasswordConfirm.getText().toString())) {

                changePassword(email.getText().toString(),
                        newPassword.getText().toString(),
                        newPasswordConfirm.getText().toString()
                );
            }else if(!newPassword.getText().toString().equals(newPasswordConfirm.getText().toString())) {
                dialog(getResources().getString(R.string.KataSandiBaruDanKonfirmasiHarusSama), getResources().getString(R.string.Perhatian));
            }else if(newPassword.length() < 6 || newPasswordConfirm.length() <6) {
                dialog(getResources().getString(R.string.KataSandiLebihAtauSamaDengan6), getResources().getString(R.string.Perhatian));
            }

        }
    }

    private void changePassword(final String email,
                          final String newPassword,
                          final String newPasswordConfirm) {


        System.out.println("email :"+email+", newPassword :"+newPassword+", newPasswordConfirm :"+newPasswordConfirm);

        StringRequest listdata = new StringRequest(Request.Method.POST,
                ApiReferences.forgetPassword(),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.i("RESULT JSON", response.toString());

                        try {
                            Message msg = Parser.getMessage(response);
                            dialog(msg.getMessages().toString(), getResources().getString(R.string.Perhatian));

                        } catch (JsonSyntaxException e) {
                            dialog(getResources().getString(R.string.KesalahanServer), getResources().getString(R.string.Perhatian));
                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {

                NetworkResponse response = error.networkResponse;
                if (error instanceof ServerError && response != null) {
                    try {
                        String res = new String(response.data,
                                HttpHeaderParser.parseCharset(response.headers, "utf-8"));
                        // Now you can use any deserializer to make sense of data
                        JSONObject obj = new JSONObject(res);

                        String b = obj.toString().substring(1, obj.toString().length() -1);
                        String c = b.toString().replace("messages", "");
                        String d = c.toString().replace("\"", "");
                        String e = d.toString().replace(":", "");
                        String f = e.toString().replace("\\", "");
                        String g = f.toString().replace("[", "");
                        String h = g.toString().replace("]", "");

                        dialog(h, getResources().getString(R.string.Perhatian));
                    } catch (UnsupportedEncodingException e1) {
                        // Couldn't properly decode data to string
                        e1.printStackTrace();
                    } catch (JSONException e2) {
                        // returned data is not JSONObject?
                        e2.printStackTrace();
                    }
                }


            }
        }) {

            protected Map<String, String> getParams() {
                HashMap<String, String> params = new HashMap<String, String>();
                params.put("email", email);
                params.put("new_password", newPasswordConfirm);
                return params;


            }
        };

        listdata.setRetryPolicy(new DefaultRetryPolicy(
                20000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        mRequest.add(listdata);
    }



    @Override
    public int getLayout() {
        return R.layout.activity_awam_forgot_password;
    }

    @Override
    public void updateUI() {

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

    }


    @Override
    public void onBackPressed() {
        Intent i = new Intent(getBaseContext(), SplashPagerActivity.class);
        startActivity(i);
        finish();
    }

    public void dialog(String content, String msg) {
        new MaterialDialog.Builder(this)
                .title(msg)
                .content(content)
                .positiveText("Ok")
                .callback(new MaterialDialog.ButtonCallback() {
                    @Override
                    public void onPositive(MaterialDialog dialog) {

                    }

                })
                .show();
    }

}