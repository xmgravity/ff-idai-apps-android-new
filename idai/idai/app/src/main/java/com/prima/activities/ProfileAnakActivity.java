package com.prima.activities;

import android.content.Intent;
import android.graphics.Typeface;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.JsonSyntaxException;
import com.prima.Preferences;
import com.prima.controllers.ApiReferences;
import com.prima.controllers.Parser;
import com.prima.entities.DaftarAnak;
import com.prima.entities.Message;
import com.prima.helpers.DatabaseHandler;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import prima.test.prima.R;

/**
 * Created by Codelabs on 09/09/2015.
 */
public class ProfileAnakActivity extends BaseActivity implements View.OnClickListener {

    TextView title;
    ImageView close, back;
    Button ubahprofilanak;
    LinearLayout list;
    private MaterialDialog dialog;
    private RequestQueue mRequest;
    ArrayList<LinearLayout> days = new ArrayList<LinearLayout>();
    DatabaseHandler db;


    @Override
    public void initView() {
        db = new DatabaseHandler(context);
        mRequest = Volley.newRequestQueue(getApplicationContext());
        list = (LinearLayout) findViewById(R.id.list_daftar_anak);
        close = (ImageView) findViewById(R.id.close);
        back = (ImageView) findViewById(R.id.back);
        // close.setVisibility(View.GONE);
        //set font
        title = (TextView) findViewById(R.id.tv_actionbar_title);
        Typeface tf = Typeface.createFromAsset(getApplicationContext().getAssets(), "BREVIA-BLACK.OTF");
        title.setTypeface(tf);
        title.setText("Anak");
        //    getData();
        //  save(data);
    }

    @Override
    public void setUICallbacks() {
        close.setOnClickListener(this);
        back.setOnClickListener(this);
        ubahprofilanak.setOnClickListener(this);
    }


    private void getData() {
        progresDialog(false, "Memuat data");
        StringRequest listdata = new StringRequest(Request.Method.GET,
                ApiReferences.getPasienForAwam(),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        dialog.dismiss();
                        try {
                            if (response.equals("[]")) {
                                dialog("Data belum tersedia", getResources().getString(R.string.Perhatian));
                            } else {
                                Message msg = Parser.getMessage(response);
                                if (msg.getStatus().equals("0")) {
                                    dialog(msg.getMessages(), getResources().getString(R.string.Perhatian));
                                } else {
                                    Log.i("RESULT JSON", response.toString());
                                    final DaftarAnak daftarAnakResult = Parser.getDaftarAnak(response);

                                    for (int i = 0; i < daftarAnakResult.getObjects().size(); i++) {
                                        View child = getLayoutInflater().inflate(R.layout.list_item_anak, null);
                                        TextView child_detail_name = (TextView) child.findViewById(R.id.child_detail_name);
                                        TextView child_detail_gender = (TextView) child.findViewById(R.id.child_detail_gender);
                                        TextView child_detail_age = (TextView) child.findViewById(R.id.child_detail_age);
                                        TextView child_detail_height = (TextView) child.findViewById(R.id.child_detail_height);
                                        TextView child_detail_weight = (TextView) child.findViewById(R.id.child_detail_weight);
                                        LinearLayout day = (LinearLayout) child.findViewById(R.id.day);

                                        child_detail_name.setId(i);
                                        child_detail_name.setText(daftarAnakResult.getObjects().get(i).getId());
                                        child_detail_gender.setId(i);
                                        if(daftarAnakResult.getObjects().get(i).getGender().toString().equals("m")){
                                            child_detail_gender.setText("Laki-laki");
                                        }else {
                                            child_detail_gender.setText("Perempuan");
                                        }

                                        child_detail_age.setId(i);
                                        child_detail_age.setText(daftarAnakResult.getObjects().get(i).getDob());

                                        child_detail_height.setId(i);
                                        child_detail_height.setText(daftarAnakResult.getObjects().get(i).getTinggi());

                                        child_detail_weight.setId(i);
                                        child_detail_weight.setText(daftarAnakResult.getObjects().get(i).getBerat());

                                        child.setId(i);

                                        list.addView(child);


                                    }


                                    for (int i = 0; i < days.size(); i++) {
                                        View v = list.getChildAt(i);
                                        if (days.get(i).getChildCount() == 0) {
                                            list.removeView(v);
                                        }
                                    }

                                }
                            }
                        } catch (JsonSyntaxException e) {
                            dialog(getResources().getString(R.string.KesalahanServer), getResources().getString(R.string.Perhatian));
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                NetworkResponse response = error.networkResponse;
                if (error instanceof ServerError && response != null) {
                    try {
                        String res = new String(response.data,
                                HttpHeaderParser.parseCharset(response.headers, "utf-8"));
                        // Now you can use any deserializer to make sense of data
                        JSONObject obj = new JSONObject(res);
                        dialog(res.toString(), getResources().getString(R.string.Perhatian));
                    } catch (UnsupportedEncodingException e1) {
                        // Couldn't properly decode data to string
                        e1.printStackTrace();
                    } catch (JSONException e2) {
                        // returned data is not JSONObject?
                        e2.printStackTrace();
                    }
                }
            }
        }) {

            public Map<String, String> getHeaders() {
                Map<String, String>  params = new HashMap<String, String>();
                params.put("token", preferences.getString(Preferences.ACCESS_TOKEN, ""));

                return params;
            }
        };

        listdata.setRetryPolicy(new DefaultRetryPolicy(
                20000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        mRequest.add(listdata);
    }


    public void dialog(String content, String msg) {
        new MaterialDialog.Builder(this)
                .title(msg)
                .content(content)
                .positiveText("Ok")
                .callback(new MaterialDialog.ButtonCallback() {
                    @Override
                    public void onPositive(MaterialDialog dialog) {
                    }

                })
                .show();
    }

    public void dialog2(String content, String msg) {
        new MaterialDialog.Builder(this)
                .title(msg)
                .content(content)
                .negativeText("OK")
                .callback(new MaterialDialog.ButtonCallback() {
                    @Override
                    public void onPositive(MaterialDialog dialog) {

                    }

                    @Override
                    public void onNegative(MaterialDialog dialog) {
                        super.onNegative(dialog);
                    }

                })
                .show();
    }

    private void progresDialog(boolean horizontal, String msg) {
        dialog = new MaterialDialog.Builder(this)
                .title(msg)
                .content(getResources().getString(R.string.MohonTunggu))
                .progress(true, 0)
                .progressIndeterminateStyle(horizontal)
                .cancelable(false)
                .show();
    }


    @Override
    public void onClick(View v) {
        if (v == back) {
            Intent i = new Intent(getApplicationContext(), ProfileActivity.class);
            startActivity(i);
        } else if (v == close) {
            Intent i = new Intent(getApplicationContext(), PraktikActivity.class);
            startActivity(i);
        } else if (v == ubahprofilanak) {
            Intent i = new Intent(getApplicationContext(), ProfileAnakChangeActivity.class);
            startActivity(i);
            //   finish();
        }
    }


    @Override
    public int getLayout() {
        return R.layout.activity_profileanak;
    }

    @Override
    public void updateUI() {
        list.removeAllViewsInLayout();
        days.clear();
        getData();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

    }


    @Override
    public void onBackPressed() {
        Intent i = new Intent(getApplicationContext(), ProfileActivity.class);
        startActivity(i);
        //finish();
    }




}
