package com.prima.activities;

import android.content.Intent;
import android.graphics.Typeface;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import prima.test.prima.R;

/**
 * Created by Codelabs on 29/08/2015.
 */
public class ForgotPasswordActivity extends BaseActivity implements View.OnClickListener {

    private TextView forgot, txt;
    ImageView back;

    @Override
    public void initView() {
        back = (ImageView) findViewById(R.id.back);
        txt = (TextView) findViewById(R.id.txt);
        Typeface tf = Typeface.createFromAsset(getApplicationContext().getAssets(), "BREVIA-BLACK.OTF");
        Typeface tfItalic = Typeface.createFromAsset(getApplicationContext().getAssets(), "Brevia-ExtraBlackItalic.otf");
        txt.setTypeface(tf);
        forgot = (TextView) findViewById(R.id.forgot);
        forgot.setTypeface(tfItalic);
        back.setVisibility(View.GONE);

    }

    @Override
    public void setUICallbacks() {
        back.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        if (v == back) {
            //  Intent i = new Intent(ForgotPasswordActivity.this, LoginActivity.class);
            ///    startActivity(i);
            finish();
        }

    }


    @Override
    public int getLayout() {
        return R.layout.activity_forgot_password;
    }

    @Override
    public void updateUI() {

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

    }


    @Override
    public void onBackPressed() {
        //   Intent i = new Intent(ForgotPasswordActivity.this, LoginActivity.class);
        //  startActivity(i);
        finish();
    }
}