package com.prima.activities;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.prima.controllers.ApiReferences;
import com.prima.controllers.Parser;
import com.prima.entities.JadwalPraktek;
import com.prima.entities.JadwalPraktekSingle;
import com.prima.entities.Message;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import prima.test.prima.R;

/**
 * Created by Codelabs on 10/09/2015.
 */
public class EditJadwalActivity extends BaseActivity implements View.OnClickListener {

    private TextView title, txt_alamat;
    private ImageView close, back;
    private ArrayList<String> listItem = new ArrayList<String>();
    private Context mContext;
    private RelativeLayout alamat, jadwal;
    private Button simpan;
    private MaterialDialog dialog;
    private RequestQueue mRequest;
    private ArrayList<JadwalPraktek.Jadwal> praktek = new ArrayList<JadwalPraktek.Jadwal>();
    private EditText edt_nama_praktek, edt_tlp;
    private JadwalPraktekSingle jdwlPraktek;
    private JadwalPraktekSingle objJadwal = new JadwalPraktekSingle();
    LinearLayout list;
    String latitude = "";
    String longitude = "";

    @Override
    public void initView() {
        list = (LinearLayout) findViewById(R.id.day);
        mRequest = Volley.newRequestQueue(getApplicationContext());
        edt_nama_praktek = (EditText) findViewById(R.id.edt_nama_praktek);
        edt_tlp = (EditText) findViewById(R.id.edt_tlp);
        alamat = (RelativeLayout) findViewById(R.id.alamat);
        jadwal = (RelativeLayout) findViewById(R.id.jadwal);
        title = (TextView) findViewById(R.id.tv_actionbar_title);
        txt_alamat = (TextView) findViewById(R.id.txt_alamat);
        close = (ImageView) findViewById(R.id.close);
        back = (ImageView) findViewById(R.id.back);
        simpan = (Button) findViewById(R.id.simpan);
        close.setVisibility(View.GONE);
        mContext = getApplicationContext();
        //set font
        Typeface tf = Typeface.createFromAsset(getApplicationContext().getAssets(), "BREVIA-BLACK.OTF");
        title.setTypeface(tf);
        title.setText(getResources().getString(R.string.Praktik));
        for (int i = 0; i < 10; i++) {
            listItem.add("Text");
        }

        Bundle bn = getIntent().getExtras();
        if (bn != null) {
            jdwlPraktek = Parser.getJadwalSingle(bn.getString("result"));
            Log.d("dbgbg", "alamat: " + jdwlPraktek.getObjects().getAlamat() + "\nnama: " + jdwlPraktek.getObjects().getNama_praktik() + "\ntelp: " + jdwlPraktek.getObjects().getTelp() + "\nlat: " + jdwlPraktek.getObjects().getLatitude() + "\nlon: " + jdwlPraktek.getObjects().getLongitude() + "\nhours: " + jdwlPraktek.getObjects().getJadwal().get(0).getId() + "\n" + jdwlPraktek.getObjects().getJadwal().get(0).getId_profil_praktik() + "\n" + jdwlPraktek.getObjects().getJadwal().get(0).getHari() + "\n" + jdwlPraktek.getObjects().getJadwal().get(0).getJam_mulai() + "\n" + jdwlPraktek.getObjects().getJadwal().get(0).getJam_akhir());
            txt_alamat.setText(jdwlPraktek.getObjects().getAlamat());
            edt_nama_praktek.setText(jdwlPraktek.getObjects().getNama_praktik());
            edt_tlp.setText(jdwlPraktek.getObjects().getTelp());
            latitude = jdwlPraktek.getObjects().getLatitude();
            longitude = jdwlPraktek.getObjects().getLongitude();
            objJadwal = Parser.getJadwalSingle(bn.getString("result"));
            //objJadwal.getObjects().setJadwal(jdwlPraktek.getObjects().getJadwal());
            /*for (JadwalPraktek.Jadwal jadwal : jdwlPraktek.getObjects().getJadwal()) {
                objJadwal.getObjects().getJadwal().add(jadwal);
            }*/

            for (int k = 0; k < jdwlPraktek.getObjects().getJadwal().size(); k++) {
                //if (jdwlPraktek.getStatus().equals("1")) {
                    View chidlHari = getLayoutInflater().inflate(R.layout.list_item_day, null);
                    TextView waktu = (TextView) chidlHari.findViewById(R.id.waktu);
                    TextView lokasi = (TextView) chidlHari.findViewById(R.id.lokasi);
                    waktu.setId(22222 + k);
                    lokasi.setId(11111 + k);
                    lokasi.setText(jdwlPraktek.getObjects().getJadwal().get(k).getJam_mulai().substring(0, 5) + "-" + jdwlPraktek.getObjects().getJadwal().get(k).getJam_akhir().substring(0, 5));

                    if (jdwlPraktek.getObjects().getJadwal().get(k).getHari().equalsIgnoreCase("0")) {
                        waktu.setText(getResources().getString(R.string.Senin));
                    }
                    if (jdwlPraktek.getObjects().getJadwal().get(k).getHari().equalsIgnoreCase("1")) {
                        waktu.setText(getResources().getString(R.string.Selasa));
                    }
                    if (jdwlPraktek.getObjects().getJadwal().get(k).getHari().equalsIgnoreCase("2")) {
                        waktu.setText(getResources().getString(R.string.Rabu));
                    }
                    if (jdwlPraktek.getObjects().getJadwal().get(k).getHari().equalsIgnoreCase("3")) {
                        waktu.setText(getResources().getString(R.string.Kamis));
                    }
                    if (jdwlPraktek.getObjects().getJadwal().get(k).getHari().equalsIgnoreCase("4")) {
                        waktu.setText(getResources().getString(R.string.Jumat));
                    }
                    if (jdwlPraktek.getObjects().getJadwal().get(k).getHari().equalsIgnoreCase("5")) {
                        waktu.setText(getResources().getString(R.string.Sabtu));
                    }
                    if (jdwlPraktek.getObjects().getJadwal().get(k).getHari().equalsIgnoreCase("6")) {
                        waktu.setText(getResources().getString(R.string.Minggu));
                    }
                    list.addView(chidlHari);
                //}
            }
        }


    }

    @Override
    public void setUICallbacks() {
        simpan.setOnClickListener(this);
        back.setOnClickListener(this);
        alamat.setOnClickListener(this);
        jadwal.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        if (v == back) {
            // Intent i = new Intent(getApplicationContext(), ProfileActivity.class);
            //    startActivity(i);
            finish();
        } else if (v == alamat) {
            Intent i = new Intent(getApplicationContext(), SearchAlamatActivity.class);
            i.putExtra("longitude", longitude);
            i.putExtra("latitude", latitude);
            startActivityForResult(i, 2);
            //    finish();
        } else if (v == jadwal) {
            Intent i = new Intent(getApplicationContext(), PraktikPickHariActivity.class);
            Gson gson = new Gson();
            i.putExtra("respon", gson.toJson(jdwlPraktek));
            startActivityForResult(i, 1);
            //   finish();
        } else if (v == simpan) {
            if (edt_nama_praktek.getText().toString().trim().equals("") ||
                    txt_alamat.getText().toString().trim().equals("") ||
                    edt_tlp.getText().toString().trim().equals("")) {
                dialog(getResources().getString(R.string.LengkapiDataJadwalPraktik), getResources().getString(R.string.Perhatian));
            } else {
                try {
                    save(objJadwal.getObjects().getJadwal());
                } catch (NullPointerException e) {
                    e.printStackTrace();
                    dialog(getResources().getString(R.string.IsiJadwalPraktik), getResources().getString(R.string.Perhatian));
                }
            }
        }
    }


    private void save(final ArrayList<JadwalPraktek.Jadwal> data) {
        progresDialog(false, getResources().getString(R.string.MemuatData));
        StringRequest listdata = new StringRequest(Request.Method.POST,
                ApiReferences.updatePraktik(jdwlPraktek.getObjects().getId()),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.i("RESULT JSON", response.toString());
                        dialog.dismiss();
                        try {
                            Message msg = Parser.getMessage(response);
                            if (msg.getStatus().equals("1")) {
                                finish();
                            } else {
                                dialog(msg.getMessages(), getResources().getString(R.string.Perhatian));
                            }
                        } catch (JsonSyntaxException e) {
                            dialog(getResources().getString(R.string.KesalahanServer), getResources().getString(R.string.Perhatian));
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("dbgbg", error.toString());
                dialog.dismiss();
                dialog(getResources().getString(R.string.NoConnection), getResources().getString(R.string.Perhatian));
            }
        }) {

            protected Map<String, String> getParams() {
                HashMap<String, String> params = new HashMap<String, String>();
                params.put("name", edt_nama_praktek.getText().toString());
                params.put("address", txt_alamat.getText().toString());
                params.put("phone", edt_tlp.getText().toString());
                params.put("lng", longitude);
                params.put("lat", latitude);
                for (int i = 0; i < data.size(); i++) {
                    /*if (data.get(i).getStatus().equals("0")) {
                        params.put("hari[" + i + "]", String.valueOf(i));
                        params.put("jam_mulai[" + i + "]", "");
                        params.put("jam_akhir[" + i + "]", "");
                        params.put("status[" + i + "]", "");
                    } else */if (data.get(i).getStatus().equals("1")) {
                    Log.d("dbgbg", "getParams: " + i);
                        params.put("day[" + i + "]", data.get(i).getHari());
                        params.put("start_time[" + i + "]", data.get(i).getJam_mulai());
                        params.put("end_time[" + i + "]", data.get(i).getJam_akhir());
                        //params.put("status[" + i + "]", data.get(i).getStatus());
                    }
                }
                return params;
            }
        };
        listdata.setRetryPolicy(new DefaultRetryPolicy(
                20000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        mRequest.add(listdata);
    }


    @Override
    public int getLayout() {
        return R.layout.activity_praktik;
    }

    @Override
    public void updateUI() {

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 1) {
            if (data != null) {
                list.removeAllViews();
                objJadwal = Parser.getJadwalSingle(data.getStringExtra("result"));
                // Toast.makeText(this, data.getStringExtra("result"), Toast.LENGTH_LONG).show();
                jdwlPraktek.getObjects().setJadwal(objJadwal.getObjects().getJadwal());

                Log.d("dbgbg", "size: "+objJadwal.getObjects().getJadwal().size());
                for (int k = 0; k < objJadwal.getObjects().getJadwal().size(); k++) {
                    Log.d("dbgbg", "k: "+String.valueOf(k));
                    if (objJadwal.getObjects().getJadwal().get(k).getStatus().equals("1")) {
                        Log.d("dbgbg", String.valueOf(k)+" is 1");
                        View chidlHari = getLayoutInflater().inflate(R.layout.list_item_day, null);
                        TextView waktu = (TextView) chidlHari.findViewById(R.id.waktu);
                        TextView lokasi = (TextView) chidlHari.findViewById(R.id.lokasi);
                        waktu.setId(22222 + k);
                        lokasi.setId(11111 + k);
                        lokasi.setText(objJadwal.getObjects().getJadwal().get(k).getJam_mulai().substring(0, 5) + "-" + objJadwal.getObjects().getJadwal().get(k).getJam_akhir().substring(0, 5));

                        if (objJadwal.getObjects().getJadwal().get(k).getHari().equals("0")) {
                            waktu.setText(getResources().getString(R.string.Senin));
                        }
                        if (objJadwal.getObjects().getJadwal().get(k).getHari().equals("1")) {
                            waktu.setText(getResources().getString(R.string.Selasa));
                        }
                        if (objJadwal.getObjects().getJadwal().get(k).getHari().equals("2")) {
                            waktu.setText(getResources().getString(R.string.Rabu));
                        }
                        if (objJadwal.getObjects().getJadwal().get(k).getHari().equals("3")) {
                            waktu.setText(getResources().getString(R.string.Kamis));
                        }
                        if (objJadwal.getObjects().getJadwal().get(k).getHari().equals("4")) {
                            waktu.setText(getResources().getString(R.string.Jumat));
                        }
                        if (objJadwal.getObjects().getJadwal().get(k).getHari().equals("5")) {
                            waktu.setText(getResources().getString(R.string.Sabtu));
                        }
                        if (objJadwal.getObjects().getJadwal().get(k).getHari().equals("6")) {
                            waktu.setText(getResources().getString(R.string.Minggu));
                        }
                        list.addView(chidlHari);
                    }
                }
            }
        } else if (requestCode == 2) {
            if (data != null) {
                txt_alamat.setText(data.getStringExtra("result"));
                longitude = data.getStringExtra("longitude");
                latitude = data.getStringExtra("latitude");
            }
        }
    }


    public void dialog(String content, String msg) {
        new MaterialDialog.Builder(this)
                .title(msg)
                .content(content)
                .positiveText("Ok")
                .callback(new MaterialDialog.ButtonCallback() {
                    @Override
                    public void onPositive(MaterialDialog dialog) {
                    }

                })
                .show();
    }

    public void dialog2(String content, String msg) {
        new MaterialDialog.Builder(this)
                .title(msg)
                .content(content)
                .negativeText("OK")
                .callback(new MaterialDialog.ButtonCallback() {
                    @Override
                    public void onPositive(MaterialDialog dialog) {

                    }

                    @Override
                    public void onNegative(MaterialDialog dialog) {
                        super.onNegative(dialog);
                    }

                })
                .show();
    }

    private void progresDialog(boolean horizontal, String msg) {
        dialog = new MaterialDialog.Builder(this)
                .title(msg)
                .content(getResources().getString(R.string.MohonTunggu))
                .progress(true, 0)
                .progressIndeterminateStyle(horizontal)
                .cancelable(false)
                .show();
    }


    @Override
    public void onBackPressed() {
        //   Intent i = new Intent(getApplicationContext(), ProfileActivity.class);
        //   startActivity(i);
        finish();
    }
}
