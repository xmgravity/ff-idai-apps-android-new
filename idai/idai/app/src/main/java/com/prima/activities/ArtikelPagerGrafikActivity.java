package com.prima.activities;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.JsonSyntaxException;
import com.prima.MainActivity;
import com.prima.Preferences;
import com.prima.adapters.PagerAdapter;
import com.prima.controllers.ApiReferences;
import com.prima.controllers.Parser;
import com.prima.entities.Artikel;
import com.prima.entities.Grafik;
import com.prima.entities.Message;
import com.prima.entities.Pasien;
import com.prima.fragments.ArtikelPinkFragment;
import com.prima.helpers.DateHelper;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import prima.test.prima.R;

/**
 * Created by Codelabs on 06/10/2015.
 */
public class ArtikelPagerGrafikActivity extends BaseActivity implements View.OnClickListener {
    private Button ok;
    private Context mContext;
    private ViewPager mPager;
    private PagerAdapter mPagerAdapter;
    private static int currentPage;
    PageListener pageListener;
    static TextView title;
    List<Fragment> fragmentList;
    static LinearLayout indikatorWrapper, rel_lingkar_kepala, potensiGenetikGroup;
    private Artikel artikel;


    ///
    private CheckBox potensiGenetik;
    private ImageView close;
    private RequestQueue mRequest;
    private Button dialogLanjut;
    List<CheckBox> checkBoxs;
    ArrayList<String> selected;
    ArrayList<String> grafikName;
    ArrayList<String> interpretasi;
    ArrayList<String> label;
    String id;
    static MaterialDialog dialog;
    Dialog dialog2;
    String result;
    RelativeLayout source_wraper;
    DateHelper dateHelper;
    int circumference_graph;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = getApplicationContext();
    }

    @Override
    public void initView() {
        indikatorWrapper = (LinearLayout) findViewById(R.id.indikator_pager);
        mPager = (ViewPager) findViewById(R.id.pager);

        title = (TextView) findViewById(R.id.title);
        pageListener = new PageListener();
        mPager.setOnPageChangeListener(pageListener);
        ok = (Button) findViewById(R.id.lanjut);
        Typeface tf = Typeface.createFromAsset(getApplicationContext().getAssets(), "BREVIA-BLACK.OTF");
        title.setTypeface(tf);

        Typeface tfAriali = Typeface.createFromAsset(getApplicationContext().getAssets(), "ariali.ttf");
        Typeface tfArialb = Typeface.createFromAsset(getApplicationContext().getAssets(), "arialbd.ttf");

        TextView trademark = (TextView) findViewById(R.id.trademark);
        TextView trademark2 = (TextView) findViewById(R.id.trademark2);
        TextView trademark3 = (TextView) findViewById(R.id.trademark3);
        trademark.setTypeface(tfAriali);
        trademark2.setTypeface(tfArialb);
        trademark3.setTypeface(tfAriali);

        if(preferences.getString(Preferences.TIPE_USER, "").equalsIgnoreCase("awam")){
            trademark.setVisibility(View.GONE);
            trademark2.setVisibility(View.GONE);
            trademark3.setVisibility(View.GONE);
        }

        dateHelper = new DateHelper();
        close = (ImageView) findViewById(R.id.close);
        mRequest = Volley.newRequestQueue(getApplicationContext());
        id = preferences.getString(Preferences.ID, "");

    }

    @Override
    public void setUICallbacks() {
        ok.setOnClickListener(this);
        close.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        if (v == ok) {
            if (!id.equalsIgnoreCase("")) {
                showDialog();
            } else {
                // get prompts.xml view
                LayoutInflater li = LayoutInflater.from(getApplicationContext());
                View dialogView = li.inflate(R.layout.dialog_offline_kalkulator, null);
                final Dialog dialog3;
                dialog3 = new Dialog(context);
                dialog3.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog3.setContentView(dialogView);
                //make transparent dialog_sesi_offline_pelod
                dialog3.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                dialog3.show();


                Button dialogLanjut = (Button) dialogView.findViewById(R.id.ok);
                final RadioButton rad1 = (RadioButton) dialogView.findViewById(R.id.rad1);
                final RadioButton rad2 = (RadioButton) dialogView.findViewById(R.id.rad2);
                rad1.setChecked(true);

                rad1.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        rad1.setChecked(true);
                        rad2.setChecked(false);
                    }
                });

                rad2.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        rad2.setChecked(true);
                        rad1.setChecked(false);
                    }
                });

                final TextView edt = (TextView) dialogView.findViewById(R.id.edt1);
                final EditText edt2 = (EditText) dialogView.findViewById(R.id.edt2);
                final EditText edt3 = (EditText) dialogView.findViewById(R.id.edt3);


                edt.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        showDatepickerDialog(edt);
                    }
                });
                dialogLanjut.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (edt.getText().toString().equals("") ||
                                edt2.getText().toString().equals("") ||
                                edt3.getText().toString().equals("")) {
                            dialog(getResources().getString(R.string.IsiSeluruhIsian), getResources().getString(R.string.Perhatian));
                        } else {
                            String jk;
                            if (rad1.isChecked()) {
                                jk = "m";
                            } else if (rad2.isChecked()) {
                                jk = "f";
                            } else {
                                jk = "";
                            }
                            saveSesiPasienOffline(jk, edt.getText().toString(), edt3.getText().toString(), edt2.getText().toString());
                            dialog3.dismiss();
                        }
                    }
                });
            }

        } else if (v == close) {
            Intent i = new Intent(this, MainActivity.class);
            startActivity(i);
            finish();
        }
    }


    @Override
    public int getLayout() {
        return R.layout.activity_grafik_pertumbuhan;
    }

    @Override
    public void updateUI() {
        indikatorWrapper.removeAllViewsInLayout();
        fragmentList = new ArrayList<Fragment>();
        selected = new ArrayList<String>();
        grafikName = new ArrayList<String>();
        interpretasi = new ArrayList<String>();
        label = new ArrayList<String>();
        Bundle bn = getIntent().getExtras();
        if (bn != null) {
            String respon = bn.getString("result");
            artikel = Parser.getArtikel(respon);
            //   title.setText(artikel.getObjects().get(0).getTitle());
            for (int i = 0; i < artikel.getObjects().size(); i++) {
                fragmentList.add(new ArtikelPinkFragment(i, artikel));
                View child = getLayoutInflater().inflate(R.layout.indikator_pager, null);
                View child2 = getLayoutInflater().inflate(R.layout.indikator_pager_blue, null);
                if (i == 0) {
                    indikatorWrapper.addView(child2);
                } else {
                    indikatorWrapper.addView(child);
                }
            }

            setupFrae();
            if (artikel.getObjects().size() > 1) {
                indikatorWrapper.setVisibility(View.VISIBLE);
            } else {
                indikatorWrapper.setVisibility(View.GONE);
            }
        }

        editor.putString(Preferences.DAD_HEIGHT, "0");
        editor.putString(Preferences.MOM_HEIGHT, "0");
        editor.commit();


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

    }


    void showDialog() {
        // get prompts.xml view
        LayoutInflater li = LayoutInflater.from(getApplicationContext());
        View dialogView = li.inflate(R.layout.dialog_grafik_pertumbuhan, null);

        dialog2 = new Dialog(context);
        dialog2.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog2.setContentView(dialogView);
        //make transparent dialog_sesi_offline_pelod
        dialog2.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog2.show();
        checkBoxs = new ArrayList<CheckBox>();

        checkBoxs.add((CheckBox) dialogView.findViewById(R.id.panjang_thd_umur));
        checkBoxs.add((CheckBox) dialogView.findViewById(R.id.berat_thd_umur));
        checkBoxs.add((CheckBox) dialogView.findViewById(R.id.berat_thd_panjang));
        checkBoxs.add((CheckBox) dialogView.findViewById(R.id.indeks_masa_tubuh));
        checkBoxs.add((CheckBox) dialogView.findViewById(R.id.lingkar_kepala));

        potensiGenetik = (CheckBox) dialogView.findViewById(R.id.potensiGenetik);

        if(preferences.getString(Preferences.TIPE_USER, "").equalsIgnoreCase("awam")){
            LinearLayout potensiGenetikGroup = (LinearLayout) dialogView.findViewById(R.id.potensiGenetikGroup);
            potensiGenetikGroup.setVisibility(View.GONE);
        }else{
            LinearLayout rel_lingkar_kepala = (LinearLayout) dialogView.findViewById(R.id.rel_lingkar_kepala);
            rel_lingkar_kepala.setVisibility(View.GONE);

        }


        rel_lingkar_kepala = (LinearLayout) dialogView.findViewById(R.id.rel_lingkar_kepala);
        potensiGenetikGroup = (LinearLayout) dialogView.findViewById(R.id.potensiGenetikGroup);

        int i = getYear();
        if (i > 5) {
            rel_lingkar_kepala.setVisibility(View.GONE);
        }else if (i < 5) {
            potensiGenetikGroup.setVisibility(View.GONE);
        }

        dialogLanjut = (Button) dialogView.findViewById(R.id.lanjut);
        dialogLanjut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //TODO tinggi potensi genetik
                if (potensiGenetik.isChecked()) {

                    // get prompts.xml view
                    LayoutInflater li = LayoutInflater.from(getApplicationContext());
                    View dialogView = li.inflate(R.layout.dialog_potensi_genetik, null);
                    final Dialog dialog3;
                    dialog3 = new Dialog(context);
                    dialog3.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    dialog3.setContentView(dialogView);
                    //make transparent dialog_sesi_offline_pelod
                    dialog3.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                    dialog3.show();
                    Button dialogLanjut = (Button) dialogView.findViewById(R.id.lanjut);
                    final EditText edt = (EditText) dialogView.findViewById(R.id.edt1);
                    final EditText edt2 = (EditText) dialogView.findViewById(R.id.edt2);
                    dialogLanjut.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (edt.getText().toString().trim().equals("") || edt2.getText().toString().trim().equals("")) {
                                dialog(getResources().getString(R.string.IsiData), getResources().getString(R.string.TdkDapatDilanjutkan));
                            } else {
                                dialog3.dismiss();

                                editor.putString(Preferences.DAD_HEIGHT, edt.getText().toString().trim());
                                editor.putString(Preferences.MOM_HEIGHT, edt2.getText().toString().trim());
                                editor.commit();

                                if (checkBoxs.get(4).isChecked()) {
                                    //TODO di hidden kalau mau hilangkan lingkar kepala
                                    // get prompts.xml view
                                    LayoutInflater li1 = LayoutInflater.from(getApplicationContext());
                                    View dialogView1 = li1.inflate(R.layout.dialog_lingkar_kepala, null);
                                    final Dialog dialog4;
                                    dialog4 = new Dialog(context);
                                    dialog4.requestWindowFeature(Window.FEATURE_NO_TITLE);
                                    dialog4.setContentView(dialogView1);
                                    //make transparent dialog_sesi_offline_pelod
                                    dialog4.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                                    dialog4.show();
                                    Button dialogLanjut1 = (Button) dialogView1.findViewById(R.id.lanjut);
                                    final EditText edt3 = (EditText) dialogView1.findViewById(R.id.edt1);
                                    LinearLayout grafik_option_group = (LinearLayout) dialogView1.findViewById(R.id.grafik_option_group);
                                    grafik_option_group.setVisibility(View.GONE);

                                    dialogLanjut1.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            if (edt3.getText().toString().trim().equals("")) {
                                                dialog(getResources().getString(R.string.IsiLingkarKepala), getResources().getString(R.string.TdkDapatDilanjutkan));
                                            } else {
                                                dialog4.dismiss();
                                                editor.putString(Preferences.LINGKAR_KEPALA, edt3.getText().toString());
                                                editor.commit();
                                                boolean ketemu = false;
                                                for (int i = 0; i < checkBoxs.size(); i++) {
                                                    if (checkBoxs.get(i).isChecked()) {
                                                        ketemu = true;
                                                    }
                                                }

                                                if (ketemu) {
                                                    pjgTinggiThdUmur();
                                                } else {
                                                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.PilihGrafik), Toast.LENGTH_LONG).show();
                                                }
                                            }
                                        }
                                    });
                                }else{
                                    boolean ketemu = false;
                                    for (int i = 0; i < checkBoxs.size(); i++) {
                                        if (checkBoxs.get(i).isChecked()) {
                                            ketemu = true;
                                        }
                                    }
                                    if (ketemu) {
                                        pjgTinggiThdUmur();
                                    } else {
                                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.PilihGrafik), Toast.LENGTH_LONG).show();
                                    }

                                }
                            }
                        }
                    });
                }else if (checkBoxs.get(4).isChecked()) {
                    //TODO di hidden kalau mau hilangkan lingkar kepala
                    // get prompts.xml view
                    LayoutInflater li1 = LayoutInflater.from(getApplicationContext());
                    View dialogView1 = li1.inflate(R.layout.dialog_lingkar_kepala, null);
                    final Dialog dialog4;
                    dialog4 = new Dialog(context);
                    dialog4.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    dialog4.setContentView(dialogView1);
                    //make transparent dialog_sesi_offline_pelod
                    dialog4.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                    dialog4.show();
                    Button dialogLanjut1 = (Button) dialogView1.findViewById(R.id.lanjut);
                    final EditText edt3 = (EditText) dialogView1.findViewById(R.id.edt1);
                    LinearLayout grafik_option_group = (LinearLayout) dialogView1.findViewById(R.id.grafik_option_group);
                    grafik_option_group.setVisibility(View.GONE);

                    dialogLanjut1.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (edt3.getText().toString().trim().equals("")) {
                                dialog(getResources().getString(R.string.IsiLingkarKepala), getResources().getString(R.string.TdkDapatDilanjutkan));
                            } else {
                                dialog4.dismiss();
                                editor.putString(Preferences.LINGKAR_KEPALA, edt3.getText().toString());
                                editor.commit();


                                if (potensiGenetik.isChecked()) {

                                    // get prompts.xml view
                                    LayoutInflater li = LayoutInflater.from(getApplicationContext());
                                    View dialogView = li.inflate(R.layout.dialog_potensi_genetik, null);
                                    final Dialog dialog3;
                                    dialog3 = new Dialog(context);
                                    dialog3.requestWindowFeature(Window.FEATURE_NO_TITLE);
                                    dialog3.setContentView(dialogView);
                                    //make transparent dialog_sesi_offline_pelod
                                    dialog3.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                                    dialog3.show();
                                    Button dialogLanjut = (Button) dialogView.findViewById(R.id.lanjut);
                                    final EditText edt = (EditText) dialogView.findViewById(R.id.edt1);
                                    final EditText edt2 = (EditText) dialogView.findViewById(R.id.edt2);
                                    dialogLanjut.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            if (edt.getText().toString().trim().equals("") || edt2.getText().toString().trim().equals("")) {
                                                dialog(getResources().getString(R.string.IsiData), getResources().getString(R.string.TdkDapatDilanjutkan));
                                            } else {
                                                dialog3.dismiss();

                                                editor.putString(Preferences.DAD_HEIGHT, edt.getText().toString().trim());
                                                editor.putString(Preferences.MOM_HEIGHT, edt2.getText().toString().trim());
                                                editor.commit();

                                                boolean ketemu = false;
                                                for (int i = 0; i < checkBoxs.size(); i++) {
                                                    if (checkBoxs.get(i).isChecked()) {
                                                        ketemu = true;
                                                    }
                                                }

                                                if (ketemu) {
                                                    pjgTinggiThdUmur();
                                                } else {
                                                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.PilihGrafik), Toast.LENGTH_LONG).show();
                                                }

                                           }
                                        }
                                    });
                                }else{

                                    boolean ketemu = false;
                                    for (int i = 0; i < checkBoxs.size(); i++) {
                                        if (checkBoxs.get(i).isChecked()) {
                                            ketemu = true;
                                        }
                                    }
                                    if (ketemu) {
                                        pjgTinggiThdUmur();
                                    } else {
                                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.PilihGrafik), Toast.LENGTH_LONG).show();
                                    }
                                }
                            }
                        }
                    });
                }else{
                    boolean ketemu = false;
                    for (int i = 0; i < checkBoxs.size(); i++) {
                        if (checkBoxs.get(i).isChecked()) {
                            ketemu = true;
                        }
                    }
                    if (ketemu) {
                        pjgTinggiThdUmur();
                    } else {
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.PilihGrafik), Toast.LENGTH_LONG).show();
                    }
                }




            }
        });
    }


    @Override
    public void onBackPressed() {
        Intent i = new Intent(this, MainActivity.class);
        startActivity(i);
        finish();
    }

    private class PageListener extends ViewPager.SimpleOnPageChangeListener {
        public void onPageSelected(int position) {
            currentPage = position;
            // title.setText(artikel.getObjects().get(position).getTitle());
            indikatorWrapper.removeAllViewsInLayout();
            for (int i = 0; i < artikel.getObjects().size(); i++) {
                View child = getLayoutInflater().inflate(R.layout.indikator_pager, null);
                View child2 = getLayoutInflater().inflate(R.layout.indikator_pager_blue, null);
                if (i == position) {
                    indikatorWrapper.addView(child2);
                } else {
                    indikatorWrapper.addView(child);
                }
            }

        }
    }


    private void setupFrae() {
        mPagerAdapter = new PagerAdapter(getSupportFragmentManager(),
                ArtikelPagerGrafikActivity.this, fragmentList);
        mPager.setAdapter(mPagerAdapter);

    }

    private void pjgTinggiThdUmur() {
        progresDialog(false, getResources().getString(R.string.MemuatGrafik));
        if (checkBoxs.get(0).isChecked()) {

            if(!preferences.getString(Preferences.USER_ONLINE, "").equalsIgnoreCase("false") || !preferences.getString(Preferences.ID, "").equalsIgnoreCase("")) {
                StringRequest listdata = new StringRequest(Request.Method.GET,
                        ApiReferences.generatePanjangTUmur(id),
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                Log.i("RESULT JSON", response.toString());
                                try {
                                    if (response.equals("[]")) {
                                        dialog(getResources().getString(R.string.GrafikBelumTersedia), getResources().getString(R.string.Perhatian));
                                    } else {
                                        Message msg = Parser.getMessage(response);
                                        if (msg.getStatus().equals("0")) {
                                            dialog(msg.getMessages(), getResources().getString(R.string.Perhatian));
                                        } else {
                                            Grafik graf = new Grafik();
                                            graf = Parser.getGrafik(response
                                                    .toString());
                                            if (graf.getStatus().equals("1")) {

                                                selected.add(graf.getObjects().getImage_url());
                                                interpretasi.add(graf.getObjects().getInterpretation());
                                                grafikName.add(getResources().getString(R.string.grafikHeightAge));
                                                label.add(graf.getObjects().getLabel());
                                                beratThdUmur();
                                            }
                                        }
                                    }
                                } catch (JsonSyntaxException e) {
                                    dialog.dismiss();
                                    dialog(getResources().getString(R.string.KesalahanServer), getResources().getString(R.string.Perhatian));
                                }
                            }
                        }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        dialog.dismiss();
                        dialog(getResources().getString(R.string.NoConnection), getResources().getString(R.string.Perhatian));

                    }
                });
                listdata.setRetryPolicy(new DefaultRetryPolicy(
                        20000,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                mRequest.add(listdata);
            }else{
                StringRequest listdata = new StringRequest(Request.Method.POST,
                        ApiReferences.generatePanjangTUmurOffline(),
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                Log.i("RESULT JSON", response.toString());
                                try {
                                    if (response.equals("[]")) {
                                        dialog(getResources().getString(R.string.GrafikBelumTersedia), getResources().getString(R.string.Perhatian));
                                    } else {
                                        Message msg = Parser.getMessage(response);
                                        if (msg.getStatus().equals("0")) {
                                            dialog(msg.getMessages(), getResources().getString(R.string.Perhatian));
                                        } else {
                                            Grafik graf = new Grafik();
                                            graf = Parser.getGrafik(response
                                                    .toString());
                                            if (graf.getStatus().equals("1")) {

                                                selected.add(graf.getObjects().getImage_url());
                                                interpretasi.add(graf.getObjects().getInterpretation());
                                                grafikName.add(getResources().getString(R.string.grafikHeightAge));
                                                label.add(graf.getObjects().getLabel());
                                                beratThdUmur();
                                            }
                                        }
                                    }
                                } catch (JsonSyntaxException e) {
                                    dialog.dismiss();
                                    dialog(getResources().getString(R.string.KesalahanServer), getResources().getString(R.string.Perhatian));
                                }
                            }
                        }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                            NetworkResponse response = error.networkResponse;
                            if (error instanceof ServerError && response != null) {
                                try {
                                    String res = new String(response.data,
                                            HttpHeaderParser.parseCharset(response.headers, "utf-8"));
                                    // Now you can use any deserializer to make sense of data
                                    JSONObject obj = new JSONObject(res);
                                    dialog(res.toString(), getResources().getString(R.string.Perhatian));
                                } catch (UnsupportedEncodingException e1) {
                                    // Couldn't properly decode data to string
                                    e1.printStackTrace();
                                } catch (JSONException e2) {
                                    // returned data is not JSONObject?
                                    e2.printStackTrace();
                                }
                            }


                        }
                    }) {

                    protected Map<String, String> getParams() {
                        HashMap<String, String> params = new HashMap<String, String>();
                        params.put("dob", preferences.getString(Preferences.BIRTH_DAY, ""));
                        params.put("gender", preferences.getString(Preferences.JENIS_KELAMIN, ""));
                        params.put("height", preferences.getString(Preferences.TINGGI, ""));
                        return params;

                    }
                };


                listdata.setRetryPolicy(new DefaultRetryPolicy(
                        20000,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                mRequest.add(listdata);
            }
        } else {
            beratThdUmur();
        }
    }

    private void beratThdUmur() {
        if (checkBoxs.get(1).isChecked()) {
            if(!preferences.getString(Preferences.USER_ONLINE, "").equalsIgnoreCase("false") || !preferences.getString(Preferences.ID, "").equalsIgnoreCase("")) {
                //  progresDialog(false, "Memuat Grafik");
                StringRequest listdata = new StringRequest(Request.Method.GET,
                        ApiReferences.generateBeratTUmur(id),
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                try {
                                    if (response.equals("[]")) {
                                        dialog(getResources().getString(R.string.GrafikBelumTersedia), getResources().getString(R.string.Perhatian));
                                    } else {
                                        Message msg = Parser.getMessage(response);
                                        if (msg.getStatus().equals("0")) {
                                            dialog(msg.getMessages(), getResources().getString(R.string.Perhatian));
                                        } else {
                                            Log.i("RESULT JSON", response.toString());
                                            Grafik graf = new Grafik();
                                            graf = Parser.getGrafik(response
                                                    .toString());
                                            if (graf.getStatus().equals("1")) {
                                                selected.add(graf.getObjects().getImage_url());
                                                interpretasi.add(graf.getObjects().getInterpretation());
                                                grafikName.add(getResources().getString(R.string.grafikWeightAge));
                                                label.add(graf.getObjects().getLabel());
                                                BeratThdPjgTinggi();
                                            }
                                        }
                                    }
                                } catch (JsonSyntaxException e) {
                                    dialog.dismiss();
                                    dialog(getResources().getString(R.string.KesalahanServer), getResources().getString(R.string.Perhatian));
                                }
                            }
                            //   dialog_sesi_offline_pelod.dismiss();

                        }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        dialog.dismiss();
                        dialog(getResources().getString(R.string.NoConnection), getResources().getString(R.string.Perhatian));
                    }
                });
                listdata.setRetryPolicy(new DefaultRetryPolicy(
                        20000,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                mRequest.add(listdata);
            }else {
                //  progresDialog(false, "Memuat Grafik");
                StringRequest listdata = new StringRequest(Request.Method.POST,
                        ApiReferences.generateBeratTUmurOffline(),
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                try {
                                    if (response.equals("[]")) {
                                        dialog(getResources().getString(R.string.GrafikBelumTersedia), getResources().getString(R.string.Perhatian));
                                    } else {
                                        Message msg = Parser.getMessage(response);
                                        if (msg.getStatus().equals("0")) {
                                            dialog(msg.getMessages(), getResources().getString(R.string.Perhatian));
                                        } else {
                                            Log.i("RESULT JSON", response.toString());
                                            Grafik graf = new Grafik();
                                            graf = Parser.getGrafik(response
                                                    .toString());
                                            if (graf.getStatus().equals("1")) {
                                                selected.add(graf.getObjects().getImage_url());
                                                interpretasi.add(graf.getObjects().getInterpretation());
                                                grafikName.add(getResources().getString(R.string.grafikWeightAge));
                                                label.add(graf.getObjects().getLabel());
                                                BeratThdPjgTinggi();
                                            }
                                        }
                                    }
                                } catch (JsonSyntaxException e) {
                                    dialog.dismiss();
                                    dialog(getResources().getString(R.string.KesalahanServer), getResources().getString(R.string.Perhatian));
                                }
                            }
                            //   dialog_sesi_offline_pelod.dismiss();

                        }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                            NetworkResponse response = error.networkResponse;
                            if (error instanceof ServerError && response != null) {
                                try {
                                    String res = new String(response.data,
                                            HttpHeaderParser.parseCharset(response.headers, "utf-8"));
                                    // Now you can use any deserializer to make sense of data
                                    JSONObject obj = new JSONObject(res);
                                    dialog(res.toString(), getResources().getString(R.string.Perhatian));
                                } catch (UnsupportedEncodingException e1) {
                                    // Couldn't properly decode data to string
                                    e1.printStackTrace();
                                } catch (JSONException e2) {
                                    // returned data is not JSONObject?
                                    e2.printStackTrace();
                                }
                            }


                        }
                    }) {

                        protected Map<String, String> getParams() {
                            HashMap<String, String> params = new HashMap<String, String>();
                            params.put("dob", preferences.getString(Preferences.BIRTH_DAY, ""));
                            params.put("gender", preferences.getString(Preferences.JENIS_KELAMIN, ""));
                            params.put("weight", preferences.getString(Preferences.BERAT, ""));
                            return params;

                        }
                    };
                listdata.setRetryPolicy(new DefaultRetryPolicy(
                        20000,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                mRequest.add(listdata);
            }
        } else {
            BeratThdPjgTinggi();
        }
    }

    private void BeratThdPjgTinggi() {
        if (checkBoxs.get(2).isChecked()) {
            if(!preferences.getString(Preferences.USER_ONLINE, "").equalsIgnoreCase("false") || !preferences.getString(Preferences.ID, "").equalsIgnoreCase("")) {


                String mom_height;
                String dad_height;
                if(preferences.getString(Preferences.JENIS_KELAMIN, "").equalsIgnoreCase("m")){
                    double temp_mom_height = ((((Double.parseDouble(preferences.getString(Preferences.MOM_HEIGHT, ""))  + 13 ) + Double.parseDouble(preferences.getString(Preferences.DAD_HEIGHT, "")) )/2) - 8.5);
                    double temp_dad_height = ((((Double.parseDouble(preferences.getString(Preferences.MOM_HEIGHT, ""))  + 13 ) + Double.parseDouble(preferences.getString(Preferences.DAD_HEIGHT, "")) )/2) + 8.5);

                    //TPG anak laki-laki = ((TB ibu (cm) + 13 cm) + TB ayah (cm))/2 ± 8,5 cm
                    mom_height = temp_mom_height+"";
                    dad_height = temp_dad_height+"";
                }else{
                    double temp_mom_height = ((((Double.parseDouble(preferences.getString(Preferences.DAD_HEIGHT, ""))  - 13 ) + Double.parseDouble(preferences.getString(Preferences.MOM_HEIGHT, "")) )/2) - 8.5);
                    double temp_dad_height = ((((Double.parseDouble(preferences.getString(Preferences.DAD_HEIGHT, "")) - 13 ) + Double.parseDouble(preferences.getString(Preferences.MOM_HEIGHT, "")) )/2) + 8.5);

                    //TPG anak perempuan = ((TB ayah (cm) - 13 cm) + TB ibu (cm))/2 ± 8,5 cm
                    mom_height = temp_mom_height+"";
                    dad_height = temp_dad_height+"";
                }

                editor.putString(Preferences.DAD_HEIGHT, dad_height);
                editor.putString(Preferences.MOM_HEIGHT, mom_height);
                editor.commit();

                //   progresDialog(false, "Memuat Grafik");
                StringRequest listdata = new StringRequest(Request.Method.GET,
                        ApiReferences.generateBeratTPanjangTinggi(id, mom_height, dad_height),
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                try {
                                    if (response.equals("[]")) {
                                        dialog(getResources().getString(R.string.GrafikBelumTersedia), getResources().getString(R.string.Perhatian));
                                    } else {
                                        Message msg = Parser.getMessage(response);
                                        if (msg.getStatus().equals("0")) {
                                            dialog(msg.getMessages(), getResources().getString(R.string.Perhatian));
                                        } else {
                                            Log.i("RESULT JSON", response.toString());
                                            Grafik graf = new Grafik();
                                            graf = Parser.getGrafik(response
                                                    .toString());
                                            if (graf.getStatus().equals("1")) {

                                                selected.add(graf.getObjects().getImage_url());
                                                interpretasi.add(graf.getObjects().getInterpretation());
                                                grafikName.add(getResources().getString(R.string.grafikWeightHeight));
                                                label.add(graf.getObjects().getLabel());
                                                imt();
                                            }
                                        }
                                    }
                                } catch (JsonSyntaxException e) {
                                    dialog.dismiss();
                                    dialog(getResources().getString(R.string.KesalahanServer), getResources().getString(R.string.Perhatian));
                                }
                            }
                        }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        dialog.dismiss();
                        dialog(getResources().getString(R.string.NoConnection), getResources().getString(R.string.Perhatian));
                    }
                });
                listdata.setRetryPolicy(new DefaultRetryPolicy(
                        20000,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                mRequest.add(listdata);
            }else{
                //   progresDialog(false, "Memuat Grafik");
                StringRequest listdata = new StringRequest(Request.Method.POST,
                        ApiReferences.generateBeratTPanjangTinggiOffline(),
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                try {
                                    if (response.equals("[]")) {
                                        dialog(getResources().getString(R.string.GrafikBelumTersedia), getResources().getString(R.string.Perhatian));
                                    } else {
                                        Message msg = Parser.getMessage(response);
                                        if (msg.getStatus().equals("0")) {
                                            dialog(msg.getMessages(), getResources().getString(R.string.Perhatian));
                                        } else {
                                            Log.i("RESULT JSON", response.toString());
                                            Grafik graf = new Grafik();
                                            graf = Parser.getGrafik(response
                                                    .toString());
                                            if (graf.getStatus().equals("1")) {

                                                selected.add(graf.getObjects().getImage_url());
                                                interpretasi.add(graf.getObjects().getInterpretation());
                                                grafikName.add(getResources().getString(R.string.grafikWeightHeight));
                                                label.add(graf.getObjects().getLabel());
                                                imt();
                                            }
                                        }
                                    }
                                } catch (JsonSyntaxException e) {
                                    dialog.dismiss();
                                    dialog(getResources().getString(R.string.KesalahanServer), getResources().getString(R.string.Perhatian));
                                }
                            }
                        }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                            NetworkResponse response = error.networkResponse;
                            if (error instanceof ServerError && response != null) {
                                try {
                                    String res = new String(response.data,
                                            HttpHeaderParser.parseCharset(response.headers, "utf-8"));
                                    // Now you can use any deserializer to make sense of data
                                    JSONObject obj = new JSONObject(res);
                                    dialog(res.toString(), getResources().getString(R.string.Perhatian));
                                } catch (UnsupportedEncodingException e1) {
                                    // Couldn't properly decode data to string
                                    e1.printStackTrace();
                                } catch (JSONException e2) {
                                    // returned data is not JSONObject?
                                    e2.printStackTrace();
                                }
                            }


                        }
                    }) {

                        protected Map<String, String> getParams() {
                            HashMap<String, String> params = new HashMap<String, String>();
                            params.put("dob", preferences.getString(Preferences.BIRTH_DAY, ""));
                            params.put("gender", preferences.getString(Preferences.JENIS_KELAMIN, ""));
                            params.put("weight", preferences.getString(Preferences.BERAT, ""));
                            params.put("height", preferences.getString(Preferences.TINGGI, ""));


                            String mom_height;
                            String dad_height;
                            if(preferences.getString(Preferences.JENIS_KELAMIN, "").equalsIgnoreCase("m")){
                                double temp_mom_height = ((((Double.parseDouble(preferences.getString(Preferences.MOM_HEIGHT, ""))  + 13 ) + Double.parseDouble(preferences.getString(Preferences.DAD_HEIGHT, "")) )/2) - 8.5);
                                double temp_dad_height = ((((Double.parseDouble(preferences.getString(Preferences.MOM_HEIGHT, ""))  + 13 ) + Double.parseDouble(preferences.getString(Preferences.DAD_HEIGHT, "")) )/2) + 8.5);

                                mom_height = temp_mom_height+"";
                                dad_height = temp_dad_height+"";
                            }else{
                                double temp_mom_height = ((((Double.parseDouble(preferences.getString(Preferences.DAD_HEIGHT, ""))  - 13 ) + Double.parseDouble(preferences.getString(Preferences.MOM_HEIGHT, "")) )/2) - 8.5);
                                double temp_dad_height = ((((Double.parseDouble(preferences.getString(Preferences.DAD_HEIGHT, "")) - 13 ) + Double.parseDouble(preferences.getString(Preferences.MOM_HEIGHT, "")) )/2) + 8.5);

                                mom_height = temp_mom_height+"";
                                dad_height = temp_dad_height+"";
                            }

                            params.put("mom_height", mom_height+"");
                            params.put("dad_height", dad_height+"");

                            editor.putString(Preferences.DAD_HEIGHT, dad_height);
                            editor.putString(Preferences.MOM_HEIGHT, mom_height);
                            editor.commit();

                            return params;

                        }
                    };
                listdata.setRetryPolicy(new DefaultRetryPolicy(
                        20000,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                mRequest.add(listdata);
            }
        } else {
            imt();
        }
    }

    private void imt() {
        if (checkBoxs.get(3).isChecked()) {
            if(!preferences.getString(Preferences.USER_ONLINE, "").equalsIgnoreCase("false") || !preferences.getString(Preferences.ID, "").equalsIgnoreCase("")) {

                // progresDialog(false, "Memuat Grafik");
                StringRequest listdata = new StringRequest(Request.Method.GET,
                        ApiReferences.generateIMTTUmur(id),
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                try {
                                    if (response.equals("[]")) {
                                        dialog(getResources().getString(R.string.GrafikBelumTersedia), getResources().getString(R.string.Perhatian));
                                    } else {
                                        Message msg = Parser.getMessage(response);
                                        if (msg.getStatus().equals("0")) {
                                            dialog(msg.getMessages(), getResources().getString(R.string.Perhatian));
                                        } else {
                                            Log.i("RESULT JSON", response.toString());
                                            Grafik graf = new Grafik();
                                            graf = Parser.getGrafik(response
                                                    .toString());
                                            if (graf.getStatus().equals("1")) {
                                                selected.add(graf.getObjects().getImage_url());
                                                interpretasi.add(graf.getObjects().getInterpretation());
                                                grafikName.add(getResources().getString(R.string.grafikBodyMassIndex));
                                                label.add(graf.getObjects().getLabel());
                                                lingkarKepala();
                                            }
                                        }
                                    }
                                } catch (JsonSyntaxException e) {
                                    dialog.dismiss();
                                    dialog(getResources().getString(R.string.KesalahanServer), getResources().getString(R.string.Perhatian));
                                }
                            }

                        }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        dialog.dismiss();
                        dialog(getResources().getString(R.string.NoConnection), getResources().getString(R.string.Perhatian));
                    }
                });
                listdata.setRetryPolicy(new DefaultRetryPolicy(
                        20000,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                mRequest.add(listdata);
            }else{

                // progresDialog(false, "Memuat Grafik");
                StringRequest listdata = new StringRequest(Request.Method.POST,
                        ApiReferences.generateIMTTUmurOffline(),
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                try {
                                    if (response.equals("[]")) {
                                        dialog(getResources().getString(R.string.GrafikBelumTersedia), getResources().getString(R.string.Perhatian));
                                    } else {
                                        Message msg = Parser.getMessage(response);
                                        if (msg.getStatus().equals("0")) {
                                            dialog(msg.getMessages(), getResources().getString(R.string.Perhatian));
                                        } else {
                                            Log.i("RESULT JSON", response.toString());
                                            Grafik graf = new Grafik();
                                            graf = Parser.getGrafik(response
                                                    .toString());
                                            if (graf.getStatus().equals("1")) {
                                                selected.add(graf.getObjects().getImage_url());
                                                interpretasi.add(graf.getObjects().getInterpretation());
                                                grafikName.add(getResources().getString(R.string.grafikBodyMassIndex));
                                                label.add(graf.getObjects().getLabel());
                                                lingkarKepala();
                                            }
                                        }
                                    }
                                } catch (JsonSyntaxException e) {
                                    dialog.dismiss();
                                    dialog(getResources().getString(R.string.KesalahanServer), getResources().getString(R.string.Perhatian));
                                }
                            }

                        }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                            NetworkResponse response = error.networkResponse;
                            if (error instanceof ServerError && response != null) {
                                try {
                                    String res = new String(response.data,
                                            HttpHeaderParser.parseCharset(response.headers, "utf-8"));
                                    // Now you can use any deserializer to make sense of data
                                    JSONObject obj = new JSONObject(res);
                                    dialog(res.toString(), getResources().getString(R.string.Perhatian));
                                } catch (UnsupportedEncodingException e1) {
                                    // Couldn't properly decode data to string
                                    e1.printStackTrace();
                                } catch (JSONException e2) {
                                    // returned data is not JSONObject?
                                    e2.printStackTrace();
                                }
                            }


                        }
                    }) {

                        protected Map<String, String> getParams() {
                            HashMap<String, String> params = new HashMap<String, String>();
                            params.put("dob", preferences.getString(Preferences.BIRTH_DAY, ""));
                            params.put("gender", preferences.getString(Preferences.JENIS_KELAMIN, ""));
                            params.put("weight", preferences.getString(Preferences.BERAT, ""));
                            params.put("height", preferences.getString(Preferences.TINGGI, ""));
                            return params;

                        }
                    };
                listdata.setRetryPolicy(new DefaultRetryPolicy(
                        20000,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                mRequest.add(listdata);
            }
        } else {
            lingkarKepala();
        }
    }

    private void lingkarKepala() {

        System.out.println("nandha1");
        //  progresDialog(false, "Memuat Grafik");
        final String lingkarKepala = preferences.getString(Preferences.LINGKAR_KEPALA, "");
        if (checkBoxs.get(4).isChecked()) {

            System.out.println("nandha2"+id);
            //if(!preferences.getString(Preferences.USER_ONLINE, "").equalsIgnoreCase("false")) {
            if (!preferences.getString(Preferences.ID, "").equals("")) {
                System.out.println("nandha3");

                System.out.println("nandha4");
                StringRequest listdata = new StringRequest(Request.Method.GET,
                        ApiReferences.generateLingkarKepalaTUmur(id, lingkarKepala),
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                dialog.dismiss();
                                try {
                                    if (response.equals("[]")) {
                                        dialog(getResources().getString(R.string.GrafikBelumTersedia), getResources().getString(R.string.Perhatian));
                                    } else {
                                        Message msg = Parser.getMessage(response);
                                        if (msg.getStatus().equals("0")) {
                                            dialog(msg.getMessages(), getResources().getString(R.string.Perhatian));
                                        } else {
                                            Log.i("RESULT JSON", response.toString());
                                            Grafik graf = new Grafik();
                                            graf = Parser.getGrafik(response
                                                    .toString());
                                            if (graf.getStatus().equals("1")) {
                                                selected.add(graf.getObjects().getImage_url());
                                                interpretasi.add(graf.getObjects().getInterpretation());
                                                label.add(graf.getObjects().getLabel());
                                                grafikName.add(getResources().getString(R.string.grafikLingkarKepalaUmur));
                                            }
                                            dialog.dismiss();
                                            dialog2.dismiss();

                                            Intent i = new Intent(ArtikelPagerGrafikActivity.this, GrafikPertumbuhanResultActivity.class);
                                            i.putStringArrayListExtra("page", selected);
                                            i.putStringArrayListExtra("title", grafikName);
                                            i.putStringArrayListExtra("interpretasi", interpretasi);
                                            i.putStringArrayListExtra("label", label);
                                            i.putExtra("result", result);
                                            startActivity(i);
                                            //    finish();
                                        }
                                    }
                                } catch (JsonSyntaxException e) {
                                    dialog.dismiss();
                                    dialog(getResources().getString(R.string.KesalahanServer), getResources().getString(R.string.Perhatian));
                                }
                            }
                        }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        dialog.dismiss();
                        dialog(getResources().getString(R.string.NoConnection), getResources().getString(R.string.Perhatian));
                    }
                });
                listdata.setRetryPolicy(new DefaultRetryPolicy(
                        20000,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                mRequest.add(listdata);
            }else{
                String uri;
                System.out.println("nandha5");

                uri = ApiReferences.generateLingkarKepalaTUmurOffline();

                StringRequest listdata = new StringRequest(Request.Method.POST,
                        uri,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                dialog.dismiss();
                                try {
                                    System.out.println("nandha9");
                                    if (response.equals("[]")) {
                                        dialog(getResources().getString(R.string.GrafikBelumTersedia), getResources().getString(R.string.Perhatian));
                                    } else {
                                        Message msg = Parser.getMessage(response);
                                        if (msg.getStatus().equals("0")) {
                                            dialog(msg.getMessages(), getResources().getString(R.string.Perhatian));
                                        } else {
                                            Log.i("RESULT JSON", response.toString());
                                            Grafik graf = new Grafik();
                                            graf = Parser.getGrafik(response
                                                    .toString());
                                            if (graf.getStatus().equals("1")) {
                                                selected.add(graf.getObjects().getImage_url());
                                                interpretasi.add(graf.getObjects().getInterpretation());
                                                label.add(graf.getObjects().getLabel());
                                                grafikName.add(getResources().getString(R.string.grafikLingkarKepalaUmur));
                                            }
                                            dialog.dismiss();
                                            dialog2.dismiss();
                                            Intent i = new Intent(ArtikelPagerGrafikActivity.this, GrafikPertumbuhanResultActivity.class);
                                            i.putStringArrayListExtra("page", selected);
                                            i.putStringArrayListExtra("title", grafikName);
                                            i.putStringArrayListExtra("interpretasi", interpretasi);
                                            i.putStringArrayListExtra("label", label);
                                            i.putExtra("result", result);
                                            startActivity(i);
                                            //    finish();
                                        }
                                    }
                                } catch (JsonSyntaxException e) {
                                    dialog.dismiss();
                                    dialog(getResources().getString(R.string.KesalahanServer), getResources().getString(R.string.Perhatian));
                                }
                            }
                        }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        NetworkResponse response = error.networkResponse;
                        if (error instanceof ServerError && response != null) {
                            try {
                                String res = new String(response.data,
                                        HttpHeaderParser.parseCharset(response.headers, "utf-8"));
                                // Now you can use any deserializer to make sense of data
                                JSONObject obj = new JSONObject(res);
                                dialog(res.toString(), getResources().getString(R.string.Perhatian));
                            } catch (UnsupportedEncodingException e1) {
                                // Couldn't properly decode data to string
                                e1.printStackTrace();
                            } catch (JSONException e2) {
                                // returned data is not JSONObject?
                                e2.printStackTrace();
                            }
                        }


                    }
                }) {

                    protected Map<String, String> getParams() {
                        HashMap<String, String> params = new HashMap<String, String>();
                        params.put("dob", preferences.getString(Preferences.BIRTH_DAY, ""));
                        params.put("gender", preferences.getString(Preferences.JENIS_KELAMIN, ""));
                        params.put("head_circumference", lingkarKepala);
                        return params;

                    }
                };
                listdata.setRetryPolicy(new DefaultRetryPolicy(
                        20000,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                mRequest.add(listdata);

            }
        } else {
            dialog.dismiss();
            dialog2.dismiss();
            Intent i = new Intent(this, GrafikPertumbuhanResultActivity.class);
            i.putStringArrayListExtra("page", selected);
            i.putStringArrayListExtra("title", grafikName);
            i.putStringArrayListExtra("interpretasi", interpretasi);
            i.putStringArrayListExtra("label", label);
            i.putExtra("result", result);
            startActivity(i);
            //  finish();
        }
    }


    private void saveSesiPasien(final String jenis_kelamin, final String tanggal_lahir, final String berat, final String tinggi) {
        progresDialog(false, getResources().getString(R.string.MenyimpanDataPasien));
        StringRequest postlogin = new StringRequest(Request.Method.POST,
                ApiReferences.insertPasien(), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                dialog.dismiss();
                try {
                    Log.i("RESULT JSON", response.toString());
                    Pasien pasien = Parser.getPasien(response
                            .toString());
                    if (pasien.getStatus().equalsIgnoreCase("1")) {
                        editor.putString(Preferences.ID, pasien.getObjects().getId());
                        editor.putString(Preferences.BERAT, pasien.getObjects().getBerat());
                        editor.putString(Preferences.TINGGI, pasien.getObjects().getTinggi());
                        editor.putString(Preferences.JENIS_KELAMIN, pasien.getObjects().getJenis_kelamin());
                        editor.putString(Preferences.BIRTH_DAY, pasien.getObjects().getTanggal_lahir());
                        editor.commit();
                        id = preferences.getString(Preferences.ID, "");
                        dialogEmail("Kode : " + pasien.getObjects().getId() + "\nSimpan Code ini untuk pemeriksaan berikutnya.", "Pendaftaran Sesi Pasien Berhasil", pasien.getObjects().getId());
                    }
                } catch (JsonSyntaxException e) {
                    dialog(getResources().getString(R.string.KesalahanServer), getResources().getString(R.string.Perhatian));
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                dialog.dismiss();
                dialog(getResources().getString(R.string.NoConnection), getResources().getString(R.string.Perhatian));
            }
        }) {

            protected Map<String, String> getParams() {
                HashMap<String, String> params = new HashMap<String, String>();
                params.put("jenis_kelamin", jenis_kelamin);
                params.put("tanggal_lahir", tanggal_lahir);
                params.put("berat", berat);
                params.put("tinggi", tinggi);
                return params;
            }
        };
        postlogin.setRetryPolicy(new DefaultRetryPolicy(
                20000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        mRequest.add(postlogin);
    }


    private void saveSesiPasienOffline(final String jenis_kelamin, final String tanggal_lahir, final String berat, final String tinggi) {

        editor.putString(Preferences.BERAT, berat);
        editor.putString(Preferences.TINGGI, tinggi);
        editor.putString(Preferences.JENIS_KELAMIN, jenis_kelamin);
        editor.putString(Preferences.BIRTH_DAY, tanggal_lahir);
        editor.putString(Preferences.USER_ONLINE, "false");
        editor.commit();
        id = preferences.getString(Preferences.ID, "");


        showDialog();
    }

    public void dialogEmail(String content, String msg, final String kode) {
        new MaterialDialog.Builder(this)
                .title(msg)
                .content(content)
                .autoDismiss(false)
                .cancelable(false)
                .positiveText(getResources().getString(R.string.KirimEmail))
                .negativeText(getResources().getString(R.string.tutup))
                .callback(new MaterialDialog.ButtonCallback() {
                    @Override
                    public void onPositive(MaterialDialog dialog) {
                        Intent sendEmail = new Intent(Intent.ACTION_SEND);
                        sendEmail.setType("text/plain");
                        sendEmail.putExtra(Intent.EXTRA_EMAIL, new String[]{""});
                        sendEmail.putExtra(Intent.EXTRA_SUBJECT, "Kode Pasien");
                        sendEmail.putExtra(Intent.EXTRA_TEXT, "Berikut ini adalah kode pasien anda.\nKode : " + kode);
                        startActivity(Intent.createChooser(sendEmail, "Email With"));
                    }

                    @Override
                    public void onNegative(MaterialDialog dialog) {
                        dialog.dismiss();
                        showDialog();
                    }
                })
                .show();
    }

    private void showDatepickerDialog(final TextView txt) {
        dateHelper.showDatePicker(this, System.currentTimeMillis(), new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                int _year = year;
                int _month = monthOfYear + 1;
                int _day = dayOfMonth;
                String date = _year + "-" + _month + "-" + _day;
                String birthDate = dateHelper.formatDate(date);
                txt.setText(birthDate);
            }
        });
    }

    private void progresDialog(boolean horizontal, String msg) {
        dialog = new MaterialDialog.Builder(this)
                .title(msg)
                .content(getResources().getString(R.string.MohonTunggu))
                .progress(true, 0)
                .progressIndeterminateStyle(horizontal)
                .cancelable(false)
                .show();
    }

    @Override
    protected void onDestroy() {
        try {
            if (dialog.isShowing()) {
                dialog.cancel();
            }
        } catch (NullPointerException e) {

        }
        super.onDestroy();
    }


    public void dialog(String content, String msg) {
        new MaterialDialog.Builder(this)
                .title(msg)
                .content(content)
                .positiveText("Ok")
                .callback(new MaterialDialog.ButtonCallback() {
                    @Override
                    public void onPositive(MaterialDialog dialog) {
                    }

                })
                .show();
    }

}
