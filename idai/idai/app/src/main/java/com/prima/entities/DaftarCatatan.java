package com.prima.entities;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Codelabs on 09/09/2015.
 */
public class DaftarCatatan extends Message implements Serializable {
    private ArrayList<Catatan> data;
    public ArrayList<Catatan> getObjects() {
        return data;
    }
    public void setObjects(ArrayList<Catatan> objects) {
        this.data = objects;
    }


    public class GetCatatan extends Message {
        private Catatan objects;

        public Catatan getObjects() {
            return objects;
        }

        public void setObjects(Catatan objects) {
            this.objects = objects;
        }
    }

    public class Catatan {
        private String title;
        private String description;
        private String date_time;
        private String photo;




        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getDate_time() { return date_time; }

        public void setDate_time(String date_time) {
            this.date_time = date_time;
        }

        public String getPhoto() {
            return photo;
        }

        public void setPhoto(String photo) {
            this.photo = photo;
        }

    }
}