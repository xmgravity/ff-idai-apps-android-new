package com.prima.entities;

import java.util.ArrayList;

/**
 * Created by Codelabs on 14/09/2015.
 */
public class Agenda extends Message{
    private ArrayList<Agendas> data;

    public ArrayList<Agendas> getObjects() {
        return data;
    }

    public void setObjects(ArrayList<Agendas> objects) {
        this.data = objects;
    }

    public class Agendas {
        private String id;
        private String id_profil;
        private String sub_judul;
        private String jam_mulai;
        private String jam_akhir;
        private String lokasi;
        private String pengingat;
        private String suara;
        private String getar;
        private String tanggal_kunjungan;
        private String nama_pasien;
        private String latitude;
        private String longitude;
        private String id_kalender;
        private String id_agenda;
        private Agendas objects;
        private ArrayList<Agendas> jadwal;

        public String getIs_notif() {
            return is_notif;
        }

        public void setIs_notif(String is_notif) {
            this.is_notif = is_notif;
        }

        private String is_notif;

        public ArrayList<Agendas> getJadwal() {
            return jadwal;
        }

        public void setJadwal(ArrayList<Agendas> jadwal) {
            this.jadwal = jadwal;
        }

        public Agendas getObjects() {
            return objects;
        }

        public void setObjects(Agendas objects) {
            this.objects = objects;
        }

        public String getId_kalender() {
            return id_kalender;
        }

        public void setId_kalender(String id_kalender) {
            this.id_kalender = id_kalender;
        }

        public String getId_agenda() {
            return id_agenda;
        }

        public void setId_agenda(String id_agenda) {
            this.id_agenda = id_agenda;
        }

        public String getNama_pasien() {
            return nama_pasien;
        }

        public void setNama_pasien(String nama_pasien) {
            this.nama_pasien = nama_pasien;
        }

        public String getLatitude() {
            return latitude;
        }

        public void setLatitude(String latitude) {
            this.latitude = latitude;
        }

        public String getLongitude() {
            return longitude;
        }

        public void setLongitude(String longitude) {
            this.longitude = longitude;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getId_profil() {
            return id_profil;
        }

        public void setId_profil(String id_profil) {
            this.id_profil = id_profil;
        }

        public String getSub_judul() {
            return sub_judul;
        }

        public void setSub_judul(String sub_judul) {
            this.sub_judul = sub_judul;
        }

        public String getJam_mulai() {
            return jam_mulai;
        }

        public void setJam_mulai(String jam_mulai) {
            this.jam_mulai = jam_mulai;
        }

        public String getJam_akhir() {
            return jam_akhir;
        }

        public void setJam_akhir(String jam_akhir) {
            this.jam_akhir = jam_akhir;
        }

        public String getLokasi() {
            return lokasi;
        }

        public void setLokasi(String lokasi) {
            this.lokasi = lokasi;
        }

        public String getPengingat() {
            return pengingat;
        }

        public void setPengingat(String pengingat) {
            this.pengingat = pengingat;
        }

        public String getSuara() {
            return suara;
        }

        public void setSuara(String suara) {
            this.suara = suara;
        }

        public String getGetar() {
            return getar;
        }

        public void setGetar(String getar) {
            this.getar = getar;
        }

        public String getTanggal_kunjungan() {
            return tanggal_kunjungan;
        }

        public void setTanggal_kunjungan(String tanggal_kunjungan) {
            this.tanggal_kunjungan = tanggal_kunjungan;
        }
    }
}
