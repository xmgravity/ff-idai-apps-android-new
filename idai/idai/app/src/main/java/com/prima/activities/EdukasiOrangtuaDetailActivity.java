package com.prima.activities;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import prima.test.prima.R;

/**
 * Created by Codelabs on 14/08/2015.
 */
public class EdukasiOrangtuaDetailActivity extends BaseActivity implements View.OnClickListener {

    Button lanjut;
    TextView title;
    TextView judul;
    Bundle bn;
    String respon;

    @Override
    public void initView() {
        lanjut = (Button) findViewById(R.id.lanjut);
        judul = (TextView) findViewById(R.id.judul);
        title = (TextView) findViewById(R.id.title);
        //set font
        Typeface tf = Typeface.createFromAsset(getApplicationContext().getAssets(), "Brevia-ExtraBlackItalic.otf");
        title.setTypeface(tf);
    }

    @Override
    public void setUICallbacks() {
        lanjut.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        if (v == lanjut) {
            Intent i = new Intent(getApplicationContext(), EdukasiOrangtuaMateriActivity.class);
            i.putExtra("respon", respon);
            startActivity(i);
            finish();
        }
    }


    @Override
    public int getLayout() {
        return R.layout.activity_edukasi_orangtua_detail;
    }

    @Override
    public void updateUI() {
        bn = getIntent().getExtras();
        if (bn != null) {
            respon = bn.getString("respon");
            String jdl = bn.getString("judul");
            judul.setText(getResources().getString(R.string.MateriEdukasiAnak2)+jdl);
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

    }


    @Override
    public void onBackPressed() {
        //   Intent i = new Intent(getApplicationContext(), EdukasiOrangtuaActivity.class);
        //   startActivity(i);
        finish();
    }
}