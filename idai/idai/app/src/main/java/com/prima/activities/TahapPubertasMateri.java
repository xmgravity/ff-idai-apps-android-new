package com.prima.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

import java.util.ArrayList;
import java.util.List;

import com.prima.Preferences;
import com.prima.adapters.PagerAdapter;
import com.prima.controllers.Parser;
import com.prima.entities.Pubertas;
import com.prima.fragments.TahapPubertasFragment;
import com.prima.helpers.NonSwipeAbleViewPager;
import prima.test.prima.R;

/**
 * Created by Codelabs on 11/08/2015.
 */
public class TahapPubertasMateri extends BaseActivity implements View.OnClickListener {
    private Context mContext;
    private NonSwipeAbleViewPager mPager;
    private PagerAdapter mPagerAdapter;
    private static int currentPage;
    PageListener pageListener;

    static LinearLayout indikatorWrapper1;
    static LinearLayout indikatorWrapper2;
    public Pubertas pubertas;
    private RequestQueue mRequest;
    MaterialDialog dialog;
    TextView title;
    private static List<Fragment> fragmentList = new ArrayList<Fragment>();

    public static List<Fragment> getFragmentList() {
        return fragmentList;
    }

    public static void setFragmentList(List<Fragment> fragmentList) {
        TahapPubertasMateri.fragmentList = fragmentList;
    }


    private ArrayList<String> ansewer = new ArrayList<String>();

    public ArrayList<String> getAnsewer() {
        return ansewer;
    }

    public void setAnsewer(ArrayList<String> ansewer) {
        this.ansewer = ansewer;
    }

    ArrayList<Pubertas.PubertasMaterei> item = new ArrayList<Pubertas.PubertasMaterei>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = getApplicationContext();
        mRequest = Volley.newRequestQueue(getApplicationContext());
        getMateri();
    }

    @Override
    public void initView() {
        title = (TextView) findViewById(R.id.title);
        indikatorWrapper1 = (LinearLayout) findViewById(R.id.indicator_atas);
        indikatorWrapper2 = (LinearLayout) findViewById(R.id.indicator_bawah);
        indikatorWrapper1.setVisibility(View.GONE);
        mPager = (NonSwipeAbleViewPager) findViewById(R.id.pager);
        pageListener = new PageListener();
        mPager.setOnPageChangeListener(pageListener);

    }

    @Override
    public void setUICallbacks() {
    }


    @Override
    public void onClick(View v) {
    }


    @Override
    public int getLayout() {
        return R.layout.activity_tahap_pubertas_materi;
    }

    @Override
    public void updateUI() {
        mPager.setCurrentItem(0);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

    }


    @Override
    public void onBackPressed() {
        if (currentPage > 0) {
            currentPage--;
            mPager.setCurrentItem(currentPage, true);
        } else {
            if (preferences.getString(Preferences.ID,"").equals("1")) {
                editor.putString(Preferences.BIRTH_DAY, "");
                editor.putString(Preferences.JENIS_KELAMIN, "");
                editor.putString(Preferences.BERAT, "");
                editor.putString(Preferences.TINGGI, "");
                editor.putString(Preferences.ID, "");
                editor.commit();
            }
            //  Intent i = new Intent(getApplicationContext(), TahapPubertasActivity.class);
            //   startActivity(i);
            finish();
        }
    }

    private class PageListener extends ViewPager.SimpleOnPageChangeListener {
        public void onPageSelected(int position) {
            if (position > 1) {
                title.setText(item.get(0).getObject().get(0).getHeader());
            }
            currentPage = position;
            indikatorWrapper1.removeAllViewsInLayout();
            indikatorWrapper2.removeAllViewsInLayout();
            fragmentList.clear();
            for (int i = 0; i < item.size(); i++) {
                fragmentList.add(new TahapPubertasFragment(item.get(i).getObject(),i));
            }
            for (int j = 0; j < item.get(0).getObject().size(); j++) {
                View child = getLayoutInflater().inflate(R.layout.indikator_pager, null);
                View child2 = getLayoutInflater().inflate(R.layout.indikator_pager_blue, null);
                View child3 = getLayoutInflater().inflate(R.layout.indikator_pager, null);
                View child4 = getLayoutInflater().inflate(R.layout.indikator_pager_blue, null);
                ansewer.add("");

                if (j == position) {
                    indikatorWrapper1.addView(child2);
                    indikatorWrapper2.addView(child4);
                } else {
                    indikatorWrapper1.addView(child);
                    indikatorWrapper2.addView(child3);
                }
            }
            mPagerAdapter.notifyDataSetChanged();
        }
    }


    private void setupFrae() {
        mPagerAdapter = new PagerAdapter(getSupportFragmentManager(),
                TahapPubertasMateri.this, fragmentList);
        mPager.setAdapter(mPagerAdapter);

    }


    private void getMateri() {
        Bundle bn = getIntent().getExtras();
        if (bn != null) {
            String response = bn.getString("respon");
            setupFrae();
            Log.i("RESULT JSON NANDHA 1", response);
            pubertas = new Pubertas();
            pubertas = Parser.getPubertas(response);

            for (int i = 0; i < pubertas.getObjects().size(); i++) {
                if (pubertas.getObjects().get(i).getType().equals("f") && preferences.getString(Preferences.JENIS_KELAMIN, "").equalsIgnoreCase("f")) {
                    item.add(pubertas.getObjects().get(i));
                    ansewer.add("");
                }
            }

            for (int i = 0; i < pubertas.getObjects().size(); i++) {
                if (pubertas.getObjects().get(i).getType().equals("m") && preferences.getString(Preferences.JENIS_KELAMIN, "").equalsIgnoreCase("m")) {
                    item.add(pubertas.getObjects().get(i));
                    ansewer.add("");
                }
            }

            Log.i("RESULT JSON NANDHA 2", item.get(0).getObject().get(0).getTitle());
            Log.i("RESULT NANDHA 1", item.get(0).toString());
            Log.i("RESULT NANDHA 2", item.toString());


            indikatorWrapper1.removeAllViewsInLayout();
            indikatorWrapper2.removeAllViewsInLayout();
            fragmentList.clear();
            title.setText(item.get(0).getObject().get(0).getHeader());
            for (int i = 0; i < item.size(); i++) {
                    fragmentList.add(new TahapPubertasFragment(item.get(i).getObject(),i));
                    View child = getLayoutInflater().inflate(R.layout.indikator_pager, null);
                    View child2 = getLayoutInflater().inflate(R.layout.indikator_pager_blue, null);
                    View child3 = getLayoutInflater().inflate(R.layout.indikator_pager, null);
                    View child4 = getLayoutInflater().inflate(R.layout.indikator_pager_blue, null);
                    if (i == 0 ) {
                        indikatorWrapper1.addView(child2);
                        indikatorWrapper2.addView(child4);
                    } else {
                        indikatorWrapper1.addView(child);
                        indikatorWrapper2.addView(child3);
                    }
            }
            mPagerAdapter.notifyDataSetChanged();

        }
    }

}