package com.prima.activities;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.joanzapata.pdfview.PDFView;
import com.joanzapata.pdfview.listener.OnPageChangeListener;

import com.prima.Preferences;
import prima.test.prima.R;

/**
 * Created by Codelabs on 13/08/2015.
 */
public class MchatrPdfActivity extends BaseActivity implements View.OnClickListener {
    private ImageView close;
    private ImageView back;
    private Context mContext;
    private Button email;
    TextView title;
    private PDFView pdfView;
    String url;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = getApplicationContext();


    }

    @Override
    public void initView() {
        back = (ImageView) findViewById(R.id.back);
        close = (ImageView) findViewById(R.id.close);
        pdfView = (PDFView) findViewById(R.id.pdfview);
        email = (Button) findViewById(R.id.email);
        title = (TextView) findViewById(R.id.tv_actionbar_title);
        Typeface tf = Typeface.createFromAsset(getApplicationContext().getAssets(), "BREVIA-BLACK.OTF");
        title.setTypeface(tf);
        title.setText("M-CHAT R");



        pdfView.fromAsset("Stimulasi  pada bayi umur 0-3 bulan.pdf")
                .defaultPage(1)
                .showMinimap(false)
                .enableSwipe(true)
                .onPageChange(new OnPageChangeListener() {
                    @Override
                    public void onPageChanged(int i, int i1) {

                    }
                })
                .load();

    }

    @Override
    public void setUICallbacks() {
        back.setOnClickListener(this);
        close.setOnClickListener(this);
        email.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        if (v == back) {
            Intent i = new Intent(getApplicationContext(), MchatActivity.class);
            startActivity(i);
            finish();
        } else if (v == close) {
            Intent i = new Intent(getApplicationContext(), MchatActivity.class);
            startActivity(i);
            finish();
        } else if (v == email) {
            String badanEmail;
            String jk;
            if (preferences.getString(Preferences.ID, "").equalsIgnoreCase("") || preferences.getString(Preferences.JENIS_KELAMIN, "").equalsIgnoreCase("")) {
                badanEmail = "";
            } else {
                if (preferences.getString(Preferences.JENIS_KELAMIN, "").equals("0")) {
                    jk = "Perempuan";
                } else {
                    jk = "Laki-Laki";
                }

                badanEmail = "Kode Pasien : " + preferences.getString(Preferences.ID, "") +
                        "\nTanggal Lahir : " + preferences.getString(Preferences.BIRTH_DAY, "") +
                        "\nBerat Badan : " + preferences.getString(Preferences.BERAT, "") +
                        "\nTinggi Badan : " + preferences.getString(Preferences.TINGGI, "") +
                        "\nJenis Kelamin : " + jk;
            }
            Intent sendEmail = new Intent(Intent.ACTION_SEND);
            sendEmail.setType("application/pdf");
            sendEmail.putExtra(Intent.EXTRA_EMAIL, new String[]{""});
            sendEmail.putExtra(Intent.EXTRA_SUBJECT, "Lampiran Mchart");
            sendEmail.putExtra(Intent.EXTRA_TEXT, "Berikut ini adalah lampiran Mchart\n\n"+badanEmail);
            sendEmail.putExtra(Intent.EXTRA_STREAM, Uri.parse("file:///sdcard/tutorial/Image.jpg"));
            startActivity(Intent.createChooser(sendEmail, "Email With"));
        }
    }


    @Override
    public int getLayout() {
        return R.layout.activity_mchat_r_pdf;
    }

    @Override
    public void updateUI() {

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

    }


    @Override
    public void onBackPressed() {
        Intent i = new Intent(getApplicationContext(), MchatActivity.class);
        startActivity(i);
        finish();
    }

}
