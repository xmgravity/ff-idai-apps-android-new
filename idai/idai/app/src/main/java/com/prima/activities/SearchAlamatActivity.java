package com.prima.activities;

import android.content.Context;
import android.content.Intent;
import android.graphics.Point;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.VisibleRegion;

import com.prima.adapters.AdapterListAlamat;
import com.prima.controllers.ApiReferences;
import com.prima.controllers.Parser;
import com.prima.entities.Alamat;
import com.prima.helpers.GPSTracker;
import prima.test.prima.R;

/**
 * Created by Codelabs on 20/08/2015.
 */
public class SearchAlamatActivity extends BaseActivity implements OnMapReadyCallback, View.OnClickListener, GoogleMap.OnMarkerClickListener {
    private GPSTracker location;
    private RequestQueue mRequest;
    EditText edt_alamat, txt_alamat;
    Alamat alamat;
    ListView list;
    GoogleMap gmap;
    private Button simpan;
    private TextView title;
    private ImageView close, back;
    MapFragment mapFragment;
    LatLng centerFromPoint;
    MaterialDialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mRequest = Volley.newRequestQueue(getApplicationContext());

    }

    @Override
    public void initView() {
        dialog = new MaterialDialog.Builder(this).build();
        close = (ImageView) findViewById(R.id.close);
        close.setVisibility(View.GONE);
        back = (ImageView) findViewById(R.id.back);
        list = (ListView) findViewById(R.id.listView);
        list.setVisibility(View.GONE);
        title = (TextView) findViewById(R.id.tv_actionbar_title);
        txt_alamat = (EditText) findViewById(R.id.txt_alamat);
        simpan = (Button) findViewById(R.id.simpan);
        mapFragment = (MapFragment) this.getFragmentManager()
                .findFragmentById(R.id.map);
        edt_alamat = (EditText) findViewById(R.id.alamat);
        mapFragment.getMapAsync(this);
        Typeface tf = Typeface.createFromAsset(getApplicationContext().getAssets(), "BREVIA-BLACK.OTF");
        title.setTypeface(tf);
        title.setText("PRAKTIK");
        edt_alamat.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                loadListItem();
            }
        });

        txt_alamat.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                list.setVisibility(View.GONE);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        if (!isNetworkAvailable()){
            txt_alamat.setText("-");
            if (!dialog.isShowing()) {
                dialog2(getResources().getString(R.string.NoConnection), getResources().getString(R.string.Perhatian));
            }
        }

    }

    @Override
    public void setUICallbacks() {
        simpan.setOnClickListener(this);
        back.setOnClickListener(this);
    }

    @Override
    public int getLayout() {
        return R.layout.activity_search_alamat;
    }

    @Override
    public void updateUI() {

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

    }

    @Override
    public void onClick(View v) {
        if (v == simpan) {
            if (!txt_alamat.getText().toString().trim().equalsIgnoreCase("") &&
                    !txt_alamat.getText().toString().trim().equalsIgnoreCase("-")) {
                Intent i = new Intent(SearchAlamatActivity.this, PraktikActivity.class);
                i.putExtra("result", txt_alamat.getText().toString());
                i.putExtra("longitude", String.valueOf(centerFromPoint.longitude));
                i.putExtra("latitude", String.valueOf(centerFromPoint.latitude));
                setResult(2, i);
                finish();
            } else {
                dialog2("Harap isi alamat terlebih dahulu sebelum menyimpan", getResources().getString(R.string.Perhatian));
            }
        } else if (v == back) {
            finish();
        }
    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        return false;
    }


    @Override
    public void onMapReady(final GoogleMap googleMap) {
        Bundle bn = getIntent().getExtras();
        try {
            if (!bn.getString("latitude").equals("") ||
                    !bn.getString("longitude").equals("")) {
                LatLng currentLocation = new LatLng(Double.valueOf(bn.getString("latitude")), Double.valueOf(bn.getString("longitude")));
                googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(currentLocation, 16));
                googleMap.setMyLocationEnabled(true);
                gmap = googleMap;
            } else {
                location = new GPSTracker(this);
                LatLng currentLocation = new LatLng(location.getLatitude(), location.getLongitude());
                googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(currentLocation, 16));
                googleMap.setMyLocationEnabled(true);
                gmap = googleMap;
            }
        } catch (NullPointerException e) {
            location = new GPSTracker(this);
            LatLng currentLocation = new LatLng(location.getLatitude(), location.getLongitude());
            googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(currentLocation, 16));
            googleMap.setMyLocationEnabled(true);
            gmap = googleMap;
        }

        googleMap.setOnCameraChangeListener(new GoogleMap.OnCameraChangeListener() {
            @Override
            public void onCameraChange(CameraPosition cameraPosition) {


                VisibleRegion visibleRegion = googleMap.getProjection()
                        .getVisibleRegion();

                Point x = googleMap.getProjection().toScreenLocation(visibleRegion.farRight);

                Point y = googleMap.getProjection().toScreenLocation(visibleRegion.nearLeft);

                Point centerPoint = new Point(x.x / 2, y.y / 2);

                centerFromPoint = googleMap.getProjection().fromScreenLocation(
                        centerPoint);

                getAlamat(String.valueOf(centerFromPoint.latitude), String.valueOf(centerFromPoint.longitude));
            }
        });

    }

    @Override
    public void onBackPressed() {
        finish();
    }

    private void loadListItem() {
        StringRequest listdata = new StringRequest(Request.Method.GET,
                ApiReferences.getLokasi(edt_alamat.getText().toString()),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.i("RESULT JSON", response.toString());

                        alamat = Parser.getParserAlamat(response
                                .toString());

                        if (alamat.getStatus().equalsIgnoreCase("ZERO_RESULTS")) {
                            list.setVisibility(View.GONE);
                        } else {
                            list.setVisibility(View.VISIBLE);
                            AdapterListAlamat adapterList = new AdapterListAlamat(
                                    alamat, getApplicationContext());
                            list.setAdapter(adapterList);

                            list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                @Override
                                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                    try {
                                        LatLng currentLocation = new LatLng(alamat.getResults().get(position).getGeometry().getLocation().getLat()
                                                , alamat.getResults().get(position).getGeometry().getLocation().getLng());
                                        gmap.moveCamera(CameraUpdateFactory.newLatLngZoom(currentLocation, 16));
                                        gmap.setMyLocationEnabled(true);
                                        txt_alamat.setText(alamat.getResults().get(position).getFormatted_address());

                                    } catch (IndexOutOfBoundsException e) {
                                        e.printStackTrace();
                                    }

                                }
                            });
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                txt_alamat.setText("-");
                if (!dialog.isShowing()) {
                    dialog2(getResources().getString(R.string.NoConnection), getResources().getString(R.string.Perhatian));
                }
            }
        });
        mRequest.add(listdata);
    }


    private void getAlamat(String lat, String lng) {
        StringRequest listdata = new StringRequest(Request.Method.GET,
                ApiReferences.getLokasiByLatlng(lat, lng),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.i("RESULT JSON", response.toString());
                        alamat = Parser.getParserAlamat(response
                                .toString());
                        if (alamat.getStatus().equalsIgnoreCase("ZERO_RESULTS")) {
                            //
                        } else {
                            try {
                                txt_alamat.setText(alamat.getResults().get(0).getFormatted_address());
                            } catch (IndexOutOfBoundsException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                txt_alamat.setText("-");
                if (!dialog.isShowing()) {
                    dialog2(getResources().getString(R.string.NoConnection), getResources().getString(R.string.Perhatian));
                }
            }
        });
        mRequest.add(listdata);
    }


    public void dialog2(String content, String msg) {
        dialog = new MaterialDialog.Builder(this)
                .title(msg)
                .cancelable(false)
                .content(content)
                .negativeText("OK")
                .callback(new MaterialDialog.ButtonCallback() {
                    @Override
                    public void onPositive(MaterialDialog dialog) {
                        txt_alamat.setText("-");
                    }

                    @Override
                    public void onNegative(MaterialDialog dialog) {
                        super.onNegative(dialog);
                    }

                })
                .show();
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

}
